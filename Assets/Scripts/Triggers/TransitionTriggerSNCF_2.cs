﻿using UnityEngine;
using System.Collections;
using Toolz.Transitions;

namespace Toolz.Triggers {

	public class TransitionTriggerSNCF_2 : MonoBehaviour {


		[Tooltip("Time when the \"To\" module will be activated (relative to the beginning of the transition).")]
		public float ModuleToActivationTime = 0f;
		[Tooltip("Time when the \"From\" module will be deactivated (relative to the beginning of the transition).")]
		public float ModuleFromDeactivationTime = 0f;


		[Tooltip("Duration after which this transition brick will activate, relative to global transition's start (in seconds)")]
		public float StartingTime = 0f;
		[Tooltip("Transition's duration in seconds")]
		public float TransitionDuration = 2f;

		public Module _from;
        [Tooltip("The module we are transitioning to")]
        public Module To;

        //Has a CustomEditor attached to it handling transitionName
        [HideInInspector]
        public string TransitionName = "Null";

        //The module we are transitioning from
        
		Transition _transitionModele;
		Transition _transition;


		private void Awake() {

            _from = GetComponentInParent<Module>();
#if UNITY_EDITOR
            if (TransitionName == "Null") {
                Debug.LogWarning("No transition added to trigger " + gameObject.name);
            }
            if (_from == null) {
				Debug.LogWarning("A trigger cannot be outside of a Module." + gameObject.name);
            }
#endif

        }

        public void TriggerTransition() {
            Resources.UnloadUnusedAssets();
			if(TransitionName != "Null"){
				//Debug.LogError("TriggerTransition");
				_transitionModele = Resources.Load<Transition>(Transition.TRANSITIONS_PATH + "/" + TransitionName);
				_transition = (Transition) Instantiate(_transitionModele);
				_transition.gameObject.SetActive(true);
				_transition.ModuleFromDeactivationTime = ModuleFromDeactivationTime;
				_transition.ModuleToActivationTime = ModuleToActivationTime;
				_transition.DoTransition(_from, To, StartingTime, TransitionDuration);
			}else{
				_from.Deactivate();
				To.Activate();
			}
        }
    }
}