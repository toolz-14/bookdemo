﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SliderAddBornes : MonoBehaviour {

	public Text SliderValue;
	public GameObject[] BornesList = new GameObject[4];
	private Slider theSlider;

	private void Start () {
		theSlider = GetComponent<Slider> ();
		SliderValue.text = "0";
	}
	
	public void CalculateValues(){
		SliderValue.text = theSlider.value.ToString ();
		for (int i=0; i<BornesList.Length; i++) {
			if (i <= (int)((theSlider.value - 1f))) {
				BornesList [i].SetActive (true);
			} else {
				BornesList [i].SetActive (false);
			}
		}
	}
}
