﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
//using Vector = MathNet.Numerics.LinearAlgebra.Vector<double>;

public class WaterfallUIManager_Fiches : MonoBehaviour {

	//pivot at the top of element
	public GameObject Line_Expenses;
	//pivot at the bottom of element
	public GameObject Line_Revenues;
	//pivot at the bottom of element
	public RectTransform VerticalBar;

	//parent for blocks
	public RectTransform GraphBlockHolder;

	public RectTransform Footer;
	public RectTransform Texts;
	public InputField inputField;
	public RectTransform DiamondGraph;

	int nbYears = 15;  //variable decided by toolz

	private List<float> _expensesPerYear = new List<float>();
	private List<float> _revenuesPerYear = new List<float>();

	private float _previousTempRevenues = 0;
	private float _previousRevenuesSize = 0;
	private Vector3 _tempVect3;

	private const float LOW_RATE = 0.01f;
	private const float HIGH_RATE = 0.5f;
	private const int MAX_ITERATION = 1000;
	private const float PRECISION_REQ = 0.00000001f;
	[HideInInspector]
	public bool _isOpen;


	private float _valueMin;
	private float _valueMax;
	private float _valueBestMinMax;
	private float _chartSize;

	private float _maxExpense;
	private float _maxRevenue;

	private List<GameObject> _objectHolder = new List<GameObject>();
	private float _aUnitHeight;
	private GameObject _tempRevenues;
	private GameObject _tempExpenses;
	
	private void Start () {
		_isOpen = false;
		inputField.text = "0";
	}

	public void OpenPanel(){
		_isOpen = true;
		SetPanelValues ();

		if(DiamondGraph.localScale.x > 0f){
			DiamondGraph.GetComponent<DiamondUIManager_Fiches>().ClosePanel();
		}

		GetComponent<Animation> ().GetComponent<Animation>() ["WaterfallGraph"].speed = 1f;
	
		GetComponent<Animation> ().Play ("WaterfallGraph");
	}

	public void ClosePanel(){
		_isOpen = false;
		GetComponent<Animation>().GetComponent<Animation>()["WaterfallGraph"].time = GetComponent<Animation>().GetComponent<Animation>()["WaterfallGraph"].length;
		GetComponent<Animation>().GetComponent<Animation>()["WaterfallGraph"].speed = -1f;
		
		GetComponent<Animation>().Play("WaterfallGraph");

		DestroyGraphElements ();
		Invoke ("Deactivate", 0.4f);
	}

	private void Deactivate(){
		gameObject.SetActive (false);
	}

	private void SetPanelValues(){

		SetExpensesPerYear ();
		SetRevenuesPerYear ();

		CalculateValueBestMinMax ();
		SetAmountsTexts ();

		//break even text
		Footer.Find("BreakEven").Find("Text_BreakEvenDynamic").GetComponent<Text>().text = CalculateBreakEvenYear ().ToString();

		//update localisation with word plural or not
		if (CalculateBreakEvenYear () < 2) {
			Footer.Find ("BreakEven").Find ("Text_BreakEvenyears").GetComponent<Lean.LeanLocalizedText> ().PhraseName = "WaterfallGraphUIYear";
		} else {
			Footer.Find ("BreakEven").Find ("Text_BreakEvenyears").GetComponent<Lean.LeanLocalizedText> ().PhraseName = "WaterfallGraphUIYears";
		}
		Footer.Find("BreakEven").Find("Text_BreakEvenyears").GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();

		double[] cashflow = new double[nbYears];
		for (int i=0; i<nbYears; i++) {
			cashflow[i] = _revenuesPerYear[i] - _expensesPerYear[i];
		}

//		//rate text
//		float irr = 0.00f;
//		irr = (float)computeIRR(cashflow, nbYears);
//		Footer.FindChild("ProfitabilityRate").FindChild("Text_ProfitabilityDynamic").GetComponent<Text>().text = (irr*100f).ToString()+" %";
//
//		//net value text
//		float npv = 0.00f;
//		npv = (float)NetPresentValue (cashflow, irr);
//		Footer.FindChild("Netvalue").FindChild("Text_NetvalueDynamic").GetComponent<Text>().text = (npv).ToString()+" $";

		_objectHolder.Clear ();

		_aUnitHeight = VerticalBar.sizeDelta.y / (2 * _valueBestMinMax);
		for (int i=0; i<nbYears; i++) {
			SetDoubleBar(i);
		}

		CalculateAndDisplayCancellingCost ();
	}

	private void SetExpensesPerYear(){
		_expensesPerYear.Clear ();
		//_expensesPerYear.Add (8);
		for (int i=0; i<nbYears; i++) {
			//only for testing
//			_expensesPerYear.Add(UnityEngine.Random.Range(1000f, 10000f));
			if(i == 0){
				_expensesPerYear.Add(155000f);
			}else if( i > 0 && i<nbYears-1 ){
				_expensesPerYear.Add(3500f);
			}else{
				//i == nbYears-1
				_expensesPerYear.Add(35000f);
			}
//			Debug.Log("_expensesPerYear[i] = "+_expensesPerYear[i]);
		}
	}

	private void SetRevenuesPerYear(){
		_revenuesPerYear.Clear ();
		for (int i=0; i<nbYears; i++) {
			//only for testing
//			_revenuesPerYear.Add(UnityEngine.Random.Range(1000f, 10000f));
			_revenuesPerYear.Add(10000f);
		}
	}

	private void CalculateValueBestMinMax(){
		float previousYeartotal = 0f;
		float tempMin = 0f;
		float currentMin = 0f;

		float tempMax = 0f;
		float currentMax = 0f;

		// lowest bottom of red bar
		for (int i=0; i<nbYears; i++) {

			if(i==0){
				tempMin = - _expensesPerYear[i];
			}else{
				previousYeartotal += _revenuesPerYear[i-1]-_expensesPerYear[i-1];
				tempMin = previousYeartotal - _expensesPerYear[i];
			}
//			Debug.Log("tempMin = "+tempMin+"  currentMin = "+currentMin);
			if(tempMin < currentMin){
				currentMin = tempMin;
			}
		}

		previousYeartotal = 0f;
		//highest top of green bar
		for (int i=1; i<nbYears; i++) {
			previousYeartotal += _revenuesPerYear[i-1]-_expensesPerYear[i-1];
			tempMax = previousYeartotal + _revenuesPerYear[i] - _expensesPerYear[i];
//			Debug.Log("tempMax = "+tempMax+"  currentMax = "+currentMax);
			if(tempMax > currentMax){
				currentMax = tempMax;
			}
		}
		
		if (Mathf.Abs(currentMin) > currentMax) {
			_valueBestMinMax = Mathf.Abs(currentMin);
		} else {
			_valueBestMinMax = currentMax;
		}
	}

	private void SetAmountsTexts(){
		Texts.Find ("Text_Amount_0").GetComponent<Text> ().text = ((int)_valueBestMinMax).ToString();
		Texts.Find ("Text_Amount_1").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* 8)).ToString();
		Texts.Find ("Text_Amount_2").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* 6)).ToString();
		Texts.Find ("Text_Amount_3").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* 4)).ToString();
		Texts.Find ("Text_Amount_4").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* 2)).ToString();

		Texts.Find ("Text_Amount_5").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* -2)).ToString();
		Texts.Find ("Text_Amount_6").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* -4)).ToString();
		Texts.Find ("Text_Amount_7").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* -6)).ToString();
		Texts.Find ("Text_Amount_8").GetComponent<Text> ().text = ((int)((_valueBestMinMax/10)* -8)).ToString();
		Texts.Find ("Text_Amount_9").GetComponent<Text> ().text = ((int)(_valueBestMinMax * -1)).ToString();
	}

	public void CalculateAndDisplayCancellingCost(){
		int year = 0;
		int.TryParse(inputField.text, out year);
		float result = 0f;

		//to prevent index outta range error:
		if(year < 0){
			year = 0;
			inputField.text = year.ToString();
		}
		if(year > nbYears){
			year = nbYears;
			inputField.text = year.ToString();
		}

		for(int i=0; i<year; i++){
			result += (_revenuesPerYear[i] - _expensesPerYear[i]);
		}

		Footer.Find ("CostOfCancelling").Find ("Text_CancellingCostsDynamic").GetComponent<Text> ().text = result.ToString ()+" $";
	}

	private void SetDoubleBar(int i){

		//expenses
		_tempExpenses = Instantiate (Line_Expenses) as GameObject;
		_tempExpenses.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_tempExpenses.GetComponent<RectTransform> ().sizeDelta.x, (_aUnitHeight *_expensesPerYear[i]));

		//set parent
		_tempExpenses.transform.SetParent (GraphBlockHolder);

		//set pos, !!! the first expenses needs to start from graph 0 which is (anchored) y = 318.5 x = -160.5 !!!
		_tempExpenses.transform.localScale = new Vector3 (1f, 1f, 1f);
		_tempVect3 = _tempExpenses.GetComponent<RectTransform> ().anchoredPosition3D;
		//then each element is every 12 x
		_tempVect3.x = -160.5f + (12*(i*2));

		if (i == 0) {
			//expenses top must be aligned with graph 0
			_tempVect3.y = VerticalBar.anchoredPosition.y + VerticalBar.sizeDelta.y/2;
		} else {
			//expenses top must be aligned with previous year revenues top
			_tempVect3.y = _previousTempRevenues + _previousRevenuesSize;
		}
		_tempVect3.z = 0;
		_tempExpenses.GetComponent<RectTransform> ().anchoredPosition3D = _tempVect3;

		//save
		_objectHolder.Add (_tempExpenses);

		//revenue
		_tempRevenues = Instantiate (Line_Revenues) as GameObject;
		_tempRevenues.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_tempRevenues.GetComponent<RectTransform> ().sizeDelta.x, (_aUnitHeight *_revenuesPerYear[i]));

		//set parent
		_tempRevenues.transform.SetParent (GraphBlockHolder);

		//set pos
		_tempRevenues.transform.localScale = new Vector3 (1f, 1f, 1f);
		_tempVect3 = _tempRevenues.GetComponent<RectTransform> ().anchoredPosition3D;
		//then each element is every 12 x
		_tempVect3.x = -160.5f + (12*((i*2)+1));

		//revenues bottom must be aligned with this year expenses bottom
		_tempVect3.y = _tempExpenses.GetComponent<RectTransform> ().anchoredPosition3D.y - _tempExpenses.GetComponent<RectTransform> ().sizeDelta.y;
		_tempVect3.z = 0;
		_tempRevenues.GetComponent<RectTransform> ().anchoredPosition3D = _tempVect3;

		//save
		_objectHolder.Add (_tempRevenues);

		//save previous year revenues position and size
		_previousTempRevenues = _tempRevenues.GetComponent<RectTransform> ().anchoredPosition3D.y;
		_previousRevenuesSize = _tempRevenues.GetComponent<RectTransform> ().sizeDelta.y;
	}

	private void DestroyGraphElements(){

		if(_objectHolder != null && _objectHolder.Count > 0){
			foreach(GameObject go in _objectHolder){
				Destroy(go);
			}
			_objectHolder.Clear();
		}
	}

	private int CalculateBreakEvenYear(){
		float result = 0;
		for (int i=0; i<nbYears; i++) {
			result += (_revenuesPerYear[i] - _expensesPerYear[i]);
			if(result > 0){
				return i;
			}
		}
		return 0;
	}

	private double computeIRR(double[] cf, int numOfFlows) {
		int i = 0;
		int j = 0;
		double old = 0.00;
		double _new = 0.00;
//		double oldguessRate = LOW_RATE;
		double newguessRate = LOW_RATE;
		double guessRate = LOW_RATE;
		double lowGuessRate = LOW_RATE;
		double highGuessRate = HIGH_RATE;
		double npv = 0.0;
		double denom = 0.0;
		for(i=0; i<MAX_ITERATION; i++)	{
			npv = 0.00;
			for(j=0; j<numOfFlows; j++)	{
				denom = Math.Pow((1 + guessRate),j);
				npv = npv + (cf[j]/denom);
			}
			/* Stop checking once the required precision is achieved */
			if((npv > 0) && (npv < PRECISION_REQ))
				break;
			if(old == 0)
				old = npv;
			else
				old = _new;
			_new = npv;
			if(i > 0){
				if(old < _new)	{
					if(old < 0 && _new < 0)
						highGuessRate = newguessRate;
					else
						lowGuessRate = newguessRate;
				}else{
					if(old > 0 && _new > 0)
						lowGuessRate = newguessRate;
					else
						highGuessRate = newguessRate;
				}
			}
//			oldguessRate = guessRate;
			guessRate = (lowGuessRate + highGuessRate) / 2;
			newguessRate = guessRate;
		}
		return guessRate;
	}

	private double NetPresentValue(double[] c, double r) {
//		int noOfPeriods = c.Length; // including time 0
//		var ror = 1 + r;
//		
//		var cashflowValues = Vector.Build.Dense(c);
//		
//		var discountValues = Vector.Build.Dense(noOfPeriods, currPeriod => 1 / Math.Pow(ror, currPeriod));
//		
//		var presentValues = cashflowValues.PointwiseMultiply(discountValues);
//		
//		return presentValues.Sum();
		return 0.01;
	}
}
