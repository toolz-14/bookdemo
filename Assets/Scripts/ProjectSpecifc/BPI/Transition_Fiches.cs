﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Transition_Fiches : MonoBehaviour {
    //Path to transitions's prefab in 'Resources' folder
    public static readonly string TRANSITIONS_PATH = "SetTransitionsBPI";

	[HideInInspector]
    public float ModuleToActivationTime;
  
	[HideInInspector]
	public float ModuleFromDeactivationTime;

    //The module we are transitioning from
	public Module_Fiches From {get; private set;}
	public Toolz.Module From2 {get; private set;}
    //The module we are transitioning to
	public Module_Fiches To {get; private set;}
	public Toolz.Module To2 {get; private set;}

	private List<TransitionBrick_Fiches> _bricks;
    private float _totalDuration;
	private TransitionBrick_Fiches _brick;

	private void Awake() {
		_bricks = new List<TransitionBrick_Fiches>();
        _totalDuration = 0f;
        foreach (Transform child in transform) {
			_brick = child.GetComponent<TransitionBrick_Fiches>();
			if (_brick == null) {
                Debug.LogError("A Transition cannot have a child which is not a TransitionBrick.");
                continue;
            }
			_bricks.Add(_brick);   
			_brick.gameObject.SetActive(false);
        }
    }

	private void Start() {

    }

    // PUBLIC METHODS

	public void DoTransition(Module_Fiches from, Module_Fiches to, float startingTime, float transitionDuration) {
        From = from;
        To = to;
		foreach (TransitionBrick_Fiches brick in _bricks) {
            brick.gameObject.SetActive(true);
			brick.StartingTime = startingTime;
			brick.TransitionDuration = transitionDuration;
			float totalDuration = startingTime + transitionDuration;
			if (totalDuration > _totalDuration) {
				_totalDuration = totalDuration;
			}
        }
        
        BroadcastMessage("ActivateTransition");
        Invoke("TransitionFinished", _totalDuration);

		if(ModuleToActivationTime < 0f) {
            Debug.LogError("ModuleToActivationTime is negative.");
            ModuleToActivationTime = 0f;
        }

        Invoke("ActivateModuleTo", ModuleToActivationTime);
        Invoke("DeactivateModuleFrom", ModuleFromDeactivationTime);
    }

	public void DoTransitionTo(Module_Fiches from, Toolz.Module to, float startingTime, float transitionDuration) {
		From = from;
		To2 = to;
		foreach (TransitionBrick_Fiches brick in _bricks) {
			brick.gameObject.SetActive(true);
			brick.StartingTime = startingTime;
			brick.TransitionDuration = transitionDuration;
			float totalDuration = startingTime + transitionDuration;
			if (totalDuration > _totalDuration) {
				_totalDuration = totalDuration;
			}
		}

		BroadcastMessage("ActivateTransition");
		Invoke("TransitionFinished", _totalDuration);

		if(ModuleToActivationTime < 0f) {
			Debug.LogError("ModuleToActivationTime is negative.");
			ModuleToActivationTime = 0f;
		}

		Invoke("ActivateModuleTo2", ModuleToActivationTime);
		Invoke("DeactivateModuleFrom", ModuleFromDeactivationTime);
	}

	public void DoTransitionFrom(Toolz.Module from, Module_Fiches to, float startingTime, float transitionDuration) {
		From2 = from;
		To = to;
		foreach (TransitionBrick_Fiches brick in _bricks) {
			brick.gameObject.SetActive(true);
			brick.StartingTime = startingTime;
			brick.TransitionDuration = transitionDuration;
			float totalDuration = startingTime + transitionDuration;
			if (totalDuration > _totalDuration) {
				_totalDuration = totalDuration;
			}
		}

		BroadcastMessage("ActivateTransition");
		Invoke("TransitionFinished", _totalDuration);

		if(ModuleToActivationTime < 0f) {
			Debug.LogError("ModuleToActivationTime is negative.");
			ModuleToActivationTime = 0f;
		}

		Invoke("ActivateModuleTo", ModuleToActivationTime);
		Invoke("DeactivateModuleFrom2", ModuleFromDeactivationTime);
	}

    // PRIVATE METHODS

    private void TransitionFinished() {

		foreach (TransitionBrick_Fiches brick in _bricks) {
            brick.gameObject.SetActive(false);
        }

        Invoke("DestroyMyself", 0.5f);
    }

    private void ActivateModuleTo() {
        To.Activate();
    }

	private void ActivateModuleTo2() {
		To2.Activate();
	}

    private void DeactivateModuleFrom() {
        From.Deactivate();
    }

	private void DeactivateModuleFrom2() {
		From2.Deactivate();
	}

    private void DestroyMyself() {
        Destroy(gameObject);
    }
}