﻿using UnityEngine;
using System.Collections;
using System.Xml;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class FreeTerrainCamera_Fiches : BaseCamera_Fiches
{
	public float Speed = 1f;
    public bool fixedElevation = false;
    public float minElevation = 0;
    public float maxElevation = 500;
    public float elevationOffset = 10; // offset de la camera en y par rapport a l'altitude
    public float minRX = 20;
    public float maxRX = 90;
	public MeshFilter terrain;
	public string heightMapID;
	public Vector2 heightMapOffset = new Vector2(0, 0);

	private Texture2D _heightMap;

    public bool hasPositionConstrains = false;
    public Vector3 minPosConstrain = Vector3.zero;
    public Vector3 maxPosConstrain = Vector3.zero;

    protected Vector3 _rxOffset = Vector3.zero;

    protected Vector3 _min;
    protected Vector3 _max;
	protected Vector3 _drag;

    protected Vector2 _textureSize;
    protected Vector3 _posRatio;
    protected float _elevation = 100;
    protected float _altitude = 0;
    protected float _minAltitude;
    protected float _altitudeRatio = 0;

    private float _speedMult = 1f;
    private bool _autoElevationRotation = true;

	private Vector3 _tempVect3;
	private Vector3 _newZoomPosition;

	private bool _updatePosition = true;
	[HideInInspector]
	public bool IsMovingByGesture = true;

	#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
	#endif
	public static bool ForwardCollision;


    //------------------| Awake |
 
    protected override void Awake()
    {
		base.Awake();
        _posRatio = Vector3.zero;
        _drag = Vector3.zero;
        _min = Vector3.zero;
        _max = Vector3.zero;

		if(this.heightMap != null){
//			this.heightMap = SharedSettings.instance.assets.texture(heightMapID);
		}

		if (terrain != null) {
			InitBounds ();
		}

		ApplyClosestConfig ();
    }

	protected override void Start () 
	{
		base.Start();
	}

    //------------------| Bounds |

    private void InitBounds()
    {
        Mesh m = terrain.mesh;
        Vector3 scale = terrain.transform.localScale;
        Vector3 position = terrain.transform.position;
        Vector3 v;

        for(int i = 0 ; i<m.vertexCount ; ++i)
        {
            v = m.vertices[i];
            v.x *= scale.x;
            v.y *= scale.y;
            v.z *= scale.z;
            v += position;

            if (v.x < _min.x) _min.x = v.x;
            if (v.y < _min.y) _min.y = v.y;
            if (v.z < _min.z) _min.z = v.z;
            if (v.x > _max.x) _max.x = v.x;
            if (v.y > _max.y) _max.y = v.y;
            if (v.z > _max.z) _max.z = v.z;
        }

        //Debug.Log("min = " + _min + ", max = " + _max);
    }

	/**
     * Deplace le gameObject vers la position la plus proche
     */ 
	protected void ApplyClosestConfig()
	{
		_position = _cameraHolder.position;
		_rotation = _cameraHolder.rotation.eulerAngles;
		
//		_posRatio.x = (_position.x - _min.x) / (_max.x - _min.x);
//		_posRatio.z = (_position.z - _min.z) / (_max.z - _min.z);
//		_altitude = GetAltitude();
//		
//		if(fixedElevation)
//		{
//			_altitude = _min.y + (_max.y - _min.y) * _altitude + elevationOffset;
//			_position.y = _altitude;
//			_elevation = elevationOffset;
//		}
//		else
//		{
//			_elevation = _position.y;
//			// terrain min altitude
//			_altitude = _min.y + (_max.y - _min.y) * _altitude; 
//			if (_elevation < _altitude) _elevation = _altitude;
//			
//			_altitudeRatio = ((_elevation - _altitude) / (maxElevation - _altitude));
//			_rotation.x = minRX + _altitudeRatio * (maxRX - minRX);
//
//			_position.y = _elevation + elevationOffset;
//		}
		
		_cameraHolder.position = _position;
		_cameraHolder.rotation = Quaternion.Euler(_rotation);
	}

    //------------------| Activate / Deactivate |

    protected override void OnEnable()
    {
		base.OnEnable();
		if (_cameraHolder == null) {
			return;
		}
		_updatePosition = true;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
		_updatePosition = false;
    }

    //------------------| Reset |

    public void Reset()
    {
        _autoElevationRotation = true;
        //_speedMult = 1f;
    }


    //------------------| Debug |

//    private void LogTerrain()
//    {
//        string s = "<terrain>";
//        s += "\n\t<vertices>";
//        for (int i = 0; i < terrain.mesh.vertexCount; ++i)
//        {
//            if (i > 0) s += "|";
//            s += terrain.mesh.vertices[i].x + "|";
//            s += terrain.mesh.vertices[i].y + "|";
//            s += terrain.mesh.vertices[i].z;
//        }
//        s += "\n\t</vertices>";
//        s += "\n\t<indices>";
//        for (int i = 0; i < terrain.mesh.triangles.Length; ++i)
//        {
//            if (i > 0) s += "|";
//            s += terrain.mesh.triangles[i];
//        }
//        s += "\n\t</indices>";
//        s += "\n</terrain>";
//
//        Debug.Log(s);
//    }

    //------------------| Update |

	protected override void Update ()
	{
		base.Update ();
		dt *= _speedMult;
		//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
		if (EventSystem.current != null) {
			if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
				return;
			}
		}

		if (IsMovingByGesture) {
			//Debug.Log ("before lerping");

			_tempVect3 = Vector3.Lerp (_cameraHolder.position, _position, moveSpeed * dt);

			if (!AuthorizeMove (_tempVect3, false)) {
				_updatePosition = false;
				_position = _cameraHolder.position;
			} else {
				if (Physics.Linecast (_cameraHolder.position, _tempVect3) ) {
					_updatePosition = false;
					_position = _cameraHolder.position;
				}else{
					AuthorizeMove (_tempVect3, true);
					_updatePosition = true;
				}
			}

			_posRatio.x = (_cameraHolder.position.x - _min.x) / (_max.x - _min.x);
			_posRatio.z = (_cameraHolder.position.z - _min.z) / (_max.z - _min.z);
			_altitude = GetAltitude ();
		
			if (fixedElevation) {
				_altitude = _min.y + (_max.y - _min.y) * _altitude + _elevation;
//				_position.y = _altitude;
			} else {
				// terrain min altitude
				_altitude = _min.y + (_max.y - _min.y) * _altitude; 
				if (_elevation < _altitude)
					_elevation = _altitude;
			
				_altitudeRatio = ((_elevation - _altitude) / (maxElevation - _altitude));

//				if (_autoElevationRotation){
//					_rotation.x = minRX + _altitudeRatio * (maxRX - minRX);
//				}
//
//				_tempVect3 = _position;
//				_tempVect3.y = _elevation + elevationOffset;
//	
//				if (!AuthorizeMove (_tempVect3, false)) {
//				} else {
//					_position.y = _elevation + elevationOffset;
//				}
//			
//				_rotation += _rxOffset * (1 - _altitudeRatio);
			
				moveSens = (.2f + .8f * _altitudeRatio) * Speed;
				zoomSens = .5f + .5f * _altitudeRatio;
				rotateSens = .4f + .6f * _altitudeRatio;
			}

			_cameraHolder.rotation = Quaternion.Lerp (_cameraHolder.rotation, Quaternion.Euler (_rotation), rotateSpeed * dt);
		}
    }


    //------------------| Utils |

    protected float GetAltitude()
    {
		if (_heightMap != null) {
			return _heightMap.GetPixel (
               Mathf.RoundToInt ((1 - _posRatio.x) * _textureSize.x),
               Mathf.RoundToInt ((1 - _posRatio.z) * _textureSize.y)
			).r;
		} else {
			return _cameraHolder.position.y;
		}
    }

    //------------------| Special |

    public override void MoveTo(Transform t)
    {
        MoveTo(t.position, t.eulerAngles);
        if (!_enabled)
        {
            transform.position = _position;
            transform.eulerAngles = _rotation + _rxOffset;
        }
    }

    public override void MoveTo(Vector3 position, Vector3 eulerAngles)
    {
        _speedMult = .4f;

        _position.x = position.x;
        _position.z = position.z;
		_position.y = position.y;
        _elevation = position.y - elevationOffset;

        _rotation = eulerAngles;

        _altitude = GetAltitude();
        _altitude = _min.y + (_max.y - _min.y) * _altitude; // terrain min altitude
        if (_elevation < _altitude) _elevation = _altitude;

        _position.y = _elevation + elevationOffset;

        _rxOffset = Vector3.zero;
    }


    //------------------| Gestures |

    public override void Drag(DragGesture g, Vector2 magnitude)
    {
		if (_updatePosition) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			_speedMult = 1f;

        
			_drag.x = -magnitude.x * moveSens;
			_drag.z = -magnitude.y * moveSens;

			float angle = Mathf.Atan2 (_drag.z, _drag.x) - _cameraHolder.eulerAngles.y * Mathf.Deg2Rad;
			float move = _drag.magnitude;

			//Debug.Log("FreeTerrainCamera DRAG......" + magnitude);
			_position.x += Mathf.Cos (angle) * move;
			_position.z += Mathf.Sin (angle) * move;

			if (terrain != null) {
				//Debug.Log("position = " + _position);
				if (hasPositionConstrains) {
					if (_position.x < minPosConstrain.x)
						_position.x = minPosConstrain.x;
					else if (_position.x > maxPosConstrain.x)
						_position.x = maxPosConstrain.x;
					if (_position.z < minPosConstrain.z)
						_position.z = minPosConstrain.z;
					else if (_position.z > maxPosConstrain.z)
						_position.z = maxPosConstrain.z;
				}

				if (_position.x < _min.x)
					_position.x = _min.x;
				else if (_position.x > _max.x)
					_position.x = _max.x;
				if (_position.z < _min.z)
					_position.z = _min.z;
				else if (_position.z > _max.z)
					_position.z = _max.z;
			}
		}
    }

    public override void Zoom(ZoomGesture g, float delta)
    {
		if (enabled) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			//Debug.Log("FreeTerrainCamera Zoom......" + delta);
			_autoElevationRotation = true;
			_speedMult = 1f;

			_position.y -= delta * zoomSpeed * zoomSens;

			//if (fixedElevation) return;
//		_elevation -= delta * zoomSpeed * zoomSens;
//
//		if (_elevation < minElevation)
//			_elevation = minElevation;
//		else if (_elevation > maxElevation)
//			_elevation = maxElevation;
		}
    }

    public override void Rotate(RotateGesture g, float angle)
    {
		if (_updatePosition) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			//Debug.Log("FreeTerrainCamera Rotate......" + angle);
			_speedMult = 1f;
			_rotation.y += angle * 4 * rotateSens;
		}
    }

    public override void Pan(PanGesture g, Vector2 panning)
    {
		if (_updatePosition) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			//Debug.Log("FreeTerrainCamera Pan......" + panning);
			_speedMult = 1f;
			_rotation.y -= panning.x * .8f * rotateSens;


			if (Mathf.Abs (panning.y) > 0){
				_autoElevationRotation = true;
			}
			//_rxOffset.x += panning.y * .8f * rotateSens;

			ConstrainRotationX ();
			_rotation.x += panning.y * .8f * rotateSens;
		}
    } 

    private void ConstrainRotationX()
    {
		if (_rxOffset.x < -20){ _rxOffset.x = -20;}
		else if (_rxOffset.x > 20){ _rxOffset.x = 20;}
    }


    //------------------| Getters / Setters |

    public Texture2D heightMap
    {
        get { return _heightMap; }
        set
        {
            _heightMap = value;

            _textureSize = new Vector2(
                _heightMap.width - 2 * heightMapOffset.x,
                _heightMap.height - 2 * heightMapOffset.y
            );
        }
    }

    public bool autoElevationRotation
    {
        get { return _autoElevationRotation; }
        set
        {
            _autoElevationRotation = value;
        }
    }
}
