﻿using UnityEngine;
using System.Collections;

public abstract class TransitionBrick_Fiches : MonoBehaviour {

    [HideInInspector]
    public float StartingTime;
    [HideInInspector]
    public float TransitionDuration;

    public abstract void ActivateTransition();
}
