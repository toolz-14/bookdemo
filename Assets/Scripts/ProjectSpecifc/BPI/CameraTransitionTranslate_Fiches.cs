﻿using UnityEngine;
using System.Collections;

public class CameraTransitionTranslate_Fiches : TransitionBrick_Fiches {

    private Transform _transitionCamera;

	private bool _move;
	private Transform _from;
	private float _fromFOV;
	private float _fromFarClipping;
	private float _fromNearClipping;
	private Transform _to;
	private float _toFOV;
	private float _toFarClipping;
	private float _toNearClipping;
	private float _timeSinceTransitionStarted;
	private float _t;

	private Vector3 _fromPos;


	private void Awake() {
		_move = false;
		if(transform.parent.GetComponent<Transition_Fiches>().From != null){
			_from = transform.parent.GetComponent<Transition_Fiches>().From.ModuleCameraHolder.transform;
		}else if(transform.parent.GetComponent<Transition_Fiches>().From2 != null){
			_from = transform.parent.GetComponent<Transition_Fiches>().From2.ModuleCameraHolder.transform;
		}else if(transform.parent.GetComponent<Toolz.Transitions.Transition>() != null){
			_from = transform.parent.GetComponent<Toolz.Transitions.Transition>().From.ModuleCameraHolder.transform;
		}
		if(transform.parent.GetComponent<Transition_Fiches>().To != null){
			_to = transform.parent.GetComponent<Transition_Fiches>().To.ModuleCameraHolder.transform;
		}else if(transform.parent.GetComponent<Transition_Fiches>().To2 != null){
			_to = transform.parent.GetComponent<Transition_Fiches>().To2.ModuleCameraHolder.transform;
		}else if(transform.parent.GetComponent<Toolz.Transitions.Transition>() != null){
			_to = transform.parent.GetComponent<Toolz.Transitions.Transition>().To.ModuleCameraHolder.transform;
		}

		_timeSinceTransitionStarted = 0f;
	}

	private void Start() {

	}

	private void Update() {
		if (_move){
			LerpCamera();
		}
	}


	// PUBLIC METHODS

	public override void ActivateTransition() {
        _timeSinceTransitionStarted = 0f;
        if (_transitionCamera == null) {
            _transitionCamera = GetComponentInChildren<Camera>().transform;
        }
		_fromPos = _from.position;
		//set camera start position
		_transitionCamera.position = _fromPos;
		_transitionCamera.rotation = _from.rotation;

		_fromFOV = _from.Find ("Camera").GetComponent<Camera> ().fieldOfView;
		_toFOV = _to.Find ("Camera").GetComponent<Camera> ().fieldOfView;

		_fromFarClipping = _from.Find ("Camera").GetComponent<Camera> ().farClipPlane;
		_toFarClipping = _to.Find ("Camera").GetComponent<Camera> ().farClipPlane;
		_transitionCamera.GetComponent<Camera> ().farClipPlane = _fromFarClipping;

		_fromNearClipping = _from.Find ("Camera").GetComponent<Camera> ().nearClipPlane;
		_toNearClipping = _to.Find ("Camera").GetComponent<Camera> ().nearClipPlane;
		_transitionCamera.GetComponent<Camera> ().nearClipPlane = _fromNearClipping;

		Invoke("StartTransition", StartingTime);
	}
	
	// PRIVATE METHODS

	private void LerpCamera(){
        /*
         * 0 => TransitionDuration / 0 => 1
         * _ratio = currentTime since start/ TransitionDuration
         * */
        _timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / TransitionDuration;
		_transitionCamera.position = Vector3.Lerp(_fromPos, _to.position, _t);
		_transitionCamera.rotation = Quaternion.Lerp(_from.rotation, _to.rotation, _t);
		_transitionCamera.GetComponent<Camera> ().fieldOfView = Mathf.Lerp(_fromFOV, _toFOV, _t);
		_transitionCamera.GetComponent<Camera> ().farClipPlane = Mathf.Lerp(_fromFarClipping, _toFarClipping, _t);
		_transitionCamera.GetComponent<Camera> ().nearClipPlane = Mathf.Lerp(_fromNearClipping, _toNearClipping, _t);
		if(Vector3.Distance(_transitionCamera.position, _to.position) < 0.01f){
			_move = false;
		}
	}

	private void StartTransition(){
		_move = true;
	}

}

