﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Toolz.Transitions {
	[AddComponentMenu("Toolz/Transitions/UITransitionFadeInOut_Fiches")]

	public class UITransitionFadeInOut_Fiches : TransitionBrick_Fiches {

        [Tooltip("Color of the alphaed screen")]
        public Color FadeColor = Color.black;

        public static readonly string PANEL_NAME = "Panel";

        private Image _toFade;

		private void Awake() {
            _toFade = transform.Find(PANEL_NAME).GetComponent<Image>();
            _toFade.CrossFadeAlpha(0f, 0f, false);
        }
		
		private void Start() {

        }

        // PUBLIC METHODS
        public override void ActivateTransition() {
            Invoke("StartTransition", StartingTime);
        }

        // PRIVATE METHODS

        //Called after starting time has been reached
        private void StartTransition() {
            _toFade.CrossFadeAlpha(1f, TransitionDuration/2f, false);
           Invoke("StartFadeOut", (TransitionDuration/2f)+0.5f);
        }

        private void StartFadeOut() {
            _toFade.CrossFadeAlpha(0f, TransitionDuration/2f, false);
        }
    }
}