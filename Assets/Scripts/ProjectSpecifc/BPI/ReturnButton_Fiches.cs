﻿using UnityEngine;
using System.Collections;

public class ReturnButton_Fiches : MonoBehaviour {

	public GameObject ProjectSummary;
	public ProjectUIProjectPart_Fiches ProjectPart;
	public GameObject CloseButton;
	public GameObject TitleProject;
	public GameObject TitleSubsection;
	public MoveEmptyCamera moveEmptyCamera;
	public GameObject NacelleHS;
	public GameObject RaccordHS;
	public GameObject NacellePanel;
	public GameObject RaccordPanel;
	public GameObject Scroller;
	public GameObject ModelRaccordDomestique;
	public GameObject ModelRaccordPublic;

	public MoveEmptyCameraVoiture moveEmptyCameraV;
	public GameObject MoteurHS;
	public GameObject BatterieHS;
	public GameObject MoteurPanel;
	public GameObject BatteriePanel;
	public Transform BatterieTop;

	public MoveEmptyCameraPhoto moveEmptyCameraP;
	public GameObject PanneauHS;
	public GameObject OndulateurHS;
	public GameObject PanneauPanel;
	public GameObject OndulateurPanel;

	public MoveEmptyCameraThermique moveEmptyCameraTH;
	public GameObject PanneauTHHS;
	public GameObject BallonEauChaudeHS;
	public GameObject PanneauTHPanel;
	public GameObject BallonEauChaudePanel;

	public MoveEmptyCameraChaudiere moveEmptyCameraChaudiere;
	public GameObject FoyerHS;
	public GameObject FoyerPanel;

	public MoveEmptyCameraBiodigesteur moveEmptyCameraBiodig;
	public GameObject DigesteurTHHS;
	public GameObject MoteurDigesteurHS;
	public GameObject DigesteurPanel;
	public GameObject MoteurDigesteurPanel;

	public GameObject WaterfallGraph;

	private Vector3 TempVect3;
	public static string SubSubsectionName = "";

	private void Start () {
	
	}

	public void ReturnFromSubsection(){
		ProjectSummary.SetActive (true);
		ProjectPart.SetAllInactive ();
		CloseButton.SetActive (true);
		TitleProject.SetActive (true);
		TitleSubsection.SetActive (false);
		gameObject.SetActive (false);
		if (WaterfallGraph.activeInHierarchy) {
			if (WaterfallGraph.GetComponent<WaterfallUIManager_Fiches> () != null) {
				WaterfallGraph.GetComponent<WaterfallUIManager_Fiches> ().ClosePanel ();
			}else if (WaterfallGraph.GetComponent<WaterfallUIManager> () != null) {
				WaterfallGraph.GetComponent<WaterfallUIManager> ().ClosePanel ();
			}
		}
	}

	public void ReturnFromFontionnementPanneaux(){
		PanneauHS.GetComponent<PanneauHotspot> ().CloseMoteur ();
		Invoke ("FinishReturn", 1f);
	}

	private void FinishReturn () {
		PanneauPanel.SetActive (false);
		Scroller.SetActive (true);
		PanneauHS.SetActive (true);
		OndulateurHS.SetActive (true);
	}

	public void ReturnFromFontionnementOndulateur(){
		//move camera back
		moveEmptyCameraP.FromOndulateurToStart ();
		//close extra ui
		OndulateurPanel.SetActive (false);
		Scroller.SetActive (true);
		PanneauHS.SetActive (true);
		OndulateurHS.SetActive (true);
	}

	public void ReturnFromFontionnementMoteur(){
		//move camera back
		moveEmptyCameraV.FromMoteurToStart ();
		//close extra ui
		MoteurPanel.SetActive (false);
		Scroller.SetActive (true);
		MoteurHS.SetActive (true);
		BatterieHS.SetActive (true);
	}

	public void ReturnFromFontionnementBatterie(){
		TempVect3 = BatterieTop.localPosition;
		TempVect3.y -= 1f;
		BatterieTop.localPosition = TempVect3;
		//move camera back
		moveEmptyCameraV.FromBatterieToStart ();
		//close extra ui
		BatteriePanel.SetActive (false);
		Scroller.SetActive (true);
		MoteurHS.SetActive (true);
		BatterieHS.SetActive (true);
	}

	public void ReturnFromFontionnementNacelle(){
		//hide labels and show nacelle
		NacelleHS.GetComponent<NacelleHotspot> ().CloseEolienneMotor ();
		//move camera back
		moveEmptyCamera.FromNacelleToStart ();
		//close extra ui
		NacellePanel.SetActive (false);
		Scroller.SetActive (true);
		NacelleHS.SetActive (true);
		RaccordHS.SetActive (true);
	}

	public void ReturnFromFontionnementRaccord(){
		//hide models
		if (ModelRaccordDomestique.activeInHierarchy) {
			ModelRaccordDomestique.SetActive(false);
		}
		if (ModelRaccordPublic.activeInHierarchy) {
			ModelRaccordPublic.SetActive(false);
		}

		//move camera back
		moveEmptyCamera.FromRaccordToStart ();
		//close extra ui
		RaccordPanel.SetActive (false);
		Scroller.SetActive (true);
		NacelleHS.SetActive (true);
		RaccordHS.SetActive (true);
	}

	public void ReturnFromFontionnementPanneauxTH(){
		//move camera back
		PanneauTHHS.GetComponent<PanneauTHHotspot> ().CloseMoteur ();
		//close extra ui
		PanneauTHPanel.SetActive (false);
		Scroller.SetActive (true);
		PanneauTHHS.SetActive (true);
		BallonEauChaudeHS.SetActive (true);
	}

	public void ReturnFromFontionnementBallonEauChaude(){
		//move camera back
		BallonEauChaudeHS.GetComponent<BallonHotspot> ().CloseMoteur ();
		//close extra ui
		BallonEauChaudePanel.SetActive (false);
		Scroller.SetActive (true);
		PanneauTHHS.SetActive (true);
		BallonEauChaudeHS.SetActive (true);
	}

	public void ReturnFromFontionnementFoyer(){
		moveEmptyCameraChaudiere.FromFoyerToStart ();

		FoyerPanel.SetActive (false);
		Scroller.SetActive (true);
		FoyerHS.SetActive (true);

	}

	public void ReturnFromFontionnementDigesteur(){
		//move camera back
		moveEmptyCameraBiodig.FromDigesteurToStart ();
		//close extra ui
		DigesteurPanel.SetActive (false);
		Scroller.SetActive (true);
		MoteurDigesteurHS.SetActive (true);
		DigesteurTHHS.SetActive (true);
	}

	public void ReturnFromFontionnementMoteurDigesteur(){
		//move camera back
		moveEmptyCameraBiodig.FromMoteurDigesteurToStart ();
		//close extra ui
		MoteurDigesteurPanel.SetActive (false);
		Scroller.SetActive (true);
		MoteurDigesteurHS.SetActive (true);
		DigesteurTHHS.SetActive (true);
	}

	public void ChooseWhatToDo(){
		//if in subsection
		if (SubSubsectionName == "") {
			ReturnFromSubsection ();
		} else {
			if (SubSubsectionName == "FontionnementNacelle") {
				ReturnFromFontionnementNacelle ();
			}else if (SubSubsectionName == "FontionnementRaccord") {
				ReturnFromFontionnementRaccord ();
			}else if (SubSubsectionName == "FontionnementMoteur") {
				ReturnFromFontionnementMoteur ();
			}else if (SubSubsectionName == "FontionnementBatterie") {
				ReturnFromFontionnementBatterie ();
			}else if (SubSubsectionName == "FontionnementPanneaux") {
				ReturnFromFontionnementPanneaux ();
			}else if (SubSubsectionName == "FontionnementOndulateur") {
				ReturnFromFontionnementOndulateur ();
			}else if (SubSubsectionName == "FontionnementPanneauxTH") {
				ReturnFromFontionnementPanneauxTH ();
			}else if (SubSubsectionName == "FontionnementBallonEauChaude") {
				ReturnFromFontionnementBallonEauChaude ();
			}else if (SubSubsectionName == "FontionnementFoyer") {
				ReturnFromFontionnementFoyer ();
			}else if (SubSubsectionName == "FontionnementDigesteur") {
				ReturnFromFontionnementDigesteur ();
			}else if (SubSubsectionName == "FontionnementMoteurDigesteur") {
				ReturnFromFontionnementMoteurDigesteur ();
			}else{
				Debug.Log("SubSubsectionName not recognised");
			}
		}
	}

}
