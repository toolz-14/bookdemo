﻿using UnityEngine;
using System.Collections;

public class Module_Fiches : MonoBehaviour {

    public bool IsModuleInitial;

    //needed to have this camera accessible in other components's awake
    public Transform ModuleCameraHolder { get; private set; }

    public UnityEngine.Events.UnityAction OnModuleActivated;
    public UnityEngine.Events.UnityAction OnModuleDeactivated;

    public static readonly string ALWAYS_ACTIVE = "AlwaysActive";
    public static readonly string ACTIVE_WHEN_MODULE_IS_ACTIVE = "ActiveWhenModuleIsActive";
    public static readonly string TO_BE_ACTIVATED = "ToBeActivated";

    private void Awake() {
        //Ensure that children are activated/deactivated correctly
        transform.Find(ALWAYS_ACTIVE).gameObject.SetActive(true);

        //Do stuff in children before deactivating them
        transform.Find(ACTIVE_WHEN_MODULE_IS_ACTIVE).gameObject.SetActive(true);
        transform.Find(TO_BE_ACTIVATED).gameObject.SetActive(true);

        //Get the CameraHolder
		if (transform.Find ("ActiveWhenModuleIsActive") != null) {
			if (transform.Find ("ActiveWhenModuleIsActive").GetComponentInChildren<Camera> () != null) {
				if (transform.Find ("ActiveWhenModuleIsActive").GetComponentInChildren<Camera> ().transform.parent != null) {
					ModuleCameraHolder = transform.Find ("ActiveWhenModuleIsActive").GetComponentInChildren<Camera> ().transform.parent;
				}
			}else{
				//still unsure why i had to do this for this sncf module .. (otherwise would not find cam holder and mess up the whole app)
				if(transform.name == "Module_LotB"){
					if(transform.Find ("ActiveWhenModuleIsActive").Find("ViewOrbitalCamera").Find("ConstraintBox").Find("CameraHolder")){
						Debug.Log("Module_LotB, found cam holder");
						ModuleCameraHolder = transform.Find ("ActiveWhenModuleIsActive").Find("ViewOrbitalCamera").Find("ConstraintBox").Find("CameraHolder");
					}
				}
			}
		}

        if (!IsModuleInitial) {
            transform.Find(ACTIVE_WHEN_MODULE_IS_ACTIVE).gameObject.SetActive(false);
        }else{
			//needed since module initial doesn't call Activate()
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ModuleCameraHolder.Find("Camera").GetComponent<Camera>();
		}
        transform.Find(TO_BE_ACTIVATED).gameObject.SetActive(false);

    }

    private void Start() {

    }

    public void Activate() {
        transform.Find(ACTIVE_WHEN_MODULE_IS_ACTIVE).gameObject.SetActive(true);
		Toolz.Managers.LevelManagerFiches.ActiveCamera = ModuleCameraHolder.Find("Camera").GetComponent<Camera>();
        if (OnModuleActivated != null) {
            OnModuleActivated();
        }
    }

    public void Deactivate() {
        transform.Find(ACTIVE_WHEN_MODULE_IS_ACTIVE).gameObject.SetActive(false);
        if (OnModuleDeactivated != null) {
            OnModuleDeactivated();
        }
    }
}
