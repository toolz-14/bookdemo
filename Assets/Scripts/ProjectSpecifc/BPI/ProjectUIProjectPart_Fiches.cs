﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProjectUIProjectPart_Fiches : MonoBehaviour {
	
	public GameObject Context;
	public GameObject Fonctionnement;
	public GameObject Realisation;
	public GameObject CoutBenefice;

	public RectTransform ContextImage;

	private Vector3 _savedCameraPos;
	private Quaternion _savedCameraRot;
	private string _myText;
	private Color tempColor;

	private void Start () {

	}

	public void SetAllInactive(){
		if(Context.activeInHierarchy){
			if(ContextImage.GetComponent<ImageContext_Fiches> ().isOpen){
				ContextImage.GetComponent<ImageContext_Fiches> ().StartCloseImage();
			}
			ContextImage.gameObject.SetActive (false);
			Context.SetActive(false);
		}
		if(Fonctionnement.activeInHierarchy){

			if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "EoliennePark"){
				SwapCameraEolienneOrbitToEmpty(false);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "VoitureElectrique"){
				SwapCameraVoitureOrbitToEmpty(false);
				tempColor = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.VoitureMatToAlpha.color;
				tempColor.a = 1f;
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.VoitureMatToAlpha.color = tempColor;
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.VoitureSeats.SetActive(true);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "PhotovoltaiqueBPI"){
				SwapCameraPhotoOrbitToEmpty(false, true);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(true);
//				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(false);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "ThermiqueBPI"){
				SwapCameraThermiqueOrbitToEmpty(false, true);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(true);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(false);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.SwitchButton.SetActive(false);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "ChaudiereBiomasseBPI"){
				
			}else if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "BiodigesteurBPI"){
				
			}
			Fonctionnement.SetActive(false);
		}
		if(CoutBenefice.activeInHierarchy){

			if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "EoliennePark"){
				CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(true);
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.StartCloseSlideshow();
			}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "VoitureElectrique"){
				CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("TextHolder").gameObject.SetActive(false);
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.StartCloseSlideshow();
			}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "PhotovoltaiqueBPI"){
				CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("TextHolder").gameObject.SetActive(false);
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.StartCloseSlideshow();
			}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "ThermiqueBPI"){
				CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("TextHolder").gameObject.SetActive(false);
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.StartCloseSlideshow();
			}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "ChaudiereBiomasseBPI"){
				CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("TextHolder").gameObject.SetActive(false);
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.StartCloseSlideshow();
			}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "BiodigesteurBPI"){
				CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(true);
				CoutBenefice.transform.Find ("TextHolder").gameObject.SetActive(false);
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.StartCloseSlideshow();
			}
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.SetActive (false);
			CoutBenefice.SetActive(false);
		}
		if(Realisation.activeInHierarchy){

			if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "EoliennePark"){
				ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(false);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "VoitureElectrique"){
				ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(false);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "PhotovoltaiqueBPI"){
				ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(false);

				if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.activeInHierarchy){
					SwapCameraPhotoOrbitToEmpty(false, false);
					ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(true);
//					ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(false);
				}else{
					SwapCameraPhotoFreeToOrbit();
				}
			}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "ThermiqueBPI"){
				SwapCameraThermiqueOrbitToFree(false);
				ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(false);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "ChaudiereBiomasseBPI"){
				ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(false);
			}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "BiodigesteurBPI"){
				ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(false);
			}
			Realisation.SetActive(false);
		}
	}

	public void ActivateContext(){
		SetAllInactive ();
		SetContextData ();
		Context.SetActive(true);
	}

	private void SetContextData(){

		//set text
		_myText = ProjectUIManager_Fiches.CurrentProjectData.contextData.TextContext;
		_myText = _myText.Replace("NEWLINE", "\n");
		Context.transform.Find ("Scroller").Find ("Container").Find ("Text").GetComponent<Text> ().text = _myText;
		//if null there is no image
		if (ProjectUIManager_Fiches.CurrentProjectData.contextData.ImageContext != null) {
			//set image
			ContextImage.gameObject.SetActive (true);
			ContextImage.Find ("Image_Fiche").GetComponent<Image> ().sprite = ProjectUIManager_Fiches.CurrentProjectData.contextData.ImageContext;
			ContextImage.GetComponent<ImageContext_Fiches> ().StartOpenImage();
		}
	}

	public void ActivateFonctionnement(){
		SetAllInactive ();
		SetFonctionnementData ();
		Fonctionnement.SetActive(true);
	}

	private void SetFonctionnementData(){
		
		//set text
		_myText = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.TextFonctionnement;
		_myText = _myText.Replace("NEWLINE", "\n");
		Fonctionnement.transform.Find ("Scroller").Find ("Container").Find ("Text").GetComponent<Text> ().text = _myText;

		if (ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "EoliennePark") {
			//swap camera
			SwapCameraEolienneOrbitToEmpty(true);
		}else if (ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "VoitureElectrique") {
			//swap camera
			SwapCameraVoitureOrbitToEmpty(true);
			tempColor = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.VoitureMatToAlpha.color;
			tempColor.a = 0.3f;
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.VoitureMatToAlpha.color = tempColor;
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.VoitureSeats.SetActive(false);
		}else if (ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "PhotovoltaiqueBPI") {
			//swap camera
			SwapCameraPhotoOrbitToEmpty(true, true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(false);
//			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(true);
		}else if (ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "ThermiqueBPI") {
			SwapCameraThermiqueOrbitToEmpty(true, true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.SwitchButton.SetActive(true);
		}else if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "ChaudiereBiomasseBPI"){
			
		}else if(ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.projectID == "BiodigesteurBPI"){
			
		}
	}

	public void SwapCameraEolienneOrbitToEmpty(bool isGoingIn){
		//Debug.Log("SwapCameraEolienne isGoingIn = "+isGoingIn);
		if (isGoingIn) {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Nacelle_Domestic").gameObject.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Racordement_Domestic").gameObject.SetActive (true);
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		} else {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Nacelle_Domestic").gameObject.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Racordement_Domestic").gameObject.SetActive (false);
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		}
	}

	public void SwapCameraVoitureOrbitToEmpty(bool isGoingIn){
		//Debug.Log("SwapCameraVoiture isGoingIn = "+isGoingIn);
		if (isGoingIn) {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Moteur").gameObject.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Batterie").gameObject.SetActive (true);
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		} else {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Moteur").gameObject.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Batterie").gameObject.SetActive (false);
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		}
	}

	public void SwapCameraPhotoOrbitToEmpty(bool isGoingIn, bool showHS){
		if (isGoingIn) {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (true);
			if(showHS){
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Panneaux").gameObject.SetActive (true);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Onduleur").gameObject.SetActive (true);
			}
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		} else {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (false);
			if(showHS){
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Panneaux").gameObject.SetActive (false);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_Onduleur").gameObject.SetActive (false);
			}
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		}
	}

	public void SwapCameraPhotoFreeToOrbit(){
		ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (true);
		ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraFree.SetActive (false);
	}

	public void SwapCameraThermiqueOrbitToEmpty(bool isGoingIn, bool showHS){
		if (isGoingIn) {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (true);
			if(showHS){
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_PanneauxTH").gameObject.SetActive (true);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_BallonEauChaude").gameObject.SetActive (true);
			}
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		} else {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.SetActive (false);
			if(showHS){
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_PanneauxTH").gameObject.SetActive (false);
				ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.CanvasFonctionnement.Find ("Hotspot_BallonEauChaude").gameObject.SetActive (false);
			}
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		}
	}

	public void ActivateCoutBenefice(){
		SetAllInactive ();

		CoutBenefice.SetActive(true);
		SetCoutBeneficeData ();
	}
	
	private void SetCoutBeneficeData(){

		//set text
		_myText = ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.TextCoutbenef;
		_myText = _myText.Replace("NEWLINE", "\n");
		CoutBenefice.transform.Find ("Scroller").Find ("Container").Find ("Text").GetComponent<Text> ().text = _myText;

		if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "EoliennePark"){
			if(!ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.activeInHierarchy){
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.SetActive(true);
			}
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.GetComponent<SlideShow_Fiches>().ActivateStatsText(true);
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.LoadSprites(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShowID);
			CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(false);
		}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "VoitureElectrique"){
			if(!ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.activeInHierarchy){
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.SetActive(true);
			}
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.GetComponent<SlideShow_Fiches>().ActivateStatsText(false);
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.LoadSprites(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShowID);
			CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("TextHolder").gameObject.SetActive(true);
		}
		else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "PhotovoltaiqueBPI"){
			if(!ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.activeInHierarchy){
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.SetActive(true);
			}
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.GetComponent<SlideShow_Fiches>().ActivateStatsText(false);
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.LoadSprites(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShowID);
			CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(false);
		}
		else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "ThermiqueBPI"){
			if(!ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.activeInHierarchy){
				ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.gameObject.SetActive(true);
			}
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.GetComponent<SlideShow_Fiches>().ActivateStatsText(false);
			ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShow.LoadSprites(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.SlideShowID);
			CoutBenefice.transform.Find ("Text_1").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("Text_1_Dynamic").gameObject.SetActive(false);
			CoutBenefice.transform.Find ("Scroller").gameObject.SetActive(false);

		}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "ChaudiereBiomasseBPI"){
			
		}else if(ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "BiodigesteurBPI"){
			
		}
	}

	public void ActivateRealisation(){
		SetAllInactive ();
		SetRealisationData ();
		Realisation.SetActive (true);
	}

	private void SetRealisationData(){

		if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "EoliennePark"){
			ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(true);
		}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "VoitureElectrique"){
			ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(true);
		}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "PhotovoltaiqueBPI"){
			ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(true);
			SwapCameraPhotoOrbitToEmpty(true, false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(false);
//			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(true);
		}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "ThermiqueBPI"){
			ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(true);
			SwapCameraThermiqueOrbitToFree(true);
		}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "ChaudiereBiomasseBPI"){
			ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(true);
		}else if(ProjectUIManager_Fiches.CurrentProjectData.realisationData.projectID == "BiodigesteurBPI"){
			ProjectUIManager_Fiches.CurrentProjectData.realisationData.ObjectToActivate.SetActive(true);
		}
	}

	public void SwapCameraThermiqueOrbitToFree(bool isGoingIn){
		if (isGoingIn) {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraFree.SetActive (true);
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraEmpty.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		} else {
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.SetActive (true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraFree.SetActive (false);
			Toolz.Managers.LevelManagerFiches.ActiveCamera = ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.ProjectCameraOrbitale.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
		}
	}
}
