﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SliderCollectif : MonoBehaviour {

	public Text Investissement;
	public Text NrjProduite;
	public Text Maintenance;
	public Text CO2Produit;
	public Text NrjProduiteFoyers;
	public Text SurfaceSliderValue;
	public Text EnsoleillementSliderValue;
	public Slider theEnsoleillementSlider;
	public Slider theSurfaceSlider;
	public List<GameObject> OtherPanneauList = new List<GameObject> ();
	public float CoutInstalPourUneSurfaceDe1 = 4000f; //euro
	public float CoutMaintenanceUneSurfaceDe1 = 2000f; //euro
	public float NrjProduiteUneSurfaceDe1PourSoleilDe1 = 6f; //kWh
	public float EconomiePourUneSurfaceDe1PourSoleilDe1 = 30; //euro
	public float PrixPourRevente1kWh = 30; //euro
	
	private float Nrj = 0f;
	private float Mainten = 0f;
	
	private void Start () {
		
	}
	
	public void CalculateValues(){
		SurfaceSliderValue.text = theSurfaceSlider.value.ToString();
		EnsoleillementSliderValue.text = theEnsoleillementSlider.value.ToString();
		Nrj = (NrjProduiteUneSurfaceDe1PourSoleilDe1 * theEnsoleillementSlider.value) * theSurfaceSlider.value;
		Mainten = CoutMaintenanceUneSurfaceDe1 * theSurfaceSlider.value;
		Investissement.text = (CoutInstalPourUneSurfaceDe1 * theSurfaceSlider.value).ToString();
		NrjProduite.text = (Nrj).ToString();
		Maintenance.text = (Mainten).ToString();
		CO2Produit.text = ((Nrj * PrixPourRevente1kWh) - Mainten).ToString();
		NrjProduiteFoyers.text = (Nrj / 4.679f).ToString ();
	}

	public void ShowHideOtherPanneau(){
		//slider 1 to 12
		if (OtherPanneauList.Count > 0) {
			for (int i=0; i<OtherPanneauList.Count; i++) {
				if (i <= (int)((theSurfaceSlider.value - 1f))) {
					if(!OtherPanneauList [i].activeInHierarchy){
						OtherPanneauList [i].SetActive (true);
					}
				} else {
					if(OtherPanneauList [i].activeInHierarchy){
						OtherPanneauList [i].SetActive (false);
					}
				}
			}
		}
	}
	
	public void ForceHideOtherPanneau(){
		if (OtherPanneauList.Count > 0) {
			for (int i=0; i<OtherPanneauList.Count; i++) {
				OtherPanneauList [i].SetActive (false);
			}
		}
	}
}
