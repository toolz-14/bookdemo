﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DropDownSelected_Fiches : MonoBehaviour {

	public Sprite DefaultFlag;
	public string DefautText;
	//	public DropDown DropDown;
//	public DropDown1Entry DropDown1Entry;
	public Lean.LeanLocalization LeanLocalization;
	
	private void Start () {
		
	}
	
	private void Awake () {
		
	}
	
	public void SetDefaultLanguage(){
		
		transform.Find ("Text_LanguageName").GetComponent<Text> ().text = DefautText;
		transform.Find ("Image_LanguageFlag").GetComponent<Image> ().sprite = DefaultFlag;
		LeanLocalization.SetLanguage ("English");
	}
	
	public string GetCurrentLanguage(){
		return LeanLocalization.CurrentLanguage;
	}
	
	public void SetText(GameObject textObject){
		transform.Find ("Text_LanguageName").GetComponent<Text> ().text = textObject.GetComponent<Text> ().text;
	}
	
	public void SetFlag(GameObject spriteObject){
		transform.Find ("Image_LanguageFlag").GetComponent<Image> ().sprite = spriteObject.GetComponent<Image> ().sprite;
		
//		DropDown1Entry.AnimateButtonClose ();
//		DropDown1Entry.buttonIsBlocked = false;
//		DropDown1Entry.isOpen = false;
	}
}
