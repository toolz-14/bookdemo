﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class SlideShow_Fiches : MonoBehaviour, ISwipeGesture {

	protected Gestures _gestures;
	protected SwipeGesture _swipeGesture;
	
	private Sprite[] _slides;
	public static bool LockGesture;
	public SlideshowStats SlideshowStats;
	public SetCoutBenefTextHolder SetCoutBenefTextHolder;
	public Slide_Fiches Slide1;
	public Slide_Fiches Slide2;
	public Slide_Fiches Slide3;

	private Image _image1;
	private Image _image2;
	private Image _image3;
	[HideInInspector]
	public int _shownImageIndex = 0;
	
	private bool _nextIsDone;

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.3f;
	
	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.3f;

	public static bool LockButtons = false;


	private void Awake(){
		LockGesture = false;

		GetComponent<RectTransform> ().localScale = Vector2.zero;

		_gestures = Gestures.instance;
	}
	
	private void Start(){

	}

	private void Update () {
		if(_launch){
			OpenSlideshow();
		}
		if(_launch2){
			CloseSlideshow();
		}
	}
	
	public void SwipeDrag(SwipeGesture g, Vector2 drag, float scope){
	}
	
	public void SwipeReset(SwipeGesture g, Vector2 axis){
	}
	
	public void SwipeValidate(SwipeGesture g, Vector2 direction, float tweenDuration){
		if (!LockGesture && transform.localScale != Vector3.zero) {
			if (direction.x == -1) {
				LockGesture = true;
				Next ();
			}
			if (direction.x == 1) {
				LockGesture = true;
				Previous ();
			}
		}
	}

	private void OnEnable() {
		_swipeGesture = new SwipeGesture(this, "world");
	}
	
	private void OnDisable() { 
		if (_swipeGesture == null) return;
		_swipeGesture.Dispose();
		_swipeGesture = null;
	}
	
	public void LoadSprites(string id){
		Resources.UnloadUnusedAssets ();
		_slides = Resources.LoadAll("Slideshow_Fiches/"+id, typeof(Sprite)).Cast<Sprite>().ToArray();
		StartOpenSlideshow ();
	}
	
	private void InitSlide(){
		_image1 = Slide1.image;
		_image2 = Slide2.image;
		_image3 = Slide3.image;
		_image1.sprite = GetNextImage(0);
		_image2.sprite = GetNextImage(+1);
		_image3.sprite = GetNextImage(-1);
		SetCoutTexts ();
	}

	/**
         * @param direction: >0 == we want the next image to the right. <0 == we want the next image to the left
         */
	public Sprite GetNextImage(int direction = 0){
		int indexImage = _shownImageIndex;
		indexImage += direction;
//		Debug.Log ("GetNextImage(), _slides.Length = " + _slides.Length);
		if (indexImage >= _slides.Length) {
			indexImage = 0;
		}
		else if (indexImage < 0) {
			indexImage = _slides.Length - 1;
		}
		if (indexImage >= 0 && indexImage <= _slides.Length) {
			Sprite nextImage = _slides [indexImage];
			return nextImage;
		} else {
			//--- if no image in resources folder?
			return null;
		}
	}

	private void SetCoutTexts(){
		if (ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "EoliennePark") {
			SlideshowStats.SetTexts (_shownImageIndex);
		}else if (ProjectUIManager_Fiches.CurrentProjectData.coutbenefData.projectID == "VoitureElectrique") {
			SetCoutBenefTextHolder.UpdateVoitureStats(_shownImageIndex);
		}
	}
	
	public void Next(){
		if (!LockButtons) {
			LockButtons = true;
			++_shownImageIndex;
			if (_shownImageIndex >= _slides.Length) {
				_shownImageIndex = 0;
			}
//		Debug.Log ("Next, _shownImageIndex = "+_shownImageIndex);
			Slide1.InitLerp (false);
			Slide2.InitLerp (false);
			Slide3.InitLerp (false);
			SetCoutTexts();
		}
	}
	
	public void Previous(){
		if (!LockButtons) {
			LockButtons = true;
			--_shownImageIndex;
			if (_shownImageIndex < 0) {
				_shownImageIndex = _slides.Length - 1;
			}
//		Debug.Log ("Previous, _shownImageIndex = "+_shownImageIndex);
			Slide1.InitLerp (true);
			Slide2.InitLerp (true);
			Slide3.InitLerp (true);
			SetCoutTexts();
		}
	}

	private void StartOpenSlideshow(){

		InitSlide();
		HideSlides (true);

		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().localScale;
		_to1 = new Vector3 (1f, 1f, 1f);
		_launch = true;
	}
	
	private void OpenSlideshow(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		transform.localScale = Vector3.Lerp(_from1, _to1, _t);
		if(Vector3.Distance(transform.localScale, _to1) < 0.01f){
			_launch = false;
			HideSlides (false);
		}
	}
	
	public void StartCloseSlideshow(){

		HideSlides (true);

		_timeSinceTransitionStarted2 = 0f;
		_from2 = transform.localScale;
		_to2 = Vector3.zero;
		_launch2 = true;
	}
	
	private void CloseSlideshow(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		transform.localScale = Vector3.Lerp(_from2, _to2, _t2);
		if(Vector3.Distance(transform.localScale, _to2) < 0.01f){
			_launch2 = false;
			transform.localScale = Vector2.zero;
			gameObject.SetActive(false);
		}
	}

	//hide the left and right slides during onpen/close lerp [looks better]
	private void HideSlides(bool hide){
		if (hide) {
			if(Slide1.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide1.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide1.gameObject.SetActive(false);
			}
			if(Slide2.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide2.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide2.gameObject.SetActive(false);
			}
			if(Slide3.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide3.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide3.gameObject.SetActive(false);
			}
		} else {
			if(Slide1.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide1.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide1.gameObject.SetActive(true);
			}
			if(Slide2.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide2.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide2.gameObject.SetActive(true);
			}
			if(Slide3.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide3.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide3.gameObject.SetActive(true);
			}
		}
	}

	public void ActivateStatsText(bool active){
		transform.Find("Image_Stats").gameObject.SetActive(active);
	}
}
