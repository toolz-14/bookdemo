﻿using UnityEngine;
using System.Collections;

public class PanneauHotspot : MonoBehaviour {

	public GameObject Labels;
	public Animation AnimatedObject;
	public MoveEmptyCameraPhoto moveEmptyCamera;
	
	private void Start () {
		
	}

	public void SelectAndShowMoteur(){
		moveEmptyCamera.FromStartToPanneaux ();
		Invoke ("DoAnimation", 0.4f);
	}

	private void DoAnimation(){
		AnimatedObject["Photovolt"].speed = 1;
		AnimatedObject["Photovolt"].time = 0;
		AnimatedObject.Play("Photovolt");
		Invoke ("ShowLabels", 1.8f);
	}

	private void ShowLabels(){
		Labels.SetActive (true);
	}
	
	public void CloseMoteur(){
		AnimatedObject["Photovolt"].speed = -2;
		AnimatedObject["Photovolt"].time = AnimatedObject["Photovolt"].length;
		AnimatedObject.Play("Photovolt");
		Invoke ("Moteur", 1f);
	}

	private void Moteur(){
		Labels.SetActive (false);
		moveEmptyCamera.FromPanneauxToStart ();
	}
}
