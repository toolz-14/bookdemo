﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SwapCars : MonoBehaviour {

	public SliderVoitureKm SliderVoitureKm;
	public GameObject[] CarArray = new GameObject[4];
	//0 = citadine, 1 = berline, 2 = familliale, 3 = sport
	public static int carIndex = 0;
	[HideInInspector]
	public Text carText;
	public Text Elect100km;
	public Text Essence100km;


	private void Awake () {

	}

	private void Start () {
		//citadine
		carIndex = 0;
		SetVisibleCar ();
		carText = transform.Find ("Text_Dynamic").GetComponent<Text> ();
		SetTextandStats ();
		//SliderVoitureKm.CalculateValues ();
	}

	private void OnDisable () {
		//citadine
		carIndex = 0;
		SetVisibleCar ();
		SetTextandStats ();
		SliderVoitureKm.CalculateValues ();
	}

	public void Previous(){
		carIndex--;
		if(carIndex < 0){
			carIndex = 3;
		}
		SetVisibleCar ();
		SetTextandStats ();
		SliderVoitureKm.CalculateValues ();
	}

	public void Next(){
		carIndex++;
		if(carIndex > 3){
			carIndex = 0;
		}
		SetVisibleCar ();
		SetTextandStats ();
		SliderVoitureKm.CalculateValues ();
	}

	private void SetVisibleCar(){
		if (CarArray != null && CarArray.Length > 0) {
			for (int i=0; i<CarArray.Length; i++) {
				if (i == carIndex) {
					if(CarArray [i] != null){
						CarArray [i].SetActive (true);
					}
				} else {
					if(CarArray [i] != null){
						CarArray [i].SetActive (false);
					}
				}
			}
		}
	}

	private void SetTextandStats(){
		switch (carIndex) {
		case 0: carText.text = "Citadine";
			Elect100km.text = "1.1";
			Essence100km.text = "1.94";
			break;
		case 1: carText.text = "Berline";
			Elect100km.text = "1.63";
			Essence100km.text = "2.16";
			break;
		case 2: carText.text = "Familiale";
			Elect100km.text = "1.75";
			Essence100km.text = "2.34";
			break;
		case 3: carText.text = "Sport";
			Elect100km.text = "1.96";
			Essence100km.text = "2.83";
			break;
		default: carText.text = "";
			break;
		}
	}
}
