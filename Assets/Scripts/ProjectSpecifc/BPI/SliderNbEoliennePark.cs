﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SliderNbEoliennePark : MonoBehaviour {

	public Text Investissement;
	public Text NrjProduite;
	public Text NrjProduiteFoyers;
	public Text Maintenance;
	public Text CO2Produit;	
	public Text SurfaceSliderValue;
	public Text WindSliderValue;
	public Slider theWindSlider;
	public Slider theSurfaceSlider;
	public List<GameObject> EoliennesPark = new List<GameObject> ();
	//35ha == 1 eolienne

	public float CoutInstalPourUneEolienne = 30000; //euro
	public float CoutMaintenancePourUneEolienne = 2000; //euro
	public float NrjProduitePourUneEoliennePourVent1ms = 36; //kWh
	public float NrjNecessairePourUnFoyer = 1; //kWh
	public float PrixPourRevente1kWh = 500; //euro
	
	private float Nrj = 0f;
	private float Mainten = 0f;
	
	private void Start () {
		CalculateValues ();
	}
	
	public void CalculateValues(){
		SurfaceSliderValue.text = (theSurfaceSlider.value * 35).ToString ();
		WindSliderValue.text = (theWindSlider.value).ToString ();
		RotorSpeedIndustriel.speedmult = theWindSlider.value * 0.2f;
		Nrj = (NrjProduitePourUneEoliennePourVent1ms * theWindSlider.value) * theSurfaceSlider.value;
		Mainten = CoutMaintenancePourUneEolienne * theSurfaceSlider.value;
		Investissement.text = (CoutInstalPourUneEolienne * theSurfaceSlider.value).ToString();
		NrjProduite.text = (Nrj).ToString();
		Maintenance.text = (Mainten).ToString();
		CO2Produit.text = ((Nrj * PrixPourRevente1kWh) - Mainten).ToString();
		NrjProduiteFoyers.text = (Nrj / 4.679f).ToString ();
	}
	
	public void ShowHideEoliennes(){	
		for (int i=0; i<EoliennesPark.Count; i++) {
			if( i <= (int)(theSurfaceSlider.value - 1)){
//				Debug.Log("setting i = "+i+" to true");
				EoliennesPark [i].SetActive (true);
			}else{
//				Debug.Log("setting i = "+i+" to false");
				EoliennesPark [i].SetActive (false);
			}
		}
	}

	public void ForceHideEoliennes(){
		for (int i=0; i<EoliennesPark.Count; i++) {
			EoliennesPark [i].SetActive (false);
		}
	}
}
