﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderVoitureKm : MonoBehaviour {

	public Text SliderValue;
	public Text CurrentValue;
	public Text ElectCurrentSliderValue;
	public Text EssenceCurrentSliderValue;

	private float Elect_CostFor1km;
	private float Essence_CostFor1km;
	private Slider theSlider;

	private void Start () {
		theSlider = GetComponent<Slider> ();
	}
	
	public void CalculateValues(){
		switch (SwapCars.carIndex) {
		case 0: //Citadine
			Elect_CostFor1km = 0.011f;
			Essence_CostFor1km = 0.0194f;
			ElectCurrentSliderValue.text = (theSlider.value * Elect_CostFor1km).ToString();
			EssenceCurrentSliderValue.text = (theSlider.value * Essence_CostFor1km).ToString();
			break;
		case 1: //Berline
			Elect_CostFor1km = 0.0163f;
			Essence_CostFor1km = 0.0216f;
			ElectCurrentSliderValue.text = (theSlider.value * Elect_CostFor1km).ToString();
			EssenceCurrentSliderValue.text = (theSlider.value * Essence_CostFor1km).ToString();
			break;
		case 2: //Familiale
			Elect_CostFor1km = 0.0175f;
			Essence_CostFor1km = 0.0234f;
			ElectCurrentSliderValue.text = (theSlider.value * Elect_CostFor1km).ToString();
			EssenceCurrentSliderValue.text = (theSlider.value * Essence_CostFor1km).ToString();
			break;
		case 3: //Sport
			Elect_CostFor1km = 0.0196f;
			Essence_CostFor1km = 0.0283f;
			ElectCurrentSliderValue.text = (theSlider.value * Elect_CostFor1km).ToString();
			EssenceCurrentSliderValue.text = (theSlider.value * Essence_CostFor1km).ToString();
			break;
		default: ;
			break;
		}
		SliderValue.text = theSlider.value.ToString ();
		CurrentValue.text = theSlider.value.ToString ();
	}
}
