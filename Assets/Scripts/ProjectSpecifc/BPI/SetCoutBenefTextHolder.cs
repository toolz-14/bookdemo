﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetCoutBenefTextHolder : MonoBehaviour {

	public Text text_modele;
	public Text text_stat;
	private string _myText;

	private void Start () {
		UpdateVoitureStats (0);
	}

	public void UpdateVoitureStats (int index) {
		switch(index){
		case 0: text_modele.text = "Citadine";
			_myText = "Consommation électrique : 13,7 kWh/100 kmNEWLINEEquivalent l/100 km : 1,39 l/100 kmNEWLINEAutonomie : 175 kmNEWLINEBatterie : 24 kWhNEWLINEPrix du plein : 3,00 € (abonnement base), 3,25 € (heures pleines) ou 2,22 € (heures creuses)";
			_myText = _myText.Replace("NEWLINE", "\n");
			text_stat.text = _myText;
			break;
		case 1: text_modele.text = "Sportive";
			_myText = "Consommation électrique : 15,1 kWh/100 kmNEWLINEEquivalent l/100 km : 1,55 l/100 kmNEWLINEAutonomie : 350 kmNEWLINEBatterie : 53 kWhNEWLINEPrix du plein : 6,62 € (abonnement base), 7,17 € (heures pleines) ou 4,91 € (heures creuses)";
			_myText = _myText.Replace("NEWLINE", "\n");
			text_stat.text = _myText;
			break;
		case 2: text_modele.text = "Berline";
			_myText = "Consommation électrique : 19,9 kWh/100 kmNEWLINEEquivalent l/100 km : 2,03 l/100 kmNEWLINEAutonomie : 426 km (donnée EPA)NEWLINEBatterie : 85 kWhNEWLINEPrix du plein : 10,62 € (abonnement base), 11,50 € (heures pleines) ou 7,87 € (heures creuses)";
			_myText = _myText.Replace("NEWLINE", "\n");
			text_stat.text = _myText;
			break;
		default:text_modele.text = "";
			text_stat.text = "";
			break;
		}
	}
}
