﻿using UnityEngine;
using System.Collections;

public class GlobalErrorManagerFiches : MonoBehaviour {

	public void OpenGlobalErrorPanel(){

		CancelInvoke ("CloseGlobalErrorPanel");
		GetComponent<Animation>().GetComponent<Animation>()["GlobalErrorSlideIn"].speed = 1f;
		
		GetComponent<Animation>().Play("GlobalErrorSlideIn");

		Invoke ("CloseGlobalErrorPanel", 4f);
	}
	
	public void CloseGlobalErrorPanel(){
		CancelInvoke ("CloseGlobalErrorPanel");
		GetComponent<Animation>().GetComponent<Animation>()["GlobalErrorSlideIn"].time = GetComponent<Animation>().GetComponent<Animation>()["GlobalErrorSlideIn"].length;
		GetComponent<Animation>().GetComponent<Animation>()["GlobalErrorSlideIn"].speed = -1f;
		
		GetComponent<Animation>().Play("GlobalErrorSlideIn");
	}
}
