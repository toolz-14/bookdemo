﻿using UnityEngine;
using System.Collections;

public class PanneauTHHotspot : MonoBehaviour {

	public Animation AnimatedObject;
	public GameObject Labels;
	public GameObject Switch;
	public MoveEmptyCameraThermique moveEmptyCamera;
	
	private void Start () {
		
	}

	public void SelectAndShowMoteur(){
		Switch.SetActive (false);
		moveEmptyCamera.FromStartToPanneaux ();
		Invoke ("DoAnimation", 0.4f);
	}

	private void DoAnimation(){
		AnimatedObject["Thermique"].speed = 1;
		AnimatedObject["Thermique"].time = 0;
		AnimatedObject.Play("Thermique");
		Invoke ("ShowLabels", 1.8f);
	}

	private void ShowLabels(){
		Labels.SetActive (true);
	}
	
	public void CloseMoteur(){
		Labels.SetActive (false);
		AnimatedObject["Thermique"].speed = -2;
		AnimatedObject["Thermique"].time = AnimatedObject["Thermique"].length;
		AnimatedObject.Play("Thermique");
		Invoke ("Moteur", 1f);
	}

	private void Moteur(){
		Switch.SetActive (true);
		moveEmptyCamera.FromPanneauxToStart ();
	}
}
