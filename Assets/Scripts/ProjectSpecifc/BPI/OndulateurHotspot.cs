﻿using UnityEngine;
using System.Collections;

public class OndulateurHotspot : MonoBehaviour {

	public MoveEmptyCameraPhoto moveEmptyCamera;
	
	private void Start () {
		
	}

	public void SelectAndShowMoteur(){
		moveEmptyCamera.FromStartToOndulateur ();
	}
	
	public void CloseMoteur(){
		moveEmptyCamera.FromOndulateurToStart ();
	}
}
