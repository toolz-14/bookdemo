﻿using UnityEngine;
using System.Collections;

public class BallonHotspot : MonoBehaviour {

	public GameObject Labels;
	public GameObject Switch;
	public MoveEmptyCameraThermique moveEmptyCamera;
	public GameObject BallonCover;
	
	private void Start () {
		
	}
	
	public void SelectAndShowMoteur(){
		Switch.SetActive (false);
		moveEmptyCamera.FromStartToBallon ();
		BallonCover.SetActive (false);
		Invoke ("ShowLabels", 0.4f);
	}

	private void ShowLabels(){
		Labels.SetActive (true);
	}
	
	public void CloseMoteur(){
		BallonCover.SetActive (true);
		Labels.SetActive (false);
		Switch.SetActive (true);
		moveEmptyCamera.FromBallonToStart ();
	}
}
