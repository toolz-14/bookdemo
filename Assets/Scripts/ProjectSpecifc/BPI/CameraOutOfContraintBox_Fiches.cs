﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraOutOfContraintBox_Fiches : MonoBehaviour {

	[HideInInspector]
	public bool isOut;
	
	private void OnDrawGizmosSelected () {
		if(isOut){
			Gizmos.color = Color.red;
			Gizmos.DrawSphere (transform.position, 5f);
		}
	}

	#if UNITY_EDITOR
	private void OnGUI(){
		
		if (!transform.parent.GetComponent<BoxCollider>().bounds.Contains(transform.position)){
			
			isOut = true;
		}else{
			
			isOut = false;
		}
	}
	#endif
}

