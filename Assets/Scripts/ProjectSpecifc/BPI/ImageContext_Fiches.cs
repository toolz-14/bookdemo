﻿using UnityEngine;
using System.Collections;

public class ImageContext_Fiches : MonoBehaviour {

	private Vector3 _from1;
	private Vector3 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.1f;
	
	private Vector3 _from2;
	private Vector3 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.1f;
	
	private Vector3 _startScale;
	[HideInInspector]
	public bool isOpen;
	
	private void Start () {
		_startScale = GetComponent<RectTransform> ().localScale;
		isOpen = false;
	}
	
	private void Update () {
		if(_launch){
			OpenImage();
		}
		if(_launch2){
			CloseImage();
		}
	}
	
	public void StartOpenImage(){
		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().localScale;
		_to1 = Vector3.one;
		_launch = true;
	}
	
	private void OpenImage(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<RectTransform>().localScale = Vector3.Lerp(_from1, _to1, _t);
		if(Vector3.Distance(GetComponent<RectTransform>().localScale, _to1) < 0.01f){
			_launch = false;
			isOpen = true;
		}
	}
	
	public void StartCloseImage(){
		_timeSinceTransitionStarted2 = 0f;
		_from2 = GetComponent<RectTransform> ().localScale;
		_to2 = _startScale;
		_launch2 = true;
	}
	
	private void CloseImage(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		GetComponent<RectTransform>().localScale = Vector3.Lerp(_from2, _to2, _t2);
		if(Vector3.Distance(GetComponent<RectTransform>().localScale, _to2) < 0.01f){
			_launch2 = false;
			isOpen = false;
		}
	}
}
