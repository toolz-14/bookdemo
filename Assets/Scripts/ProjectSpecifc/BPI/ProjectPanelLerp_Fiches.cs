﻿using UnityEngine;
using System.Collections;

public class ProjectPanelLerp_Fiches : MonoBehaviour {

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;

	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private Vector2 _startPos;
	public Camera theMainCamera;
	
	private void Start () {
		_startPos = GetComponent<RectTransform> ().anchoredPosition;
	}

	private void Update () {
		if(_launch){
			OpenProjectPanel();
		}
		if(_launch2){
			CloseProjectPanel();
		}
	}

	public void StartLerpToQuaterCanvas(){
		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().anchoredPosition;
		float myFloat = 0f;
//		Debug.Log("Camera.main.aspect = "+Camera.main.aspect);
		if (theMainCamera.aspect >= 1.7f) {
//			Debug.Log("16:9");
			myFloat = -transform.parent.GetComponent<RectTransform> ().sizeDelta.x/4.1f;
		}
		else if (theMainCamera.aspect >= 1.59f) {
//			Debug.Log("16:10");
			myFloat = -transform.parent.GetComponent<RectTransform> ().sizeDelta.x/3.7f;
		}
		else if (theMainCamera.aspect >= 1.5f) {
//			Debug.Log("3:2");
			myFloat = -transform.parent.GetComponent<RectTransform> ().sizeDelta.x/3.5f;
		}
		else{
//			Debug.Log("4:3");
			myFloat = -transform.parent.GetComponent<RectTransform> ().sizeDelta.x/3.1f;
		}
		_to1 = new Vector2 (myFloat, GetComponent<RectTransform> ().anchoredPosition.y);
		_launch = true;
	}
	
	private void OpenProjectPanel(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f){
			_launch = false;
		}
	}

	public void StartLerpBack(){
		_timeSinceTransitionStarted2 = 0f;
		_from2 = GetComponent<RectTransform> ().anchoredPosition;
		_to2 = _startPos;
		_launch2 = true;
	}
	
	private void CloseProjectPanel(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
			GetComponent<ProjectUIManager_Fiches>().AfterPanelClose();
		}
	}
}
