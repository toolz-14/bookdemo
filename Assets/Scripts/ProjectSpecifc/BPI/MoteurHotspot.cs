﻿using UnityEngine;
using System.Collections;

public class MoteurHotspot : MonoBehaviour {

	public MoveEmptyCameraVoiture moveEmptyCamera;
	
	private void Start () {
		
	}

	public void SelectAndShowMoteur(){
		moveEmptyCamera.FromStartToMoteur ();
	}
	
	public void CloseMoteur(){
		moveEmptyCamera.FromMoteurToStart ();
	}
}
