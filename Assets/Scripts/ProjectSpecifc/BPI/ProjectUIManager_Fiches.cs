﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;

public struct ProjectData_Fiches {
	//general
	public string projectName;
	public string idProject;
	public float projectCost;
    public int nbUpvote;
    public int nbDownVote;
	
	//projectUIPanel management
	public bool contexte;
	public bool fonctionnement;
	public bool coutbenefice;
	public bool realisation;
	public bool discussion;
	public bool comparer;
	
	public bool ShowVotes;
	public bool ShowBudget;

	//project Theme management
	public List<string> activeThemes;
	public bool energy;
	public bool waste;
	public bool water;
	public bool transport;
	public bool architecture;
	public bool economy;
	public bool education;
	public bool health;
	public bool culture;

	public Project_ContextData_Fiche contextData;
	public Project_FonctionnementData_Fiche fonctionnementData;
	public Project_CoutbenefData_Fiche coutbenefData;
	public Project_RealisationData_Fiche realisationData;
}

public class ProjectUIManager_Fiches : MonoBehaviour {

	public ThemeUIManager ThemePanel;
	public GlobalErrorManager GlobalErrorManager;
	public DiscussionUIManagerLabel DiscussionUIManager;
	//exit button needs to exit the project too
	public GameObject ExitButton;
	public GameObject BackButton;
	public Text ProjectName;
	public Text SubSectionName;
	public GameObject ProjectVote;
	public GameObject Footer;
	public List<GameObject> Menu = new List<GameObject>();
	public List<GameObject> OtherMenu = new List<GameObject>();

	public float BudgetTotal = 100000f;

	private Button _projectExitButton;
	private Vector2 _tempVect2;
    private PostUserProjectVote _postUserProjectVoteService;
    private GetUserProjectVote _getUserProjectVoteService;
	
	public static ProjectData_Fiches CurrentProjectData;
	public static bool IsOpen;
	public static List<ProjectData_Fiches> CurrentProjectDataList;
	private bool _projectIsRealized;
	public static bool InitOnlyOnce;

	private void Awake () {
		IsOpen = false;
		InitOnlyOnce = false;
	}

    private void Start() {
        BackButton.SetActive(false);
       
    }

	public void OpenProjecPanel(Button projectExitButton){

		GetComponent<ProjectPanelLerp_Fiches> ().StartLerpToQuaterCanvas ();

		//get ref to project exit button
		_projectExitButton = projectExitButton;

		//close ui
		if (ThemePanel != null) {
			ThemePanel.CloseThemePanel ();
		}

		IsOpen = true;
	}

	public void CloseProjectPanel(){
		if(DiscussionUIManager.IsOpen){
			DiscussionUIManager.CloseDiscussionPanel ();
		}
		GetComponent<ProjectPanelLerp_Fiches> ().StartLerpBack ();
		if(_projectExitButton != null){
			_projectExitButton.transform.parent.parent.GetComponent<ResizeTheViewport_Fiches>().StartResize(false);
		}
	}

	public void AfterPanelClose(){

		if(_projectExitButton != null){
			//close the project as well
			_projectExitButton.onClick.Invoke();
		}
		
		//open ui
		if(ThemePanel != null){
			ThemePanel.OpenThemePanel ();
		}
		
		//reset menus
		foreach(GameObject go in Menu){
			go.SetActive(false);
		}
		IsOpen = false;

		gameObject.SetActive (false);
	}

	public void SetSubSectionTitle(string title){
		SubSectionName.text = title;
//		SubSectionName.GetComponent<Lean.LeanLocalizedText>().PhraseName = title;
//		SubSectionName.GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
	}
	
	public void SwapTitles(bool isSubsection){
		ProjectName.gameObject.SetActive (!isSubsection);
		SubSectionName.gameObject.SetActive (isSubsection);
		
	}

	public void SetProjectData(Project_Fiche theProject){
		if(!InitOnlyOnce){
			InitOnlyOnce = true;
			GameObject webservice = GameObject.FindGameObjectWithTag("WebServices");
			_postUserProjectVoteService = webservice.GetComponent<PostUserProjectVote>();
			_postUserProjectVoteService.onComplete += _postUserProjectVoteService_onComplete;
			_postUserProjectVoteService.onError += _postUserProjectVoteService_onError;

			_getUserProjectVoteService = webservice.GetComponent<GetUserProjectVote>();
			_getUserProjectVoteService.onComplete += _getUserProjectVoteService_onComplete;
			_getUserProjectVoteService.onError += _getUserProjectVoteService_onError;
		}

		SetProjectDataStruct (theProject);
		_tempVect2 = Vector2.zero;
		int i;
		//Debug.Log ("CurrentProjectData.projectName = "+CurrentProjectData.projectName);
		ProjectName.text = CurrentProjectData.projectName;
		ProjectVote.SetActive(CurrentProjectData.ShowVotes);

		Footer.SetActive(CurrentProjectData.ShowBudget);
		//if footer is show, set it
		if(CurrentProjectData.ShowBudget){
			SetFooter();
		}

		Menu [0].SetActive (CurrentProjectData.contexte);
		Menu [1].SetActive (CurrentProjectData.fonctionnement);
		Menu [2].SetActive (CurrentProjectData.coutbenefice);
		Menu [3].SetActive (CurrentProjectData.realisation);
		i = 0;
		foreach(GameObject go in Menu){
			if(go.activeInHierarchy){
				_tempVect2.x = go.GetComponent<RectTransform>().anchoredPosition.x;
				_tempVect2.y = i*(-35f);
				go.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
				i++;
			}
		}

		OtherMenu [0].SetActive (CurrentProjectData.discussion);
		OtherMenu [1].SetActive (CurrentProjectData.comparer);
		i = 0;
		foreach(GameObject go in OtherMenu){
			if(go.activeInHierarchy){
				_tempVect2.x = go.GetComponent<RectTransform>().anchoredPosition.x;
				_tempVect2.y = i*(-50f);
				go.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
				i++;
			}
		}

        //Get user's vote
		Debug.Log("CurrentProjectData.idProject = "+CurrentProjectData.idProject);
        _getUserProjectVoteService.idProject = CurrentProjectData.idProject;
        _getUserProjectVoteService.UseWebService();
	}

	public void FindProjectSetDataStruct(string projectId){
		Project_Fiche[] allprojects = GameObject.FindObjectsOfType<Project_Fiche> ();
		
		foreach (Project_Fiche pr in allprojects) {
			if(pr.projectId == projectId){
				SetProjectDataStruct(pr);
			}
		}
	}

	private void SetProjectDataStruct(Project_Fiche theProject){

		CurrentProjectData.projectName = theProject.projectName;
        CurrentProjectData.idProject = theProject.projectId;
		CurrentProjectData.projectCost = theProject.projectCost;

		CurrentProjectData.contexte = System.Convert.ToBoolean ((int)theProject.contexte);
		if(CurrentProjectData.contexte && theProject.gameObject.GetComponent<Project_ContextData_Fiche>() != null){
			CurrentProjectData.contextData = theProject.gameObject.GetComponent<Project_ContextData_Fiche>();
		}
		CurrentProjectData.fonctionnement = System.Convert.ToBoolean ((int)theProject.fonctionnement);
		if(CurrentProjectData.fonctionnement && theProject.gameObject.GetComponent<Project_FonctionnementData_Fiche>() != null){
			CurrentProjectData.fonctionnementData = theProject.gameObject.GetComponent<Project_FonctionnementData_Fiche>();
		}
		CurrentProjectData.coutbenefice = System.Convert.ToBoolean ((int)theProject.coutbenefice);
		if(CurrentProjectData.coutbenefice && theProject.gameObject.GetComponent<Project_CoutbenefData_Fiche>() != null){
			CurrentProjectData.coutbenefData = theProject.gameObject.GetComponent<Project_CoutbenefData_Fiche>();
		}
		CurrentProjectData.realisation = System.Convert.ToBoolean ((int)theProject.realisation);
		if(CurrentProjectData.realisation && theProject.gameObject.GetComponent<Project_RealisationData_Fiche>() != null){
			CurrentProjectData.realisationData = theProject.gameObject.GetComponent<Project_RealisationData_Fiche>();
		}
		CurrentProjectData.discussion = System.Convert.ToBoolean ((int)theProject.discussion);
		CurrentProjectData.comparer = System.Convert.ToBoolean ((int)theProject.comparer);

		CurrentProjectData.ShowVotes = theProject.ShowVotes;
		CurrentProjectData.ShowBudget = theProject.ShowBudget;

		CurrentProjectData.activeThemes = new List<string> ();
		CurrentProjectData.energy = System.Convert.ToBoolean ((int)theProject.energy);
		if(CurrentProjectData.energy){
			CurrentProjectData.activeThemes.Add("Energy");
		}
		CurrentProjectData.waste = System.Convert.ToBoolean ((int)theProject.waste);
		if(CurrentProjectData.waste){
			CurrentProjectData.activeThemes.Add("Waste");
		}
		CurrentProjectData.water = System.Convert.ToBoolean ((int)theProject.water);
		if(CurrentProjectData.water){
			CurrentProjectData.activeThemes.Add("Water");
		}
		CurrentProjectData.transport = System.Convert.ToBoolean ((int)theProject.transport);
		if(CurrentProjectData.transport){
			CurrentProjectData.activeThemes.Add("Transportation");
		}
		CurrentProjectData.architecture = System.Convert.ToBoolean ((int)theProject.architecture);
		if(CurrentProjectData.architecture){
			CurrentProjectData.activeThemes.Add("Architecture");
		}
		CurrentProjectData.economy = System.Convert.ToBoolean ((int)theProject.economy);
		if(CurrentProjectData.economy){
			CurrentProjectData.activeThemes.Add("Economy");
		}
		CurrentProjectData.education = System.Convert.ToBoolean ((int)theProject.education);
		if(CurrentProjectData.education){
			CurrentProjectData.activeThemes.Add("Education");
		}
		CurrentProjectData.health = System.Convert.ToBoolean ((int)theProject.health);
		if(CurrentProjectData.health){
			CurrentProjectData.activeThemes.Add("Health");
		}
		CurrentProjectData.culture = System.Convert.ToBoolean ((int)theProject.culture);
		if(CurrentProjectData.culture){
			CurrentProjectData.activeThemes.Add("Culture");
		}

		SetColorLines ();
	}

	private void SetColorLines(){
		if (CurrentProjectData.activeThemes.Count > 0) {
			DeactivateallColorLines();
			Image line;
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 10f;
			for (int i=0; i < CurrentProjectData.activeThemes.Count; i++) {
				line = transform.Find ("VLayout").Find ("ColorLines").Find ("Image_"+(i+1).ToString()).GetComponent<Image>();
				line.color = GetThemeColor(CurrentProjectData.activeThemes[i]);
				line.gameObject.SetActive(true);
			}
		} else {
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 0f;
		}
	}

	private void DeactivateallColorLines(){
		Transform line;
		for(int i=0; i<9; i++){
			line = transform.Find ("VLayout").Find ("ColorLines").Find ("Image_"+(i+1).ToString());
			line.gameObject.SetActive(false);
		}
	}

	private Color GetThemeColor(string themeName){
		foreach(Theme th in ThemePanel.availableThemes){
			if(th.themeName.Contains(themeName.ToLower())){
				return th.themeColor;
			}
		}
		return Color.white;
	}

	public void ProjectRealisation(){
		//should show project mesh always
//		if (CurrentProjectData.idProject == "MuseeMarine") {
//			_projectIsRealized = true;
//			SaveLoadManager.SaveMuseeMarineRealise(true);
//		}
		CloseProjectPanel ();
	}

	public void ProjectCancel(){
		//should hide project mesh always
//		if (CurrentProjectData.idProject == "MuseeMarine") {
//			_projectIsRealized = false;
//			SaveLoadManager.SaveMuseeMarineRealise(false);
//		}
		CloseProjectPanel ();
	}

	public void ProjectValidate(){
		//should show project mesh always?
		//something with the budget ?
		CloseProjectPanel ();
	}

	private void SetFooter(){

		Vector3 tempScale;

		//set CostValue value and unit
		//cout du projet
		Footer.transform.Find("Budget").Find("CostValue").GetComponent<Text>().text = CurrentProjectData.projectCost.ToString()+" €";

		//set RemainingValue and unit
		//budgettotal - (tous les cout de projet realises) [-le cout de ce projet ?]
		Footer.transform.Find("Budget").Find("RemainingValue").GetComponent<Text>().text = "€";

		//set BudgetGauge Cost (scale x)
		tempScale = Footer.transform.Find("Budget").Find("BudgetGauge").Find("Cost").localScale;
		tempScale.x = 0.7f;
		Footer.transform.Find ("Budget").Find ("BudgetGauge").Find ("Cost").localScale = tempScale;

		//set BudgetGauge Remaining  (scale x)
		tempScale = Footer.transform.Find("Budget").Find("BudgetGauge").Find("Remaining").localScale;
		tempScale.x = 0.5f;
		Footer.transform.Find ("Budget").Find ("BudgetGauge").Find ("Remaining").localScale = tempScale;

		//set BudgetGauge Emboss  (scale x)
		Footer.transform.Find ("Budget").Find ("BudgetGauge").Find ("Emboss").localScale = tempScale;


		//if project is "realized"
		//show both buttons
		Footer.transform.Find ("Buttons").gameObject.SetActive (true);
		Footer.transform.Find ("RealizeButton").gameObject.SetActive (false);
		
		//else
		//show realisation button
		Footer.transform.Find("Buttons").gameObject.SetActive (false);
		Footer.transform.Find ("RealizeButton").gameObject.SetActive (true);


	}

	private void DoVote(bool isFor){
        _postUserProjectVoteService.idProject = CurrentProjectData.idProject;
        if (isFor)
            _postUserProjectVoteService.userVote = 1;
        else
            _postUserProjectVoteService.userVote = -1;

        _postUserProjectVoteService.UseWebService();
	}

	public void ShowglobalError(){
		GlobalErrorManager.gameObject.SetActive (true);
		GlobalErrorManager.OpenGlobalErrorPanel();
	}

	private void ResetMenus(){
		foreach (GameObject go in Menu) {
			go.SetActive(false);
		}
		foreach (GameObject go in OtherMenu) {
			go.SetActive(false);
		}
	}

    private void UpdateVoteUI(int uservote) {
        Transform ButtonFor = ProjectVote.transform.Find("Bts").Find("ForBt");
        Transform ButtonAgainst = ProjectVote.transform.Find("Bts").Find("AgainstBt");

        if (GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>().idUser != -1) {

            //if user already voted
            if (uservote != 0) {
                //if voted yes
                if (uservote == 1) {
                    //Grey out 'vote yes' button
                    ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                    //Lighten up 'vote no' button
                    ButtonAgainst.GetComponent<CanvasGroup>().alpha = 1.0f;
                    //disable 'vote yes' button
                    ButtonFor.GetComponent<Button>().interactable = false;
                    //Activate 'vote no' button
                    ButtonAgainst.GetComponent<Button>().interactable = true;
                    ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => DoVote(false));
                }

                //if voted no
                if (uservote == -1) {
                    //Grey out "vote no" button
                    ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                    //Lighten up "vote yes" button
                    ButtonFor.GetComponent<CanvasGroup>().alpha = 1.0f;
                    //disable "vote no" button
                    ButtonAgainst.GetComponent<Button>().interactable = false;
                    //Activate "vote yes" button
                    ButtonFor.GetComponent<Button>().interactable = true;
                    ButtonFor.GetComponent<Button>().onClick.AddListener(() => DoVote(true));
                }
            }
            else {
                ButtonFor.GetComponent<Button>().onClick.AddListener(() => DoVote(true));
                ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => DoVote(false));

				//reset all
				ButtonAgainst.GetComponent<CanvasGroup>().alpha = 1.0f;
				ButtonFor.GetComponent<CanvasGroup>().alpha = 1.0f;
				ButtonAgainst.GetComponent<Button>().interactable = true;
				ButtonFor.GetComponent<Button>().interactable = true;
            }

        }
        else {
            ButtonFor.GetComponent<Button>().onClick.AddListener(() => ShowglobalError());
            ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => ShowglobalError());
        }
    }

    // WEB SERVICES CALLBACKS
    private void _postUserProjectVoteService_onError(long status, string message) {
        Debug.Log("User vote project error");
    }

    private void _postUserProjectVoteService_onComplete(long status, string message) {
        Debug.Log("User vote project OK");
        UpdateVoteUI(_postUserProjectVoteService.userVote);
    }


    private void _getUserProjectVoteService_onError(long status, string message) {
        Debug.Log("GetUserProjectVote error");
    }

    private void _getUserProjectVoteService_onComplete(long status, string message) {
        UpdateVoteUI(_getUserProjectVoteService.uservote);
    }
}
