﻿using UnityEngine;
using System.Collections;

public class BatterieHotspot : MonoBehaviour {

	public MoveEmptyCameraVoiture moveEmptyCamera;
	public Transform BatterieTop;

	private Vector3 TempVect3;

	private void Start () {

	}

	public void SelectAndShowBatterie(){
		moveEmptyCamera.FromStartToBatterie ();
		TempVect3 = BatterieTop.localPosition;
		TempVect3.y += 1f;
		BatterieTop.localPosition = TempVect3;
	}

	public void CloseBatterie(){
		moveEmptyCamera.FromBatterieToStart ();
	}
}