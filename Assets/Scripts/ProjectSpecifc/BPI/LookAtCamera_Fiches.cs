﻿using UnityEngine;
using System.Collections;


public abstract class LookAtCamera_Fiches : BaseCamera_Fiches {

	[Tooltip("The target this camera is looking at")]
	public Transform LookAtTarget;

	[HideInInspector]
	public Transform TheLookAtCamera;

	public abstract void SetCamera();

	public void LookAtTheTarget(){
		SetCamera();
		if(LookAtTarget != null){
			TheLookAtCamera.LookAt(LookAtTarget);
		}else{
			Debug.LogWarning("LookAtTarget is null");
		}
	}
}
