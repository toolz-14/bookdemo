﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public struct CostBenefElement_Fiche {
	public string Name;
	public enum elementType{ Cost, Revenue}
	public elementType amountType;
	public float amount;
	public int year;
	public enum timeType{ Repeat, Single}
	public timeType frequency;
	public enum theValueType{ Percent, Value}
	public theValueType valueType;
}

[System.Serializable]
public struct DiamondGraphValues_Fiche {

	public float JobCreation;
	public float Transport;
	public float Health;
	public float LifeBetterment;
	public float Clothing;
	public float CO2;

}

public class Project_Fiche : MonoBehaviour {


	//general
	public string projectName;
	//can contain ONLY letters numbers underscores minus.
	public string projectId;

	//General project costs
	public float projectCost = 10000f;

	//values for diamond graph
	public DiamondGraphValues_Fiche ValuesForDiamondGraph;

	public List<CostBenefElement_Fiche> projectCostsAndRevenues = new List<CostBenefElement_Fiche>();

	//projectUIPanel management
	public enum ProjectUIElements{ hidden, shown}
	public ProjectUIElements contexte;
	public ProjectUIElements fonctionnement;
	public ProjectUIElements coutbenefice;
	public ProjectUIElements realisation;
	public ProjectUIElements discussion;
	public ProjectUIElements comparer;
	
	public bool ShowVotes;
	public bool ShowBudget;

	//get ref to the hotspot button to get access to its transition
	//bad way of doing this, when there is time to work on the framework, 
	//the project should hold the data to be able to create the transition where we need it
	public GameObject Hotspot;
 	//same as above but for project exit button
	public Button ExitButton;

	//project Theme management
	public enum ProjectTheme{ no, yes}
	public ProjectTheme energy;
	public ProjectTheme waste;
	public ProjectTheme water;
	public ProjectTheme transport;
	public ProjectTheme architecture;
	public ProjectTheme economy;
	public ProjectTheme education;
	public ProjectTheme health;
	public ProjectTheme culture;

	public Dictionary<string, bool> ProjectsThemes;
	[HideInInspector]
	public List<string> activeThemes;

	[HideInInspector]
	public float CurrentUserInvestment;
	[HideInInspector]
	public float AllUserInvestmentAverage;
	
	private void Start(){

	}

	private void Awake () {
		CreateThemeDictio ();
	}

	private void CreateThemeDictio(){

		ProjectsThemes = new Dictionary<string, bool>();
		activeThemes = new List<string> ();

		ProjectsThemes ["energy"] = System.Convert.ToBoolean ((int)energy);
		if(System.Convert.ToBoolean ((int)energy)){
			activeThemes.Add("energy");
		}

		ProjectsThemes ["waste"] = System.Convert.ToBoolean ((int)waste);
		if(System.Convert.ToBoolean ((int)waste)){
			activeThemes.Add("waste");
		}

		ProjectsThemes ["water"] = System.Convert.ToBoolean ((int)water);
		if(System.Convert.ToBoolean ((int)water)){
			activeThemes.Add("water");
		}

		ProjectsThemes ["transportation"] = System.Convert.ToBoolean ((int)transport);
		if(System.Convert.ToBoolean ((int)transport)){
			activeThemes.Add("transportation");
		}

		ProjectsThemes ["architecture"] = System.Convert.ToBoolean ((int)architecture);
		if(System.Convert.ToBoolean ((int)architecture)){
			activeThemes.Add("architecture");
		}

		ProjectsThemes ["economy"] = System.Convert.ToBoolean ((int)economy);
		if(System.Convert.ToBoolean ((int)economy)){
			activeThemes.Add("economy");
		}

		ProjectsThemes ["education"] = System.Convert.ToBoolean ((int)education);
		if(System.Convert.ToBoolean ((int)education)){
			activeThemes.Add("education");
		}

		ProjectsThemes ["health"] = System.Convert.ToBoolean ((int)health);
		if(System.Convert.ToBoolean ((int)health)){
			activeThemes.Add("health");
		}

		ProjectsThemes ["culture"] = System.Convert.ToBoolean ((int)culture);
		if(System.Convert.ToBoolean ((int)culture)){
			activeThemes.Add("culture");
		}
	}
}
