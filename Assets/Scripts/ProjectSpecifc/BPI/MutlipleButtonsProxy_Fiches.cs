﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/**
 * This script has to be added to a gameobject with multiple buttons in its children.
 * The goal of this class is to set a single action that will be triggered by all of the child buttons.
 */

public class MutlipleButtonsProxy_Fiches : MonoBehaviour {

	[Tooltip("This event will be sent to every child button of this gameobject.")]
    public UnityEngine.Events.UnityEvent HotspotEvent;

	private Button[] _buttons;

	private void Start () {
		RegisterToButtonsEvents();
	}

    private void TriggerHotspotEvent() {
        HotspotEvent.Invoke();
    }

	private void RegisterToButtonsEvents(){
		_buttons = GetComponentsInChildren<Button>();
		foreach(Button b in _buttons){
            b.onClick.AddListener(TriggerHotspotEvent);
            
		}
	}
}

