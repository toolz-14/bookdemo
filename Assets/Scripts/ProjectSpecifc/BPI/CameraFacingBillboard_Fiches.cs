﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard_Fiches : MonoBehaviour {

	public Transform MyCamera = null;

	void Update () {
		if (MyCamera == null) {
			transform.LookAt (transform.position + Toolz.Managers.LevelManagerFiches.ActiveCamera.transform.rotation * Vector3.forward,
			                  Toolz.Managers.LevelManagerFiches.ActiveCamera.transform.rotation * Vector3.up);
		} else {
			transform.LookAt (2 * transform.position - MyCamera.position);
		}
	}
}
