﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SliderIndividuel : MonoBehaviour {

	public Text Investissement;
	public Text NrjProduite;
	public Text Maintenance;
	public Text Economies;
	public Text SurfaceSliderValue;
	public Text EnsoleillementSliderValue;
	public Slider theEnsoleillementSlider;
	public Slider theSurfaceSlider;

	public float CoutInstalPourUneSurfaceDe1 = 4000f; //euro
	public float CoutMaintenanceUneSurfaceDe1 = 2000f; //euro
	public float NrjProduiteUneSurfaceDe1PourSoleilDe1 = 6f; //kWh
	public float EconomiePourUneSurfaceDe1PourSoleilDe1 = 30; //euro
	
	private float Nrj = 0f;
	private float Mainten = 0f;
	
	private void Start () {
		
	}
	
	public void CalculateValues(){
		SurfaceSliderValue.text = theSurfaceSlider.value.ToString();
		EnsoleillementSliderValue.text = theEnsoleillementSlider.value.ToString();
		Nrj = (NrjProduiteUneSurfaceDe1PourSoleilDe1 * theEnsoleillementSlider.value) * theSurfaceSlider.value;
		Mainten = CoutMaintenanceUneSurfaceDe1 * theSurfaceSlider.value;
		Investissement.text = (CoutInstalPourUneSurfaceDe1 * theSurfaceSlider.value).ToString();
		NrjProduite.text = (Nrj).ToString();
		Maintenance.text = (Mainten).ToString();
		Economies.text = ((Nrj * EconomiePourUneSurfaceDe1PourSoleilDe1) - Mainten).ToString();
	}
}
