﻿using UnityEngine;
using System.Collections;

public class WaterfallButton_Fiches : MonoBehaviour {

	public GameObject WaterfallGraph;

	private void Start () {
	
	}

	public void Swap(){
		if (WaterfallGraph.activeInHierarchy) {
			if (WaterfallGraph.GetComponent<WaterfallUIManager_Fiches> () != null) {
				WaterfallGraph.GetComponent<WaterfallUIManager_Fiches> ().ClosePanel ();
			}else if (WaterfallGraph.GetComponent<WaterfallUIManager> () != null) {
				WaterfallGraph.GetComponent<WaterfallUIManager> ().ClosePanel ();
			}
		} else {
			WaterfallGraph.SetActive (true);
			if (WaterfallGraph.GetComponent<WaterfallUIManager_Fiches> () != null) {
				WaterfallGraph.GetComponent<WaterfallUIManager_Fiches> ().OpenPanel ();
			}else if (WaterfallGraph.GetComponent<WaterfallUIManager> () != null) {
				WaterfallGraph.GetComponent<WaterfallUIManager> ().OpenPanel ();
			}
		}
	}
}
