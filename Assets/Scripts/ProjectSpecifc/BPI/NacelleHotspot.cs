﻿using UnityEngine;
using System.Collections;

public class NacelleHotspot : MonoBehaviour {

	public static bool isDomestic;
	public MoveEmptyCamera moveEmptyCamera;
	public GameObject PartToRemove_domestic;
	public GameObject Canvas_domestic;
	public GameObject PartToRemove_industriel;
	public GameObject Canvas_industriel;

	private void Awake () {
		isDomestic = true;
	}

	private void Start () {
	
	}

	public void SelectAndShowEolienneMotor(){
		if(isDomestic){
			PartToRemove_domestic.SetActive(false);
			Canvas_domestic.SetActive(true);
			moveEmptyCamera.FromStartToNacelle_domestic();
		}else{
			PartToRemove_industriel.SetActive(false);
			Canvas_industriel.SetActive(true);
			moveEmptyCamera.FromStartToNacelle_industriel();
		}
	}

	public void CloseEolienneMotor(){
		if(isDomestic){
			PartToRemove_domestic.SetActive(true);
			Canvas_domestic.SetActive(false);
		}else{
			PartToRemove_industriel.SetActive(true);
			Canvas_industriel.SetActive(false);
		}
	}
}
