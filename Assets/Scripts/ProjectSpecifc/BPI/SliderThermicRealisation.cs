﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SliderThermicRealisation : MonoBehaviour {

	public Text NrjProduite;
	public Text CouvreBesoin;
	public Text Installation;
	public Text Maintenance;
	public Text Economies;
	public Text SurfaceSliderValue;
	public Text EnsoleillementSliderValue;
	public Slider theEnsoleillementSlider;
	public Slider theSurfaceSlider;
	public List<GameObject> OtherPanneauList = new List<GameObject> ();

	public float CoutInstalPourUneSurfaceDe1 = 4000f; //euro
	public float CoutMaintenanceUneSurfaceDe1 = 200f; //euro
	public float NrjProduiteUneSurfaceDe1PourSoleilDe1 = 6f; //J
	public float EconomiePourUneSurfaceDe1PourSoleilDe1 = 100; //euro
	public float BesoinNRJpourUnePersonne = 50; //personnes
	
	private float Nrj = 0f;
	private float Mainten = 0f;
	private double temp;
	
	private void Start () {
		
	}
	
	public void CalculateValues(){
		SurfaceSliderValue.text = theSurfaceSlider.value.ToString();
		EnsoleillementSliderValue.text = theEnsoleillementSlider.value.ToString();
		Nrj = (NrjProduiteUneSurfaceDe1PourSoleilDe1 * theEnsoleillementSlider.value) * theSurfaceSlider.value;
		Mainten = CoutMaintenanceUneSurfaceDe1 * theSurfaceSlider.value;
		Installation.text = (CoutInstalPourUneSurfaceDe1 * theSurfaceSlider.value).ToString();
		NrjProduite.text = (Nrj).ToString();
		Maintenance.text = (Mainten).ToString();
		Economies.text = ((Nrj * EconomiePourUneSurfaceDe1PourSoleilDe1) - Mainten).ToString();
		temp = (((NrjProduiteUneSurfaceDe1PourSoleilDe1 * theEnsoleillementSlider.value) * theSurfaceSlider.value) / BesoinNRJpourUnePersonne);
		CouvreBesoin.text = System.Math.Round(temp,2).ToString();
	}

	public void ShowHideOtherPanneau(){
		if (OtherPanneauList.Count > 0) {
			if ((theSurfaceSlider.value == 1 || theSurfaceSlider.value == 4 || theSurfaceSlider.value == 7 || theSurfaceSlider.value == 10 ||
			     theSurfaceSlider.value == 13 || theSurfaceSlider.value == 16 || theSurfaceSlider.value == 19 || theSurfaceSlider.value == 21 ||
			     theSurfaceSlider.value == 24 || theSurfaceSlider.value == 27 || theSurfaceSlider.value == 30)){
				for (int i=0; i<OtherPanneauList.Count; i++) {
					if (i <= (int)((theSurfaceSlider.value - 1f)/3f)) {
						if(!OtherPanneauList [i].activeInHierarchy){
							OtherPanneauList [i].SetActive (true);
						}
					} else {
						if(OtherPanneauList [i].activeInHierarchy){
							OtherPanneauList [i].SetActive (false);
						}
					}
				}
			}
		}
	}
	
	public void ForceHideOtherPanneau(){
		if (OtherPanneauList.Count > 0) {
			for (int i=0; i<OtherPanneauList.Count; i++) {
				OtherPanneauList [i].SetActive (false);
			}
		}
	}
}
