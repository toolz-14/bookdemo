﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public struct HospotData_Fiches {
	public List<Sprite> HotspotIconList;
	public List<Color> HotspotIconColorList;
	public string ProjectName;
}

public class CanvasHotspots_Fiches : MonoBehaviour {

	public Sprite[] SmallThemeTextures;
//	public ThemeUIManager ThemePanel;

	void Start () {
	
	}

	public HospotData_Fiches GetHotspotData(string projectID){
		List<string> tempList = FindProjectActiveThemes(projectID);
		HospotData_Fiches hospotData = new HospotData_Fiches();
		hospotData.HotspotIconList = new List<Sprite>();
		hospotData.HotspotIconColorList = new List<Color>();
		hospotData.ProjectName = FindProjectName(projectID);

		if(tempList != null && tempList.Count > 0){
			foreach(string theme in tempList){
				hospotData.HotspotIconList.Add( GetThemeSprite(theme));
				hospotData.HotspotIconColorList.Add( GetThemeColor(theme));
			}
		}
		return hospotData;
	}

	private List<string> FindProjectActiveThemes(string projectID){
		Project_Fiche[] allprojects = GameObject.FindObjectsOfType<Project_Fiche> ();
		
		foreach (Project_Fiche pr in allprojects) {
			if(pr.projectId == projectID){
//				return pr.activeThemes;
			}
		}
		return null;
	}

	private string FindProjectName(string projectID){
		Project_Fiche[] allprojects = GameObject.FindObjectsOfType<Project_Fiche> ();
		
		foreach (Project_Fiche pr in allprojects) {
			if(pr.projectId == projectID){
				return pr.projectName;
			}
		}
		return null;
	}
	
	private Color GetThemeColor(string themeName){
//		foreach(Theme th in ThemePanel.availableThemes){
//			if(th.themeName.Contains(themeName.ToLower())){
//				return th.themeColor;
//			}
//		}
		return Color.white;
	}

	private Sprite GetThemeSprite(string themeName){
		foreach(Sprite sp in SmallThemeTextures){
			if(sp.name.ToLower().Contains(themeName)){
				return sp;
			}
		}
		return null;
	}

	//order sibling index for hotspots depending on distance to camera
	//to avoid seeing hospots through other hotspots

	//label vs hotspot ?


}
