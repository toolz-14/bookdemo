﻿using UnityEngine;
using System.Collections;

public class MoveEmptyCamera : MonoBehaviour {

	public Transform CameraHolder;
	public Transform NacellePos_domestic;
	public Transform NacellePos_industriel;
	public Transform RaccordPos_domestic;
	public Transform RaccordPos_industriel;

	private Vector3 _from1;
	private Vector3 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;
	private Vector3 _startPos;
	private Vector3 _startEuler;
	private Vector3 _fromRot;
	private Vector3 _toRot;

	private void Start () {
		_startPos = CameraHolder.localPosition;
		_startEuler = CameraHolder.localEulerAngles;
	}

	private void OnEnable(){
		_startPos = CameraHolder.localPosition;
		_startEuler = CameraHolder.localEulerAngles;
	}

	public void FromStartToNacelle_domestic(){
		StartMoveCamera (_startPos, NacellePos_domestic.localPosition, _startEuler, NacellePos_domestic.eulerAngles, true);
		ReturnButton_Fiches.SubSubsectionName = "FontionnementNacelle";
	}

	public void FromStartToNacelle_industriel(){
		StartMoveCamera (_startPos, NacellePos_industriel.localPosition, _startEuler, NacellePos_industriel.eulerAngles, true);
		ReturnButton_Fiches.SubSubsectionName = "FontionnementNacelle";
	}

	public void FromNacelleToStart(){
		StartMoveCamera (CameraHolder.localPosition, _startPos, CameraHolder.eulerAngles, _startEuler, false);
		ReturnButton_Fiches.SubSubsectionName = "";
	}

	public void FromStartToRaccord_domestic(){
		StartMoveCamera (_startPos, RaccordPos_domestic.localPosition, _startEuler, RaccordPos_domestic.eulerAngles, true);
		ReturnButton_Fiches.SubSubsectionName = "FontionnementRaccord";
	}

	public void FromStartToRaccord_industriel(){
		StartMoveCamera (_startPos, RaccordPos_industriel.localPosition, _startEuler, RaccordPos_industriel.eulerAngles, true);
		ReturnButton_Fiches.SubSubsectionName = "FontionnementRaccord";
	}

	public void FromRaccord_industrielToRaccord_domestic(){
		StartMoveCamera (CameraHolder.localPosition, RaccordPos_domestic.localPosition, CameraHolder.eulerAngles, RaccordPos_domestic.eulerAngles, true);
		ReturnButton_Fiches.SubSubsectionName = "FontionnementRaccord";
	}

	public void FromRaccord_domesticToRaccord_industriel(){
		StartMoveCamera (CameraHolder.localPosition, RaccordPos_industriel.localPosition, CameraHolder.eulerAngles, RaccordPos_industriel.eulerAngles, true);
		ReturnButton_Fiches.SubSubsectionName = "FontionnementRaccord";
	}
	
	public void FromRaccordToStart(){
		StartMoveCamera (CameraHolder.localPosition, _startPos, CameraHolder.eulerAngles, _startEuler, false);
		ReturnButton_Fiches.SubSubsectionName = "";
	}

	private void Update () {
		if(_launch){
			MoveCamera();
		}
	}
	
	public void StartMoveCamera(Vector3 from, Vector3 to, Vector3 fromRot, Vector3 toRot, bool useOffset){
		_timeSinceTransitionStarted = 0f;
		_from1 = from;
		_to1 = to;
		_fromRot = fromRot;
		_toRot = toRot;
		if (useOffset) {
			//to compensate for constraintbox offset
			_to1.x -= transform.Find("ConstraintBox").GetComponent<Transform>().localPosition.x;
			_to1.y -= transform.Find("ConstraintBox").GetComponent<Transform>().localPosition.y;
			_to1.z -= transform.Find("ConstraintBox").GetComponent<Transform>().localPosition.z;
		}
		_launch = true;
	}
	
	private void MoveCamera(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		CameraHolder.localPosition = Vector3.Lerp(_from1, _to1, _t);
		CameraHolder.localEulerAngles = Vector3.Lerp(_fromRot, _toRot, _t);
		if(Vector3.Distance(CameraHolder.localPosition, _to1) < 0.01f){
			_launch = false;
		}
	}
}
