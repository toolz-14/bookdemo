﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SwitchButton_Fiches : MonoBehaviour {

	public enum SwitchButtonTypes{ Eolienne, Photo, Thermique_1, Thermique_2}
	public SwitchButtonTypes SwitchButtonType = SwitchButtonTypes.Eolienne;
	public MoveEmptyCamera moveEmptyCamera;
	public MoveEmptyCameraPhoto moveEmptyCameraPhoto;
	public RectTransform handle;
	public ReturnButton_Fiches returnButton_Fiches;
	public GameObject ObjectToActivate1;
	public GameObject ObjectToActivate2;
	private Vector3 initHandle;
	private Vector3 savedHandle;
	private bool isDomestique = true;

	private void Awake(){
		initHandle = handle.anchoredPosition3D;
	}

	private void Start () {
		isDomestique = true;
		if (SwitchButtonType == SwitchButtonTypes.Photo) {
			//public actif
			ObjectToActivate1.SetActive(true);
			ObjectToActivate2.SetActive(false);
		}else if (SwitchButtonType == SwitchButtonTypes.Thermique_1) {
			ObjectToActivate1.SetActive(false);
		}else if (SwitchButtonType == SwitchButtonTypes.Thermique_2) {
			ObjectToActivate2.SetActive(false);
		}
	}

	private void OnEnable(){
		handle.anchoredPosition3D = initHandle;
		isDomestique = true;
		if (SwitchButtonType == SwitchButtonTypes.Photo) {
			//public actif
			ObjectToActivate1.SetActive(true);
			ObjectToActivate2.SetActive(false);
		}else if (SwitchButtonType == SwitchButtonTypes.Thermique_1) {
			ObjectToActivate1.SetActive(false);
		}else if (SwitchButtonType == SwitchButtonTypes.Thermique_2) {
			ObjectToActivate2.SetActive(false);
		}
	}

	private void OnDisable(){
		handle.anchoredPosition3D = initHandle;
		isDomestique = true;
		if (SwitchButtonType == SwitchButtonTypes.Photo) {
			//public actif
			if(ObjectToActivate1 != null){
				ObjectToActivate1.SetActive(true);
			}
			if(ObjectToActivate2 != null){
				ObjectToActivate2.SetActive(false);
			}
		}else if (SwitchButtonType == SwitchButtonTypes.Thermique_1) {
			if(ObjectToActivate1 != null){
				ObjectToActivate1.SetActive(false);
			}
		}else if (SwitchButtonType == SwitchButtonTypes.Thermique_2) {
			if(ObjectToActivate2 != null){
				ObjectToActivate2.SetActive(false);
			}
		}
	}

	public void OnBgClick(){
		if (isDomestique) {
			isDomestique = false;

			//move handle
			savedHandle = handle.anchoredPosition3D;
			savedHandle.x -= 46f;
			handle.anchoredPosition3D = savedHandle;

			//do stuff
			if (SwitchButtonType == SwitchButtonTypes.Eolienne) {
				if(transform.parent.Find("Text_Switch_Dynamic") != null){
					transform.parent.Find("Text_Switch_Dynamic").GetComponent<Text>().text = "Reseau public";
				}

				if(returnButton_Fiches != null){
					returnButton_Fiches.ModelRaccordDomestique.SetActive(false);
					returnButton_Fiches.ModelRaccordPublic.SetActive(true);
				}
				moveEmptyCamera.FromRaccord_domesticToRaccord_industriel();
			}else if (SwitchButtonType == SwitchButtonTypes.Photo) {
				//domestic actif
				moveEmptyCameraPhoto.FromStartToRaccordDomestic();

				ObjectToActivate1.SetActive(false);
				ObjectToActivate2.SetActive(true);
			}else if (SwitchButtonType == SwitchButtonTypes.Thermique_1) {
				ObjectToActivate1.SetActive(true);
			}else if (SwitchButtonType == SwitchButtonTypes.Thermique_2) {
				ObjectToActivate2.SetActive(true);
			}

		}else{
			isDomestique = true;

			//move handle
			savedHandle = handle.anchoredPosition3D;
			savedHandle.x += 46f;
			handle.anchoredPosition3D = savedHandle;

			//do stuff
			if (SwitchButtonType == SwitchButtonTypes.Eolienne) {
				if(transform.parent.Find("Text_Switch_Dynamic") != null){
					transform.parent.Find("Text_Switch_Dynamic").GetComponent<Text>().text = "Reseau domestique";
				}

				if(returnButton_Fiches != null){
					returnButton_Fiches.ModelRaccordDomestique.SetActive(true);
					returnButton_Fiches.ModelRaccordPublic.SetActive(false);
				}
				moveEmptyCamera.FromRaccord_industrielToRaccord_domestic();
			}else if (SwitchButtonType == SwitchButtonTypes.Photo) {
				//public actif
				ObjectToActivate1.SetActive(true);
				ObjectToActivate2.SetActive(false);
				moveEmptyCameraPhoto.FromRaccordDomesticToStart();
			}else if (SwitchButtonType == SwitchButtonTypes.Thermique_1) {
				ObjectToActivate1.SetActive(false);
			}else if (SwitchButtonType == SwitchButtonTypes.Thermique_2) {
				ObjectToActivate2.SetActive(false);
			}
		}
	}
}
