﻿using UnityEngine;
using System.Collections;
using Toolz.Transitions;

public class TransitionTrigger_Fiches : MonoBehaviour {


	[Tooltip("Time when the \"To\" module will be activated (relative to the beginning of the transition).")]
	public float ModuleToActivationTime = 0f;
	[Tooltip("Time when the \"From\" module will be deactivated (relative to the beginning of the transition).")]
	public float ModuleFromDeactivationTime = 0f;


	[Tooltip("Duration after which this transition brick will activate, relative to global transition's start (in seconds)")]
	public float StartingTime = 0f;
	[Tooltip("Transition's duration in seconds")]
	public float TransitionDuration = 2f;

    [Tooltip("The module we are transitioning to")]
	public Module_Fiches To;
	public Toolz.Module To2;

    //Has a CustomEditor attached to it handling transitionName
    [HideInInspector]
    public string TransitionName = "Null";

    //The module we are transitioning from
	private Module_Fiches _from;
	private Toolz.Module _from2;
	Transition_Fiches _transitionModele;
	Transition_Fiches _transition;


	private void Awake() {

		_from = GetComponentInParent<Module_Fiches>();
		_from2 = GetComponentInParent<Toolz.Module>();
#if UNITY_EDITOR
        if (TransitionName == "Null") {
            Debug.LogWarning("No transition added to trigger " + gameObject.name);
        }
		if (_from == null && _from2 == null) {
			Debug.LogWarning("A trigger cannot be outside of a Module." + gameObject.name);
        }
		if (To == null && To2 == null) {
			Debug.LogWarning("Module to transition to missing" + gameObject.name);
		}
#endif

    }

    public void TriggerTransition() {
        Resources.UnloadUnusedAssets();
		if(TransitionName != "Null"){
			if (To != null && _from != null) {
				//Debug.LogError("TriggerTransition");
				_transitionModele = Resources.Load<Transition_Fiches> (Transition_Fiches.TRANSITIONS_PATH + "/" + TransitionName);
				_transition = (Transition_Fiches)Instantiate (_transitionModele);
				_transition.gameObject.SetActive (true);
				_transition.ModuleFromDeactivationTime = ModuleFromDeactivationTime;
				_transition.ModuleToActivationTime = ModuleToActivationTime;
				_transition.DoTransition (_from, To, StartingTime, TransitionDuration);
			} else if (To != null && _from2 != null) {
				_transitionModele = Resources.Load<Transition_Fiches> (Transition_Fiches.TRANSITIONS_PATH + "/" + TransitionName);
				_transition = (Transition_Fiches)Instantiate (_transitionModele);
				_transition.gameObject.SetActive (true);
				_transition.ModuleFromDeactivationTime = ModuleFromDeactivationTime;
				_transition.ModuleToActivationTime = ModuleToActivationTime;
				_transition.DoTransitionFrom (_from2, To, StartingTime, TransitionDuration);
			}else if (To2 != null && _from != null) {
				_transitionModele = Resources.Load<Transition_Fiches> (Transition_Fiches.TRANSITIONS_PATH + "/" + TransitionName);
				_transition = (Transition_Fiches)Instantiate (_transitionModele);
				_transition.gameObject.SetActive (true);
				_transition.ModuleFromDeactivationTime = ModuleFromDeactivationTime;
				_transition.ModuleToActivationTime = ModuleToActivationTime;
				_transition.DoTransitionTo (_from, To2, StartingTime, TransitionDuration);
			}
		}else{
			if (_from != null) {
				_from.Deactivate();
			} else if (_from2 != null) {
				_from2.Deactivate();
			}

			if (To != null) {
				To.Activate ();
			} else if (To2 != null) {
				To2.Activate ();
			}
		}
    }
}
