﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SliderNbEolienneDomestique : MonoBehaviour {

	public Text Investissement;
	public Text NrjProduite;
	public Text Maintenance;
	public Text Revente;
	public Text AmountSliderValue;
	public Text WindSliderValue;
	public Slider theWindSlider;
	public Slider theAmountSlider;
	public List<GameObject> EoliennesDomestique = new List<GameObject> ();
	public float CoutInstalPourUneEolienne = 30000; //euro
	public float CoutMaintenancePourUneEolienne = 2000; //euro
	public float NrjProduitePourUneEoliennePourVent1ms = 36; //kWh
	public float PrixPourRevente1kWh = 500; //euro

	private float Nrj = 0f;
	private float Mainten = 0f;

	private void Start () {
		CalculateValues ();
	}

	public void CalculateValues(){
		AmountSliderValue.text = theAmountSlider.value.ToString();
		WindSliderValue.text = theWindSlider.value.ToString();
		RotorSpeed.speedmult = theWindSlider.value * 0.2f;
		Nrj = (NrjProduitePourUneEoliennePourVent1ms * theWindSlider.value) * theAmountSlider.value;
		Mainten = CoutMaintenancePourUneEolienne * theAmountSlider.value;
		Investissement.text = (CoutInstalPourUneEolienne * theAmountSlider.value).ToString();
		NrjProduite.text = (Nrj).ToString();
		Maintenance.text = (Mainten).ToString();
		Revente.text = ((Nrj * PrixPourRevente1kWh) - Mainten).ToString();
	}

	public void ShowHideEoliennes(){
		for (int i=0; i<EoliennesDomestique.Count; i++) {
			if( i <= (int)(theAmountSlider.value - 1)){
				EoliennesDomestique [i].SetActive (true);
			}else{
				EoliennesDomestique [i].SetActive (false);
			}
		}
	}

	public void ForceHideEoliennes(){
		for (int i=0; i<EoliennesDomestique.Count; i++) {
			EoliennesDomestique [i].SetActive (false);
		}
	}
}
