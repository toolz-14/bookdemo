﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TabsColorSwap : MonoBehaviour {

	public GameObject Photo_freeCam;
	public GameObject Photo_fixedCam;
	public GameObject DomesticPanel;
	public GameObject ParcPanel;
	public Button DomesticTab;
	public Button ParcTab;

	public Color TabActiveColor;
	public Color TabInactiveColor;

	public SliderNbEolienneDomestique SliderNbEolienneDomestique;
	public SliderNbEoliennePark SliderNbEoliennePark;

	private void Awake(){
		Init ();
	}

	private void Start(){

	}

	private void Init(){
		SwapTheColorDomestic ();
	}

	public void SwapTheColorDomestic(){
		NacelleHotspot.isDomestic = true;

		//color swap
		DomesticTab.GetComponent<Image>().color = TabActiveColor;
		ParcTab.GetComponent<Image>().color = TabInactiveColor;

		//panel swap
		DomesticPanel.SetActive(true);
		ParcPanel.SetActive(false);

		//set models
		if (SliderNbEolienneDomestique != null) {
			SliderNbEolienneDomestique.ShowHideEoliennes ();
		}
		if (SliderNbEoliennePark != null) {
			SliderNbEoliennePark.ForceHideEoliennes ();
		}

		if (Photo_freeCam != null && Photo_fixedCam != null) {
			Photo_fixedCam.SetActive(true);
			Photo_freeCam.SetActive(false);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(false);
//			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(true);
		}
	}

	public void SwapTheColorIndustrie(){
		NacelleHotspot.isDomestic = false;

		//color swap
		DomesticTab.GetComponent<Image>().color = TabInactiveColor;
		ParcTab.GetComponent<Image>().color = TabActiveColor;
		
		//panel swap
		DomesticPanel.SetActive(false);
		ParcPanel.SetActive(true);

		//set models
		if (SliderNbEolienneDomestique != null) {
			SliderNbEolienneDomestique.ForceHideEoliennes ();
		}
		if (SliderNbEoliennePark != null) {
			SliderNbEoliennePark.ShowHideEoliennes ();
		}
		if (Photo_freeCam != null && Photo_fixedCam != null) {
			Photo_fixedCam.SetActive(false);
			Photo_freeCam.SetActive(true);
			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_Bati.SetActive(true);
//			ProjectUIManager_Fiches.CurrentProjectData.fonctionnementData.Photo_WallIso.SetActive(false);
		}
	}

	public void ClosePanels(){
		if(DomesticPanel.activeInHierarchy){
			DomesticPanel.SetActive(false);
		}
		if(ParcPanel.activeInHierarchy){
			ParcPanel.SetActive(false);
		}
	}
}
