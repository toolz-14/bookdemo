﻿using UnityEngine;
using System.Collections;

public class RotorSpeedIndustriel : MonoBehaviour {

	public enum MyAxis{axeY, axeZ}
	public MyAxis ChosenAxis = MyAxis.axeY;
	public float windspeed = 60f;
	public static float speedmult = 1f;
	private float WSc;
	
	private void Start () {

		if (ChosenAxis == MyAxis.axeY) {
			//random start position
			transform.Rotate (0f, Random.Range (0f, 180f), 0f, Space.Self);
		} else {
			transform.Rotate (0f, 0f, Random.Range (0f, 180f), Space.Self);

		}
	}
	
	private void Update () {
		WSc = windspeed * Time.deltaTime * speedmult;
		if (ChosenAxis == MyAxis.axeY) {
			transform.Rotate( 0f, WSc, 0f, Space.Self);
		} else {
			transform.Rotate (0f, 0f, WSc, Space.Self);
		}
	}
}
