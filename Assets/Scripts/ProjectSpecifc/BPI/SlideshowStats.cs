﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SlideshowStats : MonoBehaviour {
	
	public Text text_modele;
	public Text text_stat;

	private void Start () {
	
	}

	public void SetTexts(int index){
		switch(index){
		case 0: text_modele.text = "Eolienne en hélices";
			text_stat.text = "5 kWh";
			break;
		case 1: text_modele.text = "Eolienne Hélicoïdale";
			text_stat.text = "0,6 kWh";
			break;
		case 2: text_modele.text = "Moulin à vent";
			text_stat.text = "1,1 kWh";
			break;
		default:text_modele.text = "";
			text_stat.text = "";
			break;
		}
	}
}
