﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorButton : MonoBehaviour {

	[SerializeField] private float NewCameraY = 0;
	[SerializeField] private float NewCameraTargetY = 4;
	[SerializeField] private int FloorIndex = 0;
	[SerializeField] private Sprite ActiveIcon;
	[SerializeField] private Sprite InactiveIcon;


    private Text text;
    [SerializeField] private Color activeColor;
    [SerializeField] private Color inactiveColor;

    private FloorsButtonManager FloorsButtonManager;

    private void Awake()
    {

        text = transform.Find("Text").GetComponent<Text>();
    }

    private void Start ()
    {
    }

	private void OnEnable(){
		FloorsButtonManager = GetComponentInParent<FloorsButtonManager> ();
	}

	public void OnButtonClick () {
		if (FloorsButtonManager != null) {
			DeactivateFloors ();

			//move camera target y to new value
			Vector3 temp = FloorsButtonManager.CameraTarget.localPosition;
			temp.y = NewCameraTargetY;
			FloorsButtonManager.CameraTarget.localPosition = temp;

			//move camera y to new value
			temp = FloorsButtonManager.ModuleCamera.localPosition;
			temp.y = NewCameraY;
			FloorsButtonManager.ModuleCamera.localPosition = temp;
			FloorsButtonManager.ModuleCamera.LookAt (FloorsButtonManager.ModuleCamera.parent.parent.GetComponent<OrbitCam> ().LookAtTarget);

            //swap previous button icon to inactive
            FloorsButtonManager.SetAllIconInactive();
            FloorsButtonManager.SetAllTextColorInactive();

            //swap button icon to active
            GetComponent<Image>().sprite = ActiveIcon;
            text.color = activeColor;
        }
	}

	public void SwapIconInactive(){
		//swap button icon to inactive
		GetComponent<Image>().sprite = InactiveIcon;
	}

    public void SwapTextInactive()
    {
        text.color = inactiveColor;
    }

    private void DeactivateFloors()
    {
        if (FloorsButtonManager.Floors != null && FloorsButtonManager.Floors.Length > 0) {
			for (int i = 0; i < FloorsButtonManager.Floors.Length; i++)
            {
                if (i > FloorIndex) {
					if (FloorsButtonManager.Floors [i] != null) {
						FloorsButtonManager.Floors [i].SetActive (false);
                    }
				} else {
					if (FloorsButtonManager.Floors [i] != null)
                    {
                        FloorsButtonManager.Floors[i].SetActive(true);
                        FloorsButtonManager.Floors[i].transform.Find("Interieur").gameObject.SetActive(true);
                    }
				}
			}
		}
	}
}
