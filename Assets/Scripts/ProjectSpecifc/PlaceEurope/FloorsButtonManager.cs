﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorsButtonManager : MonoBehaviour {

	[SerializeField] private List<FloorButton> FloorButtonList = new List<FloorButton>();
	public GameObject[] Floors = new GameObject[6];
	public Transform ModuleCamera;
	public Transform CameraTarget;

	private bool notFirstTime;

	private void Start () {
		FloorButtonList [0].OnButtonClick ();
	}

	public void SetAllIconInactive(){
		foreach (FloorButton fb in FloorButtonList) {
			fb.SwapIconInactive ();
		}
	}

    public void SetAllTextColorInactive()
    {
        foreach (FloorButton fb in FloorButtonList)
        {
            fb.SwapTextInactive();
        }
    }

    public void ReactivateAllFloors(){
		if (Floors != null && Floors.Length > 0) {
			for (int i = 0; i < Floors.Length; i++) {
				if (Floors [i] != null)
                {
                    Floors[i].SetActive(true);

                    Transform[] trs = Floors[i].transform.GetComponentsInChildren<Transform>(true);
                    foreach (Transform t in trs)
                    {
                        if (t.name == "Interieur")
                        {
                            t.gameObject.SetActive(false);
                        }
                    }
                }
			}
		}
		//deactivate interior mesh for floors 2 and 3
	}

	private void OnEnable(){
		if (!notFirstTime){
			notFirstTime = true;
			return;
		}

		if(FloorButtonList [0] != null){
			FloorButtonList [0].OnButtonClick ();
		}
	}
}
