﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorActiveButton : MonoBehaviour {

	public Button button_1;
	public Button button_2;
	public Button button_3;

	public Color activeColor;
	public Color inactiveColor;

	private void Start () {
		AllInactiveColor ();
	}

	private void AllInactiveColor () {
		button_1.GetComponent<Image> ().color = inactiveColor;
		button_2.GetComponent<Image> ().color = inactiveColor;
		button_3.GetComponent<Image> ().color = inactiveColor;
	}

	public void ChangeColorActive(int buttonID){
		AllInactiveColor ();

		if (buttonID == 1) {
			button_1.GetComponent<Image> ().color = activeColor;
		} else if (buttonID == 2) {
			button_2.GetComponent<Image> ().color = activeColor;
		} else if (buttonID == 3) {
			button_3.GetComponent<Image> ().color = activeColor;
		}
	}
}
