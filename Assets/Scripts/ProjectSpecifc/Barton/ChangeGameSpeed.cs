﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangeGameSpeed : MonoBehaviour {

	private Image _speedIcon;
	private Image _speedBg;
	private Text _speedText;
	private Text _speedText_2;
	private float _speed;
	private Color Black303034 = new Color(0.1176f, 0.1176f, 0.1333f);

	private void Awake () {
		_speedIcon = transform.Find ("Image").GetComponent<Image> ();
		_speedBg = GetComponent<Image> ();
		_speedText = transform.Find ("Text_1").GetComponent<Text> ();
		_speedText_2 = transform.Find ("Text_2").GetComponent<Text> ();
	}

	private void Start () {
		SetSpeed ();
	}

	private void SetSpeed(){
		_speed = 1f;
		_speedIcon.color = Color.white;
		_speedText.color = Color.white;
		_speedText_2.color = Color.white;
		_speedBg.color = Black303034;
	}

	public void ChangeTheSpeed(){
		if(_speed <= 1f){
			_speed = _speed + 1f;
		}else if(_speed > 1f){
			_speed = _speed + 4f;
		}
		if(_speed > 10f){
			_speed = 1f;
		}
		if (_speed == 1f) {
			_speedIcon.color = Color.white;
			_speedText.color = Color.white;
			_speedText_2.color = Color.white;
			_speedBg.color = Black303034;
		}else if(_speed > 1f){
			_speedIcon.color = Black303034;
			_speedText.color = Black303034;
			_speedText_2.color = Black303034;
			_speedBg.color = Color.white;
		}
		Time.timeScale = _speed;
		_speedText_2.text = "x"+_speed.ToString();
	}

	public void ResetSpeed(){
		_speed = 1f;
		Time.timeScale = _speed;
		_speedText_2.text = "x"+_speed.ToString();
	}
}
