﻿using UnityEngine;
using System.Collections;

public class BusLines : MonoBehaviour {

	public GameObject line1;
	public GameObject line2;
	public GameObject line3;

	public GameObject line1Bus_1;
	public GameObject line1Bus_2;
	public GameObject line1Bus_3;

	public GameObject line2Bus_1;
	public GameObject line2Bus_2;
	public GameObject line2Bus_3;

	public GameObject line3Bus_1;
	public GameObject line3Bus_2;
	public GameObject line3Bus_3;

	private void Awake () {
		SetAllBusInactive ();
	}

	private void Start () {
	
	}

	private void SetAllBusInactive () {
		line1Bus_1.SetActive (false);
		line1Bus_2.SetActive (false);
		line1Bus_3.SetActive (false);

		line2Bus_1.SetActive (false);
		line2Bus_2.SetActive (false);
		line2Bus_3.SetActive (false);

		line3Bus_1.SetActive (false);
		line3Bus_2.SetActive (false);
		line3Bus_3.SetActive (false);
	}

	private void SetAllLinesInactive(){
		line1.SetActive (false);
		line2.SetActive (false);
		line3.SetActive (false);
	}

	public void SetAllInactive(){
		SetAllLinesInactive ();
		SetAllBusInactive ();
	}

	public void SetALineActive(int lineNumber){
		SetAllInactive ();
		if (lineNumber == 1) {
			line1.SetActive (true);
			line1Bus_1.SetActive (true);
			line1Bus_2.SetActive (true);
			line1Bus_3.SetActive (true);
		} else if (lineNumber == 2) {
			line2.SetActive (true);
			line2Bus_1.SetActive (true);
			line2Bus_2.SetActive (true);
			line2Bus_3.SetActive (true);
		} else if (lineNumber == 3) {
			line3.SetActive (true);
			line3Bus_1.SetActive (true);
			line3Bus_2.SetActive (true);
			line3Bus_3.SetActive (true);
		}
	}
}
