﻿using UnityEngine;
using System.Collections;

public class ResetMapCamera : MonoBehaviour {

	public Transform view;
	private Vector3 camPosition = new Vector3(-0.019f, -0.1798f, 0.006f);
	private Vector3 camEuler = new Vector3(90f, 270f, 0f);


	private void Start () {
	
	}

	public void Reset(){
		Debug.Log ("Reset()");
		if (view != null &&	view.GetComponent<FreeTerrainCamera> () != null) {
			view.GetComponent<FreeTerrainCamera> ().MoveTo (camPosition, camEuler);
		} else {
			Debug.Log ("CANNOT Reset camera");
		}
	}
}
