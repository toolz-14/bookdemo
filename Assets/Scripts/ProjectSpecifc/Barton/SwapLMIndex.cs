﻿using UnityEngine;
using System.Collections;

public class SwapLMIndex : MonoBehaviour {

	public MeshRenderer Employment;
	public MeshRenderer Bati1;
	public MeshRenderer Bati2;
	public MeshRenderer Bati3;
	public MeshRenderer Market;
	public MeshRenderer Mosquee;

	public int EmploymentIndex = 0;
	public int Bati1Index = 0;
	public int Bati2Index = 0;
	public int Bati3Index = 0;
	public int MarketIndex = 0;
	public int MosqueeIndex = 0;

	void Start () {

	}

	public void SwapIndexes(bool isOn){
		if (isOn) {
			Mosquee.lightmapIndex = 255;
			Mosquee.lightmapIndex = 255;
			Mosquee.lightmapIndex = 255;
			Mosquee.lightmapIndex = 255;
			Mosquee.lightmapIndex = 255;
			Mosquee.lightmapIndex = 255;
		} else {
			Mosquee.lightmapIndex = 2;
			Mosquee.lightmapIndex = 2;
			Mosquee.lightmapIndex = 2;
			Mosquee.lightmapIndex = 2;
			Mosquee.lightmapIndex = 2;
			Mosquee.lightmapIndex = 2;
		}
	}
}
