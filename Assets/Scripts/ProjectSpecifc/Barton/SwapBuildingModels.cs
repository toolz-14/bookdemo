﻿using UnityEngine;
using System.Collections;

public class SwapBuildingModels : MonoBehaviour {

	//active
	public GameObject Building1;
	//inactive
	public GameObject Building2;

	private bool _swap = false;

	private void Start () {
	
	}

	public void Swap(){
		_swap = !_swap;
		if (_swap) {
			Building1.SetActive(false);
			Building2.SetActive(true);
		} else {
			Building1.SetActive(true);
			Building2.SetActive(false);
		}
	}
}
