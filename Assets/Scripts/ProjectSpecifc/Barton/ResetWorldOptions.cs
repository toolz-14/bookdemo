﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResetWorldOptions : MonoBehaviour {

	public List<WorldOptionsContentSlide> WorldOptions = new List<WorldOptionsContentSlide> ();
	private bool isFirstTimeIn = false;

	private void Start(){

	}

	private void OnDisable(){
		if (isFirstTimeIn) {
			Reset (100);
		} else {
			isFirstTimeIn = true;
		}
	}

	public void Reset(int indexExclude){
		for (int i=0; i< WorldOptions.Count; i++) {
			if(i != indexExclude){
				//close all
				WorldOptions[i].StartLerpBack();
				//hide tick
				WorldOptions[i].imageTick.enabled = false;
//				WorldOptions[i].isToggleOn = false;
//				WorldOptions[i].objectToActivate.alpha = 0;
//				if(WorldOptions[i].objectToActivate2 != null){
//					WorldOptions[i].objectToActivate2.alpha = 0f;
//				}
//				if(WorldOptions[i].LandUseMats != null){
//					WorldOptions[i].LandUseMats.SwapColor(false);
//				}
//				//change button color dark
//				WorldOptions[i].buttonBg.color = WorldOptions[i].dark;
			}
		}
	}
}
