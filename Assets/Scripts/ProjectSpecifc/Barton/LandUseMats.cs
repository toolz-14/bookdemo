﻿using UnityEngine;
using System.Collections;

public class LandUseMats : MonoBehaviour {

	public MeshRenderer ZoneFixe1;
	public MeshRenderer ZoneFixe2;
	public MeshRenderer ZoneFixe3;

	public MeshRenderer Employment;
	public MeshRenderer Mesh609;
	public MeshRenderer Bati1;
	public MeshRenderer Bati2;
	public MeshRenderer Bati3;
	public MeshRenderer Market;
	public MeshRenderer Mosquee;

	public Material White;
	public Material Jaune;
	public Material Bleue;
	public Material Vert;
	public Material Water;

	private void Start(){

	}

	public void SwapColor(bool isColored){
		if (isColored) {
			Material[] tempMat = new Material[2];
			tempMat = ZoneFixe1.materials;
			tempMat[0] = White;
			tempMat[1] = Jaune;
			ZoneFixe1.materials = tempMat;

			tempMat = new Material[2];
			tempMat = ZoneFixe2.materials;
			tempMat[0] = White;
			tempMat[1] = Jaune;
			ZoneFixe2.materials = tempMat;

			tempMat = new Material[2];
			tempMat = ZoneFixe3.materials;
			tempMat[0] = White;
			tempMat[1] = Jaune;
			ZoneFixe3.materials = tempMat;

			tempMat = new Material[3];
			tempMat = Bati1.materials;
			tempMat[0] = Bleue;
			tempMat[1] = Jaune;
			tempMat[2] = Vert;
			Bati1.materials = tempMat;

			tempMat = new Material[3];
			tempMat = Bati2.materials;
			tempMat[0] = Jaune;
			tempMat[1] = White;
			tempMat[2] = Bleue;
			Bati2.materials = tempMat;

			tempMat = new Material[2];
			tempMat = Bati3.materials;
			tempMat[0] = Jaune;
			tempMat[1] = White;
			Bati3.materials = tempMat;

			tempMat = new Material[3];
			tempMat = Market.materials;
			tempMat[0] = Jaune;
			tempMat[1] = Bleue;
			tempMat[2] = Water;
			Market.materials = tempMat;

			Employment.material = Jaune;
			Mesh609.material = Vert;
			Mosquee.material = Vert;

		} else {
			Material[] tempMat = new Material[2];
			tempMat = ZoneFixe1.materials;
			tempMat[0] = White;
			tempMat[1] = White;
			ZoneFixe1.materials = tempMat;
			
			tempMat = new Material[2];
			tempMat = ZoneFixe2.materials;
			tempMat[0] = White;
			tempMat[1] = White;
			ZoneFixe2.materials = tempMat;
			
			tempMat = new Material[2];
			tempMat = ZoneFixe3.materials;
			tempMat[0] = White;
			tempMat[1] = White;
			ZoneFixe3.materials = tempMat;
			
			tempMat = new Material[3];
			tempMat = Bati1.materials;
			tempMat[0] = White;
			tempMat[1] = White;
			tempMat[2] = White;
			Bati1.materials = tempMat;
			
			tempMat = new Material[3];
			tempMat = Bati2.materials;
			tempMat[0] = White;
			tempMat[1] = White;
			tempMat[2] = White;
			Bati2.materials = tempMat;
			
			tempMat = new Material[2];
			tempMat = Bati3.materials;
			tempMat[0] = White;
			tempMat[1] = White;
			Bati3.materials = tempMat;
			
			tempMat = new Material[3];
			tempMat = Market.materials;
			tempMat[0] = White;
			tempMat[1] = White;
			tempMat[2] = Water;
			Market.materials = tempMat;
			
			Employment.material = White;
			Mesh609.material = White;
			Mosquee.material = White;
		}
	}

}
