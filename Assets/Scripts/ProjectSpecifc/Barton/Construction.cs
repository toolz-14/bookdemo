﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Construction : MonoBehaviour {

	public Slider slider;
	public Animation _animation;
	bool startUpdate = false;

	private void Start () {

	}

	private void Update () {
		if (startUpdate) {
			_animation ["ConstructionAnimation"].normalizedTime = slider.value;
		}
	}

	private void OnEnable(){

		if(slider != null){
			slider.value = 0f;
		}
		if (_animation != null) {
			_animation.Play ("ConstructionAnimation");
			_animation ["ConstructionAnimation"].speed = 0;
			startUpdate = true;
		}
	}

	private void OnDisable(){
		startUpdate = false;
	}

	public void StopAnim(){
		if(slider != null && _animation != null){
			slider.value = 1f;
			_animation ["ConstructionAnimation"].normalizedTime = slider.value;
		}
	}
}
