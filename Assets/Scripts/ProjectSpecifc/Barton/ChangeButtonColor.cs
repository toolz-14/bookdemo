﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangeButtonColor : MonoBehaviour {
	
	public string TooltipTitle;
	public string TooltipText;
	public Color OffColor;
	public Color OnColor;
//	public TextTooltip TextTooltip;
	private bool isOn;
	private Image ButtonImage;

	private void Start () {
		isOn = false;
		ButtonImage = GetComponent<Image> ();
		ButtonImage.color = OffColor;
	}
	
	public void ChangeColor(){
		isOn = !isOn;

		if (isOn) {
			ButtonImage.color = OnColor;
//			TextTooltip.SetText(TooltipText);
//			TextTooltip.SetTitle(TooltipTitle);
//
//			if(!TextTooltip.isOpen){
//				TextTooltip.OpenTextTooltipPanel();
//			}
		}else{
			ButtonImage.color = OffColor;
//			if(TextTooltip.isOpen){
//				TextTooltip.CloseTextTooltipPanel();
//			}
		}
	}
}
