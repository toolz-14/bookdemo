﻿using UnityEngine;
using System.Collections;

public class GoToScene : MonoBehaviour {

	public string TheSceneToGoTo = "";
	public GameObject overlay = null;

	private void Start () {
	
	}

	public void GoToTheScene(){
		if(TheSceneToGoTo != ""){
			if (overlay != null) {
				overlay.SetActive (true);
			}
			Application.LoadLevel (TheSceneToGoTo);
		}
	}
}
