﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_spawner : MonoBehaviour {

[SerializeField]
private List<GameObject> CarWaves = new List<GameObject>();
[SerializeField]
private Transform ParentCar;
[SerializeField]
private Transform WantedTransform;

	private void Start () {
				StartCoroutine("InstatiateCars");
	}

IEnumerator InstatiateCars()
	{
		yield return new WaitForSeconds( Random.Range(3f,7f));
		int car = Random.Range(0,CarWaves.Count);
		GameObject temp = Instantiate(CarWaves[car]);
		temp.transform.SetParent(ParentCar);
		temp.transform.localScale = WantedTransform.localScale;
		temp.transform.localPosition = WantedTransform.localPosition;
		temp.transform.localRotation= WantedTransform.localRotation;
		StartCoroutine("InstatiateCars");
	}
}

