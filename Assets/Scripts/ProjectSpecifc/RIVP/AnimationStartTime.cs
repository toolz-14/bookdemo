﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStartTime : MonoBehaviour {

[SerializeField]
private string stateName = "";

[SerializeField]
private float startTime = new float();

	void Start () {
		if (this.gameObject.GetComponent<Animator>())
			this.gameObject.GetComponent<Animator>().Play(stateName,-1,startTime);
	}
}
