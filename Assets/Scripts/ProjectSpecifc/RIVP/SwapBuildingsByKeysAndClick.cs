﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Toolz.ProjectSpecifc.Stade;

public class SwapBuildingsByKeysAndClick : MonoBehaviour {
	[SerializeField]
	private int firstBuildingIndex = 0;
	[SerializeField]
	private List<GameObject> buildings = new List<GameObject>();
	[SerializeField]
	private GameObject[] SelectedImageOn;
	//[SerializeField]
	//private GameObject[] colliders;

	private GameObject displayedObject;
	private GameObject displayedUI;
	private GameObject displayedCollider;
	private bool isBlocked;
	private int SelectedIndex;

	private void Start () {
		displayedObject = null;
		displayedUI = null;
		displayedCollider = null;
		isBlocked = false;

        if (buildings.Count > 0) // && colliders.Length > 0
        {
            //remove all buildings
            foreach (GameObject go in buildings)
            {
                go.SetActive(false);
            }
            ////remove all colliders
            //foreach (GameObject go in colliders)
            //{
            //    go.SetActive(false);
            //}

            SetDisplayed(buildings[firstBuildingIndex], SelectedImageOn[firstBuildingIndex]); //, colliders[firstBuildingIndex]
            SelectedIndex = firstBuildingIndex;
        }
	}

    //#if UNITY_STANDALONE
    //    //update displayed building by key
    //    private void Update () {
    //		if (Input.GetKey ("1") && !isBlocked) {
    //			isBlocked = true;
    //			SelectedIndex = 0;
    //			SetDisplayed (buildings [0], SelectedImageOn [0]); //, colliders[0]
    //        }
    //		if (Input.GetKey ("2") && !isBlocked) {
    //			isBlocked = true;
    //			SelectedIndex = 1;
    //			SetDisplayed (buildings [1], SelectedImageOn [1]); //, colliders[1]
    //        }
    //		if (Input.GetKey ("3") && !isBlocked) {
    //			isBlocked = true;
    //			SelectedIndex = 2;
    //			SetDisplayed (buildings [2], SelectedImageOn [2]); //, colliders[2]
    //        }
    //		if (Input.GetKey ("4") && !isBlocked) {
    //			isBlocked = true;
    //			SelectedIndex = 3;
    //			SetDisplayed (buildings [3], SelectedImageOn [3]); //, colliders[3]
    //        }
    //		if (Input.GetKey ("5") && !isBlocked) {
    //			isBlocked = true;
    //			SelectedIndex = 4;
    //			SetDisplayed (buildings [4], SelectedImageOn [4]); //, colliders[4]
    //        }
    //	}
    //#endif

    //update displayed building by click
    public void OnClickButton(int index){
		if (!isBlocked) {
			isBlocked = true;
			SelectedIndex = index;
            if (buildings.Count <= 0)
            {
                Transform temp = GameObject.FindGameObjectWithTag("RIVPProjectParent").transform;
                if (temp != null)
                {
                    buildings.Add(temp.GetComponentInChildren<ToFindKraft>(true).gameObject);
                    buildings.Add(temp.GetComponentInChildren<ToFindDarmon>(true).gameObject);
                    displayedObject = buildings[0];
                    displayedUI = SelectedImageOn[0];
                }
                
            }
            if (buildings.Count > 0)
            {
                switch (index)
                {
                    case 0:
                        SetDisplayed(buildings[0], SelectedImageOn[0]); //, colliders[0]
                        break;
                    case 1:
                        SetDisplayed(buildings[1], SelectedImageOn[1]); //, colliders[1]
                        break;
                    case 2:
                        SetDisplayed(buildings[2], SelectedImageOn[2]); //, colliders[2]
                        break;
                    case 3:
                        SetDisplayed(buildings[3], SelectedImageOn[3]); //, colliders[3]
                        break;
                    case 4:
                        SetDisplayed(buildings[4], SelectedImageOn[4]); //, colliders[4]
                        break;
                    default:
                        Debug.Log("Index not recognized");
                        break;
                }
            }
		}
	}


	//deactivate previous building+ui and activate relevant building and ui
	private void SetDisplayed(GameObject obj, GameObject ui) //, GameObject col
    { 
		if (displayedObject != null) {
			displayedObject.SetActive (false);
		}

		displayedObject = obj;

		if (displayedObject != null) {
			displayedObject.SetActive (true);
		}

		//if (Toolz.ProjectSpecifc.FPSMovementKeys.fpsIsActive) {
		//	if (displayedCollider != null) {
		//		//Debug.Log ("displayedCollider.SetActive (false) = "+displayedCollider.name);
		//		displayedCollider.SetActive (false);
		//	}
		//	if (displayedCollider != null) {
		//		//Debug.Log ("displayedCollider = " + displayedCollider.name);
		//	}
		//	//Debug.Log ("col = "+col.name);
		//	displayedCollider = col;
		//	if (displayedCollider != null) {
		//		//Debug.Log ("displayedCollider.SetActive (true) = "+displayedCollider.name);
		//		displayedCollider.SetActive (true);
		//	}
		//}

		if (displayedUI != null) {
			displayedUI.SetActive (false);
		}

		displayedUI = ui;

		if (displayedUI != null) {
			displayedUI.SetActive (true);
		}

        System.GC.Collect();
        Resources.UnloadUnusedAssets();
        Invoke ("Delayed", 0.2f);
	}

	//to prevent update from calling multiple times
	private void Delayed(){
		isBlocked = false;
	}

	//to be called by fps
	public void UpdateColliderByFpsState(bool fpsIsOn){
		//displayedCollider = colliders [SelectedIndex];
		//displayedCollider.SetActive (fpsIsOn);
	}
}
