using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.CrossPlatformInput.PlatformSpecific;


namespace Toolz.ProjectSpecifc {

	[AddComponentMenu("Toolz/ProjectSpecifc/FPSMovementKeys")]

	public class FPSMovementKeys : MonoBehaviour {

        //public bool useRTCinput;

        //public Text debugtext;
        //public Text debugtext2;
        //public Text debugtext3;

        //for project RIVP
        //[SerializeField]
        //private GameObject CollidersParent;
        //[SerializeField]
        //private SwapBuildingsByKeysAndClick swapBuildingsByKeysAndClick;
        public static bool fpsIsActive;
        //for project RIVP

        public float fpsShadowsDistance = 0.5f;
        public float mvmtTouchSpeed = 0.1f;
        public float viewTouchSpeed = 0.5f;
#if UNITY_STANDALONE || UNITY_WEBGL || WIN_TOUCH
        public float mvmtKeySpeed = 2f;
        public float viewMouseSpeed = 2f;

        public float mvmtJoystickSpeed = 2f;
        public float viewJoystickSpeed = 2f;
        //float LerpSpeed = 50f;
#endif
        float rotationY = 0F;
        public Transform _cameraHolder;
        private Rigidbody _body;
        private Vector3 _initialPosition = Vector3.zero;
        private Quaternion _initialRotation = Quaternion.identity;
        public float minimumY = -60F;
        public float maximumY = 60F;
#if UNITY_STANDALONE || UNITY_WEBGL || WIN_TOUCH
        private int _detectController = 0;
#endif
        private float savedShadowDistance;
        float z = 0.0f;
        float x = 0.0f;


        private void Awake()
        {
            _initialPosition = transform.position;
            _initialRotation = transform.rotation;

            //for project RIVP
            //if (CollidersParent != null)
            //{
            //    CollidersParent.SetActive(false);
            //}
            //for project RIVP
        }

        private void Start()
        {
            _body = GetComponent<Rigidbody>();
            if (_cameraHolder == null)
            {
                _cameraHolder = transform.Find("ViewFps").Find("ConstraintBox").Find("CameraHolder");
            }
        }

        void OnEnable()
        {
            //for project RIVP
            fpsIsActive = true;
            //swapBuildingsByKeysAndClick.UpdateColliderByFpsState(true);
            //if (CollidersParent != null)
            //{
            //    CollidersParent.SetActive(true);
            //}
            //for project RIVP

#if UNITY_STANDALONE || UNITY_WEBGL || WIN_TOUCH
            ShowCursor(false);
#endif
            savedShadowDistance = QualitySettings.shadowDistance;
            QualitySettings.shadowDistance = fpsShadowsDistance;
        }

        void OnDisable()
        {
            //for project RIVP
            fpsIsActive = false;
            //swapBuildingsByKeysAndClick.UpdateColliderByFpsState(false);
            //if (CollidersParent != null)
            //{
            //    CollidersParent.SetActive(false);
            //}
            //for project RIVP

            QualitySettings.shadowDistance = savedShadowDistance;
#if UNITY_STANDALONE || UNITY_WEBGL || WIN_TOUCH
            ShowCursor(true);
            transform.position = _initialPosition;
            transform.rotation = _initialRotation;
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_WP8_1 || WINTOUCH
			transform.position = _initialPosition;
			transform.rotation = _initialRotation;
#endif
        }

#if UNITY_STANDALONE || UNITY_WEBGL || WIN_TOUCH


        //going to use system language to decide between azerty / qwerty
        private bool DetectAzerty()
        {
            if (Application.systemLanguage == SystemLanguage.French)
            {
                //Debug.Log("SystemLanguage.French detected");
                return true;
            }
            else
            {
                //Debug.Log("SystemLanguage non French detected");
                return false;
            }
        }

        //returns: 0 if no controller, 1 if xbox, 2 if ps4 <- disabled for now
        private int DetectControllerPresent()
        {
            int result = 0;
            string[] names = null;

            if (CustomInputModule.useRTCinputStatic)
            {
                names = RTCInput.GetJoystickNames();
            }
            else
            {
                names = Input.GetJoystickNames();
            }
                
            for (int x = 0; x < names.Length; x++)
            {

                if (names[x] == "Controller (Xbox 360 Wireless Receiver for Windows)" || names[x] == "Controller (Xbox One For Windows)")
                {
                    result = 1;
                }
            }

            return result;
        }

        private void FixedUpdate()
        {
            _detectController = DetectControllerPresent();
           // if (_detectController == 0)
            //{
                if (DetectAzerty())
                {
                    if (CustomInputModule.useRTCinputStatic)
                    {
                        z = RTCInput.GetAxis("VerticalAzerty") * mvmtKeySpeed;
                        x = RTCInput.GetAxis("HorizontalAzerty") * mvmtKeySpeed;
                    }
                    else
                    {
                        z = Input.GetAxis("VerticalAzerty") * mvmtKeySpeed;
                        x = Input.GetAxis("HorizontalAzerty") * mvmtKeySpeed;
                    }
                    Vector3 temp = transform.localEulerAngles;

                    if (CustomInputModule.useRTCinputStatic)
                    {
                        temp.y += RTCInput.GetAxis("Mouse X") * viewMouseSpeed;
                    }
                    else
                    {
                        temp.y += Input.GetAxis("Mouse X") * viewMouseSpeed;
                    }


                    _body.velocity = new Vector3(x, _body.velocity.y, z);
                    _body.velocity = Quaternion.Euler(temp) * _body.velocity;

                    transform.localEulerAngles = temp;

                    if (CustomInputModule.useRTCinputStatic)
                    {
                        rotationY += -RTCInput.GetAxis("Mouse Y") * viewMouseSpeed;
                    }
                    else
                    {
                        rotationY += -Input.GetAxis("Mouse Y") * viewMouseSpeed;
                    }

                    rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                    _cameraHolder.localEulerAngles = new Vector3(rotationY, _cameraHolder.localEulerAngles.y, 0);
                }
                else
                {
                    if (CustomInputModule.useRTCinputStatic)
                    {
                        z = RTCInput.GetAxis("Vertical") * mvmtKeySpeed;
                        x = RTCInput.GetAxis("Horizontal") * mvmtKeySpeed;
                    }
                    else
                    {
                        //Debug.Log("mvmtKeySpeed = " + mvmtKeySpeed);
                        z = Input.GetAxis("Vertical") * mvmtKeySpeed;
                        x = Input.GetAxis("Horizontal") * mvmtKeySpeed;
                    }

                    Vector3 temp = transform.localEulerAngles;

                    if (CustomInputModule.useRTCinputStatic)
                    {
                        temp.y += RTCInput.GetAxis("Mouse X") * viewMouseSpeed;
                    }
                    else
                    {
                        temp.y += Input.GetAxis("Mouse X") * viewMouseSpeed;
                    }

                    _body.velocity = new Vector3(x, _body.velocity.y, z);
                    _body.velocity = Quaternion.Euler(temp) * _body.velocity;

                    transform.localEulerAngles = temp;

                    if (CustomInputModule.useRTCinputStatic)
                    {
                        rotationY += -RTCInput.GetAxis("Mouse Y") * viewMouseSpeed;
                    }
                    else
                    {
                        rotationY += -Input.GetAxis("Mouse Y") * viewMouseSpeed;
                    }
                    rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                    _cameraHolder.localEulerAngles = new Vector3(rotationY, _cameraHolder.localEulerAngles.y, 0);
                }

                //}
                //else
                //{
                //    float zj = 0.0f;
                //    float xj = 0.0f;

                //    if (CustomInputModule.useRTCinputStatic)
                //    {
                //        zj = RTCInput.GetAxis("Vertical") * mvmtKeySpeed;
                //        xj = RTCInput.GetAxis("Horizontal") * mvmtKeySpeed;
                //    }
                //    else
                //    {
                //        zj = Input.GetAxis("Vertical") * mvmtKeySpeed;
                //        xj = Input.GetAxis("Horizontal") * mvmtKeySpeed;
                //    }

                //    Vector3 tempj = transform.localEulerAngles;

                //    if (CustomInputModule.useRTCinputStatic)
                //    {
                //        tempj.y += RTCInput.GetAxis("Mouse X") * viewJoystickSpeed;
                //    }
                //    else
                //    {
                //        tempj.y += Input.GetAxis("Mouse X") * viewJoystickSpeed;
                //    }

                //    _body.velocity = new Vector3(xj, _body.velocity.y, zj);
                //    _body.velocity = Quaternion.Euler(tempj) * _body.velocity;

                //    transform.localEulerAngles = tempj;
                //    //transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, temp, Time.deltaTime);

                //    if (CustomInputModule.useRTCinputStatic)
                //    {
                //        rotationY += -RTCInput.GetAxis("Mouse Y") * viewJoystickSpeed;
                //    }
                //    else
                //    {
                //        rotationY += -Input.GetAxis("Mouse Y") * viewJoystickSpeed;
                //    }
                //    rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                //    _cameraHolder.localEulerAngles = new Vector3(rotationY, _cameraHolder.localEulerAngles.y, 0);
                //    //				Debug.Log("Input.GetAxis (XBOX4th) = "+Input.GetAxis ("XBOX4th")+"  Input.GetAxis(XBOX5th) = "+Input.GetAxis("XBOX5th"));
                //}
            }

        private void ShowCursor(bool b)
        {
            if (b)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            Cursor.visible = b;
        }

#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_WP8_1 || WINTOUCH

//		private void OnDisable(){
//			transform.position = _initialPosition;
//			transform.rotation = _initialRotation;
//		}

		void FixedUpdate () {
			float z = CrossPlatformInputManager.GetAxis("Vertical") * mvmtTouchSpeed;
//			Debug.Log ("z = "+z);
			Vector3 temp = transform.localEulerAngles;
			temp.y += CrossPlatformInputManager.GetAxis("HorizontalLook") * viewTouchSpeed;

			_body.velocity = transform.forward * z;

			transform.localEulerAngles = temp;

			rotationY += -CrossPlatformInputManager.GetAxis("VerticalLook") * viewTouchSpeed;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			_cameraHolder.localEulerAngles = new Vector3(rotationY, _cameraHolder.localEulerAngles.y, 0);

		}
#endif
    }
}
