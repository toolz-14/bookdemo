using UnityEngine;
using System.Collections;


namespace Toolz.ProjectSpecifc {
	[AddComponentMenu("Toolz/ProjectSpecifc/FPSMovementMouse")]

	public class FPSMovementMouse : MonoBehaviour {

		public float mvmTouchSpeed = 0.1f;
		public float viewTouchSpeed = 0.5f;
		public float MouseSpeed = 2f;
		public float mvmtJoystickSpeed = 2f;
		public float viewJoystickSpeed = 2f;

		float rotationY = 0F;
		private Transform _cameraHolder;
	    private Rigidbody _body;
		private Vector3 _initialPosition = Vector3.zero;

		public float minimumY = -60F;
		public float maximumY = 60F;
		private int _detectController = 0;
		
		private void Awake(){
			_initialPosition = transform.position;
		}

		private void Start () {
	        _body = GetComponent<Rigidbody>();
			_cameraHolder = transform.Find("ViewFps").Find("ConstraintBox").Find("CameraHolder");

		}

		private void OnEnable(){

		}

		private void OnDisable(){
			transform.position = _initialPosition;
		}
		
		//returns: 0 if no controller, 1 if xbox, 2 if ps4 <- disabled for now
		private int DetectControllerPresent(){
			int result = 0;
			string[] names = Input.GetJoystickNames();
			for (int x = 0; x < names.Length; x++){

				if (names[x] == "Controller (Xbox 360 Wireless Receiver for Windows)"){
					result = 1;
				}
			}

			return result;
		}
		
		private void FixedUpdate () {
			_detectController = DetectControllerPresent ();
			if (_detectController == 0) {
				if (Input.GetMouseButtonDown (0)) {
					transform.Translate (0.0f, 0.0f, MouseSpeed * Time.fixedDeltaTime);
				}

			} else {
				float zj = Input.GetAxis ("Vertical") * mvmtJoystickSpeed;
				float xj = Input.GetAxis ("Horizontal") * mvmtJoystickSpeed;
				
				Vector3 tempj = transform.localEulerAngles;
				
				tempj.y += Input.GetAxis ("Mouse X") * viewJoystickSpeed;
				
				_body.velocity = new Vector3 (xj, _body.velocity.y, zj);
				_body.velocity = Quaternion.Euler (tempj) * _body.velocity;
				
				transform.localEulerAngles = tempj;

				rotationY += Input.GetAxis("Mouse Y") * viewJoystickSpeed;
				rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
				
				_cameraHolder.localEulerAngles = new Vector3 (rotationY, _cameraHolder.localEulerAngles.y, 0);
			}
		}


	}
}
