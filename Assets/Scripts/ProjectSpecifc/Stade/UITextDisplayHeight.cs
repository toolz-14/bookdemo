﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Toolz.ProjectSpecifc.Stade {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Stade/UITextDisplayHeight")]

	public class UITextDisplayHeight : MonoBehaviour {

		public Slider TheSlider;
		private string _height;

		private void Start () {
			ReplaceText ((((int)TheSlider.value)+15).ToString());
		}

		private void ReplaceText(string TextToUse){
			GetComponent<Text>().text = TextToUse+" m";
		}

		public void DisplayHeight(){
			_height = (((int)TheSlider.value)+15).ToString();
			ReplaceText (_height);
		}
	}
}