﻿using UnityEngine;
using System.Collections;

namespace Toolz.ProjectSpecifc.Stade {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Stade/StadiumRoof")]

	public class StadiumRoof : MonoBehaviour {


		public GameObject DistTarget;
		public GameObject Roof;
		public float OpenDist = 58f;
		public float CloseDist = 80f;
		private float _dist = 0f;
		private bool _bOpened = false;

		private void Start () {

		}

		private void Update () {
			if(Roof.activeInHierarchy){
				PlayRoofAnim();
			}	
		}

		public bool isOpen{
			get { return Roof.GetComponent<Animator>().GetBool("isOpen"); }
			set { Roof.GetComponent<Animator>().SetBool("isOpen", value); }
		}


		private void PlayRoofAnim(){
			_dist = Vector3.Distance(DistTarget.transform.position, Toolz.Managers.LevelManager.ActiveCamera.transform.position);
            
			if(_dist < OpenDist && !_bOpened){
                
                isOpen = true;
				_bOpened = true;

			}else if(_dist > CloseDist && _bOpened){
				isOpen = false;
				_bOpened = false;
			}
		}
	}
}
