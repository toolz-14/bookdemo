﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolz.ProjectSpecifc.Stade {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Stade/LaunchCars")]

	public class LaunchCars : MonoBehaviour {

		public List<GameObject> CarPool = new List<GameObject>();

		private void Start () {
			StartCoroutine("StagedLaunch");
		}

		IEnumerator StagedLaunch(){
			foreach(GameObject go in CarPool){
				go.SetActive(true);
				yield return new WaitForSeconds(10f);
			}
		}
	}
}