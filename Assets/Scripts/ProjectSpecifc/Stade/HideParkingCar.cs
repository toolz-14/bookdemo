﻿using UnityEngine;
using System.Collections;

namespace Toolz.ProjectSpecifc.Stade {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Stade/HideParkingCar")]

	public class HideParkingCar : MonoBehaviour {

		public Transform ParkingCar;

		private void OnEnable(){
			Hide(true);
		}

		private void OnDisable(){
			Hide(false);
		}

		private void Hide(bool shouldHide){
			if(ParkingCar != null && ParkingCar.Find("moving_car"))
			{
				if(shouldHide ){
					ParkingCar.Find("moving_car").GetComponent<MeshRenderer>().enabled = false;
				}else{
					ParkingCar.Find("moving_car").GetComponent<MeshRenderer>().enabled = true;
				}
			}
		}
	}
}
