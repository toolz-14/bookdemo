﻿using UnityEngine;
using System.Collections;

namespace Toolz.ProjectSpecifc.Stade {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Stade/FloorSwap")]

	public class FloorSwap : MonoBehaviour {
		
		public GameObject MovableTerrain;
		//public GameObject StadiumRoof;
		public float delayBeforeAnimationPlays = 0.2f;

		private Vector3 _terrain1SavedPositon;
		private Vector3 _terrain2SavedPositon;
		private Transform _terrain1;
		private Transform _terrain2;

		private void Awake(){
			_terrain1 = MovableTerrain.transform.Find("movable_terrain1");
			_terrain2 = MovableTerrain.transform.Find("movable_terrain2");
			_terrain1SavedPositon = _terrain1.position;
			_terrain2SavedPositon = _terrain2.position;
		}
		
		private void Start () {
		}

		public bool shouldPlay{
			get { return MovableTerrain.GetComponent<Animator>().GetBool("shouldPlay"); }
			set { MovableTerrain.GetComponent<Animator>().SetBool("shouldPlay", value); }
		}

	//	public bool isAplha{
	//		get { return StadiumRoof.GetComponent<Animator>().GetBool("isAplha"); }
	//		set { StadiumRoof.GetComponent<Animator>().SetBool("isAplha", value); }
	//	}

		public IEnumerator PlayOneShot (){
			//isAplha = true;
			yield return new WaitForSeconds(delayBeforeAnimationPlays);
			shouldPlay = !shouldPlay;
		}

		private void OnEnable(){
			StartCoroutine( PlayOneShot() );
		}

		public void Deactivate(){
			//isAplha = false;
			shouldPlay = !shouldPlay;
			//reset terrains
			_terrain1.position = _terrain1SavedPositon;
			_terrain2.position = _terrain2SavedPositon;
		}
	}
}
