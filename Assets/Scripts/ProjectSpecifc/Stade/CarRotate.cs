﻿using UnityEngine;
using System.Collections;

namespace Toolz.ProjectSpecifc.Stade {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Stade/CarRotate")]

	public class CarRotate : MonoBehaviour {

		private Vector3 _temp;

		private void Start () {
		
		}

		public void ChangeDirection(){
			_temp = transform.Find("moving_car").localEulerAngles;
			_temp.y += 180f;
			transform.Find("moving_car").localEulerAngles = _temp;
		}
	}
}