﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleBus : MonoBehaviour {

	public List<GameObject> BusObjects = new List<GameObject>();

	private Toggle _toggle;

	private void Start () {
		_toggle = GetComponent<Toggle> ();
	}

	public void ShowBus(){
		for(int i=0; i<BusObjects.Count ; i++){
			BusObjects [i].SetActive (true);
		}
	}

	public void HideBus(){
		for(int i=0; i<BusObjects.Count ; i++){
			BusObjects [i].SetActive (false);
		}
	}

	public void ToggleButton(){
		_toggle.isOn = !_toggle.isOn;

		if (_toggle.isOn) {
			ShowBus ();
		} else {
			HideBus ();
		}
	}
}
