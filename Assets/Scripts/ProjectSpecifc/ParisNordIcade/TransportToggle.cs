﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TransportToggle : MonoBehaviour {

	private Toggle _toggle;

	private void Start () {
		_toggle = GetComponent<Toggle> ();
	}
	
	public void ToggleButton(){
		_toggle.isOn = !_toggle.isOn;
	}
}
