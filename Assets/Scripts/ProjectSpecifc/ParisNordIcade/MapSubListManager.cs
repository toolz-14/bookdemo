﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapSubListManager : MonoBehaviour {

	public List<GameObject> subList = new List<GameObject> ();
	public bool isShowing;

	private void Start () {
		isShowing = false;
	}
	
	public void ShowToggle(){
		isShowing = !isShowing;
		if(isShowing){
			for(int i=0; i<subList.Count; i++){
				subList [i].SetActive (true);
			}
		}else{
			for(int i=0; i<subList.Count; i++){
				subList [i].SetActive (false);
			}
		}
	}

	public void CloseToggle(){
			
		for(int i=0; i<subList.Count; i++){
			subList [i].SetActive (false);
		}
	}
}
