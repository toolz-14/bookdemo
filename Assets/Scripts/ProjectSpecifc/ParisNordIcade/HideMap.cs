﻿using UnityEngine;
using System.Collections;

public class HideMap : MonoBehaviour {

	public GameObject surroundings;
	public Material map;
	public Material grey;

	private void Start () {
	}

	public void Show (bool show) {
		if(show){
		surroundings.GetComponent<MeshRenderer>().material = map;

		}else{
		surroundings.GetComponent<MeshRenderer>().material = grey;
		}
	}
}
