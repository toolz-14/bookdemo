﻿using UnityEngine;
using System.Collections;

public class Hide3D : MonoBehaviour {

	public GameObject threeD;

	private void Start () {

	}
	
	public void Hide (bool hide) {
		if(hide){
			threeD.SetActive(false);

		}else{
			threeD.SetActive(true);
		}
	}
}
