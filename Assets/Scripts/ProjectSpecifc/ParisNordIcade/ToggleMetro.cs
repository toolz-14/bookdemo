﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleMetro : MonoBehaviour {

	public List<GameObject> MetroObjects = new List<GameObject>();

	private Toggle _toggle;

	private void Start () {
		_toggle = GetComponent<Toggle> ();
	}

	public void ShowMetro(){
		for(int i=0; i<MetroObjects.Count ; i++){
			MetroObjects [i].SetActive (true);
		}
	}

	public void HideMetro(){
		for(int i=0; i<MetroObjects.Count ; i++){
			MetroObjects [i].SetActive (false);
		}
	}

	public void ToggleButton(){
		_toggle.isOn = !_toggle.isOn;

		if (_toggle.isOn) {
			ShowMetro ();
		} else {
			HideMetro ();
		}
	}
}
