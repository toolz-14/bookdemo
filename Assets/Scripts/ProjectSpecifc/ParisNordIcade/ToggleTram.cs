﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleTram : MonoBehaviour {

	public List<GameObject> TramObjects = new List<GameObject>();

	private Toggle _toggle;

	private void Start () {
		_toggle = GetComponent<Toggle> ();
	}

	public void ShowTram(){
		for(int i=0; i<TramObjects.Count ; i++){
			TramObjects [i].SetActive (true);
		}
	}

	public void HideTram(){
		for(int i=0; i<TramObjects.Count ; i++){
			TramObjects [i].SetActive (false);
		}
	}

	public void ToggleButton(){
		_toggle.isOn = !_toggle.isOn;

		if (_toggle.isOn) {
			ShowTram ();
		} else {
			HideTram ();
		}
	}
}
