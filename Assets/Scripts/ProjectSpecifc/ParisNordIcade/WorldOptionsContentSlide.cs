﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WorldOptionsContentSlide : MonoBehaviour {


	public RectTransform content;
	public Image imageTick;
	public Image buttonBg;
	public CanvasGroup objectToActivate;
	public CanvasGroup objectToActivate2 = null;
	public LandUseMats LandUseMats = null;
	public ResetWorldOptions ResetWorldOptions;

	// 0 = content hidden, 1 = content open no tick, 2 = content open with tick, 3 = content halfopen with tick
	[HideInInspector]
	public int buttonState = 0;
	[HideInInspector]
	public bool isToggleOn;
	[HideInInspector]
	public Color dark = new Color (0.0823f, 0.1647f, 0.196f, 1f);
	private Color sfGreen = new Color (0.1725f, 0.3803f, 0.4117f, 1f);

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;

	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private Vector2 _from3;
	private Vector2 _to3;
	private bool _launch3;
	private float _timeSinceTransitionStarted3;
	private float _t3;
	private float Duration3 = 0.4f;

	private float _from4;
	private float _to4;
	private bool _launch4;
	private float _timeSinceTransitionStarted4;
	private float _t4;
	private float Duration4 = 0.4f;

	//position gotten by hand from editor
	private Vector2 _allHiddenPos = new Vector2(-50f, 0f);
	//position gotten by hand from editor
	private Vector2 _showPanelOpenFull = new Vector2(100f, 0f);
	//position gotten by hand from editor
	private Vector2 _showPanelOpenHalf = new Vector2(0f, 0f);

	private int _wantedIndex = 0;


	private void Start () {
		isToggleOn = false;
		_allHiddenPos = content.anchoredPosition;
	}

	private void Update () {
		if(_launch){
			LerpPanelOpenFull();
		}
		if(_launch2){
			LerpPanelOpenHalf();
		}
		if(_launch3){
			LerpBack();
		}
		if(_launch4){
			LerpAlpha();
		}
	}

	public void CloseMapPanel(){
		//close all
		StartLerpBack();
		//hide tick
		imageTick.enabled = false;
		isToggleOn = false;
		
		//change button color dark
		buttonBg.color = dark;

		transform.parent.parent.GetComponent<Map3dPanel> ().StartLerpPanelBack ();
	}

	// 0 = content hidden, 1 = content open no tick, 2 = content open with tick, 3 = content halfopen with tick
	public void ManageContentSlide(){
		CancelInvoke ();
		if(buttonState == 0){
			StartLerpPanelOpenFull ();

			//open then delay and auto close
			Invoke("DelayAutoClose", 2f);
		}
		if(buttonState == 1){

//			ResetWorldOptions.Reset(_wantedIndex);
			StartLerpPanelOpenHalf ();
			//show tick
			imageTick.enabled = true;
			isToggleOn = true;
			//change button color green
			buttonBg.color = sfGreen;
			if(gameObject.name == "WorldOption_Transport"){
				transform.parent.parent.GetComponent<Map3dPanel> ().StartLerpPanelOpen ();
			}
		}
		if(buttonState == 2){
			//close all
			StartLerpBack();
			//hide tick
			imageTick.enabled = false;
			isToggleOn = false;

			//change button color dark
			buttonBg.color = dark;

			if(gameObject.name == "WorldOption_Transport"){
				transform.parent.parent.GetComponent<Map3dPanel> ().StartLerpPanelBack ();
			}
		}
		if(buttonState == 3){
			StartLerpPanelOpenFull ();

			//open then delay and auto close
			Invoke("DelayAutoCloseWithTick", 2f);
		}
	}

	private void DelayAutoClose(){
		StartLerpBack ();
	}

	private void DelayAutoCloseWithTick(){
		StartLerpPanelOpenHalf ();
	}

	public void StartLerpAlpha(bool isGoingDown){
		_timeSinceTransitionStarted4 = 0f;
		if (isGoingDown) {
			_from4 = 0.8f;
			_to4 = 0f;
		} else {
			_from4 = 0f;
			_to4 = 0.8f;
		}
		_launch4 = true;
	}
	
	private void LerpAlpha(){
		_timeSinceTransitionStarted4 += Time.deltaTime;
		_t4 = _timeSinceTransitionStarted4 / Duration4;

		if(objectToActivate2 != null){
			objectToActivate2.alpha = Mathf.Lerp(_from4, _to4, _t4);
		}
		objectToActivate.alpha = Mathf.Lerp(_from4, _to4, _t4);

		if(Mathf.Abs(objectToActivate.alpha - _to4) < 0.01f){
			_launch4 = false;
		}
	}

	public void StartLerpPanelOpenFull(){
		_timeSinceTransitionStarted = 0f;
		_from1 = content.anchoredPosition;
		_to1 = _showPanelOpenFull;
		_launch = true;
	}

	private void LerpPanelOpenFull(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;

		content.anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(content.anchoredPosition, _to1) < 0.01f){
			if (!isToggleOn) {
				buttonState = 1;
			} else {
				buttonState = 2;
			}
			_launch = false;
		}
	}

	public void StartLerpPanelOpenHalf(){
		_timeSinceTransitionStarted2 = 0f;
		_from2 = content.anchoredPosition;
		_to2 = _showPanelOpenHalf;
		_launch2 = true;
	}

	private void LerpPanelOpenHalf(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;

		content.anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(content.anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
			buttonState = 3;
		}
	}

	public void StartLerpBack(){
		_timeSinceTransitionStarted3 = 0f;
		_from3 = content.anchoredPosition;
		_to3 = _allHiddenPos;
		_launch3 = true;
	}

	private void LerpBack(){
		_timeSinceTransitionStarted3 += Time.deltaTime;
		_t3 = _timeSinceTransitionStarted3 / Duration3;

		content.anchoredPosition = Vector2.Lerp(_from3, _to3, _t3);
		if(Vector2.Distance(content.anchoredPosition, _to3) < 0.01f){
			_launch3 = false;
			buttonState = 0;
		}
	}
}
