﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BusDelay : MonoBehaviour {

	public float delay = 20f;
	public List<GameObject> buses = new List<GameObject>();

	private void Start () {
		StartCoroutine ("StartWithDelay");
	}

	IEnumerator StartWithDelay(){
		for(int i=0; i< buses.Count; i++){
			yield return new WaitForSeconds (delay+Random.Range(1f,4f));
			buses[i].SetActive(true);
		}
	}
}
