﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleRER : MonoBehaviour {

	public List<GameObject> RERObjects = new List<GameObject>();

	private Toggle _toggle;

	private void Start () {
		_toggle = GetComponent<Toggle> ();
	}

	public void ShowRER(){
		for(int i=0; i<RERObjects.Count ; i++){
			RERObjects [i].SetActive (true);
		}
	}

	public void HideRER(){
		for(int i=0; i<RERObjects.Count ; i++){
			RERObjects [i].SetActive (false);
		}
	}

	public void ToggleButton(){
		_toggle.isOn = !_toggle.isOn;

		if (_toggle.isOn) {
			ShowRER ();
		} else {
			HideRER ();
		}
	}
}
