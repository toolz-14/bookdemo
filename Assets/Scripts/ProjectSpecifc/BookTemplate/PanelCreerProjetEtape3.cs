﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean;
using Crosstales.FB;
using System.IO;
using Toolz.WebServices;

public class PanelCreerProjetEtape3 : MonoBehaviour
{

    [SerializeField]
    private PanelCreerModifierProjet panelCreerProjet;
    public static bool isPanelImagesProjetOpen;


    private int ICIndex = 0;
    private int selectedImageCouvertureIndex = 0;
    public static bool isPanelImageCouvertureProjetOpen;

    //image related stuff
    [SerializeField]
    private GameObject panelImages;
    [SerializeField]
    private GameObject imagesProjet_Item;
    [SerializeField]
    private Transform imagesProjetItem_Parent;
    //image couverture related stuff
    //[SerializeField]
    //private GameObject panelImageCouvertureProjet;
    [SerializeField]
    private Image imageCouverture;
    private byte[] videoByteArray;

    private bool uploadingImage = false;
    private bool uploadingVideo = false;
    private GameObject currentimageUploaded;
    private byte[] imageByteArray;
    private List<GameObject> projectImagesList = new List<GameObject>(); // List of active images
    private List<Sprite> projetImages = new List<Sprite>(); // List of active images

    //dropdown related stuff
    [SerializeField]
    private GameObject dropDown_Item;
    [SerializeField]
    private Transform DropDownItem_Parent;
    [SerializeField]
    private Transform addDropDown_Button;
    private Image currentimageToDelete;

    [HideInInspector]
    public List<GameObject> dropDownCategoryList = new List<GameObject>(); // List of active dropdown

    
    public void ClearPanel()
    {
        GameObject temp = dropDownCategoryList[0];

        for (int i = 0; i < dropDownCategoryList.Count; i++)
        {
            Destroy(dropDownCategoryList[i]);
        }
        dropDownCategoryList.Clear();
        // ajout d'un premier dropdown
        AddaDropDownItem(false);


        for (int i = 0; i < projectImagesList.Count; i++)
        {
            Destroy(projectImagesList[i]);
        }
        projectImagesList.Clear();
        projetImages.Clear();
    }

    public void FillPanel(ProjectMarkerData projectData)
    {
        if (projectData.rawData.categories!=null && projectData.rawData.categories.Count > 0)
        {
            // Fill 1st dropdown
            SetDropDown(dropDownCategoryList[0], projectData.rawData.categories[0]);

            if (projectData.rawData.categories.Count > 1)
            {
                for (int i = 1; i < projectData.rawData.categories.Count ; i++) // We skip 1st item
                {
                    AddaDropDownItem(true);
                    SetDropDown(dropDownCategoryList[i], projectData.rawData.categories[i]);
                    //dropDownCategoryList[i];
                }
            }
        }
        // Set Dropdown
        //projectData.rawData.
    }

    public void SetDropDown(GameObject dropdown, int id)
    {
        dropdown.GetComponent<DropDownCategoryManager>().SetDropDownValue(id);
    }

    public void AddaDropDownItem(bool isDeletable)
    {
        //instantiate
        GameObject temp = Instantiate(dropDown_Item);
        temp.SetActive(true);
        //parent
        temp.transform.SetParent(DropDownItem_Parent);
        //set position one before last in hierachy
        int indexNumber = addDropDown_Button.GetSiblingIndex();
        temp.transform.SetSiblingIndex(indexNumber);
        if (isDeletable)
            temp.transform.Find("ButtonDeleteCategory").gameObject.SetActive(true);
        //reset scale to avoid unity bug
        temp.transform.localScale = Vector3.one;
        //resize scroll container size
        DropDownItem_Parent.GetComponent<ResizeContainerWithChildAmountProject>().GetChildrensAndResize();
        //force layout rebuild
        LayoutRebuilder.ForceRebuildLayoutImmediate(DropDownItem_Parent.GetComponent<RectTransform>());
        dropDownCategoryList.Add(temp);
        
    }

    public void DeleteaDropDownItem(GameObject temp)
    {
        dropDownCategoryList.Remove(temp);
        Destroy(temp);

        //resize scroll container size
        DropDownItem_Parent.GetComponent<ResizeContainerWithChildAmountProject>().GetChildrensAndResize();
        //force layout rebuild
        LayoutRebuilder.ForceRebuildLayoutImmediate(DropDownItem_Parent.GetComponent<RectTransform>());
    }

    #region image
    public void ButtonImages()
    {
        isPanelImagesProjetOpen = !isPanelImagesProjetOpen;
        if (isPanelImagesProjetOpen)
        {
            panelImages.SetActive(true);
        }
        else
        {
            panelImages.SetActive(false);
        }
        //resize scroll container size
        DropDownItem_Parent.GetComponent<ResizeContainerWithChildAmountProject>().GetChildrensAndResize();
    }

    public string OpenSingleImgFile()
    {
        string testPath = @"C:";
        string path = FileBrowser.OpenSingleFile("Open single file", testPath, new ExtensionFilter("Image Files", "png", "jpg", "jpeg"));

        return path;
    }

    public void ButtonImagesPanelForceClose()
    {
        if (isPanelImagesProjetOpen)
        {
            panelImages.SetActive(false);
            isPanelImagesProjetOpen = false;
            //resize scroll container size
            DropDownItem_Parent.GetComponent<ResizeContainerWithChildAmountProject>().GetChildrensAndResize();
        }
    }

    public void AddAnImage()
    {
        string path = OpenSingleImgFile();
        // TO DO AFTER FILE LOADED
        if (uploadingImage)
        {
            // TODO DISPLAY ERROR
            Debug.LogError("image already uploading");
        }
        else
        {
            if (path != null && path != "")
            {
                // read image and store in a byte array
                imageByteArray = File.ReadAllBytes(path);

                GameObject image = Instantiate(imagesProjet_Item);
                image.SetActive(true);
                currentimageUploaded = image;
                projectImagesList.Add(currentimageUploaded);

                // WEBSERVICE CALL
                uploadingImage = true;
                panelCreerProjet.UploadImage(imageByteArray);
            }
        }
    }

    public void UploadSucceed(string imageName)
    {
        //Debug.Log("UploadSucceed");

        uploadingImage = false;
        //create a texture and load byte array to it
        // Texture size does not matter 
        Texture2D sampleTexture = new Texture2D(2, 2);
        // the size of the texture will be replaced by image size
        bool isLoaded = sampleTexture.LoadImage(imageByteArray);

        Sprite sprite = Sprite.Create(sampleTexture, new Rect(0, 0, sampleTexture.width, sampleTexture.height), new Vector2(0.5f, 0.5f));

        if (isLoaded)
        {
            // apply this texure as per requirement on image or material
            currentimageUploaded.transform.Find("Image").GetComponent<Image>().sprite = sprite;
            currentimageUploaded.transform.Find("Image").GetComponent<Image>().sprite.name = imageName;

            // Disable loading img
            currentimageUploaded.transform.Find("Image").Find("Image_Loader").gameObject.SetActive(false);
            currentimageUploaded.transform.Find("Image").Find("Button_Supprimer").gameObject.SetActive(true);

            //GetComponent<RawImage>().texture = sampleTexture;
            //parent
            currentimageUploaded.transform.SetParent(imagesProjetItem_Parent);

            //reset scale to avoid unity bug
            currentimageUploaded.transform.localScale = Vector3.one;

            projetImages.Add(sprite);
        }
        else // TODO DISPLAY ERROR
            Debug.LogError("LoadImage failed");

        //if first image added need to set couverture image
    }

    // TODO DISPLAY ERROR
    public void UploadFailed()
    {
        Debug.Log("UploadFailed");
        uploadingImage = false;
        Destroy(currentimageUploaded);
    }

    public void DeleteAnImage(Image image)
    {
        currentimageToDelete = image;
        panelCreerProjet.DeleteImage(image.sprite.name);
    }

    public void DeleteSucceed()
    {
        if (currentimageToDelete != null)
        {
            GameObject temp = currentimageToDelete.transform.parent.gameObject;
            projectImagesList.Remove(temp);
            Destroy(temp);
            projetImages.Remove(currentimageToDelete.sprite);
            currentimageToDelete = null;
        }
        else
        {
            Debug.Log("Error occurs : currentimageToDelete is null");
        }

    }

    //TODO DISPLAY ERROR
    public void DeleteFailed()
    {
        Debug.Log("DeleteFailed");
    }

    public bool CheckPanel3Requirements()
    {
        bool allfieldCompleted = false;

        // Check dropdown
        int catID = -1;
        if (dropDownCategoryList != null && dropDownCategoryList.Count > 0 && dropDownCategoryList[0] != null)
        {
            foreach (GameObject cat in dropDownCategoryList)
            {
                catID = cat.GetComponent<DropDownCategoryManager>().chosenCategoryID;
                //Debug.Log("catID : " + catID);
                if (catID == -1)
                {
                    allfieldCompleted = false;
                    return allfieldCompleted;
                }
            }
            allfieldCompleted = true;
        }

        // TODO Check other fields

        return allfieldCompleted;
    }

    public void ButtonImageCouverturePanelForceClose()
    {
        if (isPanelImageCouvertureProjetOpen)
        {
            //panelImageCouvertureProjet.SetActive(false);
            isPanelImageCouvertureProjetOpen = false;
            //resize scroll container size
            DropDownItem_Parent.GetComponent<ResizeContainerWithChildAmountProject>().GetChildrensAndResize();
        }
    }

    public void SetImageCouverture(Toggle toggle)
    {
        if (toggle)
        {
            string imgName = toggle.transform.parent.GetComponent<Image>().sprite.name;

            bool imageFound = projetImages.Exists(x => x.name == imgName);
            if (imageFound)
            {
                imageCouverture.sprite = projetImages.Find(x => x.name == imgName);
                imageCouverture.sprite.name = imgName;
                panelCreerProjet.coverImageName = imgName;
                //Debug.Log("Image found : " + imgName);
            }
            else
            {
                Debug.Log("Image not found");
                // TODO Display error cover image not found
            }
        }
    }

    //TODO auto set cover image when 1st image is uploaded
    public void AutoSetImageCouverture(string name)
    {
        bool imageFound = projetImages.Exists(x => x.name == name);
        if (imageFound)
        {
            imageCouverture.sprite = projetImages.Find(x => x.name == name);
            imageCouverture.sprite.name = name;
        }
        else
        {
            // TODO Display error cover image not found
        }
    }
    #endregion image

    //get path
    public string OpenSingleVideoFile()
    {
        string testPath = @"C:";
        string path = FileBrowser.OpenSingleFile("Open single file", testPath, new ExtensionFilter("Video Files", "webm", "mp4"));

        return path;
    }

    public void AddAVideo()
    {
        string path = OpenSingleVideoFile();
        // TO DO AFTER FILE LOADED
        if (uploadingVideo)
        {
            // TODO DISPLAY ERROR
            Debug.LogError("image already uploading");
        }
        else
        {
            if (path != null && path != "")
            {
                // read image and store in a byte array
                videoByteArray = File.ReadAllBytes(path);
                
                // WEBSERVICE CALL
                uploadingVideo = true;
                panelCreerProjet.UploadVideo(videoByteArray);
            }
        }
    }
}
