namespace Mapbox.Examples
{
	using UnityEngine;
	using Mapbox.Utils;
	using Mapbox.Unity.Map;
	using Mapbox.Unity.MeshGeneration.Factories;
	using Mapbox.Unity.Utilities;
	using System.Collections.Generic;
    using Toolz.WebServices;
    using System.Collections;

    public class MarkersManager : MonoBehaviour
	{
        [SerializeField]
        private Transform theParent;

        [SerializeField]
		AbstractMap _map;

		Vector2d _location;

		[SerializeField]
		float _spawnScale = 100f;

		[SerializeField]
		private GameObject _markerPrefab;
        [SerializeField]
        private GameObject _groupMarkerPrefab;

        private Dictionary<GameObject, Vector2d> _spawnedMarkers;
        public List<ProjectMarkerData> projectMarkers;

        private List<GameObject> markerGroupsPOOL;
        private const int POOLSIZE = 6; 

        private List<GameObject> markers = new List<GameObject>();
        [HideInInspector]
        public List<GameObject> markersGroupped = new List<GameObject>();
        private List<GameObject> markerDisabledfromCategory = new List<GameObject>();
        private List<List<GameObject>> markerGroups = new List<List<GameObject>>();
        private int oldZoom;
        public static bool blockUpdate;

        private GameObject spawnedObject;
        private Vector2d location;
        private Vector3 tempPos;
        private Vector3 tempScale = Vector3.zero;

        public static bool SetToggleAllOn;
        [SerializeField]
        private ToolzToggle metiersToggle;
        [SerializeField]
        private ToolzToggle secteurMarcheToggle;

        // WEBSERVICE
        private GetBookProjectsbyCompanyID _getBookProjectsbyCompanyID;
        private GetImageProject _getImageProject;
        public bool waitingWebRequest;

        private bool refreshCategories = false;
        
        private void Awake()
        {
            CreatePoolOfGroupMarkerPrefab();

            GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
            _getImageProject = webService.GetComponent<GetImageProject>();

            _getBookProjectsbyCompanyID = webService.GetComponent<GetBookProjectsbyCompanyID>();

            LoaderProfileUI.loginCompleted += GetProjectsData;
        }

        // Called after user login
        private void GetProjectsData(int userStatus)
        {
            SetToggleAllOn = true;
            _getBookProjectsbyCompanyID.adminAccess = (userStatus == (int)ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) ? true : false;
            _getBookProjectsbyCompanyID.onComplete += _getBookProjectsbyCompanyID_onComplete;
            _getBookProjectsbyCompanyID.onError += _getBookProjectsbyCompanyID_onError;
            _getBookProjectsbyCompanyID.UseWebService();
        }

        // Called after project updated / deleted
        public void Refresh()
        {
            // Refresh categories  
            refreshCategories = true;

            User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>();
            _getBookProjectsbyCompanyID.adminAccess = (user.userStatus == (int)ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) ? true : false;
            _getBookProjectsbyCompanyID.onComplete += _getBookProjectsbyCompanyID_onComplete;
            _getBookProjectsbyCompanyID.onError += _getBookProjectsbyCompanyID_onError;
            _getBookProjectsbyCompanyID.UseWebService();
        }

        private void Start()
		{
            oldZoom = _map.AbsoluteZoom;
            _spawnedMarkers = new Dictionary<GameObject, Vector2d>();
            projectMarkers = new List<ProjectMarkerData>();

            // GET all projects by companyID
        }

		private void Update()
		{
            if (!blockUpdate)
            {
                foreach (KeyValuePair<GameObject, Vector2d> marker in _spawnedMarkers)
                {
                    spawnedObject = marker.Key;
                    location = marker.Value;
                    tempPos = _map.GeoToWorldPosition(location, true);
                    tempPos.y = 1;
                    spawnedObject.transform.position = tempPos;
                    tempScale.x = _spawnScale;
                    tempScale.y = _spawnScale;
                    tempScale.z = _spawnScale;
                    spawnedObject.transform.localScale = tempScale;
                }
                if (oldZoom != _map.AbsoluteZoom)
                {
                    blockUpdate = true;
                    oldZoom = _map.AbsoluteZoom;
                    GroupProjectMarkerBydistanceAndCategory(CategoryManager.instance.selectedProjectIds);
                }
            }
		}

        private void GetProjectsData(List<BookProjectData> filteredProjectData)
        {
            projectMarkers.Clear();
            StopCoroutine(WaitWebRequestAndCallNewOne());
            ClearProjectMarkers();
            SetProjectMarkers(filteredProjectData);
            Invoke("GetProjectsImageCover",0.25f); // wait so that project markers can be destroyed
        }

        public void SetProjectMarkers(List<Toolz.WebServices.BookProjectData> filteredProjectData)
        {
            for (int i=0; i<filteredProjectData.Count; i++)
            {
                GameObject instance = Instantiate(_markerPrefab);
                _location.x = filteredProjectData[i].longitude;
                _location.y = filteredProjectData[i].latitude;
                instance.transform.SetParent(theParent);
                instance.transform.localPosition = _map.GeoToWorldPosition(_location, true);
                instance.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
                instance.name = "projectMarker_"+ filteredProjectData[i].id.ToString();

                ProjectMarkerData projectMarker = instance.GetComponent<ProjectMarkerData>();
                projectMarkers.Add(projectMarker);
                projectMarker.ProcessData(filteredProjectData[i].id, _location, filteredProjectData[i]);

                _spawnedMarkers.Add(instance, _location);
            }

            //set toggles from canvas calques at start(for now all on)
            if (SetToggleAllOn)
            {
                //Debug.Log("MarkersManager, SetToggleAllOn true");
                SetToggleAllOn = false;
                metiersToggle.ForceOn(true);
                secteurMarcheToggle.ForceOn(true);
            }
            //else
            //{
            //    Debug.Log("MarkersManager, SetToggleAllOn false");
            //}
        }

        public void GetProjectsImageCover()
        {
            StartCoroutine(WaitWebRequestAndCallNewOne());
        }

        private IEnumerator WaitWebRequestAndCallNewOne()
        {
            //ProjectMarker[] projectMarkerArray = theParent.GetComponentsInChildren<ProjectMarker>(true);

            foreach (var marker in projectMarkers)
            {
                ProjectMarkerData project = marker.GetComponent<ProjectMarkerData>();
                if (project != null)
                {
                    _getImageProject.idProject = project.rawData.id;
                    _getImageProject.imageName = project.rawData.imageCover;
                    _getImageProject.onComplete += _getImageProject_onComplete;
                    _getImageProject.onError += _getImageProject_onError;
                    _getImageProject.UseWebService();
                    waitingWebRequest = true;
                    while (waitingWebRequest)
                    {
                        yield return null;
                    }
                    // Request on complete done
                    Texture2D sampleTexture = new Texture2D(8, 8);
                    project.imageCover = null;

                    if(_getImageProject.sprite!=null)
                        project.imageCover = _getImageProject.sprite;
                    else
                        Debug.Log("image Not loaded");
                }
            }
            yield return null;
            Debug.Log("All cover images downloaded");
        }

        private void ClearProjectMarkers()
        {
            foreach (KeyValuePair<GameObject, Vector2d> marker in _spawnedMarkers)
            {
                Destroy(marker.Key);
            }

            _spawnedMarkers.Clear();
        }

        private void CreatePoolOfGroupMarkerPrefab()
        {
            markerGroupsPOOL = new List<GameObject>();

            for (int i=0; i< POOLSIZE; i++)
            {
                GameObject instance = Instantiate(_groupMarkerPrefab);
                instance.transform.SetParent(theParent);
                instance.name = "projectMarker_Group_Pooled";
                instance.SetActive(false);

                markerGroupsPOOL.Add(instance);
            }
            //Debug.Log("markerGroupsPOOL.Count = " + markerGroupsPOOL.Count);
        }

        private GameObject GetGroupMarkerFromPool()
        {
            GameObject temp = null;

            if (markerGroupsPOOL.Count > 0)
            {
                temp = markerGroupsPOOL[0];
                markerGroupsPOOL.Remove(temp);
                //Debug.Log("temp is null ? " + temp);
            }
            else
            {
                //pool is empty so create new one to avoid null reference
                GameObject instance = Instantiate(_groupMarkerPrefab);
                instance.transform.SetParent(theParent);
                instance.SetActive(false);
                temp = instance;
            }

            return temp;
        }

        //needs to be called whenever the mapbox zoom changes <- TODO DEFINE VALUE
        public void GroupProjectMarkerBydistanceAndCategory(List<int> projectIds)
        {
            //create group list
            CreateProjectMarkerGroup(projectIds);

            //Debug.Log("markerGroups.Count = "+ markerGroups.Count);
            //instantiate group markers
            for (int i = 0; i < markerGroups.Count; i++)
            {
                GameObject instance = GetGroupMarkerFromPool();
                instance.SetActive(true);
                instance.transform.localPosition = _map.GeoToWorldPosition(_location, true);
                instance.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
                instance.name = "projectMarker_Group_" + (i+1).ToString();

                instance.transform.Find("Quad").Find("TextMeshPro").GetComponent<TMPro.TextMeshPro>().text = markerGroups[i].Count.ToString();

                _location.x = markerGroups[i][0].GetComponent<ProjectMarkerData>().latlong.x;
                _location.y = markerGroups[i][0].GetComponent<ProjectMarkerData>().latlong.y;

                instance.GetComponent<ProjectMarkerGroup>().subProjectList.Clear();
                for (int j = 0; j < markerGroups[i].Count; j++)
                {
                    instance.GetComponent<ProjectMarkerGroup>().subProjectList.Add(markerGroups[i][j].GetComponent<ProjectMarkerData>());

                    RemoveProjectMarkersInGroup(markerGroups[i][j]);
                }

                _spawnedMarkers.Add(instance, _location);
            }

            blockUpdate = false;
        }

        //create List of Project Marker Groups
        private void CreateProjectMarkerGroup(List<int> projectIds)
        {
            int inc = 0;

            if (markersGroupped.Count > 0)
            {
                ClearProjectMarkerGroups();

                foreach (GameObject go in markersGroupped)
                {
                    if (go != null)
                    {
                        go.SetActive(true);
                    }
                }
                markersGroupped.Clear();
            }
            
            if (markerDisabledfromCategory != null && markerDisabledfromCategory.Count > 0)
            {
                foreach (GameObject go in markerDisabledfromCategory)
                {
                    if (go != null)
                    {
                        go.SetActive(true);
                    }
                    
                }
                markerDisabledfromCategory.Clear();
            }

            markers.Clear();
            foreach (KeyValuePair<GameObject, Vector2d> marker in _spawnedMarkers)
            {
                markers.Add(marker.Key);
            }

            //Debug.Log("markers list :");
            //for (int i=0; i< markers.Count; i++)
            //{
            //    Debug.Log("markers["+ i +"].name = " + markers[i].name);
            //}

            if (projectIds != null)
            {
                if (projectIds.Count > 0)
                {
                    //remove non selected categories
                    for (int i = 0; i < markers.Count; i++)
                    {
                        if (markers[i].GetComponent<ProjectMarkerData>() != null)
                        {
                            if (!projectIds.Contains(markers[i].GetComponent<ProjectMarkerData>().rawData.id))
                            {
                                //Debug.Log(markers[i].GetComponent<ProjectMarkerData>().rawData.title + " is not selected by categories");
                                markerDisabledfromCategory.Add(markers[i]);
                                foreach (KeyValuePair<GameObject, Vector2d> marker in _spawnedMarkers)
                                {
                                    if (marker.Key.name == markers[i].name)
                                    {
                                        marker.Key.SetActive(false);
                                    }
                                }
                            }
                            else
                            {
                                //Debug.Log(markers[i].GetComponent<ProjectMarkerData>().rawData.title + " is selected");
                            }
                        }
                    }

                    foreach (GameObject go in markerDisabledfromCategory)
                    {
                        markers.Remove(go);
                    }
                }
                else
                {
                    markers.Clear();
                    markerDisabledfromCategory.Clear();
                    foreach (KeyValuePair<GameObject, Vector2d> marker in _spawnedMarkers)
                    {
                        markerDisabledfromCategory.Add(marker.Key);
                        marker.Key.SetActive(false);
                    }
                }
            }

            if (markers.Count > 0)
            {
                if (markerGroups.Count <= 0)
                {
                    markerGroups.Add(new List<GameObject>());
                }
                int counter = 0;
                for (int i = 0; i < markers.Count; i++)
                {
                    if (markers[0].activeInHierarchy && markers[i].activeInHierarchy)
                    {
                        //if it is not the same element
                        if (markers[0] != markers[i])
                        {
                            //they intersect
                            if (markers[0].GetComponent<BoxCollider>().bounds.Intersects(markers[i].GetComponent<BoxCollider>().bounds))
                            {
                                //Debug.Log(markers[0].name + " intersects with: " + markers[i].name);

                                if (!markerGroups[inc].Contains(markers[0]))
                                {
                                    markerGroups[inc].Add(markers[0]);
                                }
                                if (!markerGroups[inc].Contains(markers[i]))
                                {
                                    markerGroups[inc].Add(markers[i]);
                                }
                            }
                            else
                            {
                                //0 doesn't intersect with i
                                counter++;
                            }
                        }
                        else
                        {
                            //same element
                            counter++;
                        }
                    }
                }

                //previous marker[0] doesn't intersect with anything
                if (counter >= markers.Count)
                {
                    //Debug.Log(markers[0].name + " doesn't intersect with anything");
                    markers.Remove(markers[0]);
                }

                if (markerGroups[inc].Count > 0)
                {
                    for (int i = 0; i < markerGroups[inc].Count; i++)
                    {
                        markers.Remove(markerGroups[inc][i]);
                    }
                    inc++;
                    markerGroups.Add(new List<GameObject>());
                }
            }

            //Ugly coding but last loop generate one extra empty group so need to remove it
            for (int i = 0; i < markerGroups.Count; i++)
            {
                if (markerGroups[i].Count <= 0)
                {
                    markerGroups.Remove(markerGroups[i]);
                }
            }

            //only for debug
            //for (int i = 0; i < markerGroups.Count; i++)
            //{
            //    for (int j = 0; j < markerGroups[i].Count; j++)
            //    {
            //        Debug.Log(markerGroups[i][j].name + " in group: " + (i + 1).ToString());
            //    }
            //}
        }

        public void ClearProjectMarkerGroups()
        {
            List<GameObject> temp = new List<GameObject>();
            foreach (KeyValuePair<GameObject, Vector2d> marker in _spawnedMarkers)
            {
                if (marker.Key.GetComponent<ProjectMarkerGroup>() != null)
                {
                    temp.Add(marker.Key);
                    ReturnGroupMarkerToPool(marker.Key);
                }
            }

            foreach (GameObject go in temp)
            {
                _spawnedMarkers.Remove(go);
            }
            
            markerGroups.Clear();
        }

        private void ReturnGroupMarkerToPool(GameObject grpMarker)
        {
            grpMarker.name = "projectMarker_Group_Pooled";
            grpMarker.SetActive(false);
            markerGroupsPOOL.Add(grpMarker);
        }

        //set markers that have been put in a group to false
        private void RemoveProjectMarkersInGroup(GameObject grouppedMaker)
        {
            foreach (KeyValuePair<GameObject, Vector2d> marker in _spawnedMarkers)
            {
                if (marker.Key == grouppedMaker)
                {
                    marker.Key.SetActive(false);
                    markersGroupped.Add(marker.Key);
                    break;
                }

            }
        }

        // TODO : display error 
        private void _getImageProject_onError(long status, string message)
        {
            waitingWebRequest = false;
            _getImageProject.onComplete -= _getImageProject_onComplete;
            _getImageProject.onError -= _getImageProject_onError;
            Debug.Log("_getImageProject_onError "+ _getImageProject.imageName + " " + status + " : " + message);
        }

        private void _getImageProject_onComplete(long status, string message)
        {
            waitingWebRequest = false;
            _getImageProject.onComplete -= _getImageProject_onComplete;
            _getImageProject.onError -= _getImageProject_onError;
            //Debug.Log("_getImageProject_onComplete " + status + " : " + " id " + _getImageProject.idProject + " " + _getImageProject.image.Length);
        }

        private void _getBookProjectsbyCompanyID_onComplete(long status, string message)
        {
            //Debug.Log("Get Age _getBookProjectsbyCompanyID_onComplete OnComplete "/* + status + " : " + message */);
            //Debug.Log( FilteredProjectData.Count);
            _getBookProjectsbyCompanyID.onComplete -= _getBookProjectsbyCompanyID_onComplete;
            _getBookProjectsbyCompanyID.onError -= _getBookProjectsbyCompanyID_onError;
            GetProjectsData(_getBookProjectsbyCompanyID.projectDataList); // Pass data to spawn on map book
                                                                          //Debug.Log( _getBookProject.bookData.ToString());
            if (refreshCategories)
            {
                refreshCategories = false;
                CategoryManager.instance.SendRequest();
            }
        }

        private void _getBookProjectsbyCompanyID_onError(long status, string message)
        {
            _getBookProjectsbyCompanyID.onComplete -= _getBookProjectsbyCompanyID_onComplete;
            _getBookProjectsbyCompanyID.onError -= _getBookProjectsbyCompanyID_onError;
            Debug.Log("_getBookProjectsbyCompanyID_onError " + status + " : " + message);
        }


        public void GetProjectsImages(ProjectMarkerData projectMarker)
        {
            StartCoroutine(WaitWebRequestAndCallNewOne(projectMarker));
        }

        private IEnumerator WaitWebRequestAndCallNewOne(ProjectMarkerData projectMarker)
        {
            foreach (string image in projectMarker.rawData.images)
            {
                if (image != null)
                {
                    _getImageProject.idProject = projectMarker.rawData.id;
                    _getImageProject.imageName = projectMarker.rawData.imageCover;
                    _getImageProject.onComplete += _getImageProject_onComplete;
                    _getImageProject.onError += _getImageProject_onError;
                    _getImageProject.UseWebService();
                    waitingWebRequest = true;
                    while (waitingWebRequest)
                    {
                        yield return null;
                    }

                    if (_getImageProject.sprite!=null)
                    {
                        projectMarker.images.Add(_getImageProject.sprite);
                    }
                    else
                        Debug.Log("error getting image");
                    //// Request on complete done
                    //Texture2D sampleTexture = new Texture2D(2, 2);
                    //// the size of the texture will be replaced by image size
                    //bool isLoaded = sampleTexture.LoadImage(_getImageProject.image);

                    //if (isLoaded)
                    //{
                    //    Sprite sprite = Sprite.Create(sampleTexture, new Rect(0, 0, sampleTexture.width, sampleTexture.height), new Vector2(1, 1));
                    //    projectMarker.images.Add(sprite);
                    //}
                }
            }
            yield return null;
            Debug.Log("All cover images downloaded");
        }
    }
}