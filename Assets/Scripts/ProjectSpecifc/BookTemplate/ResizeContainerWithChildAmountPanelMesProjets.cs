﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResizeContainerWithChildAmountPanelMesProjets : MonoBehaviour {

	[SerializeField]
	private bool addOffet = true;
	private float containerSize;
    private int column = 0;
    private int row = 0;
    private float childHeight = 0f;
    private float childWidth = 0f;

    private void Start () {

    }

    public void StartResize()
    {
        //used when there is a content size fitter
        StartCoroutine("Delay");
    }

    IEnumerator Delay()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        GetChildrensAndResize();
        StopCoroutine("Delay");
    }

    public void GetChildrensAndResize()
    {
		containerSize = 0f;
        row = 0;
        column = 0;
        childHeight = 0f;
        childWidth = 0f;

        GetColumnAndRow(GetComponent<GridLayoutGroup>(), out column, out row, out childHeight, out childWidth);

        containerSize = row * childHeight + (row-1) * GetComponent<GridLayoutGroup>().spacing.y;

        containerSize += GetComponent<GridLayoutGroup>().padding.top;
        containerSize += GetComponent<GridLayoutGroup>().padding.bottom;

        if (addOffet)
        {
			containerSize += 30f;
		}
		GetComponent<RectTransform> ().sizeDelta = new Vector2 (GetComponent<RectTransform> ().sizeDelta.x, containerSize);
        GetComponent<RectTransform>().anchoredPosition = new Vector2(GetComponent<RectTransform>().anchoredPosition.x, 0f);

    }

    void GetColumnAndRow(GridLayoutGroup glg, out int column, out int row, out float childHeight, out float childWidth)
    {
        column = 0;
        row = 0;
        childHeight = 0f;
        childWidth = 0f;

        if (glg.transform.childCount == 0)
            return;

        //Column and row are now 1
        column = 1;
        row = 1;

        //Get the first child GameObject of the GridLayoutGroup
        RectTransform firstChildObj = glg.transform.GetChild(0).GetComponent<RectTransform>();

        //get height
        childHeight = firstChildObj.sizeDelta.y;

        //get width
        childWidth = firstChildObj.sizeDelta.x;

        Vector3 firstChildPos = firstChildObj.localPosition;

        bool stopCountingcolumn = false;

        //Loop through the rest of the child object
        for (int i = 1; i < glg.transform.childCount; i++)
        {
            //Get the next child
            RectTransform currentChildObj = glg.transform.GetChild(i).GetComponent<RectTransform>();

            Vector3 currentChildPos = currentChildObj.localPosition;

            if (firstChildPos.y == currentChildPos.y)
            {
                if(!stopCountingcolumn)
                column++;

            }
            else
            {
                firstChildPos = currentChildPos;
                stopCountingcolumn = true;
                row++;
            }
        }
        //Debug.Log("row = "+ row);
        //Debug.Log("column = " + column);
    }
}
