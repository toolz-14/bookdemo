﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelGroupedMarkerLerp : MonoBehaviour {

    private Vector2 _from1;
    private Vector2 _to1;
    private bool _launch;
    private float _timeSinceTransitionStarted;
    private float _t;
    private float Duration1 = 0.2f;

    private Vector2 _from2;
    private Vector2 _to2;
    private bool _launch2;
    private float _timeSinceTransitionStarted2;
    private float _t2;
    private float Duration2 = 0.2f;

    private Vector2 _startPos;
    public static bool isOpen;

    private void Start()
    {
        _startPos = GetComponent<RectTransform>().anchoredPosition;
    }

    private void Update()
    {
        if (_launch)
        {
            OpenPanel();
        }
        if (_launch2)
        {
            ClosePanel();
        }
    }

    public void StartLerpOpen()
    {
        if (!isOpen)
        {
            _timeSinceTransitionStarted = 0f;
            _from1 = GetComponent<RectTransform>().anchoredPosition;
            _to1 = new Vector2(0f, GetComponent<RectTransform>().anchoredPosition.y);
            _launch = true;
        }
    }

    private void OpenPanel()
    {
        _timeSinceTransitionStarted += Time.deltaTime;
        _t = _timeSinceTransitionStarted / Duration1;

        GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
        if (Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f)
        {
            _launch = false;
            isOpen = true;
        }
    }

    public void StartLerpClose()
    {
        if (isOpen)
        {
            _timeSinceTransitionStarted2 = 0f;
            _from2 = GetComponent<RectTransform>().anchoredPosition;
            _to2 = _startPos;
            _launch2 = true;
        }
    }

    private void ClosePanel()
    {
        _timeSinceTransitionStarted2 += Time.deltaTime;
        _t2 = _timeSinceTransitionStarted2 / Duration2;

        GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
        if (Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to2) < 0.01f)
        {
            _launch2 = false;
            isOpen = false;
            gameObject.SetActive(false);
        }
    }
}
