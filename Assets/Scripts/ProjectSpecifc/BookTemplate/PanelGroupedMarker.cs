﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelGroupedMarker : MonoBehaviour {

    [SerializeField]
    private GameObject ProjectItem;
    public Transform ProjectItemParent;
    [SerializeField]
    private DetectClickOnProjectMarker detectClickOnProjectMarker;

    private List<GameObject> ProjectItemsList = new List<GameObject>();
    private const float timeOut = 10f;

    public void SetAnItem(ProjectMarkerData projectMarker)
    {
        GameObject instance = Instantiate(ProjectItem);
        instance.SetActive(true);
        instance.transform.SetParent(ProjectItemParent);
        instance.transform.localScale = Vector3.one;

        //Debug.Log("PanelGroupedMarker, SetAnItem() projectMarker = " + projectMarker);
        instance.transform.Find("Text_NomProjet").GetComponent<Text>().text = projectMarker.rawData.title;
        instance.transform.Find("Text_AdresseProjet").GetComponent<Text>().text = projectMarker.rawData.address;
        
        Sprite temp = null;
        if (projectMarker.imageCover != null)
        {
            temp = projectMarker.imageCover;
            instance.transform.Find("ImageHolder").Find("Image").GetComponent<Image>().sprite = temp;
            Destroy(instance.transform.Find("ImageHolder").Find("Image_Loader").gameObject);
        }
        else
        {
            StartCoroutine(FetchImage(instance, projectMarker));
        }

        instance.transform.Find("HolderButtonGoToProject").Find("Button_GoToProject").GetComponent<Button>().onClick.AddListener(() =>
        {
            GetComponent<PanelGroupedMarkerLerp>().StartLerpClose();
            detectClickOnProjectMarker.GoToAProject(projectMarker);
        });

        ProjectItemsList.Add(instance);
    }


    private IEnumerator FetchImage(GameObject instance, ProjectMarkerData projectMarker)
    {
        float timeSpend = 0f;

        while (timeSpend<timeOut) // if sprite in cover image is null
        {
            if(projectMarker.imageCover == null)
            {
                timeSpend++;
                Debug.Log("fetching....");
                yield return new WaitForSeconds(1f);
            }
            else
            {
                instance.transform.Find("ImageHolder").Find("Image").GetComponent<Image>().sprite = projectMarker.imageCover;
                Destroy(instance.transform.Find("ImageHolder").Find("Image_Loader").gameObject);
                timeSpend = timeOut;
            }
            yield return null;
        }

        if (projectMarker.imageCover == null)
        {
            // TODO : failed to load image, retry later
        }
    }

    public void ClearProjectItemsList()
    {
        if (ProjectItemsList != null && ProjectItemsList.Count > 0)
        {
            foreach (GameObject go in ProjectItemsList)
            {
                Destroy(go);
            }

            ProjectItemsList.Clear();
        }
    }
}
