﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetView360 : MonoBehaviour {

    [SerializeField]
    private Texture chosen360;

    [SerializeField]
    private Material mat360;

    private void Start () {
		
	}

    public void SetThe360()
    {
        mat360.mainTexture = chosen360;
    }
}
