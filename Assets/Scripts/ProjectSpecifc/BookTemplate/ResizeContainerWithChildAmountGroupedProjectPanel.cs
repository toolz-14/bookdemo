﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResizeContainerWithChildAmountGroupedProjectPanel : MonoBehaviour {

    [SerializeField]
    private bool addOffet = true;
    private float containerSize;

    private void Start()
    {
        StartCoroutine("Delay");
    }

    private void OnEnable()
    {
        StartCoroutine("Delay");
    }

    public void StartResize()
    {
        StartCoroutine("Delay");
    }

    IEnumerator Delay()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        GetChildrensAndResize();
        StopCoroutine("Delay");
    }

    public void GetChildrensAndResize()
    {
        containerSize = 0f;
        foreach (Transform trans in transform)
        {
            if (trans.gameObject.activeInHierarchy)
            {
                containerSize += trans.GetComponent<RectTransform>().sizeDelta.y + GetComponent<VerticalLayoutGroup>().spacing;
            }
        }
        //		Debug.Log ("GetChildrensAndResize, containerSize = "+containerSize);

        containerSize += GetComponent<VerticalLayoutGroup>().padding.top;
        containerSize += GetComponent<VerticalLayoutGroup>().padding.bottom;

        if (addOffet)
        {
            containerSize += 30f;
        }
        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, containerSize);
    }
}
