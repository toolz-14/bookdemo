using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class ProjectHolder : MonoBehaviour {

    [SerializeField]
    private Image imageCover;
    [SerializeField]
    private Text title; 
    [SerializeField]
    private Text address;
    [SerializeField]
    private Text published; // status
    [SerializeField]
    private Text tags;
    [SerializeField]
    private GameObject imageLoading;
    private ProjectMarkerData projectData;
    private Coroutine loadImg;

    public void FillData(ProjectMarkerData _projectData)
    {
        projectData = _projectData;
        if (projectData.imageCover != null)
        {
            imageCover.sprite = projectData.imageCover;
            Destroy(imageLoading);
        }
        else
            loadImg = StartCoroutine(LoadCoverImage());
        title.text = projectData.rawData.title;
        address.text = projectData.rawData.address;
        published.text = (projectData.rawData.status == 0) ? "Non publié" : "Publié";
        tags.text = projectData.rawData.tags;

    }

    private IEnumerator LoadCoverImage()
    {
        while (projectData.imageCover==null)
        {
            Debug.Log("image waiting");
            yield return null;
        }

        Debug.Log("image loaded");
        imageCover.sprite = projectData.imageCover;
        Destroy(imageLoading);
    }

    private void OnDestroy()
    {
        if (loadImg != null)
            StopCoroutine(loadImg);
    }
}
