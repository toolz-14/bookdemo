﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMessage : MonoBehaviour {

    [SerializeField]
    private GameObject panel_DisplayPopUP; // handle data
    [SerializeField]
    private Text textMessage; // handle data

    public void SetMessage(string message)
    {
        textMessage.text = message; 
    }

    public void Open()
    {
        panel_DisplayPopUP.SetActive(true);
    }

    public void Close()
    {
        panel_DisplayPopUP.SetActive(false);
    }
}
