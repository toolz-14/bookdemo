﻿using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChapellePosition : MonoBehaviour {

    private AbstractMap _map;

    Vector3 PosAtScale1 = new Vector3(160.4f, 35, 177.35f);
    Vector3 PosAtScale2 = new Vector3(102.6f, 35, 396.9f);

    Vector3 EulerAtScale1 = new Vector3(0f, 179.5f, 0f);
    Vector3 EulerAtScale2 = new Vector3(0f, 179.5f, 0f);

    Vector3 ScaleAtScale1 = new Vector3(1.426823f, 1.426827f, 1.426827f);
    Vector3 ScaleAtScale2 = new Vector3(1.426823f, 1.426827f, 1.426827f);

    private void Update()
    {
        _map = FindObjectOfType<AbstractMap>();
        if (_map.AbsoluteZoom >= 16)
        {
            transform.localPosition = PosAtScale1;
            transform.localEulerAngles = EulerAtScale1;
            transform.localScale = ScaleAtScale1;
        }
        else if (_map.AbsoluteZoom <= 15)
        {
            transform.localPosition = PosAtScale2;
            transform.localEulerAngles = EulerAtScale2;
            transform.localScale = ScaleAtScale2;
        }
    }
}
