﻿using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;

// Processed data from BookProjectData
public class ProjectMarkerData : MonoBehaviour {

    public Vector2d latlong;
    public BookProjectData rawData;
    public Vector3 cameraPos;
    public Vector3 cameraRot;
    public bool cameraPosRotValid;
    public bool noFpsIconCam;
    public bool imagesDownloaded;

    public List<Vector3> iconPos;
    public List<Vector3> playerPos;
    public List<Vector3> playerRot;

    public List<Sprite> images;
    public int imageCoverIndex;
    public Sprite imageCover;
    
    // TODO PULL IMAGES, VIDEOS, 3D
    public void ProcessData(int _projectID, Vector2d _latlong, BookProjectData _rawData)
    {
        latlong = _latlong;
        rawData = _rawData;

        // TODO CHECK IF rawData.cameraPosRot is in the correct format

        cameraPosRotValid = false;
        if (rawData.cameraPosRot.Contains("_"))
        {
            string[] temp = rawData.cameraPosRot.Split('_');
            if (temp.Length == 6)
            {
                float[] proccessedData = new float[6];
                bool resultValidParse = false;
                for (int i=0;i<temp.Length; i++)
                {
                    resultValidParse = float.TryParse(temp[i], out proccessedData[i]);
                    if (!resultValidParse) // if parse failed get out of the loop
                        break;
                }

                if (resultValidParse) // all parses have succeed
                {
                    cameraPos = new Vector3(proccessedData[0], proccessedData[1], proccessedData[2]);
                    cameraRot = new Vector3(proccessedData[3], proccessedData[4], proccessedData[5]);
                    cameraPosRotValid = true;
                }
            }
        }

        int counter = 0;
        foreach (KeyValuePair<string, string> fpsCameraData in rawData.fpsCamera)
        {
            string[] temp;

            // icon hostpot
            string hostpotFpsData = fpsCameraData.Key;
            if (hostpotFpsData.Contains("_"))
            {
                temp = hostpotFpsData.Split('_');
                if (temp.Length == 3)
                {
                    float[] proccessedData = new float[3];
                    bool resultValidParse = false;
                    for (int i = 0; i < temp.Length; i++)
                    {
                        resultValidParse = float.TryParse(temp[i], out proccessedData[i]);
                        if (!resultValidParse) // if parse failed get out of the loop
                            break;
                    }

                    if (resultValidParse) // all parses have succeed
                    {
                        Vector3 pos = new Vector3(proccessedData[0], proccessedData[1], proccessedData[2]);
                        iconPos.Add(pos);
                    }
                    else
                    {
                        Debug.LogWarning("Error Hotspot fps resultValidParse camera not valid : " + counter);
                        iconPos.Add(Vector3.zero);
                    }
                }
                else
                {
                    Debug.LogWarning("Error Hotspot fps Length camera not valid : " + counter);
                    iconPos.Add(Vector3.zero);
                }
            }
            else
                Debug.LogWarning("Error Hotspot fps camera not valid : " + counter);

            // fps Camera
            string fpsCameraPosRot = fpsCameraData.Value;
            if (fpsCameraPosRot.Contains("_"))
            {
                temp = fpsCameraPosRot.Split('_');
                if (temp.Length == 6)
                {
                    float[] proccessedData = new float[6];
                    bool resultValidParse = false;
                    for (int i = 0; i < temp.Length; i++)
                    {
                        resultValidParse = float.TryParse(temp[i], out proccessedData[i]);
                        if (!resultValidParse) // if parse failed get out of the loop
                            break;
                    }

                    if (resultValidParse) // all parses have succeed
                    {
                        Vector3 pos = new Vector3(proccessedData[0], proccessedData[1], proccessedData[2]);
                        Vector3 rot = new Vector3(proccessedData[3], proccessedData[4], proccessedData[5]);
                        playerPos.Add(pos);
                        playerRot.Add(rot);
                    }
                    else
                    {
                        Debug.LogWarning("Error fpsCameraPosRot fps resultValidParse camera not valid : " + counter);
                        playerPos.Add(Vector3.zero);
                        playerRot.Add(Vector3.zero);
                    }
                }
                else
                {
                    Debug.LogWarning("Error fpsCameraPosRot fps Length camera not valid : " + counter);
                    playerPos.Add(Vector3.zero);
                    playerRot.Add(Vector3.zero);
                }
            }
            else
                Debug.LogWarning("Error fpsCameraPosRot fps camera not valid : " + counter);

            counter++;
        }

    }


    // TODO PULL IMAGES, VIDEOS, 3D

}
