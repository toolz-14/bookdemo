﻿using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;

public class CategoryManager : MonoBehaviour {

    private List<int> CategoriesSelected = new List<int>();
    [HideInInspector]
    public List<int> selectedProjectIds = new List<int>();
    public static CategoryManager instance;
    private bool shouldWait;
    [SerializeField]
    private Mapbox.Examples.MarkersManager markersManager;
    // Webservice
    private GetIdProjectByCategories _getIdProjectByCategories;

    private void Awake()
    {
        // singleton
        if (instance == null)
        {
            instance = this;
            GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
            _getIdProjectByCategories = webService.GetComponent<GetIdProjectByCategories>();
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void UpdateCategoryList(int value, bool adding)
    {
        if (adding)
        {
            //Debug.Log("ADDING: " + value);
            CategoriesSelected.Add(value);
        }
        else
        {
            //Debug.Log("REMOVING: " + value);
            List<int> temp = new List<int>();
            for (int i=0; i< CategoriesSelected.Count; i++)
            {
                if (CategoriesSelected[i] == value)
                {
                    temp.Add(value);
                }
            }

            foreach(int integer in temp)
            {
                CategoriesSelected.Remove(integer);
            }
        }

        //if (!shouldWait)
        //{
        //    if (CategoriesSelected != null && CategoriesSelected.Count > 0)
        //    {
        //        shouldWait = true;
        //        Invoke("Delayed", 0.2f);
        //    }
        //    else
        //        Debug.Log("No categories selected");
        //}

        if (CategoriesSelected != null && CategoriesSelected.Count > 0)
        {
            SendRequest();
        }
        else
        {
            selectedProjectIds.Clear();
            markersManager.GroupProjectMarkerBydistanceAndCategory(selectedProjectIds);
        }
    }

    //private void Delayed()
    //{
    //    //Debug.Log("SendRequest: ");
    //    SendRequest();
    //}

    public void SendRequest()
    {
        _getIdProjectByCategories.categoriesSelected = CategoriesSelected;
        _getIdProjectByCategories.onComplete += _getIdProjectByCategories_onComplete;
        _getIdProjectByCategories.onError += _getIdProjectByCategories_onError;

        _getIdProjectByCategories.UseWebService();
    }

    // WEB SERVICES CALLBACKS
    private void _getIdProjectByCategories_onComplete(long status, string message)
    {
        _getIdProjectByCategories.onComplete -= _getIdProjectByCategories_onComplete;
        _getIdProjectByCategories.onError -= _getIdProjectByCategories_onError;
        shouldWait = false;

        selectedProjectIds.Clear();
        for (int i = 0; i < _getIdProjectByCategories.projectIds.Count; i++)
        {
            selectedProjectIds.Add(_getIdProjectByCategories.projectIds[i]);
        }
        markersManager.GroupProjectMarkerBydistanceAndCategory(selectedProjectIds);
    }

    private void _getIdProjectByCategories_onError(long status, string message)
    {
        Debug.Log("_getIdProjectByCategories_onError" + status +" "+ message);
        _getIdProjectByCategories.onComplete -= _getIdProjectByCategories_onComplete;
        _getIdProjectByCategories.onError -= _getIdProjectByCategories_onError;
        shouldWait = false;
    }

}
