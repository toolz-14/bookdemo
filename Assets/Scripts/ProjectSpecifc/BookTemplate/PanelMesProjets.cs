﻿using Mapbox.Examples;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

// TODO CHECK USER INPUT (FOR LAT LONG)
public class PanelMesProjets : MonoBehaviour {
    
    [SerializeField]
    private DetectClickOnProjectMarker detectClickOnProjectMarker;
    [SerializeField]
    private PanelMesProjetsLerp panelMesProjetsLerp;
    [SerializeField]
    private GameObject loadingText;
    [SerializeField]
    private GameObject myProjectsList;
    [SerializeField]
    private Transform projectHolderParent;
    [SerializeField]
    private GameObject projectHolderPrefab;
    [SerializeField]
    private MarkersManager markersManager; // handle data
    [SerializeField]
    private SupprimerProjet supprimerProjet;
    private PanelCreerModifierProjet panelCreerModifierProjet;

    private List<GameObject> projectHolderPrefabs;
    private ResizeContainerWithChildAmountPanelMesProjets resizeScript;


    private void Start ()
    {
        resizeScript = myProjectsList.transform.Find("Viewport").Find("Content").GetComponent<ResizeContainerWithChildAmountPanelMesProjets>();

        myProjectsList.SetActive(false);
        loadingText.SetActive(true);

        if (projectHolderPrefabs == null)
            projectHolderPrefabs = new List<GameObject>();

        panelCreerModifierProjet = GetComponent<PanelCreerModifierProjet>();
    }

    public void OpenPanel()
    {
        SetProjectsData();
    }

    public void ClosePanel()
    {
        if (projectHolderPrefabs == null)
            projectHolderPrefabs = new List<GameObject>();
        else
        {
            //clear list
            foreach (GameObject prefab in projectHolderPrefabs)
            {
                Destroy(prefab);
            }
            projectHolderPrefabs.Clear();
        }

        myProjectsList.SetActive(false);
        loadingText.SetActive(true);
        panelMesProjetsLerp.StartLerpClose();
    }

    private void SetProjectsData()
    {
        List<ProjectMarkerData> _projectMarkers = markersManager.projectMarkers;
        myProjectsList.SetActive(true);
        loadingText.SetActive(false);

        foreach (ProjectMarkerData projectData in _projectMarkers)
        {
            GameObject tempPrefabHolder = Instantiate(projectHolderPrefab, projectHolderParent);
            projectHolderPrefabs.Add(tempPrefabHolder);
            tempPrefabHolder.GetComponent<ProjectHolder>().FillData(projectData);

            tempPrefabHolder.transform.Find("Image_Projet").GetComponent<Button>().onClick.AddListener(() =>
            {
                detectClickOnProjectMarker.GoToAProject(projectData);
                ClosePanel();
            });

            User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>();
            bool enableButtons = (user.userStatus == (int)ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) ? true : false;

            if (enableButtons)
            {
                tempPrefabHolder.transform.Find("Image_Projet").Find("Button_Delete").GetComponent<Button>().onClick.AddListener(() =>
                {
                    supprimerProjet.DeleteProjectButton(projectData.rawData.id, this.gameObject);
                });
                tempPrefabHolder.transform.Find("Image_Projet").Find("Button_Edit").GetComponent<Button>().onClick.AddListener(() =>
                {
                    panelCreerModifierProjet.ModifyProject(projectData);
                });
            }
            else
            {
                tempPrefabHolder.transform.Find("Image_Projet").Find("Button_Delete").transform.gameObject.SetActive(false);
                tempPrefabHolder.transform.Find("Image_Projet").Find("Button_Edit").transform.gameObject.SetActive(false);
            }
        }

        resizeScript.StartResize();
    }

}
