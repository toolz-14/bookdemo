using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelTopRightMenu : MonoBehaviour {

    [SerializeField]
    private PanelMesSelectionsLerp PMSMenu;
    [SerializeField]
    private PanelMesProjetsLerp MPMenu;
    [SerializeField]
    private Mapbox.Examples.MarkersManager markersManager;
    [SerializeField]
    private GameObject loginPanel;

    private bool doOnce;

    private void Start () {
        doOnce = false;
    }

    public void SelectionButton()
    {
        if (!doOnce)
        {
            PMSMenu.transform.localScale = Vector3.zero;
            PMSMenu.gameObject.SetActive(true);
            PMSMenu.StartLerpOpen();
            Invoke("Delayed", 2f);
        }
    }


    public void MesProjetsButton()
    {
        if (!doOnce)
        {
            MPMenu.transform.localScale = Vector3.zero;
            MPMenu.gameObject.SetActive(true);
            MPMenu.StartLerpOpen();
            Invoke("Delayed", 2f);
        }
    }

    public void LogoutButton()
    {
        if (!doOnce)
        {
            GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>().LogOff();
            loginPanel.SetActive(true);
            markersManager.ClearProjectMarkerGroups();
            markersManager.markersGroupped.Clear();
            loginPanel.GetComponent<LoaderProfileUI>().ResetAlpha();
            loginPanel.GetComponent<LoaderProfileUI>().SwapView(0);
            Invoke("Delayed", 2f);
        }
    }

    private void Delayed()
    {
        doOnce = false;
    }

}
