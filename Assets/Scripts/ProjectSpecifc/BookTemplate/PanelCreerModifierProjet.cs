﻿using Mapbox.Examples;
using System;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class PanelCreerModifierProjet : MonoBehaviour
{

    [SerializeField]
    private GameObject buttonCreateProject;

    [SerializeField]
    private PanelCreerProjetLerp CPMenu;

    [SerializeField]
    private List<Image> etapesImages = new List<Image>();

    [SerializeField]
    private List<GameObject> etapesPanels = new List<GameObject>();

    [SerializeField]
    private List<GameObject> bottomButtons = new List<GameObject>();

    // WEBSERVICE
    private PostBookProject _postBookProject;
    private PatchBookProject _patchBookProject;
    private bool updatingData; // Patch request in progress, need to wait end of updating data 

    [SerializeField]
    private Text panelTitle;
    [Header("Panel 1")]
    [SerializeField]
    private InputField title;
    [SerializeField]
    private InputField address;
    [SerializeField]
    private InputField description;
    [SerializeField]
    private InputField mission;
    [SerializeField]
    private InputField estimation;
    [SerializeField]
    private InputField stake;

    [Header("Panel 2")]
    [SerializeField]
    private InputField approach;
    [SerializeField]
    private InputField solution;
    [SerializeField]
    private InputField clientName;
    [SerializeField]
    private InputField completionDate;
    [SerializeField]
    private InputField tags;

    [Header("Panel 3")]
    [SerializeField]
    private GameObject panelLoader;
    [SerializeField]
    DisplayMessage displayMessage;
    [SerializeField]
    private PanelCreerProjetEtape3 panelCreerProjetEtape3;
    [SerializeField]
    private InputField longitude;
    [SerializeField]
    private InputField latitude;
    [HideInInspector]
    public string coverImageName;


    [SerializeField]
    private MarkersManager markersManager;


    private int projectID;
    private ProjectMarkerData projectData; // get data when modifying
    private PanelMesProjets panelMesProjets;

    // WEBSERVICE
    private PostImageProject _postImageProject;
    private PostVideoProject _postVideoProject;
    private DeleteImageProject _deleteImageProject;

    private Coroutine reloadDataCoroutine;

    private void Start()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");

        _postBookProject = webService.GetComponent<PostBookProject>();
        _postBookProject.onComplete += _postBookProject_onComplete;
        _postBookProject.onError += _postBookProject_onError;

        _patchBookProject = webService.GetComponent<PatchBookProject>();

        _postImageProject = webService.GetComponent<PostImageProject>();
        _postImageProject.onComplete += _postImageProject_onComplete;
        _postImageProject.onError += _postImageProject_onError;

        _deleteImageProject = webService.GetComponent<DeleteImageProject>();

        _postVideoProject = webService.GetComponent<PostVideoProject>();
        _postVideoProject.onComplete += _postVideoProject_onComplete;
        _postVideoProject.onError += _postVideoProject_onError;

        panelCreerProjetEtape3.AddaDropDownItem(false);

        LoaderProfileUI.getUserAccess += HideCreateProjectButton;

        panelMesProjets = GetComponent<PanelMesProjets>();
    }

    public void HideCreateProjectButton(int userStatus)
    {
        if (userStatus == (int)ApplicationLogin.ApplicationLoginStatus.USER_ADMIN)
            buttonCreateProject.SetActive(true);
        else
            buttonCreateProject.SetActive(false);
    }

    public void CreateNewProject()
    {
        panelTitle.text = "Créer un projet";
        CPMenu.transform.localScale = Vector3.zero;
        CPMenu.gameObject.SetActive(true);
        CPMenu.StartLerpOpen();


        etapesPanels[0].SetActive(true);
        etapesPanels[1].SetActive(false);
        etapesPanels[2].SetActive(false);

        bottomButtons[0].SetActive(true);
        bottomButtons[1].SetActive(false);
        bottomButtons[2].SetActive(false);

        etapesImages[0].enabled = true;
        etapesImages[1].enabled = false;
        etapesImages[2].enabled = false;

        projectID = -1;
        // CREATE an empty project
        _postBookProject.UseWebService();
    }

    // 1st panel submit
    public void ButtonSuivant_1()
    {
        // UPDATE a project with id
        if (projectID != -1)
            Save1stForm();
        else
            Debug.LogWarning("post project has failed, no valid project ID");

        etapesImages[1].enabled = true;

        bottomButtons[0].SetActive(false);
        bottomButtons[1].SetActive(true);

        etapesPanels[0].SetActive(false);
        etapesPanels[1].SetActive(true);
    }

    public void ButtonSuivant_2()
    {
        if (projectID != -1)
            Save2dForm();
        etapesImages[2].enabled = true;

        bottomButtons[1].SetActive(false);
        bottomButtons[2].SetActive(true);

        etapesPanels[1].SetActive(false);
        etapesPanels[2].SetActive(true);
    }

    public void ButtonPrecedent_1()
    {
        if (projectID != -1)
            Save2dForm();
        etapesImages[1].enabled = false;

        bottomButtons[0].SetActive(true);
        bottomButtons[1].SetActive(false);

        etapesPanels[0].SetActive(true);
        etapesPanels[1].SetActive(false);
    }

    public void ButtonPrecedent_2()
    {
        //Debug.Log("ButtonPrecedent_2 projectID " + projectID);
        if (projectID != -1)
        {
            Save3dForm(); // always save as draft in create new project panel
        }

        etapesImages[2].enabled = false;

        bottomButtons[1].SetActive(true);
        bottomButtons[2].SetActive(false);

        etapesPanels[1].SetActive(true);
        etapesPanels[2].SetActive(false);
    }

    public void ButtonSauver()
    {
        if (projectID != -1)
        {
            Save3dForm();
        }
        
        if (projectData != null)
        {
            projectData = null;
            panelMesProjets.ClosePanel();
        }

        CloseProject();
    }


    public void ButtonPublier()
    {
        // TODO CHECK ALL FIELDS
        if (panelCreerProjetEtape3.CheckPanel3Requirements()) // all Mandatory data is filled
        {
            //Debug.Log("ButtonPublier, projectID " + projectID);
            if (projectID != -1)
            {
                Save3dForm(1);
            }

            if (projectData != null)
            {
                projectData = null;
                panelMesProjets.ClosePanel();
            }

            CloseProject();
        }
        else
        {
            Debug.Log("all Mandatory data is not filled ");
        }
    }

    // SET PROJECT
    public void CloseProject()
    {
        ClearPanel1();
        ClearPanel2();
        ClearPanel3();

        if (etapesPanels[2].activeSelf)
        {
            Debug.Log("Clear panel 3");
            panelCreerProjetEtape3.ClearPanel();
        }
        CPMenu.StartLerpClose();

        // TODO Must be only called in ButtonPublier
        if (reloadDataCoroutine == null)
            reloadDataCoroutine = StartCoroutine(UpdateProjectsData());
        else
            Debug.Log("ERROR Coroutines is still waiting");
    }

    private void ClearPanel1()
    {
        title.text = "";
        address.text = "";
        description.text = "";
        mission.text = "";
        estimation.text = "";
        stake.text = "";
    }

    private void ClearPanel2()
    {
        approach.text = "";
        solution.text = "";
        clientName.text = "";
        completionDate.text = "";
        tags.text = "";
    }

    private void ClearPanel3()
    {
        /// Clear panel 3
        longitude.text = "";
        latitude.text = "";
    }

    private IEnumerator UpdateProjectsData()
    {
        while (updatingData)
        {
            yield return null;
        }
        markersManager.Refresh();
        reloadDataCoroutine = null;
        Debug.Log("markers refreshed");
    }
    
    public void ModifyProject(ProjectMarkerData _projectData)
    {
        panelTitle.text = "Modifier un projet";

        CPMenu.transform.localScale = Vector3.zero;
        CPMenu.gameObject.SetActive(true);
        CPMenu.StartLerpOpen();

        etapesPanels[0].SetActive(true);
        etapesPanels[1].SetActive(false);
        etapesPanels[2].SetActive(false);

        bottomButtons[0].SetActive(true);
        bottomButtons[1].SetActive(false);
        bottomButtons[2].SetActive(false);

        etapesImages[0].enabled = true;
        etapesImages[1].enabled = false;
        etapesImages[2].enabled = false;

        projectData = _projectData;
        projectID = projectData.rawData.id;
        FillData();
        // CREATE an empty project
    }

    // Fill data when modifying project
    private void FillData()
    {
        // Panel 1
        title.text = projectData.rawData.title;
        address.text = projectData.rawData.address;
        description.text = projectData.rawData.description;
        mission.text = projectData.rawData.mission;
        estimation.text = projectData.rawData.estimate;
        stake.text = projectData.rawData.stake;
        // Panel 2
        approach.text = projectData.rawData.approach;
        solution.text = projectData.rawData.solution;
        clientName.text = projectData.rawData.clientsName;
        completionDate.text = projectData.rawData.completionDate;
        tags.text = projectData.rawData.title;
        // Panel 3
        longitude.text = projectData.rawData.longitude.ToString();
        latitude.text = projectData.rawData.latitude.ToString();
        panelCreerProjetEtape3.FillPanel(projectData);
    }

    // WEBSERVICES

    private void _postBookProject_onComplete(long status, string message)
    {
        //Debug.Log("_postBookProject_onComplete " + status + " : " + message);
        //Debug.Log( _getBookProject.bookData.ToString());

        projectID = _postBookProject.newprojectID;
    }

    private void _postBookProject_onError(long status, string message)
    {
        Debug.Log("_postBookProject_onError" + status + " : " + message);
    }

    private void Save1stForm()
    {
        if (!updatingData)
        {
            //Debug.Log( projectID);
            _patchBookProject.FillFirstPageForm(projectID, title.text, address.text, description.text, mission.text, estimation.text, stake.text);
            _patchBookProject.onComplete += _patchBookProject_onComplete;
            _patchBookProject.onError += _patchBookProject_onError;
            updatingData = true;
            _patchBookProject.UseWebService();
        }
        else
        {
            // TODO DISPLAY MESSAGE TO WAIT END OF SAVE
        }
    }

    private void Save2dForm()
    {
        if (!updatingData)
        {
            //Debug.Log( projectID);
            _patchBookProject.FillSecondPageForm(projectID, approach.text, solution.text, clientName.text, completionDate.text, tags.text);
            _patchBookProject.onComplete += _patchBookProject_onComplete;
            _patchBookProject.onError += _patchBookProject_onError;
            updatingData = true;
            _patchBookProject.UseWebService();
        }
        else
        {
            // TODO DISPLAY MESSAGE TO WAIT END OF SAVE
        }

    }

    private void Save3dForm()
    {
        if (!updatingData)
        {
            //Debug.Log( projectID);
            double lon, lat;
            bool result1, result2 = false;
            result1 = double.TryParse(longitude.text, out lon);
            result2 = double.TryParse(latitude.text, out lat);
            List<int> categoryIds = new List<int>();
            int catID = 0;

            foreach (GameObject cat in panelCreerProjetEtape3.dropDownCategoryList)
            {
                catID = cat.GetComponent<DropDownCategoryManager>().chosenCategoryID;
                if (catID != -1)
                    categoryIds.Add(catID);
            }

            if (result1 && result2)
            {
                _patchBookProject.FillThirdPageForm(categoryIds, projectID, lon, lat, coverImageName);
            }
            else
            {
                _patchBookProject.FillThirdPageForm(categoryIds, projectID, 0, 0, coverImageName);
            }
            _patchBookProject.onComplete += _patchBookProject_onComplete;
            _patchBookProject.onError += _patchBookProject_onError;
            updatingData = true;
            _patchBookProject.UseWebService();
        }
        else
        {
            // TODO DISPLAY MESSAGE TO WAIT END OF SAVE
        }
    }

    private void Save3dForm(int projectStatus)
    {
        if (!updatingData)
        {
            //Debug.Log( projectID);
            double lon, lat;
            bool result1, result2 = false;
            result1 = double.TryParse(longitude.text, out lon);
            result2 = double.TryParse(latitude.text, out lat);
            List<int> categoryIds = new List<int>();
            int catID = 0;

            foreach (GameObject cat in panelCreerProjetEtape3.dropDownCategoryList)
            {
                catID = cat.GetComponent<DropDownCategoryManager>().chosenCategoryID;
                categoryIds.Add(catID);
            }

            if (result1 && result2)
            {
                _patchBookProject.FillThirdPageForm(categoryIds, projectID, lon, lat, coverImageName, projectStatus);
            }
            else
            {
                _patchBookProject.FillThirdPageForm(categoryIds, projectID, 0, 0, coverImageName, projectStatus);
            }
            _patchBookProject.onComplete += _patchBookProject_onComplete;
            _patchBookProject.onError += _patchBookProject_onError;
            updatingData = true;
            _patchBookProject.UseWebService();
        }
        else
        {
            // TODO DISPLAY MESSAGE TO WAIT END OF SAVE
        }
    }

    public void UploadImage(byte[] dataToUpload)
    {
        _postImageProject.newprojectID = projectID;
        _postImageProject._imageToUpload = dataToUpload;
        _postImageProject.UseWebService();
    }

    public void DeleteImage(string _name)
    {
        _deleteImageProject.idProject = projectID;
        _deleteImageProject.imageName = _name;
        _deleteImageProject.onComplete += _deleteImageProject_onComplete;
        _deleteImageProject.onError += _deleteImageProject_onError;
        _deleteImageProject.UseWebService();
    }

    public void UploadVideo(byte[] dataToUpload)
    {
        _postVideoProject.newprojectID = projectID;
        _postVideoProject._videoToUpload = dataToUpload;
        _postVideoProject.UseWebService();
        panelLoader.SetActive(true);
    }
    
    private void _postVideoProject_onComplete(long status, string message)
    {
        panelLoader.SetActive(false);
        displayMessage.SetMessage("La vidéo a bien été uploadée");
        displayMessage.Open();
        updatingData = false;
        _postVideoProject.onComplete -= _postVideoProject_onComplete;
        _postVideoProject.onError -= _postVideoProject_onError;
    }

    private void _postVideoProject_onError(long status, string message)
    {
        Debug.Log("_postVideoProject " + status + " : " + message);
        panelLoader.SetActive(false);
        displayMessage.SetMessage("Une erreur est survenue lors de l'upload de la vidéo, veuillez réessayer plus tard.");
        displayMessage.Open();
        updatingData = false;
        _postVideoProject.onComplete -= _patchBookProject_onComplete;
        _postVideoProject.onError -= _patchBookProject_onError;
    }


    private void _patchBookProject_onComplete(long status, string message)
    {
        updatingData = false;
        _patchBookProject.onComplete -= _patchBookProject_onComplete;
        _patchBookProject.onError -= _patchBookProject_onError;
    }

    private void _patchBookProject_onError(long status, string message)
    {
        updatingData = false;
        _patchBookProject.onComplete -= _patchBookProject_onComplete;
        _patchBookProject.onError -= _patchBookProject_onError;
        Debug.Log("_patchBookProject_onError " + status + " : " + message);
        if (status == (long)WebService.Status.PRECONDITION_FAILED)
        {
            //TODO LOG NO DATA TO SAVE
            Debug.Log("No data to save");
        }
    }

    // WEB SERVICES CALLBACKS
    private void _postImageProject_onComplete(long status, string message)
    {
        //Debug.Log("_postImageProject_onComplete" + status + " : " + message);
        panelCreerProjetEtape3.UploadSucceed(_postImageProject.imageName);
    }

    private void _postImageProject_onError(long status, string message)
    {
        Debug.Log("_postImageProject_onError" + status + " : " + message);
        panelCreerProjetEtape3.UploadFailed();
    }

    private void _deleteImageProject_onError(long status, string message)
    {
        _deleteImageProject.onComplete -= _deleteImageProject_onComplete;
        _deleteImageProject.onError -= _deleteImageProject_onError;
        Debug.Log("_deleteImageProject_onError" + status + " : " + message);
        panelCreerProjetEtape3.DeleteFailed();
    }

    private void _deleteImageProject_onComplete(long status, string message)
    {
        _deleteImageProject.onComplete -= _deleteImageProject_onComplete;
        _deleteImageProject.onError -= _deleteImageProject_onError;
        Debug.Log("_deleteImageProject_onComplete" + status + " : " + message);
        panelCreerProjetEtape3.DeleteSucceed();
    }
}
