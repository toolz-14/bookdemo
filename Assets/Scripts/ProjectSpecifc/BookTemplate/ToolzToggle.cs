﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolzToggle : MonoBehaviour {

    public UnityEngine.Events.UnityEvent OnToggleClick;

    [HideInInspector]
    public bool isOn;

    private bool swap;

    void Start () {

    }
	
	public void OnToggleclicked (bool displayToggleMark) {
        swap = !swap;
        isOn = swap;
        OnToggleClick.Invoke();

        if (displayToggleMark)
        ChangeToggleDisplay(swap);
    }

    public void ChangeToggleDisplay(bool isOn)
    {
        if (isOn)
        {
            transform.GetChild(0).GetComponent<Image>().enabled = true;
        }
        else
        {
            transform.GetChild(0).GetComponent<Image>().enabled = false;
        }
    }

    public void ForceOn(bool displayToggleMark)
    {
        swap = true;
        isOn = true;
        OnToggleClick.Invoke();
        if (displayToggleMark)
            ChangeToggleDisplay(true);
    }

    public void ForceOff(bool displayToggleMark)
    {
        swap = false;
        isOn = false;
        OnToggleClick.Invoke();
        if (displayToggleMark)
            ChangeToggleDisplay(false);
    }
}
