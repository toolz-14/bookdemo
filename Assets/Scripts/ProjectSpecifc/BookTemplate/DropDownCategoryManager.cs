﻿using Lean;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropDownCategoryManager : MonoBehaviour
{

    [SerializeField]
    private Dropdown dropDown_1;
    [SerializeField]
    private Dropdown dropDown_2;
    [SerializeField]
    private Dropdown dropDown_3;
    [SerializeField]
    private Text dropdownLabel2;
    [SerializeField]
    private Text dropdownLabel3;
    
    private int[] SecteurMarcheID = new int[] { 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58 };
    private int[][] MetierID = new int[][] { new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, new int[] { 10, 11, 12, 13, 14, 15, 16, 17 }, new int[] { 18, 19, 20, 21, 22 }, new int[] { 23, 24, 25, 26, 27, 28, 29, 30 }, new int[]{ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41} };
    [HideInInspector]
    public int chosenCategoryID;
    private bool dontupdateDropdown;
    private int deselectedIndex;// index so that the dropdown is has no value selected

    public Dictionary<string, string[]> firstDDDictionnaryFrench = new Dictionary<string, string[]>()
    {
        { "Métiers", new string[] { "Bâtiment", "Environnement", "Eau", "Energie", "Infrastructure"} },
        { "Secteur de marché", new string[] { "Assurances", "Automobile", "Aéronautique", "Chimie", "Conglomérats", "Energie (production & distribution)", "Etat & Collectivités publiques", "Finance", "Immobilier", "Industrie", "Minier", "Papier", "Pétrole & Gaz", "Santé", "Transports", "Communication, Media & Digital", "Autres" } }
    };

    public Dictionary<string, string[]> SecondDDDictionnaryFrench = new Dictionary<string, string[]>()
    {
        { "Bâtiment", new string[] { "Bureaux", "Commerces", "Education", "Bâtiments publics", "Equipements résidentiels", "Industrie", "Loisirs", "Santé", "Autres"} },
        { "Environnement", new string[] { "Changement climatique", "Développement durable", "Etudes environnementales", "Installation de Stockage de Déchets", "Programme de réhabilitation", "Transactionnel", "Travaux", "Autres"} },
        { "Eau", new string[] { "Distribution", "Traitement", "Voie d’eaux", "Littoral", "Autres"} },
        { "Energie", new string[] { "Eolien", "Solaire", "Géothermie", "Pétrole", "Gaz", "Charbon", "Hydraulique", "Autres"} },
        { "Infrastructure", new string[] { "Aménagement foncier & urbain", "Aménagement  rural", "Aéroports", "Ferroviaire", "Mines", "Ponts", "Transports urbains", "Routes et autoroutes", "Ports et voies navigables", "Tunnels et structures souterraines", "Autres" } }
    };

    public Dictionary<string, string[]> firstDDDictionnaryEnglish = new Dictionary<string, string[]>()
    {
        { "Business Lines", new string[] { "Buildings", "Environment", "Water", "Energy & Power", "Infrastructure" } },
        { "Market sectors", new string[] { "Insurance", "Automotive", "Aviation", "Chemicals", "Conglomerates", "Energy", "State & local authorities", "Finance", "Real estate", "Industry", "Mining", "Paper", "Oil & Gas", "Healthcare", "Transports", "Digital, Media & Creative", "Other" } }
    };

    public Dictionary<string, string[]> SecondDDDictionnaryEnglish = new Dictionary<string, string[]>()
    {
        { "Buildings", new string[] { "Offices", "Retail", "Education", "Public buildings", "Residential", "Industrial buildings", "Leisure", "Healthcare", "Other" } },
        { "Environment", new string[] { "Climate change", "Sustainable development", "Contaminated site evaluation and remediation", "Municipal solid waste landfills", "Incident planning, recovery, and response", "Transactional", "Works", "Other" } },
        { "Water", new string[] { "Distribution", "Treatment", "Waterways", "Coastal", "Other" } },
        { "Energy & Power", new string[] { "Wind", "Solar", "Geothermal energy", "Oil", "Gas", "Coal", "Hydraulic", "Other" } },
        { "Infrastructure", new string[] { "Urban / Land development", "Rural development", "Airports", "Railways", "Mining", "Bridges", "Urban mass transportation", "Roads & Highways", "Ports & Waterways", "Tunnels & underground structures", "Other" } }
    };

    private void OnEnable()
    {
        chosenCategoryID = -1;
        SetDropDownValues();

        if (SecondDDDictionnaryFrench.Count != SecondDDDictionnaryEnglish.Count)
            Debug.LogError("ERROR dictionnaries doesn't match lenght");

        deselectedIndex = SecondDDDictionnaryFrench.Count;

        //set image couverture
        //if (projetImages.Count > 1)
        //{
        //    SetImageCouverture(0);
        //}
    }

    public void SetDropDownValue(int id)
    {
        chosenCategoryID = id;
    }

    private void SetDropDownValues()
    {
        // Set DropDown 1 values
        Dropdown.OptionData itemdropDownArea;
        dropDown_1.options.Clear();

        dropDown_1.value = 0;

        if (LeanLocalization.Instance.GetLanguage() == "French")
        {
            foreach (KeyValuePair<string, string[]> entry in firstDDDictionnaryFrench)
            {
                itemdropDownArea = new Dropdown.OptionData(entry.Key);
                dropDown_1.options.Add(itemdropDownArea);
            }

            // Set DropDown 2 values
            Dropdown.OptionData itemdropDropDownDistrict;
            dropDown_2.options.Clear();

            foreach (string var in firstDDDictionnaryFrench["Métiers"])
            {
                itemdropDropDownDistrict = new Dropdown.OptionData(var);
                dropDown_2.options.Add(itemdropDropDownDistrict);
            }
        }
        else if (LeanLocalization.Instance.GetLanguage() == "English")
        {
            foreach (KeyValuePair<string, string[]> entry in firstDDDictionnaryEnglish)
            {
                itemdropDownArea = new Dropdown.OptionData(entry.Key);
                dropDown_1.options.Add(itemdropDownArea);
            }

            // Set DropDown 2 values
            Dropdown.OptionData itemdropDropDownDistrict;
            dropDown_2.options.Clear();

            foreach (string var in firstDDDictionnaryEnglish["Business Lines"])
            {
                itemdropDropDownDistrict = new Dropdown.OptionData(var);
                dropDown_2.options.Add(itemdropDropDownDistrict);
            }
        }
    }

    public void OnValueChange_dropDown_1()
    {
        //Reset dropdowns
        dropDown_3.gameObject.SetActive(false);
        
        if (dropDown_1.value == 0)
        {
            chosenCategoryID = -1;
            dontupdateDropdown = true;
            dropDown_2.value = deselectedIndex; // will go directly in OnValueChange_dropDown_2
        }
        else if (dropDown_1.value == 1)
        {
            chosenCategoryID = SecteurMarcheID[0];
            dropDown_2.value = 0;
        }

        UpdatedropDown_2(dropDown_1.options[dropDown_1.value].text);
    }

    private void UpdatedropDown_2(string key)
    {
        Dropdown.OptionData itemdropDropDown_2;
        dropDown_2.options.Clear();

        if (LeanLocalization.Instance.GetLanguage() == "French")
        {
            foreach (string var in firstDDDictionnaryFrench[key])
            {
                itemdropDropDown_2 = new Dropdown.OptionData(var);
                dropDown_2.options.Add(itemdropDropDown_2);
            }

            //Update texte of the selected dropdown item
            if (dropDown_2.value != deselectedIndex)
                dropdownLabel2.text = firstDDDictionnaryFrench[key][dropDown_2.value];
            else
                dropdownLabel2.text = "";

        }
        else if (LeanLocalization.Instance.GetLanguage() == "English")
        {
            foreach (string var in firstDDDictionnaryEnglish[key])
            {
                itemdropDropDown_2 = new Dropdown.OptionData(var);
                dropDown_2.options.Add(itemdropDropDown_2);
            }

            //Update texte of the selected dropdown item 

            if (dropDown_2.value != deselectedIndex)
                dropdownLabel2.text = firstDDDictionnaryEnglish[key][dropDown_2.value];
            else
                dropdownLabel2.text = firstDDDictionnaryEnglish[key][0];
        }
    }

    // OnValueChange of Dropdown2
    public void OnValueChange_dropDown_2()
    {
        //dropDown_2.options[dropDown_2.value].text does not update instantly so we have to do a work around
        string key = "";
        if (dropDown_1.value == 0)
        {
            if (LeanLocalization.Instance.GetLanguage() == "French")
            {
                if(dropDown_2.value!=deselectedIndex)
                    key = firstDDDictionnaryFrench["Métiers"][dropDown_2.value];
                else
                    key = firstDDDictionnaryFrench["Métiers"][0];
            }
            else if (LeanLocalization.Instance.GetLanguage() == "English")
            {
                if (dropDown_2.value != deselectedIndex)
                    key = firstDDDictionnaryEnglish["Business Lines"][dropDown_2.value];
                else
                    key = firstDDDictionnaryEnglish["Business Lines"][0];
            }
        }
        else if (dropDown_1.value == 1)
        {
            if (LeanLocalization.Instance.GetLanguage() == "French")
            {
                key = firstDDDictionnaryFrench["Secteur de marché"][dropDown_2.value];
            }
            else if (LeanLocalization.Instance.GetLanguage() == "English")
            {
                key = firstDDDictionnaryEnglish["Market sectors"][dropDown_2.value];
            }
        }

        if (dropDown_1.value == 0)
        {
            //Debug.Log("DD0 dropDown_2.value" + dropDown_2.value + " " + key + " other : " + dropDown_2.options[dropDown_2.value].text);
            //UpdatedropDown_3(dropDown_2.options[dropDown_2.value].text);
            if (dontupdateDropdown) // if we reset manually Dropdown 0, we don't want to update dropdrown 3
                dontupdateDropdown = false;
            else
                UpdatedropDown_3(dropDown_2.options[dropDown_2.value].text);

        }
        else if (dropDown_1.value == 1)
        {
            // set ID category for backend
            chosenCategoryID = SecteurMarcheID[dropDown_2.value];
        }
    }

    private void UpdatedropDown_3(string key)
    {
        dropDown_3.gameObject.SetActive(true);

        Dropdown.OptionData itemdropDropDown_3;
        dropDown_3.options.Clear();

        if (LeanLocalization.Instance.GetLanguage() == "French")
        {
            foreach (string var in SecondDDDictionnaryFrench[key])
            {
                itemdropDropDown_3 = new Dropdown.OptionData(var);
                dropDown_3.options.Add(itemdropDropDown_3);
            }

            //Update texte of the selected dropdown item
            if (dropDown_3.value >= SecondDDDictionnaryFrench.Count)
            {
                dropDown_3.value = SecondDDDictionnaryFrench.Count - 1;
            }
            dropdownLabel3.text = SecondDDDictionnaryFrench[key][dropDown_3.value];

            chosenCategoryID = MetierID[dropDown_2.value][dropDown_3.value];
        }
        else if (LeanLocalization.Instance.GetLanguage() == "English")
        {
            foreach (string var in SecondDDDictionnaryEnglish[key])
            {
                itemdropDropDown_3 = new Dropdown.OptionData(var);
                dropDown_3.options.Add(itemdropDropDown_3);
            }

            //Update texte of the selected dropdown item
            if (dropDown_3.value >= SecondDDDictionnaryEnglish.Count)
            {
                dropDown_3.value = SecondDDDictionnaryEnglish.Count - 1;
            }
            dropdownLabel3.text = SecondDDDictionnaryEnglish[key][dropDown_3.value];
        }
    }

    // OnValueChange of Dropdown2
    public void OnValueChange_dropDown_3()
    {
        // set ID category for backend
        chosenCategoryID = MetierID[dropDown_2.value][dropDown_3.value];
        Debug.Log("chosenCategoryID" + chosenCategoryID);
    }
}
