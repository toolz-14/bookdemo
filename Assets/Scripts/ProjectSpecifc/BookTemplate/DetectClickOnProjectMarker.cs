﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Geocoding;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Toolz.WebServices;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DetectClickOnProjectMarker : MonoBehaviour {

    [SerializeField]
    private Camera myCamera;
    [SerializeField]
    private AbstractMap _map;
    [SerializeField]
    private PanelProject panelProject;
    [SerializeField]
    private GameObject canvasCalque;
    [SerializeField]
    private FreeTerrainCameraProject projectCamera;
    [SerializeField]
    private PanelGroupedMarkerLerp panelGroupedMarkerLerp;

    private Coroutine _reloadRoutine;
    private WaitForSeconds _wait;
    private Vector2d savedlatlong; 
    private int savedZoom;

    private RaycastHit hit;
    private Ray ray;
    private bool DoOnlyOnce;

    private string projectcameraValues;
    private int currentProjectId = 0;

    // WEBSERVICE
    private GetImageProject _getImageProject;

    public bool waitingWebRequest;


    private void Awake()
    {
        _wait = new WaitForSeconds(.3f);
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getImageProject = webService.GetComponent<GetImageProject>();
    }

    private void Update()
    {
        if (EventSystem.current != null)
        {
            if (IsPointerOverUIObject() == 5)
            {
                return;
            }
        }

        if (Input.GetButtonDown("Fire1"))
        {
            ray = myCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                //project marker
                if (hit.transform.gameObject.layer == 8)
                {
                    if (!DoOnlyOnce)
                    {
                        DoOnlyOnce = true;

                        if (hit.transform.GetComponent<ProjectMarkerData>() != null)
                        {
                            ProjectMarkerData project = hit.transform.GetComponent<ProjectMarkerData>();
                            //TODO OPEN GROUPED PANEL DONT GO DIRECTLY IN PROJECT
                            GoToAProject(project);
                        }

                        Invoke("Delayed", 0.4f);
                    }
                }
                //groupped project marker
                else if (hit.transform.gameObject.layer == 9)
                {
                    if (!DoOnlyOnce)
                    {
                        DoOnlyOnce = true;

                        if (hit.transform.GetComponent<ProjectMarkerGroup>() != null)
                        {
                            //clean
                            panelGroupedMarkerLerp.GetComponent<PanelGroupedMarker>().ClearProjectItemsList();

                            List<ProjectMarkerData> subProjectList = hit.transform.GetComponent<ProjectMarkerGroup>().subProjectList;
                            //Debug.Log("subProjectList.Count" + subProjectList.Count);

                            panelGroupedMarkerLerp.gameObject.SetActive(true); // cannot start coroutines if gameobject is not active

                            //set panel
                            foreach (ProjectMarkerData pm in subProjectList)
                            {
                                panelGroupedMarkerLerp.GetComponent<PanelGroupedMarker>().SetAnItem(pm); 
                            }

                            //resize
                            panelGroupedMarkerLerp.GetComponent<PanelGroupedMarker>().ProjectItemParent.GetComponent<ResizeContainerWithChildAmountGroupedProjectPanel>().GetChildrensAndResize();

                            //open panel
                            panelGroupedMarkerLerp.StartLerpOpen();
                        }

                        Invoke("Delayed", 0.4f);
                    }
                }
            }
        }
    }

    public void GoToAProject(ProjectMarkerData project)
    {      
        Reload(project.latlong);
        
        currentProjectId = project.rawData.id;
        //Debug.Log("GoToAProject " + project.name);
        if (project.cameraPosRotValid)
            projectCamera.ForceCameraPositionRotation(project.cameraPos, project.cameraRot);
        
        panelProject.SetUpPanelProjectView(project);
        canvasCalque.SetActive(false);

        // Pull images
        GetProjectsImages(project);
    }

    private void Delayed()
    {
        DoOnlyOnce = false;
    }

    private void Reload(Vector2d latlong)
    {
        if (_reloadRoutine != null)
        {
            StopCoroutine(_reloadRoutine);
            _reloadRoutine = null;
        }
        //save current zoom and latlong
        savedlatlong = _map.CenterLatitudeLongitude;
        savedZoom = _map.AbsoluteZoom;
        ReloadAfterDelayIn(16, latlong, MapExtentType.RangeAroundCenter);
        //_reloadRoutine = StartCoroutine(ReloadAfterDelayIn(16, latlong, MapExtentType.RangeAroundCenter));
    }

    void ReloadAfterDelayIn(int zoom, Vector2d latlong, MapExtentType mapExtentType)
    {
        _map.SetExtent(mapExtentType);
        _map.UpdateMap(latlong, zoom);
        _reloadRoutine = null;
        _map.BlockUpdateMap();
        if (GetComponent<Toolz.Triggers.TransitionTrigger>() != null)
        {
            GetComponent<Toolz.Triggers.TransitionTrigger>().TriggerTransition();
        }
    }

    //IEnumerator ReloadAfterDelayIn(int zoom, Vector2d latlong, MapExtentType mapExtentType)
    //{
    //    yield return new WaitForEndOfFrame();
    //    _map.SetExtent(mapExtentType);
    //    _map.UpdateMap(latlong, zoom);
    //    _reloadRoutine = null;
    //    _map.BlockUpdateMap();
    //    if (GetComponent<Toolz.Triggers.TransitionTrigger>() != null)
    //    {
    //        GetComponent<Toolz.Triggers.TransitionTrigger>().TriggerTransition();
    //    }
    //}

    public void ExitProject_Button()
    {
        _getImageProject.Dispose();

        if (_reloadRoutine != null)
        {
            StopCoroutine(_reloadRoutine);
            _reloadRoutine = null;
        }
        //_reloadRoutine = StartCoroutine(ReloadAfterDelayOut(savedZoom, savedlatlong, MapExtentType.CameraBounds));
        ReloadAfterDelayOut(savedZoom, savedlatlong, MapExtentType.CameraBounds);
    }

    void ReloadAfterDelayOut(int zoom, Vector2d latlong, MapExtentType mapExtentType)
    {
        _map.UnblockUpdateMap();
        _map.SetExtent(mapExtentType);
        _map.UpdateMap(latlong, zoom);
        _reloadRoutine = null;
        canvasCalque.SetActive(true);
    }

    //IEnumerator ReloadAfterDelayOut(int zoom, Vector2d latlong, MapExtentType mapExtentType)
    //{
    //    yield return new WaitForEndOfFrame();
    //    _map.UnblockUpdateMap();
    //    _map.SetExtent(mapExtentType);
    //    _map.UpdateMap(latlong, zoom);
    //    _reloadRoutine = null;
    //    canvasCalque.SetActive(true);
    //}

    public void GetProjectsImages(ProjectMarkerData projectMarker)
    {
        StartCoroutine(WaitWebRequestAndCallNewOne(projectMarker));
    }

    private IEnumerator WaitWebRequestAndCallNewOne(ProjectMarkerData projectMarker)
    {
        int counter = 0;
        foreach (string imageName in projectMarker.rawData.images)
        {
            if (imageName != null)
            {
                _getImageProject.idProject = projectMarker.rawData.id;
                _getImageProject.imageName = imageName;
                _getImageProject.onComplete += _getImageProject_onComplete;
                _getImageProject.onError += _getImageProject_onError;
                _getImageProject.UseWebService();
                waitingWebRequest = true;
                while (waitingWebRequest)
                {
                    yield return null;
                }
                // Request on complete done

                if (_getImageProject.sprite != null)
                {
                    projectMarker.images.Add(_getImageProject.sprite);
                    counter++;
                    Debug.Log("image " + counter + "/" + projectMarker.rawData.images.Count);
                }
                else
                    Debug.Log("error getting image");
                //Texture2D sampleTexture = new Texture2D(2, 2);
                //// the size of the texture will be replaced by image size
                //bool isLoaded = sampleTexture.LoadImage(_getImageProject.image);

                //if (isLoaded)
                //{
                //    counter++;
                //    Sprite sprite = Sprite.Create(sampleTexture, new Rect(0, 0, sampleTexture.width, sampleTexture.height), new Vector2(1, 1));
                //    projectMarker.images.Add(sprite);
                //    Debug.Log("image " + counter + "/"+ projectMarker.rawData.images.Count);
                //}
            }
        }
        yield return null;
        Debug.Log("All images downloaded");
        panelProject.ImagesSlideShowReady();
        projectMarker.imagesDownloaded = true;
    }

    // TODO : display error 
    private void _getImageProject_onError(long status, string message)
    {
        waitingWebRequest = false;
        _getImageProject.onComplete -= _getImageProject_onComplete;
        _getImageProject.onError -= _getImageProject_onError;
        Debug.Log("_getImageProject_onError " + status + " : " + message);
    }

    private void _getImageProject_onComplete(long status, string message)
    {
        waitingWebRequest = false;
        _getImageProject.onComplete -= _getImageProject_onComplete;
        _getImageProject.onError -= _getImageProject_onError;
        //Debug.Log("_getImageProject_onComplete " + status + " : " + " id " + _getImageProject.idProject + " " + _getImageProject.image.Length);
    }

    //detect clicks on UI (mouse)
    protected int IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        if (CustomInputModule.useRTCinputStatic)
        {
            eventDataCurrentPosition.position = new Vector2(RTCInput.mousePosition.x, RTCInput.mousePosition.y);
        }
        else
        {
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }


        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (results.Count > 0)
        {
            return results[0].gameObject.layer;
        }
        else
        {
            return -1;
        }
    }

    //detect clicks on UI (touch)
    protected int IsPointerOverUIObjectTouch(Canvas canvas, Vector2 screenPosition)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);

        eventDataCurrentPosition.position = screenPosition;
        GraphicRaycaster uiRaycaster = canvas.gameObject.GetComponent<GraphicRaycaster>();

        List<RaycastResult> results = new List<RaycastResult>();
        uiRaycaster.Raycast(eventDataCurrentPosition, results);

        if (results.Count > 0)
        {
            return results[0].gameObject.layer;
        }
        else
        {
            return -1;
        }
    }
}
