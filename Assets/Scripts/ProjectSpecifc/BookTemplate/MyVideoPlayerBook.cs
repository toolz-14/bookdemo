﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using System.IO;

public class MyVideoPlayerBook : MonoBehaviour {

    public GameObject loader;
    public GameObject videoBG;
    public VideoPlayer TheVideoComponent;
	public string TheVideoName = "";
	public VideoClip TheClip;
	public GameObject TheVideoCanvas;
    public GameObject projectPanel;
    public GameObject lightcycleButton;
    public LightCycle24H lightCycle24H;
    private bool IsPaused = false;
	public Sprite PauseOn;
	public Sprite PauseOff;
	public Image Pause;
	//public Material CameraBG;
	//private Material SavedMat = null;
 //   private string subFolder = "";

    private void Start()
    {
        TheVideoComponent.loopPointReached += EndReached;
    }

    public void StartTheVideo()
    {
        //GetComponent<IsImage>().theCamera.localRotation = Quaternion.identity;
        //GetComponent<IsImage>().theCamera.localPosition = Vector3.zero;

        //if (GetComponent<IsImage>() != null && GetComponent<IsImage>().StartwithImage)
        //{
        //    GetComponent<IsImage>().theImage.SetActive(false);
        //}
        //if (TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material != null &&
        //    !TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material.name.Contains("Cam_black_bg"))
        //{
        //    SavedMat = TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material;
        //}
        //TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material = CameraBG;

        TheVideoComponent.playOnAwake = false;
        TheVideoComponent.isLooping = false;

        if (TheVideoComponent.clip == null && TheVideoName != "")
        {
            TheVideoComponent.source = VideoSource.Url;
            //Debug.Log("video url = "+ "http://preprod.toolz.fr/internal/BagneuxVideos2019/" + subFolder + TheVideoName);
            //TheVideoComponent.url = "http://preprod.toolz.fr/internal/BagneuxVideos/" + TheVideoName + ".mp4";
            //TheVideoComponent.url = "http://budgetparticipatif.bagneux92.fr/carte3d//BagneuxVideos2019/" + subFolder + TheVideoName + ".mp4";
            //TheVideoComponent.url = "http://toolz.mairie-bagneux.fr:8080/BagneuxVideos2019/" + subFolder + TheVideoName + ".mp4";

            //LOCAL
            //TheVideoComponent.url = "http://localhost/internal/book-backend/files/video/" + TheVideoName;
            //PREPROD
            TheVideoComponent.url = "http://preprod.toolz.fr/internal/book-backend/files/video/" + TheVideoName;
            Debug.Log(TheVideoComponent.url);
        }
        else if(TheVideoComponent.clip == null && TheVideoName == "" && TheClip != null)
        {
            TheVideoComponent.source = VideoSource.VideoClip;
            TheVideoComponent.clip = TheClip;
        }

        TheVideoComponent.playOnAwake = false;
        TheVideoComponent.isLooping = true;

        TheVideoComponent.audioOutputMode = VideoAudioOutputMode.AudioSource;
        TheVideoComponent.controlledAudioTrackCount = 1;
        TheVideoComponent.EnableAudioTrack(0, true);
        TheVideoComponent.SetTargetAudioSource(0, GetComponent<AudioSource>());
        GetComponent<AudioSource>().volume = 1f;

        StartCoroutine(PrepareAndPlayVideo());
    }

    IEnumerator PrepareAndPlayVideo()
    {
        TheVideoCanvas.SetActive(true);
        projectPanel.SetActive(false);
        lightcycleButton.SetActive(false);
        videoBG.SetActive(true);
        lightCycle24H.LightCycleForceClose();

        if (loader != null)
        {
            loader.SetActive(true);
        }

        TheVideoComponent.Prepare();
        while (!TheVideoComponent.isPrepared)
        {
            //Debug.Log("Preparing Video");
            yield return null;
        }

        TheVideoComponent.Play();
        GetComponent<AudioSource>().Play();

        if (loader != null)
        {
            loader.SetActive(false);
        }
        //if (GetComponent<IsImage>() != null && GetComponent<IsImage>().Loader != null)
        //{
        //    GetComponent<IsImage>().Loader.SetActive(false);
        //}
    }

    private void EndReached(VideoPlayer vp)
    {
        StopTheVideoPanelProject();
    }

    public void StopTheVideo()
    {
        if (TheVideoCanvas.activeInHierarchy)
        {
            if (IsPaused)
            {
                Pause.sprite = PauseOff;
                IsPaused = false;
            }
            StopCoroutine(PrepareAndPlayVideo());
            TheVideoComponent.Stop();
            TheVideoCanvas.SetActive(false);
            projectPanel.SetActive(true);
            lightcycleButton.SetActive(true);
            videoBG.SetActive(false);
            //if (GetComponent<IsImage>() != null && GetComponent<IsImage>().StartwithImage)
            //{
            //    GetComponent<IsImage>().theImage.SetActive(true);
            //}
            //TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material = SavedMat;
            //if (GetComponent<IsImage>().theCamera.GetComponent<PanoramicViewBouyguesWeb>() != null)
            //{
            //    GetComponent<IsImage>().theCamera.GetComponent<PanoramicViewBouyguesWeb>().enabled = true;
            //}
            //GetComponent<IsImage>().Reset360CamPosition();
        }
        if (loader != null)
        {
            loader.SetActive(false);
        }
        //if (GetComponent<IsImage>() != null && GetComponent<IsImage>().Loader != null)
        //{
        //    GetComponent<IsImage>().Loader.SetActive(false);
        //}
    }

    public void StopTheVideoPanelProject()
    {
        StopTheVideo();
        //ProjectUIManager.Instance.ActiveProjectPanel();
    }

    public void SwapVideoPause()
    {
        //if (TheVideoComponent.length != 0)
        //{
            IsPaused = !IsPaused;

            if (IsPaused)
            {
                PauseTheVideo();
                Pause.sprite = PauseOn;
            }
            else
            {
                TheVideoComponent.Play();
                Pause.sprite = PauseOff;
            }
        //}
    }

    public void PauseTheVideo()
    {
        TheVideoComponent.Pause();
    }
}
