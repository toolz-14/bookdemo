﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelTopRightMenuButton : MonoBehaviour {

    public static bool isOpen;
    public static bool doOnce;
    [SerializeField]
    private PanelTopRightMenuLerp TRMenu;

    public void Swap()
    {

        if (!doOnce)
        {
            doOnce = true;
            isOpen = !isOpen;
            if (isOpen)
            {
                TRMenu.gameObject.SetActive(true);
                TRMenu.StartLerpOpen();
            }
            else
            {
                TRMenu.StartLerpClose();
            }
        }
    }

    public void ForceClose()
    {
        if (isOpen)
        {
            TRMenu.StartLerpClose();
            isOpen = false;
        }
    }
}
