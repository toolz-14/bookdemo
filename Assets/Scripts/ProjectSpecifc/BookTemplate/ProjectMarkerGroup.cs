﻿using Mapbox.Utils;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;

public class ProjectMarkerGroup : MonoBehaviour {

    [HideInInspector]
    public List<ProjectMarkerData> subProjectList = new List<ProjectMarkerData>();
}
