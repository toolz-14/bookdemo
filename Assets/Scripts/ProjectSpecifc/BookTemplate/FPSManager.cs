﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSManager : MonoBehaviour {

    [SerializeField]
    private GameObject projectCamera;
    [SerializeField]
    private GameObject projectPanel;
    [SerializeField]
    private GameObject canvasWorld;
    [SerializeField]
    private GameObject lightCycleButton;
    [SerializeField]
    private GameObject playerHolderFPS;
    [SerializeField]
    private GameObject[] fpsPlayers = new GameObject[6];

    private int Savedindex = 0;
    private Vector3 startPos;

    private void Start ()
    {
		
	}

    public void OnFPSClick(int index)
    {
        playerHolderFPS.SetActive(true);
        projectCamera.SetActive(false);
        projectPanel.SetActive(false);
        lightCycleButton.SetActive(false);
        Savedindex = index;
        fpsPlayers[index].SetActive(true);
        startPos = fpsPlayers[index].transform.position;
        canvasWorld.SetActive(false);
    }

    public void OnFPSExit()
    {
        canvasWorld.SetActive(true);
        projectCamera.SetActive(true);
        projectPanel.SetActive(true);
        lightCycleButton.SetActive(true);
        fpsPlayers[Savedindex].transform.position = startPos;
        fpsPlayers[Savedindex].SetActive(false);
        playerHolderFPS.SetActive(false);
    }
}
