﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResizeContainerWithChildAmountCalques : MonoBehaviour {

	[SerializeField]
	private bool addOffet = true;
    private float calquesCanvasSize = 0f;
    private float calquesBgStartSize = 0f;
    private float calquesTopInfoSize = 0f;
    private float calquesSearchSize = 0f;
    private float containerSize;

	private void Start () {
        //GetChildrensAndResize ();

        calquesCanvasSize = transform.parent.parent.parent.parent.parent.GetComponent<RectTransform>().sizeDelta.y;
        calquesBgStartSize = transform.parent.parent.parent.parent.GetComponent<RectTransform>().sizeDelta.y - GetComponent<RectTransform>().sizeDelta.y;
        calquesTopInfoSize = transform.parent.parent.parent.parent.Find("Top_Info").GetComponent<RectTransform>().sizeDelta.y;
        calquesSearchSize = transform.parent.parent.parent.parent.Find("Panel_Search").GetComponent<RectTransform>().sizeDelta.y;
    }


	//if click on top group or group
	//get first depth childrens and resize
	public void GetChildrensAndResize(){
		containerSize = 0f;

		foreach(Transform trans in gameObject.transform){
            //add 30 to height per openned toggle
            if (trans.gameObject.activeInHierarchy)
            {
                containerSize += trans.GetComponent<RectTransform>().sizeDelta.y + GetComponent<VerticalLayoutGroup>().spacing;
            }
        }

		//Debug.Log ("GetChildrensAndResize, containerSize = "+containerSize);

		//resize
        containerSize += GetComponent<VerticalLayoutGroup>().padding.top;
        containerSize += GetComponent<VerticalLayoutGroup>().padding.bottom;
		//Debug.Log ("containerSize = "+containerSize);
        if (addOffet){
	        containerSize += 30f;
        }

        // container
        GetComponent<RectTransform> ().sizeDelta = new Vector2 (GetComponent<RectTransform> ().sizeDelta.x, containerSize);

        //limit scrollview height to canvas height minus other panels (topview + search)
        float temp = containerSize;
        //offset found by looking for space between scrollview and search + space between search and bottom
        float magicNumber = 30f;
        float maxSize = calquesCanvasSize - calquesTopInfoSize - calquesSearchSize - magicNumber;
        if (temp > maxSize)
        {
            temp = maxSize;
        }
        transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, temp);
        transform.parent.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.parent.parent.GetComponent<RectTransform>().sizeDelta.x, temp);

        //limit general bg height to canvas height
        temp = calquesBgStartSize + containerSize;
        if (temp > calquesCanvasSize)
        {
            temp = calquesCanvasSize;
        }
        transform.parent.parent.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.parent.parent.parent.GetComponent<RectTransform>().sizeDelta.x, (temp));
	}
}
