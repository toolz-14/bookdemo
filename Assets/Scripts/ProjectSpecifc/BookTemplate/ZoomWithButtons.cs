﻿using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomWithButtons : MonoBehaviour {

    [SerializeField]
    AbstractMap _mapManager;

    float zoom = 0;

    public void ButtonMinus()
    {
        zoom = _mapManager.AbsoluteZoom;
        zoom -= 1f;
        if (zoom < 3)
        {
            zoom = 3;
        }
        _mapManager.UpdateMap(_mapManager.CenterLatitudeLongitude, zoom);
    }

    public void ButtonPlus()
    {
        zoom = _mapManager.AbsoluteZoom;
        zoom += 1f;
        if (zoom > 16)
        {
            zoom = 16;
        }
        _mapManager.UpdateMap(_mapManager.CenterLatitudeLongitude, zoom);
    }
}
