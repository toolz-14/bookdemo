﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelMesProjetsLerp : MonoBehaviour {

    [SerializeField]
    private GameObject antiClickOverlay;

    private Vector3 _from1;
    private Vector3 _to1;
    private bool _launch;
    private float _timeSinceTransitionStarted;
    private float _t;
    private float Duration1 = 0.2f;

    private Vector3 _from2;
    private Vector3 _to2;
    private bool _launch2;
    private float _timeSinceTransitionStarted2;
    private float _t2;
    private float Duration2 = 0.2f;


    private void Update()
    {
        if (_launch)
        {
            OpenPanel();
        }
        if (_launch2)
        {
            ClosePanel();
        }
    }

    public void StartLerpOpen()
    {
        _timeSinceTransitionStarted = 0f;
        _from1 = transform.localScale;
        _to1 = Vector3.one;
        _launch = true;
    }

    private void OpenPanel()
    {
        _timeSinceTransitionStarted += Time.deltaTime;
        _t = _timeSinceTransitionStarted / Duration1;

        transform.localScale = Vector3.Lerp(_from1, _to1, _t);
        if (Vector3.Distance(transform.localScale, _to1) < 0.001f)
        {
            _launch = false;
            antiClickOverlay.SetActive(true);
        }
    }

    public void StartLerpClose()
    {
        _timeSinceTransitionStarted2 = 0f;
        _from2 = transform.localScale;
        _to2 = Vector3.zero;
        _launch2 = true;
    }

    private void ClosePanel()
    {
        _timeSinceTransitionStarted2 += Time.deltaTime;
        _t2 = _timeSinceTransitionStarted2 / Duration2;

        transform.localScale = Vector3.Lerp(_from2, _to2, _t2);
        if (Vector3.Distance(transform.localScale, _to2) < 0.001f)
        {
            _launch2 = false;
            antiClickOverlay.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
