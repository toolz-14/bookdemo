﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Toolz.WebServices;

public class PanelProject : MonoBehaviour {

    [SerializeField]
    private GameObject iconHolderFPS;
    [SerializeField]
    private GameObject playerHolderFPS; 
    [SerializeField]
    private MyVideoPlayerBook myVideoPlayerBook;

    [SerializeField]
    private GameObject[] iconsFPS;
    [SerializeField]
    private GameObject[] playerFPS;

    //only for DEMO
    [SerializeField]
    private GameObject panelRIVP;
    [SerializeField]
    private GameObject InsideStadiumHS;
    [SerializeField]
    private GameObject IcadePulseholder;
    [SerializeField]
    private GameObject PulseTerrasseHolder;

    [HideInInspector]
    public ProjectMarkerData projectMarker;

    [Header("Outputs fields")]
    public Text projectTitle;
    public Text locationText;
    public Image coverImage;
    public Text descriptionText;
    public Text missionText;
    public Text estimateText;
    public Text stakeText;
    public Text approachText;
    public Text solutionText;
    public Text tagsText;
    public Text completionDateText;
    public Text clientsNameText;
    private int projectID;

    [SerializeField]
    private SlideShow slideshow;

    private bool delaySlideshowIsPassed;

    private void OnEnable()
    {
        delaySlideshowIsPassed = false;
        Invoke("DelayForslideshow", 0.4f);
    }

    private void DelayForslideshow()
    {
        delaySlideshowIsPassed = true;
    }

    public void SetUpPanelProjectView(ProjectMarkerData _projectMarker)
    {
        coverImage.transform.Find("Image_Loader").gameObject.SetActive(true);
        coverImage.transform.Find("Image_Icon").gameObject.SetActive(false);

        projectMarker = _projectMarker;

        if (projectMarker == null)
        {
            Debug.LogError("Error : projectMarker is null");
        }
        else
        {
            projectTitle.text = projectMarker.rawData.title;
            locationText.text = projectMarker.rawData.address;
            descriptionText.text = projectMarker.rawData.description;
            missionText.text = projectMarker.rawData.mission;
            estimateText.text = projectMarker.rawData.estimate;
            stakeText.text = projectMarker.rawData.stake;
            approachText.text = projectMarker.rawData.approach;
            solutionText.text = projectMarker.rawData.solution;
            tagsText.text = projectMarker.rawData.tags;
            completionDateText.text = projectMarker.rawData.completionDate;
            clientsNameText.text = projectMarker.rawData.clientsName;
            projectID = projectMarker.rawData.id;

            if (projectMarker.imageCover != null)
                coverImage.sprite = projectMarker.imageCover;
            else
                Debug.Log("no cover image");

            //fps stuff
            if (!projectMarker.noFpsIconCam)
            {
                //Debug.Log("PanelProject, SetUpFPSViews()");
                SetUpFPSViews(projectMarker.iconPos, projectMarker.playerPos, projectMarker.playerRot);
            }

            // only one video for now
            if(projectMarker.rawData.videos!=null&& projectMarker.rawData.videos.Count>0)
                myVideoPlayerBook.TheVideoName = projectMarker.rawData.videos[0];

            if (projectID == 3)
            {
                panelRIVP.SetActive(true);
            }

            if (projectID == 4)
            {
                InsideStadiumHS.SetActive(true);
            }

            if (projectID == 2)
            {
                IcadePulseholder.SetActive(true);
                PulseTerrasseHolder.SetActive(true);
            }
        }
    }


    //12 values for each icon/camera pair: 3 for pos and 3 for rot for ech of them
    //MAX 6 PAIR !!!
    private void SetUpFPSViews(List<Vector3> iconPos, List<Vector3> playerPos, List<Vector3> playerRot)
    {
        //Debug.Log("iconPos.Count = "+ iconPos.Count);
        int amount = iconPos.Count;
        if (amount > 6)
        {
            amount = 6;
        }

        for (int i=0; i< amount; i++)
        {
            iconsFPS[i].transform.localPosition = iconPos[i];
            iconsFPS[i].SetActive(true);
            playerFPS[i].transform.localPosition = playerPos[i];
            playerFPS[i].transform.localEulerAngles = playerRot[i];
        }

        iconHolderFPS.SetActive(true);
    }

    public void ResetAllFPSToInactive()
    {
        foreach (GameObject go in iconsFPS)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in playerFPS)
        {
            go.SetActive(false);
        }
        
        iconHolderFPS.SetActive(false);
    }

    public void OpenSlideShow()
    {
        if (projectMarker.imagesDownloaded && delaySlideshowIsPassed)
        {
            slideshow.StartOpenSlideshow(0, projectMarker.images);
        }
        else
            Debug.Log("Images not loaded yet");
    }

    public void ImagesSlideShowReady()
    {
        coverImage.transform.Find("Image_Loader").gameObject.SetActive(false);
        coverImage.transform.Find("Image_Icon").gameObject.SetActive(true);
    }
}
