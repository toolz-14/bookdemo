using UnityEngine;
using System.Collections;
using System.Xml;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using Mapbox.Unity.Map;

public class FreeTerrainCameraMapbox : BaseCamera
{
    //used for zoom atm
    public AbstractMap abstractMap;
    //for globe view
    public Mapbox.Unity.Map.TileProviders.GlobeTileProvider globeTileProvider;
    public Mapbox.Unity.Map.TileProviders.TileErrorHandler tileErrorHandler;
    public Mapbox.Examples.Scripts.Utilities.DragRotate dragRotate;
    public Mapbox.Examples.QuadTreeCameraMovement quadTreeCameraMovement;


    public float SpeedZoom = 1f;
    public float SpeedMove = 1f;
    public bool isOrtographicCam = false;

    protected Vector3 _min;
    protected Vector3 _max;
    protected Vector3 _drag;

    protected Vector2 _textureSize;
    protected Vector3 _posRatio;
    protected float _elevation = 100;
    protected float _altitude = 0;
    protected float _minAltitude;
    protected float _altitudeRatio = 0;

    private float _speedMult = 1f;
    //	private bool _autoElevationRotation = true;

    private Vector3 _tempVect3;
    private Vector3 _newZoomPosition;

    private bool _updatePosition = true;
    [HideInInspector]
    public bool IsMovingByGesture = true;

#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
#endif
    public static bool ForwardCollision;

    private Vector3 MySavedPos;
    private Vector3 MySavedEuler;
    public static bool PreventAllInput;
    public static bool preventDrag;
    public static bool preventZoom;
    public static bool preventPan;
    public static bool preventRotate;

    public static bool BlockCameraMovement;
    public static bool ignoreUI;
    private bool JustExitedBlock;

    private float savedMoveSpeed = 10f;
    private float savedRotateSpeed = 10f;
    private float savedZoomSpeed = 10f;

    // Simulates Camera movement 
    private float maxdist = 0f; // distance of mouse drag
    private float scalingSpeed = 0f; // camera moving speed, decreases when closed to destination

    private float maxDragValue = 0.025f; // Clamp drag distance to prevent camera sliding to much
    private float speedSmoothingValue = 0.05f;
    private bool updatingSpeed;
    private bool block_1 = false;
    private bool block_2 = true;

    //------------------| Awake |

    protected override void Awake()
    {
        base.Awake();
        JustExitedBlock = false;
        BlockCameraMovement = false;
        ignoreUI = false;
        _posRatio = Vector3.zero;
        _drag = Vector3.zero;
        _min = Vector3.zero;
        _max = Vector3.zero;
        PreventAllInput = false;
        preventDrag = false;
        preventRotate = false;
        preventZoom = false;

        //pan not wanted for this project
        preventPan = true;
    }

    protected override void Start()
    {
        _position = _cameraHolder.position;
        _rotation = _cameraHolder.rotation.eulerAngles;
        _cameraHolder.position = _position;
        _cameraHolder.rotation = Quaternion.Euler(_rotation);
        base.Start();

    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (_cameraHolder == null)
        {
            return;
        }
        _updatePosition = true;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        _updatePosition = false;
    }

    protected override void Update()
    {
        base.Update();
        dt *= _speedMult;
        if (BlockCameraMovement)
        {
            JustExitedBlock = true;
            return;
        }
        else
        {
            if (JustExitedBlock)
            {
                JustExitedBlock = false;
                _position = _cameraHolder.position;
                _rotation = _cameraHolder.rotation.eulerAngles;

                _cameraHolder.position = _position;
                _cameraHolder.rotation = Quaternion.Euler(_rotation);
            }
        }
        if (EventSystem.current != null && !ignoreUI)
        {
            if (IsPointerOverUIObject() == 5)
            {
                JustExitedBlock = true;
                return;
            }
            else
            {
                if (JustExitedBlock)
                {
                    JustExitedBlock = false;
                    _position = _cameraHolder.position;
                    _rotation = _cameraHolder.rotation.eulerAngles;

                    _cameraHolder.position = _position;
                    _cameraHolder.rotation = Quaternion.Euler(_rotation);
                }
            }
        }

        _tempVect3 = Vector3.Lerp(_cameraHolder.position, _position, 100f * dt);
        if (!AuthorizeMove(_tempVect3, true))
        {
            _updatePosition = false;
            _position = _cameraHolder.position;
        }
        else
        {
            if (Physics.Linecast(_cameraHolder.position, _tempVect3))
            {
                _updatePosition = false;
                _position = _cameraHolder.position;
            }
            else
            {
                AuthorizeMove(_tempVect3, true);
                Vector3 temp = _cameraHolder.position;
                temp.y = _tempVect3.y;
                _cameraHolder.position = temp;
                _updatePosition = true;
            }
        }

        //moveSens = (.2f + .8f) * SpeedMove;
        zoomSens = (.5f + .5f) * SpeedZoom;
        rotateSens = .4f + .6f;

        _cameraHolder.rotation = Quaternion.Lerp(_cameraHolder.rotation, Quaternion.Euler(_rotation), rotateSpeed * dt);

    }

    public void ForceCameraPosition(Transform pos)
    {
        if (_cameraHolder != null)
        {
            PreventAllInput = true;
            BlockCameraMovement = true;

            _cameraHolder.position = pos.position;
            _cameraHolder.rotation = pos.rotation;

            _position = _cameraHolder.position;
            _rotation = _cameraHolder.rotation.eulerAngles;

            PreventAllInput = false;
            BlockCameraMovement = false;
        }
    }

    public override void MoveTo(Transform t)
    {
        MoveTo(t.position, t.eulerAngles);

        Invoke("SpeedValuesBack", 1f);
    }

    public void MoveToSpeed(Transform t, float speed)
    {
        MoveTo(t.position, t.eulerAngles);

        savedMoveSpeed = moveSpeed;
        moveSpeed = speed;
        savedRotateSpeed = rotateSpeed;
        rotateSpeed = speed;
        savedZoomSpeed = zoomSpeed;
        zoomSpeed = speed;

        Invoke("SpeedValuesBack", 1f);
    }

    private void SpeedValuesBack()
    {
        moveSpeed = savedMoveSpeed;
        rotateSpeed = savedRotateSpeed;
        zoomSpeed = savedZoomSpeed;
    }

    public override void MoveTo(Vector3 position, Vector3 eulerAngles)
    {
        _position.x = position.x;
        _position.z = position.z;
        _position.y = position.y;

        _rotation = eulerAngles;
    }

    public void PreventMouseTouchInput(bool inputBlocked)
    {
        PreventAllInput = inputBlocked;
    }

    public void SaveCameraValuesBeforeMoveTo()
    {
        MySavedPos = _position;
        MySavedEuler = _rotation;
    }

    public void ResetCameraAfterMoveTo()
    {
        _position.y = MySavedPos.y;
        _rotation = MySavedEuler;
    }

    public void InverseMoveSens()
    {
        moveSens = moveSens * -1f;
    }

    //------------------| Gestures |

    public override void Drag(DragGesture g, Vector2 magnitude)
    {
        //if (!PreventAllInput && !preventDrag)
        //{
        //    if (_updatePosition)
        //    {
        //        if (EventSystem.current != null)
        //        {
        //            if (IsPointerOverUIObject() == 5)
        //            {
        //                return;
        //            }
        //        }
        //        _speedMult = 1f;

        //        _drag.x = magnitude.x * moveSens;
        //        _drag.z = magnitude.y * moveSens;
                
        //        float angle = Mathf.Atan2(_drag.z, _drag.x) - _cameraHolder.eulerAngles.y * Mathf.Deg2Rad;
        //        float move = _drag.magnitude;

        //        _position.x += Mathf.Cos(angle) * move;
        //        _position.z += Mathf.Sin(angle) * move;
        //    }
        //}
    }

    public override void Zoom(ZoomGesture g, float delta)
    {
        if (!PreventAllInput && !preventZoom)
        {
            if (enabled)
            {
                if (EventSystem.current != null)
                {
                    if (IsPointerOverUIObject() == 5)
                    {
                        return;
                    }
                }
                _speedMult = 1f;

                if (!isOrtographicCam)
                {
                    if ((16f - abstractMap.Options.locationOptions.zoom) <= 0.1f)
                    {
                        if (_cameraHolder.localPosition.y < 220f)
                        {
                            Mapbox.Examples.QuadTreeCameraMovement.BlockMapboxZoom = true;
                        }
                        else
                        {
                            Mapbox.Examples.QuadTreeCameraMovement.BlockMapboxZoom = false;
                        }
                        
                        if (_cameraHolder.localPosition.y >= 220f)
                        {
                            if (delta > 0)
                            {

                            }
                            else
                            {
                                Vector3 temp = _cameraHolder.localPosition;
                                temp.y = 220f;
                                _position = temp;
                                _rotation.x = 90f;
                                delta = 0;
                            }
                        }

                        if (_cameraHolder.localPosition.y < 41f && delta > 0)
                        {
                            _rotation.x = 40.58f;
                            delta = 0;
                        }

                        if (_cameraHolder.localPosition.y > 90f && _cameraHolder.localPosition.y < 220f)
                        {
                            if (delta < 0)
                            {
                                _rotation.x += 20f;
                            }

                            if (delta > 0)
                            {
                                _rotation.x -= 20f;
                            }

                            if (_rotation.x > 90f)
                            {
                                _rotation.x = 90f;
                            }

                            if (_rotation.x < 40.58f)
                            {
                                _rotation.x = 40.58f;
                            }
                        }

                        _position.y -= delta * zoomSpeed * zoomSens;
                        if (_position.y < 41f)
                        {
                            Vector3 temp = _position;
                            temp.y = 41f;
                            _position = temp;
                        }
                    }

                    //if (!block_1 && abstractMap.AbsoluteZoom < 4)
                    //{
                    //    block_1 = true;
                    //    block_2 = false;

                    //    quadTreeCameraMovement.enabled = false;
                    //    tileErrorHandler.enabled = true;
                    //    dragRotate.enabled = true;

                    //    Debug.Log("Freecam, abstractMap.AbsoluteZoom < 4");
                    //    Mapbox.Unity.Map.TileProviders.AbstractTileProvider temp = globeTileProvider;
                    //    Debug.Log("Freecam, temp = "+ temp);
                    //    abstractMap.TileProvider = temp;
                    //    Debug.Log("Freecam, abstractMap.TileProvider = " + abstractMap.TileProvider);
                    //    temp.OnInitialized();
                    //    abstractMap.SetExtent(MapExtentType.Custom);
                    //    abstractMap.SetTileProvider();

                    //    globeTileProvider.Initialize(abstractMap);
                    //    abstractMap._terrain.ElevationType = ElevationLayerType.GlobeTerrain;

                    //    _rotation.x = 0f;
                    //    abstractMap.transform.localPosition = new Vector3(115f, 759f, 4092f);

                    //    preventDrag = true;
                    //    preventRotate = true;
                    //}
                    //else if(!block_2 && abstractMap.AbsoluteZoom >= 4)
                    //{
                    //    block_1 = false;
                    //    block_2 = true;
                    //    CameraBoundsTileProviderOptions temp = new CameraBoundsTileProviderOptions();
                    //    temp.SetOptions(_cameraHolder.Find("Camera").GetComponent<Camera>(), 100, 200);
                    //    abstractMap.SetExtent(MapExtentType.CameraBounds, temp);
                    //    abstractMap._terrain.ElevationType = ElevationLayerType.FlatTerrain;
                    //    abstractMap.transform.localPosition = new Vector3(0f, 0f, 0f);

                    //    tileErrorHandler.enabled = false;
                    //    dragRotate.enabled = false;
                    //    quadTreeCameraMovement.enabled = true;

                    //    BlockCameraMovement = false;
                    //}
                }
                else
                {

                    _cameraHolder.Find("Camera").GetComponent<Camera>().orthographicSize -= delta * zoomSpeed * zoomSens;
                    if (_cameraHolder.Find("Camera").GetComponent<Camera>().orthographicSize <= 100f)
                    {
                        _cameraHolder.Find("Camera").GetComponent<Camera>().orthographicSize = 100f;
                    }
                }
            }
        }
    }

    public override void Rotate(RotateGesture g, float angle)
    {
        if (!PreventAllInput && !preventRotate)
        {
            if (_updatePosition)
            {
                if (EventSystem.current != null)
                {
                    if (IsPointerOverUIObject() == 5)
                    {
                        return;
                    }
                }
                _speedMult = 1f;
                _rotation.y += angle * 4 * rotateSens;
            }
        }
    }

    public override void Pan(PanGesture g, Vector2 panning)
    {
        if (!PreventAllInput && !preventPan)
        {
            if (_updatePosition)
            {
                if (EventSystem.current != null)
                {
                    if (IsPointerOverUIObject() == 5)
                    {
                        return;
                    }
                }
                if (_rotation.x < 92f && _rotation.x > -92f)
                {
                    _speedMult = 1f;
					//_rotation.y -= panning.x * .8f * rotateSens;


                    if (Mathf.Abs(panning.y) > 0)
                    {
					//_autoElevationRotation = true;
                    }

                    //ConstrainRotationX();

                    _rotation.x += panning.y * .8f * rotateSens;
                }
                else
                {
                    if (_rotation.x > 92f)
                    {
                        Vector3 temp = _rotation;
                        temp.x = 91f;
                        _rotation = temp;
                    }
                    if (_rotation.x < -92f)
                    {
                        Vector3 temp = _rotation;
                        temp.x = -91f;
                        _rotation = temp;
                    }
                }
            }
        }
    }
}
