﻿using Mapbox.Examples;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class SupprimerProjet : MonoBehaviour {

    private DeleteBookProject _deleteBookProject;
    [SerializeField]
    private GameObject panelSupprimerProjet; // handle data
    [SerializeField]
    private GameObject popUpConfirmation;
    [SerializeField]
    private GameObject popUpResponse;
    [SerializeField]
    private Text panelResponseDeleteprojectText;

    [SerializeField]
    private MarkersManager markers;
    private GameObject panelToDesactivate;
    private int projectID;

    private void Start()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _deleteBookProject = webService.GetComponent<DeleteBookProject>();
    }

    public void DeleteProjectButton(int _projectID,GameObject _panelToDesactivate)
    {
        projectID = _projectID;
        panelToDesactivate = _panelToDesactivate;
        popUpConfirmation.SetActive(true);
        panelSupprimerProjet.SetActive(true);
    }

    public void YesDeleteProjectButton()
    {
        popUpConfirmation.SetActive(false);
        ConfirmDeleteProject(projectID);
    }

    public void Close(GameObject panel)
    {
        panel.SetActive(false);
        panelSupprimerProjet.SetActive(false);
    }

    public void ConfirmDeleteProject(int projectID)
    {
        _deleteBookProject.onComplete += _deleteBookProject_onComplete;
        _deleteBookProject.onError += _deleteBookProject_onError;
        _deleteBookProject.idProject = projectID;
        _deleteBookProject.UseWebService();
    }
    private void _deleteBookProject_onError(long status, string message)
    {
        _deleteBookProject.onComplete -= _deleteBookProject_onComplete;
        _deleteBookProject.onError -= _deleteBookProject_onError;
        Debug.Log("_deleteBookProject_onError " + status + " : " + message);
        popUpResponse.SetActive(true);
        panelResponseDeleteprojectText.text = "Une erreur est survenue, veuillez réessayer plus tard";
    }

    private void _deleteBookProject_onComplete(long status, string message)
    {
        _deleteBookProject.onComplete -= _deleteBookProject_onComplete;
        _deleteBookProject.onError -= _deleteBookProject_onError;
        //Debug.Log("_deleteBookProject_onComplete " + status + " : " + message);
        popUpResponse.SetActive(true);
        panelResponseDeleteprojectText.text = "Le projet a bien été supprimé";

        markers.Refresh();
        // Delete from Panel projet
        PanelMesProjets panelMesProjets = panelToDesactivate.GetComponent<PanelMesProjets>();
        if (panelMesProjets != null)
        {
            panelMesProjets.ClosePanel();
        }
        else
            Debug.Log("No Panel given");
    }

}
