﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerspectivesViewer : MonoBehaviour {

	[SerializeField]
	private Sprite[] prespectives;
	[SerializeField]
	private Image perspectiveImage;
	[SerializeField]
	private GameObject nextButton;
	private int savedIndex;
	private bool swap;

	private void Start () {
		
	}

	public void ActivatePerspective(int index){
		swap = false;
		savedIndex = index;
		gameObject.SetActive (true);

		perspectiveImage.sprite = prespectives[savedIndex];

		if (savedIndex == 0 || savedIndex == 4) {
			nextButton.SetActive (true);
		} else {
			nextButton.SetActive (false);
		}
	}

	public void NextButton(){

		swap = !swap;
		if (swap) {
			savedIndex = savedIndex + 1;
		} else {
			savedIndex = savedIndex - 1;
		}

		perspectiveImage.sprite = prespectives[savedIndex];
	}

	public void ExitButton(){
		gameObject.SetActive (false);
	}
}
