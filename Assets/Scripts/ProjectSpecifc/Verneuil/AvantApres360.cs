﻿using UnityEngine;
using UnityEngine.UI;

public class AvantApres360 : MonoBehaviour {

	[SerializeField]
	private DetectClickOn360Verneuil detectClickOn360Verneuil;

	private bool isApres;

	private void Start () {
		
	}

	private void OnEnable(){
		isApres = false;
        GetComponentInChildren<Text>().text = "Avant";
    }

	public void Swap(){
		isApres = !isApres;
		if(isApres){
			detectClickOn360Verneuil.SwapToApres ();
			GetComponentInChildren<Text>().text = "Apres";
		}else{
			detectClickOn360Verneuil.SwapToAvant ();
			GetComponentInChildren<Text>().text = "Avant";
		}
	}
}
