﻿using UnityEngine;

public class DetectClickOn360Verneuil : MonoBehaviour {

	[SerializeField]
	private Camera myCamera;
	[SerializeField]
	private GameObject assets;
	[SerializeField]
	private GameObject view360;
	[SerializeField]
	private Material mat360;
	[SerializeField]
	private GameObject mainUI;
    [SerializeField]
    private GameObject calquesUI;
    [SerializeField]
    private GameObject commentaireButton;
    [SerializeField]
	private GameObject directionalLight;
    [SerializeField]
    private GameObject avantApresUI;

    private RaycastHit hit;
	private Ray ray;
	private bool DoOnlyOnce;
	private SwapTo360View swapTo360View;
    private bool commentaireButtonWasActive;


    private void Start()
    {
        commentaireButtonWasActive = false;
    }


    private void Update () {

		if (Input.GetButtonDown ("Fire1")) {
			ray = myCamera.ScreenPointToRay (Input.mousePosition);
			//get the selection
			if (Physics.Raycast (ray, out hit)) {
				if (hit.transform.gameObject.layer == 12) {
					if(!DoOnlyOnce){
						DoOnlyOnce = true;
						assets.SetActive (false);
						myCamera.transform.parent.parent.parent.gameObject.SetActive (false);
						view360.SetActive (true);
                        mainUI.SetActive(false);
                        calquesUI.SetActive(false);
                        directionalLight.SetActive(false);
                        if (commentaireButton.activeInHierarchy)
                        {
                            commentaireButtonWasActive = true;
                            commentaireButton.SetActive(false);
                        }
						swapTo360View = hit.transform.GetComponent<SwapTo360View>();
						view360.GetComponentInChildren<Camera> (true).transform.localEulerAngles = swapTo360View.CameraStartEuler;
                        if (swapTo360View.avant360 == null)
                        {
                            //no avant/apres, so disable ui and show apres
                            swapTo360View.SwapToApres360(mat360);
                            avantApresUI.SetActive(false);
                        }
                        else
                        {
                            //enable ui and show apres
                            swapTo360View.SwapToAvant360(mat360);
                            avantApresUI.SetActive(true);
                        }
					}
				}
			}
		}
	}

	public void Setview360Off(){
		assets.SetActive (true);
		view360.SetActive (false);
		myCamera.transform.parent.parent.parent.gameObject.SetActive (true);
        mainUI.SetActive(true);
        calquesUI.SetActive(true);
        directionalLight.SetActive(true);
		DoOnlyOnce = false;
        if (commentaireButtonWasActive)
        {
            commentaireButtonWasActive = false;
            commentaireButton.SetActive(true);
        }
		#if UNITY_STANDALONE || UNITY_WEBGL
		ShowCursor(true);
		#endif
	}

	private void ShowCursor(bool b){
		Cursor.visible = b;

		if (b) {
			Cursor.lockState = CursorLockMode.None;
		}else{
			Cursor.lockState = CursorLockMode.Locked;
		}
	}

	public void SwapToAvant(){
		swapTo360View.SwapToAvant360 (mat360);
	}

	public void SwapToApres(){
		swapTo360View.SwapToApres360 (mat360);
	}
}
