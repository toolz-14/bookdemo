﻿using UnityEngine;

public class SwapTo360View : MonoBehaviour {

    public Texture avant360;
	[SerializeField]
	private Texture apres360;
	public Vector3 CameraStartEuler;


	private void Start () {
		
	}

	public void SwapToAvant360(Material mat360){
		mat360.mainTexture = avant360;
	}

	public void SwapToApres360(Material mat360){
		mat360.mainTexture = apres360;
	}
}
