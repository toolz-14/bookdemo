﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleNappe : MonoBehaviour {

	public List<GameObject> theObjects = new List<GameObject>();
	private Toggle _toggle;

	private void Start () {
		_toggle = GetComponent<Toggle> ();
	}

	public void ShowTransport(){
		for(int i=0; i<theObjects.Count ; i++){
			if (theObjects [i] != null) {
				theObjects [i].SetActive (true);
			}
		}
	}

	public void HideTransport(){
		for(int i=0; i<theObjects.Count ; i++){
			if (theObjects [i] != null) {
				theObjects [i].SetActive (false);
			}
		}
	}

	public void ToggleButton(){
		_toggle.isOn = !_toggle.isOn;

		if (_toggle.isOn) {
			ShowTransport ();
		} else {
			HideTransport ();
		}
	}

	public void OnToggleClick(){
		if (_toggle.isOn) {
			ShowTransport ();
		} else {
			HideTransport ();
		}
	}
}
