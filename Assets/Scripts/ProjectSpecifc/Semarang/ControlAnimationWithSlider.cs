﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlAnimationWithSlider : MonoBehaviour {

	public Animation anim;
	public Slider slider;

	private void Start () {
		anim.Play ("Anim_Semarang");
		anim ["Anim_Semarang"].speed = 0;
	}


	public void AnimateOnSliderValue () {
		anim["Anim_Semarang"].time = slider.value;
	}
		
}
