﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouttonDensite : MonoBehaviour {

	public GameObject Densite;
	public GameObject Densiteflat;

	private bool swap = false;

	private void Start () {
		swap = false;
	}

	public void DoTheSwap(){
		swap = !swap;
		if (swap) {
			Densiteflat.SetActive (true);
			Densite.SetActive (false);
		} else {
			Densiteflat.SetActive (false);
			Densite.SetActive (true);
		}
	}

	public void OnDisable(){
		if(Densiteflat != null){
			Densiteflat.SetActive (false);
		}
		if (Densite != null) {
			Densite.SetActive (false);
		}
	}
}
