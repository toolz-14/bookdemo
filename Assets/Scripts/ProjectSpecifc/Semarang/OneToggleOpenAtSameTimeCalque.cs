﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OneToggleOpenAtSameTimeCalque : MonoBehaviour {

	public List<ToggleCalque> toggleList= new List<ToggleCalque>();

	private void Start () {
	
	}

	public void ResetAllToggles(ToggleCalque toggle){
		for(int i=0; i<toggleList.Count ; i++){
			if(toggleList [i] != toggle){
				toggleList [i].GetComponent<Toggle>().isOn = false;
			}
		}
	}
}
