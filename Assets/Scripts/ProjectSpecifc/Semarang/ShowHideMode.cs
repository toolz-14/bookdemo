﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShowHideMode : MonoBehaviour {

	public List<GameObject> ModeList = new List<GameObject>();

	private bool swap = true;


	public void ShowHide(){
		swap = !swap;
		if (swap) {
			for (int i = 0; i < ModeList.Count; i++) {
				ModeList [i].SetActive (true);
			}
		} else {
			for (int i = 0; i < ModeList.Count; i++) {
				ModeList [i].SetActive (false);
			}
		}
	}

}
