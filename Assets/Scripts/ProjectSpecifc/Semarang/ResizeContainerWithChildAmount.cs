﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResizeContainerWithChildAmount : MonoBehaviour {

	public enum Direction{ width, height};
	public Direction ResizeDirection = Direction.height;
	[SerializeField]
	private bool addOffet = true;
	private float containerSize;

	private void Start () {
		GetChildrensAndResize ();
	}


	//if click on top group or group
	//get first depth childrens and resize
	public void GetChildrensAndResize(){
		containerSize = 0f;
		foreach(Transform trans in gameObject.transform){
            //add 30 to height per openned toggle
            if (trans.gameObject.activeInHierarchy)
            {
                if (ResizeDirection == Direction.height)
                {
                    containerSize += trans.GetComponent<RectTransform>().sizeDelta.y + GetComponent<VerticalLayoutGroup>().spacing;
                }
                else
                {
                    containerSize += trans.GetComponent<RectTransform>().sizeDelta.x;
                }
            }
        }
//		Debug.Log ("GetChildrensAndResize, containerSize = "+containerSize);
		//resize
		if (ResizeDirection == Direction.height) {
            containerSize += GetComponent<VerticalLayoutGroup>().padding.top;
            containerSize += GetComponent<VerticalLayoutGroup>().padding.bottom;
            //			Debug.Log ("containerSize = "+containerSize);
            if (addOffet){
				containerSize += 30f;
			}
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (GetComponent<RectTransform> ().sizeDelta.x, containerSize);

            //GetComponent<VerticalLayoutGroup> ().childForceExpandHeight = false;
            //GetComponent<VerticalLayoutGroup> ().childControlHeight = false;
            //GetComponent<VerticalLayoutGroup> ().childForceExpandHeight = false;
            //GetComponent<VerticalLayoutGroup> ().childControlHeight = false;
        } else {
            containerSize += GetComponent<HorizontalLayoutGroup>().padding.right;
            containerSize += GetComponent<HorizontalLayoutGroup>().padding.left;
            if (addOffet){
				containerSize += 60f;
			}
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (containerSize, GetComponent<RectTransform> ().sizeDelta.y);
			//GetComponent<HorizontalLayoutGroup> ().childForceExpandHeight = false;
			//GetComponent<HorizontalLayoutGroup> ().childControlHeight = false;
			//GetComponent<HorizontalLayoutGroup> ().childForceExpandHeight = true;
			//GetComponent<HorizontalLayoutGroup> ().childControlHeight = true;
		}
	}
}
