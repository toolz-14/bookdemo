﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedActivate : MonoBehaviour {

	public GameObject ObjectToActivate = null;
	public float delayValue = 1f;

	private void Start () {
		
	}

	public void StartDelay(){
		if (ObjectToActivate != null && !ObjectToActivate.activeInHierarchy) {
			Invoke ("DeactivateObject", delayValue);
		}
	}

	private void DeactivateObject(){

		ObjectToActivate.SetActive (true);
	}
}
