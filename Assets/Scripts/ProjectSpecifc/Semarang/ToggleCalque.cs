﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleCalque : MonoBehaviour {

	//swap texture
	public List<GameObject> theTerrain = new List<GameObject>();
	public ResizeContainerWithChildAmount LegendContainer;
	public OneToggleOpenAtSameTimeCalque OneToggle;
	private Toggle _toggle;

	private void Start () {
		_toggle = GetComponent<Toggle> ();
	}

	public void ShowCalque(){
//		if (OneToggle != null) {
//			OneToggle.ResetAllToggles (this);
//		}
		for (int i = 0; i < theTerrain.Count; i++) {
			theTerrain[i].SetActive (true);
		}
		LegendContainer.GetChildrensAndResize ();

	}

	public void HideCalque(){
		for (int i = 0; i < theTerrain.Count; i++) {
			theTerrain[i].SetActive (false);
		}
		LegendContainer.GetChildrensAndResize ();

	}

	public void ToggleButton(){
		_toggle.isOn = !_toggle.isOn;

		if (_toggle.isOn) {
			ShowCalque ();
		} else {
			HideCalque ();
		}
	}

	public void OnToggleClick(){
		if (_toggle.isOn) {
			ShowCalque ();
		} else {
			HideCalque ();
		}
	}
}
