﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeOptionTopView : MonoBehaviour {

	public FreeTerrainCamera FreeTerrainCamera;
	[SerializeField]
	private Transform newPos;
	[SerializeField]
	private Sprite sprite3D;
	[SerializeField]
	private Sprite sprite2D;
	private float XRotation = 90f;
	private float YHeight = 800f;
	private bool isTopView = false;
	private Vector3 newCameraPosition;
	private Vector3 newCameraEulerAngles;
	private Transform CameraHolder;

	private void Awake () {
		isTopView = false;
	}

	private void Start () {
		if(FreeTerrainCamera != null){
			CameraHolder = FreeTerrainCamera.transform.Find ("ConstraintBox").Find ("CameraHolder");
		}
	}

	public void ChangeTheCamera(){
		//start normal
		isTopView = !isTopView;
		//go to tv first
		if (isTopView) {
			MoveCameraTo ();
		}else{
			//then back
			MoveCameraBack();
		}
	}

	private void MoveCameraTo(){
		if (FreeTerrainCamera != null) {
			transform.Find("Image_Icon").GetComponent<Image> ().sprite = sprite2D;
			//go to free cam
			FreeTerrainCamera.SaveCameraValuesBeforeMoveTo ();
			FreeTerrainCamera.PreventMouseTouchInput (true);

//			newCameraPosition = CameraHolder.position;
//			newCameraPosition.y += YHeight;
//
//			newCameraEulerAngles = CameraHolder.eulerAngles;
//			newCameraEulerAngles.x = XRotation;

			FreeTerrainCamera.InverseMoveSens ();

			FreeTerrainCamera.MoveTo (newPos);

			Invoke ("ReEnableInput", 0.1f);
		}
	}

	private void ReEnableInput(){
		if (FreeTerrainCamera != null) {
			FreeTerrainCamera.PreventMouseTouchInput (false);
		}
	}

	private void MoveCameraBack(){
		if (FreeTerrainCamera != null) {
			transform.Find("Image_Icon").GetComponent<Image> ().sprite = sprite3D;
			FreeTerrainCamera.ResetCameraAfterMoveTo ();
			FreeTerrainCamera.InverseMoveSens ();
		}
	}
}
