﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PanoramicViewBouyguesWeb : MonoBehaviour {


    private Camera _camera;
    private float _zoom;
    //private float _baseZoom;

	public float turnSpeed = 4.0f;		// Speed of camera turning when mouse moves in along an axis
	private Vector3 _mouseOrigin;	// Position of cursor when mouse dragging starts
	private bool _isPanning;	// Is the camera being panned?

	public static bool IsRotating;	// Is the camera being rotated?
	private bool _isZooming;        // Is the camera zooming?
#if UNITY_IOS || UNITY_ANDROID
	private Touch _touchZero;
	private Touch _touchOne;
    public float perspectiveTouchZoomSpeed = 0.5f;
#endif
    private Vector2 _touchZeroPrevPos;
	private Vector2 _touchOnePrevPos;
	private float _prevTouchDeltaMag;
	private float _touchDeltaMag;
	private float _deltaMagnitudeDiff;
	private Vector3 _pos;
	private float AngleY;
	private Vector3 temp;
	private float timeMouseDown = 0f;
	public static bool IsDragged;


	private void Awake() {
		Resources.UnloadUnusedAssets();
//			_camera = GetComponent<Camera>();
		//_baseZoom = _camera.fieldOfView;
	}

	private void OnDisable() {

    }

	private void Update() {

		if (EventSystem.current != null) {
			if (IsPointerOverUIObject () == 5 ) {
				return;
			}
		}

		if (Input.touchCount == 2)
		{
#if UNITY_IOS || UNITY_ANDROID
			// Store both touches.
			_touchZero = Input.GetTouch(0);
			_touchOne = Input.GetTouch(1);
			
			// Find the position in the previous frame of each touch.
			_touchZeroPrevPos = _touchZero.position - _touchZero.deltaPosition;
			 _touchOnePrevPos = _touchOne.position - _touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			 _prevTouchDeltaMag = (_touchZeroPrevPos - _touchOnePrevPos).magnitude;
			_touchDeltaMag = (_touchZero.position - _touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			 _deltaMagnitudeDiff = _prevTouchDeltaMag - _touchDeltaMag;

			// Otherwise change the field of view based on the change in distance between the touches.
			_camera.fieldOfView += _deltaMagnitudeDiff * perspectiveTouchZoomSpeed;
			
			// Clamp the field of view to make sure it's between 0 and 180.
			_camera.fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 40f, 80f);
#endif
        }
        else
        {
			// Get the left mouse button
			if(Input.GetMouseButtonDown(0))
			{
				// Get mouse origin
				_mouseOrigin = Input.mousePosition;
				IsRotating = true;
				timeMouseDown = 0f;
			}

			if(Input.GetMouseButton(0)){
				timeMouseDown += Time.deltaTime;
			}

			if(timeMouseDown > 0.3f){
				IsDragged = true;
			}else{
				IsDragged = false;
			}
			
			// Disable movements on button release
			if (!Input.GetMouseButton (0)) {
				IsRotating = false;
				timeMouseDown = 0f;
			}
			
			// Rotate camera along X and Y axis
			if (IsRotating)
			{
				_pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - _mouseOrigin);
				transform.RotateAround(transform.position, Vector3.up, _pos.x * turnSpeed);


				AngleY = -_pos.y * turnSpeed;
				transform.RotateAround(transform.position, transform.right, AngleY);

				temp = transform.localEulerAngles;

				if(temp.x > 50f && temp.x < 260f){
					temp.x = 50f;
				}
				if(temp.x < 300f && temp.x > 260f){
					temp.x = 300f;
				}
				temp.z = 0;
				transform.localEulerAngles = temp;
			}
		}
    }

	protected int IsPointerOverUIObject(){
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		if (results.Count > 0) {
			return results [0].gameObject.layer;
		} else {
			return -1;
		}
	}
}