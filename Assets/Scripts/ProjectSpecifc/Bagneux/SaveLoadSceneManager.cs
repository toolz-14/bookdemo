﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveLoadSceneManager : MonoBehaviour
{
    private Scene scene;
    private static bool fistTimeIn;

    //class instance
    public static SaveLoadSceneManager instance;

    // ButtonMaquette
    [HideInInspector]
    public ButtonMaquette.Bstate buttonState;
    public delegate ButtonMaquette.Bstate OnButtonClickDelegate();
    public static OnButtonClickDelegate saveMaquetteButtonValue;

    // LightCycle
    [HideInInspector]
    public float startSliderValue;
    public delegate float startSliderValueDelegate();
    public static startSliderValueDelegate savestartSliderValue;
    [HideInInspector]
    public int chosenStartDate;
    public delegate int chosenStartDateDelegate();
    public static chosenStartDateDelegate savechosenStartDate;
    [HideInInspector]
    public bool lightCyclePanelState;
    public delegate bool lightCyclePanelStateDelegate();
    public static lightCyclePanelStateDelegate savelightCyclePanelState;

    //main camera rotation + position
    private Vector3 cameraPosition;
    private Vector3 cameraRotation;

    //toggle panel
    [HideInInspector]
    public List<bool> parentIsShowing = new List<bool>();
    [HideInInspector]
    public List<bool> subParentIsShowing = new List<bool>();
    [HideInInspector]
    public List<bool> subSubParentIsShowing = new List<bool>();
    [HideInInspector]
    public List<bool> calquesToggleOn = new List<bool>();

    //timeline

    //3D button -> unsure if should do this as button is kinda useless and i think wil be removed soon

    //Compass set itself from camera, no need to deal with it here


    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            buttonState = ButtonMaquette.Bstate.Textured;

            SceneManager.sceneLoaded += OnSceneLoaded;
            fistTimeIn = false;
            GameObject.FindGameObjectWithTag("ModuleInitial").GetComponent<LaunchStartMenu>().SetMenuActive();
            cameraPosition = Vector3.zero;
            cameraRotation = Vector3.zero;

            startSliderValue = LightCycle24H.instance.startTime;
            chosenStartDate = (int)LightCycle24H.instance.chosenStartDate;
            lightCyclePanelState = LightCycle24H.instance.swap;
        }
        else if(instance!=this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }

    
    public void ChangeScene()
    {
        scene = SceneManager.GetActiveScene();

        if (scene.name == "Main")
        {
            buttonState = saveMaquetteButtonValue();
            startSliderValue = savestartSliderValue();
            chosenStartDate = savechosenStartDate();
            lightCyclePanelState = savelightCyclePanelState();

            //save camera values
            cameraPosition = GameObject.FindGameObjectWithTag("FreeCamera").GetComponent<FreeTerrainCamera>().GetCameraHolder().position;
            cameraRotation = GameObject.FindGameObjectWithTag("FreeCamera").GetComponent<FreeTerrainCamera>().GetCameraHolder().eulerAngles;

            //save toggle panel values
            GameObject.FindGameObjectWithTag("Canvas_Calques_PC").GetComponent<CanvasCalqueBagneux>().GetAllBooleans();
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Main")
        {
            if (fistTimeIn)
            {
                //load camera values
                GameObject.FindGameObjectWithTag("FreeCamera").GetComponent<FreeTerrainCamera>().ForceCameraPosition(cameraPosition, cameraRotation);

                //load toggle panel toggle clicked
                GameObject.FindGameObjectWithTag("Canvas_Calques_PC").GetComponent<CanvasCalqueBagneux>().LoadAllBooleans();

            }
            else
            {
                //should only be called the first time we go to main after app openned 
                fistTimeIn = true;
            }
        }
    }
}
