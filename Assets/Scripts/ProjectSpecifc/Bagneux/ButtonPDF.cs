﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPDF : MonoBehaviour {

    [SerializeField]
    private GameObject subButton_1;
    [SerializeField]
    private GameObject subButton_2;

    private bool swap;


    private void Start () {
        swap = false;
    }

    public void Swap()
    {
        swap = !swap;
        if (swap)
        {
            DisplaySubButtons();
        }
        else
        {
            HideSubButtons();
        }
    }

    private void DisplaySubButtons()
    {
        subButton_1.SetActive(true);
        subButton_2.SetActive(true);
    }

    private void HideSubButtons()
    {
        subButton_1.SetActive(false);
        subButton_2.SetActive(false);
    }
}
