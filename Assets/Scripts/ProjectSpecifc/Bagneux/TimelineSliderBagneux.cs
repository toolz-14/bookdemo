﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TimelineSliderBagneux : MonoBehaviour {

	public Slider slider;
	[HideInInspector]
	public bool startUpdate = false;

    //other ecole calques to deactivated
    [SerializeField]
    private GameObject maternelle2018;
    private bool maternelle2018WasOn;
    [SerializeField]
    private GameObject secteurAffecte;
    private bool secteurAffecteWasOn;

    public GameObject secteur4Tex;
    public GameObject secteur4TWhite;
    public GameObject secteur4BATISliced;
    public GameObject secteur4BATIOutline;

    //insure that L4 is displayed at start
    public GameObject l4;
	//insure that station 'mairie Monrouge' is displayed at start
	[SerializeField]
	private GameObject stationMM;

	//from septembre 2018 to decembre 2030 //carte scolaire
	[SerializeField]
	private List<GameObject> sept2018 = new List<GameObject>();

	//from Septembre 2019 to decembre 2030 //carte scolaire + Ecole Rosenberg
	[SerializeField]
	private List<GameObject> sept2019 = new List<GameObject>();

	//from septembre 2020 to decembre 2030 //Ecole Niki de Saint Phalle
	[SerializeField]
	private List<GameObject> sept2020 = new List<GameObject>();

	//from juin 2021 to decembre 2030 //prolongement Ligne 4 + stations :'Barbara' et 'Bagneux - Lucie Aubrac'
	[SerializeField]
	private List<GameObject> juin2021 = new List<GameObject>();

	//from septembre 2023 to decembre 2030 //Ecole les Mathurins
	[SerializeField]
	private List<GameObject> sept2023 = new List<GameObject>();

	//from janvier 2025 to decembre 2030 //L15
	[SerializeField]
	private List<GameObject> janv2025 = new List<GameObject>();



//	//EXAMPLE from decembre 2018 to mars 2020
//	[SerializeField]
//	private List<GameObject> dec2018mar2020 = new List<GameObject>();

	private bool sept2018_Shown;
	private bool sept2018_Hidden;
	private bool sept2019_Shown;
	private bool sept2019_Hidden;
	private bool sept2020_Shown;
	private bool sept2020_Hidden;
	private bool juin2021_Shown;
	private bool juin2021_Hidden;
	private bool sept2023_Shown;
	private bool sept2023_Hidden;
	private bool janv2025_Shown;
	private bool janv2025_Hidden;


//	EXAMPLE
//	private bool dec2018mar2020_Shown;
//	private bool dec2018mar2020_Hidden;



	//janvier 2018 -> janvier 2030 + slider value 0->1 means 1year = 0.083916 sv and 1 month = 0.006993 sv for a total of 143 months (12years but don't count first moth as is 0)
	private void Update () {
		if (startUpdate) {
			//septembre 2018 to decembre 2030
			if (slider.value >= 0.00001f && slider.value < 0.055944f) {//8months
				//if (SubSubListManager.scolaireIsOn) {
				//	sept2018_Hidden = true;
				//}
                if (PanelAtelier.isTex)
                {
                    secteur4Tex.SetActive(true);
                    secteur4TWhite.SetActive(false);
                }
                else
                {
                    secteur4Tex.SetActive(false);
                    secteur4TWhite.SetActive(true);
                }
            } else if (slider.value >= 0.055944f) {
				//if (SubSubListManager.scolaireIsOn) {
				//	sept2018_Shown = true;
				//}
                if (PanelAtelier.isTex)
                {
                    secteur4Tex.SetActive(true);
                    secteur4TWhite.SetActive(false);
                }
                else
                {
                    secteur4Tex.SetActive(false);
                    secteur4TWhite.SetActive(true);
                }
            }
			if (sept2018_Shown) {
				sept2018_Shown = false;
                if (PanelAtelier.isTex)
                {
                    secteur4Tex.SetActive(true);
                    secteur4TWhite.SetActive(false);
                }
                else
                {
                    secteur4Tex.SetActive(false);
                    secteur4TWhite.SetActive(true);
                }
                foreach (GameObject go in sept2018) {
					go.SetActive (true);
				}
			}
			if(sept2018_Hidden){
				sept2018_Hidden = false;
				foreach (GameObject go in sept2018) {
					go.SetActive (false);
				}
			}

			//septembre 2019 to decembre 2030
			if (slider.value >= 0.00001f && slider.value < 0.13986f) {//20months
				sept2019_Hidden = true;
			} else if (slider.value >= 0.13986f) {
				sept2019_Shown = true;
				//if (SubSubListManager.scolaireIsOn) {
				//	sept2018_Hidden = true;
				//}
			}
			if (sept2019_Shown) {
				sept2019_Shown = false;
				sept2019[1].SetActive (true);
                sept2019[2].SetActive(true);
                secteur4Tex.SetActive(false);
                secteur4TWhite.SetActive(false);
                sept2020[1].SetActive(false);
                juin2021[2].SetActive(false);
                //if (SubSubListManager.scolaireIsOn) {
				//	sept2019[0].SetActive (true);
				//}
			}
			if(sept2019_Hidden){
				sept2019_Hidden = false;
				foreach (GameObject go in sept2019) {
					go.SetActive (false);
				}
			}

			//septembre 2020 to decembre 2030
			if (slider.value >= 0.00001f && slider.value < 0.223776f) {//32months
				sept2020_Hidden = true;
			} else if (slider.value >= 0.223776f) {
				sept2020_Shown = true;
			}
			if (sept2020_Shown) {
				sept2020_Shown = false;
                sept2019[2].SetActive(false);
                juin2021[2].SetActive(false);
                foreach (GameObject go in sept2020) {
					go.SetActive (true);
				}
			}
			if(sept2020_Hidden){
				sept2020_Hidden = false;
				foreach (GameObject go in sept2020) {
					go.SetActive (false);
				}
			}

			//juin 2021 to decembre 2030
			if (slider.value >= 0.00001f && slider.value < 0.286713f) {//41months
				juin2021_Hidden = true;
			} else if (slider.value >= 0.286713f) {
				juin2021_Shown = true;
			}
			if (juin2021_Shown) {
				juin2021_Shown = false;
                sept2019[2].SetActive(false);
                sept2020[1].SetActive(false);
                foreach (GameObject go in juin2021) {
					go.SetActive (true);
				}
			}
			if(juin2021_Hidden){
				juin2021_Hidden = false;
				foreach (GameObject go in juin2021) {
					go.SetActive (false);
				}
			}

			//septembre 2023 to decembre 2030
			if (slider.value >= 0.00001f && slider.value < 0.475524f) {//68months
				sept2023_Hidden = true;
			} else if (slider.value >= 0.475524f) {
				sept2023_Shown = true;
			}
			if (sept2023_Shown) {
				sept2023_Shown = false;
				foreach (GameObject go in sept2023) {
					go.SetActive (true);
				}
			}
			if(sept2023_Hidden){
				sept2023_Hidden = false;
				foreach (GameObject go in sept2023) {
					go.SetActive (false);
				}
			}

			//janvier 2025 to decembre 2030
			if (slider.value >= 0.00001f && slider.value < 0.580419f) {//83months
				janv2025_Hidden = true;
			} else if (slider.value >= 0.580419f) {
				janv2025_Shown = true;
			}
			if (janv2025_Shown) {
				janv2025_Shown = false;
				foreach (GameObject go in janv2025) {
					go.SetActive (true);
				}
			}
			if(janv2025_Hidden){
				janv2025_Hidden = false;
				foreach (GameObject go in janv2025) {
					go.SetActive (false);
				}
			}


//			EXAMPLE: decembre 2018 to mars 2020
//			if (slider.value >= 0.00001f && slider.value < 0.076351f) {
//				dec2018mar2020_Hidden = true;
//			} else if (slider.value >= 0.076351f && slider.value < 0.187407f) {
//				dec2018mar2020_Shown = true;
//			} else if (slider.value >= 0.187407f) {
//				dec2018mar2020_Hidden = true;
//			}
//
//			if (dec2018mar2020_Shown) {
//				dec2018mar2020_Shown = false;
//				foreach (GameObject go in dec2018mar2020) {
//					go.SetActive (true);
//				}
//			}
//			if(dec2018mar2020_Hidden){
//				dec2018mar2020_Hidden = false;
//				foreach (GameObject go in dec2018mar2020) {
//					go.SetActive (false);
//				}
//			}
		}
	}

	public void ForcecloseAndReset(bool solZAC5 = true)
    {
        if (TimlineLerpBagneux.isOpen)
        {
            GetComponent<TimlineLerpBagneux>().StartLerpTimelineClose(solZAC5);
            slider.value = 0f;
            if (l4.activeInHierarchy)
            {
                l4.SetActive(false);
            }
            if (stationMM.activeInHierarchy)
            {
                stationMM.SetActive(false);
            }
            foreach (GameObject go in sept2018)
            {
                go.SetActive(false);
            }
            foreach (GameObject go in sept2019)
            {
                go.SetActive(false);
            }
            foreach (GameObject go in sept2020)
            {
                go.SetActive(false);
            }
            foreach (GameObject go in juin2021)
            {
                go.SetActive(false);
            }
            foreach (GameObject go in sept2023)
            {
                go.SetActive(false);
            }
            foreach (GameObject go in janv2025)
            {
                go.SetActive(false);
            }
        }
	}

	public void ForceReset(){
		slider.value = 0f;
        secteur4BATISliced.SetActive(false);
        secteur4BATIOutline.SetActive(false);
        if (PanelAtelier.isTex)
        {
            secteur4Tex.SetActive(true);
            secteur4TWhite.SetActive(false);
        }
        else
        {
            secteur4Tex.SetActive(false);
            secteur4TWhite.SetActive(true);
        }
        if (!l4.activeInHierarchy){
			l4.SetActive (true);
		}
		if(!stationMM.activeInHierarchy){
			stationMM.SetActive (true);
		}
		foreach (GameObject go in sept2018) {
			go.SetActive (false);
		}
		foreach (GameObject go in sept2019) {
			go.SetActive (false);
		}
		foreach (GameObject go in sept2020) {
			go.SetActive (false);
		}
		foreach (GameObject go in juin2021) {
			go.SetActive (false);
		}
		foreach (GameObject go in sept2023) {
			go.SetActive (false);
		}
		foreach (GameObject go in janv2025) {
			go.SetActive (false);
		}


//		EXAMPLE
//		foreach (GameObject go in dec2018mar2020) {
//			go.SetActive (false);
//		}
	}
}
