﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCycleButton : MonoBehaviour {

	[SerializeField]
	private GameObject theObject;
	private bool swap;

	private void Start () {
		swap = false;
	}
	
	public void StartLightCycle(){
		swap = !swap;
		if (swap) {
			theObject.SetActive (true);
		} else {
			theObject.SetActive (false);
		}
	}

	public void LightCycleForceClose(){
		if(swap){
			theObject.SetActive (false);
			swap = false;
		}
	}
}
