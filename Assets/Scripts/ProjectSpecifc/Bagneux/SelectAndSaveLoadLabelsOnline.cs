﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Toolz.Triggers;

// Script that manages interactions with labels outside labelEditor (saving labels, loading labels, buttons...)
public class SelectAndSaveLoadLabelsOnline : MonoBehaviour
{
    [SerializeField] private LabelEditorSaveLoad labelEditorSaveLoad;
    [SerializeField] private LabelEditor labelEditor;

    Toolz.WebServices.ApplicationLogin appLogin;
    Toolz.WebServices.User user;

    public static GameObject selectedLabel;
	[SerializeField]
	private ChatUIManagerLabel chatUIManagerLabel;
    [SerializeField]
    private Button buttonOpenLabelEditor;
    [SerializeField]
    private GameObject popUpValidationExit;
    [SerializeField]
    private GameObject panelAtelier;
    [SerializeField]
    private GameObject panelPresentationAtelier;
    [SerializeField]
    private TimelineSliderBagneux timeLine;
    [SerializeField]
    private LightCycle24H lightCycle;
    //[SerializeField]
    //private Button buttonSaveLabelEditor;

    private RaycastHit hit;
    private Ray ray;
    private bool doOnce;
    private bool atelierWasActive;
    private bool lightCycleWasActive;

    private void Awake()
    {
        // WebService initialization
        user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>(); // Get user
    }

    private void Start()
    {
        lightCycleWasActive = false;
        atelierWasActive = false;
        doOnce = false;
		//Invoke ("Delayed2", 1f);     
    }
    /*
	private void Delayed2(){
		LoadOnline();
	}*/

    private void Update()
    {
        if (Camera.main != null)
        {
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            }
            else
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            }
            if (Input.GetButtonDown("Fire1"))
            {
                if (IsPointerOverUIObject() == 5)
                {
                    return;
                }
                if (Physics.Raycast(ray, out hit, 10000f))
                {
                    if (hit.transform.tag != "EditableLabel" || hit.transform.tag == null)
                    {
                        if (selectedLabel != null)
                        { // Deselect a label
                            if(selectedLabel.GetComponent<LabelEditorObject>() != null)
                            selectedLabel.GetComponent<LabelEditorObject>().Outline.enabled = false;
                            else selectedLabel.GetComponent<Outline>().enabled = false;

                            selectedLabel = null;
                            //Debug.Log("deselect");
                            if (chatUIManagerLabel.IsOpen)
                            {
                                chatUIManagerLabel.CloseChatPanel();
                            }
                        }
                    }
                    else if (hit.transform.tag == "EditableLabel")
                    {

                        if (!doOnce)
                        {
                            doOnce = true;

                            if (selectedLabel != null)
                            { // Deselect a label
                                if (selectedLabel.GetComponent<LabelEditorObject>() != null)
                                    selectedLabel.GetComponent<LabelEditorObject>().Outline.enabled = false;
                                else selectedLabel.GetComponent<Outline>().enabled = false;

                                selectedLabel = null;
                            }
                            selectedLabel = hit.transform.gameObject;
                            if (selectedLabel.GetComponent<LabelEditorObject>() != null)
                                selectedLabel.GetComponent<LabelEditorObject>().Outline.enabled = true;
                            else selectedLabel.GetComponent<Outline>().enabled = true;

//                        discussionUIManagerLabel.OpenDiscussionPanel(false, selectedLabel.GetComponent<LabelEditorObject>());
                            chatUIManagerLabel.OpenChatPanel(selectedLabel.GetComponentInParent<LabelEditorObject>());
                            Invoke("Delayed", 0.05f);
                        }
                    }
                }
                else
                {
                    //Debug.Log ("2");
                }
            }
        }
    }

    protected int IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (results.Count > 0)
        {
            return results[0].gameObject.layer;
        }
        else
        {
            return -1;
        }
    }

    public void SaveOnline()
    {
        if (user.idUser != -1)// && (Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin
        {
            labelEditorSaveLoad.ButtonSave();
        }
        else
        {
            labelEditorSaveLoad.DisplayErrorMessage("Vous n'êtes pas connecté");
        }
    }

    public void LoadOnline()
    {
        labelEditorSaveLoad.ButtonLoad();
        /*
        if (user.idUser != -1) //  User is already logged in 
        {
        }
        else
        {
            labelEditorSaveLoad.DisplayErrorMessage("Vous n'êtes pas connecté");
        }*/
    }

    private void Delayed()
    {
        doOnce = false;
    }

    public void DeselectLabelsWithComments()
    {
        if (selectedLabel != null)
        { // Deselect a label
            if (selectedLabel.GetComponent<LabelEditorObject>() != null)
                selectedLabel.GetComponent<LabelEditorObject>().Outline.enabled = false;
            else selectedLabel.GetComponent<Outline>().enabled = false;

            selectedLabel = null;
        }
        if (chatUIManagerLabel.IsOpen)
        {
            chatUIManagerLabel.CloseChatPanel();
        }
    }

    public void OpenLabelEditor()
    {
        if (panelAtelier.activeInHierarchy)
        {
            atelierWasActive = true;
            panelAtelier.SetActive(false);
            panelPresentationAtelier.SetActive(false);
        }

        timeLine.ForcecloseAndReset(false);


        lightCycle.ClosePanel();

        if (user.idUser != -1)// (Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin
        {
            buttonOpenLabelEditor.GetComponent<TransitionTrigger>().TriggerTransition();
            labelEditor.OpenLabelEditor();

            if ((Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus != Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
            {
                labelEditor.AddALabel();
            }
        }
        else // will never hapen since the button will not be displayed
        {
            labelEditorSaveLoad.DisplayErrorMessage("Vous n'êtes pas connecté");
        }
    }

    public void DisplayLabelButton()
    {
        user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>(); // Get user
        if (user.idUser != -1) // && (Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin
        {
            buttonOpenLabelEditor.gameObject.SetActive(true);
            //buttonSaveLabelEditor.gameObject.SetActive(true);
        }
    }
    
    public void UnDisplayLabelButton()
    {
        buttonOpenLabelEditor.gameObject.SetActive(false);
        //buttonSaveLabelEditor.gameObject.SetActive(false);
    }

    public void ExitTheLabelEditor()
    {
        if (atelierWasActive)
        {
            atelierWasActive = false;
            panelAtelier.SetActive(true);
            panelPresentationAtelier.SetActive(true);
        }
        Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>();
        if ((Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
        {
            popUpValidationExit.transform.Find("Image").Find("Button_OK").GetComponent<Button>().onClick.Invoke();
        }
        else
        {
            popUpValidationExit.SetActive(true);
        }
    }
}
