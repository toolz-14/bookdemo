﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimlineLerpBagneux : MonoBehaviour {

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;

	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private Vector2 _startPos;
	private Vector2 _startPos_BSchemat;

	[SerializeField]
	private GameObject BlockCameraOverlay;
    [SerializeField]
    private LightCycle24H lightCycleButton;

    public static bool isOpen;

    [SerializeField]
    private GameObject panelAtelier;
    [SerializeField]
    private GameObject panelAtelierPresentation;
    private bool panelAtelierWasOpen;

    [SerializeField]
    private GameObject solZAC5;
    private bool solZAC5WasOpen;

    [SerializeField]
    private GameObject hotspotsFPS;
    private bool hotspotsFPSWasOpen;


    private void Start () {
        isOpen = false;
        solZAC5WasOpen = false;
        hotspotsFPSWasOpen = false;
        _startPos = GetComponent<RectTransform> ().anchoredPosition;
	}

	private void Update () {
		if(_launch){
			OpenTimelinePanel();
		}
		if(_launch2){
			CloseTimelinePanel();
		}
	}

	public void StartLerpTimelineOpen(){
        if (panelAtelier.activeInHierarchy)
        {
            panelAtelier.SetActive(false);
            panelAtelier.GetComponent<PanelAtelier>().CloseAtelierPanel();
            panelAtelierPresentation.SetActive(false);
        }
        if (solZAC5.activeInHierarchy)
        {
            solZAC5WasOpen = true;
            solZAC5.SetActive(false);
        }
        if (hotspotsFPS.activeInHierarchy)
        {
            hotspotsFPSWasOpen = true;
            hotspotsFPS.SetActive(false);
        }
        lightCycleButton.LightCycleForceClose();
        GetComponent<TimelineSliderBagneux>().ForceReset ();
		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().anchoredPosition;
		_to1 = new Vector2 (GetComponent<RectTransform> ().anchoredPosition.x, 47f);
		_launch = true;
	}

	private void OpenTimelinePanel(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f){
			_launch = false;
			TimelineSwapButtonBagneux.doOnce = false;
			BlockCameraOverlay.SetActive (true);
			GetComponent<TimelineSliderBagneux> ().startUpdate = true;
            isOpen = true;
        }
	}

	public void StartLerpTimelineClose(bool solZAC5 = true)
    {
        solZAC5WasOpen = solZAC5;
        _timeSinceTransitionStarted2 = 0f;
		_from2 = GetComponent<RectTransform> ().anchoredPosition;
		_to2 = _startPos;
		BlockCameraOverlay.SetActive (false);
		_launch2 = true;
	}

	private void CloseTimelinePanel(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
            isOpen = false;
            GetComponent<TimelineSliderBagneux>().l4.SetActive(false);
            TimelineSwapButtonBagneux.isOpen = false;
            TimelineSwapButtonBagneux.doOnce = false;
			GetComponent<TimelineSliderBagneux> ().startUpdate = false;
            GetComponent<TimelineSliderBagneux>().ForceReset();
            if (ShowHidePanelAtelier.isAtelierOn && !LabelEditor.isOpen)
            {
                panelAtelier.SetActive(true);
                panelAtelierPresentation.SetActive(true);
            }
            if (solZAC5WasOpen)
            {
                solZAC5WasOpen = false;
                solZAC5.SetActive(true);
            }
            if (hotspotsFPSWasOpen)
            {
                hotspotsFPSWasOpen = false;
                hotspotsFPS.SetActive(true);
            }
        }
	}
}
