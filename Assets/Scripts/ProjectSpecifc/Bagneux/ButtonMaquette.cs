﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonMaquette : MonoBehaviour
{

    public enum Bstate { Textured, Maquette, Plan }
    [HideInInspector]
    public Bstate buttonState = Bstate.Textured;

    //displayed button sprite
    [SerializeField]
    private Sprite spriteTextured;
    [SerializeField]
    private Sprite spriteMaquette;
    [SerializeField]
    private Sprite spritePlan;

    //2D
    [SerializeField]
    private GameObject orthoPhoto;
    [SerializeField]
    private GameObject plan;

    //3D
    //[SerializeField]
    //private GameObject zACWhite;
    [SerializeField]
    private GameObject grues;
    [SerializeField]
    private List<GameObject> modeEcoles = new List<GameObject>();
    [SerializeField]
    private List<GameObject> modeExtra = new List<GameObject>();
    //[SerializeField]
    //private GameObject wireFrameZAC;
    [SerializeField]
    private GameObject modeZACBATI;
    [SerializeField]
    private GameObject cityWhite;
    [SerializeField]
    private GameObject cityTextured;
    [SerializeField]
    private GameObject quartiersNordWhite;
    [SerializeField]
    private GameObject quartiersNordTextured;
    [SerializeField]
    private GameObject secteur4White;
    [SerializeField]
    private GameObject secteur4Tex;
    [SerializeField]
    private GameObject secteur4BATISliced;
    [SerializeField]
    private GameObject secteur4BATIOutlined;

    //access to secteur4 mode
    [SerializeField]
    private PanelAtelier atelierUI;

    public static bool wireframeIsOn;


    private void InitButton()
    {
        buttonState = SaveLoadSceneManager.instance.buttonState;
        ActiveButtonState();
    }

    private void Start()
    {
        InitButton();
    }

    private void OnEnable()
    {
        SaveLoadSceneManager.saveMaquetteButtonValue += SaveButtonMaquetteState;
    }
    private void OnDisable()
    {
        SaveLoadSceneManager.saveMaquetteButtonValue -= SaveButtonMaquetteState;
    }

    private Bstate SaveButtonMaquetteState()
    {
        return buttonState;
    }


    public void ActiveButtonState()
    {
        switch ((int)buttonState)
        {
            case 0:
                Textured();
                break;
            case 1:
                Maquette();
                break;
            case 2:
                Plan();
                break;
            default:
                Textured();
                break;
        }
    }

    public void SwapButtonState()
    {
        buttonState = buttonState + 1;

        if ((int)buttonState > 2)
            buttonState = 0;

        switch ((int)buttonState)
        {
            case 0:
                Textured();
                break;
            case 1:
                Maquette();
                break;
            case 2:
                Plan();
                break;
            default:
                Textured();
                break;
        }
    }

    //change le sol en orthophoto, et la 3D en texturé
    private void Textured()
    {
        GetComponent<Image>().sprite = spriteTextured;
        orthoPhoto.SetActive(true);

        SetAllMode();
    }

    // change le sol en plan, et la 3D en White
    private void Maquette()
    {
        GetComponent<Image>().sprite = spriteMaquette;
        orthoPhoto.SetActive(false);

        SetAllMode();
    }

    // change le sol en plan, et retire la 3D
    private void Plan()
    {
        GetComponent<Image>().sprite = spritePlan;
        orthoPhoto.SetActive(false);

        SetAllMode();
    }

    //to be called by the programmation toggle (CanvasCalquePC) OnValueChanged event;
    public void EnableDisableWireFrame(Toggle toggle)
    {
        wireframeIsOn = toggle.isOn;

        if (wireframeIsOn)
        {
            modeZACBATI.SetActive(false);
            atelierUI.blockModeSwap = true;
            atelierUI.ResetPhaseObject();
        }
        else
        {
            atelierUI.blockModeSwap = false;
            atelierUI.SetDisplayedObjects(atelierUI.currentSelectedButton);
            modeZACBATI.SetActive(true);
        }
    }

    private void SetAllMode()
    {
        //wireframe ON
        if (wireframeIsOn)
        {
            //textured
            if ((int)buttonState == 0)
            {
                PanelAtelier.isTex = true;
                //other mode
                grues.SetActive(true);
                cityWhite.SetActive(false);
                cityTextured.SetActive(true);
                quartiersNordWhite.SetActive(false);
                quartiersNordTextured.SetActive(true);
                foreach (GameObject go in modeEcoles)
                {
                    go.SetActive(true);
                }
                foreach (GameObject go in modeExtra)
                {
                    go.SetActive(true);
                }
            }
            //maquette
            else if ((int)buttonState == 1)
            {
                PanelAtelier.isTex = false;
                //other mode
                grues.SetActive(true);
                cityWhite.SetActive(true);
                cityTextured.SetActive(false);
                quartiersNordWhite.SetActive(true);
                quartiersNordTextured.SetActive(false);
                foreach (GameObject go in modeEcoles)
                {
                    go.SetActive(true);
                }
                foreach (GameObject go in modeExtra)
                {
                    go.SetActive(true);
                }
            }
            //plan
            else if ((int)buttonState == 2)
            {
                //other mode
                grues.SetActive(false);
                cityWhite.SetActive(false);
                cityTextured.SetActive(false);
                quartiersNordWhite.SetActive(false);
                quartiersNordTextured.SetActive(false);
                foreach (GameObject go in modeEcoles)
                {
                    go.SetActive(false);
                }
                foreach (GameObject go in modeExtra)
                {
                    go.SetActive(false);
                }
            }
        }
        //wireframe OFF
        else
        {
            //textured
            if ((int)buttonState == 0)
            {
                PanelAtelier.isTex = true;
                //secteur4 mode + can swap
                atelierUI.blockModeSwap = false;
                atelierUI.SetDisplayedObjects(atelierUI.currentSelectedButton);

                //if wireframe not actif
                modeZACBATI.SetActive(true);

                //other mode
                grues.SetActive(true);
                cityWhite.SetActive(false);
                cityTextured.SetActive(true);
                quartiersNordWhite.SetActive(false);
                quartiersNordTextured.SetActive(true);
                foreach (GameObject go in modeEcoles)
                {
                    go.SetActive(true);
                }
                foreach (GameObject go in modeExtra)
                {
                    go.SetActive(true);
                }
            }
            // maquette
            else if ((int)buttonState == 1)
            {
                PanelAtelier.isTex = false;
                //secteur4 mode + can swap
                atelierUI.blockModeSwap = false;
                atelierUI.SetDisplayedObjects(atelierUI.currentSelectedButton);

                //if wireframe not actif
                modeZACBATI.SetActive(true);

                //other mode
                grues.SetActive(true);
                cityWhite.SetActive(true);
                cityTextured.SetActive(false);
                quartiersNordWhite.SetActive(true);
                quartiersNordTextured.SetActive(false);
                foreach (GameObject go in modeEcoles)
                {
                    go.SetActive(true);
                }
                foreach (GameObject go in modeExtra)
                {
                    go.SetActive(true);
                }
            }
            //plan
            else if ((int)buttonState == 2)
            {
                //secteur4 hide all mode + cannot swap
                atelierUI.blockModeSwap = true;
                atelierUI.ResetPhaseObject();

                //ZAC
                modeZACBATI.SetActive(false);

                //other mode
                grues.SetActive(false);
                cityWhite.SetActive(false);
                cityTextured.SetActive(false);
                quartiersNordWhite.SetActive(false);
                quartiersNordTextured.SetActive(false);
                foreach (GameObject go in modeEcoles)
                {
                    go.SetActive(false);
                }
                foreach (GameObject go in modeExtra)
                {
                    go.SetActive(false);
                }
            }
        }
    }
}
