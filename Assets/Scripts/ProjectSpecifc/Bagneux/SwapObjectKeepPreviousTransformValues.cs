﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapObjectKeepPreviousTransformValues : MonoBehaviour {

    [SerializeField]
    private Transform newObjectPrefab;

	private void Start () {
		
	}

    public void SwapTheObjects()
    {
        if (newObjectPrefab != null)
        {
            List<Vector3> savedPosition = new List<Vector3>();
            List<Quaternion> savedRotationn = new List<Quaternion>();
            List<Vector3> savedScale = new List<Vector3>();
            List<string> savedName = new List<string>();

			foreach (Transform trans in transform)
            {
                savedPosition.Add(trans.position);
                savedRotationn.Add(trans.rotation);
                savedScale.Add(trans.localScale);
                savedName.Add(trans.name);
            }

            for (int i = this.transform.childCount; i > 0; --i)
            {
                DestroyImmediate(this.transform.GetChild(0).gameObject);
            }


            for (int i = 0; i < savedPosition.Count; i++)
            {
                GameObject temp = Instantiate(newObjectPrefab.gameObject) as GameObject;
                temp.transform.SetParent(transform);
                temp.transform.localScale = savedScale[i];
                temp.transform.position = savedPosition[i];
                temp.transform.rotation = savedRotationn[i];
                temp.transform.name = savedName[i];
            }
        }
    }
}
