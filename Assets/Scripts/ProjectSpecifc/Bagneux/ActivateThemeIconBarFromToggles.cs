﻿using UnityEngine;

public class ActivateThemeIconBarFromToggles : MonoBehaviour {

    public static bool themeBarOpenFromToggle;

	private void Start () {
        themeBarOpenFromToggle = false;
    }

	public void Activate(bool isOn){
        themeBarOpenFromToggle = isOn;
	}
}
