﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPhase5Plan3D : MonoBehaviour
{
    [SerializeField]
    private GameObject button3d;
    [SerializeField]
    private GameObject holder;

    [SerializeField]
    private GameObject[] Batis3D = new GameObject[7];
    [SerializeField]
    private GameObject[] L4p1_Plans = new GameObject[9];
    [SerializeField]
    private GameObject[] L4p2_Plans = new GameObject[9];
    [SerializeField]
    private GameObject[] L5_Plans = new GameObject[9];
    [SerializeField]
    private GameObject[] L8_Plans = new GameObject[9];
    [SerializeField]
    private GameObject[] C1p1_Plans = new GameObject[9];
    [SerializeField]
    private GameObject[] C1p2_Plans = new GameObject[9];
    [SerializeField]
    private GameObject[] OPPrive_Plans = new GameObject[9];

    //move camera to top
    private float XRotation = 90f;
    private float ZRotation = 0f;
    private float YRotation = 194f;

    private float savedXRotation = 0f;
    private float savedZRotation = 0f;
    private float savedYRotation = 0f;

    private float YPos = 90.4f;
    private float ZPos = 77f;
    private float XPos = -65f;

    private float savedXPos = 0f;
    private float savedZPos = 0f;
    private float savedYPos = 0f;

    [SerializeField]
    private FreeTerrainCamera FreeTerrainCamera;
    private Vector3 newCameraPosition;
    private Vector3 newCameraEulerAngles;
    private Transform CameraHolder;

    private bool swapPlan;
    private bool doOnce;


    private void Start()
    {
        swapPlan = false;
        doOnce = false;
        if (FreeTerrainCamera != null)
        {
            CameraHolder = FreeTerrainCamera.transform.Find("ConstraintBox").Find("CameraHolder");
        }
    }

    private void OnEnable()
    {
        swapPlan = false;
        doOnce = false;
        holder.SetActive(false);
        button3d.transform.Find("Text").GetComponent<Text>().text = "PLAN";
        foreach (GameObject go in Batis3D)
        {
            go.SetActive(true);
        }
    }

    public void ActivateMainButton(bool active)
    {
        button3d.SetActive(active);
        if (!active)
        {
            holder.SetActive(false);
            ResetAllPlans();
            foreach (GameObject go in Batis3D)
            {
                go.SetActive(false);
            }
        }
        else
        {
            swapPlan = false;
            doOnce = false;
            button3d.transform.Find("Text").GetComponent<Text>().text = "PLAN";
            foreach (GameObject go in Batis3D)
            {
                go.SetActive(true);
            }
        }
    }

    public void MainButtonOnclick()
    {
        if (!doOnce)
        {
            doOnce = true;
            swapPlan = !swapPlan;
            if (swapPlan)
            {
                holder.SetActive(true);
                button3d.transform.Find("Text").GetComponent<Text>().text = "3D";
                RDC_Onclick();
                foreach (GameObject go in Batis3D)
                {
                    go.SetActive(false);
                }
                MoveCameraToTop();
            }
            else
            {
                holder.SetActive(false);
                button3d.transform.Find("Text").GetComponent<Text>().text = "PLAN";
                ResetAllPlans();
                foreach (GameObject go in Batis3D)
                {
                    go.SetActive(true);
                }
                MoveCameraBack();
            }
            Invoke("Delayed", 0.2f);
        }
    }

    private void Delayed()
    {
        doOnce = false;
    }

    public void SousSol_Onclick()
    {
        SetThePlans(0);
    }

    public void RDC_Onclick()
    {
        SetThePlans(1);
    }

    public void R1_Onclick()
    {
        SetThePlans(2);
    }

    public void R2_Onclick()
    {
        SetThePlans(3);
    }

    public void R3_Onclick()
    {
        SetThePlans(4);
    }

    public void R4_Onclick()
    {
        SetThePlans(5);
    }

    public void R5_Onclick()
    {
        SetThePlans(6);
    }

    public void R6_Onclick()
    {
        SetThePlans(7);
    }

    public void R7_Onclick()
    {
        SetThePlans(8);
    }

    private void SetThePlans(int i)
    {
        ResetAllPlans();

        if (L4p1_Plans[i] != null)
        {
            L4p1_Plans[i].SetActive(true);
        }
        if (L4p2_Plans[i] != null)
        {
            L4p2_Plans[i].SetActive(true);
        }
        if (L5_Plans[i] != null)
        {
            L5_Plans[i].SetActive(true);
        }
        if (L8_Plans[i] != null)
        {
            L8_Plans[i].SetActive(true);
        }
        if (C1p1_Plans[i] != null)
        {
            C1p1_Plans[i].SetActive(true);
        }
        if (C1p2_Plans[i] != null)
        {
            C1p2_Plans[i].SetActive(true);
        }
        if (OPPrive_Plans[i] != null)
        {
            OPPrive_Plans[i].SetActive(true);
        }
    }

    private void ResetAllPlans()
    {
        foreach (GameObject go in L4p1_Plans)
        {
            if(go != null)
            {
                go.SetActive(false);
            }
        }

        foreach (GameObject go in L4p2_Plans)
        {
            if (go != null)
            {
                go.SetActive(false);
            }
        }
        foreach (GameObject go in L5_Plans)
        {
            if (go != null)
            {
                go.SetActive(false);
            }
        }

        foreach (GameObject go in L8_Plans)
        {
            if (go != null)
            {
                go.SetActive(false);
            }
        }
        foreach (GameObject go in C1p1_Plans)
        {
            if (go != null)
            {
                go.SetActive(false); 
            }
        }
        foreach (GameObject go in C1p2_Plans)
        {
            if (go != null)
            {
                go.SetActive(false);
            }
        }
        foreach (GameObject go in OPPrive_Plans)
        {
            if (go != null)
            {
                go.SetActive(false);
            }
        }
    }

    private void MoveCameraToTop()
    {
        //save stuff
        savedXPos = CameraHolder.position.x;
        savedYPos = CameraHolder.position.y;
        savedZPos = CameraHolder.position.z;
        savedXRotation = CameraHolder.eulerAngles.x;
        savedZRotation = CameraHolder.eulerAngles.z;
        savedYRotation = CameraHolder.eulerAngles.y;

        //move
        FreeTerrainCamera.PreventMouseTouchInput(true);
        newCameraPosition = Vector3.zero;
        newCameraPosition.x = XPos;
        newCameraPosition.y = YPos;
        newCameraPosition.z = ZPos;

        newCameraEulerAngles = Vector3.zero;
        newCameraEulerAngles.x = XRotation; 
        newCameraEulerAngles.z = ZRotation;
        newCameraEulerAngles.y = YRotation;

        FreeTerrainCamera.MoveTo(newCameraPosition, newCameraEulerAngles);

        FreeTerrainCamera.InverseMoveSens();

        Invoke("ReEnableInput", 0.1f);
    }

    private void MoveCameraBack()
    {
        newCameraPosition = Vector3.zero;
        newCameraPosition.y = savedYPos;
        newCameraPosition.z = savedZPos;
        newCameraPosition.x = savedXPos;

        newCameraEulerAngles = Vector3.zero;
        newCameraEulerAngles.x = savedXRotation;
        newCameraEulerAngles.z = savedZRotation;
        newCameraEulerAngles.y = savedYRotation;

        FreeTerrainCamera.MoveTo(newCameraPosition, newCameraEulerAngles);

        FreeTerrainCamera.InverseMoveSens();

        Invoke("ReEnableInput", 0.1f);
    }

    private void ReEnableInput()
    {
        if (FreeTerrainCamera != null)
        {
            FreeTerrainCamera.PreventMouseTouchInput(false);
        }
    }
}
