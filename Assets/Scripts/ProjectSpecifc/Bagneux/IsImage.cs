﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class IsImage : MonoBehaviour
{

    public bool LoadFromStreaming;
    [HideInInspector]
    public UnityWebRequest www;
    [HideInInspector]
    public MeshRenderer theRenderer;

    [SerializeField]
    private float cameraStartY = 0f;
    [SerializeField]
    private float cameraStartX = 0f;
    public Transform theCamera;
    public GameObject Loader = null;
    public bool StartwithImage;
    public GameObject theImage;
    public string NamesOfWantedImage;
    public string NamesOfWanted360;
    public string NamesOfWantedBundle;

    private bool NotFirstTime;
    private string subFolder = "";
    //private GameManagerBagneux GameManagerBagneux;
    private Dictionary<string, string> headers = new Dictionary<string, string>();

    private void Start()
    {

    }

    private void OnEnable()
    {
        if (NotFirstTime)
        {
            if (StartwithImage)
            {
                if (NamesOfWantedImage != "")
                {
                    theImage.SetActive(true);
                    theImage.transform.Find("Image").GetComponent<Image>().sprite = (Sprite)Resources.Load("ProjectImages/"/* + subFolder*/ + NamesOfWantedImage, typeof(Sprite));
                }
            }
            else
            {
                //if bundle already loaded don't reload (error otherwise)
                if ((TheBundle.instance.loadedBundle == null) || (TheBundle.instance.loadedBundle != null && !TheBundle.instance.loadedBundle.name.Equals(NamesOfWantedBundle)))
                {
                    if (!LoadFromStreaming)
                    {
                        //set 360 from bundle
                        StartCoroutine(GetAssetBundle());
                        //if (GameObject.FindGameObjectWithTag("GameManager") != null)
                        //{
                        //    GameManagerBagneux = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerBagneux>();
                        //    GetFirst360();
                        //}
                    }
                    else
                    {
                        GetBundleFromStreaming();
                    }
                }
                else if (TheBundle.instance.loadedBundle != null && TheBundle.instance.loadedBundle.name.Equals(NamesOfWantedBundle))
                {
                    if (!LoadFromStreaming)
                    {
                        Texture a360 = TheBundle.instance.loadedBundle.LoadAsset<Texture>(NamesOfWanted360);
                        if (a360 != null)
                        {
                            Instantiate(a360);
                            theRenderer = transform.Find("Sphere_Bg").GetComponent<MeshRenderer>();
                            theRenderer.material.mainTexture = a360;
                            Reset360CamPosition();
                        }
                        else
                        {
                            Debug.Log("Asset bundle:" + TheBundle.instance.loadedBundle.name + " could not find object: " + NamesOfWanted360);
                        }
                    }
                    else
                    {
                        GetBundleFromStreaming();
                    }
                }
            }
        }
        else
        {
            NotFirstTime = true;
            if (GetComponentInParent<Project>() != null)
            {
                if (GetComponentInParent<Project>().is2018)
                {
                    subFolder = "2018/";
                }
                //else if (GetComponentInParent<Project>().is2019)
                //{
                //    subFolder = "2019/";
                //}
            }
        }
    }

    private void OnDisable()
    {
        //theRenderer = transform.Find ("Sphere_Bg").GetComponent<MeshRenderer>();
        //theRenderer.material.mainTexture = null;
    }

    //private void GetFirst360(){
    //	theRenderer = transform.Find ("Sphere_Bg").GetComponent<MeshRenderer>();
    //	if (NamesOfWanted360 != "") {
    //		Texture a360 = GameManagerBagneux.loadedBundle.LoadAsset<Texture> (NamesOfWanted360);
    //		if (a360 != null) {
    //			Instantiate (a360);
    //			theRenderer.material.mainTexture = a360;
    //		}
    //	}
    //}

    IEnumerator GetAssetBundle()
    {
        theRenderer = transform.Find("Sphere_Bg").GetComponent<MeshRenderer>();
        if (NamesOfWantedBundle != "")
        {
            TheBundle.instance.bundleIsLoading = true;
            if (Loader != null)
            {
                Loader.SetActive(true);
            }
#if UNITY_ANDROID
            www = UnityWebRequest.GetAssetBundle("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_Android/" + NamesOfWantedBundle);
#elif UNITY_IOS
			www = UnityWebRequest.GetAssetBundle ("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_iOS/" + NamesOfWantedBundle);
#elif UNITY_WEBGL
            //www = UnityWebRequest.GetAssetBundle ("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_Webgl/" + subFolder + NamesOfWantedBundle);
            //www = UnityWebRequest.GetAssetBundle("http://toolz.mairie-bagneux.fr:8080/Bagneux2019_BundleBP_Webgl/" + subFolder + NamesOfWantedBundle);
            www = UnityWebRequest.GetAssetBundle("https://budgetparticipatif.bagneux92.fr/carte3d//Bagneux2019_BundleBP_Webgl/" + subFolder + NamesOfWantedBundle);
#elif UNITY_STANDALONE
            www = UnityWebRequest.GetAssetBundle("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_PC/" + subFolder + NamesOfWantedBundle);
#endif
            Reset360CamPosition();
            //Debug.Log ("GetAssetBundle before sendwebrequest, bundle path = "+ "http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_Webgl/" + subFolder + NamesOfWantedBundle);
            www.timeout = 50;
            //www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            //www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            //www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            //www.SetRequestHeader("Access-Control-Allow-Origin", "*");

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("bundle loading error: " + www.error);
                if (Loader != null)
                {
                    Loader.SetActive(false);
                }
            }
            else
            {

                if (Loader != null)
                {
                    Loader.SetActive(false);
                }

                if (TheBundle.instance.loadedBundle != null)
                {
                    TheBundle.instance.loadedBundle.Unload(false);
                    Caching.ClearCache();
                    TheBundle.instance.loadedBundle = null;
                }

                TheBundle.instance.loadedBundle = DownloadHandlerAssetBundle.GetContent(www);

                //				Debug.Log ("Got Asset Bundle from Server");
                Resources.UnloadUnusedAssets();

                Texture a360 = TheBundle.instance.loadedBundle.LoadAsset<Texture>(NamesOfWanted360);
                if (a360 != null)
                {
                    Instantiate(a360);
                    theRenderer.material.mainTexture = a360;
                    //  Debug.Log("GetAssetBundle, Asset bundle:" + loadedBundle.name + " found object: " + NamesOfWanted360);
                }
                else
                {
                    Debug.Log("GetAssetBundle, Asset bundle:" + TheBundle.instance.loadedBundle.name + " could not find object: " + NamesOfWanted360);
                }
                TheBundle.instance.bundleIsLoading = false;
            }
        }
    }

    public void StopGetAssetBundle()
    {
        if (Loader != null)
        {
            Loader.SetActive(false);
        }
        StopCoroutine(GetAssetBundle());
    }


    private void GetBundleFromStreaming()
    {
        TheBundle.instance.bundleIsLoading = true;
        //		Debug.Log ("Got Asset Bundle from StreamingAssets");
        Resources.UnloadUnusedAssets();

        if (Loader != null)
        {
            Loader.SetActive(false);
        }
        if (TheBundle.instance.loadedBundle != null)
        {
            TheBundle.instance.loadedBundle.Unload(false);
            Caching.ClearCache();
            TheBundle.instance.loadedBundle = null;
        }
        TheBundle.instance.loadedBundle = AssetBundle.LoadFromFile(Application.streamingAssetsPath + "/" + NamesOfWantedBundle);
        Texture a360 = TheBundle.instance.loadedBundle.LoadAsset<Texture>(NamesOfWanted360);
        if (a360 != null)
        {
            Instantiate(a360);
            theRenderer.material.mainTexture = a360;
            Reset360CamPosition();
        }
        else
        {
            Debug.Log("GetBundleFromStreaming, Asset bundle:" + TheBundle.instance.loadedBundle.name + " could not find object: " + NamesOfWanted360);
            if (Loader != null)
            {
                Loader.SetActive(false);
            }
        }
        TheBundle.instance.bundleIsLoading = false;
    }

    public void Reset360CamPosition()
    {
        Vector3 temp = theCamera.localEulerAngles;
        temp.y = cameraStartY;
        temp.x = cameraStartX;
        theCamera.localEulerAngles = temp;
    }
}
