﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewsSecteur4 : MonoBehaviour {

    [SerializeField]
    private Color onColor;
    [SerializeField]
    private Color offColor;
    [SerializeField]
    private GameObject view_1;
    [SerializeField]
    private GameObject view_2;
    [SerializeField]
    private GameObject view_3;
    [SerializeField]
    private GameObject view_4;

    [SerializeField]
    private List<Image> buttonViews = new List<Image>();


    private void Start () {
		
	}

    public void OnCamerabuttonClick(int index)
    {
         if (index == 1)
        {
            view_1.SetActive(true);
            view_2.SetActive(false);
            view_3.SetActive(false);
            view_4.SetActive(false);
        }
        else if(index == 2)
        {
            view_1.SetActive(false);
            view_2.SetActive(true);
            view_3.SetActive(false);
            view_4.SetActive(false);
        }
        else if (index == 3)
        {
            view_1.SetActive(false);
            view_2.SetActive(false);
            view_3.SetActive(true);
            view_4.SetActive(false);
        }
        else if (index == 4)
        {
            view_1.SetActive(false);
            view_2.SetActive(false);
            view_3.SetActive(false);
            view_4.SetActive(true);
        }

        SetButtonColor(index);
    }

    private void SetButtonColor(int index)
    {
        for(int i=0; i< buttonViews.Count; i++)
        {
            if (i == index-1)
            {
                buttonViews[i].color = onColor;
            }
            else
            {
                buttonViews[i].color = offColor;
            }
        }
    }
}
