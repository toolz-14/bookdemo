﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Toolz.WebServices;

public class SceneInitPanel : MonoBehaviour
{

    [SerializeField]
    private GameObject ButtonModeration;
    [SerializeField]
    private GameObject Holder;
    [SerializeField]
    private ProfileUIManager profileUIManager;
    [SerializeField]
    private InterfaceButton optionsInterfaceButton;
    [SerializeField]
    private InterfaceButton optionsArbres;
    [SerializeField]
    private CanvasCalqueBagneux canvasCalqueBagneux;
    [SerializeField]
    private GameObject calquesGroup_BudgetParticipatif;
    [SerializeField]
    private GameObject calquesGroup_ZACVHugo;
    [SerializeField]
    private GameObject calquesGroup_LabelZAC;
    [SerializeField]
    private GameObject calquesGroup_LabelSecteur4;
    [SerializeField]
    private GameObject calquesGroup_CarteScolaire;
    [SerializeField]
    private GameObject calquesGroup_PLU;
    [SerializeField]
    private GameObject calquesGroup_Transport;
    [SerializeField]
    private GameObject calquesGroup_Services;
    [SerializeField]
    private Transform viewBudgetParticipatif;
    [SerializeField]
    private Transform viewZACVHugo;
    [SerializeField]
    private Transform viewCarteScolaire;
    [SerializeField]
    private Transform viewToutVoir;
    //[SerializeField]
    //private ShowHide3D showHide3D;
    [SerializeField]
    private TimelineSliderBagneux timelineSliderBagneux;
    [SerializeField]
    private LightCycle24H lightCycleButton;
    //[SerializeField]
    //private ButtonShematic3DBagneux buttonShematic3DBagneux;
    //[SerializeField]
    //private ButtonModeTexture buttonModeTexture;
    [SerializeField]
    private GameObject lieuxLabellises;
    [SerializeField]
    private List<DisplayCalquePC> togglesLotsZac = new List<DisplayCalquePC>();


    //TODO make common to all buttons method for code readability

    private void Start()
    {
        Holder = transform.Find("Holder").gameObject;
    }

    public void OnBudgetParticipatifClick()
    {
        ButtonModeration.SetActive(false);
        //close profile
        profileUIManager.CloseProfilePanel();
        //reset theme options
        optionsInterfaceButton.Reset();
        optionsArbres.Reset();
        //timeline reset
        timelineSliderBagneux.ForcecloseAndReset();
        //lighcycle close
        lightCycleButton.LightCycleForceClose();
        //reset toggles
        //canvasCalqueBagneux.ResetAllToggles();
        //deal with calques
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().isShowing = false;
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_BudgetParticipatif.SetActive(true);
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_ZACVHugo.SetActive(false);
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_CarteScolaire.SetActive(false);
        calquesGroup_PLU.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_PLU.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_PLU.SetActive(false);
        calquesGroup_Transport.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_Transport.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Transport.SetActive(false);
        calquesGroup_Services.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_Services.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Services.SetActive(false);

        canvasCalqueBagneux.OpenPanelNormal();
        lieuxLabellises.SetActive(true);
        //activate all toggles for both lots groups
        ShowHideTogglesLotsZAC(false);
        //showHide3D.ForceShow3D ();
        //buttonShematic3DBagneux.Force3D ();

        Holder.SetActive(false);

        GetComponent<ButtonChangeCameraView>().SectorPosition = viewBudgetParticipatif;
        GetComponent<ButtonChangeCameraView>().ChangeTheCamera();
    }

    public void OnZACVHugoClick()
    {

        //if is logged as admin then show button
        User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>();
        if ((ApplicationLogin.ApplicationLoginStatus)user.userStatus == ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
        {
            ButtonModeration.SetActive(true);
        }
        //reset theme options
        optionsInterfaceButton.Reset();
        optionsArbres.Reset();
        //timeline reset
        timelineSliderBagneux.ForcecloseAndReset();
        //lighcycle close
        lightCycleButton.LightCycleForceClose();
        //reset toggles
        //canvasCalqueBagneux.ResetAllToggles();
        //deal with calques
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_BudgetParticipatif.SetActive(false);
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceOpenToggle();
        calquesGroup_ZACVHugo.SetActive(true);
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_CarteScolaire.SetActive(false);
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_PLU.SetActive(false);
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Transport.SetActive(false);
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Services.SetActive(false);

        canvasCalqueBagneux.OpenPanelNormal();
        lieuxLabellises.SetActive(false);
        //activate all toggles for both lots groups
        //		ShowHideTogglesLotsZAC(true);
        //showHide3D.ForceShow3D ();
        //buttonShematic3DBagneux.Force3D ();
        //buttonModeTexture.ForceTex();


        Holder.SetActive(false);

        GetComponent<ButtonChangeCameraView>().SectorPosition = viewZACVHugo;
        GetComponent<ButtonChangeCameraView>().ChangeTheCamera();
    }

    public void OnCarteScolaireClick()
    {
        ButtonModeration.SetActive(false);
        //reset theme options
        optionsInterfaceButton.Reset();
        optionsArbres.Reset();
        //timeline reset
        timelineSliderBagneux.ForcecloseAndReset();
        //lighcycle close
        lightCycleButton.LightCycleForceClose();
        //reset toggles
        //canvasCalqueBagneux.ResetAllToggles();
        //deal with calques
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_BudgetParticipatif.SetActive(false);
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_ZACVHugo.SetActive(false);
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceOpenToggle();
        calquesGroup_CarteScolaire.SetActive(true);
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_PLU.SetActive(false);
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Transport.SetActive(false);
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Services.SetActive(false);

        canvasCalqueBagneux.OpenPanelNormal();
        lieuxLabellises.SetActive(false);
        //activate all toggles for both lots groups
        ShowHideTogglesLotsZAC(false);
        //showHide3D.ForceHide3D ();
        //buttonShematic3DBagneux.Force3D ();

        Holder.SetActive(false);

        GetComponent<ButtonChangeCameraView>().SectorPosition = viewCarteScolaire;
        GetComponent<ButtonChangeCameraView>().ChangeTheCamera();
    }

    public void OnCarteProjetsUrbainsClick()
    {
        ButtonModeration.SetActive(false);
        //reset theme options
        optionsInterfaceButton.Reset();
        optionsArbres.Reset();
        //timeline reset
        timelineSliderBagneux.ForcecloseAndReset();
        //lighcycle close
        lightCycleButton.LightCycleForceClose();
        //reset toggles
        //canvasCalqueBagneux.ResetAllToggles();
        //deal with calques
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_BudgetParticipatif.SetActive(false);
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_ZACVHugo.SetActive(false);
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_CarteScolaire.SetActive(false);
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_PLU.SetActive(false);
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Transport.SetActive(false);
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Services.SetActive(false);

        canvasCalqueBagneux.OpenPanelNormal();
        lieuxLabellises.SetActive(false);
        //activate all toggles for both lots groups
        ShowHideTogglesLotsZAC(false);
        //showHide3D.ForceShow3D ();
        //buttonShematic3DBagneux.ForceSchematic ();

        Holder.SetActive(false);
    }

    public void OnPLUClick()
    {
        ButtonModeration.SetActive(false);
        //reset theme options
        optionsInterfaceButton.Reset();
        optionsArbres.Reset();
        //timeline reset
        timelineSliderBagneux.ForcecloseAndReset();
        //lighcycle close
        lightCycleButton.LightCycleForceClose();
        //reset toggles
        //canvasCalqueBagneux.ResetAllToggles();
        //deal with calques
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_BudgetParticipatif.SetActive(false);
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_ZACVHugo.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_ZACVHugo.SetActive(false);
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_CarteScolaire.SetActive(false);
        calquesGroup_PLU.GetComponent<SubSubListManager>().ForceOpenToggle();
        calquesGroup_PLU.SetActive(true);
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Transport.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Transport.SetActive(false);
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceCloseToggle();
        calquesGroup_Services.GetComponent<SubSubListManager>().ForceOff();
        calquesGroup_Services.SetActive(false);

        canvasCalqueBagneux.OpenPanelNormal();
        lieuxLabellises.SetActive(false);
        //activate all toggles for both lots groups
        ShowHideTogglesLotsZAC(false);
        //showHide3D.ForceShow3D ();
        //buttonShematic3DBagneux.ForceSchematic ();

        Holder.SetActive(false);
    }

    public void OnToutVoirClick()
    {
        //if is logged as admin then show button
        User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>();
        if ((ApplicationLogin.ApplicationLoginStatus)user.userStatus == ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
        {
            ButtonModeration.SetActive(true);
        }
        //deal with calques
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_BudgetParticipatif.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_BudgetParticipatif.SetActive(true);

        calquesGroup_ZACVHugo.SetActive(true);
        calquesGroup_LabelZAC.GetComponent<SubSubSubListManager>().ForceOff();
        calquesGroup_LabelSecteur4.GetComponent<SubSubSubListManager>().ForceOff();

        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_CarteScolaire.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_CarteScolaire.SetActive(true);
        calquesGroup_PLU.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_PLU.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_PLU.SetActive(true);
        calquesGroup_Transport.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_Transport.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_Transport.SetActive(true);
        calquesGroup_Services.GetComponent<SubSubListManager>().isShowing = true;
        calquesGroup_Services.GetComponent<SubSubListManager>().ShowToggle();
        calquesGroup_Services.SetActive(true);

        canvasCalqueBagneux.OpenPanelNormal();
        ShowHideTogglesLotsZAC(false);

        Holder.SetActive(false);

        GetComponent<ButtonChangeCameraView>().SectorPosition = viewToutVoir;
        GetComponent<ButtonChangeCameraView>().ChangeTheCamera();
    }

    public void ShowHideTogglesLotsZAC(bool show)
    {
        if (show)
        {
            foreach (DisplayCalquePC dcp in togglesLotsZac)
            {
                dcp.ForceOn();
            }
        }
        else
        {
            foreach (DisplayCalquePC dcp in togglesLotsZac)
            {
                dcp.ForceOff();
            }
        }
    }
}
