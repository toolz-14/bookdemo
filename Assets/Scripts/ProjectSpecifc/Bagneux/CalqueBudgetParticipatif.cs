﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalqueBudgetParticipatif : MonoBehaviour {

	[SerializeField]
	private List<string> projectIDs = new List<string>();
	[SerializeField]
	private ThemeUIManager themeUIManager;

	private void Start () {

	}

	public void Activate(bool isOn){
		themeUIManager.SelectDisplayedProject (projectIDs, isOn);
	}
}
