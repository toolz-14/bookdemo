﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Toolz.WebServices;
using System.Collections.Generic;

public class ResponseUIManager : MonoBehaviour {

	public DiscussionUIManagerLabel discussionUIManager;
	public GlobalErrorManager GlobalErrorManager;
	public ResponseInputUIManager ResponseInputUIManager;
	public GameObject ResponseItem;
	public GameObject Panel_KeyboardInput;

	private GameObject _tempObject;
	private Vector2 _tempVect2;
	public static bool IsOpen;

    private List<GameObject> _responseObjects;
    private GetCommentReplies _getCommentRepliesService;

    private void Awake () {
        _getCommentRepliesService = GameObject.FindGameObjectWithTag("WebServices").GetComponent<GetCommentReplies>();
        _getCommentRepliesService.onComplete += _getCommentRepliesService_onComplete;
        _getCommentRepliesService.onError += _getCommentRepliesService_onError;

        _responseObjects = new List<GameObject>();
        IsOpen = false;
	}

    public void OpenResponsePanel(uint idComment){
        _getCommentRepliesService.idComment = idComment;
        _getCommentRepliesService.UseWebService();

	}
	
	public void CloseResponsePanel(){
		discussionUIManager.MoveDiscussionPanelRight ();

	}

	private void SetResponsePanel(uint idComment){
        //Clean response objects's list
        foreach (GameObject go in _responseObjects) {
            Destroy(go);
        }
        _responseObjects.Clear();

		string originalCommentAuthorFullName = _getCommentRepliesService.OriginalCommentAuthorName;
        //set Caption
		transform.Find("VLayout").Find("Header").Find("Caption_2").GetComponent<Text>().text = originalCommentAuthorFullName;

		//set footer button onclick
		transform.Find ("VLayout").Find ("Footer").Find ("Buttons").Find ("ValidateBt").GetComponent<Button>().onClick.AddListener(delegate{ResponseInputUIManager.gameObject.SetActive(true);ResponseInputUIManager.SetLabelText("reponse a " + originalCommentAuthorFullName);});
		transform.Find("VLayout").Find("Footer").Find("Buttons").Find("ValidateBt").GetComponent<Button>().onClick.AddListener(() => { Panel_KeyboardInput.SetActive(true);ResponseInputUIManager.gameObject.SetActive(true); ResponseInputUIManager.PrepareToReply((int)idComment, 0); });

		//set response list
		int i = 0;
		
        List<Reply> replies = _getCommentRepliesService.Replies;
        foreach (Reply reply in replies) {
			_tempObject = (GameObject)Instantiate (ResponseItem);
			_tempObject.SetActive (true);

			//seprarator disabled in first comment
			if(i == 0){
				_tempObject.transform.Find("Separator").gameObject.SetActive(false);
			}
			
			//set user name 
			_tempObject.transform.Find ("UserName").GetComponent<Text>().text = reply.firstname + " " + reply.name;
			
			//set comment text 
            _tempObject.transform.Find("Text").GetComponent<Text>().text = reply.text;

			//set date (published time)
			string date = reply.publishedTime.ToString("dd/MM/yyyy HH:mm");
			_tempObject.transform.Find("Date").GetComponent<Text>().text = date;

            int idUser = GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser;
			Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>(); // Get user
			//if not admin hide validation buttons
			if (idUser == -1 || (idUser != -1 && (Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus != Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN)) { //  User is admin
				_tempObject.transform.Find ("ValidationHolder").gameObject.SetActive (false);
			} else
            {
                if (reply.status == 1) // comment has already been validated
                    _tempObject.transform.Find("ValidationHolder").gameObject.SetActive(false);
                else
                {
                    Button deleteButton = _tempObject.transform.Find("ValidationHolder").Find("Button_DeleteItem").GetComponent<Button>();
                    Button validateButton = _tempObject.transform.Find("ValidationHolder").Find("Button_ValidateItem").GetComponent<Button>();

                    deleteButton.onClick.AddListener(() => OnClickDeleteCommentButton(deleteButton, validateButton, reply.idReply, reply.idReplyAuthor, (int)reply.idOriginalComment, reply.idAnsweredRep, reply.text, true));

                    validateButton.onClick.AddListener(() => OnClickValidateCommentButton(deleteButton, validateButton, reply.idReply, reply.idReplyAuthor, (int)reply.idOriginalComment, reply.idAnsweredRep, reply.text, false));
                }
			}

			//set button if direct reply to the comment
            Button responseButton = _tempObject.transform.Find("ResponseBt").GetComponent<Button>();
            if (reply.idAnsweredRep == 0) {
                if (idUser != -1) {
                    //if response is NOT from user set onclick
                    if (reply.idReplyAuthor != idUser) {
                        responseButton.onClick.AddListener(
                            delegate 
                            {
								ResponseInputUIManager.gameObject.SetActive(true);
								ResponseInputUIManager.SetLabelText("reponse a USER");
								ResponseInputUIManager.PrepareToReply((int)idComment, reply.idReply);
                            });
                        responseButton.onClick.AddListener(() => Panel_KeyboardInput.SetActive(true));
                    }
                    else {
                        responseButton.onClick.AddListener(() => ShowglobalError());
                        responseButton.onClick.AddListener(() => ShowglobalError());
                    }

                }
                else {
                    responseButton.onClick.AddListener(() => ShowglobalError());
                    responseButton.onClick.AddListener(() => ShowglobalError());
                }
            }
            else {
                _tempObject.transform.Find("ResponseBt").gameObject.SetActive(false);
            }


			//set parent
			_tempObject.transform.SetParent(ResponseItem.transform.parent);
			_tempObject.GetComponent<RectTransform>().localScale = new Vector3(1f,1f,1f);
			//set position
			_tempVect2.x = 0;
			_tempVect2.y = i * (-230f);
			_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
			_tempObject.GetComponent<RectTransform>().offsetMax = new Vector2(-1f, _tempObject.GetComponent<RectTransform>().offsetMax.y);
			_tempObject.GetComponent<RectTransform>().offsetMin = new Vector2(-1f, _tempObject.GetComponent<RectTransform>().offsetMin.y);
			++i;

            _responseObjects.Add(_tempObject);
		}

		//modify container height depending on amount of comments (so it can scroll properly)
		ResponseItem.transform.parent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (ResponseItem.transform.parent.GetComponent<RectTransform> ().sizeDelta.x, i*230f);
	}

	public void OnClickDeleteCommentButton(Button delete, Button validate, int idReply, int idAuthor, int idComment, int idAnsweredReply, string msg, bool isDeleted)
    {

		delete.interactable = false;
		validate.interactable = false;
        ResponseInputUIManager.UpdateReply(idAuthor, idComment, idReply, idAnsweredReply, msg, isDeleted);
    }

    public void OnClickValidateCommentButton(Button delete, Button validate, int idReply, int idAuthor, int idComment, int idAnsweredReply, string msg, bool isDeleted)
    {

		delete.interactable = false;
		validate.interactable = false;
        ResponseInputUIManager.UpdateReply(idAuthor, idComment, idReply, idAnsweredReply, msg, isDeleted);
    }

    public void ShowglobalError(){
		GlobalErrorManager.gameObject.SetActive (true);
		GlobalErrorManager.OpenGlobalErrorPanel();
	}

    private void _getCommentRepliesService_onError(long status, string message) {
        Debug.Log("GetCommentReplies service Error : " + message);
    }

    private void _getCommentRepliesService_onComplete(long status, string message) {
        if (status == (long)WebService.Status.OK) {
            SetResponsePanel(_getCommentRepliesService.idComment);
        }
    }
}
