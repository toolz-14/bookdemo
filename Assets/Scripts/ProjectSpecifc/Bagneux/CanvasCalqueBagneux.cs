﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasCalqueBagneux : MonoBehaviour {

	private bool calquesWasOpenBeforeEcole;
	public static bool isOpen;
    [SerializeField]
    private Transform container;
    [SerializeField]
	private GameObject panel;
	[SerializeField]
	private GameObject button;
	[SerializeField]
	private ThemeUIManager themeUIManager;
    [SerializeField]
    private GameObject themeIconBar;
    [SerializeField]
	private OptionsDisplayManager optionsDisplayManager;
	[SerializeField]
	private List<SubSubListManager> togglesParent = new List<SubSubListManager>();
    [SerializeField]
    private List<SubSubSubListManager> togglesSubParent = new List<SubSubSubListManager>();
    [SerializeField]
    private List<SubSubSubSubListManager> togglesSubSubParent = new List<SubSubSubSubListManager>();

    private List<DisplayCalquePC> displayCalques = new List<DisplayCalquePC>();
    private List<bool> parentIsShowing = new List<bool>();
    private List<bool> subParentIsShowing = new List<bool>();
    private List<bool> subSubParentIsShowing = new List<bool>();
    private List<bool> calquesToggleOn = new List<bool>();

    private void Start () {
		isOpen = true;
		calquesWasOpenBeforeEcole = false;
    }

	public void OpenPanelNormal(){
		panel.SetActive (true);
		button.SetActive (false);
		isOpen = true;
        if (themeIconBar.activeInHierarchy)
        {
            themeIconBar.SetActive(false);
        }
	}

	public void ClosePanelNormal(){
		panel.SetActive (false);
		button.SetActive (true);
		isOpen = false;
        if (!themeIconBar.activeInHierarchy && ActivateThemeIconBarFromToggles.themeBarOpenFromToggle)
        {
            themeIconBar.SetActive(true);
        }
	}

	public void OpenPanelFromEcole(){
		if (calquesWasOpenBeforeEcole) {
			calquesWasOpenBeforeEcole = false;
			panel.SetActive (true);
            if (themeIconBar.activeInHierarchy)
            {
                themeIconBar.SetActive(false);
            }
        } else {
			button.SetActive (true);
            if (!themeIconBar.activeInHierarchy && ActivateThemeIconBarFromToggles.themeBarOpenFromToggle)
            {
                themeIconBar.SetActive(true);
            }
        }
        themeUIManager.OpenThemePanel();
    }

	public void ClosePanelFromEcole(){
		if (panel.activeInHierarchy) {
			calquesWasOpenBeforeEcole = true;
			panel.SetActive (false);
		} else {
			calquesWasOpenBeforeEcole = false;
			button.SetActive (false);
		}
        if (themeIconBar.activeInHierarchy)
        {
            themeIconBar.SetActive(false);
        }
        themeUIManager.CloseThemePanel();
    }

	public void ResetAllToggles(){
		foreach (SubSubListManager sslm in togglesParent) {
			sslm.ForceOff ();
            sslm.ForceCloseToggle();
        }
	}

    public void GetAllBooleans()
    {
        //clean
        parentIsShowing.Clear();
        subParentIsShowing.Clear();
        subSubParentIsShowing.Clear();
        calquesToggleOn.Clear();
        SaveLoadSceneManager.instance.parentIsShowing.Clear();
        SaveLoadSceneManager.instance.subParentIsShowing.Clear();
        SaveLoadSceneManager.instance.subSubParentIsShowing.Clear();
        SaveLoadSceneManager.instance.calquesToggleOn.Clear();

        //fill
        foreach (Transform tr in container)
        {
            if (tr.GetComponent<DisplayCalquePC>() != null)
            {
                displayCalques.Add(tr.GetComponent<DisplayCalquePC>());
            }
        }
        for (int i = 0; i < togglesParent.Count; i++)
        {
            parentIsShowing.Add(togglesParent[i].isShowing);
        }
        for (int i = 0; i < togglesSubParent.Count; i++)
        {
            subParentIsShowing.Add(togglesSubParent[i].isShowing);
        }
        for (int i = 0; i < togglesSubSubParent.Count; i++)
        {
            subSubParentIsShowing.Add(togglesSubSubParent[i].isShowing);
        }
        for (int i = 0; i < displayCalques.Count; i++)
        {
            calquesToggleOn.Add(displayCalques[i].toggleOn);
        }

        //save to scene manager
        SaveLoadSceneManager.instance.parentIsShowing = new List<bool>(parentIsShowing);
        SaveLoadSceneManager.instance.subParentIsShowing = new List<bool>(subParentIsShowing);
        SaveLoadSceneManager.instance.subSubParentIsShowing = new List<bool>(subSubParentIsShowing);
        SaveLoadSceneManager.instance.calquesToggleOn = new List<bool>(calquesToggleOn);
    }

    public void LoadAllBooleans()
    {
        foreach (Transform tr in container)
        {
            if (tr.GetComponent<DisplayCalquePC>() != null)
            {
                displayCalques.Add(tr.GetComponent<DisplayCalquePC>());
            }
        }

        //set toggle open
        for (int i = 0; i < SaveLoadSceneManager.instance.parentIsShowing.Count; i++)
        {
            if (SaveLoadSceneManager.instance.parentIsShowing[i])
            {
                togglesParent[i].ForceOpenToggle();
            }
        }
        for (int i = 0; i < SaveLoadSceneManager.instance.subParentIsShowing.Count; i++)
        {
            if (SaveLoadSceneManager.instance.subParentIsShowing[i])
            {
                togglesSubParent[i].ForceOpenToggle();
            }
        }
        for (int i = 0; i < SaveLoadSceneManager.instance.subSubParentIsShowing.Count; i++)
        {
            if (SaveLoadSceneManager.instance.subSubParentIsShowing[i])
            {
                togglesSubSubParent[i].ForceOpenToggle();
            }
        }

        //set toggle on
        for (int i = 0; i < SaveLoadSceneManager.instance.calquesToggleOn.Count; i++)
        {
            if (SaveLoadSceneManager.instance.calquesToggleOn[i])
            {
                if (i < displayCalques.Count)
                {
                    displayCalques[i].ForceOn();
                }
            }
        }
    }
}
