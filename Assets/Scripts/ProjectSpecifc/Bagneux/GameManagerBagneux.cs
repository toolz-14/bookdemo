﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class GameManagerBagneux : MonoBehaviour {

	public static GameManagerBagneux instance = null;
	[HideInInspector]
	public AssetBundle loadedBundle = null;
	[HideInInspector]
	public UnityWebRequest www;
	[HideInInspector]
	public bool BundleIsLoaded = false;
	public string NamesOfWantedBundle;

	private void Awake(){
		
		//Check if instance already exists
		if (instance == null){

			//if not, set instance to this
			instance = this;
		}

		//If instance already exists and it's not this:
		else if (instance != this){

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);
		}

		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);

	}

	private void Start(){
        StartCoroutine("GetAssetBundle");

    }

	IEnumerator GetAssetBundle() {

		if (NamesOfWantedBundle != "") {

//			if (Loader != null) {
//				Loader.SetActive (true);
//			}
			#if UNITY_STANDALONE
			www = UnityWebRequest.GetAssetBundle ("http://preprod.toolz.fr/internal/BagneuxBundlesWindows/" + NamesOfWantedBundle);
#elif UNITY_ANDROID
			www = UnityWebRequest.GetAssetBundle ("http://preprod.toolz.fr/internal/BagneuxBundlesAndroid2/" + NamesOfWantedBundle);
#endif

            yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log ("bundle loading error: " + www.error);
//				if (Loader != null) {
//					Loader.SetActive (false);
//				}
			} else {

				if (loadedBundle != null) {
					loadedBundle.Unload (false);
					Caching.ClearCache ();
					loadedBundle = null;
				}

				loadedBundle =  DownloadHandlerAssetBundle.GetContent (www);
				BundleIsLoaded = true;
					Debug.Log ("Got Asset Bundle from Server");
				Resources.UnloadUnusedAssets ();
//				if (Loader != null) {
//					Loader.SetActive (false);
//				}
			}
		}
	}
}
