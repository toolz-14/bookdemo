﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFpsCamera : MonoBehaviour {

    [SerializeField]
    private GameObject fpsCamera1;
    [SerializeField]
    private GameObject fpsCamera2;
    [SerializeField]
    private GameObject fpsCamera3;
    [SerializeField]
    private GameObject fpsCamera4;
    [SerializeField]
    private GameObject fpsCamera5;
    [SerializeField]
    private GameObject fpsCamera6;


    private void Start () {
		
	}

    public void SetCameraActive(int index)
    {
        fpsCamera1.SetActive(false);
        fpsCamera2.SetActive(false);
        fpsCamera3.SetActive(false);
        fpsCamera4.SetActive(false);
        fpsCamera5.SetActive(false);
        fpsCamera6.SetActive(false);

        if (index == 1)
        {
            fpsCamera1.SetActive(true);
        }
        else if(index == 2)
        {
            fpsCamera2.SetActive(true);
        }
        else if (index == 3)
        {
            fpsCamera3.SetActive(true);
        }
        else if (index == 4)
        {
            fpsCamera4.SetActive(true);
        }
        else if (index == 5)
        {
            fpsCamera5.SetActive(true);
        }
        else if (index == 6)
        {
            fpsCamera6.SetActive(true);
        }
    }
}
