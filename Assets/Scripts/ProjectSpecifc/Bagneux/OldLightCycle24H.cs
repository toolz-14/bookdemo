﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*
 * MARS sunrise = 7h (S.Value = 0.0416f), sunset = 19h (S.Value = 0.5408f)
 * JUIN sunrise = 6h (S.Value = 0f), sunset = 22h (S.Value = 0.6656f)
 * SEPTEMBRE sunrise = 7h30 (S.Value = 0.0624f), sunset = 20h (S.Value = 0.5824f)
 * DECEMBRE sunrise = 9h (S.Value = 0.1248f), sunset = 17h (S.Value = 0.4576f)
 * 
 */
public class OldLightCycle24H : MonoBehaviour
{

    [SerializeField]
    private GameObject holder;
    [SerializeField]
    private TimelineSliderBagneux timeLine;
    [SerializeField]
    private float marsRotationX = 40f;
    [SerializeField]
    private float juinRotationX = 60f;
    [SerializeField]
    private float septembreRotationX = 40f;
    [SerializeField]
    private float decembreRotationX = 20f;
    [SerializeField]
    private Animation AnimationLight;
    [SerializeField]
    private Transform AnimationObject;
    [SerializeField]
    private Slider theSlider;
    public Color colorOn;
    public Color colorOff;

    public static bool isOpen;
    private bool startUpdate;
    private string chosenAnimationClipName = "";
    private bool swap;


    private void Start()
    {
        swap = false;
        isOpen = false;
        theSlider.value = SaveLoadSceneManager.instance.startSliderValue;
        //chosenAnimationClipName = SaveLoadSceneManager.instance.chosenAnimationClipName;
        AnimationLight[chosenAnimationClipName].time = (theSlider.value * AnimationLight[chosenAnimationClipName].length);
        startUpdate = true;
        switch (chosenAnimationClipName)
        {
            case "21mars":
                if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Mars") != null)
                {
                    transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOn;
                }
                break;
            case "21Juin":
                if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Juin") != null)
                {
                    transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOn;
                }
                break;
            case "21Septembre":
                if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Septembre") != null)
                {
                    transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOn;
                }
                break;
            case "21Decembre":
                if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Decembre") != null)
                {
                    transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOn;
                }
                break;
        }
    }
		
	private void OnEnable()
    {
       // if (chosenAnimationClipName.Equals(""))
        //    chosenAnimationClipName = SaveLoadSceneManager.instance.chosenAnimationClipName;
        AnimationLight.Play(chosenAnimationClipName);
        AnimationLight[chosenAnimationClipName].speed = 0;
        startUpdate = true;
        SaveLoadSceneManager.savestartSliderValue += SavestartSliderValue;
       // SaveLoadSceneManager.savechosenAnimationClipName += SavechosenAnimationClipName;
    }

    public float SavestartSliderValue()
    {
        return theSlider.value;
    }

    public string SavechosenAnimationClipName()
    {
        return chosenAnimationClipName;
    }

    public void SwaptLightCycle()
    {
        swap = !swap;
        if (swap)
        {
            OpenPanel();
        }
        else
        {
            ClosePanel();
        }
    }

    public void LightCycleForceClose()
    {
        if (swap)
        {
            ClosePanel();
            swap = false;
        }
    }

    public void OpenPanel()
    {
        holder.SetActive(true);
        isOpen = true;
        if (TimlineLerpBagneux.isOpen)
        {
            timeLine.ForcecloseAndReset();
        }
    }

    public void ClosePanel()
    {
        holder.SetActive(false);
        isOpen = false;
    }

    private void OnDisable()
    {
        startUpdate = false;
        SaveLoadSceneManager.savestartSliderValue -= SavestartSliderValue;
        //SaveLoadSceneManager.savechosenAnimationClipName -= SavechosenAnimationClipName;
    }

    public void Update()
    {
        if (startUpdate)
        {
            AnimationLight[chosenAnimationClipName].time = (theSlider.value * AnimationLight[chosenAnimationClipName].length);
        }
    }

    public void OnButtonclick(int yearDate)
    {
        Vector3 temp = AnimationObject.localEulerAngles;

        switch (yearDate)
        {
            case 0:
                AnimationLight.Stop(chosenAnimationClipName);
                chosenAnimationClipName = "21mars";
                AnimationLight.Play(chosenAnimationClipName);
                AnimationLight[chosenAnimationClipName].speed = 0;
                temp.x = marsRotationX;

                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOff;

                break;
            case 1:
                AnimationLight.Stop(chosenAnimationClipName);
                chosenAnimationClipName = "21Juin";
                AnimationLight.Play(chosenAnimationClipName);
                AnimationLight[chosenAnimationClipName].speed = 0;
                temp.x = juinRotationX;

                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOff;

                break;
            case 2:
                AnimationLight.Stop(chosenAnimationClipName);
                chosenAnimationClipName = "21Septembre";
                AnimationLight.Play(chosenAnimationClipName);
                AnimationLight[chosenAnimationClipName].speed = 0;
                temp.x = septembreRotationX;

                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOff;

                break;
            case 3:
                AnimationLight.Stop(chosenAnimationClipName);
                chosenAnimationClipName = "21Decembre";
                AnimationLight.Play(chosenAnimationClipName);
                AnimationLight[chosenAnimationClipName].speed = 0;
                temp.x = decembreRotationX;

                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOff;

                break;
        }
        AnimationObject.localEulerAngles = temp;
    }
}
