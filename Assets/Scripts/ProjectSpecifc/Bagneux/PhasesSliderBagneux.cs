﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhasesSliderBagneux : MonoBehaviour {

	[SerializeField]
	private Slider slider;
	public static bool startUpdate = false;

	[SerializeField]
	private GameObject Bati;

	//Phase 0
	[SerializeField]
	private List<GameObject> zeroBatch= new List<GameObject>();
	//Phase 1
	[SerializeField]
	private List<GameObject> firstBatch= new List<GameObject>();
	//Phase 2
	[SerializeField]
	private List<GameObject> secondBatch = new List<GameObject>();
	//Phase 3
	[SerializeField]
	private List<GameObject> thirdBatch = new List<GameObject>();
	//Phase 4
	[SerializeField]
	private List<GameObject> fourthBatch = new List<GameObject>();
	//Phase 5
	[SerializeField]
	private List<GameObject> fifthBatch = new List<GameObject>();

	private bool batch_0_Shown;
	private bool batch_0_Hidden;
	private bool batch_1_Shown;
	private bool batch_1_Hidden;
	private bool batch_2_Shown;
	private bool batch_2_Hidden;
	private bool batch_3_Shown;
	private bool batch_3_Hidden;
	private bool batch_4_Shown;
	private bool batch_4_Hidden;
	private bool batch_5_Shown;
	private bool batch_5_Hidden;


	private void Start () {

	}

	private void Update () {
		if (startUpdate) {
			if (slider.value <= 0f) {
				Bati.SetActive (true);
				batch_0_Shown = true;
				batch_1_Hidden = true;
				batch_2_Hidden = true;
				batch_3_Hidden = true;
				batch_4_Hidden = true;
				batch_5_Hidden = true;
			} else if (slider.value > 0.9f && slider.value <=1f) {
				batch_0_Hidden = true;
				batch_1_Shown = true;
				batch_2_Hidden = true;
				batch_3_Hidden = true;
				batch_4_Hidden = true;
				batch_5_Hidden = true;
			} else if (slider.value > 1.9f && slider.value <= 2f) {
				batch_0_Hidden = true;
				batch_1_Hidden = true;
				batch_2_Shown = true;
				batch_3_Shown = true;
				batch_4_Hidden = true;
				batch_5_Hidden = true;
			} else if (slider.value > 2.9f && slider.value <= 3f) {
				batch_0_Hidden = true;
				batch_1_Hidden = true;
				batch_2_Shown = true;
				batch_3_Shown  = true;
				batch_4_Hidden = true;
				batch_5_Hidden = true;
			}else if (slider.value > 3.9f && slider.value <= 4f) {
				batch_0_Hidden = true;
				batch_1_Hidden = true;
				batch_2_Hidden = true;
				batch_3_Hidden = true;
				batch_4_Shown  = true;
				batch_5_Shown = true;
			}else if (slider.value >= 5f) {
				batch_0_Hidden = true;
				batch_1_Hidden = true;
				batch_2_Hidden = true;
				batch_3_Hidden = true;
				batch_4_Shown = true;
				batch_5_Shown = true;
			}

			if (batch_0_Shown) {
				batch_0_Shown = false;
				if (ButtonModeTexture.isTex) {
					zeroBatch[1].SetActive (true);
				}else{
					zeroBatch[0].SetActive (true);
				}
			}
			if(batch_0_Hidden){
				batch_0_Hidden = false;
				if (ButtonModeTexture.isTex) {
					zeroBatch[1].SetActive (false);
				}else{
					zeroBatch[0].SetActive (false);
				}
			}
			if (batch_1_Shown) {
				batch_1_Shown = false;
				foreach (GameObject go in firstBatch) {
					go.SetActive (true);
				}
			}
			if(batch_1_Hidden){
				batch_1_Hidden = false;
				foreach (GameObject go in firstBatch) {
					go.SetActive (false);
				}
			}
			if (batch_2_Shown) {
				batch_2_Shown = false;
				foreach (GameObject go in secondBatch) {
					go.SetActive (true);
				}
			}
			if(batch_2_Hidden){
				batch_2_Hidden = false;
				foreach (GameObject go in secondBatch) {
					go.SetActive (false);
				}
			}
			if (batch_3_Shown) {
				batch_3_Shown = false;
				foreach (GameObject go in thirdBatch) {
					go.SetActive (true);
				}
			}
			if(batch_3_Hidden){
				batch_3_Hidden = false;
				foreach (GameObject go in thirdBatch) {
					go.SetActive (false);
				}
			}
			if (batch_4_Shown) {
				batch_4_Shown = false;
				foreach (GameObject go in fourthBatch) {
					go.SetActive (true);
				}
			}
			if(batch_4_Hidden){
				batch_4_Hidden = false;
				foreach (GameObject go in fourthBatch) {
					go.SetActive (false);
				}
			}
			if (batch_5_Shown) {
				batch_5_Shown = false;
				foreach (GameObject go in fifthBatch) {
					go.SetActive (true);
				}
			}
			if(batch_5_Shown){
				batch_5_Shown = false;
				foreach (GameObject go in fifthBatch) {
					go.SetActive (false);
				}
			}
		}
	}

	private void OnEnable(){
		startUpdate = true;
	}

	private void OnDisable(){
		startUpdate = false;
	}
}
