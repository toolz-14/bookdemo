﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {

	AsyncOperation MyAsync = null;
	private GameManagerBagneux GameManagerBagneux;

	private void Start () {
		GameManagerBagneux = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManagerBagneux> ();
		GameManagerBagneux.StartCoroutine ("GetAssetBundle");
//		GameManagerBagneux.GetAssetBundle();
		StartCoroutine ("LoadMain");
	}
	
	private void Update () {

		if(MyAsync != null){
			if (MyAsync.progress >= 0.9f && GameManagerBagneux.BundleIsLoaded) {
				MyAsync.allowSceneActivation = true;
			}
		}
	}

	IEnumerator LoadMain() {
		yield return new WaitForEndOfFrame();
		MyAsync = SceneManager.LoadSceneAsync("MainBagneux");
		MyAsync.allowSceneActivation = false;
		yield return MyAsync;
	}
}
