﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedAnimationStart : MonoBehaviour {

	public Animation[] gruesAnims = new Animation[6];

	public float time1 = 0f;
	public float time2 = 0f;
	public float time3 = 0f;
	public float time4 = 0f;
	public float time5 = 0f;
	public float time6 = 0f;
	public float time7 = 0f;
	public float time8 = 0f;
	public float time9 = 0f;
	public float time10 = 0f;
	public float time11 = 0f;

	private void Start () {
		
	}

	private void OnEnable(){
		foreach(Animation an in gruesAnims){
			an [an.clip.name].time = 0f;
			an [an.clip.name].speed = 0.3f;
		}
		StartCoroutine ("startDelayed");
	}

	IEnumerator startDelayed(){
		yield return new WaitForSeconds (time1);
		gruesAnims [0].Play ();
		yield return new WaitForSeconds (time2);
		gruesAnims [1].Play ();
		yield return new WaitForSeconds (time3);
		gruesAnims [2].Play ();
		yield return new WaitForSeconds (time4);
		gruesAnims [3].Play ();
		yield return new WaitForSeconds (time5);
		gruesAnims [4].Play ();
		yield return new WaitForSeconds (time6);
		gruesAnims [5].Play ();
	}
}
