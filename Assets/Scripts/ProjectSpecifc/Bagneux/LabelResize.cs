﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelResize : MonoBehaviour {

	private float _distanceToCamera;
	public  float _minScale = 1f;
	public  float _maxScale = 2.5f;
	public  float _distOffset = 0.02f;


	private void Start () {
		
	}
	
	private void OnEnable (){
		InvokeRepeating ("MyUpdate", 0f, 0.04f);
	}

	private void OnDisable (){
		CancelInvoke ("MyUpdate");
	}

	private float DistanceTOCamera(){
        return Vector3.Distance(Toolz.Managers.LevelManager.ActiveCamera.transform.position, transform.position);
 	}

	private void MyUpdate (){
//		Debug.Log ("DistanceTOCamera() = "+DistanceTOCamera());
		_distanceToCamera = DistanceTOCamera ();
		_distanceToCamera = Mathf.Clamp (_distanceToCamera * _distOffset, _minScale, _maxScale);
		transform.localScale = new Vector3 (_distanceToCamera, _distanceToCamera, _distanceToCamera);
	}
}
