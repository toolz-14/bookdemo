﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatUIManagerLerp : MonoBehaviour {

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;

	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private Vector2 _startPos;
	private Vector2 startSize;


	private void Start () {
        ResizeAndPlaceSubPanels();

    }

	private void Update () {
		if(_launch){
			OpenPanel();
		}
		if(_launch2){
			ClosePanel();
		}
	}

	//the transform.parent is the Canvas
	private void ResizeAndPlaceSubPanels(){
        
		startSize.x = (transform.parent.GetComponent<RectTransform>().rect.width ) / 2.7f;
        startSize.y = GetComponent<RectTransform>().sizeDelta.y;

        GetComponent<RectTransform>().sizeDelta = startSize;

		GetComponent<RectTransform>().anchoredPosition = new Vector2 (transform.parent.GetComponent<RectTransform>().rect.width, GetComponent<RectTransform>().anchoredPosition.y);

		_startPos = GetComponent<RectTransform>().anchoredPosition;
	}

	public void StartOpenPanel(){
		
		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().anchoredPosition;
		_to1 = new Vector2 (0f, GetComponent<RectTransform> ().anchoredPosition.y);
		_launch = true;
	}

	private void OpenPanel(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;

		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f){
			_launch = false;
		}
	}

	public void StartClosePanel()
    {
        _timeSinceTransitionStarted2 = 0f;
		_from2 = GetComponent<RectTransform> ().anchoredPosition;
		_to2 = _startPos;
		_launch2 = true;
	}

	private void ClosePanel(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;

		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
			gameObject.SetActive (false);
        }
	}
}
