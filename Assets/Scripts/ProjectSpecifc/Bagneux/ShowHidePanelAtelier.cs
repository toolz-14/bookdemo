﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHidePanelAtelier : MonoBehaviour {

    [SerializeField]
    private GameObject panelAtelier;
    [SerializeField]
    private GameObject panelPresentationAtelier;

    public static bool isAtelierOn;


    private void Start()
    {
        isAtelierOn = false;
    }

    public void ShowPanel()
    {
        isAtelierOn = true;
        if (!TimlineLerpBagneux.isOpen)
        {
            panelAtelier.SetActive(true);
            panelPresentationAtelier.SetActive(true);
        }
    }

    public void HidePanel()
    {
        isAtelierOn = false;
        panelAtelier.SetActive(false);
        panelPresentationAtelier.SetActive(false);
    }
}
