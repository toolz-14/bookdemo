﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderCommentResize : MonoBehaviour
{
    void Start()
    {

    }

    public void ResizeHolder()
    {
        float temp = GetComponent<RectTransform>().sizeDelta.x/2f;
        transform.Find("UserName").GetComponent<RectTransform>().sizeDelta = new Vector2(temp, GetComponent<RectTransform>().sizeDelta.y);
        transform.Find("Date").GetComponent<RectTransform>().sizeDelta = new Vector2(temp, GetComponent<RectTransform>().sizeDelta.y);
        //reposition Date
        Vector3 temp2 = transform.Find("Date").GetComponent<RectTransform>().anchoredPosition3D;
        temp2.x = temp + 2f;
        transform.Find("Date").GetComponent<RectTransform>().anchoredPosition3D = temp2;
    }
}
