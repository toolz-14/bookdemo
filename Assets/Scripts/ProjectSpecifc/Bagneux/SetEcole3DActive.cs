﻿using UnityEngine;


public class SetEcole3DActive : MonoBehaviour {

    [SerializeField]
    private GameObject ecole3D;

	private void Start () {
		
	}

    public void SetActive(bool isactive)
    {
        if (isactive && !ecole3D.activeInHierarchy)
        {
            ecole3D.SetActive(true);
        }
        else if (!isactive && ecole3D.activeInHierarchy)
        {
            ecole3D.SetActive(false);
        }
    }
}
