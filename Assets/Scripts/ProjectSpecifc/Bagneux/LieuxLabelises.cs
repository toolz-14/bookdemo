﻿using UnityEngine;
using UnityEngine.UI;

public class LieuxLabelises : MonoBehaviour {

    [SerializeField]
    private GameObject Panel;
    [SerializeField]
    private string PlaceName = "";
    [SerializeField]
    private string Adresse = "";
    [SerializeField]
    private string Telephone = "";

	private void Start () {
		
	}
	
	public void OnButtonClick(){
		Panel.SetActive (true);
        Panel.transform.Find("Button_Promoteur").gameObject.SetActive(false);
        Panel.transform.Find("Button_Architect").gameObject.SetActive(false);
        Panel.transform.Find("Button_PDF").gameObject.SetActive(false);
        PlaceName = PlaceName.Replace("NEWLINE", "\n");
        Adresse = Adresse.Replace("NEWLINE", "\n");
        Telephone = Telephone.Replace("NEWLINE", "\n");
        Panel.transform.Find ("Text_Name").GetComponent<Text>().text = PlaceName;
		Panel.transform.Find ("Text_Info").GetComponent<Text>().text = Adresse;
		Panel.transform.Find ("Text_Info2").GetComponent<Text>().text = Telephone;
	}
}
