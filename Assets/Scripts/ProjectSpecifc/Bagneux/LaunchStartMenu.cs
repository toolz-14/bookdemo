﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchStartMenu : MonoBehaviour {

    [SerializeField]
    private GameObject startMenu;
    [SerializeField]
    private bool startTheMenu;

    void Awake () {
        
    }

    public void SetMenuActive()
    {
        if(startTheMenu)
        startMenu.SetActive(true);
    }
}
