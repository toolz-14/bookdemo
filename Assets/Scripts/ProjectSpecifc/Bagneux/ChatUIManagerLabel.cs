﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;

public class ChatUIManagerLabel : MonoBehaviour {

	[SerializeField]
	private GameObject commentPrefab;
	[SerializeField]
	private GlobalErrorManager globalErrorManager;
	[SerializeField]
	private ProfileUIManager ProfileManager;
	[SerializeField]
	private Transform commentParent;
    [SerializeField]
    private GameObject panelDisplayMessage;
    [SerializeField]
    private GameObject ValidationPopUp;
    [SerializeField]
    private Text presentationText;
    [SerializeField]
    private GameObject LotsPanel;

    [HideInInspector]
	public bool IsOpen;
    [HideInInspector]
    public Color labelColor;
    [HideInInspector]
    public bool waitForServerReply;

    private GameObject _tempObject;
	private List<GameObject> CommentItems;
	private string inputedText;
	//webservices
    private GetComments _getCommentsService;
    private PostUserCommentVote _postUserCommentVoteService;
    private PostUserFollowProject _postUserFollowProjectService;
    private PostUserComment _postUserCommentService;
    private Toolz.WebServices.UserGetFollowedProjects _userGetFollowedProjects;
    private GetCommentReplies _getCommentRepliesService;
    private PostUserReply _postUserReplyService;
    private PostUserReplyVote _postUserReplyVoteService;

    public static ChatUIManagerLabel instance;
    [HideInInspector]
    public ChatCommentItem currentChatItem;

    private LabelEditorObject currentLabel;
    private int idComment;
    private int idAnsweredReply;
    private int labelCategory;

    //Is this ResponseInputUIManager used to send a comment or a reply?
    private enum InputMode { COMMENT, REPLY };
    private InputMode _inputMode;

    private void Awake(){
		IsOpen = false;
		SetDiscussionWebService ();

        // singleton
        if (instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }

    }

    private void SetDiscussionWebService(){
		GameObject webservice = GameObject.FindGameObjectWithTag("WebServices");
		
		_getCommentsService = webservice.GetComponent<GetComments>();
        _getCommentsService.onComplete += _getCommentsService_onComplete;
        _getCommentsService.onError += _getCommentsService_onError;
		
		_postUserCommentVoteService = webservice.GetComponent<PostUserCommentVote>();
		_postUserCommentVoteService.onError += _postUserCommentVote_onError;
		_postUserCommentVoteService.onComplete += _postUserCommentVote_onComplete;

        _getCommentRepliesService = webservice.GetComponent<GetCommentReplies>();
        _getCommentRepliesService.onComplete += _getCommentRepliesService_onComplete;
        _getCommentRepliesService.onError += _getCommentRepliesService_onError;

        _postUserReplyService = webservice.GetComponent<PostUserReply>();
        _postUserReplyService.onComplete += _postUserReply_onComplete;
        _postUserReplyService.onError += _postUserReply_onError;
        
        _postUserCommentService = webservice.GetComponent<PostUserComment>();
        _postUserCommentService.onComplete += _postUserCommentService_onComplete;
        _postUserCommentService.onError += _postUserCommentService_onError;

        _postUserReplyVoteService = webservice.GetComponent<PostUserReplyVote>();
        _postUserReplyVoteService.onComplete += _postUserReplyVoteService_onComplete;
        _postUserReplyVoteService.onError += _postUserReplyVoteService_onError;

        CommentItems = new List<GameObject>();
	}

    public void OpenChatPanel(LabelEditorObject label)
    {
        //Debug.Log("OpenChatPanel " + label.LabelText.text);
        currentLabel = label;
        idComment = label.id;
		this.gameObject.SetActive(true);

		//set chat name:
		transform.Find("Header").Find("Caption").GetComponent<Text>().text = label.LabelText.text;

        //set description
        presentationText.text = label.description;

        //set label color
        labelColor = label.ImageHeaderColor;
        SetPanelColor(label.ImageHeaderColor);
        labelCategory = label.category;

        //Replace with correct id
        _getCommentsService.idProject = label.id;
		_getCommentsService.UseWebService();

		IsOpen = true;

        if (ProfileUIManager.IsOpen) {
			ProfileManager.CloseProfilePanel ();
		}

        if (LotsPanel.activeInHierarchy)
        {
            LotsPanel.SetActive(false);
        }

        GetComponent<ChatUIManagerLerp> ().StartOpenPanel();
	}
	
	public void CloseChatPanel()
    {
        foreach (GameObject go in CommentItems)
        {
            Destroy(go);
        }
        CommentItems.Clear();
        GetComponent<ChatUIManagerLerp> ().StartClosePanel();
		IsOpen = false;
	}
	
	private void SetCommentListView(){
        //set scroll view mask size
        float tempSizeY = 0.0f;
        foreach (Transform trans in transform)
        {
            if (!trans.name.Contains("PanelContentContainer"))
            {
                //Debug.Log("Adding size = "+ trans.GetComponent<RectTransform>().sizeDelta.y + " of object: "+ trans.name);
                tempSizeY += trans.GetComponent<RectTransform>().sizeDelta.y;
            }
        }
        //Debug.Log("GetComponent<RectTransform>().rect.height = " + GetComponent<RectTransform>().rect.height);
        //Debug.Log("tempSizeY = " + tempSizeY);

        if (transform.Find("PanelContentContainer") != null && transform.Find("PanelContentContainer").GetComponent<RectTransform>() != null)
        {
            transform.Find("PanelContentContainer").GetComponent<RectTransform>().sizeDelta = new Vector2(transform.Find("PanelContentContainer").GetComponent<RectTransform>().sizeDelta.x, (GetComponent<RectTransform>().rect.height - tempSizeY));
        }
        


        //Clean memory of last graphic object
        foreach (GameObject go in CommentItems) {
            Destroy(go);
        }
        CommentItems.Clear();

		List<Comment> comments = _getCommentsService.Comments;

        foreach (Comment comment in comments) {
            if(comment.status == 1) // Add only validated comment
                AddCommentToList(comment);
		}

		//modify container height depending on amount of comments (so it can scroll properly)
		commentParent.GetComponent<ResizeContainerWithChildAmount>().GetChildrensAndResize();
        commentParent.GetComponent<RectTransform>().sizeDelta = new Vector2(commentParent.GetComponent<RectTransform>().sizeDelta.x, commentParent.GetComponent<RectTransform>().sizeDelta.y+40f);

        //force layout group rebuild
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }

	//directly assigned to button envoyer
	public void OnClickCommentEnvoyerButton()
    {
        int idUser = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>().idUser;
        if (idUser != -1)
        {
            inputedText = transform.Find("AddComment").Find("Text_BG").Find("InputField").GetComponent<InputField>().text;
            _inputMode = InputMode.COMMENT;
            PublishResponse();
        }
        else
        {
            ShowglobalError();
        }
    }

    public void OnClickReplyEnvoyerButton(ChatCommentItem chatItem, string msg, int _idAnsweredReply)
    {
        currentChatItem = chatItem;
        int idUser = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>().idUser;
        if (idUser != -1)
        {
            inputedText = msg;
            idAnsweredReply = _idAnsweredReply;
            _inputMode = InputMode.REPLY;
            PublishResponse();
        }
        else
        {
            ShowglobalError();
        }
    }

    public void PublishResponse()
    {
        User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>();
        if (_inputMode == InputMode.COMMENT)
        {
            if (idComment != -1)
            {
                _postUserCommentService.idLabel = idComment;
                _postUserCommentService.commentText = inputedText;
                _postUserCommentService.request_status = 0;

                if ((ApplicationLogin.ApplicationLoginStatus)user.userStatus == ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
                {
                    _postUserCommentService.commentStatus = 1;
                }
                else
                {
                    _postUserCommentService.commentStatus = 0;
                }

                _postUserCommentService.UseWebService();
            }
            else
            {
                panelDisplayMessage.SetActive(true);
                panelDisplayMessage.transform.Find("TextError").GetComponent<Text>().text = "Le label choisi n'est pas enregistré en ligne, veuillez l'enregistrer en ligne";
            }
        }
        else if (_inputMode == InputMode.REPLY)
        {
            if (idComment != -1)
            {
                _postUserReplyService.idComment = currentChatItem.idComment; // id of first comment
                _postUserReplyService.idAnsweredReply = idAnsweredReply;
                _postUserReplyService.text = inputedText;
                _postUserReplyService.request_status = 0;
                if ((ApplicationLogin.ApplicationLoginStatus)user.userStatus == ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
                {
                    _postUserReplyService.replyStatus = 1;
                }
                else
                {
                    _postUserReplyService.replyStatus = 0;
                }

                _postUserReplyService.UseWebService();
            }
            else
            {
                panelDisplayMessage.SetActive(true);
                panelDisplayMessage.transform.Find("TextError").GetComponent<Text>().text = "Le label choisi n'est pas enregistré en ligne, veuillez l'enregistrer en ligne";
            }
        }
    }

    //for the whole chat layout group order to work properly, the reply list has to be part of the comment object
    public void AddCommentToList(Comment comment) {
        int i = CommentItems.Count;
        int idUser = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>().idUser;

		_tempObject = (GameObject)Instantiate(commentPrefab);
        _tempObject.AddComponent<CommentId>();
        _tempObject.GetComponent<CommentId>().idComment = comment.idComment;
        int idComment = (int) comment.idComment;

        //set reply color
        _tempObject.GetComponent<ChatCommentItem>().SetReplyColor(labelCategory);

        _tempObject.SetActive(true);

        //set user name 
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("HolderCommentStats").Find("UserName").GetComponent<Text>().text = comment.firstname + " " + comment.name;

        //set comment text
        comment.text = comment.text.Replace("\\r\\n", "\n");
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find ("Text_BG").Find ("Text").GetComponent<Text> ().text = comment.text;

        //set text bg color
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("Text_BG").GetComponent<Image>().color = labelColor;

        //set date (published time)
        string date = comment.publishedTime.ToString("dd/MM/yyyy HH:mm");
		_tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("HolderCommentStats").Find("Date").GetComponent<Text>().text = date;

		Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>(); // Get user

        //set vote count (total votes)
        SetCommentVoteColor(_tempObject.transform.Find("Holder").Find("VoteButtonHolder").Find("VoteCount").GetComponent<Text>(),comment);

        //set vote
        Transform ButtonFor = _tempObject.transform.Find("Holder").Find("VoteButtonHolder").Find("Yes");
		Transform ButtonAgainst = _tempObject.transform.Find("Holder").Find("VoteButtonHolder").Find("No");

        if (idUser != -1) {
            //if the comment was made by the user then both greyed out
            if (comment.idAuthor == idUser) {
                ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                ButtonFor.GetComponent<Button>().interactable = false;
                ButtonAgainst.GetComponent<Button>().interactable = false;
            }
            else {
                //if the logged user has already voted then the button is greyed out   
                if (comment.userVote != 0) {
                    //if voted yes
                    if (comment.userVote == 1) {
                        ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                    }
                    //if voted no
                    else if (comment.userVote == -1) {
                        ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                    }
                    ButtonFor.GetComponent<Button>().interactable = false;
                    ButtonAgainst.GetComponent<Button>().interactable = false;
                }
                else {
                    ButtonFor.GetComponent<Button>().onClick.AddListener(() => DoVote(true, idComment));
                    ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => DoVote(false, idComment));
                }
            }
        }
        else
        {
            ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
            ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
            ButtonFor.GetComponent<Button>().interactable = false;
            ButtonAgainst.GetComponent<Button>().interactable = false;
        }

        //get nb responses
        _tempObject.GetComponent<ChatCommentItem>().nbValidatedReply = comment.nbValidatedResponses;
        _tempObject.GetComponent<ChatCommentItem>().nbUnvalidatedReply = comment.nbUnvalidatedResponses;
        //get commentId
        _tempObject.GetComponent<ChatCommentItem>().idComment = (int)comment.idComment;

        //set parent
        _tempObject.transform.SetParent(commentParent);
		_tempObject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

		//set text width, this only way I found to do it for now. the 55% is a magic number found be messing around with the value
		float temp = ((_tempObject.GetComponent<RectTransform> ().sizeDelta.x * 55f) / 100f);
		_tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find ("Text_BG").Find ("Text").GetComponent<RectTransform> ().sizeDelta = new Vector2 ( temp, GetComponent<RectTransform> ().sizeDelta.y);

		//need to pass the text height, which is dynamic due to the content size fitter, to the 2 parents so the global layout group works properly
        Canvas.ForceUpdateCanvases();
        temp = _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("Text_BG").GetComponent<RectTransform>().rect.height + 25f;
        if (temp > _tempObject.transform.Find("Holder").Find("VoteButtonHolder").GetComponent<RectTransform>().sizeDelta.y)
        {
            _tempObject.transform.Find("Holder").GetComponent<RectTransform>().sizeDelta = new Vector2(_tempObject.transform.Find("Holder").GetComponent<RectTransform>().sizeDelta.x, temp);
        }
        else
        {
            _tempObject.transform.Find("Holder").GetComponent<RectTransform>().sizeDelta = new Vector2(_tempObject.transform.Find("Holder").GetComponent<RectTransform>().sizeDelta.x
                , _tempObject.transform.Find("Holder").Find("VoteButtonHolder").GetComponent<RectTransform>().sizeDelta.y);
        }
        
        _tempObject.GetComponent<RectTransform>().sizeDelta = new Vector2(_tempObject.GetComponent<RectTransform>().sizeDelta.x, temp);
        _tempObject.GetComponentInChildren<HolderCommentResize>().ResizeHolder();

        //upadte reply parent position
        _tempObject.transform.Find ("RepliesParent").GetComponent<RectTransform>().localPosition = new Vector3(_tempObject.transform.Find ("RepliesParent").GetComponent<RectTransform>().localPosition.x, -temp, _tempObject.transform.Find ("RepliesParent").GetComponent<RectTransform>().localPosition.z);

		CommentItems.Add(_tempObject);
	}

    public void OnClickDeleteCommentButton(Button delete, Button validate, int idAuthor, string msg, bool isDeleted){

		delete.interactable = false;
		validate.interactable = false;
        UpdateComment(idAuthor, msg, isDeleted);
    }

	public void OnClickValidateCommentButton(Button delete, Button validate,int idAuthor, string msg, bool isDeleted)
    {
        delete.interactable = false;
		validate.interactable = false;
        UpdateComment(idAuthor,msg, isDeleted);
    }

    // ADMIN VALIDATION
    public void UpdateComment(int idAuthor, string msg, bool isDeleted)
    {
        _postUserCommentService.idLabel = currentLabel.id;
        _postUserCommentService.idAuthor = idAuthor;
        _postUserCommentService.commentText = msg;
        _postUserCommentService.isDeleted = isDeleted;

        if (isDeleted)
            _postUserCommentService.request_status = 1;
        else
            _postUserCommentService.request_status = 2;

        _postUserCommentService.UseWebService();
    }

    public void UpdateReply(int idAuthor, int idComment, int idReply, int idAnsweredReply, string msg, bool isDeleted)
    {
        _postUserReplyService.idComment = idComment;
        _postUserReplyService.idReply = idReply;
        _postUserReplyService.idAuthor = idAuthor;
        _postUserReplyService.idAnsweredReply = idAnsweredReply;
        _postUserReplyService.isDeleted = isDeleted;
        _postUserReplyService.text = msg;

        if (isDeleted)
            _postUserReplyService.request_status = 1;
        else
            _postUserReplyService.request_status = 2;

        _postUserReplyService.UseWebService();
    }

    private void DoVote(bool isFor, int idComment){
        int userVote = isFor ? 1 : -1;

        _postUserCommentVoteService.idComment = (uint)idComment;
        _postUserCommentVoteService.userVote = userVote;

        _postUserCommentVoteService.UseWebService();
	}

	public void ShowglobalError(){
		globalErrorManager.gameObject.SetActive (true);
		globalErrorManager.OpenGlobalErrorPanel();
	}

    private void SetCommentVoteColor(Text voteText, Comment comment)
    {

        int temp = comment.nbUp - comment.nbDown;
        if ((comment.nbUp - comment.nbDown) > 0)
        {
            voteText.color = Color.green;
            voteText.text = "+" + temp.ToString();
        }
        else if ((comment.nbUp - comment.nbDown) < 0)
        {
            voteText.color = Color.red;
            voteText.text = temp.ToString();
        }
        else
        {
            voteText.color = Color.gray;
            voteText.text = "-" + temp.ToString();
        }
    }

    private void SetPanelColor(Color labelColor)
    {
        //title color
        transform.Find("Header").Find("Caption").GetComponent<Text>().color = labelColor;
        //icon color
        transform.Find("Header").Find("Icon").GetComponent<Image>().color = labelColor;
        //exit button color
        transform.Find("Header").Find("CloseBt").Find("Image").GetComponent<Image>().color = labelColor;
        //separator color
        transform.Find("Separator").Find("Image").GetComponent<Image>().color = labelColor;
        //add comment outline color
        transform.Find("AddComment").Find("Text_BG").GetComponent<Image>().color = labelColor;
        //add comment envoyer button color
        transform.Find("AddComment").Find("AddCommentButton").GetComponent<Image>().color = labelColor;
    }

    // WEBSERVICE CALL
    public void UseCommmentReplyService(int idComment, ChatCommentItem chatItem)
    {
        currentChatItem = chatItem;
        _getCommentRepliesService.idComment = (uint)idComment;
        _getCommentRepliesService.UseWebService();
    }


    public List<Reply> GetReplies()
    {
        List<Reply> replies = _getCommentRepliesService.Replies;
        return replies;
    }

    // WEB SERVICES CALLBACKS

    private void _getCommentsService_onError(long status, string message) {
        Debug.Log("Error while loading discussion: " + message);

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();

    }

    private void _getCommentsService_onComplete(long status, string message) {
        SetCommentListView();
    }

    private void _getCommentRepliesService_onError(long status, string message)
    {
        Debug.Log("GetCommentReplies service Error : " + message);
        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _getCommentRepliesService_onComplete(long status, string message)
    {
        //Debug.Log("_getCommentRepliesService_onComplete status : " + status);

        currentChatItem.GetCommentsCallBack(status);
        waitForServerReply = false;
    }

    private void _postUserCommentService_onError(long status, string message)
    {
        Debug.Log("Post user comment ERROR : " + message);
        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _postUserCommentService_onComplete(long status, string message)
    {
        transform.Find("AddComment").Find("Text_BG").Find("InputField").GetComponent<InputField>().text = "";
        Debug.Log("Post user comment OK : " + message);

        if (_postUserCommentService.request_status == 0)
        {
            //Panel_DiscussionUIManager.AddCommentToList(_postUserCommentService.comment);
            ValidationPopUp.SetActive(true);
            ValidationPopUp.GetComponent<CommentValidationPopUpPanel>().StartOpenPanel();
        }

        if (currentLabel!=null)
        {
            OpenChatPanel(currentLabel);
        }

    }

    private void _postUserReply_onError(long status, string message)
    {
        Debug.Log("Post User Reply ERROR : " + message);
        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _postUserReply_onComplete(long status, string message)
    {
        //Debug.Log("Post User Reply OK");
        currentChatItem.PostUserReplyCallBack(_postUserReplyService, ValidationPopUp);

        if (currentLabel != null)
        {
            OpenChatPanel(currentLabel);
        }
    }

    public void UseReplyVoteService(int idReply, int userVote)
    {
        _postUserReplyVoteService.idReply = (uint)idReply;
        _postUserReplyVoteService.userVote = userVote;

        _postUserReplyVoteService.UseWebService();
    }

    private void _postUserReplyVoteService_onComplete(long status, string message)
    {
        currentChatItem.PostUserReplyVoteCallBack(_postUserReplyVoteService);

        
    }

    private void _postUserReplyVoteService_onError(long status, string message)
    {
        Debug.Log("User vote error");
        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }


    private void _postUserCommentVote_onComplete(long status, string message)
    {
        GameObject currentComment = null;
        foreach (GameObject commentObject in CommentItems)
        {
            Debug.Log("Registered id: " + _postUserCommentVoteService.idComment);
            Debug.Log("Current object id: " + commentObject.GetComponent<CommentId>().idComment);
            if (_postUserCommentVoteService.idComment == commentObject.GetComponent<CommentId>().idComment)
            {
                currentComment = commentObject;
                break;
            }
        }
        if (currentComment == null)
        {
            Debug.LogError("Current Comment is null");
            return;
        }
        Debug.Log("User vote ok");
        //set vote
        Transform ButtonFor = currentComment.transform.Find("Holder").Find("VoteButtonHolder").Find("Yes");
        Transform ButtonAgainst = currentComment.transform.Find("Holder").Find("VoteButtonHolder").Find("No");

        //set vote count (total votes)
        int totalVote = _postUserCommentVoteService.nbUp - _postUserCommentVoteService.nbDown;
        currentComment.GetComponent<ChatCommentItem>().nbValidatedReply = totalVote;

        //if the logged user has already voted then the button is geyed out

        //if voted yes
        if (_postUserCommentVoteService.userVote == 1)
        {
            ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
        }
        //if voted no
        else if (_postUserCommentVoteService.userVote == -1)
        {
            ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
        }
        ButtonFor.GetComponent<Button>().interactable = false;
        ButtonAgainst.GetComponent<Button>().interactable = false;
    }

    private void _postUserCommentVote_onError(long status, string message)
    {
        Debug.Log("User vote error");
        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

}
