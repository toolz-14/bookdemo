﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class LocalisationHS : MonoBehaviour
{

    [SerializeField]
    private float cameraStartY = 0f;
    [SerializeField]
    private float cameraStartX = 0f;
    [SerializeField]
    private Transform theCamera360;
    [SerializeField]
    private GameObject fixedCamera;
    [SerializeField]
    private string NamesOfWanted360;
    [SerializeField]
    private string NamesOfWantedBundle;
    [SerializeField]
    private string NamesOfWantedImage;
    // private AssetBundle loadedBundle = null;
    private LocalisationData LocalisationData;
    private GameManagerBagneux GameManagerBagneux;
    public bool LoadFromStreaming;
    [HideInInspector]
    public UnityWebRequest www;
    private string subFolder = "";

    private void Start()
    {
        if (GetComponentInParent<Project>() != null)
        {
            if (GetComponentInParent<Project>().is2018)
            {
                subFolder = "2018/";
            }
            //else if (GetComponentInParent<Project>().is2019)
            //{
            //    subFolder = "2019/";
            //}
        }
    }

    public void OnHSClick()
    {
        LocalisationData = transform.parent.GetComponent<LocalisationData>();

        if (LocalisationData.IsImage.StartwithImage)
        {
            GetImage();
        }
        else
        {
            //if bundle already loaded don't reload (error otherwise)
            if ((TheBundle.instance.loadedBundle == null) || (TheBundle.instance.loadedBundle != null && !TheBundle.instance.loadedBundle.name.Equals(NamesOfWantedBundle)))
            {
                if (!LoadFromStreaming)
                {
                    //set 360 from bundle
                    StartCoroutine(GetAssetBundle());
                }
                else
                {
                    Get360Streaming();
                }
            }
            else if (TheBundle.instance.loadedBundle != null && TheBundle.instance.loadedBundle.name.Equals(NamesOfWantedBundle))
            {
                if (!LoadFromStreaming)
                {
                    Texture a360 = TheBundle.instance.loadedBundle.LoadAsset<Texture>(NamesOfWanted360);
                    if (a360 != null)
                    {
                        Instantiate(a360);
                        LocalisationData.IsImage.theRenderer.material.mainTexture = a360;

                        Vector3 temp = theCamera360.localEulerAngles;
                        temp.y = cameraStartY;
                        temp.x = cameraStartX;
                        theCamera360.localEulerAngles = temp;

                        fixedCamera.SetActive(false);
                        theCamera360.parent.parent.parent.gameObject.SetActive(true);
                        Toolz.Managers.LevelManager.ActiveCamera = theCamera360.GetComponent<Camera>();

                        if (LocalisationData.IsImage.Loader != null)
                        {
                            LocalisationData.IsImage.Loader.SetActive(false);
                        }
                    }
                    else
                    {
                        Debug.Log("Get360, Asset bundle:" + TheBundle.instance.loadedBundle.name + " could not find object: " + NamesOfWanted360);
                        if (LocalisationData.IsImage.Loader != null)
                        {
                            LocalisationData.IsImage.Loader.SetActive(false);
                        }
                    }
                }
                else
                {
                    Get360Streaming();
                }
            }
        }

        //Swap to project view
        LocalisationData.ProjectReturnButton.onClick.Invoke();

    }

    private void Get360Streaming()
    {
        if (NamesOfWanted360 != "")
        {
            TheBundle.instance.bundleIsLoading = true;
            if (LocalisationData.IsImage.Loader != null)
            {
                LocalisationData.IsImage.Loader.SetActive(true);
            }

            Texture a360 = TheBundle.instance.loadedBundle.LoadAsset<Texture>(NamesOfWanted360);
            if (a360 != null)
            {
                Instantiate(a360);
                LocalisationData.IsImage.theRenderer.material.mainTexture = a360;

                Vector3 temp = theCamera360.localEulerAngles;
                temp.y = cameraStartY;
                temp.x = cameraStartX;
                theCamera360.localEulerAngles = temp;

                fixedCamera.SetActive(false);
                theCamera360.parent.parent.parent.gameObject.SetActive(true);
                Toolz.Managers.LevelManager.ActiveCamera = theCamera360.GetComponent<Camera>();

                if (LocalisationData.IsImage.Loader != null)
                {
                    LocalisationData.IsImage.Loader.SetActive(false);
                }
            }
            else
            {
                Debug.Log("LoadSwappable, Asset bundle:" + TheBundle.instance.loadedBundle.name + " could not find object: " + NamesOfWanted360);
                if (LocalisationData.IsImage.Loader != null)
                {
                    LocalisationData.IsImage.Loader.SetActive(false);
                }
            }
            TheBundle.instance.bundleIsLoading = false;
        }
    }

    // set wanted 360 -> bundle
    private void Get360()
    {

        if (NamesOfWanted360 != "")
        {
            TheBundle.instance.bundleIsLoading = true;
            if (LocalisationData.IsImage.Loader != null)
            {
                LocalisationData.IsImage.Loader.SetActive(true);
            }
            LocalisationData.IsImage.StopGetAssetBundle();
            StartCoroutine(GetAssetBundle());
        }
    }

    // set wanted image
    private void GetImage()
    {
        if (NamesOfWantedImage != "")
        {
            LocalisationData.IsImage.theImage.transform.Find("Image").GetComponent<Image>().sprite = (Sprite)Resources.Load("ProjectImages/" + subFolder + NamesOfWantedImage, typeof(Sprite));
            fixedCamera.SetActive(false);
            theCamera360.parent.parent.parent.gameObject.SetActive(true);
            Toolz.Managers.LevelManager.ActiveCamera = theCamera360.GetComponent<Camera>();
            LocalisationData.IsImage.theImage.SetActive(true);
        }
    }

    IEnumerator GetAssetBundle()
    {
        if (NamesOfWantedBundle != "")
        {
            if (LocalisationData.IsImage.Loader != null)
            {
                LocalisationData.IsImage.Loader.SetActive(true);
            }

#if UNITY_ANDROID
			www = UnityWebRequest.GetAssetBundle ("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_Android/" + LocalisationData.IsImage.NamesOfWantedBundle);
#elif UNITY_IOS
			www = UnityWebRequest.GetAssetBundle ("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_iOS/" + LocalisationData.IsImage.NamesOfWantedBundle);
#elif UNITY_WEBGL
            //www = UnityWebRequest.GetAssetBundle ("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_Webgl/" + subFolder + NamesOfWantedBundle);
            //www = UnityWebRequest.GetAssetBundle("http://toolz.mairie-bagneux.fr:8080/Bagneux2019_BundleBP_Webgl/" + subFolder + NamesOfWantedBundle);
            www = UnityWebRequest.GetAssetBundle("https://budgetparticipatif.bagneux92.fr/carte3d//Bagneux2019_BundleBP_Webgl/" + subFolder + NamesOfWantedBundle);
#elif UNITY_STANDALONE
            www = UnityWebRequest.GetAssetBundle("http://preprod.toolz.fr/internal/Bagneux2019_BundleBP_PC/" + subFolder + LocalisationData.IsImage.NamesOfWantedBundle);
#endif

            //Debug.Log ("GetAssetBundle before sendwebrequest");
            www.timeout = 50;
            //www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            //www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            //www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            //www.SetRequestHeader("Access-Control-Allow-Origin", "*");

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("bundle loading error: " + www.error);
                if (LocalisationData.IsImage.Loader != null)
                {
                    LocalisationData.IsImage.Loader.SetActive(false);
                }
            }
            else
            {
                if (TheBundle.instance.loadedBundle != null)
                {
                    TheBundle.instance.loadedBundle.Unload(false);
                    Caching.ClearCache();
                    TheBundle.instance.loadedBundle = null;
                }

                TheBundle.instance.loadedBundle = DownloadHandlerAssetBundle.GetContent(www);

                Resources.UnloadUnusedAssets();

                Texture a360 = TheBundle.instance.loadedBundle.LoadAsset<Texture>(NamesOfWanted360);
                if (a360 != null)
                {
                    Instantiate(a360);
                    LocalisationData.IsImage.theRenderer.material.mainTexture = a360;

                    Vector3 temp = theCamera360.localEulerAngles;
                    temp.y = cameraStartY;
                    temp.x = cameraStartX;
                    theCamera360.localEulerAngles = temp;

                    fixedCamera.SetActive(false);
                    theCamera360.parent.parent.parent.gameObject.SetActive(true);
                    Toolz.Managers.LevelManager.ActiveCamera = theCamera360.GetComponent<Camera>();

                    if (LocalisationData.IsImage.Loader != null)
                    {
                        LocalisationData.IsImage.Loader.SetActive(false);
                    }
                }
                else
                {
                    Debug.Log("Get360, Asset bundle:" + TheBundle.instance.loadedBundle.name + " could not find object: " + NamesOfWanted360);
                    if (LocalisationData.IsImage.Loader != null)
                    {
                        LocalisationData.IsImage.Loader.SetActive(false);
                    }
                }
                TheBundle.instance.bundleIsLoading = false;
            }
        }
    }
}
