﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Toolz.WebServices;

public class ChatCommentItem : MonoBehaviour
{

    [HideInInspector]
    public int nbValidatedReply = 0;
    [HideInInspector]
    public int nbUnvalidatedReply = 0;
    [HideInInspector]
    public int idComment = 0;
    [HideInInspector]
    public int idAnsweredReply = 0;

    [SerializeField]
    private GameObject replyPrefab;
    [SerializeField]
    private GameObject addReplyPrefab;
    [SerializeField]
    private Transform replyParent;
    [SerializeField]
    private GlobalErrorManager GlobalErrorManager;

    private GameObject _tempObject;
    private bool isShowingReplies;
    private List<GameObject> replyItems = new List<GameObject>();
    private float heightToAdd;
    private Color labelColor;
    private bool doOnce;


    private void Start()
    {
        doOnce = false;
        isShowingReplies = false;
        UpdateShowHideRepliesButtonText(isShowingReplies, nbValidatedReply);
    }

    private void UpdateShowHideRepliesButtonText(bool isShowingReplies, int nbReplies)
    {
        if (nbReplies <= 0)
        {
            if (isShowingReplies)
            {
                transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("ShowHideRepliesButton").Find("Text").GetComponent<Text>().text = "Cacher commentaires (0)";
            }
            else
            {
                transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("ShowHideRepliesButton").Find("Text").GetComponent<Text>().text = "Soyer le premier à commenter";
            }
        }
        else
        {
            //if replies > 0
            if (isShowingReplies)
            {
                transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("ShowHideRepliesButton").Find("Text").GetComponent<Text>().text = "Cacher commentaires (" + nbReplies + ")";
            }
            else
            {
                transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("ShowHideRepliesButton").Find("Text").GetComponent<Text>().text = "Afficher commentaires (" + nbReplies + ")";
            }
        }
    }

    //directly assigned to button
    public void OnClickShowHideRepliesButton()
    {
        if (!doOnce)
        {
            doOnce = true;
            isShowingReplies = !isShowingReplies;
            if (!ChatUIManagerLabel.instance.waitForServerReply)
            {
                ChatUIManagerLabel.instance.waitForServerReply = true;

                if (isShowingReplies)
                {
                    ChatUIManagerLabel.instance.UseCommmentReplyService(idComment, this);
                }
                else
                {
                    HideAllRepliesForAGivenComment();
                }
            }
        }
    }

    //callback for button envoyer since instanciated
    public void OnClickReplyEnvoyerButton()
    {
        string inputedText = transform.Find("RepliesParent").Find("AddReply(Clone)").Find("Text_BG").Find("InputField").GetComponent<InputField>().text;
        if (inputedText != "")
            ChatUIManagerLabel.instance.OnClickReplyEnvoyerButton(this, inputedText, 0); // no answeredRep to give
    }

    private void HideAllRepliesForAGivenComment()
    {
        float temp = 0;

        //Clean list and remove objects
        foreach (GameObject go in replyItems)
        {
            Destroy(go);
        }
        replyItems.Clear();

        heightToAdd = (heightToAdd + temp) * -1f;
        UpdateCommentObjectHeight();

		UpdateShowHideRepliesButtonText(isShowingReplies, nbValidatedReply);

        ChatUIManagerLabel.instance.waitForServerReply = false;
    }

    private void SetResponsePanel()
    {
        // nbReply = 0;
        heightToAdd = 0f;

        List <Reply> replies = ChatUIManagerLabel.instance.GetReplies();
        foreach (Reply reply in replies)
        {
            if (reply.status == 1) // Add only validated reply
                AddAReply(reply);
        }

        AddAddAReplyPrefab();

        //update text in button
        UpdateShowHideRepliesButtonText(isShowingReplies, nbValidatedReply);

        //need to update height of comment object(gameObject) for the whole chat layout group order to work properly
        UpdateCommentObjectHeight();
    }

    private void AddAReply(Reply reply)
    {

        _tempObject = (GameObject)Instantiate(replyPrefab);
        _tempObject.SetActive(true);
        _tempObject.AddComponent<ReplyId>();
        _tempObject.GetComponent<ReplyId>().idReply = (uint)reply.idReply;

        //set user name 
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("HolderReplyStats").Find("UserName").GetComponent<Text>().text = reply.firstname + " " + reply.name;

        //set comment text
        reply.text = reply.text.Replace("\\r\\n", "\n");
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("Text_BG").Find("Text").GetComponent<Text>().text = reply.text;

        //set text bg color
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("Text_BG").GetComponent<Image>().color = labelColor;

        //set date (published time)
        string date = reply.publishedTime.ToString("dd/MM/yyyy HH:mm");
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("HolderBottom").Find("HolderReplyStats").Find("Date").GetComponent<Text>().text = date;

        int idUser = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>().idUser;
        Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>(); // Get user

        // set color reply vote
        SetReplyVoteColor(_tempObject.transform.Find("Holder").Find("VoteButtonHolder").Find("VoteCount").GetComponent<Text>(), reply);

        //set vote
        Transform ButtonFor = _tempObject.transform.Find("Holder").Find("VoteButtonHolder").Find("Yes");
        Transform ButtonAgainst = _tempObject.transform.Find("Holder").Find("VoteButtonHolder").Find("No");

        if (idUser != -1)
        {
            //if the reply was made by the user then both greyed out
            if (reply.idReplyAuthor == idUser)
            {
                ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                ButtonFor.GetComponent<Button>().interactable = false;
                ButtonAgainst.GetComponent<Button>().interactable = false;
            }
            else
            {
                //if the logged user has already voted then the button is greyed out   
                if (reply.userVote != 0)
                {
                    //if voted yes
                    if (reply.userVote == 1)
                    {
                        ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                    }
                    //if voted no
                    else if (reply.userVote == -1)
                    {
                        ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                    }
                    ButtonFor.GetComponent<Button>().interactable = false;
                    ButtonAgainst.GetComponent<Button>().interactable = false;
                }
                else
                {
                    ButtonFor.GetComponent<Button>().onClick.AddListener(() => DoVote(true, reply.idReply));
                    ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => DoVote(false, reply.idReply));
                }
            }
        }
        else
        {
            ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
            ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
            ButtonFor.GetComponent<Button>().interactable = false;
            ButtonAgainst.GetComponent<Button>().interactable = false;
        }

        //set parent
        _tempObject.transform.SetParent(replyParent);
        _tempObject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

        //set text width, this only way I found to do it for now. the 55% is a magic number found be messing around with the value
        float temp = ((_tempObject.GetComponent<RectTransform>().sizeDelta.x * 42f) / 100f);
        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("Text_BG").Find("Text").GetComponent<RectTransform>().sizeDelta = new Vector2(temp, GetComponent<RectTransform>().sizeDelta.y);

        //need to pass the text height, which is dynamic du to the content size fitter, to the 2 parents so the global layout group works properly
        //60f is the height of other elements added + 5f for convenience
        Canvas.ForceUpdateCanvases();
        temp = _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").Find("Text_BG").GetComponent<RectTransform>().rect.height + 25f;

        _tempObject.GetComponent<RectTransform>().sizeDelta = new Vector2(_tempObject.GetComponent<RectTransform>().sizeDelta.x, temp);        

        replyItems.Add(_tempObject);
        heightToAdd += _tempObject.GetComponent<RectTransform>().rect.height + replyParent.GetComponent<VerticalLayoutGroup>().spacing;

        _tempObject.transform.Find("Holder").Find("VoteButtonHolder").Find ("VoteCount").GetComponent<Text>().text = (reply.nbUp - reply.nbDown).ToString();

        _tempObject.transform.Find("Holder").Find("HolderTextAndButtons").GetComponent<VerticalLayoutGroup>().enabled = true;

        _tempObject.GetComponentInChildren<HolderReplyResize>().ResizeHolder();
    }

    //needs to be added each time at the end of replies list even if list is empty
    private void AddAddAReplyPrefab()
    {
        _tempObject = (GameObject)Instantiate(addReplyPrefab);
        _tempObject.SetActive(true);

        //set parent
        _tempObject.transform.SetParent(replyParent);
        _tempObject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

        //set button envoyer callback
        _tempObject.transform.Find("AddReplyButton").GetComponent<Button>().onClick.AddListener(() => OnClickReplyEnvoyerButton());

        //set outline color
        _tempObject.transform.Find("Text_BG").GetComponent<Image>().color = labelColor;
        //set envoyer button color
        _tempObject.transform.Find("AddReplyButton").GetComponent<Image>().color = labelColor;

        replyItems.Add(_tempObject);
        heightToAdd += _tempObject.GetComponent<RectTransform>().rect.height;
    }

    private void UpdateCommentObjectHeight()
    {
        //update comment height + littel offset (magic number) for convenience
        if (heightToAdd > 0)
        {
            heightToAdd += 40f;
        }
        float temp = GetComponent<RectTransform>().sizeDelta.y + heightToAdd;
        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, temp);

        //resize container
        transform.parent.GetComponent<ResizeContainerWithChildAmount>().GetChildrensAndResize();

        //force layout group rebuild
        LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());

        doOnce = false;
    }

    public void OnClickDeleteCommentButton(Button delete, Button validate, int idReply, int idAuthor, int idComment, int idAnsweredReply, string msg, bool isDeleted)
    {

        delete.interactable = false;
        validate.interactable = false;
        ChatUIManagerLabel.instance.UpdateReply(idAuthor, idComment, idReply, idAnsweredReply, msg, isDeleted);
    }

    public void OnClickValidateCommentButton(Button delete, Button validate, int idReply, int idAuthor, int idComment, int idAnsweredReply, string msg, bool isDeleted)
    {

        delete.interactable = false;
        validate.interactable = false;
        ChatUIManagerLabel.instance.UpdateReply(idAuthor, idComment, idReply, idAnsweredReply, msg, isDeleted);
    }

    public void ShowglobalError()
    {
        GlobalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager.OpenGlobalErrorPanel();
    }

    private void SetReplyVoteColor(Text voteText, Reply reply)
    {
        int temp = reply.nbUp - reply.nbDown;
        if ((reply.nbUp - reply.nbDown) > 0)
        {
            voteText.color = Color.green;
            voteText.text = "+" + temp.ToString();
        }
        else if ((reply.nbUp - reply.nbDown) < 0)
        {
            voteText.color = Color.red;
            voteText.text = temp.ToString();
        }
        else
        {
            voteText.color = Color.gray;
            voteText.text = "-" + temp.ToString();
        }
    }

    public void SetReplyColor(int labelCategory)
    {

        if(labelCategory == 0)
        {
            labelColor = new Color(0.7333333333333333f, 0.6588235294117647f, 0.7764705882352941f);
        }else if (labelCategory == 1)
        {
            labelColor = new Color(0.9294117647058824f, 0.6470588235294118f, 0.5411764705882353f);
        }
        else if (labelCategory == 2)
        {
            labelColor = new Color(0.3843137254901961f, 0.4588235294117647f, 0.5490196078431373f);
        }
        else if (labelCategory == 3)
        {
            labelColor = new Color(0.6588235294117647f, 0.6588235294117647f, 0.6588235294117647f);
        }
        else
        {
            labelColor = Color.white;
        }
    }

    // WEB SERVICES CALLBACKS
    public void GetCommentsCallBack(long status)
    {
        if (status == (long)WebService.Status.OK)
        {
            SetResponsePanel();
        }
        else if (status == (long)WebService.Status.NO_CONTENT)
        {
            heightToAdd = 0f;
            AddAddAReplyPrefab();
            //need to update height of comment object(gameObject) for the whole chat layout group order to work properly
            UpdateCommentObjectHeight();
			UpdateShowHideRepliesButtonText(isShowingReplies, nbValidatedReply);
        }
    }

    public void PostUserReplyCallBack(PostUserReply _postUserReplyService, GameObject validationPopUp)
    {
        if (_postUserReplyService.request_status == 0)
        {

            //UpdateShowHideRepliesButtonText(isShowingReplies, _postUserReplyService.nbReplies);

            validationPopUp.SetActive(true);
            validationPopUp.GetComponent<CommentValidationPopUpPanel>().StartOpenPanel();
        }
        else
        {
            if (_postUserReplyService.request_status == 1) //deleted
            {
                //UpdateShowHideRepliesButtonText(isShowingReplies, _postUserReplyService.nbReplies);
            }
        }
        transform.Find("RepliesParent").Find("AddReply(Clone)").Find("Text_BG").Find("InputField").GetComponent<InputField>().text = "";
    }

    private void DoVote(bool isFor, int idReply)
    {
        int userVote = isFor ? 1 : -1;
        ChatUIManagerLabel.instance.UseReplyVoteService(idReply, userVote);
    }

    public void PostUserReplyVoteCallBack(PostUserReplyVote _postUserReplyVoteService)
    {
        GameObject currentReply = null;
        foreach (GameObject replyObject in replyItems)
        {
            //Debug.Log("Registered id: " + _postUserReplyVoteService.idReply);
            //Debug.Log("Current object id: " + replyObject.GetComponent<ReplyId>().idReply);
            if (_postUserReplyVoteService.idReply == replyObject.GetComponent<ReplyId>().idReply)
            {
                currentReply = replyObject;
                break;
            }
        }
        if (currentReply == null)
        {
            Debug.LogError("Current reply is null");
            return;
        }
        Debug.Log("User vote ok");
        //set vote
        Transform ButtonFor = currentReply.transform.Find("Holder").Find("VoteButtonHolder").Find("Yes");
        Transform ButtonAgainst = currentReply.transform.Find("Holder").Find("VoteButtonHolder").Find("No");

        //set vote count (total votes)
        int totalVote = _postUserReplyVoteService.nbUp - _postUserReplyVoteService.nbDown;
        currentReply.transform.Find("Holder").Find("VoteButtonHolder").Find("VoteCount").GetComponent<Text>().text = totalVote.ToString();

        //if the logged user has already voted then the button is geyed out

        //if voted yes
        if (_postUserReplyVoteService.userVote == 1)
        {
            ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
        }
        //if voted no
        else if (_postUserReplyVoteService.userVote == -1)
        {
            ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
        }
        ButtonFor.GetComponent<Button>().interactable = false;
        ButtonAgainst.GetComponent<Button>().interactable = false;
    }
}
