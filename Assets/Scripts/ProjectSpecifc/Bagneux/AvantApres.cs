﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvantApres : MonoBehaviour {

	[SerializeField]
	private List<GameObject> AvantObjects = new List<GameObject> ();
	[SerializeField]
	private List<GameObject> ApresObjects = new List<GameObject> ();

	private bool isApres;

	private void Start () {
		forceApres ();
	}

	public void Swap(){
		isApres = !isApres;
		if (isApres) {
			Apres ();
		} else {
			Avant ();
		}
	}

	private void Avant(){
		foreach (GameObject go in AvantObjects) {
			go.SetActive (true);
		}

		foreach (GameObject go in ApresObjects) {
			go.SetActive (false);
		}
	}

	private void Apres(){
		foreach (GameObject go in AvantObjects) {
			go.SetActive (false);
		}

		foreach (GameObject go in ApresObjects) {
			go.SetActive (true);
		}
	}

	public void ForceAvant(){
		isApres = false;
		Avant ();
	}

	public void forceApres(){
		isApres = true;
		Apres ();
	}
}
