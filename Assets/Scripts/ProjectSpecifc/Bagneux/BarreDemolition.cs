﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarreDemolition : MonoBehaviour {

	[SerializeField]
	private Animation AnimationDemoTex;
	[SerializeField]
	private Transform mozartBuildingTex;
	[SerializeField]
	private Transform rossiniBuildingTex;
	[SerializeField]
	private Animation AnimationDemoNonTex;
	[SerializeField]
	private Transform mozartBuildingNonTex;
	[SerializeField]
	private Transform rossiniBuildingNonTex;
	[SerializeField]
	private GameObject ButtonModelTex;
	[SerializeField]
	private GameObject ButtonModel3D;

	private bool blockButton;


	private void Start () {
		blockButton = false;
	}
	
	public void PlayDemolition(){
		if(!blockButton){
			blockButton = true;

			if (ButtonModeTexture.isTex) {
				if (AnimationDemoTex.gameObject.activeInHierarchy) {
					AnimationDemoTex.Play ("building_demolition");
					ButtonModelTex.SetActive (false);
					ButtonModel3D.SetActive (false);
					Invoke("Reset", 6f);
				}
			} else {
				if (AnimationDemoNonTex.gameObject.activeInHierarchy) {
					AnimationDemoNonTex.Play ("building_demolition");
					ButtonModelTex.SetActive (false);
					ButtonModel3D.SetActive (false);
					Invoke ("Reset", 6f);
				}
			}
		}
	}

	private void Reset(){

		if (ButtonModeTexture.isTex) {
			AnimationDemoTex ["building_demolition"].time = 0;
			AnimationDemoTex.Stop("building_demolition");
			mozartBuildingTex.localPosition = new Vector3(0f, 0f, 0f);
			rossiniBuildingTex.localPosition = new Vector3(0f, 0f, 0f);
		} else {
			AnimationDemoNonTex ["building_demolition"].time = 0;
			AnimationDemoNonTex.Stop("building_demolition");
			mozartBuildingNonTex.localPosition = new Vector3(0f, 0f, 0f);
			rossiniBuildingNonTex.localPosition = new Vector3(0f, 0f, 0f);
		}
		ButtonModelTex.SetActive (true);
		ButtonModel3D.SetActive (true);
		blockButton = false;
	}
}
