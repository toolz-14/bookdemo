﻿using UnityEngine;

public class ButtonChangeCameraView : MonoBehaviour {

	[SerializeField]
	private float CameraMoveSpeed = 4f;
	[SerializeField]
	private FreeTerrainCamera FreeTerrainCamera;

	public Transform SectorPosition;
	private bool updateEnabled;
	private Transform CameraHolder;

	private void Start () {
		updateEnabled = false;
	}

	public void ChangeTheCamera(){
		FreeTerrainCamera.PreventMouseTouchInput (true);
		//remove camera stops when mouse over ui
		FreeTerrainCamera.ignoreUI = true;
		FreeTerrainCamera.MoveToSpeed(SectorPosition, CameraMoveSpeed);
		CameraHolder = FreeTerrainCamera.transform.Find("ConstraintBox").Find("CameraHolder");
		Invoke ("ReEnableInput", 0.1f);
	}

	private void ReEnableInput(){
		FreeTerrainCamera.PreventMouseTouchInput (false);
		updateEnabled = true;
	}

	private void Update () {
		if(updateEnabled){
			if(Vector3.Distance(CameraHolder.localPosition, SectorPosition.localPosition) < 0.001f){
				FreeTerrainCamera.ignoreUI = false;
				updateEnabled = false;
			}
		}
	}
}
