﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadSceneButton : MonoBehaviour {

    public void SaveSceneParam()
    {
        SaveLoadSceneManager.instance.ChangeScene();
    }
}
