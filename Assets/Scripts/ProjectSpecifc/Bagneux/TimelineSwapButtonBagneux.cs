﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimelineSwapButtonBagneux : MonoBehaviour {

    public static bool isOpen;
	public static bool doOnce;
	[SerializeField]
	private TimlineLerpBagneux timlineLerp;

	public void Swap(){

        if (!doOnce) {
			doOnce = true;
			isOpen = !isOpen;
            if (isOpen) {
				timlineLerp.StartLerpTimelineOpen ();
			} else {
				timlineLerp.StartLerpTimelineClose ();
			}
		}
	}

	public void ForceClose(){
		if (isOpen) {
			timlineLerp.StartLerpTimelineClose ();
			isOpen = false;
		}
	}
}
