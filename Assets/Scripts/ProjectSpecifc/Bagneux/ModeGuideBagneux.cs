﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeGuideBagneux : MonoBehaviour {

    private List<GameObject> step_1;

    private int step = 0;
    private const int MAXSTEP = 4;
    private bool blockUpdate;
    private bool swap;

	private void Start () {
        blockUpdate = true;
        swap = false;
    }

    public void SwapModeGuide()
    {
        swap = !swap;
        if (swap)
        {
            ActivateModeGuide();
        }
        else
        {
            DeactivateModeGuide();
        }
    }

    private void ActivateModeGuide()
    {
        //block all input on current UI

        //block freecamera inputs?

        transform.Find("Text").GetComponent<Text>().text = "Guide";
        blockUpdate = false;
    }

    private void DeactivateModeGuide()
    {
        //unblock all input on current UI

        //unblock freecamera inputs?

        transform.Find("Text").GetComponent<Text>().text = "Normal";
        blockUpdate = true;
    }

    private void Update()
    {
        if (!blockUpdate)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                print("RightArrow key was pressed");
                if (step < MAXSTEP)
                {
                    step++;
                    DoStep();
                }
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                print("LeftArrow key was pressed");
                if (step > 1)
                {
                    step--;
                    DoStep();
                }
            }
        }
    }

    private void DoStep()
    {
        if (step == 1)
        {
            print("Step 1");
        }
        else if (step == 2)
        {
            print("Step 2");
        }
        else if (step == 3)
        {
            print("Step 3");
        }
        else if (step == 4)
        {
            print("Step 4");
        }
    }
}
