﻿using UnityEngine;
using UnityEngine.UI;

public class LabelsZAC : MonoBehaviour {

    [SerializeField]
    private bool hasPDF= false;
    [SerializeField]
    private string pdfName = "";
    [SerializeField]
    private bool hasWbesitInfo = false;
    [SerializeField]
    private string urlPromoteur = "";
    [SerializeField]
    private string urlArchitect = "";
    [SerializeField]
    private string labelName = "";
    [SerializeField]
    private string info = "";
    [SerializeField]
    private string info2 = "";

    private bool isOpen;
    private bool isOpen2;
    private bool isOpen3;

    private void Start()
    {
        isOpen = false;
        isOpen2 = false;
        isOpen3 = false;
    }

    public void OnButtonClick()
    {
        GameObject Panel = LabelZacData.instance.Panel;
        Panel.SetActive(true);
        if (hasWbesitInfo)
        {
            Panel.transform.Find("Button_Promoteur").gameObject.SetActive(true);
            Panel.transform.Find("Button_Promoteur").GetComponent<Button>().onClick.RemoveAllListeners();
            Panel.transform.Find("Button_Promoteur").GetComponent<Button>().onClick.AddListener(() => DisplayPromoteurWebsite());
            Panel.transform.Find("Button_Architect").gameObject.SetActive(true);
            Panel.transform.Find("Button_Architect").GetComponent<Button>().onClick.RemoveAllListeners();
            Panel.transform.Find("Button_Architect").GetComponent<Button>().onClick.AddListener(() => DisplayArchitectWebsite());
        }
        else
        {
            Panel.transform.Find("Button_Promoteur").gameObject.SetActive(false);
            Panel.transform.Find("Button_Architect").gameObject.SetActive(false);
        }
        if (hasPDF)
        {
            Panel.transform.Find("Button_PDF").gameObject.SetActive(true);
            Panel.transform.Find("Button_PDF").GetComponent<Button>().onClick.RemoveAllListeners();
            Panel.transform.Find("Button_PDF").GetComponent<Button>().onClick.AddListener(() => DisplayPDF());
        }
        else
        {
            Panel.transform.Find("Button_PDF").gameObject.SetActive(false);
        }

        Panel.transform.Find("Text_Info2").GetComponent<Text>().text ="";
        labelName = labelName.Replace("NEWLINE", "\n");
        info = info.Replace("NEWLINE", "\n");
		info2 = info2.Replace("NEWLINE", "\n");
        Panel.transform.Find("Text_Name").GetComponent<Text>().text = labelName;
        Panel.transform.Find("Text_Info").GetComponent<Text>().text = info;
		Panel.transform.Find("Text_Info2").GetComponent<Text>().text = info2;		    
	}

    private void DisplayPDF()
    {
        if (!isOpen3)
        {
            isOpen3 = true;

            if (pdfName != null && pdfName != "")
            {
                LabelZacData.instance.pdfViewer.FileName = pdfName;
                LabelZacData.instance.PDFViewer.SetActive(true);
                LabelZacData.instance.canvasUIMain.SetActive(false);
            }
            Invoke("Delayed", 0.2f);
        }
    }

    private void DisplayPromoteurWebsite()
    {
        if (!isOpen)
        {
            isOpen = true;
            Application.OpenURL(urlPromoteur);
            Invoke("Delayed", 0.2f);
        }
    }

    private void DisplayArchitectWebsite()
    {
        if (!isOpen2)
        {
            isOpen2 = true;
            Application.OpenURL(urlArchitect);
            Invoke("Delayed", 0.2f);
        }
    }

    private void Delayed()
    {
        isOpen = false;
        isOpen2 = false;
        isOpen3 = false;
    }
}
