﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LocalisationData : MonoBehaviour {

	public string NamesOfWantedBundle;
	public MeshRenderer theRenderer;
	public Button ProjectReturnButton;
	[HideInInspector]
	public IsImage IsImage;

	private void Start () {
		
	}

	private void OnEnable(){
		IsImage = transform.parent.parent.GetComponent<IsImage> ();
	}
}
