﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideChatLabelsZAC : MonoBehaviour {

	[SerializeField]
	private LabelEditorSaveLoad labelEditorSaveLoad;
    [SerializeField]
    private SelectAndSaveLoadLabelsOnline selectAndSaveLoadLabelsOnline;

    public static bool isShowing = false;
    
	
	public void ShowChatLabelsZAC(){
		isShowing = true;
        selectAndSaveLoadLabelsOnline.LoadOnline();

    }

	public void HideChatLabelsZAC(){
		isShowing = false;
        selectAndSaveLoadLabelsOnline.DeselectLabelsWithComments();
        selectAndSaveLoadLabelsOnline.UnDisplayLabelButton();
        labelEditorSaveLoad.MaskAllLabels();
    }
}
