﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectPopupDataBagneux : MonoBehaviour {

	[SerializeField]
	private string nameEcole;
	[SerializeField]
	private string nbEleves;
	[SerializeField]
	private string nbClasses;
	[SerializeField]
	private string repartitionParClasses;
	[SerializeField]
	private string historique;
	[SerializeField]
	private string analyseGraphique;
	[SerializeField]
	private string acces;
	[SerializeField]
	private string contact;
	[SerializeField]
	private string adresse;

	[SerializeField]
	private ProjectPopupManagemanentBagneux projectPopupManagemanent;



	private void Start () {

	}

	public void SetAndOpenPopupPanel(){

		if (projectPopupManagemanent != null) {
			projectPopupManagemanent.ImageBg.SetActive (true);

			//set popup values
			projectPopupManagemanent.SetPopupValues (nameEcole, nbEleves, nbClasses, repartitionParClasses, historique, analyseGraphique, acces, contact, adresse, this, GetComponent<Toolz.Module>()); 

			//open popup
			projectPopupManagemanent.OpenHotspotPopupPanel ();
		}
	}
}
