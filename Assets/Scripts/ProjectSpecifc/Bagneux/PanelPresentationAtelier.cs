﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelPresentationAtelier : MonoBehaviour {

    [SerializeField]
    private List<string> topUITitle = new List<string>();
    [SerializeField]
    private List<string> pdfButton_1_text = new List<string>();
    [SerializeField]
    private List<string> pdfButton_1_PdfName = new List<string>();
    [SerializeField]
    private List<string> pdfButton_2_text = new List<string>();
    [SerializeField]
    private List<string> pdfButton_2_PdfName = new List<string>();
    [SerializeField]
    private Text topTitle;
    [SerializeField]
    private GameObject PDFViewer;
    [SerializeField]
    private Paroxe.PdfRenderer.PDFViewer pdfViewer;

    [SerializeField]
    private Button pdfButton1;
    [SerializeField]
    private Text pdfButton1Text;
    [SerializeField]
    private Button pdfButton2;
    [SerializeField]
    private Text pdfButton2Text;
    private GameObject canvasUIMain;

    private int atelierIndex;

    private bool doOnce_1;
    private bool doOnce_2;

    private void Start () {
        doOnce_1 = false;
        doOnce_2 = false;
        canvasUIMain = transform.parent.gameObject;
    }

    public void SetPresentationAtelier(int index)
    {
        atelierIndex = index;

        //set top ui title
        topTitle.text = topUITitle[index];

        //set pdf button 1 onclick: text + pdf name
        pdfButton1Text.text = pdfButton_1_text[atelierIndex];
        pdfButton1.onClick.AddListener(() => Button_OpenPDF1(pdfButton_1_PdfName[atelierIndex]));

        //set pdf button 1 onclick: text + pdf name
        pdfButton2Text.text = pdfButton_2_text[atelierIndex];
        pdfButton2.onClick.AddListener(() => Button_OpenPDF2(pdfButton_2_PdfName[atelierIndex]));
    }

    public void Button_OpenPDF1(string pdfName)
    {
        if (!doOnce_1)
        {
            doOnce_1 = true;
            doOnce_2 = true;
            if (pdfName != null && pdfName != "")
            {
                pdfViewer.FileName = pdfName;
                PDFViewer.SetActive(true);
                canvasUIMain.SetActive(false);
            }
            Invoke("Delayed", 0.2f);
        }
    }

    public void Button_OpenPDF2(string pdfName)
    {
        if (!doOnce_2)
        {
            doOnce_1 = true;
            doOnce_2 = true;
            //Debug.Log("Button_OpenPDF2, pdfName = " + pdfName);
            if (pdfName != null && pdfName != "")
            {
                pdfViewer.FileName = pdfName;
                PDFViewer.SetActive(true);
                canvasUIMain.SetActive(false);
            }
            Invoke("Delayed", 0.2f);
        }
    }

    private void Delayed()
    {
        doOnce_1 = false;
        doOnce_2 = false;
    }
}
