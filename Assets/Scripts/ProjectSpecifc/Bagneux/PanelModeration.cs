﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Toolz.WebServices;

public class PanelModeration : MonoBehaviour
{
	[SerializeField]
	private Color itemsNotValidated = new Color ();
	[SerializeField]
	private Color allItemsValidated = new Color ();
    [SerializeField]
    private GameObject hotspotListItemPrefab;
    [SerializeField]
    private Transform hotspotListItemParent;
    [SerializeField]
    private GameObject commentListItemPrefab;
    [SerializeField]
    private Transform commentListItemParent;
    [SerializeField]
    private GameObject repliesListItemPrefab;
    [SerializeField]
    private Transform repliesListItemParent;
    [SerializeField]
    private Text userName;
    [SerializeField]
    private Text amountAdminConnected;
	[SerializeField]
	private GameObject imageMultipleModWarning;
	[SerializeField]
	private GameObject loaderMain;
	[SerializeField]
	private GameObject loaderLabels;
	[SerializeField]
	private GameObject loaderComments;
	[SerializeField]
	private GameObject loaderReplies;
    [SerializeField]
    private GameObject globalErrorManager; // boolean that indicated to refresh data

    private List<LabelEditorObject> labelEditorObjectList;
    private List<GameObject> hotspotItemsList = new List<GameObject>();
    private List<GameObject> commentItemsList = new List<GameObject>();
    private List<GameObject> repliesItemsList = new List<GameObject>();
    private GameObject _tempObject;

    //webservices
    private GetComments _getCommentsService;
    private PostUserComment _postUserCommentService;
    private GetCommentReplies _getCommentRepliesService;
    private PostUserReply _postUserReplyService;
    private GetSavedLabels _getSavedLabels;
    private PostSavedLabels _postSavedLabels;
    private GetOnlineAdmin _getOnlineAdmin;
    private PostOnlineAdmin _postOnlineAdmin;

    private GameObject currentObjectClicked;
    private GameObject currentComment;
    private GameObject currentLabel;


    private void Awake()
    {
        GameObject webservice = GameObject.FindGameObjectWithTag("WebServices");

        _getCommentsService = webservice.GetComponent<GetComments>();
        _getCommentsService.onComplete += _getCommentsService_onComplete;
        _getCommentsService.onError += _getCommentsService_onError;

        _getCommentRepliesService = webservice.GetComponent<GetCommentReplies>();
        _getCommentRepliesService.onComplete += _getCommentRepliesService_onComplete;
        _getCommentRepliesService.onError += _getCommentRepliesService_onError;

        _postUserReplyService = webservice.GetComponent<PostUserReply>();
        _postUserReplyService.onComplete += _postUserReply_onComplete;
        _postUserReplyService.onError += _postUserReply_onError;

        _postUserCommentService = webservice.GetComponent<PostUserComment>();
        _postUserCommentService.onComplete += _postUserCommentService_onComplete;
        _postUserCommentService.onError += _postUserCommentService_onError;

        _getSavedLabels = webservice.GetComponent<GetSavedLabels>();
        _getSavedLabels.onComplete += _getSavedLabels_onComplete;
        _getSavedLabels.onError += _getSavedLabels_onError;

        _postSavedLabels = webservice.GetComponent<PostSavedLabels>();
        _postSavedLabels.onComplete += _postSavedLabels_onComplete;
        _postSavedLabels.onError += _postSavedLabels_onError;

        _getOnlineAdmin = webservice.GetComponent<GetOnlineAdmin>();
        _getOnlineAdmin.onComplete += _getOnlineAdmin_onComplete;
        _getOnlineAdmin.onError += _getOnlineAdmin_onError;

        _postOnlineAdmin = webservice.GetComponent<PostOnlineAdmin>();
        _postOnlineAdmin.onComplete += _postOnlineAdmin_onComplete;
        _postOnlineAdmin.onError += _postOnlineAdmin_onError;
    }

    void OnApplicationQuit()
    {
        _postOnlineAdmin.isOnline = false;
        _postOnlineAdmin.UseWebService();
        //Debug.Log("Application ending after " + Time.time + " seconds");
    }

    private void Start()
    {
		loaderMain.SetActive (true);
        _postOnlineAdmin.isOnline = true;
        _postOnlineAdmin.UseWebService();
    }

    public void SetPanelModeration()
    {
		loaderMain.SetActive (false);
        //fill header user name
        userName.text = SaveLoadManager.LoadFirstname() + " " +SaveLoadManager.LoadLastname();

        //fill header amount of admin connected
        if (_getOnlineAdmin.nbOnlineAdmin > 1)
        {
			imageMultipleModWarning.SetActive(true);
        }
        amountAdminConnected.text = "Il y a actuellement ("+ _getOnlineAdmin.nbOnlineAdmin + ") administrateur en ligne";

        //clear comments list if filled
        if (commentItemsList.Count > 0)
        {
            foreach (GameObject go in commentItemsList)
            {
                Destroy(go);
            }
            commentItemsList.Clear();
        }

        //clear replies list if filled
        if (repliesItemsList.Count > 0)
        {
            foreach (GameObject go in repliesItemsList)
            {
                Destroy(go);
            }
            repliesItemsList.Clear();
        }
        labelEditorObjectList = new List<LabelEditorObject>();

        // Get all labels
		loaderLabels.SetActive(true);
        _getSavedLabels.UseWebService();
    }

    private void LoadLabelEditorObjects(string LoadMasterString)
    {
        //string LoadMasterString = "";
        //LoadMasterString = SaveLoadManagerLabel3D.LoadLabelEditorObjects();

        if (LoadMasterString == null)
        {
            //nothing saved
            Debug.Log("Can't Load labels, nothing was saved");
        }
        else if (LoadMasterString == "")
        {
            //problem with save or load
            Debug.Log("Can't Load labels, LoadMasterString empty");
        }
        else
        {
            //clear object list
            if (labelEditorObjectList != null && labelEditorObjectList.Count > 0)
            {
                foreach (LabelEditorObject label in labelEditorObjectList)
                {
                    Destroy(label.gameObject);
                }
                //clear object list
                labelEditorObjectList.Clear();
            }

            string[] theSplitString = LoadMasterString.Split('_');

            for (int i = 0; i < theSplitString.Length; i++)
            {
                if (theSplitString[i] != "")
                {
                    //a single label object is 7 string values
                    string[] theSubSplitString = theSplitString[i].Split('/');

                    //instantiate proper object type
                    LabelEditorObject labelEditorObject = new LabelEditorObject();

                    //set name
                    labelEditorObject.labelTextModeration = theSubSplitString[0];

                    labelEditorObject.id = _getSavedLabels.labelJSONObject.labelsList[i].id;
                    labelEditorObject.phase = _getSavedLabels.labelJSONObject.labelsList[i].phase;
                    labelEditorObject.category = _getSavedLabels.labelJSONObject.labelsList[i].category;
                    labelEditorObject.idAuthor = _getSavedLabels.labelJSONObject.labelsList[i].idAuthor;
                    labelEditorObject.publishedTime = _getSavedLabels.labelJSONObject.labelsList[i].publishedTime;
                    labelEditorObject.status = _getSavedLabels.labelJSONObject.labelsList[i].status;
                    labelEditorObject.authorName = _getSavedLabels.labelJSONObject.labelsList[i].authorName;
                    labelEditorObject.nbNonvalidatedElem = _getSavedLabels.labelJSONObject.labelsList[i].nbComments + _getSavedLabels.labelJSONObject.labelsList[i].nbReplies;
                    labelEditorObject.isDeleted = false;

                    //add to object list
                    labelEditorObjectList.Add(labelEditorObject);
                }
            }
            PopulateHotspotList();
        }
    }

    private void PopulateHotspotList()
    {
        if (hotspotItemsList.Count > 0)
        {
            foreach (GameObject go in hotspotItemsList)
            {
                Destroy(go);
            }
            hotspotItemsList.Clear();
        }

        foreach (LabelEditorObject leo in labelEditorObjectList)
        {
            _tempObject = (GameObject)Instantiate(hotspotListItemPrefab);
            _tempObject.SetActive(true);

            _tempObject.transform.SetParent(hotspotListItemParent);
            _tempObject.transform.localScale = Vector3.one;

            // HS text
            _tempObject.transform.Find("Text_LabelText").GetComponent<Text>().text = leo.labelTextModeration;

            // HS date
            _tempObject.transform.Find("Text_LabelDate").GetComponent<Text>().text = leo.publishedTime.ToString("dd/MM/yyyy HH:mm");

            // HS owner
            _tempObject.transform.Find("Text_LabelAuthor").GetComponent<Text>().text = leo.authorName;

            //display amountCommentNotValidated
            _tempObject.transform.Find("Text_CommentNotValidated").Find("Text_AmountCommentNotValidated").GetComponent<Text>().text =  leo.nbNonvalidatedElem.ToString(); //"Nombre d'éléments non validés : " +

            //display color
            if (leo.nbNonvalidatedElem > 0){
				_tempObject.GetComponent<Image>().color = itemsNotValidated;
			}else{
				_tempObject.GetComponent<Image>().color = allItemsValidated;
			}

            //onclick
            GameObject labelObject = _tempObject;
            _tempObject.GetComponent<Button>().onClick.AddListener(() => GetCommentList(leo.id, labelObject));

            //onclick validation
            if (leo.status == 1) // comment has already been validated
                _tempObject.transform.Find("ValidationHolder").gameObject.SetActive(false);
            else
            {
                Button deleteButton = _tempObject.transform.Find("ValidationHolder").Find("Button_DeleteItem").GetComponent<Button>();
                Button validateButton = _tempObject.transform.Find("ValidationHolder").Find("Button_ValidateItem").GetComponent<Button>();
                GameObject temp = _tempObject;
                deleteButton.onClick.AddListener(() => OnClickValidationHotspotButton(leo.id, true, 0, temp));
                validateButton.onClick.AddListener(() => OnClickValidationHotspotButton(leo.id, false, 1, temp));
            }

            //resize
			_tempObject.GetComponent<ResizeContainer>().StartResize();

            //add to list
            hotspotItemsList.Add(_tempObject);
        }
        //resize scroll container
		hotspotListItemParent.GetComponent<ResizeContainer>().StartResize();
		loaderLabels.SetActive(false);
    }

    public void OnClickValidationHotspotButton(int idLabel, bool isDeleted, int status, GameObject theItem)
    {
        currentObjectClicked = theItem;

        //Call webservice
        _postSavedLabels.id = idLabel;
        _postSavedLabels.isDeleted = isDeleted;
        _postSavedLabels.labelStatus = status;
        _postSavedLabels.isAdmin = true;

        _postSavedLabels.UseWebService();
    }

    private void GetCommentList(int selectedLabelID,GameObject parentLabel)
    {
        currentLabel = parentLabel;
        loaderComments.SetActive (true);
        _getCommentsService.idProject = selectedLabelID;
        _getCommentsService.UseWebService();
    }

    private void PopulateCommentsList()
    {
        //clear replies list if filled
        if (repliesItemsList.Count > 0)
        {
            foreach (GameObject go in repliesItemsList)
            {
                Destroy(go);
            }
            repliesItemsList.Clear();
        }

        foreach (GameObject go in commentItemsList)
        {
            Destroy(go);
        }
        commentItemsList.Clear();

        List<Comment> comments = _getCommentsService.Comments;

        foreach (Comment comment in comments)
        {
            _tempObject = (GameObject)Instantiate(commentListItemPrefab);
            _tempObject.SetActive(true);
            _tempObject.transform.SetParent(commentListItemParent);
            _tempObject.transform.localScale = Vector3.one;

            // Comment text
            _tempObject.transform.Find("Text_LabelText").GetComponent<Text>().text = comment.text;

            // Comment date
            _tempObject.transform.Find("Text_LabelDate").GetComponent<Text>().text = comment.publishedTime.ToString("dd/MM/yyyy HH:mm");

            // Comment vote
            int temp = comment.nbUp - comment.nbDown;
            _tempObject.transform.Find("Text_LabelVote").GetComponent<Text>().text = "Vote: " + temp.ToString();

            // Comment owner
            _tempObject.transform.Find("Text_LabelAuthor").GetComponent<Text>().text = comment.name + " " + comment.firstname;

            //display amountRepliesNotValidated
            _tempObject.transform.Find("Text_ReplyNotValidated").Find("Text_AmountReplyNotValidated").GetComponent<Text>().text = comment.nbUnvalidatedResponses.ToString();
			//display color
			if(comment.nbUnvalidatedResponses > 0){
				_tempObject.GetComponent<Image>().color = itemsNotValidated;
			}else{
				_tempObject.GetComponent<Image>().color = allItemsValidated;
			}

            //onclick
            GameObject commentObject = _tempObject;
            _tempObject.GetComponent<Button>().onClick.AddListener(() => GetRepliesList((int)comment.idComment, commentObject));

            //onclick validation
            if (comment.status == 1) // comment has already been validated
                _tempObject.transform.Find("ValidationHolder").gameObject.SetActive(false);
            else
            {
                Button deleteButton = _tempObject.transform.Find("ValidationHolder").Find("Button_DeleteItem").GetComponent<Button>();
                Button validateButton = _tempObject.transform.Find("ValidationHolder").Find("Button_ValidateItem").GetComponent<Button>();
                GameObject tempItem = _tempObject;
                deleteButton.onClick.AddListener(() => OnClickValidationCommentButton((int)comment.idComment, true, tempItem));
                validateButton.onClick.AddListener(() => OnClickValidationCommentButton((int)comment.idComment, false,  tempItem));
            }

            //resize
			_tempObject.GetComponent<ResizeContainer>().StartResize();

            //add to list
            commentItemsList.Add(_tempObject);
        }

        //resize scroll container
		commentListItemParent.GetComponent<ResizeContainer>().StartResize();
		loaderComments.SetActive (false);
    }

    public void OnClickValidationCommentButton(int idComment, bool isDeleted, GameObject theItem)
    {
        currentObjectClicked = theItem;
        _postUserCommentService.idComment = idComment;
        _postUserCommentService.isDeleted = isDeleted;

        _postUserCommentService.request_status = 3;
        _postUserCommentService.UseWebService();
    }

    private void GetRepliesList(int selectedCommentID,GameObject parentReply)
    {
        currentComment = parentReply;
		loaderReplies.SetActive (true);
        _getCommentRepliesService.idComment = (uint)selectedCommentID;
        _getCommentRepliesService.UseWebService();
    }

    private void PopulateRepliesList()
    {
        foreach (GameObject go in repliesItemsList)
        {
            Destroy(go);
        }
        repliesItemsList.Clear();

        List<Reply> replies = _getCommentRepliesService.Replies;

        foreach (Reply reply in replies)
        {
            _tempObject = (GameObject)Instantiate(repliesListItemPrefab);
            _tempObject.SetActive(true);
            _tempObject.transform.SetParent(repliesListItemParent);
            _tempObject.transform.localScale = Vector3.one;

            // reply text
            _tempObject.transform.Find("Text_LabelText").GetComponent<Text>().text = reply.text;

            // reply date
            _tempObject.transform.Find("Text_LabelDate").GetComponent<Text>().text = reply.publishedTime.ToString("dd/MM/yyyy HH:mm");

            // reply vote
            int temp = reply.nbUp - reply.nbDown;
            _tempObject.transform.Find("Text_LabelVote").GetComponent<Text>().text = "Vote: " + temp.ToString();

            // Comment owner
            _tempObject.transform.Find("Text_LabelAuthor").GetComponent<Text>().text = reply.name + " " + reply.firstname;

            //onclick validation
            if (reply.status == 1) // comment has already been validated
                _tempObject.transform.Find("ValidationHolder").gameObject.SetActive(false);
            else
            {
                Button deleteButton = _tempObject.transform.Find("ValidationHolder").Find("Button_DeleteItem").GetComponent<Button>();
                Button validateButton = _tempObject.transform.Find("ValidationHolder").Find("Button_ValidateItem").GetComponent<Button>();
                GameObject tempItem = _tempObject;
                deleteButton.onClick.AddListener(() => OnClickValidationReplyButton(reply.idReply, reply.idReplyAuthor, (int)reply.idOriginalComment, reply.idAnsweredRep, reply.text, true, tempItem));
                validateButton.onClick.AddListener(() => OnClickValidationReplyButton(reply.idReply, reply.idReplyAuthor, (int)reply.idOriginalComment, reply.idAnsweredRep, reply.text, false, tempItem));
            }

            //resize
			_tempObject.GetComponent<ResizeContainer>().StartResize();
            //add to list
            repliesItemsList.Add(_tempObject);
        }
        //resize scroll container
		repliesListItemParent.GetComponent<ResizeContainer>().StartResize();
		loaderReplies.SetActive (false);
    }

    public void OnClickValidationReplyButton(int idReply, int idAuthor, int idComment, int idAnsweredReply, string msg, bool isDeleted, GameObject theItem)
    {
        currentObjectClicked = theItem;

        _postUserReplyService.idComment = idComment;
        _postUserReplyService.idReply = idReply;
        _postUserReplyService.idAuthor = idAuthor;
        _postUserReplyService.idAnsweredReply = idAnsweredReply;
        _postUserReplyService.isDeleted = isDeleted;
        _postUserReplyService.text = msg;

        _postUserReplyService.request_status = 3;

        _postUserReplyService.UseWebService();
    }

    public void ButtonBack()
    {
		loaderMain.SetActive (true);
        _postOnlineAdmin.isOnline = false;
        _postOnlineAdmin.UseWebService();
    }

    private void ClosePanel()
    {
        this.GetComponent<LoadAScene>().LoadTheScene();
    }

    //---------Webservices

    private void _getSavedLabels_onError(long status, string message)
    {
        Debug.Log("_getSavedLabels_onError, status :" + status + ", message :" + message);
		loaderLabels.SetActive(false);

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _getSavedLabels_onComplete(long status, string message)
    {
        Debug.Log("_getSavedLabels_onComplete, status :" + status + ", message : Load success" + message);

        LoadLabelEditorObjects(_getSavedLabels.labelJSONObject.fulllLoadMasterString);
    }

    private void _getCommentsService_onError(long status, string message)
    {
        Debug.Log("Error while loading discussion: " + message);
		loaderComments.SetActive (false);

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _getCommentsService_onComplete(long status, string message)
    {
        PopulateCommentsList();
    }

    private void _getCommentRepliesService_onError(long status, string message)
    {
        Debug.Log("GetCommentReplies service Error : " + message);
		loaderReplies.SetActive (false);

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _getCommentRepliesService_onComplete(long status, string message)
    {
        //Debug.Log("_getCommentRepliesService_onComplete status : " + status);
        PopulateRepliesList();
    }

    private void _postUserCommentService_onError(long status, string message)
    {
        Debug.Log("Post user comment ERROR : " + message);

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _postUserCommentService_onComplete(long status, string message)
    {
        Debug.Log("Post user comment OK : " + message);
        //labelEditorObject.nbNonvalidatedElem--;
        if (_postUserCommentService.isDeleted == true)
        {
            Destroy(currentObjectClicked);
			//resize scroll container
			commentListItemParent.GetComponent<ResizeContainer>().GetChildrensAndResize();
            if (repliesItemsList.Count > 0)
            {
                foreach (GameObject go in repliesItemsList)
                {
                    Destroy(go);
                }
                repliesItemsList.Clear();
            }
        }
        else
        {
            currentObjectClicked.transform.Find("ValidationHolder").gameObject.SetActive(false);
			//resize
			currentObjectClicked.GetComponent<ResizeContainer>().StartResize();
        }
        
        if (currentLabel != null)
        {
            string textToConvert = currentLabel.transform.Find("Text_CommentNotValidated").Find("Text_AmountCommentNotValidated").GetComponent<Text>().text.ToString();
            int nbNonvalidatedElem = int.Parse(textToConvert);
            nbNonvalidatedElem--;
            currentLabel.transform.Find("Text_CommentNotValidated").Find("Text_AmountCommentNotValidated").GetComponent<Text>().text = nbNonvalidatedElem.ToString();
            if (nbNonvalidatedElem == 0)
            {
                currentLabel.GetComponent<Image>().color = allItemsValidated;
            }
        }
    }

    private void _postUserReply_onError(long status, string message)
    {
        Debug.Log("Post User Reply ERROR : " + message);

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _postUserReply_onComplete(long status, string message)
    {
        //Debug.Log("Post User Reply OK");

        if (_postUserReplyService.isDeleted == true)
        {
            Destroy(currentObjectClicked);
			//resize scroll container
			repliesListItemParent.GetComponent<ResizeContainer>().GetChildrensAndResize();
        }
        else
        {
            currentObjectClicked.transform.Find("ValidationHolder").gameObject.SetActive(false);
			//resize
			currentObjectClicked.GetComponent<ResizeContainer>().StartResize();
        }

        if (currentLabel != null)
        {
            string textToConvert = currentLabel.transform.Find("Text_CommentNotValidated").Find("Text_AmountCommentNotValidated").GetComponent<Text>().text.ToString();
            int nbNonvalidatedElem = int.Parse(textToConvert);
            nbNonvalidatedElem--;
            currentLabel.transform.Find("Text_CommentNotValidated").Find("Text_AmountCommentNotValidated").GetComponent<Text>().text = nbNonvalidatedElem.ToString();
            if (nbNonvalidatedElem == 0)
            {
                currentLabel.GetComponent<Image>().color = allItemsValidated;
            }
        }
        if (currentComment != null)
        {
            string textToConvert = currentComment.transform.Find("Text_ReplyNotValidated").Find("Text_AmountReplyNotValidated").GetComponent<Text>().text.ToString();
            int nbNonvalidatedElem = int.Parse(textToConvert);
            nbNonvalidatedElem--;
            currentComment.transform.Find("Text_ReplyNotValidated").Find("Text_AmountReplyNotValidated").GetComponent<Text>().text = nbNonvalidatedElem.ToString();
            if (nbNonvalidatedElem == 0)
            {
                currentComment.GetComponent<Image>().color = allItemsValidated;
            }
        }
    }

    private void _postSavedLabels_onError(long status, string message)
    {
        Debug.Log("_postSavedLabels_onError, status :" + status + ", message :" + message);

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    // Request has correctly passed
    private void _postSavedLabels_onComplete(long status, string message)
    {
        //Debug.Log("_postSavedLabels_onComplete, status :" + status + ", message :" + message);

        if(_postSavedLabels.isDeleted == true)
        {
			Destroy(currentObjectClicked);
			//resize scroll container
			hotspotListItemParent.GetComponent<ResizeContainer>().GetChildrensAndResize();
            //clear comments list if filled
            if (commentItemsList.Count > 0)
            {
                foreach (GameObject go in commentItemsList)
                {
                    Destroy(go);
                }
                commentItemsList.Clear();
            }

            //clear replies list if filled
            if (repliesItemsList.Count > 0)
            {
                foreach (GameObject go in repliesItemsList)
                {
                    Destroy(go);
                }
                repliesItemsList.Clear();
            }
        }
        else
        {
            currentObjectClicked.transform.Find("ValidationHolder").gameObject.SetActive(false);
		   //resize
			currentObjectClicked.GetComponent<ResizeContainer>().StartResize();																	 
        }
    }

    private void _getOnlineAdmin_onError(long status, string message)
    {
        Debug.Log("_getOnlineAdmin_onError, status :" + status + ", message :" + message);
		loaderMain.SetActive (false);
		ClosePanel ();

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _getOnlineAdmin_onComplete(long status, string message)
    {
        SetPanelModeration();
        //Debug.Log("_getOnlineAdmin_onComplete, status :" + status + ", message :" + message);
    }

    private void _postOnlineAdmin_onError(long status, string message)
    {
        Debug.Log("_postOnlineAdmin_onError, status :" + status + ", message :" + message);
		loaderMain.SetActive (false);
		ClosePanel ();

        globalErrorManager.gameObject.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _postOnlineAdmin_onComplete(long status, string message)
    {
        //Debug.Log("_postOnlineAdmin_onComplete, status :" + status + ", message :" + message);
        if (_postOnlineAdmin.isOnline) // user first goes into admin panel
        {
            _getOnlineAdmin.UseWebService();
        }
        else
        {
            ClosePanel(); // user exit panel
        }
    }
}
