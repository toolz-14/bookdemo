﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonShematic3DBagneux : MonoBehaviour {

	[SerializeField]
	private Sprite sprite3D;
	[SerializeField]
	private Sprite spriteShematic;

	[SerializeField]
	private GameObject objects3D;
	[SerializeField]
	private GameObject object2D;

	public static bool isSchematic;
	private bool doOnce;

	private void Start () {
		isSchematic = false;
        GetComponent<Image>().sprite = sprite3D;
    }

	public void Swap(){
		if (!doOnce) {
			doOnce = true;
			isSchematic = !isSchematic;
			if (isSchematic) {
				Schematic ();
			} else {
				Objects3D ();
			}
		}
	}

	public void Force3D(){
		if (isSchematic) {
			GetComponent<Image> ().sprite = sprite3D;

			if (objects3D != null)
				objects3D.SetActive (true);
			if (object2D != null)
				object2D.SetActive (false);
		
			isSchematic = false;
		}
	}

	public void ForceSchematic(){
		if (!isSchematic) {
			GetComponent<Image> ().sprite = spriteShematic;

			if(objects3D != null)
				objects3D.SetActive(false);
			if(object2D != null)
				object2D.SetActive (true);

			isSchematic = true;
		}
	}

	private void Objects3D(){
		GetComponent<Image> ().sprite = sprite3D;

		if(objects3D != null)
		objects3D.SetActive(true);
		if(object2D != null)
		object2D.SetActive (false);

		Invoke ("Delayed1", 0.2f);
	}

	private void Delayed1(){
		doOnce = false;
	}

	private void Schematic(){
		GetComponent<Image> ().sprite = spriteShematic;

		if(objects3D != null)
		objects3D.SetActive(false);
		if(object2D != null)
		object2D.SetActive (true);

		Invoke ("Delayed2", 0.2f);
	}

	private void Delayed2(){
		doOnce = false;
	}
}
