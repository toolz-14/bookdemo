﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHide3D : MonoBehaviour {

	[SerializeField]
	private List<GameObject> models = new List<GameObject> ();
	[SerializeField]
	private GameObject ModelBeforeTex;
	[SerializeField]
	private GameObject ModelBeforeNonTex;
	[SerializeField]
	private GameObject ButtonModelTex;

    private bool swap;

	private void Start () {
		swap = false;
	}

	public void SwapModels(){
		swap = !swap;
		if (swap) {
            Hide3D();
        } else {
            Show3D();
        }
	}

    private void Hide3D()
    {
        foreach (GameObject go in models)
        {
            if (go != null && go.activeInHierarchy)
            {
                go.SetActive(false);
            }
        }
        if (ButtonModeTexture.isTex)
        {
            ModelBeforeTex.SetActive(false);
        }
        else
        {
            ModelBeforeNonTex.SetActive(false);
        }
        ButtonModelTex.SetActive(false);

        transform.Find("Text").GetComponent<Text>().text = "Vide";
    }

    private void Show3D()
    {
        foreach (GameObject go in models)
        {
            if (go != null && !go.activeInHierarchy)
            {
                go.SetActive(true);
            }
        }
        if (ButtonModeTexture.isTex)
        {
            ModelBeforeTex.SetActive(true);
        }
        else
        {
            ModelBeforeNonTex.SetActive(true);
        }
        ButtonModelTex.SetActive(true);

        transform.Find("Text").GetComponent<Text>().text = "Model 3D";
    }

    public void ForceHide3D()
    {
        if (!swap)
        {
            foreach (GameObject go in models)
            {
                if (go != null && go.activeInHierarchy)
                {
                    go.SetActive(false);
                }
            }
            if (ButtonModeTexture.isTex)
            {
                ModelBeforeTex.SetActive(false);
            }
            else
            {
                ModelBeforeNonTex.SetActive(false);
            }
            ButtonModelTex.SetActive(false);


            swap = true;
            transform.Find("Text").GetComponent<Text>().text = "Vide";
        }
    }

    public void ForceShow3D()
    {
        if (swap)
        {
            foreach (GameObject go in models)
            {
                if (go != null && !go.activeInHierarchy)
                {
                    go.SetActive(true);
                }
            }
            if (ButtonModeTexture.isTex)
            {
                ModelBeforeTex.SetActive(true);
            }
            else
            {
                ModelBeforeNonTex.SetActive(true);
            }
            ButtonModelTex.SetActive(true);


            swap = false;
            transform.Find("Text").GetComponent<Text>().text = "Model 3D";
        }
    }
}
