﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlicedEcole : MonoBehaviour {

	public List<GameObject> otherBuildings = new List<GameObject>();

	private void Start () {
		
	}

	public void EnterEcole(){
		foreach(GameObject go in otherBuildings){
			go.SetActive(false);
		}
	}

	public void ExitEcole(){
		foreach(GameObject go in otherBuildings){
			go.SetActive(true);
		}
	}
}
