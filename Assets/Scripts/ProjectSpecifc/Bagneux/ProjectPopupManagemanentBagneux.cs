﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectPopupManagemanentBagneux : MonoBehaviour {

	public GameObject ImageBg;
	[SerializeField]
	private Text ListItemNameEcole;
	[SerializeField]
	private Text ListItemNbEleves;
	[SerializeField]
	private Text ListItemNbClasses;
	[SerializeField]
	private Text ListItemRepartitionParClasses;
	[SerializeField]
	private Text ListItemHistorique;
	[SerializeField]
	private Text ListItemAnalyseGraphique;
	[SerializeField]
	private Text ListItemAcces;
	[SerializeField]
	private Text ListItemContact;
	[SerializeField]
	private Text ListItemAdresse;

	private bool doOnce;


	private void Start () {
		
	}

	public void OpenHotspotPopupPanel(){

		GetComponent<ProjectPopupLerpBagneux> ().StartLerpOpen ();
	}

	public void CloseHotspotPopupPanel(){

		GetComponent<ProjectPopupLerpBagneux> ().StartLerpClose ();
	}

	public void SetPopupValues(string nameEcole, string nbEleves, string nbClasses, string repartitionParClasses, string historique, string analyseGraphique, string acces, string contact, string adresse, ProjectPopupDataBagneux projectPopupData, Toolz.Module From){
		if (!doOnce) {
			doOnce = true;

			if (From != null) {
				ImageBg.transform.Find ("Button_Exit").GetComponent<Toolz.Triggers.TransitionTrigger> ()._from = From;
			}

			ListItemNameEcole.text = nameEcole;
			ListItemNbEleves.text = "Nombre d’élèves: " + nbEleves;
			ListItemNbClasses.text = "Nombre de classes: " + nbClasses;
			ListItemRepartitionParClasses.text = "Répartition par classe: " + repartitionParClasses;
			ListItemHistorique.text = "Historique: " + historique;
			ListItemAnalyseGraphique.text = "Analyse graphique: " + analyseGraphique;
			ListItemAcces.text = "Accès: " + acces;
			ListItemContact.text = "Contact: " + contact;
			ListItemAdresse.text = "Adresse: " + adresse;

			Invoke ("Delayed", 0.2f);
		}
	}

	private void Delayed(){
		doOnce = false;
	}
}
