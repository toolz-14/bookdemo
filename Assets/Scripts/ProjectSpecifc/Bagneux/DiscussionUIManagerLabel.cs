﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;

public class DiscussionUIManagerLabel : MonoBehaviour {

	public GameObject NoCommentView;
	public GameObject CommentListView;
	public Transform ProjectInfoTitle;
	public Sprite[] SmallThemeTextures;
	public GameObject ThemeIcon;
	public ThemeUIManager ThemePanel;
	public GameObject CommentObject;
	public GameObject Panel_KeyboardInput;
	public GlobalErrorManager GlobalErrorManager;
	public ResponseUIManager ResponseManager;
	public ProfileUIManager ProfileManager;
	public ResponseInputUIManager ResponseInputUIManager;
	public Transform FooterButtons;
	[HideInInspector]
	public bool IsOpenFromProfile;
	[HideInInspector]
	public bool IsOpen;

	private GameObject _tempObject;
	private Vector2 _tempVect2;
	private float _offset = 3f;
	private const float _themeIconYPos = 1f;
	private List<GameObject> CommentItems;
    private GetComments _getCommentsService;
    private PostUserCommentVote _postUserCommentVoteService;
    private PostUserFollowProject _postUserFollowProjectService;
	private Toolz.WebServices.UserGetFollowedProjects _userGetFollowedProjects;

	AnimateHolder manager;

	private void Awake(){
		IsOpen = false;
		SetDiscussionWebService ();
    }

    private void SetDiscussionWebService(){
		GameObject webservice = GameObject.FindGameObjectWithTag("WebServices");
		
		_getCommentsService = webservice.GetComponent<GetComments>();
        _getCommentsService.onComplete += _getCommentsService_onComplete;
        _getCommentsService.onError += _getCommentsService_onError;
		
		_postUserCommentVoteService = webservice.GetComponent<PostUserCommentVote>();
		_postUserCommentVoteService.onError += _postUserCommentVote_onError;
		_postUserCommentVoteService.onComplete += _postUserCommentVote_onComplete;
		
		_postUserFollowProjectService = webservice.GetComponent<PostUserFollowProject>();
		_postUserFollowProjectService.onError += _postUserFollowProjectService_onError;
		_postUserFollowProjectService.onComplete += _postUserFollowProjectService_onComplete;
		
		_userGetFollowedProjects = webservice.GetComponent<Toolz.WebServices.UserGetFollowedProjects>();
		_userGetFollowedProjects.onError += _userGetFollowedProjects_onError;
		_userGetFollowedProjects.onComplete += _userGetFollowedProjects_onComplete;
		
		CommentItems = new List<GameObject>();
	}

	public void OpenDiscussionPanel(bool isFiche, LabelEditorObject label)
    {
        this.gameObject.SetActive(true);
		manager = (AnimateHolder) FindObjectOfType(typeof (AnimateHolder));
		manager.OnDiscussionLerpedBack += DeactivateDiscussionPanel;
        //Replace with correct id
        _getCommentsService.idProject = label.id; //ProjectUIManager.CurrentProjectData.idProject;
		_getCommentsService.UseWebService();

		IsOpen = true;
		
		if (GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser != -1) {
			// _userGetFollowedProjects.UseWebService ();
		}
		SetDiscussionPanel(isFiche, label);

		if (ProfileUIManager.IsOpen) {
			ProfileManager.CloseProfilePanel ();
		}
		//transform.parent.GetComponent<AnimateHolder> ().StartLerpToHalfCanvasDiscussion();
	}
	
	public void CloseDiscussionPanel(){
		IsOpen = false;
		PlayCloseAnim();
	}

	private void PlayCloseAnim(){
		//transform.parent.GetComponent<AnimateHolder> ().StartLerpBackToFullDiscussion ();
		
		if(IsOpenFromProfile){
			IsOpenFromProfile = false;
			ProfileManager.OpenProfilePanel();
		}
	}

	private void DeactivateDiscussionPanel(){
		manager.OnDiscussionLerpedBack -= DeactivateDiscussionPanel;
		gameObject.SetActive (false);
	}

	private void SetDiscussionPanel(bool isFiches, LabelEditorObject label)
    {
        ResponseInputUIManager.SetidLabel(label.id);
        if (!isFiches) {
            // SetFooter ();
            // SetHeader ();
            // SetColorLines();

            // TEMPORARY INFO FILL
            //set project name
            ProjectInfoTitle.Find("Caption").GetComponent<Text>().text = label.LabelText.text;

            //Configure ResponseInputUIManager in order to manage post comment
            Button buttonComment = transform.Find ("VLayout").Find ("Footer").Find ("Buttons").Find ("Button_Comment").GetComponent<Button> ();
			buttonComment.onClick.AddListener (() => 
			{
                int idUser = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>().idUser;
                if (idUser == -1)
                {
                    ShowglobalError();
                }
                else
                {
                    ResponseInputUIManager.gameObject.SetActive(true);
                    ResponseInputUIManager.PrepareToComment(label.id);
                    Panel_KeyboardInput.SetActive(true);
                }
			});


			Button buttonComment2 = NoCommentView.transform.Find ("AddCommentBt").GetComponent<Button> ();
			buttonComment2.onClick.AddListener (() =>
				{ 
					ResponseInputUIManager.gameObject.SetActive(true);
					ResponseInputUIManager.PrepareToComment (label.id);
				});
			 
		} else {
			SetFooter ();
			SetHeader_Fiche ();
			SetColorLines_Fiche ();

			//Configure ResponseInputUIManager in order to manage post comment
			Button buttonComment = transform.Find ("VLayout").Find ("Footer").Find ("Buttons").Find ("Button_Comment").GetComponent<Button> ();
			buttonComment.onClick.AddListener (() => 
			{ 
				ResponseInputUIManager.gameObject.SetActive(true);
				ResponseInputUIManager.PrepareToComment (label.id);
			});

			Button buttonComment2 = NoCommentView.transform.Find ("AddCommentBt").GetComponent<Button> ();
			buttonComment2.onClick.AddListener (() => 
			{ 
				ResponseInputUIManager.gameObject.SetActive(true);
				ResponseInputUIManager.PrepareToComment (label.id);
			});

		}
	}

	private void SetHeader(){
		float previousX = 0;

		for(int i=0; i < ProjectUIManager.CurrentProjectData.activeThemes.Count; i++){
			_tempObject = (GameObject)Instantiate(ThemeIcon);
			_tempObject.SetActive(true);

			//set color
			_tempObject.GetComponent<Image>().color = GetThemeColor(ProjectUIManager.CurrentProjectData.activeThemes[i]);

			//set icon
			_tempObject.GetComponent<Image>().sprite = GetThemeSprite(ProjectUIManager.CurrentProjectData.activeThemes[i]);

			//set parent
			_tempObject.transform.SetParent(ProjectInfoTitle);
			_tempObject.transform.SetAsLastSibling();

			//set position
			_tempVect2.y = _themeIconYPos;
			_tempVect2.x = previousX;
			_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;

			previousX += _tempObject.GetComponent<RectTransform>().rect.width + _offset;

		}

		//set project name
		ProjectInfoTitle.Find ("Caption").GetComponent<Text> ().text = ProjectUIManager.CurrentProjectData.projectName;
		_tempVect2.y = ProjectInfoTitle.Find ("Caption").GetComponent<RectTransform>().anchoredPosition.y;
		_tempVect2.x = previousX + _offset;
		ProjectInfoTitle.Find ("Caption").GetComponent<RectTransform>().anchoredPosition = _tempVect2;
	}

	private void SetHeader_Fiche(){
		float previousX = 0;

		for(int i=0; i < ProjectUIManager_Fiches.CurrentProjectData.activeThemes.Count; i++){
			_tempObject = (GameObject)Instantiate(ThemeIcon);
			_tempObject.SetActive(true);

			//set color
			_tempObject.GetComponent<Image>().color = GetThemeColor(ProjectUIManager_Fiches.CurrentProjectData.activeThemes[i]);

			//set icon
			_tempObject.GetComponent<Image>().sprite = GetThemeSprite(ProjectUIManager_Fiches.CurrentProjectData.activeThemes[i]);

			//set parent
			_tempObject.transform.SetParent(ProjectInfoTitle);
			_tempObject.transform.SetAsLastSibling();

			//set position
			_tempVect2.y = _themeIconYPos;
			_tempVect2.x = previousX;
			_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;

			previousX += _tempObject.GetComponent<RectTransform>().rect.width + _offset;

		}

		//set project name
		ProjectInfoTitle.Find ("Caption").GetComponent<Text> ().text = ProjectUIManager_Fiches.CurrentProjectData.projectName;
		_tempVect2.y = ProjectInfoTitle.Find ("Caption").GetComponent<RectTransform>().anchoredPosition.y;
		_tempVect2.x = previousX + _offset;
		ProjectInfoTitle.Find ("Caption").GetComponent<RectTransform>().anchoredPosition = _tempVect2;
	}

	private void SetFooter(){
		//if not logged in
		FooterButtons.Find ("Button_Follow").gameObject.SetActive(true);
		FooterButtons.Find ("Button_UnFollow").gameObject.SetActive(false);
	}

	private void SetColorLines(){
		if (ProjectUIManager.CurrentProjectData.activeThemes.Count > 0) {
			DeactivateallColorLines();
			Image line;
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 10f;
			for (int i=0; i < ProjectUIManager.CurrentProjectData.activeThemes.Count; i++) {
				line = transform.Find ("VLayout").Find ("ColorLines").Find ("Image_"+(i+1).ToString()).GetComponent<Image>();
				line.color = GetThemeColor(ProjectUIManager.CurrentProjectData.activeThemes[i]);
				line.gameObject.SetActive(true);
			}
		} else {
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 0f;
		}
	}

	private void SetColorLines_Fiche(){
		if (ProjectUIManager_Fiches.CurrentProjectData.activeThemes.Count > 0) {
			DeactivateallColorLines();
			Image line;
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 10f;
			for (int i=0; i < ProjectUIManager_Fiches.CurrentProjectData.activeThemes.Count; i++) {
				line = transform.Find ("VLayout").Find ("ColorLines").Find ("Image_"+(i+1).ToString()).GetComponent<Image>();
				line.color = GetThemeColor(ProjectUIManager_Fiches.CurrentProjectData.activeThemes[i]);
				line.gameObject.SetActive(true);
			}
		} else {
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 0f;
		}
	}

	private void DeactivateallColorLines(){
		Transform line;
		for(int i=0; i<9; i++){
			line = transform.Find ("VLayout").Find ("ColorLines").Find ("Image_"+(i+1).ToString());
			line.gameObject.SetActive(false);
		}
	}

	private Sprite GetThemeSprite(string themeName){
		foreach(Sprite sp in SmallThemeTextures){
			if(sp.name.Contains(themeName)){
				return sp;
			}
		}
		return null;
	}

	private Color GetThemeColor(string themeName){
		foreach(Theme th in ThemePanel.availableThemes){
			if(th.themeName.Contains(themeName.ToLower())){
				return th.themeColor;
			}
		}
		return Color.white;
	}
	
	private void SetCommentListView(){
        //Clean memory of last graphic object
        foreach (GameObject go in CommentItems) {
            Destroy(go);
        }
        CommentItems.Clear();

        //int idUser = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>().idUser;
        //ResponseInputUIManager responseInputUIManager = GameObject.FindGameObjectWithTag("ResponseInput").GetComponent<ResponseInputUIManager>();
		List<Comment> comments = _getCommentsService.Comments;


        foreach (Comment comment in comments) {
            AddCommentToList(comment);
		}
	}

    public void AddCommentToList(Comment comment) {
        int i = CommentItems.Count;
        int idUser = GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>().idUser;

        _tempObject = (GameObject)Instantiate(CommentObject);
        _tempObject.AddComponent<CommentId>();
        _tempObject.GetComponent<CommentId>().idComment = comment.idComment;
        int idComment = (int) comment.idComment;
		_tempObject.SetActive(true);

		//seprarator disabled in first comment
		if(i == 0){
			_tempObject.transform.Find("Separator").gameObject.SetActive(false);
		}

		//set user name 
        _tempObject.transform.Find("UserName").GetComponent<Text>().text = comment.firstname + " " + comment.name;

		//set comment text 
        _tempObject.transform.Find("Text").GetComponent<Text>().text = comment.text;

        //set date (published time)
        string date = comment.publishedTime.ToString("dd/MM/yyyy HH:mm");
        _tempObject.transform.Find("Date").GetComponent<Text>().text = date;

		Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>(); // Get user
		//if not admin hide validation buttons
		if (idUser == -1 || (idUser != -1 && (Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus != Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN)) { //  User is admin
			_tempObject.transform.Find ("ValidationHolder").gameObject.SetActive (false);
		} else {
            if(comment.status == 1) // comment has already been validated
                _tempObject.transform.Find("ValidationHolder").gameObject.SetActive(false);
            else
            {
                Button deleteButton = _tempObject.transform.Find("ValidationHolder").Find("Button_DeleteItem").GetComponent<Button>();
                Button validateButton = _tempObject.transform.Find("ValidationHolder").Find("Button_ValidateItem").GetComponent<Button>();

                deleteButton.onClick.AddListener(() => OnClickDeleteCommentButton(deleteButton, validateButton, comment.idAuthor, comment.text, true));
                validateButton.onClick.AddListener(() => OnClickValidateCommentButton(deleteButton, validateButton, comment.idAuthor, comment.text, false));
            }
		}

        //set vote count (total votes)
        _tempObject.transform.Find ("ProjectVote").Find ("Count").GetComponent<Text>().text = (comment.nbUp - comment.nbDown).ToString();

        //set vote count (total votes)
        _tempObject.transform.Find("ProjectVote").Find("Count").GetComponent<Text>().text = (comment.nbUp - comment.nbDown).ToString();

        //set vote
        Transform ButtonFor = _tempObject.transform.Find("ProjectVote").Find("ThumbUp");
        Transform ButtonAgainst = _tempObject.transform.Find("ProjectVote").Find("ThumbDown");

        if (idUser != -1) {
            //if the comment was made by the user then both greyed out
            if (comment.idAuthor == idUser) {
                ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                ButtonFor.GetComponent<Button>().interactable = false;
                ButtonAgainst.GetComponent<Button>().interactable = false;
            }
            else {
                //if the logged user has already voted then the button is greyed out   
                if (comment.userVote != 0) {
                    //if voted yes
                    if (comment.userVote == 1) {
                        ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                    }
                    //if voted no
                    else if (comment.userVote == -1) {
                        ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                    }
                    ButtonFor.GetComponent<Button>().interactable = false;
                    ButtonAgainst.GetComponent<Button>().interactable = false;
                }
                else {
                    ButtonFor.GetComponent<Button>().onClick.AddListener(() => DoVote(true, idComment));
                    ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => DoVote(false, idComment));
                }
            }
        }
        //if there are responses
        if (comment.nbValidatedResponses > 0) {
			//Debug.Log ("Comment nbresponses > 0");
            //set response button text 
            _tempObject.transform.Find("ResponseBt").Find("Text").GetComponent<Text>().text = comment.nbValidatedResponses + " réponses";

            //set button interractable
			_tempObject.transform.Find("ResponseBt").GetComponent<Button>().interactable = true;

            //set response button onclick
            _tempObject.transform.Find("ResponseBt").GetComponent<Button>().onClick.AddListener(() => { ResponseManager.gameObject.SetActive (true); ResponseManager.OpenResponsePanel((uint)idComment);});
			_tempObject.transform.Find("ResponseBt").GetComponent<Button>().onClick.AddListener(() => { MoveDiscussionPanelLeft();});
        }
        else {
            //set response button text 
            _tempObject.transform.Find("ResponseBt").Find("Text").GetComponent<Text>().text = "Répondre";
            if (idUser != -1) {
                //set response button onclick
				_tempObject.transform.Find("ResponseBt").GetComponent<Button>().onClick.AddListener(delegate {ResponseInputUIManager.gameObject.SetActive(true); ResponseInputUIManager.SetLabelText("reponse a " + comment.firstname + " " + comment.name); });
				_tempObject.transform.Find("ResponseBt").GetComponent<Button>().onClick.AddListener(() => { Panel_KeyboardInput.SetActive(true);ResponseInputUIManager.gameObject.SetActive(true); ResponseInputUIManager.PrepareToReply(idComment, 0); });
            }
            else {
                _tempObject.transform.Find("ResponseBt").GetComponent<Button>().onClick.AddListener(() => ShowglobalError());
            }
        }
        //set parent
        _tempObject.transform.SetParent(CommentObject.transform.parent);
        _tempObject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
        //set position
        _tempVect2.x = 0;
        _tempVect2.y = i * (-230f);
        _tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
		_tempObject.GetComponent<RectTransform>().offsetMax = new Vector2(-1f, _tempObject.GetComponent<RectTransform>().offsetMax.y);
		_tempObject.GetComponent<RectTransform>().offsetMin = new Vector2(-1f, _tempObject.GetComponent<RectTransform>().offsetMin.y);

        //modify container height depending on amount of comments (so it can scroll properly)
        CommentObject.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(CommentObject.transform.parent.GetComponent<RectTransform>().sizeDelta.x, (i+1) * 230f);

		CommentItems.Add(_tempObject); 
	}

    public void OnClickDeleteCommentButton(Button delete, Button validate, int idAuthor, string msg, bool isDeleted){

		delete.interactable = false;
		validate.interactable = false;
        ResponseInputUIManager.UpdateComment(idAuthor, msg, isDeleted);
    }

	public void OnClickValidateCommentButton(Button delete, Button validate,int idAuthor, string msg, bool isDeleted)
    {
        Debug.Log("id Author " + idAuthor);

        delete.interactable = false;
		validate.interactable = false;
        ResponseInputUIManager.UpdateComment(idAuthor,msg, isDeleted);
    }

    public void UpdateComment(int idComment, int newNbReplies) {
        GameObject commentObject = CommentItems.Find((item) => item.GetComponent<CommentId>().idComment == idComment);

        //set response button text 
        commentObject.transform.Find("ResponseBt").Find("Text").GetComponent<Text>().text = newNbReplies + " réponses";

        //If button was not interactable before, make it interactable and set its listeners
        Button commentButton = commentObject.GetComponent<Button>();
        if (commentButton.interactable == false) {
            //set button interractable
            commentButton.interactable = true;

            //set response button onclick
            commentButton.onClick.AddListener(() => 
            { 
				ResponseManager.gameObject.SetActive (true);
				ResponseManager.OpenResponsePanel((uint)idComment);
                MoveDiscussionPanelLeft();
            });
        }
    }

	private void DoVote(bool isFor, int idComment){
        int userVote = isFor ? 1 : -1;

        _postUserCommentVoteService.idComment = (uint)idComment;
        _postUserCommentVoteService.userVote = userVote;

        _postUserCommentVoteService.UseWebService();
	}

	public void ShowglobalError(){
		GlobalErrorManager.gameObject.SetActive (true);
		GlobalErrorManager.OpenGlobalErrorPanel();
	}

	public void FollowTheDiscussion(){
		if (GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser != -1) {
            _postUserFollowProjectService.idProject = ProjectUIManager.CurrentProjectData.idLabel;
            _postUserFollowProjectService.wantsToFollow = true;
            _postUserFollowProjectService.UseWebService();
			
		} else {
			//show global error 
			GlobalErrorManager.gameObject.SetActive (true);
			GlobalErrorManager.OpenGlobalErrorPanel();
		}
	}

    public void UnfollowTheDiscussion() {
        if (GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>().idUser != -1) {
            _postUserFollowProjectService.idProject = ProjectUIManager.CurrentProjectData.idLabel;
            _postUserFollowProjectService.wantsToFollow = false;
            _postUserFollowProjectService.UseWebService();
        }
        else {
			GlobalErrorManager.gameObject.SetActive (true);
			GlobalErrorManager.OpenGlobalErrorPanel();
        }
    }

	public void hideAllBackgrounds(){
		for(int i=0; i<CommentItems.Count; i++){
			CommentItems[i].transform.Find("Bg").GetComponent<CanvasGroup>().alpha = 0f;
		}
	}

	public void MoveDiscussionPanelLeft(){
		//transform.parent.GetComponent<AnimateHolder> ().StartLerpToFullCanvasDiscussion ();
	}
	
	public void MoveDiscussionPanelRight(){
		//transform.parent.GetComponent<AnimateHolder> ().StartLerpBackToHAlfDiscussion ();
	}

    // WEB SERVICES CALLBACKS
    private void _postUserCommentVote_onComplete(long status, string message) {
        GameObject currentComment = null;
        foreach (GameObject commentObject in CommentItems) {
            Debug.Log("Registered id: " + _postUserCommentVoteService.idComment);
            Debug.Log("Current object id: " + commentObject.GetComponent<CommentId>().idComment);
            if (_postUserCommentVoteService.idComment == commentObject.GetComponent<CommentId>().idComment) {
                currentComment = commentObject;
                break;
            }
        }
        if (currentComment == null) {
            Debug.LogError("Current Comment is null");
            return;
        }
        Debug.Log("User vote ok");
        //set vote
        Transform ButtonFor = currentComment.transform.Find("ProjectVote").Find("ThumbUp");
        Transform ButtonAgainst = currentComment.transform.Find("ProjectVote").Find("ThumbDown");

        //set vote count (total votes)
        int totalVote = _postUserCommentVoteService.nbUp - _postUserCommentVoteService.nbDown;
        currentComment.transform.Find("ProjectVote").Find("Count").GetComponent<Text>().text = totalVote.ToString();
        //if the logged user has already voted then the button is geyed out   
        
        //if voted yes
        if (_postUserCommentVoteService.userVote == 1) {
            ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
        }
        //if voted no
        else if (_postUserCommentVoteService.userVote == -1) {
            ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
        }
        ButtonFor.GetComponent<Button>().interactable = false;
        ButtonAgainst.GetComponent<Button>().interactable = false;
    }

    private void _postUserCommentVote_onError(long status, string message) {
        Debug.Log("User vote error");
    }

    private void _getCommentsService_onError(long status, string message) {
        Debug.Log("Error while loading discussion: " + message);
    }

    private void _getCommentsService_onComplete(long status, string message) {
        if (_getCommentsService.Comments.Count == 0)
        {
            // Debug.Log("No discussion: ");
            NoCommentView.SetActive(true);
            CommentListView.SetActive(false);
        }
        else
        {
            // Debug.Log("Discussion: ");
            NoCommentView.SetActive(false);
            CommentListView.SetActive(true);
            SetCommentListView();
        }
    }

    private void _postUserFollowProjectService_onComplete(long status, string message) {
        Debug.Log("PostUserFollowProject OK");
    }

    private void _postUserFollowProjectService_onError(long status, string message) {
        Debug.Log("PostUserFollowProject error");
    }

	private void _userGetFollowedProjects_onComplete(long status, string message) {

		//check if following this discussion
		foreach (string project in _userGetFollowedProjects.FollowedProjects) {
			if(project.Contains(ProjectUIManager.CurrentProjectData.idLabel) || project.Contains(ProjectUIManager_Fiches.CurrentProjectData.idProject) ){
				FooterButtons.Find ("Button_Follow").gameObject.SetActive(false);
				FooterButtons.Find ("Button_UnFollow").gameObject.SetActive(true);
				return;
			}
		}
	}
	
	private void _userGetFollowedProjects_onError(long status, string message) {
		Debug.Log("Unable to retrieve followed project: " + status + "=>" + message);
	}
}
