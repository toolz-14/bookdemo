﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelZacData : MonoBehaviour {
    public GameObject PDFViewer;
    public Paroxe.PdfRenderer.PDFViewer pdfViewer;
    public GameObject Panel;
    public GameObject canvasUIMain;

    public static LabelZacData instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
}
