﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainCamera : MonoBehaviour {

	private Quaternion savedRotation;

	private void Start(){
		savedRotation = GetComponent<Transform> ().rotation;
	}

	private void LateUpdate () {
		GetComponent<Transform> ().rotation = savedRotation;
	}
}
