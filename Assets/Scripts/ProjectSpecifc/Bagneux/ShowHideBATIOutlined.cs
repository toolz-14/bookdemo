﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideBATIOutlined : MonoBehaviour {

	[SerializeField]
	private GameObject BATIOutlined;


    public void ShowBATIOutlined(){
        BATIOutlined.SetActive(true);
    }

	public void HideBATIOutlined(){
        BATIOutlined.SetActive(false);
    }
}
