﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelAtelier : MonoBehaviour
{
    
    [SerializeField]
    private Text leftText;
    [SerializeField]
    private Text dates;
    [SerializeField]
    private List<string> topUILeftText = new List<string>();
    [SerializeField]
    private List<string> topUIDates = new List<string>();
    [SerializeField]
    private LabelEditorSaveLoad labelEditorSaveLoad;
    [SerializeField]
    private LabelEditor labelEditor;
    [SerializeField]
    private PanelPresentationAtelier panelPresentationAtelier;
    [SerializeField]
    private GameObject opUsine;

    [SerializeField]
    private List<GameObject> buttons = new List<GameObject>();
    //[SerializeField]
    //private TimlineLerpBagneux timlineLerpBagneux;

    //Phase 1
    [SerializeField]
    private List<GameObject> Phase1 = new List<GameObject>();
    //Phase 2
    [SerializeField]
    private List<GameObject> Phase2 = new List<GameObject>();
    //Phase 3
    [SerializeField]
    private List<GameObject> Phase3 = new List<GameObject>();
    //Phase 4
    [SerializeField]
    private List<GameObject> Phase4 = new List<GameObject>();
    //Phase 5
    [SerializeField]
    private List<GameObject> Phase5 = new List<GameObject>();
    //Phase 6
    [SerializeField]
    private List<GameObject> Phase6 = new List<GameObject>();
    //Phase 7
    [SerializeField]
    private List<GameObject> Phase7 = new List<GameObject>();

    [HideInInspector]
    public int currentSelectedButton;
    [HideInInspector]
    public bool blockModeSwap;

    public static bool isTex;

    [SerializeField]
    private MeshRenderer orthoPhotoSecteur4;
    [SerializeField]
    private GameObject solZAC4;
    [SerializeField]
    private ButtonPhase5Plan3D buttonPhase5Plan3D;

    private void Start()
    {
        isTex = true;
        blockModeSwap = false;
        labelEditor.AssignPhaseButton(0);

        OnButtonClick(0);
    }

    private void OnEnable()
    {
        isTex = true;
        blockModeSwap = false;
        labelEditor.AssignPhaseButton(0);

        OnButtonClick(currentSelectedButton);
    }

    public void CloseAtelierPanel()
    {
        SetTextured();
        buttonPhase5Plan3D.ActivateMainButton(false);
    }

    public void OnButtonClick(int index) {
        currentSelectedButton = index;

        //show relevent chat labels
        labelEditorSaveLoad.DisplayLabelByPhase();
        labelEditor.AssignPhaseButton(index);

        //set top ui right sub text
        leftText.text = topUILeftText[index];
        //set top ui date
        dates.text = topUIDates[index];

        SetColor(index);
        SetDisplayedObjects(index);
    }

    public void SetDisplayedObjects(int index)
    {
        if (!blockModeSwap && !ButtonMaquette.wireframeIsOn)
        {
            switch (index)
            {
                case 0:
                    SetPhase1Objects(true);
                    SetPhase2Objects(false);
                    SetPhase3Objects(false);
                    SetPhase4Objects(false);
                    SetPhase5Objects(false);
                    SetPhase6Objects(false);
                    SetPhase7Objects(false);

                    buttonPhase5Plan3D.ActivateMainButton(false);
                    solZAC4.SetActive(false);
                    opUsine.SetActive(false);
                    SetTextured();
                    break;
                case 1:
                    SetPhase1Objects(false);
                    SetPhase2Objects(true);
					SetPhase3Objects(false);
					SetPhase4Objects(false);
					SetPhase5Objects(false);
					SetPhase6Objects(false);
					SetPhase7Objects(false);

                    buttonPhase5Plan3D.ActivateMainButton(false);
                    solZAC4.SetActive(true);
                    opUsine.SetActive(true);
                    SetTextured();
                    break;
                case 2:
                    SetPhase1Objects(false);
                    SetPhase2Objects(false);
                    SetPhase3Objects(true);
					SetPhase4Objects(false);

                    buttonPhase5Plan3D.ActivateMainButton(false);
                    solZAC4.SetActive(true);
                    opUsine.SetActive(true);
                    SetTextured();
                    break;
                case 3:
                    SetPhase1Objects(false);
                    SetPhase2Objects(false);
					SetPhase3Objects(false);
                    SetPhase4Objects(true);
					SetPhase5Objects(false);
					SetPhase6Objects(false);
					SetPhase7Objects(false);

                    buttonPhase5Plan3D.ActivateMainButton(false);
                    solZAC4.SetActive(false);
                    opUsine.SetActive(true);
                    SetPhase4Maquette();
                    break;
                case 4:
                    SetPhase1Objects(false);
                    SetPhase2Objects(false);
					SetPhase3Objects(false);
                    SetPhase5Objects(true);

                    buttonPhase5Plan3D.ActivateMainButton(true);
                    solZAC4.SetActive(true);
                    opUsine.SetActive(true);
                    SetPhase4Maquette();
                    break;
                case 5:
                    SetPhase1Objects(false);
                    SetPhase2Objects(false);
                    SetPhase6Objects(true);

                    buttonPhase5Plan3D.ActivateMainButton(false);
                    solZAC4.SetActive(true);
                    opUsine.SetActive(true);
                    SetTextured();
                    break;
                case 6:
                    SetPhase1Objects(false);
                    SetPhase2Objects(false);
                    SetPhase7Objects(true);

                    buttonPhase5Plan3D.ActivateMainButton(false);
                    solZAC4.SetActive(true);
                    opUsine.SetActive(true);
                    SetTextured();
                    break;
                default:
                    ResetPhaseObject();
                    break;
            }
        }
        panelPresentationAtelier.SetPresentationAtelier(index);
    }

    public void ResetPhaseObject()
    {
        SetPhase1Objects(false);
        SetPhase2Objects(false);
        SetPhase3Objects(false);
        SetPhase4Objects(false);
        SetPhase5Objects(false);
        SetPhase6Objects(false);
        SetPhase7Objects(false);
        solZAC4.SetActive(false);
    }

    private void SetPhase1Objects(bool state)
    {
        if (state)
        {
            if (isTex)
            {
                Phase1[0].SetActive(!state);
                Phase1[1].SetActive(state);
            }
            else
            {
                Phase1[0].SetActive(state);
                Phase1[1].SetActive(!state);
            }
        }
        else
        {
            Phase1[1].SetActive(state);
            Phase1[0].SetActive(state);
        }
    }

    private void SetPhase2Objects(bool state)
    {
        foreach (GameObject go in Phase2)
        {
			if(go != null)
            	go.SetActive(state);
        }
    }

    private void SetPhase3Objects(bool state)
    {
        foreach (GameObject go in Phase3)
        {
			if(go != null)
				go.SetActive(state);
        }
    }

    private void SetPhase4Objects(bool state)
    {
        foreach (GameObject go in Phase4)
        {
			if(go != null)
				go.SetActive(state);
        }
    }

    private void SetPhase5Objects(bool state)
    {
        foreach (GameObject go in Phase5)
        {
			if(go != null)
				go.SetActive(state);
        }
    }

    private void SetPhase6Objects(bool state)
    {
        foreach (GameObject go in Phase6)
        {
			if(go != null)
				go.SetActive(state);
        }
    }

    private void SetPhase7Objects(bool state)
    {
        foreach (GameObject go in Phase7)
        {
			if(go != null)
				go.SetActive(state);
        }
    }

    private void SetColor(int index)
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            if (i == index)
            {
                buttons[i].SetActive(true);
            }
            else
            {
                buttons[i].SetActive(false);
            }
        }
    }

    public void SetPhase4Maquette()
    {
        orthoPhotoSecteur4.material.SetFloat("_Cutoff", 0.2f);
    }

    public void SetTextured()
    {
        orthoPhotoSecteur4.material.SetFloat("_Cutoff", 0f);
    }
}
