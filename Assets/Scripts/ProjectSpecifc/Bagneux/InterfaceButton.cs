﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InterfaceButton : MonoBehaviour {

	public bool startsEnabled;
	public List<GameObject> HotspotList = new List<GameObject>();
	public Image InterfaceIcon;
	public Image InterfaceBg;
	public Text InterfaceText;
	private bool _interfaceActif;
	private Color Black303034 = new Color(0.1176f, 0.1176f, 0.1333f);

	private void Start () {
		if (startsEnabled) {
			_interfaceActif = true;
			InterfaceIcon.color = Black303034;
			InterfaceText.color = Black303034;
			InterfaceBg.color = Color.white;
			for(int i=0; i<HotspotList.Count; i++){
				HotspotList[i].SetActive(true);
			}
		} else {
			_interfaceActif = false;
			InterfaceIcon.color = Color.white;
			InterfaceText.color = Color.white;
			InterfaceBg.color = Black303034;
			for(int i=0; i<HotspotList.Count; i++){
				HotspotList[i].SetActive(false);
			}
		}
	}

	public void SetHotspotActive(){
		_interfaceActif = !_interfaceActif;
		if (_interfaceActif) {
			InterfaceIcon.color = Black303034;
			InterfaceText.color = Black303034;
			InterfaceBg.color = Color.white;


			for(int i=0; i<HotspotList.Count; i++){
				HotspotList[i].SetActive(true);
			}
		} else {
			InterfaceIcon.color = Color.white;
			InterfaceText.color = Color.white;
			InterfaceBg.color = Black303034;

			for(int i=0; i<HotspotList.Count; i++){
				HotspotList[i].SetActive(false);
			}
		}
	}

	public void Reset(){
		InterfaceIcon.color = Color.white;
		InterfaceText.color = Color.white;
		InterfaceBg.color = Black303034;

		for(int i=0; i<HotspotList.Count; i++){
			//HotspotList[i].SetActive(false);
		}
		_interfaceActif = false;
	}
}
