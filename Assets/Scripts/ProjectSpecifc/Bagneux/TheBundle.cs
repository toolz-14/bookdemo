﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheBundle : MonoBehaviour
{
    // Instance
    private static TheBundle Instance;
    public static TheBundle instance { get { return Instance; } }

    [HideInInspector]
    public AssetBundle loadedBundle;
    [HideInInspector]
    public bool bundleIsLoading;


    private void Awake()
    {
        loadedBundle = null;
        bundleIsLoading = false;

        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
            Destroy(gameObject);

    }
}
