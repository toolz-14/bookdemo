﻿using UnityEngine.UI;
using UnityEngine;

public class ProjectPopupTransportToggleBagneux : MonoBehaviour {

	[HideInInspector]
	public GameObject[] ObjectsToBeActivated;

	private void Start () {
		
	}

	public void ToggleClick(){
		foreach (GameObject go in ObjectsToBeActivated) {
			if(go != null && GetComponent<Toggle>() != null){
				go.SetActive(GetComponent<Toggle>().isOn);
			}
		}
	}
}
