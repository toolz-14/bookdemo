﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonModeTexture : MonoBehaviour {

	[SerializeField]
	private List<GameObject> objects3DTex = new List<GameObject>();
	[SerializeField]
	private List<GameObject> object3DNonTex = new List<GameObject>();

    [SerializeField]
    private GameObject wireframe;

    [SerializeField]
    private GameObject secteur4Tex;
    [SerializeField]
    private GameObject secteur4NonTex;

    public static bool isTex;
	private bool doOnce;
    private bool shouldActivatesecteur4 = false;

    private void Start () {
		isTex = true;
        transform.Find("Text").GetComponent<Text>().text = "Textured";
    }

    public void Swap(){

        //Debug.Log("Swap ModeTexture");
		if (!doOnce) {
			doOnce = true;
			isTex = !isTex;
			if (isTex) {
				Objects3DTex ();
			} else {
				Objects3DNonTex ();
			}
		}
	}

    public void ForceTex()
    {
        transform.Find("Text").GetComponent<Text>().text = "Textured";

        //if wireframe actif don't swap secteur 4
        shouldActivatesecteur4 = false;
        if (!wireframe.activeInHierarchy && (secteur4Tex.activeInHierarchy || secteur4NonTex.activeInHierarchy))
        {
            shouldActivatesecteur4 = true;
        }

        foreach (GameObject go in objects3DTex)
        {

            go.SetActive(true);


            if (!shouldActivatesecteur4 && go.name.Contains("Secteur_4"))
            {
                go.SetActive(false);
            }
        }
        foreach (GameObject go in object3DNonTex)
        {
            go.SetActive(false);
        }
    }

    public void ForceNonTex()
    {
        transform.Find("Text").GetComponent<Text>().text = "White";
        //if wireframe actif don't swap secteur 4
        shouldActivatesecteur4 = false;
        if (!wireframe.activeInHierarchy && (secteur4Tex.activeInHierarchy || secteur4NonTex.activeInHierarchy))
        {
            shouldActivatesecteur4 = true;
        }
        foreach (GameObject go in objects3DTex)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in object3DNonTex)
        {

            go.SetActive(true);

            if (!shouldActivatesecteur4 && go.name.Contains("Secteur_4"))
            {
                go.SetActive(false);
            }
        }
    }

    private void Objects3DTex(){
        transform.Find("Text").GetComponent<Text>().text = "Textured";

        //if wireframe actif don't swap secteur 4
        shouldActivatesecteur4 = false;
        if (!wireframe.activeInHierarchy && (secteur4Tex.activeInHierarchy || secteur4NonTex.activeInHierarchy))
        {
            shouldActivatesecteur4 = true;
        }

        foreach (GameObject go in objects3DTex)
        {
            go.SetActive(true);

            if (!shouldActivatesecteur4 && go.name.Contains("Secteur_4"))
            {
                //Debug.Log("Objects3DTex, shouldActivatesecteur4 is false deactivating go = " + go.name);
                go.SetActive(false);
            }
        }
        foreach (GameObject go in object3DNonTex)
        {
            go.SetActive(false);
        }

        Invoke("Delayed1", 0.2f);
    }

	private void Delayed1(){
		doOnce = false;
	}

	private void Objects3DNonTex(){

        transform.Find("Text").GetComponent<Text>().text = "White";

        //if wireframe actif don't swap secteur 4
        shouldActivatesecteur4 = false;
        if (!wireframe.activeInHierarchy && (secteur4Tex.activeInHierarchy || secteur4NonTex.activeInHierarchy))
        {
            shouldActivatesecteur4 = true;
        }
        foreach (GameObject go in objects3DTex)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in object3DNonTex)
        {
            go.SetActive(true);

            if (!shouldActivatesecteur4 && go.name.Contains("Secteur_4"))
            {
                go.SetActive(false);
            }
        }

        Invoke("Delayed2", 0.2f);
    }

	private void Delayed2(){
		doOnce = false;
	}
}
