﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderReplyResize : MonoBehaviour {

    private float offset = 20f;

    void Start()
    {

    }

    public void ResizeHolder()
    {
        Vector2 tempSize = transform.parent.GetComponent<RectTransform>().sizeDelta;
        tempSize.x -= offset;
        GetComponent<RectTransform>().sizeDelta = tempSize;

        Vector3 pos = GetComponent<RectTransform>().localPosition;
        pos.x -= offset / 2f;
        GetComponent<RectTransform>().localPosition = pos;

        float temp = GetComponent<RectTransform>().sizeDelta.x / 2f;
        transform.Find("UserName").GetComponent<RectTransform>().sizeDelta = new Vector2(temp, GetComponent<RectTransform>().sizeDelta.y);
        transform.Find("Date").GetComponent<RectTransform>().sizeDelta = new Vector2(temp, GetComponent<RectTransform>().sizeDelta.y);

        //reposition Date
        Vector3 temp2 = transform.Find("Date").GetComponent<RectTransform>().anchoredPosition3D;
        temp2.x = temp + 2f;
        transform.Find("Date").GetComponent<RectTransform>().anchoredPosition3D = temp2;
    }
}
