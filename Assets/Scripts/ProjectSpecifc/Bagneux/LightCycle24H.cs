﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Hessburg;


public class LightCycle24H : MonoBehaviour
{
    //class instance
    public static LightCycle24H instance;

    public enum StartDate { MARS21, JUIN21, SEPT21, DEC21 }
    public StartDate chosenStartDate = StartDate.MARS21;
    [Range(0,24)]
    public float startTime = 12;
    public int startSpeed = 0;
    [SerializeField]
    private SunLight SunLight;
    [SerializeField]
    private GameObject holder;
    [SerializeField]
    private Text speedText;
    [SerializeField]
    private TimelineSliderBagneux timeLine;
    [SerializeField]
    private Slider theSlider;
    public Color colorOn;
    public Color colorOff;
    [HideInInspector]
    public bool swap = false;

    public static bool isOpen;
    private bool startUpdate;
    private int speed = 0;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        isOpen = false;
        theSlider.value = startTime;
        startUpdate = true;

        if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Mars") != null)
        {
            transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOn;
        }

        if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Juin") != null)
        {
            transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOn;
        }

        if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Septembre") != null)
        {
            transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOn;
        }

        if (transform.Find("Holder") != null && transform.Find("Holder").Find("Button_21_Decembre") != null)
        {
            transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOn;
        }

        OnButtonclick((int)chosenStartDate);
        SetSpeed(startSpeed);
        if (swap) OpenPanel();
    }

		
	private void OnEnable()
    {
        startUpdate = true;
        //SaveLoadSceneManager.savestartSliderValue += SavestartSliderValue;
        //SaveLoadSceneManager.savechosenStartDate += SavesChoseStarttDate;
        //SaveLoadSceneManager.savelightCyclePanelState += SaveslightCyclePanelState;
    }

    public float SavestartSliderValue()
    {
        return theSlider.value;
    }

    public int SavesChoseStarttDate()
    {
        return (int)chosenStartDate;
    }

    public bool SaveslightCyclePanelState()
    {
        return swap;
    }

    private void SetSpeed(int speed)
    {
        if (speed == 0)
        {
            SunLight.timeProgressFactor = 0.0f;
            SunLight.progressTime = false;
            speedText.text = "x0";
        }
        else if (speed == 1)
        {
            SunLight.timeProgressFactor = 1000.0f;
            SunLight.progressTime = true;
            speedText.text = "x1";
        }
        else
        {
            SunLight.timeProgressFactor = 15000.0f;
            SunLight.progressTime = true;
            speedText.text = "x2";
        }
    }

    public void SwapLightCycle()
    {
        swap = !swap;
        if (swap)
        {
            OpenPanel();
        }
        else
        {
            ClosePanel();
        }
    }

    public void LightCycleForceClose()
    {
        if (swap)
        {
            ClosePanel();
            swap = false;
        }
    }

    public void OpenPanel()
    {
        holder.SetActive(true);
        isOpen = true;
        if (TimlineLerpBagneux.isOpen)
        {
            timeLine.ForcecloseAndReset();
        }
    }

    public void ClosePanel()
    {
        holder.SetActive(false);
        isOpen = false;
    }

    private void OnDisable()
    {
        startUpdate = false;
        SaveLoadSceneManager.savestartSliderValue -= SavestartSliderValue;
        SaveLoadSceneManager.savechosenStartDate -= SavesChoseStarttDate;
        SaveLoadSceneManager.savelightCyclePanelState -= SaveslightCyclePanelState;
    }

    public void Update()
    {
        if (startUpdate)
        {
            if (speed == 0)
            {
                SunLight.SetTime(theSlider.value);
            }
            else
            {
                theSlider.value = SunLight.timeInHours;
            }
        }
    }

    public void OnButtonSpeedclick()
    {
        if (speed < 2)
        {
            speed += 1;
        }
        else
        {
            speed = 0;
        }

        SetSpeed(speed);
    }

    public void OnButtonclick(int index)
    {

        switch (index)
        {
            case 0:
                if (!SunLight.leapYear)
                {
                    SunLight.dayOfYear = 80;
                }
                else
                {
                    SunLight.dayOfYear = 81;
                }
                chosenStartDate = StartDate.MARS21;
                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOff;

                break;
            case 1:
                if (!SunLight.leapYear)
                {
                    SunLight.dayOfYear = 172;
                }
                else
                {
                    SunLight.dayOfYear = 173;
                }
                chosenStartDate = StartDate.JUIN21;
                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOff;

                break;
            case 2:
                if (!SunLight.leapYear)
                {
                    SunLight.dayOfYear = 264;
                }
                else
                {
                    SunLight.dayOfYear = 265;
                }
                chosenStartDate = StartDate.SEPT21;
                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOff;

                break;
            case 3:
                if (!SunLight.leapYear)
                {
                    SunLight.dayOfYear = 355;
                }
                else
                {
                    SunLight.dayOfYear = 356;
                }
                chosenStartDate = StartDate.DEC21;
                transform.Find("Holder").Find("Button_21_Decembre").GetComponent<Image>().color = colorOn;

                transform.Find("Holder").Find("Button_21_Mars").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Juin").GetComponent<Image>().color = colorOff;
                transform.Find("Holder").Find("Button_21_Septembre").GetComponent<Image>().color = colorOff;

                break;
        }
    }
}
