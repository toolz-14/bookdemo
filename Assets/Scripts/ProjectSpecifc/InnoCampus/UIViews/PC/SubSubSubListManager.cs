﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SubSubSubListManager : MonoBehaviour {

	[SerializeField]
	private GameObject manager;

	public List<GameObject> subList = new List<GameObject> ();
	[HideInInspector]
	public bool isShowing;
	[HideInInspector]
	public bool toggleOn;
    private bool isOn;

    private void Start () {
		if (transform.Find ("Indicator_Subsub_group") != null) {
			transform.Find ("Indicator_Subsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOffSmall;
		}
		isShowing = false;
	}
	
	public void ShowToggle(){
		isShowing = !isShowing;

		if(isShowing){
			if (transform.Find ("Indicator_Subsub_group") != null && GetComponentInParent<IndicatorSprites> () != null) {
				transform.Find ("Indicator_Subsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOnSmall;
			}
            for (int i=0; i<subList.Count; i++){
				subList [i].SetActive (true);
			}
		}else{
			if (transform.Find ("Indicator_Subsub_group") != null && GetComponentInParent<IndicatorSprites> () != null) {
				transform.Find ("Indicator_Subsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOffSmall;
			}
			for(int i=0; i<subList.Count; i++){
                if (subList[i].GetComponent<SubSubSubSubListManager>() != null)
                {
                    subList[i].GetComponent<SubSubSubSubListManager>().ForceCloseToggle();

                }
				subList [i].SetActive (false);
			}
		}
	}

    public void ForceOpenToggle()
    {
        if (transform.Find("Indicator_Subsub_group") != null && GetComponentInParent<IndicatorSprites>() != null)
        {
            transform.Find("Indicator_Subsub_group").GetComponent<Image>().sprite = GetComponentInParent<IndicatorSprites>().indicatorOnSmall;
        }
        for (int i = 0; i < subList.Count; i++)
        {
            subList[i].SetActive(true);
        }
        isShowing = true;
    }

    public void ForceCloseToggle(){
		if (transform.Find ("Indicator_Subsub_group") != null && GetComponentInParent<IndicatorSprites> () != null) {
			transform.Find ("Indicator_Subsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOffSmall;
		}
		for(int i=0; i<subList.Count; i++){
            if (subList[i].GetComponent<SubSubSubSubListManager>() != null)
            {
                subList[i].GetComponent<SubSubSubSubListManager>().ForceCloseToggle();

            }
			subList [i].SetActive (false);
		}
		isShowing = false;
	}

	public void OnGroupToggleClick(){

		if (GetComponentInChildren<ToolzToggle> (true).isOn) {
            isOn = true;
            foreach (GameObject go in subList) {
                if (go.GetComponent<SubSubSubSubListManager>() != null)
                {
                    go.GetComponent<SubSubSubSubListManager>().ForceOn();

                }
                else if (go.GetComponent<DisplayCalquePC>() != null)
                {
                    go.GetComponent<DisplayCalquePC>().ForceOn();
                }
			}
			toggleOn = true;

			if (manager != null && manager.GetComponent<SubSubListManager> () != null) {
				bool temp = true;
				foreach (GameObject go in manager.GetComponent<SubSubListManager> ().subList) {
                    //check all toggles true 
                    if (go.GetComponent<SubSubSubListManager>() != null && !go.GetComponent<SubSubSubListManager>().toggleOn)
                    {
                        temp = false;
                    }
                    else if (go.GetComponent<DisplayCalquePC>() != null && !go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = false;
                    }
				}
				if (temp) {
					manager.GetComponent<SubSubListManager> ().ForceToggleDisplayOn ();
				}
			}
		} else {
            isOn = false;
            foreach (GameObject go in subList) {
                if (go.GetComponent<SubSubSubSubListManager>() != null)
                {
                    go.GetComponent<SubSubSubSubListManager>().ForceOff();

                }
                else if (go.GetComponent<DisplayCalquePC>() != null)
                {
                    go.GetComponent<DisplayCalquePC>().ForceOff();
                }
            }
			toggleOn = false;

			if (manager != null && manager.GetComponent<SubSubListManager> () != null) {
				bool temp = false;
				foreach (GameObject go in manager.GetComponent<SubSubListManager> ().subList) {
					//check all toggles false
                    if (go.GetComponent<SubSubSubListManager>() != null && go.GetComponent<SubSubSubListManager>().toggleOn)
                    {
                        temp = true;
                    }
                    else if (go.GetComponent<DisplayCalquePC>() != null && go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = true;
                    }
                }
				if (!temp) {
					manager.GetComponent<SubSubListManager> ().ForceToggleDisplayOff ();
				}
			}
		}
	}

    public void ForceOn(){
        GetComponentInChildren<ToolzToggle>(true).ForceOn(true);
        toggleOn = true;
	}

	public void ForceOff(){
        GetComponentInChildren<ToolzToggle>(true).ForceOff(true);

        foreach (GameObject go in subList) {
            if (go.GetComponent<SubSubSubSubListManager>() != null)
            {
                go.GetComponent<SubSubSubSubListManager>().ForceOff();
            }
            else if (go.GetComponent<DisplayCalquePC>() != null)
            {
                go.GetComponent<DisplayCalquePC>().ForceOff();
            }
        }

		toggleOn = false;
	}

    public void ForceToggleDisplayOn()
    {
        GetComponentInChildren<ToolzToggle>(true).ChangeToggleDisplay(true);
        toggleOn = true;
    }

    public void ForceToggleDisplayOff()
    {
        GetComponentInChildren<ToolzToggle>(true).ChangeToggleDisplay(false);
        toggleOn = false;
    }
}
