﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SubSubListManager : MonoBehaviour {
	
	//public enum LegendName{ MobiliteModeDoux, none}
	//[SerializeField]
	//private LegendName Legende = LegendName.none;

	public List<GameObject> subList = new List<GameObject> ();
	[HideInInspector]
	public bool isShowing;
    private bool isOn;

    private void Start () {
		if (transform.Find ("Indicator_Sub_Group") != null && GetComponentInParent<IndicatorSprites> () != null) {
			transform.Find ("Indicator_Sub_Group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOff;
		}
		isShowing = false;
	}
	
	public void ShowToggle(){
		isShowing = !isShowing;
		if(isShowing){
			if (transform.Find ("Indicator_Sub_Group") != null && GetComponentInParent<IndicatorSprites> () != null) {
				transform.Find ("Indicator_Sub_Group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOn;
			}
            for (int i=0; i<subList.Count; i++){
				subList [i].SetActive (true);
			}

		}else{
			if (transform.Find ("Indicator_Sub_Group") != null && GetComponentInParent<IndicatorSprites> () != null) {
				transform.Find ("Indicator_Sub_Group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOff;
			}
			for(int i=0; i<subList.Count; i++){
				if (subList[i].GetComponent<SubSubSubListManager>() != null) {
					subList [i].GetComponent<SubSubSubListManager> ().ForceCloseToggle();
				}
				subList [i].SetActive (false);
			}
		}
	}

    public void ForceOpenToggle()
    {
        if (transform.Find("Indicator_Sub_Group") != null && GetComponentInParent<IndicatorSprites>() != null)
        {
            transform.Find("Indicator_Sub_Group").GetComponent<Image>().sprite = GetComponentInParent<IndicatorSprites>().indicatorOn;
        }
        for (int i = 0; i < subList.Count; i++)
        {
            subList[i].SetActive(true);
        }
        isShowing = true;
    }

	public void ForceCloseToggle(){
		if (transform.Find ("Indicator_Sub_Group") != null && GetComponentInParent<IndicatorSprites> () != null) {
			transform.Find ("Indicator_Sub_Group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOff;
		}
		for(int i=0; i<subList.Count; i++){
			if (subList[i].GetComponent<SubSubSubListManager>() != null) {
				subList [i].GetComponent<SubSubSubListManager> ().ForceCloseToggle();
			}
			subList [i].SetActive (false);
		}
		isShowing = false;
	}

	public void OnGroupToggleClick(){

		if (GetComponentInChildren<ToolzToggle> (true).isOn) {
            isOn = true;
            foreach (GameObject go in subList) {
                if(go.GetComponent<SubSubSubListManager>() != null)
                {
                    go.GetComponent<SubSubSubListManager>().ForceOn();
                }
				else if (go.GetComponent<DisplayCalquePC>() != null)
                {
                    go.GetComponent<DisplayCalquePC>().ForceOn();
                }
            }
		} else {
            isOn = false;
            foreach (GameObject go in subList) {
                if(go.GetComponent<SubSubSubListManager>() != null)
                {
                    go.GetComponent<SubSubSubListManager>().ForceOff();
                }
                else if (go.GetComponent<DisplayCalquePC>() != null)
                {
                    go.GetComponent<DisplayCalquePC>().ForceOff();
                }
            }
		}

//		if(Legende != LegendName.none){
//			if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.transform.parent.GetComponent<SubListManager> () == null) {
//				GetComponentInParent<LegendManager> ().SetLegend ((int)Legende, GetComponentInChildren<Toggle> ().isOn);
//			} else {
//				GetComponentInParent<LegendManager> ().SetLegend ((int)Legende, false);
//			}
//		}
    }

    public void ForceOn(){
		GetComponentInChildren<ToolzToggle>(true).ForceOn(true);
		//have to do this because is toggle was already on previous line doesn't call invoke on the toggle
//		if (Legende != LegendName.none) {
//			GetComponentInParent<LegendManager> ().SetLegend ((int)Legende, false);
//		}
	}

	public void ForceOff(){
        GetComponentInChildren<ToolzToggle>(true).ForceOff(true);

        foreach (GameObject go in subList) {
            if(go.GetComponent<SubSubSubListManager>() != null)
            {
                go.GetComponent<SubSubSubListManager>().ForceOff();
            }
            else if (go.GetComponent<DisplayCalquePC>() != null)
            {
                go.GetComponent<DisplayCalquePC>().ForceOff();
            }
        }
	}

    public void ForceToggleDisplayOn()
    {
        GetComponentInChildren<ToolzToggle>(true).ChangeToggleDisplay(true);
    }

    public void ForceToggleDisplayOff()
    {
        GetComponentInChildren<ToolzToggle>(true).ChangeToggleDisplay(false);
    }
}
