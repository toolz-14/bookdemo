﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCalquePC : MonoBehaviour {

    [SerializeField]
    private bool bg_offColorIsGrey;
    [SerializeField]
    private int categoryID;
    [SerializeField]
	private GameObject manager;
	[SerializeField]
	private GameObject CalquetoBeDisplayed;

	[HideInInspector]
	public bool toggleOn;

    private Color onColor = new Color(0.364705f, 0.364705f, 0.364705f, 1f);
    private Color offColor = new Color(0.807843137254902f, 0.807843137254902f, 0.807843137254902f, 1f);

    private bool doOnce;

    private void Start () {
        doOnce = false;
    }

	public void ShowHideCalque() {
        toggleOn = !toggleOn;

        if (!doOnce)
        {
            doOnce = true;
            CategoryManager.instance.UpdateCategoryList(categoryID, toggleOn);
            Invoke("Delayed", 0.1f);
        }
        
        //change bg color
        if (toggleOn)
        {
            transform.Find("Bg_Calque").GetComponent<Image>().color = onColor;
        }
        else
        {
            if (bg_offColorIsGrey)
            {
                transform.Find("Bg_Calque").GetComponent<Image>().color = offColor;
            }
            else
            {
                transform.Find("Bg_Calque").GetComponent<Image>().color = Color.white;
            }
        }

        if (CalquetoBeDisplayed != null) {

			CalquetoBeDisplayed.SetActive (toggleOn);
        }

        //close parent toggle if all children toggles are off
        if (!toggleOn && manager != null)
        {
            bool temp = false;
            if (manager.GetComponent<SubSubListManager>() != null)
            {
                foreach (GameObject go in manager.GetComponent<SubSubListManager>().subList)
                {
                    //check all toggles false 
                    if (go.GetComponent<DisplayCalquePC>() != null && go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = true;
                    }
                    else if (go.GetComponent<SubSubSubListManager>() != null && go.GetComponent<SubSubSubListManager>().toggleOn)
                    {
                        temp = true;
                    }
                }
                if (!temp)
                {
                    manager.GetComponent<SubSubListManager>().ForceToggleDisplayOff();
                }
            }
            else if (manager.GetComponent<SubSubSubListManager>() != null)
            {
                foreach (GameObject go in manager.GetComponent<SubSubSubListManager>().subList)
                {
                    //check all toggles false 
                    if (go.GetComponent<DisplayCalquePC>() != null && go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = true;
                    }
                    else if (go.GetComponent<SubSubSubSubListManager>() != null && go.GetComponent<SubSubSubSubListManager>().toggleOn)
                    {
                        temp = true;
                    }
                }
                if (!temp)
                {
                    manager.GetComponent<SubSubSubListManager>().ForceToggleDisplayOff();
                }
            }
            else if (manager.GetComponent<SubSubSubSubListManager>() != null)
            {
                foreach (GameObject go in manager.GetComponent<SubSubSubSubListManager>().subList)
                {
                    //check all toggles false 
                    if (go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = true;
                    }
                }
                if (!temp)
                {
                    manager.GetComponent<SubSubSubSubListManager>().ForceOff();
                }
            }
        }
        //open parent toggle if all children toggles are on
        if (toggleOn && manager != null)
        {
            bool temp = true;
            if (manager.GetComponent<SubSubListManager>() != null)
            {
                foreach (GameObject go in manager.GetComponent<SubSubListManager>().subList)
                {
                    //check all toggles false 
                    if (go.GetComponent<DisplayCalquePC>() != null && !go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = false;
                    }
                    else if (go.GetComponent<SubSubSubListManager>() != null && !go.GetComponent<SubSubSubListManager>().toggleOn)
                    {
                        temp = false;
                    }
                }
                if (temp)
                {
                    manager.GetComponent<SubSubListManager>().ForceToggleDisplayOn();
                }
            }
            else if (manager.GetComponent<SubSubSubListManager>() != null)
            {
                foreach (GameObject go in manager.GetComponent<SubSubSubListManager>().subList)
                {
                    //check all toggles false 
                    if (go.GetComponent<DisplayCalquePC>() != null && !go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = false;
                    }
                    else if (go.GetComponent<SubSubSubSubListManager>() != null && !go.GetComponent<SubSubSubSubListManager>().toggleOn)
                    {
                        temp = false;
                    }
                }
                if (temp)
                {
                    manager.GetComponent<SubSubSubListManager>().ForceToggleDisplayOn();
                }
            }
            else if (manager.GetComponent<SubSubSubSubListManager>() != null)
            {
                foreach (GameObject go in manager.GetComponent<SubSubSubSubListManager>().subList)
                {
                    //check all toggles false 
                    if (go.GetComponent<DisplayCalquePC>() != null && !go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = false;
                    }
                }
                if (temp)
                {
                    manager.GetComponent<SubSubSubSubListManager>().ForceOn();
                }
            }
        }
    }

	private void OnDisable(){

	}

	public void ForceOn(){
		toggleOn = true;

        CategoryManager.instance.UpdateCategoryList(categoryID, toggleOn);

        transform.Find("Bg_Calque").GetComponent<Image>().color = onColor;

        if (CalquetoBeDisplayed != null) {
			CalquetoBeDisplayed.SetActive (true);
		}
	}

	public void ForceOff(){
		toggleOn = false;

        CategoryManager.instance.UpdateCategoryList(categoryID, toggleOn);

        if (bg_offColorIsGrey)
        {
            transform.Find("Bg_Calque").GetComponent<Image>().color = offColor;
        }
        else
        {
            transform.Find("Bg_Calque").GetComponent<Image>().color = Color.white;
        }

        if (CalquetoBeDisplayed != null) {
			CalquetoBeDisplayed.SetActive (false);
		}
    }

    private void Delayed()
    {
        doOnce = false;
    }
}
