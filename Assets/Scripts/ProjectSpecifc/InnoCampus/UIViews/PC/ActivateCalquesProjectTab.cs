﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateCalquesProjectTab : MonoBehaviour {

	[SerializeField]
	private GameObject CalquesProjectTab;

	private void Start () {
		
	}

	private void OnEnable () {
		if (CalquesProjectTab.activeSelf) {
			if (CalquesProjectTab.GetComponent<SubSubListManager> () != null) {
				for (int i = 0; i < CalquesProjectTab.GetComponent<SubSubListManager> ().subList.Count; i++) {
					CalquesProjectTab.GetComponent<SubSubListManager> ().subList [i].SetActive (false);
				}
				CalquesProjectTab.GetComponent<SubSubListManager> ().isShowing = false;
			}
			if (CalquesProjectTab.transform.Find ("Indicator") != null && 
				CalquesProjectTab.transform.Find ("Indicator").GetComponent<Image> () != null &&
				CalquesProjectTab.GetComponentInParent<IndicatorSprites> () != null) {
				CalquesProjectTab.transform.Find ("Indicator").GetComponent<Image> ().sprite = CalquesProjectTab.GetComponentInParent<IndicatorSprites> ().indicatorOff;
			}
			CalquesProjectTab.SetActive (false);
			if (CalquesProjectTab.GetComponentInParent<ResizeContainerWithChildAmount> () != null) {
				CalquesProjectTab.GetComponentInParent<ResizeContainerWithChildAmount> ().GetChildrensAndResize ();
			}
		}
	}

	private void OnDisable () {
		if (CalquesProjectTab != null && !CalquesProjectTab.activeSelf) {
			CalquesProjectTab.SetActive (true);
		}
	}
}
