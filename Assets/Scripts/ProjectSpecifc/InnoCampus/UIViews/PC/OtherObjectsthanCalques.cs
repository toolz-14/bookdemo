﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherObjectsthanCalques : MonoBehaviour {

	[SerializeField]
	private List<GameObject> OtherObjects = new List<GameObject>();

	private void Start () {
		
	}

	public void Activate(bool isOn){
		foreach (GameObject go in OtherObjects) {
			if(go != null)
				go.SetActive (isOn);
		}
	}
}
