﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OneToggleOpenAtSameTime : MonoBehaviour {

	public List<SubListManager> subListManager = new List<SubListManager>();

	private void Start () {
	
	}

	public void ResetAllToggles(SubListManager toggle){
		for(int i=0; i<subListManager.Count ; i++){
			if(subListManager [i] != toggle){
				if (subListManager [i] != null && subListManager [i].isShowing) {
					subListManager [i].CloseToggle ();
				}
			}
		}
	}
}
