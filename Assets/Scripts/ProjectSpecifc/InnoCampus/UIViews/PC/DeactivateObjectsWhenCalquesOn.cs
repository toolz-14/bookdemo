﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateObjectsWhenCalquesOn : MonoBehaviour {

	[SerializeField]
	private List<GameObject> OtherObjects = new List<GameObject>();

    private GameObject WasActive = null;
    private bool noObjetcsWereActive;
    private bool wasOn = true;

    private void Start () {
        noObjetcsWereActive = false;
        wasOn = true;
    }

	public void Deactivate(bool isOn)
    {
        //not necessary, just to have it behave the same as OtherObjectsthanCalques for sake of easyness
        isOn = !isOn;

        if (wasOn != isOn)
        {
            wasOn = isOn;

            if (gameObject.name.Equals("ListItem_Calque_Programmation") && !isOn)
            {
                WasActive = null;
            }

            foreach (GameObject go in OtherObjects)
            {
                if (go != null)
                {
                    if (gameObject.name.Equals("ListItem_Calque_Programmation"))
                    {
                        if (!isOn)
                        {
                            if (!go.name.Equals("BATI"))
                            {
                                if (go.activeInHierarchy)
                                {
                                    WasActive = go;
                                }

                                go.SetActive(false);
                            }
                            else
                            {
                                go.SetActive(false);
                            }
                        }
                        else if (isOn)
                        {
                            if (go.name.Equals("BATI"))
                            {
                                go.SetActive(true);
                            }
                            else
                            {
                                go.SetActive(false);
                            }

                            if (WasActive != null && go == WasActive)
                            {
                                go.SetActive(true);
                            }
                        }
                    }
                    else
                    {
                        go.SetActive(isOn);
                    }
                }
            }
        }
	}
}
