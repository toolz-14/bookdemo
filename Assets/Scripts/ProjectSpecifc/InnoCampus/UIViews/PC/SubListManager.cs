﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SubListManager : MonoBehaviour {

	[SerializeField]
	private bool hasSubGroups;
	[SerializeField]
	private List<GameObject> subList = new List<GameObject> ();
	[SerializeField]
	private OneToggleOpenAtSameTime OneToggle;
	[HideInInspector]
	public bool isShowing;


    private void Start () {
		isShowing = false;
	}
	
	public void ShowToggle(){
		isShowing = !isShowing;
		if(isShowing){
			if (OneToggle != null) {
				OneToggle.ResetAllToggles (this);
			}
            for (int i=0; i<subList.Count; i++){
				subList [i].SetActive (true);
			}
		}else{
			for(int i=0; i<subList.Count; i++){
				if (hasSubGroups) {
					subList [i].GetComponent<SubSubListManager> ().ForceCloseToggle ();
				}
				subList [i].SetActive (false);
			}
		}
	}

	public void CloseToggle(){
		for(int i=0; i<subList.Count; i++){
			subList [i].SetActive (false);
		}
		isShowing = false;
	}

	public void OnGroupToggleClick(){
		if (hasSubGroups) {
			if (GetComponentInChildren<Toggle> ().isOn) {
				foreach (GameObject go in subList) {
					go.GetComponent<SubSubListManager> ().ForceOn ();
				}
			} else {
				foreach (GameObject go in subList) {
					go.GetComponent<SubSubListManager> ().ForceOff ();
				}
			}
		} else {
			if (GetComponentInChildren<Toggle> ().isOn) {
				foreach (GameObject go in subList) {
					go.GetComponent<DisplayCalquePC> ().ForceOn ();
				}
			} else {
				foreach (GameObject go in subList) {
					go.GetComponent<DisplayCalquePC> ().ForceOff ();
				}
			}
		}
	}
}
