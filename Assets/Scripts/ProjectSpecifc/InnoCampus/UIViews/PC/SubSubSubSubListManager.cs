﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SubSubSubSubListManager : MonoBehaviour {

	[SerializeField]
	private GameObject manager;
	public List<GameObject> subList = new List<GameObject> ();
	[HideInInspector]
	public bool isShowing;
	[HideInInspector]
	public bool toggleOn;
    private bool isOn;

    private void Start () {
		if (transform.Find ("Indicator_Subsubsub_group") != null && GetComponentInParent<IndicatorSprites> () != null) {
			transform.Find ("Indicator_Subsubsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOffSmall;
		}
		isShowing = false;
	}
	
	public void ShowToggle(){
		isShowing = !isShowing;
		if(isShowing){
			if (transform.Find ("Indicator_Subsubsub_group") != null && GetComponentInParent<IndicatorSprites> () != null) {
				transform.Find ("Indicator_Subsubsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOnSmall;
			}
            for (int i=0; i<subList.Count; i++){
				subList [i].SetActive (true);
			}
		}else{
			if (transform.Find ("Indicator_Subsubsub_group") != null && GetComponentInParent<IndicatorSprites> () != null) {
				transform.Find ("Indicator_Subsubsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOffSmall;
			}
			for(int i=0; i<subList.Count; i++){
				subList [i].SetActive (false);
			}
		}
	}

    public void ForceOpenToggle()
    {
        if (transform.Find("Indicator_Subsubsub_group") != null && GetComponentInParent<IndicatorSprites>() != null)
        {
            transform.Find("Indicator_Subsubsub_group").GetComponent<Image>().sprite = GetComponentInParent<IndicatorSprites>().indicatorOnSmall;
        }
        for (int i = 0; i < subList.Count; i++)
        {
            subList[i].SetActive(true);
        }
        isShowing = true;
    }

    public void ForceCloseToggle(){
		if (transform.Find ("Indicator_Subsubsub_group") != null && GetComponentInParent<IndicatorSprites> () != null) {
			transform.Find ("Indicator_Subsubsub_group").GetComponent<Image> ().sprite = GetComponentInParent<IndicatorSprites> ().indicatorOffSmall;
		}
		for(int i=0; i<subList.Count; i++){
			subList [i].SetActive (false);
		}
		isShowing = false;
	}

	public void OnGroupToggleClick(){
		if (GetComponentInChildren<Toggle> ().isOn) {
            isOn = true;
            foreach (GameObject go in subList) {
				go.GetComponent<DisplayCalquePC> ().ForceOn ();
			}
			toggleOn = true;
			if (manager != null && manager.GetComponent<SubSubSubListManager> () != null) {
				bool temp = true;
				foreach(GameObject go in manager.GetComponent<SubSubSubListManager> ().subList){
                    //check all toggles false 
                    if (go.GetComponent<SubSubSubSubListManager>() && !go.GetComponent<SubSubSubSubListManager>().toggleOn)
                    {
                        temp = false;
                    }
                    else if(go.GetComponent<DisplayCalquePC>() && !go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = false;
                    }
				}
				if(temp){
					manager.GetComponent<SubSubSubListManager> ().ForceOn();
				}
			}
		} else {
            isOn = false;
            foreach (GameObject go in subList) {
				go.GetComponent<DisplayCalquePC> ().ForceOff ();
			}
			toggleOn = false;
			if (manager != null && manager.GetComponent<SubSubSubListManager> () != null) {
				bool temp = false;
				foreach(GameObject go in manager.GetComponent<SubSubSubListManager> ().subList){
					//check all toggles false 
					if (go.GetComponent<SubSubSubSubListManager>() && go.GetComponent<SubSubSubSubListManager> ().toggleOn) {
						temp = true;
					}
                    else if (go.GetComponent<DisplayCalquePC>() && go.GetComponent<DisplayCalquePC>().toggleOn)
                    {
                        temp = true;
                    }
                }
				if(!temp){
					manager.GetComponent<SubSubSubListManager> ().ForceOff();
				}
			}
		}
	}

    public void SetToggle()
    {
        isOn = !isOn;
        if (isOn)
        {
            ForceOn();
        }
        else
        {
            ForceOff();
        }
    }

    public void ForceOn(){
		GetComponentInChildren<Toggle>().isOn = true;
		toggleOn = true;
	}

	public void ForceOff(){
		GetComponentInChildren<Toggle>().isOn = false;
		foreach (GameObject go in subList) {
			go.GetComponent<DisplayCalquePC> ().ForceOff ();
		}
		toggleOn = false;
	}
}
