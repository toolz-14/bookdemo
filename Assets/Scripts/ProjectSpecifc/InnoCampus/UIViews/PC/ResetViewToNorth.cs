﻿using UnityEngine;

public class ResetViewToNorth : MonoBehaviour {

	public FreeTerrainCamera FreeTerrainCamera;
	private Vector3 newCameraEulerAngles;
	private Transform CameraHolder;
	[SerializeField]
	private RectTransform theArrow;


	private void Start () {
		CameraHolder = FreeTerrainCamera.transform.Find ("ConstraintBox").Find ("CameraHolder");
	}

	public void ChangeTheCamera(){
		FreeTerrainCamera.PreventMouseTouchInput (true);

		newCameraEulerAngles = new Vector3(CameraHolder.localEulerAngles.x, 180f, 0f);

		FreeTerrainCamera.MoveTo(CameraHolder.position, newCameraEulerAngles);

		Invoke ("ReEnableInput", 0.1f);
	}

	private void ReEnableInput(){
		FreeTerrainCamera.PreventMouseTouchInput (false);
	}

	//update rotaion.z on arrow to match camera rotation
	private void Update(){
		Vector3 temp = theArrow.localEulerAngles;
		temp.z = 180f - CameraHolder.localEulerAngles.y;
		theArrow.localEulerAngles = temp;
	}
}
