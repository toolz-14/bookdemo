﻿using UnityEngine;
using System.Collections;

public class ShowHideProjectModel : MonoBehaviour {

	private Vector3 _from1;
	private Vector3 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;
	
	private Vector3 _from2;
	private Vector3 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private Vector3 _tempVect3;

	private void Start () {
		_tempVect3 = GetComponent<Transform> ().localPosition;

		_tempVect3.y = -36f;

		GetComponent<Transform> ().localPosition = _tempVect3;

		//set to1
		_tempVect3.y = -36f;
		_to1 = _tempVect3;

		//set to2
		_tempVect3.y = 4.1f;
		_to2 = _tempVect3;
	}

	private void Update () {
		if(_launch){
			Hide();
		}
		if(_launch2){
			Show();
		}
	}

	public void StartHide(){
		//if building is shown
		if (Vector3.Distance (GetComponent<Transform> ().localPosition, _to1) > 20f) {
			_timeSinceTransitionStarted = 0f;
			_from1 = GetComponent<Transform> ().localPosition;
			_launch = true;
		}
	}

	public void StartShow(){
		//if building is hidden
		if (Vector3.Distance (GetComponent<Transform> ().localPosition, _to2) > 20f) {
			_timeSinceTransitionStarted2 = 0f;
			_from2 = GetComponent<Transform> ().localPosition;
			_launch2 = true;
		}
	}

	private void Hide(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<Transform>().localPosition = Vector3.Lerp(_from1, _to1, _t);
		if(Vector3.Distance(GetComponent<Transform>().localPosition, _to1) < 0.01f){
			_launch = false;
		}
	}
	
	private void Show(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		GetComponent<Transform>().localPosition = Vector3.Lerp(_from2, _to2, _t2);
		if(Vector3.Distance(GetComponent<Transform>().localPosition, _to2) < 0.01f){
			_launch2 = false;
		}
	}
}
