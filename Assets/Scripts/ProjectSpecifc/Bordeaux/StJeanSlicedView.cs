﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StJeanSlicedView : MonoBehaviour {
	
	public GameObject AnimatedObject;
	public GameObject StaticObject;
	
	private Animator _animator;
	
	public int myIndex{
		get { return _animator.GetInteger("index"); }
		set { _animator.SetInteger("index", value); }
	}
	
	
	private void Awake(){
		_animator = AnimatedObject.GetComponent<Animator>();
	}
	
	public void ResetAnimSateOnEnter(){
		Invoke("SetConstruction", 1.4f);
	}
	
	private void SetConstruction () {
		AnimatedObject.SetActive (true);
		StaticObject.SetActive (false);
		myIndex = 0;
		_animator.Play("Floorup"); //StadiumDown
	}
	
	public void StartPlayingAnimation(int index){
		myIndex = index;
	}
	
	public void ResetAnimSateOnExit(){
		Invoke("ResetState", 1.4f);
	}
	
	private void ResetState(){
		myIndex = 0;
		AnimatedObject.SetActive (false);
		StaticObject.SetActive (true);
	}
}
