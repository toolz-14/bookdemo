﻿using UnityEngine;
using System.Collections;

public class BiomassAnimCamMove : MonoBehaviour {

	public GameObject Target;
	public float Duration = 2f;
	public float Delay = 0f;
	
	private Quaternion _startRotation;
	private Vector3 _startPosition;
	private Transform _cameraHolder;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;

	private void Start () {
		_cameraHolder = transform.Find ("ConstraintBox").Find ("CameraHolder");
		_startPosition = _cameraHolder.position;
		_startRotation = _cameraHolder.rotation;
	}

	private void OnEnable() {
		Reset ();
	}
	
	private void Reset() {
		_timeSinceTransitionStarted = 0f;
		_launch = false;
		if (_cameraHolder != null) {
			_startPosition = _cameraHolder.position;
			_startRotation = _cameraHolder.rotation;
		}
	}

	public void LauchCameraMvmt(){
		Invoke ("StartCameraMovement", Delay);
	}

	private void StartCameraMovement(){
		GetComponent<FreeTerrainCamera> ().IsMovingByGesture = false;
		_timeSinceTransitionStarted = 0f;
		_launch = true;
	}

	private void CameraMovement(){

		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration;
		
		_cameraHolder.position = Vector3.Lerp(_startPosition, Target.transform.position, _t);
		_cameraHolder.rotation = Quaternion.Lerp(_startRotation, Target.transform.rotation, _t);
		if(Vector3.Distance(_cameraHolder.position, Target.transform.position) < 0.01f){
			_launch = false;
			GetComponent<FreeTerrainCamera> ().SetPositionRotation(_cameraHolder.position, _cameraHolder.rotation.eulerAngles);
			GetComponent<FreeTerrainCamera> ().IsMovingByGesture = true;
		}
	}

	public void Update(){
		if(_launch){
			CameraMovement();
		}
	}
}
