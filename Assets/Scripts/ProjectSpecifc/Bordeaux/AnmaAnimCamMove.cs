﻿using UnityEngine;
using System.Collections;

public class AnmaAnimCamMove : MonoBehaviour {

	public Animation AnmaTopPart;
	public GameObject Target1;
	public GameObject Target2;
	public GameObject Target3;
	public float Duration1 = 2f;
	public float Duration2 = 2f;
	public float Duration3 = 2f;
	public float Delay = 2f;
	public float Delay2 = 0f;
	public float Delay3 = 0f;
	
	private Quaternion _startRotation;
	private Vector3 _startPosition;
	private Transform _cameraHolder;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	
	private Quaternion _startRotation2;
	private Vector3 _startPosition2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;

	private Quaternion _startRotation3;
	private Vector3 _startPosition3;
	private bool _launch3;
	private float _timeSinceTransitionStarted3;
	private float _t3;
	private bool _isAnimPlaying;
	
	private void Start () {
		_cameraHolder = transform.Find ("ConstraintBox").Find ("CameraHolder");
		_startPosition = _cameraHolder.position;
		_startRotation = _cameraHolder.rotation;
		_isAnimPlaying = false;
	}

	private void OnEnable() {
		Reset ();
	}
	
	private void Reset() {
		if(AnmaTopPart != null){
			AnmaTopPart.Play("AnmaTop");
			AnmaTopPart.GetComponent<Animation>()["AnmaTop"].speed = -1;
			AnmaTopPart.GetComponent<Animation>()["AnmaTop"].time = 0;
		}
		_timeSinceTransitionStarted = 0f;
		_timeSinceTransitionStarted2 = 0f;
		_timeSinceTransitionStarted3 = 0f;
		_launch = false;
		_launch2 = false;
		_launch3 = false;
		if (_cameraHolder != null) {
			_cameraHolder.position = _startPosition;
			_cameraHolder.rotation = _startRotation;
		}
		_isAnimPlaying = false;
	}

	private void OnDisable(){

	}
	
	public void LauchCameraMvmt(){
		if (!_isAnimPlaying) {
			_isAnimPlaying = true;
			Invoke ("StartCameraMovement", Delay);
		}
	}
	
	private void StartCameraMovement(){
		//Debug.Log ("StartCameraMovement");
		_cameraHolder.position = _startPosition;
		_cameraHolder.rotation = _startRotation;
		GetComponent<DragCam> ().IsMovingByGesture = false;
		_timeSinceTransitionStarted = 0f;
		_timeSinceTransitionStarted2 = 0f;
		_launch = true;

		AnmaTopPart.GetComponent<Animation>() ["AnmaTop"].time = 0f;
		AnmaTopPart.GetComponent<Animation>() ["AnmaTop"].speed = 1f;
		AnmaTopPart.Play ("AnmaTop");
	}
	
	private void CameraMGo1(){
		
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;

		_cameraHolder.position = Vector3.Lerp(_startPosition, Target1.transform.position, _t);
		_cameraHolder.rotation = Quaternion.Lerp(_startRotation, Target1.transform.rotation, _t);
		if(Vector3.Distance(_cameraHolder.position, Target1.transform.position) < 0.01f){
			_launch = false;
			Invoke("LauchSecondMovement", Delay2);
		}
	}
	
	private void LauchSecondMovement(){
		_startPosition2 = _cameraHolder.position;
		_startRotation2 = _cameraHolder.rotation;
		_launch2 = true;
	}
	
	private void CameraGo2(){
		
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		_cameraHolder.position = Vector3.Lerp(_startPosition2, Target2.transform.position, _t2);
		_cameraHolder.rotation = Quaternion.Lerp(_startRotation2, Target2.transform.rotation, _t2);
		if(Vector3.Distance(_cameraHolder.position, Target2.transform.position) < 0.01f){
			_launch2 = false;
			Invoke("LauchThirdMovement", Delay3);
		}
	}

	private void LauchThirdMovement(){
		_startPosition3 = _cameraHolder.position;
		_startRotation3 = _cameraHolder.rotation;
		_launch3 = true;
	}

	private void CameraGo3(){
		
		_timeSinceTransitionStarted3 += Time.deltaTime;
		_t3 = _timeSinceTransitionStarted3 / Duration3;
		
		_cameraHolder.position = Vector3.Lerp(_startPosition3, Target3.transform.position, _t3);
		_cameraHolder.rotation = Quaternion.Lerp(_startRotation3, Target3.transform.rotation, _t3);
		if(Vector3.Distance(_cameraHolder.position, Target3.transform.position) < 0.01f){
			_launch3 = false;
			GetComponent<DragCam> ().SetPositionRotation(_cameraHolder.position, _cameraHolder.rotation.eulerAngles);
			GetComponent<DragCam> ().IsMovingByGesture = true;
		}
	}
	
	public void Update(){
		if(_launch){
			CameraMGo1();
		}
		if(_launch2){
			CameraGo2();
		}
		if(_launch3){
			CameraGo3();
		}
	}
}
