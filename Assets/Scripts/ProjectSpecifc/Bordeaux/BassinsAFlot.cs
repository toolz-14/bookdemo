﻿using UnityEngine;
using System.Collections;

public class BassinsAFlot : MonoBehaviour {

	public GameObject AnimatedObject;
	public static bool isUp;
	private Animation _animation;

	private void Awake () {
		_animation = AnimatedObject.GetComponent<Animation> ();
	}

	private void Start () {

	}

	public void PlayAnim () {
	
		if(isUp){
			_animation.GetComponent<Animation>()["BassinsFlot"].time = _animation.GetComponent<Animation>()["BassinsFlot"].length;
			_animation.GetComponent<Animation>()["BassinsFlot"].speed = -1f;
			
			_animation.Play("BassinsFlot");

			isUp = false;

		}else{
			_animation.GetComponent<Animation>()["BassinsFlot"].speed = 1f;
			
			_animation.Play("BassinsFlot");

			isUp = true;
		}
	}
}
