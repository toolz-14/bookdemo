﻿using UnityEngine;
using System.Collections;

public class EffluentAnimCamMove : MonoBehaviour {

	public GameObject TargetClose;
	public GameObject TargetFar;
	public float DurationClose = 2f;
	public float DurationFar = 2f;
	public float Delay = 0f;
	public float Delay2 = 0f;
	
	private Quaternion _startRotation;
	private Vector3 _startPosition;
	private Transform _cameraHolder;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;

	private Quaternion _startRotation2;
	private Vector3 _startPosition2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	
	private void Start () {
		_cameraHolder = transform.Find ("ConstraintBox").Find ("CameraHolder");
		_startPosition = _cameraHolder.position;
		_startRotation = _cameraHolder.rotation;
	}

	private void OnEnable() {
		Reset ();
	}
	
	private void Reset() {
		_timeSinceTransitionStarted = 0f;
		_timeSinceTransitionStarted2 = 0f;
		_launch = false;
		_launch2 = false;
		if (_cameraHolder != null) {
			_startPosition = _cameraHolder.position;
			_startRotation = _cameraHolder.rotation;
		}
	}
	
	public void LauchCameraMvmt(){
		Invoke ("StartCameraMovement", Delay);
	}
	
	private void StartCameraMovement(){
		GetComponent<FreeTerrainCamera> ().IsMovingByGesture = false;
		_timeSinceTransitionStarted = 0f;
		_timeSinceTransitionStarted2 = 0f;
		_launch = true;
	}
	
	private void CameraMGoClose(){
		
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / DurationClose;
		
		_cameraHolder.position = Vector3.Lerp(_startPosition, TargetClose.transform.position, _t);
		_cameraHolder.rotation = Quaternion.Lerp(_startRotation, TargetClose.transform.rotation, _t);
		if(Vector3.Distance(_cameraHolder.position, TargetClose.transform.position) < 0.01f){
			_launch = false;
			Invoke("LauchSecondMovement", Delay2);
		}
	}

	private void LauchSecondMovement(){
		_startPosition2 = _cameraHolder.position;
		_startRotation2 = _cameraHolder.rotation;
		_launch2 = true;
	}

	private void CameraGoFarBack(){
		
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / DurationFar;
		
		_cameraHolder.position = Vector3.Lerp(_startPosition2, TargetFar.transform.position, _t2);
		_cameraHolder.rotation = Quaternion.Lerp(_startRotation2, TargetFar.transform.rotation, _t2);
		if(Vector3.Distance(_cameraHolder.position, TargetFar.transform.position) < 0.01f){
			_launch2 = false;
			GetComponent<FreeTerrainCamera> ().SetPositionRotation(_cameraHolder.position, _cameraHolder.rotation.eulerAngles);
			GetComponent<FreeTerrainCamera> ().IsMovingByGesture = true;
		}
	}
	
	public void Update(){
		if(_launch){
			CameraMGoClose();
		}
		if(_launch2){
			CameraGoFarBack();
		}
	}
}
