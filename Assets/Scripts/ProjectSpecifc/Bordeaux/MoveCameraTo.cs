﻿using UnityEngine;
using System.Collections;

public class MoveCameraTo : MonoBehaviour {

	public Transform TransitionCamera;
	private bool _move;
	private float _timeSinceTransitionStarted;
	public float TransitionDuration;

	public Transform To;

	private Vector3 _fromPos;
	private Quaternion _fromRot;
	
	void Awake() {
		_move = false;
	}

	void Start () {
	
	}

	public void Movecamera(){
		TransitionCamera.parent.parent.GetComponent<FreeTerrainCamera> ().IsMovingByGesture = false;
		_timeSinceTransitionStarted = 0f;
		_fromPos = TransitionCamera.position;
		_fromRot = TransitionCamera.rotation;
		_move = true;
	}

	void Update () {
		if (_move){
			LerpCamera();
		}
	}

	private void LerpCamera () {
		_timeSinceTransitionStarted += Time.deltaTime;
		float t = _timeSinceTransitionStarted / TransitionDuration;
		
		TransitionCamera.position = Vector3.Lerp(_fromPos, To.position, t);
		TransitionCamera.rotation = Quaternion.Lerp(_fromRot, To.rotation, t);
		if(Vector3.Distance(TransitionCamera.position, To.position) < 0.01f){
			_move = false;
			TransitionCamera.parent.parent.GetComponent<FreeTerrainCamera> ().SetPositionRotation(TransitionCamera.position, TransitionCamera.rotation.eulerAngles);
			TransitionCamera.parent.parent.GetComponent<FreeTerrainCamera> ().IsMovingByGesture = true;
		}
	}
}
