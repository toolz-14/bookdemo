﻿using UnityEngine;
using System.Collections;

public class SetActiveHotspots : MonoBehaviour {

	public GameObject objectToActivate;

	// Use this for initialization
	void Start () {
		Invoke ("ActivateHS", 2f);
	}

	public void ActivateHS(){
		objectToActivate.SetActive (true);
	}
}
