﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StJeanConstruction : MonoBehaviour {
	
	public GameObject AnimatedObject;
	public GameObject StaticObject;
	
	private Animator _animator;
	private Slider _slider;
	
	public int myIndex{
		get { return _animator.GetInteger("constructioIndex"); }
		set { _animator.SetInteger("constructioIndex", value); }
	}
	
	
	private void Awake(){
		_animator = AnimatedObject.GetComponent<Animator>();
		_slider = GetComponent<Slider>();
	}
	
	public void ResetAnimSateOnEnter(){
		Invoke("SetConstruction", 1.4f);
	}
	
	private void SetConstruction () {
		AnimatedObject.SetActive (true);
		StaticObject.SetActive (false);
		myIndex = 0;
		_slider.value = 0f;
		_animator.Play("Contruction0"); //StadiumDown
	}
	
	public void PlayAnimation(){
		
		myIndex = (int)_slider.value;
	}
	
	public void ResetAnimSateOnExit(){
		Invoke("ResetState", 1.4f);
	}
	
	private void ResetState(){
		AnimatedObject.SetActive (false);
		StaticObject.SetActive (true);
	}
}
