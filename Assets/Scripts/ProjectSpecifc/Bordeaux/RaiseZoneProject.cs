﻿using UnityEngine;
using System.Collections;

public class RaiseZoneProject : MonoBehaviour {

	public GameObject AnimatedObject;
	private bool _isUp;
	private Animation _animation;

	public float DelayBeforeAnim = 1f;
	
	private void Awake () {
		_animation = AnimatedObject.GetComponent<Animation> ();
	}
	
	private void Start () {
		Invoke ("PlayAnim", DelayBeforeAnim);
	}

	private void PlayAnim () {
		_animation.GetComponent<Animation>()["BassinsFlot"].speed = 1f;
		
		_animation.Play("BassinsFlot");

		BassinsAFlot.isUp = true;
	}
}
