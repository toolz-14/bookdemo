﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Slide : MonoBehaviour {

	[HideInInspector]
	public Image image;
	private RectTransform rectTranform;

	[HideInInspector]
	public const float MOVE_LENGTH = 1000f;

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;
	private SlideShow SlideShow;
	
	private void Awake(){
		rectTranform = GetComponent<RectTransform>();
		image = GetComponent<Image>();
		SlideShow = GetComponentInParent<SlideShow> ();
	}

	private void Start(){
		
	}
	
	private void Update(){
		if(_launch){
			LaunchLerp();
		}
	}

	public void InitLerp(bool lerpRight){
		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().anchoredPosition;
		_to1 = Vector2.zero;

		if (lerpRight) {
			//lerp right
			//If image in center
			if (Mathf.Abs(rectTranform.anchoredPosition.x - 0) < 1.0f){
				_to1.x = MOVE_LENGTH;
				_launch = true;
			}
			//Else if image right
			else if (Mathf.Abs(rectTranform.anchoredPosition.x - MOVE_LENGTH) < 1.0f){
				//pas lerp
				GetComponent<RectTransform> ().anchoredPosition = new Vector2(-MOVE_LENGTH, 0f);
				image.sprite = SlideShow.GetNextImage(0);//+1
			}
			//Else if image left
			else if (Mathf.Abs(rectTranform.anchoredPosition.x - (-MOVE_LENGTH)) < 1.0f){
				_to1.x = 0;
				_launch = true;
			}else{
				Debug.Log("InitLerp right can't find endpos");
			}
		} else {
			//lerp left
			//If image in center
			if (Mathf.Abs(rectTranform.anchoredPosition.x - 0) < 1.0f){
				_to1.x = -MOVE_LENGTH;
				_launch = true;
			}
			//Else if image right
			else if (Mathf.Abs(rectTranform.anchoredPosition.x - MOVE_LENGTH) < 1.0f){
				_to1.x = 0;
				_launch = true;
			}
			//Else if image left
			else if (Mathf.Abs(rectTranform.anchoredPosition.x - (-MOVE_LENGTH)) < 1.0f){
				//pas lerp
				GetComponent<RectTransform> ().anchoredPosition = new Vector2(MOVE_LENGTH, 0f);
				image.sprite = SlideShow.GetNextImage(0);//-1
			}else{
				Debug.Log("InitLerp left can't find endpos");
			}
		}
	}
	
	private void LaunchLerp(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f){
			_launch = false;
		}
	}	
}
