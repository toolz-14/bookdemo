﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class SlideShow : MonoBehaviour, ISwipeGesture {

	protected Gestures _gestures;
	protected SwipeGesture _swipeGesture;
    [SerializeField]
    private Button projectPanelExitButton;
	
	public Sprite[] _slides;

	public Slide Slide1;
	public Slide Slide2;
	public Slide Slide3;
    public Image imageSlide1;
    public Image imageSlide2;
    public Image imageSlide3;

    private int SlideshowID;
	private Image _image1;
	private Image _image2;
	private Image _image3;
	
	private int _shownImageIndex = 0;
	
	private bool _DoOnce;

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.1f;
	
	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.1f;

	private void Awake(){
		GetComponent<RectTransform> ().localScale = Vector2.zero;

		_gestures = Gestures.instance;
	}
	
	private void Start(){

	}

	private void Update () {
		if(_launch){
			OpenSlideshow();
		}
		if(_launch2){
			CloseSlideshow();
		}
	}
	
	public void SwipeDrag(SwipeGesture g, Vector2 drag, float scope){
	}
	
	public void SwipeReset(SwipeGesture g, Vector2 axis){
	}
	
	public void SwipeValidate(SwipeGesture g, Vector2 direction, float tweenDuration){
        if (_slides.Length > 1)
        {
            if (transform.localScale != Vector3.zero)
            {
                if (direction.x == -1)
                {
                    Next();
                }
                if (direction.x == 1)
                {
                    Previous();
                }
            }
        }
	}

	private void OnEnable() {
		_swipeGesture = new SwipeGesture(this, "world");
	}
	
	private void OnDisable() { 
		if (_swipeGesture == null) return;
		_swipeGesture.Dispose();
		_swipeGesture = null;
	}
	
	private void LoadSprites(){
		Resources.UnloadUnusedAssets ();
		_slides = Resources.LoadAll("Slideshow/Type_"+SlideshowID.ToString(), typeof(Sprite)).Cast<Sprite>().ToArray(); //UIPanelBuilding._slideshowID

        //for debug
        //Debug.Log("LoadSprites, Slideshow/Verneuil/Type_" + SlideshowID.ToString());
        //Debug.Log("LoadSprites, _slides.Length = " + _slides.Length);
        //for (int i = 0; i < _slides.Length; i++)
        //{
        //    Debug.Log("_slides[i].name" + _slides[i].name);
        //}
    }

    public void InitSlide(){

        _image1 = imageSlide1;
		_image2 = imageSlide2;
		_image3 = imageSlide3;

        if (_slides.Length == 1)
        {
            _image2.sprite = _slides[0];
        }
        else
        {
            _image1.sprite = GetNextImage(0);
            _image2.sprite = GetNextImage(+1);
            _image3.sprite = GetNextImage(+2);
        }
	}

	/**
         * @param direction: >0 == we want the next image to the right. <0 == we want the next image to the left
         */
	public Sprite GetNextImage(int direction = 0){
		int indexImage = _shownImageIndex;
		indexImage += direction;
		
		if (indexImage >= _slides.Length) {
			indexImage = 0;
		}
		else if (indexImage < 0) {
			indexImage = _slides.Length - 1;
		}
		if (indexImage >= 0 && indexImage <= _slides.Length) {
			Sprite nextImage = _slides [indexImage];
           // Debug.Log("GetNextImage" + nextImage.name);
            return nextImage;
		} else {
			//--- if no image in resources folder?
			Debug.Log("GetNextImage(), couldn't find image in resources");
			return null;
		}
	}

    public void Next()
    {
        if (_slides.Length > 1) {
            if (!_DoOnce)
            {
                _DoOnce = true;
                _shownImageIndex++;
                if (_shownImageIndex >= _slides.Length)
                {
                    _shownImageIndex = 0;
                }

                Slide1.InitLerp(false);
                Slide2.InitLerp(false);
                Slide3.InitLerp(false);
                Invoke("ReleaseButton", 0.5f);
            }
        }
	}
	
	public void Previous(){
        if (_slides.Length > 1)
        {
            if (!_DoOnce)
            {
                _DoOnce = true;
                _shownImageIndex--;
                if (_shownImageIndex < 0)
                {
                    _shownImageIndex = _slides.Length - 1;
                }

                Slide1.InitLerp(true);
                Slide2.InitLerp(true);
                Slide3.InitLerp(true);
                Invoke("ReleaseButton", 0.5f);
            }
        }
	}

	private void ReleaseButton(){
		_DoOnce = false;
	}

	public void StartOpenSlideshow(int SsID,List<Sprite> sprites){
		SlideshowID = SsID;
        _slides = sprites.ToArray();
        //LoadSprites();
        InitSlide();
		HideSlides (true);

		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().localScale;
		_to1 = new Vector3 (1f, 1f, 1f);
        gameObject.SetActive(true);
		_launch = true;
	}
	
	private void OpenSlideshow(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		transform.localScale = Vector3.Lerp(_from1, _to1, _t);
		if(Vector3.Distance(transform.localScale, _to1) < 0.01f){
			_launch = false;
            projectPanelExitButton.interactable = false;
            HideSlides (false);
		}
	}
	
	public void StartCloseSlideshow(){

		HideSlides (true);

		_timeSinceTransitionStarted2 = 0f;
		_from2 = transform.localScale;
		_to2 = Vector3.zero;
		_launch2 = true;
	}
	
	private void CloseSlideshow(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		transform.localScale = Vector3.Lerp(_from2, _to2, _t2);
		if(Vector3.Distance(transform.localScale, _to2) < 0.01f){
			_launch2 = false;
			transform.localScale = Vector2.zero;
            projectPanelExitButton.interactable = true;
            gameObject.SetActive(false);
		}
	}

	//hide the left and right slides during onpen/close lerp [looks better]
	private void HideSlides(bool hide){
		if (hide) {
			if(Slide1.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide1.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide1.gameObject.SetActive(false);
			}
			if(Slide2.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide2.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide2.gameObject.SetActive(false);
			}
			if(Slide3.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide3.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide3.gameObject.SetActive(false);
			}
		} else {
			if(Slide1.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide1.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide1.gameObject.SetActive(true);
			}
			if(Slide2.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide2.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide2.gameObject.SetActive(true);
			}
			if(Slide3.GetComponent<RectTransform>().anchoredPosition.x < -900f || Slide3.GetComponent<RectTransform>().anchoredPosition.x > 900f){
				Slide3.gameObject.SetActive(true);
			}
		}
	}

}
