﻿using UnityEngine;
using System.Collections;


namespace Toolz.ProjectSpecifc.Alsolen {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Alsolen/ModelToSwap")]

	public class ModelToSwap : MonoBehaviour {

		private SwapModelsOnClick _parent;
		private Bounds _modelBounds;

		private void Awake () {
			_parent = GetComponentInParent<SwapModelsOnClick>();
		}

		private void Start () {
		
		}

		private void OnMouseDown() {
			_parent.OnModelClicked(gameObject.name);
		}

		public void SetBounds(){
			if(gameObject.GetComponent<Collider>() != null){
				_modelBounds = gameObject.GetComponent<Collider>().bounds;
			}
		}

		public float GetModelColliderHeight(){  
			return _modelBounds.size.y;
		}
	}
}
