﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/****
 * 
 * models to be swapped must be placed in the public list
 * the first model in the list is the one that will be shown
 * 
 */ 

namespace Toolz.ProjectSpecifc.Alsolen {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Alsolen/SwapModelsOnClick")]

	public class SwapModelsOnClick : MonoBehaviour {

		public GameObject ActiveWhenModuleIs;
		public float SpaceBetweenModels = 2f;
		public List<ModelToSwap> ModelsToBeSwapped = new List<ModelToSwap>();

        public static readonly string CLICKME_LIGHT_NAME = "ClickmeLight";

		private string _activeModelName;
        private Transform _clickmeLight;

		private void Awake () {

			foreach(ModelToSwap go in ModelsToBeSwapped){
				go.SetBounds();
				go.gameObject.SetActive(false);
			}
            ModelsToBeSwapped[0].gameObject.SetActive(true);
			_activeModelName = ModelsToBeSwapped[0].gameObject.name;

            _clickmeLight = transform.Find(CLICKME_LIGHT_NAME);
            _clickmeLight.gameObject.SetActive(false);
            Module parent = GetComponentInParent<Module>();
            parent.OnModuleActivated += (() => _clickmeLight.gameObject.SetActive(true));
            parent.OnModuleDeactivated += (() => _clickmeLight.gameObject.SetActive(false));
		}

		private void Start () {
		    
		}

		public void OnModelClicked(string modelName){

			if(ActiveWhenModuleIs.activeInHierarchy){
				int modelIndex = FindModelIndex(modelName);

				//if [0] and others not active : place & show others
				if(modelIndex == 0 && !AreOtherModelsActive()){
					PlaceAndShowOtherModels();
                    _clickmeLight.gameObject.SetActive(false);
				}

				//if [0] and others actives : hide others
				else if(modelIndex == 0 && AreOtherModelsActive()){
					HideOtherModels();
                    _clickmeLight.gameObject.SetActive(true);
				}

				//if other : move this in [0] & hide others
				else if(modelIndex != 0){
					SwapSelectedAndFirstModels(ModelsToBeSwapped[modelIndex], modelIndex);
					HideOtherModels();
                    _clickmeLight.gameObject.SetActive(true);
				}
			}
		}

		public string GetActiveModelName(){
			return _activeModelName;
		}

		// returns model index in list or -1 if not found
		private int FindModelIndex(string modelName){
			int result = -1;
			for(int i=0; i<ModelsToBeSwapped.Count; i++){
				if(ModelsToBeSwapped[i].gameObject.name == modelName){
					result = i;
				}
			}
			return result;
		}

		private bool AreOtherModelsActive(){
			if(ModelsToBeSwapped[1].gameObject.activeInHierarchy){
				return true;
			}
			return false;
		}

		private void PlaceAndShowOtherModels(){
			for(int i=1; i<ModelsToBeSwapped.Count; i++){
				Vector3 temp = ModelsToBeSwapped[i-1].transform.position;
				temp.y += ModelsToBeSwapped[i-1].GetModelColliderHeight()/2 + ModelsToBeSwapped[i].GetModelColliderHeight()/2 + SpaceBetweenModels;
				ModelsToBeSwapped[i].transform.position = temp;
				ModelsToBeSwapped[i].gameObject.SetActive(true);
			}
		}

		private void HideOtherModels(){
			for(int i=1; i<ModelsToBeSwapped.Count; i++){
				ModelsToBeSwapped[i].gameObject.SetActive(false);
			}
		}

		private void SwapSelectedAndFirstModels(ModelToSwap selected, int selectedIndex){
			ModelToSwap first = ModelsToBeSwapped[0];

			//swap positions
			Vector3 temp = selected.transform.position;
			selected.transform.position = first.transform.position;
			first.transform.position = temp;

			//swap position in list
			ModelsToBeSwapped.Remove(first);
			ModelsToBeSwapped.Remove(selected);
			ModelsToBeSwapped.Insert(0, selected);
			ModelsToBeSwapped.Insert(selectedIndex, first);
			_activeModelName = ModelsToBeSwapped[0].gameObject.name;
		}

	}
}
