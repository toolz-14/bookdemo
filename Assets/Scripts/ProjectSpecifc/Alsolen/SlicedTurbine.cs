﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolz.ProjectSpecifc.Alsolen {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Alsolen/SlicedTurbine")]

	public class SlicedTurbine : MonoBehaviour {

		public GameObject NormalModels;
		public List<GameObject> ModelsToActivate = new List<GameObject>();

		private void Start () {
			
		}

		private void OnEnable(){
			SetModelActive();
			NormalModels.SetActive(false);

		}

		private void OnDisable(){
			DeactivateAllModels();
			NormalModels.SetActive(true);
		}

		private void SetModelActive(){
			string s = NormalModels.GetComponent<SwapModelsOnClick>().GetActiveModelName();
			foreach(GameObject go in ModelsToActivate){
				if(go.name.Contains(s)){
					go.SetActive(true);
				}else{
					go.SetActive(false);
				}
			}
		}

		private void DeactivateAllModels(){
			foreach(GameObject go in ModelsToActivate){
				go.SetActive(false);
			}
		}
	}
}
