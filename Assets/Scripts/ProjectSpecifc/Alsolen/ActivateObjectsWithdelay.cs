﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolz.ProjectSpecifc.Alsolen {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Alsolen/ActivateObjectsWithdelay")]

	public class ActivateObjectsWithdelay : MonoBehaviour {

		public float DelayBeforeStart = 2f;
		public float ActivationDelay = 0.2f;
		public List<GameObject> ObjectsToActivate = new List<GameObject>();
		public List<GameObject> HotspotsToActivate = new List<GameObject>();
		//public Toolz.Bricks.View.Lean_FreeCamera MainCam;

		private void Awake () {
			foreach(GameObject go in ObjectsToActivate){
				go.SetActive(false);
			}
			//MainCam.enabled = false;
		}

		private void Start () {
			for(int i=0; i<ObjectsToActivate.Count; i++){
				ObjectsToActivate[i].SetActive(false);
				HotspotsToActivate[i].SetActive(false);
			}
			StartCoroutine("ActivateObjects");
		}
		
		IEnumerator ActivateObjects(){
			yield return new WaitForSeconds(DelayBeforeStart);
			for(int i=0; i<ObjectsToActivate.Count; i++){
				yield return new WaitForSeconds(ActivationDelay);
				ObjectsToActivate[i].SetActive(true);
				HotspotsToActivate[i].SetActive(true);
			}
			//MainCam.enabled = true;
		}
	}
}
