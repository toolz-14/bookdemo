﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Toolz.ProjectSpecifc.Alsolen {
	
	[AddComponentMenu("Toolz/ProjectSpecifc/Alsolen/MirrorsAnimController")]

	public class MirrorsAnimController : MonoBehaviour {

		public Slider _slider;
		public GameObject HideReflecteurs;

		public bool isUp1{
			get { return MirrorGrp1.GetBool("isUp"); }
			set { MirrorGrp1.SetBool("isUp", value); }
		}
		public bool isUp2{
			get { return MirrorGrp2.GetBool("isUp"); }
			set { MirrorGrp2.SetBool("isUp", value); }
		}
		public bool isUp3{
			get { return MirrorGrp3.GetBool("isUp"); }
			set { MirrorGrp3.SetBool("isUp", value); }
		}
		public bool isUp4{
			get { return MirrorGrp4.GetBool("isUp"); }
			set { MirrorGrp4.SetBool("isUp", value); }
		}
		public bool isUp5{
			get { return MirrorGrp5.GetBool("isUp"); }
			set { MirrorGrp5.SetBool("isUp", value); }
		}
		public bool isUp6{
			get { return MirrorGrp6.GetBool("isUp"); }
			set { MirrorGrp6.SetBool("isUp", value); }
		}
		public bool isUp7{
			get { return MirrorGrp7.GetBool("isUp"); }
			set { MirrorGrp7.SetBool("isUp", value); }
		}
		public bool isUp8{
			get { return MirrorGrp8.GetBool("isUp"); }
			set { MirrorGrp8.SetBool("isUp", value); }
		}
		public bool isUp9{
			get { return MirrorGrp9.GetBool("isUp"); }
			set { MirrorGrp9.SetBool("isUp", value); }
		}
		public bool isUp10{
			get { return MirrorGrp10.GetBool("isUp"); }
			set { MirrorGrp10.SetBool("isUp", value); }
		}
		public bool isUp11{
			get { return MirrorGrp11.GetBool("isUp"); }
			set { MirrorGrp11.SetBool("isUp", value); }
		}
		public bool isUp12{
			get { return MirrorGrp12.GetBool("isUp"); }
			set { MirrorGrp12.SetBool("isUp", value); }
		}
		public bool isUp13{
			get { return MirrorGrp13.GetBool("isUp"); }
			set { MirrorGrp13.SetBool("isUp", value); }
		}
		public bool isUp14{
			get { return MirrorGrp14.GetBool("isUp"); }
			set { MirrorGrp14.SetBool("isUp", value); }
		}
		public bool isUp15{
			get { return MirrorGrp15.GetBool("isUp"); }
			set { MirrorGrp15.SetBool("isUp", value); }
		}
		public bool isUp16{
			get { return MirrorGrp16.GetBool("isUp"); }
			set { MirrorGrp16.SetBool("isUp", value); }
		}
		public bool isUp17{
			get { return MirrorGrp17.GetBool("isUp"); }
			set { MirrorGrp17.SetBool("isUp", value); }
		}
		public bool isUp18{
			get { return MirrorGrp18.GetBool("isUp"); }
			set { MirrorGrp18.SetBool("isUp", value); }
		}
		public bool isUp19{
			get { return MirrorGrp19.GetBool("isUp"); }
			set { MirrorGrp19.SetBool("isUp", value); }
		}
		public bool isUp20{
			get { return MirrorGrp20.GetBool("isUp"); }
			set { MirrorGrp20.SetBool("isUp", value); }
		}



		private Animator MirrorGrp1;
		private Animator MirrorGrp2;
		private Animator MirrorGrp3;
		private Animator MirrorGrp4;
		private Animator MirrorGrp5;
		private Animator MirrorGrp6;
		private Animator MirrorGrp7;
		private Animator MirrorGrp8;
		private Animator MirrorGrp9;
		private Animator MirrorGrp10;
		private Animator MirrorGrp11;
		private Animator MirrorGrp12;
		private Animator MirrorGrp13;
		private Animator MirrorGrp14;
		private Animator MirrorGrp15;
		private Animator MirrorGrp16;
		private Animator MirrorGrp17;
		private Animator MirrorGrp18;
		private Animator MirrorGrp19;
		private Animator MirrorGrp20;


		private void Awake () {
			MirrorGrp1 = GameObject.Find("Mirror_G1").GetComponent<Animator>();
			MirrorGrp2 = GameObject.Find("Mirror_G2").GetComponent<Animator>();
			MirrorGrp3 = GameObject.Find("Mirror_G3").GetComponent<Animator>();
			MirrorGrp4 = GameObject.Find("Mirror_G4").GetComponent<Animator>();
			MirrorGrp5 = GameObject.Find("Mirror_G5").GetComponent<Animator>();
			MirrorGrp6 = GameObject.Find("Mirror_G6").GetComponent<Animator>();
			MirrorGrp7 = GameObject.Find("Mirror_G7").GetComponent<Animator>();
			MirrorGrp8 = GameObject.Find("Mirror_G8").GetComponent<Animator>();
			MirrorGrp9 = GameObject.Find("Mirror_G9").GetComponent<Animator>();
			MirrorGrp10 = GameObject.Find("Mirror_G10").GetComponent<Animator>();
			MirrorGrp11 = GameObject.Find("Mirror_G11").GetComponent<Animator>();
			MirrorGrp12 = GameObject.Find("Mirror_G12").GetComponent<Animator>();
			MirrorGrp13 = GameObject.Find("Mirror_G13").GetComponent<Animator>();
			MirrorGrp14 = GameObject.Find("Mirror_G14").GetComponent<Animator>();
			MirrorGrp15 = GameObject.Find("Mirror_G15").GetComponent<Animator>();
			MirrorGrp16 = GameObject.Find("Mirror_G16").GetComponent<Animator>();
			MirrorGrp17 = GameObject.Find("Mirror_G17").GetComponent<Animator>();
			MirrorGrp18 = GameObject.Find("Mirror_G18").GetComponent<Animator>();
			MirrorGrp19 = GameObject.Find("Mirror_G19").GetComponent<Animator>();
			MirrorGrp20 = GameObject.Find("Mirror_G20").GetComponent<Animator>();
		}

		private void Start () {

		}

		private void OnEnable () {
			HideReflecteurs.SetActive(false);
		}

		private void OnDisable () {
			HideReflecteurs.SetActive(true);
		}

		//called by the slider event
		public void AddAMirrorGrp(){
			int i = (int)_slider.value;

			if(i > 0){
				isUp1 = true;
			}
			if(i < 1){
				isUp1 = false;
			}
			if(i > 1){
				isUp2 = true;
			}
			if(i < 2){
				isUp2 = false;
			}
			if(i > 2){
				isUp3 = true;
			}
			if(i < 3){
				isUp3 = false;
			}
			if(i > 3){
				isUp4 = true;
			}
			if(i < 4){
				isUp4 = false;
			}
			if(i > 4){
				isUp5 = true;
			}
			if(i < 5){
				isUp5 = false;
			}
			if(i > 5){
				isUp6 = true;
			}
			if(i < 6){
				isUp6 = false;
			}
			if(i > 6){
				isUp7 = true;
			}
			if(i < 7){
				isUp7 = false;
			}
			if(i > 7){
				isUp8 = true;
			}
			if(i < 8){
				isUp8 = false;
			}
			if(i > 8){
				isUp9 = true;
			}
			if(i < 9){
				isUp9 = false;
			}
			if(i > 9){
				isUp10 = true;
			}
			if(i < 10){
				isUp10 = false;
			}
			if(i > 10){
				isUp11 = true;
			}
			if(i < 11){
				isUp11 = false;
			}
			if(i > 11){
				isUp12 = true;
			}
			if(i < 12){
				isUp12 = false;
			}
			if(i > 12){
				isUp13 = true;
			}
			if(i < 13){
				isUp13 = false;
			}
			if(i > 13){
				isUp14 = true;
			}
			if(i < 14){
				isUp14 = false;
			}
			if(i > 14){
				isUp15 = true;
			}
			if(i < 15){
				isUp15 = false;
			}
			if(i > 15){
				isUp16 = true;
			}
			if(i < 16){
				isUp16 = false;
			}
			if(i > 16){
				isUp17 = true;
			}
			if(i < 17){
				isUp17 = false;
			}
			if(i > 17){
				isUp18 = true;
			}
			if(i < 18){
				isUp18 = false;
			}
			if(i > 18){
				isUp19 = true;
			}
			if(i < 19){
				isUp19 = false;
			}
			if(i > 19){
				isUp20 = true;
			}
			if(i < 20){
				isUp20 = false;
			}
		}
	}
}
