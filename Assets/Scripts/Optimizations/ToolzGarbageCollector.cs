﻿using UnityEngine;
using System.Collections;

public static class ToolzGarbageCollector {


	/*This strategy works best for games where allocations (and therefore collections) are relatively infrequent
	 * and can be handled during pauses in gameplay. It is useful for the heap to be as large as possible without
	 * being so large as to get your app killed by the OS due to low system memory. However, the Mono runtime avoids
	 * expanding the heap automatically if at all possible. You can expand the heap manually by preallocating some placeholder
	 * space during startup (ie, you instantiate a “useless” object that is allocated purely for its effect on the memory manager):
	 */
	//For small heap with fast and frequent garbage collection, to be used in a loop
	public static void FrequentGC(){
		if (Time.frameCount % 30 == 0){
			System.GC.Collect();
		}
	}

	/*This strategy works best for games where allocations (and therefore collections) are relatively infrequent
	 * and can be handled during pauses in gameplay. It is useful for the heap to be as large as possible without being
	 * so large as to get your app killed by the OS due to low system memory. However, the Mono runtime avoids expanding
	 * the heap automatically if at all possible. You can expand the heap manually by preallocating some placeholder
	 * space during startup (ie, you instantiate a “useless” object that is allocated purely for its effect on the memory manager):
	 */
	//For large heap with slow but infrequent garbage collection
	public static void InfrequentGC(){
		var tmp = new System.Object[1024];

		// make allocations in smaller blocks to avoid them to be treated in a special way, which is designed for large blocks
		for (int i = 0; i < 1024; i++)
			tmp[i] = new byte[1024];

		// release reference
		tmp = null;
	}

	//free use
	public static void CollectTheGC(){
		System.GC.Collect();
	}
}
