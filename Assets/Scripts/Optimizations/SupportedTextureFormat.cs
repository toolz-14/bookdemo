﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SupportedTextureFormat : MonoBehaviour {

	public Text displayText;

	private TextureFormat format;

	private void Start () {
		Invoke ("CheckAndDisplayFormat", 0.5f);
	}

	private void CheckAndDisplayFormat(){

		format = TextureFormat.Alpha8;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "Alpha8";
		}

		format = TextureFormat.ARGB32;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ARGB32";
		}

		format = TextureFormat.ARGB4444;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ARGB4444";
		}

		format = TextureFormat.ASTC_RGBA_10x10;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGBA_10x10";
		}

		format = TextureFormat.ASTC_RGBA_12x12;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGBA_12x12";
		}

		format = TextureFormat.ASTC_RGBA_4x4;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGBA_4x4";
		}

		format = TextureFormat.ASTC_RGBA_5x5;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGBA_5x5";
		}

		format = TextureFormat.ASTC_RGBA_6x6;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGBA_6x6";
		}

		format = TextureFormat.ASTC_RGBA_8x8;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGBA_8x8";
		}

		format = TextureFormat.ASTC_RGB_10x10;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGB_10x10";
		}

		format = TextureFormat.ASTC_RGB_12x12;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGB_12x12";
		}

		format = TextureFormat.ASTC_RGB_4x4;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGB_4x4";
		}

		format = TextureFormat.ASTC_RGB_5x5;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGB_5x5";
		}

		format = TextureFormat.ASTC_RGB_6x6;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGB_6x6";
		}

		format = TextureFormat.ASTC_RGB_8x8;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ASTC_RGB_8x8";
		}

		format = TextureFormat.ETC_RGB4;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ATC_RGB4";
		}

		format = TextureFormat.ETC2_RGBA8;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ATC_RGBA8";
		}

		format = TextureFormat.BC4;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "BC4";
		}

		format = TextureFormat.BC5;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "BC5";
		}

		format = TextureFormat.BC6H;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "BC6H";
		}

		format = TextureFormat.BC7;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "BC7";
		}

		format = TextureFormat.BGRA32;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "BGRA32";
		}

		format = TextureFormat.DXT1;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "DXT1";
		}
		#if UNITY_STANDALONE
		format = TextureFormat.DXT1Crunched;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "DXT1Crunched";
		}
		#endif
		format = TextureFormat.DXT5;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "DXT5";
		}
		#if UNITY_STANDALONE
		format = TextureFormat.DXT5Crunched;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "DXT5Crunched";
		}
		#endif
		format = TextureFormat.EAC_R;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "EAC_R";
		}

		format = TextureFormat.EAC_RG;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "EAC_RG";
		}

		format = TextureFormat.EAC_RG_SIGNED;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "EAC_";
		}

		format = TextureFormat.EAC_R_SIGNED;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "EAC_R_SIGNED";
		}

		format = TextureFormat.ETC2_RGB;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ETC2_RGB";
		}

		format = TextureFormat.ETC2_RGBA1;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ETC2_RGBA1";
		}

		format = TextureFormat.ETC2_RGBA8;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ETC2_RGBA8";
		}

		format = TextureFormat.ETC_RGB4;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ETC_RGB4";
		}

		format = TextureFormat.ETC_RGB4_3DS;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ETC_RGB4_3DS";
		}

		format = TextureFormat.ETC_RGBA8_3DS;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "ETC_RGBA8_3DS";
		}

		format = TextureFormat.PVRTC_RGB2;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "PVRTC_RGB2";
		}

		format = TextureFormat.PVRTC_RGB4;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "PVRTC_RGB4";
		}

		format = TextureFormat.PVRTC_RGBA2;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "PVRTC_RGBA2";
		}

		format = TextureFormat.PVRTC_RGBA4;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "PVRTC_RGBA4";
		}

		format = TextureFormat.R16;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "R16";
		}

		format = TextureFormat.R8;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "R8";
		}

		format = TextureFormat.RFloat;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RFloat";
		}

		format = TextureFormat.RG16;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RG16";
		}

		format = TextureFormat.RGB24;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGB24";
		}

		format = TextureFormat.RGB565;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGB565";
		}

		format = TextureFormat.RGB9e5Float;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGB9e5Float";
		}

		format = TextureFormat.RGBA32;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGBA32";
		}

		format = TextureFormat.RGBA4444;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGBA4444";
		}

		format = TextureFormat.RGBAFloat;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGBAFloat";
		}

		format = TextureFormat.RGBAHalf;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGBAHalf";
		}

		format = TextureFormat.RGFloat;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGFloat";
		}

		format = TextureFormat.RGHalf;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RGHalf";
		}

		format = TextureFormat.RHalf;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "RHalf";
		}

		format = TextureFormat.YUY2;
		if(SystemInfo.SupportsTextureFormat(format)){
			displayText.text += "//n supports  " + "YUY2";
		}
	}
}
