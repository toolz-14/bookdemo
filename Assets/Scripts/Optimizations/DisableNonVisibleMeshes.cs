﻿using UnityEngine;
using System.Collections;

namespace Toolz.Optimizations {
    [AddComponentMenu("Toolz/Optimizations/DisableNonVisibleMeshes")]
    public class DisableNonVisibleMeshes : MonoBehaviour {

        private Renderer[] _renderers;
		private int _inc;
		private Plane[] _cameraPlanes;

		private void Awake() {
            _renderers = FindObjectsOfType<Renderer>();

        }

		private void Start() {
            //InvokeRepeating("fakeUpdate", 1f, 0.1f);
        }

		private void fakeUpdate() {
			_cameraPlanes = GeometryUtility.CalculateFrustumPlanes(Toolz.Managers.LevelManager.ActiveCamera);
			_inc = 0;
            foreach (Renderer currentRenderer in _renderers) {
				if(GeometryUtility.TestPlanesAABB(_cameraPlanes, currentRenderer.bounds)) {
                    currentRenderer.enabled = true;
					_inc++;
                }
                else {
                    currentRenderer.enabled = false;
                }
            }
        }

		private void Update(){
			fakeUpdate ();
		}
    }
}