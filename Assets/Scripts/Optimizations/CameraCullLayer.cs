﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCullLayer : MonoBehaviour {

	public float CullDistance = 15f;
	public int LayerNumber = 10;
	public bool CullIsSpherical = true;

	private void Start () {
		Camera camera = GetComponent<Camera>();
		float[] distances = new float[32];
		distances[LayerNumber] = CullDistance;
		camera.layerCullSpherical = CullIsSpherical;
		camera.layerCullDistances = distances; 
	}
}
