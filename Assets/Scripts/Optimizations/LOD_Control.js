﻿#pragma strict

//Set the # of distance ranges to be "one less" than the amount of LODS you assign. Farthest LOD will always be automatic after farthest distance.
public var CameraHolder : Transform;
public var distanceRanges : float[];
public var lodModels : GameObject[]; //assign the 3d meshes from highest poly count to lowest poly count

private var current = -2;

function Start () {

	for (var i = 0; i < lodModels.Length; i ++)
	{
		lodModels[i].SetActive(false);
	}
}

function Update () {

//	var d = Vector3.Distance(CameraHolder.transform.position, transform.position);
	var d = CameraHolder.transform.position.y - transform.position.y;
//	Debug.Log("distance = "+d);
	var level = -1;

	for (var i = 0; i < distanceRanges.Length; i++)
	{
		if (d < distanceRanges[i])
		{
			level = i;
			i = distanceRanges.Length;
		}
	}
	if (level == -1)
	{
		level = distanceRanges.Length;
	}
	if (current != level)
	{
		ChangeLOD(level);
	}

}

function ChangeLOD(level)
{
	lodModels[level].SetActive(true);

	if( current >= 0)
	{
	lodModels[current].SetActive(false);

	}

	current = level;


}




