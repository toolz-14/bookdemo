﻿using Boomlagoon.JSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolz.WebServices
{
    public struct BookProjectData
    {
        public int id;
        public int idCompany;
        public int creator;
        public string title;
        public string address;
        public double longitude;
        public double latitude;
        public string description;
        public string mission;
        public string estimate;
        public string stake;
        public string approach;
        public string solution;
        public string tags;
        public int status;
        public string completionDate;
        public string clientsName;
        public string imageCover;
        public List<string> images;
        public List<string> models;
        public List<string> videos;
        public List<int> categories;
        public string cameraPosRot;
        public Dictionary<string, string> fpsCamera;

        public static BookProjectData Parse(JSONObject resProject)
        {
            BookProjectData bookData = new BookProjectData();
            bookData.id = int.Parse(resProject["id"].Str);
            bookData.idCompany = int.Parse(resProject["idCompany"].Str);
            bookData.creator = int.Parse(resProject["creator"].Str);
            bookData.title = resProject["title"].Str;
            bookData.address = resProject["address"].Str;
            bookData.longitude = double.Parse(resProject["longitude"].Str);
            bookData.latitude = double.Parse(resProject["latitude"].Str);
            bookData.description = resProject["description"].Str;
            bookData.mission = resProject["mission"].Str;
            bookData.estimate = resProject["estimate"].Str;
            bookData.stake = resProject["stake"].Str;
            bookData.approach = resProject["approach"].Str;
            bookData.solution = resProject["solution"].Str;
            bookData.tags = resProject["tags"].Str;
            bookData.status = int.Parse(resProject["status"].Str);
            bookData.imageCover = resProject["imageCover"].Str;
            bookData.completionDate = resProject["completionDate"].Str;
            bookData.clientsName = resProject["clientsName"].Str;
            bookData.cameraPosRot = resProject["cameraPosRot"].Str;

            JSONArray fpsCameraArray = resProject["fpscamera"].Array;
            bookData.fpsCamera = new Dictionary<string, string>();
            foreach (JSONValue value in fpsCameraArray)
            {
                JSONObject fpsCameraValues = value.Obj;
                bookData.fpsCamera.Add(fpsCameraValues["hotspotPos"].Str, fpsCameraValues["cameraPosRot"].Str);
                //Debug.Log(fpsCameraValues["cameraPosRot"].Str + " "+ fpsCameraValues["cameraPosRot"].Str);
            }

            JSONArray imagesArray = resProject["images"].Array;
            bookData.images = new List<string>();
            foreach (JSONValue value in imagesArray)
            {
                bookData.images.Add(value.Str);
            }

            JSONArray modelsArray = resProject["models"].Array;
            bookData.models = new List<string>();
            foreach (JSONValue value in modelsArray)
            {
                bookData.models.Add(value.Str);
            }

            JSONArray categories = resProject["categories"].Array;
            bookData.categories = new List<int>();
            foreach (JSONValue value in categories)
            {
                bookData.categories.Add(int.Parse(value.Str));
            }

            JSONArray videosArray = resProject["videos"].Array;
            bookData.videos = new List<string>();
            foreach (JSONValue value in videosArray)
            {
                bookData.videos.Add(value.Str);
            }
            
            return bookData;
        }

        public override string ToString()
        {
            //foreach (string value in images)
            //{
            //    Debug.Log(value);
            //}
            return id + " " + idCompany + " " + creator + " " + title + " " + longitude + " " + latitude + " " + description + " " + mission
                + " " + estimate + " " + stake + " " + approach + " " + solution + " " + tags + " " + status + " " + imageCover /*+ " " + (model == null) + " " + (video == null)*/;
        }
    }
}