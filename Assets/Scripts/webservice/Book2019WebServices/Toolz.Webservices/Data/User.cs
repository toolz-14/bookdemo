﻿using UnityEngine;
using System.Collections;

namespace Toolz.WebServices {

    public class User : MonoBehaviour {

        public int idUser = -1;
        public string login;
        public string firstname;
        public string userName;
        public int userStatus;

        public string mail;
        public string password;

        //marc
        public bool isLoggedIn;

        public const int COMPANYID = 1;

        void Awake() {
            idUser = -1;
        }

		void OnEnable(){
			idUser = -1;
		}

        public bool IsLoggedIn { get{ return idUser != -1;} }

		public void LogOff() {
			idUser = -1;
			login = "";
			firstname = "";
			userName = "";
			mail = "";
			password = "";
            userStatus = -1;

        }
    }
}