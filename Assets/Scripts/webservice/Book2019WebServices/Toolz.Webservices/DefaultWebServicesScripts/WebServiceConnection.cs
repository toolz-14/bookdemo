﻿using UnityEngine;
using System.Collections;

namespace Toolz.WebServices
{
    /**
     * This script is intended to be placed on a WebServices GameObject
     * containing all WebServices to be used by the application.
     * At startup this script automatically feed the WebServices with 
     * its information.
     */
    //    [ExecuteInEditMode]
    public class WebServiceConnection : MonoBehaviour
    {

        public static WebServiceConnection instance = null;
        public string localTestURL;
        public string preprodURL;
        public string prodURL;

        public Mode ConnectionMode;

        public enum Mode { Local, Preprod, Prod };

        /*
    private void Awake(){
        //Check if instance already exists
        if (instance == null){

            //if not, set instance to this
            instance = this;
        }

        //If instance already exists and it's not this:
        else if (instance != this){

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
        */

        void Start()
        {
            WebService[] webServices = gameObject.GetComponents<WebService>();

            foreach (WebService service in webServices)
            {
                if (ConnectionMode == Mode.Local)
                    service.RemoteURL = localTestURL;
                else if (ConnectionMode == Mode.Preprod)
                {
                    service.RemoteURL = preprodURL;
                }
                else if (ConnectionMode == Mode.Prod)
                {
                    service.RemoteURL = prodURL;
                }
            }
        }
    }
}