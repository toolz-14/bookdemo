﻿using Boomlagoon.JSON;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;

namespace Toolz.WebServices
{
    public class PatchBookProject : WebService
    {
        // INPUT
        [HideInInspector]
        public int projectID;
        //[HideInInspector]
        //public string title;
        //[HideInInspector]
        //public string address;
        //[HideInInspector]
        //public string description;
        //[HideInInspector]
        //public string mission;
        //[HideInInspector]
        //public string estimation;
        //[HideInInspector]
        //public string stake;

        // raw INPUT
        JSONObject data;

        public void FillFirstPageForm(int _projectID,string title, string address, string description, string mission, string estimation, string stake)
        {
            projectID = _projectID;
            data = new JSONObject();

            data.Add("title", title);
            data.Add("address", address);
            data.Add("description", description);
            data.Add("mission", mission);
            data.Add("estimate", estimation);
            data.Add("stake", stake);
        }

        public void FillSecondPageForm(int _projectID, string approach, string solution, string clientsName, string completionDate, string tags)
        {
            projectID = _projectID;
            data = new JSONObject();

            data.Add("approach", approach);
            data.Add("solution", solution);
            data.Add("clientsName", clientsName);
            data.Add("completionDate", completionDate);
            data.Add("tags", tags);

        }

        public void FillThirdPageForm(List<int> catIds, int _projectID, double longitude, double latitude, string imageCover)
        {
            projectID = _projectID;
            data = new JSONObject();
            JSONArray catArray = new JSONArray();

            if (catIds.Count > 0)
            {
                foreach (int cat in catIds)
                {
                    catArray.Add(cat);
                }
                data.Add("categories", catArray);
            }

            data.Add("longitude", longitude);
            data.Add("latitude", latitude);
            if(imageCover!=null&& imageCover!="")
                data.Add("imageCover", imageCover);
        }

        public void FillThirdPageForm(List<int> catIds, int _projectID, double longitude, double latitude, string imageCover, int projectStatus)
        {
            projectID = _projectID;
            data = new JSONObject();
            JSONArray catArray = new JSONArray();

            foreach (int cat in catIds)
            {
                catArray.Add(cat);
            }

            data.Add("categories", catArray);
            data.Add("longitude", longitude);
            data.Add("latitude", latitude);
            data.Add("imageCover", imageCover);
            data.Add("status", projectStatus);
        }

        protected override void FillData()
        {
            httpMethod = Method.PATCH;
            
            scriptURI = "projects/" + projectID;
            data.Add("idCompany", User.COMPANYID);
            User user = GetComponent<User>();
            data.Add("creator", user.idUser);

            //Debug.Log(RemoteURL + scriptURI + data.ToString());

            bodyData = data.ToString();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("status " + status + " result" + result);

            if (status.Equals((long)WebService.Status.OK))
            {
                JSONObject resProject = JSONObject.Parse(result);
            }
            //projectID = int.Parse(resProject["id"].Str);
            base.OnEndLoading(status, result);
        }
    }
}