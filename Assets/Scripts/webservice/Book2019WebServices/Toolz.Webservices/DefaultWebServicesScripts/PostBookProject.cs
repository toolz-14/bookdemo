﻿using Boomlagoon.JSON;
using UnityEngine;

namespace Toolz.WebServices
{
    public class PostBookProject : WebService
    {
        // OUTPUT
        [HideInInspector]
        public int newprojectID; 

        protected override void FillData()
        {
            httpMethod = Method.POST;
            uploadMethod = UploadMethod.BODYRAW;

            newprojectID = 0;
            scriptURI = "projects/";
            JSONObject data = new JSONObject();
            data.Add("idCompany", User.COMPANYID);
            User user = GetComponent<User>();
            data.Add("creator", user.idUser);
            data.Add("title", "");
            data.Add("address", "");
            data.Add("longitude", 0);
            data.Add("latitude", 0);
            data.Add("description", "");
            data.Add("mission", "");
            data.Add("estimate", "");
            data.Add("stake", "");
            data.Add("approach", "");
            data.Add("solution", "");
            data.Add("tags", "");
            data.Add("status", 0);
            data.Add("imageCover", "");
            data.Add("completionDate", "");
            data.Add("clientsName", "");
            data.Add("cameraPosRot", "");

            bodyData = data.ToString();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("OnEndLoading " +  status + "result : " + result);
            if (status.Equals((long)WebService.Status.CREATED))
            {
                JSONObject resProject = JSONObject.Parse(result);
                newprojectID = int.Parse(resProject["id"].Str);
            }
            else
                Debug.Log("status " + status + " result" + result);

            base.OnEndLoading(status, result);
        }
    }
}