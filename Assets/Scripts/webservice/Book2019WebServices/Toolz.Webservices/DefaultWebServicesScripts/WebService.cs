#define LOCALTEST

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.Networking;

namespace Toolz.WebServices
{
    /**
     * Client side of Webservices.
     * Server side there are PHP scripts returning
     * result this way:
     * A formatted string: "STATUS;".$statusNumber.";".$content
     * $content is a JSON formatted string
     */
    [ExecuteInEditMode]
    public class WebService : MonoBehaviour
    {

        [HideInInspector]
        public string RemoteURL;

        public static WebServiceConnection WebServiceConnection;


        //------------------| STATUS |
        /**
         * A WebService connects to a script page which 
         * returns a HTTP Status code used to determine 
         * the status of the request. 
         */
        public enum Status
        {
            NO_CONNECTION = 0, // Not a HTTP code
            OK = 200, // HTTP Code 200
            CREATED = 201,// HTTP Code 201
            NO_CONTENT = 204,// HTTP Code 204
            BAD_REQUEST = 400,// HTTP Code 400
            UNAUTHORIZED = 401,// HTTP Code 401, Missing authentication headers
            FORBIDDEN = 403,// HTTP Code 403
            SERVICE_NOT_FOUND = 404, // HTTP Code 404
            PRECONDITION_FAILED = 412,// HTTP Code 412
            INTERNAL_SERVER_ERROR = 500, // HTTP Code 500
            UNKNOWN_ERROR = 666 // Not a HTTP code
        }

        public enum UploadMethod
        {
            UNDEFINED,
            SIMPLEFORM, // simple form with Query Params
            BODYRAW,
            MULTIPARTFORM
        }

        //------------------| METHOD |
        public enum Method
        {
            UNDEFINED,
            GET,
            POST,
            PATCH,
            DELETE
        }

        //------------------| OUTPUT |
        public enum OutputData
        {
            STRING,
            BYTE,
            TEXTURE
        }

        //------------------| EVENTS |

        /** All WebService's events have this signature */
        public delegate void WebServiceEvent(long status, string message);

        /** This event is called when the request has been completed without error.
         * Note: Status.NO_CONTENT is not considered as an error. */
        public event WebServiceEvent onComplete;

        /** This event is called when the request has not been completed because of an error.
         * Note: Status.NO_CONTENT is not considered as an error. */
        public event WebServiceEvent onError;

        //------------------| FIELDS |
        /** Time (in seconds) to wait before cancelling the operation **/
        public int timeOut = 3;
        //TimeOut related vars
        private float timeOutCounter = 0.0f;
        private float startTimeOutTime;

        protected string scriptURI = "";
        //protected WWW w;
        protected UnityWebRequest webRequest;
        protected Method httpMethod;
        protected UploadMethod uploadMethod;
        protected WWWForm form;
        public static byte[] byteToUpload;
        protected List<IMultipartFormSection> formData;
        protected string bodyData;
        
        // OUTput
        protected OutputData outputFormat;
        protected byte[] byteDownloaded;
        protected Texture2D t;

        protected bool startLoading = true;
        protected bool started = false;

        private bool useBasicAuth = false;
        private string basicAuthCredentials;

        //TO BE MOVED IN A UTIL CLASS
        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();  //or use SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString().ToLower();
        }
        //TO BE MOVED IN A UTIL CLASS END

        public virtual void Dispose()
        {
            StopWebService();
            onComplete = null;
            onError = null;
        }

        //------------------| METHODS |
        /** Lance le Webservice*/
        public void UseWebService()
        {
            StopWebService();
            form = new WWWForm();
            t = null;
            FillData();

            if (string.IsNullOrEmpty(scriptURI))
            {
                Debug.Log("UseWebService, Pas d'URL pour le script");
                return;
            }

            if (httpMethod == Method.UNDEFINED)
            {
                Debug.Log("UseWebService, Pas de methode définie");
                return;
            }

            if (!startLoading) return;
            StartCoroutine("LoadPage");
        }

        public void SetAuthentication(string username, string password)
        {
            useBasicAuth = true;
            basicAuthCredentials = ConvertBasicAuthCredentials(username, GetHashString(GetHashString(password)));
            //Debug.Log(username + GetHashString(GetHashString(password)));
        }

        public void SetDefaultAuthentication()
        {
            useBasicAuth = true;
            basicAuthCredentials = ConvertBasicAuthCredentials("apiKEY", "apiKEY");
        }

        string ConvertBasicAuthCredentials(string username, string password)
        {
            string auth = username + ":" + password;
            auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
            auth = "Basic " + auth;
            return auth;
        } 

        public IEnumerator LoadPage()
        {
            //Debug.Log("LoadPage: ");
            started = true;
            string url = RemoteURL + scriptURI;

            // Setting Webrequest
            switch (httpMethod)
            {
                case Method.GET:
                    webRequest = UnityWebRequest.Get(url);
                    break;
                case Method.POST:
                    switch (uploadMethod)
                    {
                        case UploadMethod.UNDEFINED:
                            Debug.LogError("UploadMethod, Pas de methode définie");
                            break;
                        case UploadMethod.BODYRAW:
                            if (bodyData != null)
                            {
                                webRequest = new UnityWebRequest(url, UnityWebRequest.kHttpVerbPOST);
                                byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyData);
                                webRequest.uploadHandler = new UploadHandlerRaw(bodyRaw);
                                webRequest.SetRequestHeader("Content-Type", "application/json");
                            }
                            else
                                Debug.LogError("bodyData is invalid");
                            break;
                        case UploadMethod.MULTIPARTFORM:
                            if (byteToUpload==null|| byteToUpload != null &&byteToUpload.Length==0) //formData == null || formData != null && formData.Count == 0)
                            {
                                Debug.LogError("byteToUpload is invalid");
                                //Debug.LogError("formSections is invalid");
                            }
                            else
                            {
                                /*
                                //generate a unique boundary
                                byte[] boundary = UnityWebRequest.GenerateBoundary();
                                //serialize form fields into byte[] => requires a bounday to put in between fields
                                byte[] formSections = UnityWebRequest.SerializeFormSections(formData, boundary);

                                webRequest = new UnityWebRequest(url);
                                webRequest.uploadHandler = new UploadHandlerRaw(formSections);

                                //note: adding the boundary to the uploadHandler.contentType is essential!
                                //It won't have the boundary otherwise and won't know how to split up the fields;
                                //also it must be encoded to a string otherwise it just says prints the type,
                                //'byte[]', which is not what you want

                                webRequest.uploadHandler.contentType = "multipart/form-data; boundary=\"" + System.Text.Encoding.UTF8.GetString(boundary) + "\"";
                                webRequest.downloadHandler = new DownloadHandlerBuffer();
                                webRequest.method = "POST";
                                webRequest.SetRequestHeader("Content-Type", "multipart/form-data");
                                */

                                // This is a hack so that we can upload multipart form
                                // SEE : https://answers.unity.com/questions/1354080/unitywebrequestpost-and-multipartform-data-not-for.html
                                // https://forum.unity.com/threads/unitywebrequest-post-multipart-form-data-doesnt-append-files-to-itself.627916/

                                WWWForm form = new WWWForm();
                                form.AddBinaryData("upload", byteToUpload, "image.png", "image/png");
                                webRequest = UnityWebRequest.Post(url, form);

                                // doesn't work with multipartfileform
                                // webRequest = UnityWebRequest.Post(url, formData);
                            }
                            break;
                        case UploadMethod.SIMPLEFORM:
                            if (form.data != null && form.data.Length > 0)
                                webRequest = UnityWebRequest.Post(url, form);
                            else
                                Debug.LogError("form.data is invalid");
                            break;
                    }
                    break;
                case Method.PATCH:
                    if (bodyData != null)
                    {
                        webRequest = new UnityWebRequest(url, "PATCH");
                        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyData);
                        webRequest.uploadHandler = new UploadHandlerRaw(bodyRaw);
                        webRequest.SetRequestHeader("Content-Type", "application/json");
                    }
                    else if (form.data != null && form.data.Length > 0)
                        webRequest = UnityWebRequest.Post(url, form);
                    break;
                case Method.DELETE:
                    webRequest = UnityWebRequest.Delete(url);
                    break; 
            }

            webRequest.timeout = timeOut;

            if (useBasicAuth)
                webRequest.SetRequestHeader("AUTHORIZATION", basicAuthCredentials);

            if (WebServiceConnection == null)
            {
                WebServiceConnection = gameObject.GetComponent<WebServiceConnection>();
                //Debug.Log("Connection Mode : " + WebServiceConnection.ConnectionMode);
            }

            webRequest.downloadHandler = new DownloadHandlerBuffer();
            DownloadHandlerTexture textureDownloaded = new DownloadHandlerTexture(true);

            switch (outputFormat)
            {
                case OutputData.TEXTURE:
                    webRequest.downloadHandler = textureDownloaded;
                    break;
                default:
                    break; 
            }

            yield return webRequest.SendWebRequest();

            if (webRequest.error != null)
            {
                //Debug.Log("WebService www ERROR : " + webRequest.error + TimeOut);
            }
            string message;

            switch (outputFormat)
            {
                case OutputData.BYTE:
                    byteDownloaded = webRequest.downloadHandler.data;
                    break;
                case OutputData.TEXTURE:
                    if (!textureDownloaded.isDone)
                        t = null;
                    else
                        t = textureDownloaded.texture;
                    //Sprite s = Sprite.Create(t, new Rect(0, 0, t.width, t.height),
                    //                         Vector2.zero, 1f);
                    //_img.sprite = s;
                    break;
                default:
                    break;
            }

            //Debug.Log("Post request complete!" + " Response Code: " + webRequest.responseCode);
            message = webRequest.downloadHandler.text;
            OnEndLoading(webRequest.responseCode, message);

            //Debug.Log("Status: " + status);
            //clear our form in game
            webRequest.Dispose();
            webRequest = null;
            useBasicAuth = false;

            StopCoroutine("LoadPage");
        }


        public void StopWebService()
        {
            if (webRequest != null)
            {
                started = false;
                StopCoroutine("LoadPage");
                webRequest.Dispose();
                webRequest = null;

                form = null;
            }
        }

        //--------- TO OVERRIDE FUNCTIONS

        /** Pour ajouter les variables POST à envoyer au script, à overrider
         * ex : form.AddField("project", "Photovoltaic");
         * dans le script PHP la variable sera récupérée comme suit :
         * $varPHP = $_POST['project'];
         * */
        virtual protected void FillData()
        {
            // to override
        }

        /** Appelée lorsque le webservice a fini de s'éxecuter
         * Permet de récupérer le message envoyé par le PHP et envoie l'évenement onComplete
         (ou onError en cas d'erreur) */
        virtual protected void OnEndLoading(long status, string result)
        {
            started = false;
            if (onError != null && (status != (long)Status.OK && status != (long)Status.NO_CONTENT && status != (long)Status.CREATED))
            {
                // Debug.Log ("WebService, if OnEndLoading, Onerror and Status: " + status.ToString());
                onError(status, result);
            }
            else if (onComplete != null)
            {
                // Debug.Log ("WebService, else if OnEndLoading, OnComplete and Status: " + status.ToString());
                onComplete(status, result);
            }
        }
    }
}