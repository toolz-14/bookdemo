﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class ApplicationLogin : WebService {

        public enum ApplicationLoginStatus { NONE = -1, USER_NEED_VALIDATION = 0, USER_OK = 1, USER_BANNED = 2, USER_ADMIN = 3};

        //Inputs
        [HideInInspector]
        public string userName;
        // public string userMail;

        //Outputs
        public ApplicationLoginStatus userStatus;

        public ApplicationLogin()
            : base()
        {
            scriptURI = "users/check-access";
            userStatus = ApplicationLoginStatus.NONE;
        }

        //This is a GET request
        protected override void FillData() {
            httpMethod = Method.POST;
            uploadMethod = UploadMethod.BODYRAW;
            JSONObject data = new JSONObject();
            data.Add("username", userName);
            data.Add("deviceId", SystemInfo.deviceUniqueIdentifier);

            bodyData = data.ToString();
            SetDefaultAuthentication();
        }

        protected override void OnEndLoading(long status, string result) {
            //Debug.Log("status " + status + " result" + result);
            if (status == (long)Status.OK) {
                JSONObject userStatusJSON = JSONObject.Parse(result);
                userStatus = (ApplicationLoginStatus)userStatusJSON["status"].Number;
            }
            base.OnEndLoading(status, result);
        }
    }
}