﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

namespace Toolz.WebServices
{
    public class GetBookProjectsbyCompanyID : WebService
    {
        [HideInInspector]
        public int idProject;
        [HideInInspector]
        public bool adminAccess;
        public List<BookProjectData> projectDataList;

        protected override void FillData()
        {
            httpMethod = Method.GET;
                scriptURI = "companies/" + User.COMPANYID + "/projects";

            User user = GetComponent<User>();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("status " + status + " result" + result);
            if (status == (long)Status.OK)
            {
                projectDataList = new List<BookProjectData>();
                JSONArray projectList = JSONArray.Parse(result);
                foreach (JSONValue project in projectList)
                {
                    BookProjectData bookProjectData = BookProjectData.Parse(project.Obj);
                    projectDataList.Add(bookProjectData);
                    //Debug.Log(bookProjectData.ToString());
                }
            }
            base.OnEndLoading(status, result);
        }
    }
}