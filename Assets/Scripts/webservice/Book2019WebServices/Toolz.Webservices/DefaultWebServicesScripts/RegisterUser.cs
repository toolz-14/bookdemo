using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class RegisterUser : WebService {
        [HideInInspector]
        public string firstname;
        [HideInInspector]
        public string lastname;
        [HideInInspector]
        public string login;
        [HideInInspector]
        public string mail;
        [HideInInspector]
        public string password;

        // OUTPUT
        public int errorCode;
        
        //------------------| Construct |
        public RegisterUser()
            : base() {
            scriptURI = "users/register";
        }

        protected override void FillData()
        {
            httpMethod = Method.POST;
            uploadMethod = UploadMethod.BODYRAW;
            JSONObject data = new JSONObject();
            data.Add("firstName", firstname);
            data.Add("name", lastname);
            data.Add("login", login);
            data.Add("mail", mail);
            data.Add("password", WebService.GetHashString(password));
            data.Add("idCompany", User.COMPANYID);

            bodyData = data.ToString();
            SetDefaultAuthentication();
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("status " + status + " result" + result);
            if (status.Equals((long)WebService.Status.PRECONDITION_FAILED))
            {
                JSONObject userStatusJSON = JSONObject.Parse(result);
                errorCode = (int)userStatusJSON["error_code"].Number;
            }

            base.OnEndLoading(status, result);
        }
    }
}