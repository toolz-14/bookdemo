﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

namespace Toolz.WebServices
{
    public class DeleteBookProject : WebService
    {
        [HideInInspector]
        public int idProject = -1;

        protected override void FillData()
        {
            httpMethod = Method.DELETE;

            scriptURI = "projects/" + idProject;
            User user = GetComponent<User>();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("status " + status + " result" + result);
            base.OnEndLoading(status, result);
        }
    }
}