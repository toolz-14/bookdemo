﻿using Boomlagoon.JSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Toolz.WebServices
{
    public class PostImageProject : WebService
    {
        // INPUT
        [HideInInspector]
        public int newprojectID;
        [HideInInspector]
        public byte[] _imageToUpload;

        [HideInInspector]
        public string imageName;

        protected override void FillData()
        {
            scriptURI = "projects/" + newprojectID + "/upload/image";

            httpMethod = Method.POST;
            uploadMethod = UploadMethod.MULTIPARTFORM;
            byteToUpload = _imageToUpload;
            
            // doesn't work
            formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormFileSection("upload", _imageToUpload, "image.png", "image/png"));

            User user = GetComponent<User>();
            SetAuthentication(user.login, user.password);
        }
        
        protected override void OnEndLoading(long status, string result)
        {
            if (status.Equals((long)WebService.Status.OK))
            {
                JSONObject resProject = JSONObject.Parse(result);
                imageName = resProject["fileName"].Str;
            }
            base.OnEndLoading(status, result);
        }
    }
}