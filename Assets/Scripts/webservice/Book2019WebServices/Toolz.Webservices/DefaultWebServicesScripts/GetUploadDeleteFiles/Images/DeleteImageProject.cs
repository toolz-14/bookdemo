﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

namespace Toolz.WebServices
{
    public class DeleteImageProject : WebService
    {
        [HideInInspector]
        public int idProject = -1;
        [HideInInspector]
        public string imageName = "";

        protected override void FillData()
        {
            httpMethod = Method.DELETE;

            scriptURI = "projects/" + idProject + "/image/" + imageName;
            User user = GetComponent<User>();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            if (status.Equals((long)WebService.Status.OK))
            {

            }
            Debug.Log("status " + status + " result" + result);
            base.OnEndLoading(status, result);
        }
    }
}