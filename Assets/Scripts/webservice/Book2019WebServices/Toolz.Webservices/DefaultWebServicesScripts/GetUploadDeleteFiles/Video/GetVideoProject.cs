﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

namespace Toolz.WebServices
{
    public class GetVideoProject : WebService
    {
        // INPUT
        [HideInInspector]
        public int idProject;
        [HideInInspector]
        public string videoName;

        // OUTPUT
        //public byte[] image;
        [HideInInspector]
        public Sprite sprite;
        [SerializeField]
        private Sprite defaultSprite;

        protected override void FillData()
        {
            sprite = defaultSprite;
            httpMethod = Method.GET;
            outputFormat = OutputData.TEXTURE;
            scriptURI = "projects/" + idProject + "/videos/" + videoName;

            //Debug.Log(RemoteURL + scriptURI);
            User user = GetComponent<User>();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("status " + status + " result" + result);
            if (status == (long)Status.OK)
            {
                if (t != null)
                {
                    Sprite dlSprite = Sprite.Create(t, new Rect(0, 0, t.width, t.height), Vector2.zero);

                    if (dlSprite != null)
                    {
                        this.sprite = dlSprite;
                        this.sprite.name = videoName;
                    }
                }
            }
            base.OnEndLoading(status, result);
        }
    }
}