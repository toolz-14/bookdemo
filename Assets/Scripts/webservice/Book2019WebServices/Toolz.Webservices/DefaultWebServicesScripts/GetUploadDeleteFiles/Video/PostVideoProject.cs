﻿using Boomlagoon.JSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Toolz.WebServices
{
    public class PostVideoProject : WebService
    {
        // INPUT
        [HideInInspector]
        public int newprojectID;
        [HideInInspector]
        public byte[] _videoToUpload;

        protected override void FillData()
        {
            scriptURI = "projects/" + newprojectID + "/upload/video";

            httpMethod = Method.POST;
            uploadMethod = UploadMethod.MULTIPARTFORM;
            byteToUpload = _videoToUpload;

            // doesn't work
            formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormFileSection("upload", _videoToUpload, "video.mp4", "video/mp4"));

            User user = GetComponent<User>();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            base.OnEndLoading(status, result);
        }
    }
}