﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices {

    /**
     * There is no real login process as there is no connected mode.
     * This WebService only checks if the credentials (login+password)
     * given are correct.
     * If correct, return user information.
     * 
     **/
    public class UserLogin : WebService {
        //Input
        [HideInInspector]
        public string login;
        [HideInInspector]
        public string password;
        [HideInInspector]
        public string apiKey;

        //Output
        [HideInInspector]
        public string firstname;
        [HideInInspector]
        public string lastname;
        [HideInInspector]
        public string mail;
        [HideInInspector]
        public int userStatus;
        [HideInInspector]
        public int idUser;
        
        protected override void FillData()
        {
            httpMethod = Method.POST;
            uploadMethod = UploadMethod.BODYRAW;
            scriptURI = "users/login";
            JSONObject data = new JSONObject();
            data.Add("id", login);
            data.Add("password", WebService.GetHashString(password));

            bodyData = data.ToString();
            SetDefaultAuthentication();
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("status " + status + " result" + result);

            firstname = "";
            lastname = "";
            mail = "";
            userStatus = -1;
            idUser = -1;
			// Debug.Log ("Result : " + result);
            if (status == (long)Status.OK) {
                JSONObject resUser = JSONObject.Parse(result);
                idUser = int.Parse(resUser["id"].Str);
                firstname = resUser["firstname"].Str;
                lastname = resUser["name"].Str;
                login = resUser["login"].Str;
                mail = resUser["mail"].Str;
                userStatus = int.Parse(resUser["status"].Str);
            }

            base.OnEndLoading(status, result);
        }

    }
}