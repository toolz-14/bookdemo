﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

namespace Toolz.WebServices
{
    public class GetBookProject : WebService
    {

        [HideInInspector]
        public int idProject;
        public BookProjectData bookData;

        protected override void FillData()
        {
            scriptURI = "projects/" + idProject;
            User user = GetComponent<User>();
            SetAuthentication(user.login, user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("status " + status + " result" + result);
            if (status == (long)Status.OK)
            {
                JSONObject resProject = JSONObject.Parse(result);
                bookData = BookProjectData.Parse(resProject);
            }
            base.OnEndLoading(status, result);
        }
    }
}