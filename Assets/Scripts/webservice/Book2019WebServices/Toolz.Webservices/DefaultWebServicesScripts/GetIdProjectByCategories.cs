﻿using Boomlagoon.JSON;
using System.Collections.Generic;
using UnityEngine;

namespace Toolz.WebServices
{
    public class GetIdProjectByCategories : WebService
    {
        // INPUT
        [HideInInspector]
        public List<int> categoriesSelected;

        // OUTPUT
        [HideInInspector]
        public List<int> projectIds = new List<int>();

        protected override void FillData()
        {
            httpMethod = Method.POST;
            uploadMethod = UploadMethod.BODYRAW;

            //data.Add("idCompany", User.COMPANYID);
            scriptURI = "companies/" + User.COMPANYID + "/categories/projects";

            JSONObject data = new JSONObject();

            JSONArray catList = new JSONArray();
            foreach(int cat in categoriesSelected)
            {
                catList.Add(cat);
            }
            data.Add("categories", catList);

            //Debug.Log( RemoteURL +   scriptURI + " data " + data);

            User user = GetComponent<User>();
            bodyData = data.ToString();
            SetAuthentication(user.login, user.password);

            projectIds.Clear();
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("OnEndLoading " );
            if (status.Equals((long)WebService.Status.OK))
            {
                JSONArray idList = JSONArray.Parse(result);
                foreach (JSONValue idObject in idList)
                {
                    JSONObject value = idObject.Obj;
                    projectIds.Add(int.Parse(value["id"].Str));
                }
            }
            else
                Debug.Log("status " + status + " result" + result);

            base.OnEndLoading(status, result);
        }
    }
}