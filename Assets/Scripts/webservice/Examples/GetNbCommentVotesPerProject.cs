﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices {
    public class GetNbCommentVotesPerProject : WebService {

        public struct ProjectCommentsVotes {
            public int nbUp;
            public int nbDown;
        }

        //No inputs

        //Output
        //idProject => data
        public Dictionary<string, ProjectCommentsVotes> ProjectsCommentsVotes;

        // Use this for initialization
        void Start() {
            scriptURI = "vote/projects/nb-comment-votes";
            ProjectsCommentsVotes = new Dictionary<string, ProjectCommentsVotes>();
        }

        protected override void OnEndLoading(long status, string result) {
            ProjectsCommentsVotes.Clear();
            if (status == (long)Status.OK) {
                JSONArray array = JSONArray.Parse(result);
                foreach (JSONValue data in array) {
                    ProjectCommentsVotes res = new ProjectCommentsVotes();
                    string idProject = data.Obj["idProject"].Str;
                    res.nbUp = int.Parse(data.Obj["nbUp"].Str);
                    res.nbDown = int.Parse(data.Obj["nbDown"].Str);
                    ProjectsCommentsVotes.Add(idProject, res);
                }
            }
            base.OnEndLoading(status, result);
        }

    }
}