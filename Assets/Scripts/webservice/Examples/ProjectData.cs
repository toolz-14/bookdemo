﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toolz.WebServices {
    public struct ProjectData {
        public int IdUser;
        public string IdProject;
        public bool Applied;
        public int Investment;
        public Dictionary<string, float> Values;
    }

}
