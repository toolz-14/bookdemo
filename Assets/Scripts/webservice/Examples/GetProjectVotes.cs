﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class GetProjectVotes : WebService {

        //Input
        [HideInInspector]
        public string idProject;

        //Output
        [HideInInspector]
        public int nbUp;
        [HideInInspector]
        public int nbDown;
        
        // Use this for initialization
        void Start() {
            scriptURI += "vote/project/";
        }

        protected override void FillData() {
            scriptURI += "vote/project/" + idProject;
        }

        protected override void OnEndLoading(long status, string result) {
            nbUp = 0;
            nbDown = 0;
            if (status == (long)Status.OK) {
                JSONObject res = JSONObject.Parse(result);
                nbUp = int.Parse(res["nbUp"].Str);
                nbDown = int.Parse(res["nbDown"].Str);
            }
            base.OnEndLoading(status, result);
        }
    }
}