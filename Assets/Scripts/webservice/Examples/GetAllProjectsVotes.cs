﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices
{
    public class GetAllProjectsVotes : WebService
    {
        public struct Votes
        {
            public int nbUp;
            public int nbDown;
        }
        //No Input

        //Output
        public Dictionary<string, Votes> VotesPerProject;

        // Use this for initialization
        void Start()
        {
            scriptURI += "vote/project/all";
            VotesPerProject = new Dictionary<string, Votes>();
        }

        protected override void FillData()
        {
        }

        protected override void OnEndLoading(long status, string result)
        {
            VotesPerProject.Clear();
            if (status == (long)Status.OK)
            {
                JSONArray array = JSONArray.Parse(result);
                foreach (JSONValue value in array)
                {
                    string idProject = value.Obj["idProject"].Str;
                    int nbUp = int.Parse(value.Obj["nbUp"].Str);
                    int nbDown = int.Parse(value.Obj["nbDown"].Str);
                    Votes votes = new Votes();
                    votes.nbUp = nbUp;
                    votes.nbDown = nbDown;

                    VotesPerProject.Add(idProject, votes);
                }
            }
            base.OnEndLoading(status, result);
        }
    }
}