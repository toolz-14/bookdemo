﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices
{
    public class DeleteUserSave : WebService
    {

        //Output
        //public Dictionary<string, Votes> VotesPerProject;
        private User _user;

        [HideInInspector]
        public int SaveID;

        // Use this for initialization
        void Start()
        {
            scriptURI += "saves/delete/";
            _user = GetComponent<User>();
        }

        protected override void FillData()
        {
            scriptURI = scriptURI + SaveID + "?login=" + _user.login + "&password=" + WebService.GetHashString(_user.password);
            form.AddField("post", "1"); 
        }

        protected override void OnEndLoading(long status, string result)
        {
            base.OnEndLoading(status, result);
        }
    }
}