﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class PostUserSave : WebService {

        //Inputs
        [HideInInspector]
        public int projectID;

        [HideInInspector]
        public string data; 

        private User _user;
        

        // Use this for initialization
        void Start() {
            scriptURI += "saves/Users/";
            _user = GetComponent<User>();
        }

        protected override void FillData() {

            scriptURI = scriptURI + _user.idUser + "/Projects/" + projectID + "" + "?login=" + _user.login + "&password=" + WebService.GetHashString(_user.password); ;

            form.AddField("Data", data);
        }

        protected override void OnEndLoading(long status, string result) {
            if (status == (long)Status.OK) {
                JSONObject com = JSONObject.Parse(result);
            }
            base.OnEndLoading(status, result);
        }
    }
}