﻿using UnityEngine;
using System.Collections;

namespace Toolz.WebServices {
    public class PostUserProjectVote : WebService {

        //Inputs
        [HideInInspector]
        public string idProject;
        public int userVote;

        //No outputs

        void Start() {
            scriptURI += "vote/project";
        }

        protected override void FillData() {
            User user =  GetComponent<User>();
            form.AddField("idProject", idProject);
            form.AddField("userVote", userVote);
            form.AddField("idUser", user.idUser);
            //This service need to be connected
            form.AddField("login", user.login);
            form.AddField("password", WebService.GetHashString(user.password));
        }

        //Nothing to do here as the backend does not echo back any data

    }
}