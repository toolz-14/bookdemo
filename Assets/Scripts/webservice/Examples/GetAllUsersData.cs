﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices {
    public class GetAllUsersData : WebService {

        //No inputs

        //Output
        public Dictionary<string, ProjectData> ProjectsData;

        // Use this for initialization
        void Start() {
            scriptURI = "allUsersData";

            ProjectsData = new Dictionary<string, ProjectData>();
        }

        protected override void OnEndLoading(long status, string result) {
            ProjectsData.Clear();
            if (status == (long)Status.OK) {
                 /*
                 * Get a JSON Object 'array':
                 * array[idUser] = projects array
                 * projects array[idProject]['project_state'] = {projectState, totalCost}
                 * projects array[idProject]['project_settings'] = array[idVar, varValue]
                */
                JSONObject res = JSONObject.Parse(result);
            
                foreach (var userProjects in res)
                {
                    int userId = int.Parse(userProjects.Key);
                    foreach (var userProject in userProjects.Value.Obj)
                    {
                        ProjectData pdata = new ProjectData();
                        pdata.IdUser = userId;
                        pdata.IdProject = userProject.Key;

                        JSONObject project_state = userProject.Value.Obj["project_state"].Obj;
                        pdata.Applied = int.Parse(project_state["projectState"].Str) == 1;
                        pdata.Investment = int.Parse(project_state["totalCost"].Str);

                        //If project has attached variables
                        if (userProject.Value.Obj["project_settings"] != null)
                        {
                            JSONArray variables = userProject.Value.Obj["project_settings"].Array;
                            if (variables.Length > 0)
                                pdata.Values = new Dictionary<string, float>();
                            foreach (JSONValue variable in variables)
                            {
                                string idVar = variable.Obj["idVar"].Str;
                                float  ratio = float.Parse(variable.Obj["varRatio"].Str);
                                pdata.Values.Add(idVar, ratio);
                            }
                        }

                        ProjectsData.Add(pdata.IdProject, pdata);
                    }
                }
            }

            base.OnEndLoading(status, result);
        }
    }
}