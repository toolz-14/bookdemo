﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices {
    public class GetNbCommentsPerProject : WebService {

        //No Inputs

        //Outputs
        [HideInInspector]
        //idProject => nbComments
        public Dictionary<string, int> ProjectStats;

        // Use this for initialization
        void Start() {
            scriptURI = "discussion/stats/comment-number";
            ProjectStats = new Dictionary<string, int>();
        }

        protected override void OnEndLoading(long status, string result) {
            ProjectStats.Clear();
            if (status == (long)Status.OK) {
                JSONArray projects = JSONArray.Parse(result);
                foreach (JSONValue val in projects) {
                    string idProject = val.Obj["idProject"].Str;
                    int nbComments = int.Parse(val.Obj["nbComments"].Str);

                    ProjectStats.Add(idProject, nbComments);
                }
            }
            base.OnEndLoading(status, result);
        }
    }
}