﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices
{
    public class GetUserSave : WebService
    {

        //Output
        //public Dictionary<string, Votes> VotesPerProject;
        [HideInInspector]
        public Toolz.Flats.Save Save;

        private User _user;

        [HideInInspector]
        public int SaveID;

        // Use this for initialization
        void Start()
        {
            scriptURI += "saves/";
            _user = GetComponent<User>();
        }

        protected override void FillData()
        {
            scriptURI = scriptURI + SaveID + "?login=" + _user.login + "&password=" + WebService.GetHashString(_user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            // Saves.Clear();

            if (status == (long)Status.OK)
            {
               // result = "{ \"Items\": " + result + "}";
                Save = JsonUtility.FromJson<Toolz.Flats.Save>(result);
            }
            base.OnEndLoading(status, result);
        }
    }
}