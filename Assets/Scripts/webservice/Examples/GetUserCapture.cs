﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;
using Toolz.Flats;

namespace Toolz.WebServices
{
    public class GetUserCapture : WebService
    {

        //Output
        //public Dictionary<string, Votes> VotesPerProject;
        private User _user;

        [HideInInspector]
        public int projectID;

        [HideInInspector]
        public Capture capture;

        [HideInInspector]
        public int ID;

        // Use this for initialization
        void Start()
        {
            scriptURI += "captures/";
            _user = GetComponent<User>();
        }

        protected override void FillData()
        {
            scriptURI = scriptURI + ID + "?login=" + _user.login + "&password=" + WebService.GetHashString(_user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            // Saves.Clear();

            if (status == (long)Status.OK)
            {

                capture = JsonUtility.FromJson<Toolz.Flats.Capture>(result);
            }
            base.OnEndLoading(status, result);
        }
    }
}