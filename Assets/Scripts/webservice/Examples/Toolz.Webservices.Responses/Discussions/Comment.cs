﻿using Boomlagoon.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toolz.WebServices {
    public struct Comment {
        public uint idComment;
        public int idAuthor;
        public string login;
        public string firstname;
        public string name;
        public DateTime publishedTime;
        public string text;
        public int nbUp;
        public int nbDown;
        public int userVote;
        public int nbValidatedResponses;
        public int nbUnvalidatedResponses;
        public int status;

        public static Comment ParseComment(JSONObject comment, bool getUserVote) {
            Comment c = new Comment();
            c.idComment = uint.Parse(comment["idComment"].Str);
            c.idAuthor = int.Parse(comment["idAuthor"].Str);
            c.login = comment["login"].Str;
            c.firstname = comment["firstname"].Str;
            c.name = comment["name"].Str;
            c.publishedTime = Convert.ToDateTime(comment["publishedTime"].Str);
            c.text = comment["text"].Str;
            c.status = int.Parse(comment["status"].Str);
            c.nbUp = int.Parse(comment["nbUp"].Str);
            c.nbDown = int.Parse(comment["nbDown"].Str);
            if (getUserVote)
                c.userVote = int.Parse(comment["userVote"].Str);
            c.nbValidatedResponses = int.Parse(comment["nbValidatedResponses"].Str);
            c.nbUnvalidatedResponses = int.Parse(comment["nbUnvalidatedResponses"].Str);

            return c;
        }
    }
}
