﻿using Boomlagoon.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toolz.WebServices {
    public struct Reply {
        public uint idOriginalComment;
        public int idReply;
        public int idReplyAuthor;
        public string firstname;
        public string name;
        public uint idOriginalCommentAuthor;
        public DateTime publishedTime;
        public int idAnsweredRep;
        public string text;
        public int nbUp;
        public int nbDown;
        public int userVote;
        public int status;
        public string originalCommentAuthorFullName;

        public static Reply ParseReply(JSONObject reply, bool getUserVote)
        {
            Reply result = new Reply();

            result.idOriginalComment = uint.Parse(reply["idOriginalComment"].Str);
            result.idReply = int.Parse(reply["idReply"].Str);
            result.idReplyAuthor = int.Parse(reply["idReplyAuthor"].Str);
            result.firstname = reply["firstname"].Str;
            result.name = reply["name"].Str;
            result.idOriginalCommentAuthor = uint.Parse(reply["idOriginalCommentAuthor"].Str);
            result.publishedTime = Convert.ToDateTime(reply["publishedTime"].Str);
            //Can be null
            if (reply["idAnsweredRep"] != null && reply["idAnsweredRep"].Str != null)
                result.idAnsweredRep = int.Parse(reply["idAnsweredRep"].Str);
            else result.idAnsweredRep = 0;
            result.text = reply["text"].Str;
            result.status = int.Parse(reply["status"].Str);
            result.nbUp = int.Parse(reply["nbUp"].Str);
            result.nbDown = int.Parse(reply["nbDown"].Str);
            if (getUserVote)
                result.userVote = int.Parse(reply["userVote"].Str);
            result.originalCommentAuthorFullName = reply["originalCommentAuthorFullName"].Str;

            return result;
        }
    }

}
