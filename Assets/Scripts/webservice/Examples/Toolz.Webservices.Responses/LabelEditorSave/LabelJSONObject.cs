﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    public struct LabelJSONObject
    {
        public List<LabelObject> labelsList;
        public string fulllLoadMasterString;

        public LabelJSONObject Parse(JSONArray jsonArray)
        {
            LabelJSONObject data = new LabelJSONObject();
            data.labelsList = new List<LabelObject>();
            data.fulllLoadMasterString = "";

            foreach (JSONValue val in jsonArray)
            {
                int id = int.Parse(val.Obj["id"].Str);
                int phase = int.Parse(val.Obj["phase"].Str);
                int category = int.Parse(val.Obj["category"].Str);
                int idAuthor = int.Parse(val.Obj["idAuthor"].Str);
                int status = int.Parse(val.Obj["status"].Str);
                DateTime publishedTime = Convert.ToDateTime(val.Obj["publishedTime"].Str);
                string saveMasterString = val.Obj["saveMasterString"].Str;
                string authorName = val.Obj["firstname"].Str + " " + val.Obj["name"].Str;
                int nbComments = int.Parse(val.Obj["nbUnvalidatedComments"].Str);
                int nbReplies = int.Parse(val.Obj["nbUnvalidatedResponses"].Str);

                LabelObject temp = new LabelObject(id, phase, category, saveMasterString, idAuthor, status, publishedTime, authorName, nbComments, nbReplies);
                data.labelsList.Add(temp);
                data.fulllLoadMasterString += saveMasterString + "_";
            }

            //remove last '_' to make load split easier
            data.fulllLoadMasterString.Substring(0, data.fulllLoadMasterString.Length - 1);

            return data;
        }

    }

    public struct LabelObject
    {
        public int id;
        public int phase;
        public int category;
        public int idAuthor;
        public int nbReplies;
        public string authorName;
        public int status;
        public string saveMasterString;
        public DateTime publishedTime;
        public int nbComments;

        public LabelObject(int _id, int _phase, int _category, string _saveMasterString, int _idAuthor, int _status, DateTime _publishedTime, string _authorName, int _nbComments, int _nbReplies)
        {
            id = _id;
            phase = _phase;
            category = _category;
            saveMasterString = _saveMasterString;
            idAuthor = _idAuthor;
            status = _status;
            publishedTime = _publishedTime;
            authorName = _authorName;
            nbComments = _nbComments;
            nbReplies = _nbReplies;
        }
    }
}