﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices
{
    public class GetUserSaves : WebService
    {

        //Output
        //public Dictionary<string, Votes> VotesPerProject;
        [HideInInspector]
        public Toolz.Flats.Save[] Saves;

        private User _user;

        public int projectID;

        // Use this for initialization
        void Start()
        {
            scriptURI += "saves/Users/";
            _user = GetComponent<User>();
        }

        protected override void FillData()
        {
            scriptURI = scriptURI + _user.idUser + "/Projects/" + projectID + "" + "?login=" + _user.login + "&password=" + WebService.GetHashString(_user.password); ;
        }

        protected override void OnEndLoading(long status, string result)
        {
            // Saves.Clear();

            if (status == (long)Status.OK)
            {
                result = "{ \"Items\": " + result + "}";
                Saves = JsonHelper.FromJson<Toolz.Flats.Save>(result);
            }
            base.OnEndLoading(status, result);
        }
    }
}