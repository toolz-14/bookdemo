﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class GetAverageInvestmentPerProject : WebService {

        //No input

        //Output
        public Dictionary<string, float> ProjectsAverageInvestment;

        // Use this for initialization
        void Start() {
            scriptURI = "user/average-investment/";
            ProjectsAverageInvestment = new Dictionary<string, float>();
        }

        protected override void OnEndLoading(long status, string result) {
            ProjectsAverageInvestment.Clear();
            if (status == (long)Status.OK) {
                JSONArray results = JSONArray.Parse(result);
                foreach (JSONValue val in results) {
                    string idProject = val.Obj["idProject"].Str;
                    float averageInvestment = float.Parse(val.Obj["averageInvestment"].Str);

                    ProjectsAverageInvestment.Add(idProject, averageInvestment);
                }
            }
            base.OnEndLoading(status, result);
        }
    }
}