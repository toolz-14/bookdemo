﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    public struct SurveyVisitingReasonStats
    {
        private Dictionary<string, float> result;

        public Dictionary<string, float> ParseSurvey(JSONObject jsonObject)
        {
            result = new Dictionary<string, float>();
            int max = 0;

            JSONArray jsonArray = jsonObject["resultArray"].Array;

            foreach (JSONValue JSONValue in jsonArray)
            {
                string result2 = JSONValue.ToString();
                JSONObject json = JSONObject.Parse(result2);
                max = max + int.Parse(json["effectif"].Str);
            }

            foreach (JSONValue JSONValue in jsonArray)
            {
                string result2 = JSONValue.ToString();
                JSONObject json = JSONObject.Parse(result2);
                float value = (float)int.Parse(json["effectif"].Str);
                result.Add(json["raisonPlace"].Str, value / max);

            }
            return result;
        }
    }
}