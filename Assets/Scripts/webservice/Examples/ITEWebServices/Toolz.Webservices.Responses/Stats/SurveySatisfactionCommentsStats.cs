﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    public struct SurveySatisfactionCommentsStats
    {
        private string[] result;

        public string[] ParseSurvey(JSONObject jsonObject)
        {
            JSONArray jsonArray = jsonObject["commentaires"].Array;
            result = new string[jsonArray.Length];

            for (int i=0; i< jsonArray.Length; i++) {
                result[i] = jsonArray[i].Str;
            }

            return result;
        }
    }
}