﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    /// <summary>
    /// Représente l'objet contenant le nombre d'hommes et de femmes qui ont répondu au questionnaire
    /// </summary>
    /// 
    public struct SurveyGender
    {
        public int numberOfWomen;
        public int numberOfMen;
        public float total;
        public float proportionOfWomen;
        public float proportionOfMen;

        public SurveyGender ParseSurveyGender(JSONObject surveyGender)
        {
            SurveyGender data = new SurveyGender();
            data.numberOfWomen = 0;
            data.numberOfMen = 0;
            if (surveyGender["femme"]!=null){
                data.numberOfWomen = int.Parse(surveyGender["femme"].Str);
            }
            if (surveyGender["homme"] != null)
            {
                data.numberOfMen = int.Parse(surveyGender["homme"].Str);
            }
            data.total = data.numberOfWomen + data.numberOfMen;
            data.proportionOfWomen = (data.numberOfWomen)/ data.total;
            data.proportionOfMen = (data.numberOfMen)/ data.total;

            return data;
        }

    }
}
