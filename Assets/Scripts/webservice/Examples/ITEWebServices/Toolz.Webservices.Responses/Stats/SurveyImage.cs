﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    /// <summary>
    /// Représente l'objet contenant le nombre d'hommes et de femmes qui ont répondu au questionnaire
    /// </summary>
    /// 
    public struct SurveyImage
    {
        private Dictionary<string, int> imagesVoteValues;

        public Dictionary<string, int> ParseSurveyImage(JSONObject surveyImagesValues)
        {
            imagesVoteValues = new Dictionary<string, int>();
            JSONArray surveyImagesJSONArray = surveyImagesValues["Images"].Array;
            JSONArray surveyValuesJSONArray = surveyImagesValues["Values"].Array;

            for (int i = 0; i < surveyImagesJSONArray.Length; i++)
            {
                imagesVoteValues.Add(surveyImagesJSONArray[i].Str, int.Parse(surveyValuesJSONArray[i].Str));
            }
            return imagesVoteValues;
            // return imagesVoteValues;
        }
    }
}
