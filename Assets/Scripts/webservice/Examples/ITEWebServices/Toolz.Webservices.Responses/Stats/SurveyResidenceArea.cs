﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    /// <summary>
    /// Représente l'objet contenant le nombre d'hommes et de femmes qui ont répondu au questionnaire
    /// </summary>
    /// 
    public struct SurveyResidenceArea
    {
        public int[] resultsArray;

        public int[] ParseSurveyResidenceArea(JSONObject surveyResidenceArea)
        {
            resultsArray = new int[7];
            JSONArray surveyResidenceAreaArray = surveyResidenceArea["PopulationSecteur"].Array;
            
            for (int i =0; i<surveyResidenceAreaArray.Length; i++) {
                resultsArray[i] = int.Parse(surveyResidenceAreaArray[i].Str);
            }
            return resultsArray;
        }

    }
}
