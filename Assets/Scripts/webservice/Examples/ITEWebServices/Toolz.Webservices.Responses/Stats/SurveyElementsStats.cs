﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    /// <summary>
    /// Représente l'objet contenant le nombre d'hommes et de femmes qui ont répondu au questionnaire
    /// </summary>
    /// 
    public struct GetElementsStats
    {
        private int[] result;

        public int[] ParseSurveyElementsStats(JSONObject jsonObject)
        {
            result = new int[10];
            JSONArray surveyElementStatsArray = jsonObject["ElementsAjout"].Array;
            for (int i = 0; i < surveyElementStatsArray.Length; i++)
            {
                result[i] = int.Parse(surveyElementStatsArray[i].Str);
            }
            return result;
        }
    }
}