﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    /// <summary>
    /// Représente l'objet contenant le nombre d'hommes et de femmes qui ont répondu au questionnaire
    /// </summary>
    /// 
    public struct SurveySocioCat
    {
        private int[] socioCatValues;
        private float[] socioCatValuesToPercent;

        public float[] ParseSurveySocio(JSONObject socioCatStatsObject)
        {
            socioCatValues = new int[11];
            socioCatValuesToPercent = new float[11];
            int total = 0;
            JSONArray surveySocioJSONArray = socioCatStatsObject["CatSocioEffectif"].Array;

            for (int i = 0; i < socioCatValues.Length; i++)
            {
                socioCatValues[i] = int.Parse(surveySocioJSONArray[i].Str);
                total += socioCatValues[i];
            }

            for (int i = 0; i < socioCatValues.Length; i++)
            {
                if (total == 0)
                {
                    socioCatValuesToPercent[i] = 0f;
                }
                else
                {
                    socioCatValuesToPercent[i] = (float)socioCatValues[i]/ (float)total;
                }
            }
            return socioCatValuesToPercent;
        }
    }
}
