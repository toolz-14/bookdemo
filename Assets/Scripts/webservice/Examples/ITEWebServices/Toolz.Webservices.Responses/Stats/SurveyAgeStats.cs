﻿using System;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices.Responses
{
    /// <summary>
    /// Représente l'objet contenant le nombre d'hommes et de femmes qui ont répondu au questionnaire
    /// </summary>
    /// 
    public struct SurveyAgeStats
    {
        private int[] women;
        private int[] men;
        private Dictionary<int, int[]> result;

        public Dictionary<int, int[]> ParseSurveySocio(JSONObject ageStatsObject)
        {
            women = new int[7];
            men = new int[7];
            result = new Dictionary<int, int[]>();


            JSONArray surveyWomenarray = ageStatsObject["Femme"].Array;
            JSONArray surveyMenarray = ageStatsObject["Homme"].Array;

            for (int i = 0; i < surveyWomenarray.Length; i++)
            {
                women[i] = (int)surveyWomenarray[i].Number;
                men[i] = (int)surveyMenarray[i].Number;
            }

            result.Add(0, men);
            result.Add(1, women);
            
            return result;
        }
    }
}