﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
namespace Toolz.WebServices
{
    public class DeleteUserSurveys : WebService
    {
        //Input
        [HideInInspector]
        public string deviceID;
        
        // Use this for initialization
        void Start()
        {
            scriptURI += "saves/survey/deleteByUID";
        }

        protected override void FillData()
        {
            form.AddField("deviceID", deviceID);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("DeleteSurvey Intance : " + this.GetInstanceID());
            base.OnEndLoading(status, result);
        }
    }
}