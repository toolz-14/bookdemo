﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class PostUserSurvey : WebService
    {
        //Inputs
        [HideInInspector]
        public string deviceID;
        [HideInInspector]
        public string frequencePlace;
        [HideInInspector]
        public string raisonPlace;
        [HideInInspector]
        public string mot1;
        [HideInInspector]
        public string mot2;
        [HideInInspector]
        public string satisfaction;
        [HideInInspector]
        public string raisonSatisfaction;
        [HideInInspector]
        public string satisfactionElement;
        // Question 4 fields
        [HideInInspector]
        public int q4_1;
        [HideInInspector]
        public int q4_2;
        [HideInInspector]
        public int q4_3;
        [HideInInspector]
        public int q4_4;
        [HideInInspector]
        public int q4_5;
        [HideInInspector]
        public int q4_6;
        [HideInInspector]
        public int q4_7;
        [HideInInspector]
        public int q4_8;
        [HideInInspector]
        public int q4_9;
        [HideInInspector]
        public int q4_10;
        [HideInInspector]
        public int q4_11;
        [HideInInspector]
        public string q4_autre;

        [HideInInspector]
        public string image;
        [HideInInspector]
        public int sexe;
        [HideInInspector]
        public int age;
        [HideInInspector]
        public string prenom;
        [HideInInspector]
        public string nom;
        [HideInInspector]
        public string mail;
        [HideInInspector]
        public string association;
        [HideInInspector]
        public int residenceSecteur;
        [HideInInspector]
        public int residenceQuartier;
        [HideInInspector]
        public int catSocioPro;
        [HideInInspector]
        public int interviewOK;
        [HideInInspector]
        public string dateTime;


        // Use this for initialization
        void Start()
        {
            scriptURI += "saves/survey";
        }

        protected override void FillData()
        {
            form.AddField("deviceID", deviceID);
            form.AddField("frequencePlace", frequencePlace);
            form.AddField("raisonPlace", raisonPlace);
            form.AddField("mot1", mot1);
            form.AddField("mot2", mot2);
            form.AddField("satisfaction", satisfaction);
            form.AddField("raisonSatisfaction", raisonSatisfaction);
            form.AddField("satisfactionElement", satisfactionElement);

            form.AddField("q4_1", q4_1);
            form.AddField("q4_2", q4_2);
            form.AddField("q4_3", q4_3);
            form.AddField("q4_4", q4_4);
            form.AddField("q4_5", q4_5);
            form.AddField("q4_6", q4_6);
            form.AddField("q4_7", q4_7);
            form.AddField("q4_8", q4_8);
            form.AddField("q4_9", q4_9);
            form.AddField("q4_10", q4_10);
            form.AddField("q4_11", q4_11);
            form.AddField("q4_autre", q4_autre);

            form.AddField("image", image);
            form.AddField("sexe", sexe);
            form.AddField("age", age);
            form.AddField("prenom", prenom);
            form.AddField("nom", nom);
            form.AddField("mail", mail);
            form.AddField("association", association);
            form.AddField("residenceSecteur", residenceSecteur);
            form.AddField("residenceQuartier", residenceQuartier);
            form.AddField("catSocioPro", catSocioPro);
            form.AddField("interviewOK", interviewOK);
            form.AddField("dateTime", dateTime);
        }

        protected override void OnEndLoading(long status, string result)
        {
            // Debug.Log("PostSurvey Intance : " + this.GetInstanceID());
            base.OnEndLoading(status, result);
        }
    }
}