﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetVisitingReasonStats : WebService
    {
        public Dictionary<string, float> elementsValues;
        public Toolz.WebServices.Responses.SurveyVisitingReasonStats surveyObject;

        protected override void FillData()
        {
            scriptURI = "stats/visitingReason";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading result : " + result);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                elementsValues = surveyObject.ParseSurvey(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}

