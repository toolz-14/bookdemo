﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetSatisfactionsStats : WebService
    {
        public Dictionary<string, float> elementsValues;
        public Toolz.WebServices.Responses.SurveySatisfactionStats surveyObject;

        protected override void FillData()
        {
            scriptURI = "stats/satisfactions";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading result : " + result);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                elementsValues = surveyObject.ParseSurvey(json);
                
            }
            base.OnEndLoading(status, result);
        }
    }
}
