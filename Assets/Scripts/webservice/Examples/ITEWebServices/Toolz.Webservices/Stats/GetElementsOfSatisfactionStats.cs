﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetElementsOfSatisfactionStats : WebService
    {
        private string url;

        public Dictionary<string, float> elementsValues;
        public Toolz.WebServices.Responses.SurveyElementOfSatisfaction surveyElementsStats;

        protected override void FillData()
        {
            scriptURI = "stats/satisfaction/elements";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading result : " + result);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                elementsValues = surveyElementsStats.ParseSurveyElementOfSatisfaction(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}

