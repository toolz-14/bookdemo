﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetSocioCatStats : WebService
    {
        private string url;

        public float[] socioCatValues;
        public Toolz.WebServices.Responses.SurveySocioCat surveySocioCat;
        
        protected override void FillData()
        {
            scriptURI = "stats/catSocioPro";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading status : " + status);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                socioCatValues = surveySocioCat.ParseSurveySocio(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}
