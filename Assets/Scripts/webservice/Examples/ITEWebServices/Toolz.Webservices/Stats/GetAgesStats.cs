﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetAgesStats : WebService
    {
        private string url;
        
        public Dictionary<int, int[]> agesDictionnary; // 0 : men, 1 women
        public Toolz.WebServices.Responses.SurveyAgeStats surveyAgesObject;

        // Use this for initialization
        protected override void FillData()
        {
            scriptURI = "stats/agesByPortion";
        }
        protected override void OnEndLoading(long status, string result)

        {
            //Debug.Log("On end loading result : " + result);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                agesDictionnary = surveyAgesObject.ParseSurveySocio(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}