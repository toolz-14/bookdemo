﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetGenderStats : WebService
    {
        public Toolz.WebServices.Responses.SurveyGender surveyGender;

		protected override void FillData() {
			scriptURI = "stats/gender";
		}

        protected override void OnEndLoading(long status, string result)
        {
            if (status == (long)Status.OK)
            {
                JSONObject com = JSONObject.Parse(result);
                surveyGender = surveyGender.ParseSurveyGender(com);
                surveyGender.ToString();
            }
            base.OnEndLoading(status, result);
        }
    }
}
