﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetResidenceAreaStats : WebService
    {
        private string url;

        public int[] resultsArray;
        public Toolz.WebServices.Responses.SurveyResidenceArea surveyResidenceArea;

        // Use this for initialization
        protected override void FillData()
        {
            scriptURI = "stats/residence/area";
        }

        protected override void OnEndLoading(long status, string result)
        {
            if (status == (long)Status.OK)
            {
                JSONObject com = JSONObject.Parse(result);
                resultsArray = surveyResidenceArea.ParseSurveyResidenceArea(com);
            }
            base.OnEndLoading(status, result);
        }
    }
}