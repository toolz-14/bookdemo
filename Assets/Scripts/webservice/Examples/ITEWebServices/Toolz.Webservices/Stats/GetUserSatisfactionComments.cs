﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetUserSatisfactionComments : WebService
    {
        public string[] elementsValues;
        public string statisfactionCategory;
        public Toolz.WebServices.Responses.SurveySatisfactionCommentsStats surveyObject;

        protected override void FillData()
        {
            scriptURI = "stats/satisfaction/comments/" + statisfactionCategory;
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading result : " + result);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                elementsValues = surveyObject.ParseSurvey(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}

