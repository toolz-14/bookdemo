﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetPlaceVisitingFrequencyStats : WebService
    {
        public Dictionary<string, float> elementsValues;
        public Toolz.WebServices.Responses.SurveyPlaceVisitingFrequencyStats surveyVisitingStats;

        protected override void FillData()
        {
            scriptURI = "stats/userFrequency";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading result : " + result);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                elementsValues = surveyVisitingStats.ParseSurvey(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}

