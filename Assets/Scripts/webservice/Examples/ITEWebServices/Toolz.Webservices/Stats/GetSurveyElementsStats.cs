﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetSurveyElementsStats : WebService
    {
        private string url;

        public int[] elementsValues;
        public Toolz.WebServices.Responses.GetElementsStats surveyElementsStats;

        protected override void FillData()
        {
            scriptURI = "stats/ajoutElements";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading status : " + status);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                elementsValues = surveyElementsStats.ParseSurveyElementsStats(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}
