﻿using UnityEngine;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetImagesStats : WebService
    {
        private string url;

        public Dictionary<string, int> imagesVoteValues;
        public Toolz.WebServices.Responses.SurveyImage surveyImage;

        // Use this for initialization
        protected override void FillData()
        {
            scriptURI = "stats/images";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("On end loading status : " + status);
            if (status == (long)Status.OK)
            {
                JSONObject json = JSONObject.Parse(result);
                imagesVoteValues = surveyImage.ParseSurveyImage(json);
            }
            base.OnEndLoading(status, result);
        }
    }
}