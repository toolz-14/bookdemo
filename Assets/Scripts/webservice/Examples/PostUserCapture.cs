﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;
using System;
using Toolz.Flats;

namespace Toolz.WebServices {
    public class PostUserCapture : WebService {

        
        //Inputs
        [HideInInspector]
        public int projectID;

        [HideInInspector]
        public List<Capture> Captures;

        public byte[] byteArray;

        private User _user;
        

        // Use this for initialization
        void Start() {
            scriptURI += "captures/Users/";
            _user = GetComponent<User>();
        }

        protected override void FillData() {

            scriptURI = scriptURI + _user.idUser + "/Projects/" + projectID + "?login=" + _user.login + "&password=" + WebService.GetHashString(_user.password); ;
            

            if(byteArray != null)
                form.AddBinaryData("capture", byteArray, "screenShot.png", "image/png");

        }

        protected override void OnEndLoading(long status, string result) {
            if (status == (long)Status.OK) {
                JSONObject com = JSONObject.Parse(result);
            }
            base.OnEndLoading(status, result);
        }
    }
}