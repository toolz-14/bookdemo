﻿using UnityEngine;
using System.Collections;

namespace Toolz.WebServices {
    public class PostUserFollowProject : WebService {

        //Inputs
        [HideInInspector]
        public string idProject;
        [HideInInspector]
        public bool wantsToFollow;

        //No output

        protected override void FillData()
        {
            scriptURI = "project/follow";

            User user = GetComponent<User>();
            form.AddField("idUser", user.idUser);
            form.AddField("idProject", idProject);
            form.AddField("wantsToFollow", wantsToFollow ? "True" : "False");

            //User need to be logged in
            form.AddField("login", user.login);
            form.AddField("password", WebService.GetHashString(user.password));
        }

        //Nothing to do here as the backend does not echo back any data
    }
}