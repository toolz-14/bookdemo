﻿using Boomlagoon.JSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolz.WebServices
{
    public class PostUserReplyVote : WebService
    {
        [HideInInspector]
        public uint idReply;
        [HideInInspector]
        public int userVote;

        //Outputs
        [HideInInspector]
        public int nbUp;
        [HideInInspector]
        public int nbDown;
        
        protected override void FillData()
        {
            scriptURI = "vote/reply";

            User user = GetComponent<User>();
            form.AddField("idUser", user.idUser);
            form.AddField("idReply", idReply.ToString());
            form.AddField("userVote", userVote);

            //Need to be connected
            form.AddField("login", user.login);
            form.AddField("password", WebService.GetHashString(user.password));
        }

        protected override void OnEndLoading(long status, string result)
        {
            Debug.Log(result);
            JSONObject res = JSONObject.Parse(result);
            nbUp = int.Parse(res["nbUp"].Str);
            nbDown = int.Parse(res["nbDown"].Str);

            base.OnEndLoading(status, result);
        }
    }
}