﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class PostOnlineAdmin : WebService
    {
        //Output
        [HideInInspector]
        public bool isOnline;

        protected override void FillData()
        {
            scriptURI = "admin/onlineAdmin";
            int idUser = SaveLoadManager.LoadUserID();
            form.AddField("idUser", idUser);

            if (isOnline)
            {
                form.AddField("isOnline", 1);
            }
            else
            {
                form.AddField("isOnline", 0);
            }
        }

        protected override void OnEndLoading(long status, string result)
        {
            base.OnEndLoading(status, result);
        }
    }
}