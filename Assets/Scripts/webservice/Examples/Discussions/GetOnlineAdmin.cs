﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetOnlineAdmin : WebService
    {
        //Output
        [HideInInspector]
        public int nbOnlineAdmin;

        protected override void FillData()
        {
            scriptURI = "admin/onlineAdmin";
        }

        protected override void OnEndLoading(long status, string result)
        {

            // Debug.Log ("Result : " + result);
            if (status == (long)Status.OK)
            {
                nbOnlineAdmin = int.Parse(result);
            }

            base.OnEndLoading(status, result);
        }
    }
}