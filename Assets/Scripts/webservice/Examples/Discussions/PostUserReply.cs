﻿using UnityEngine;
using System.Collections;
using System;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class PostUserReply : WebService {

        //Inputs
        [HideInInspector]
        public int idComment;
        [HideInInspector]
        public string text;
        [HideInInspector]
        public int idAnsweredReply;
        [HideInInspector]
        public int idReply;
        [HideInInspector]
        public int idAuthor;
        [HideInInspector]
        public bool isDeleted;
        [HideInInspector]
        public int replyStatus;

        [HideInInspector]
        public int request_status; // 0 = adding, 1 = delete, 2 = update, 3 = update status by admin


        //Outputs
        [HideInInspector]
        public Reply reply;
        [HideInInspector]
        public int nbReplies;

        // Use this for initialization
        void Start() {
            scriptURI = "discussions/reply";
            idAuthor = 0;
            idReply = 0;
        }

        protected override void FillData()
        {
            User user = GetComponent<User>();
            if (request_status != 3)
            {
                form.AddField("idComment", idComment);
                if (idAuthor == 0)
                    form.AddField("idUser", user.idUser);
                else
                    form.AddField("idUser", idAuthor);

                form.AddField("status", replyStatus);
                form.AddField("text", text);
                form.AddField("idReply", idReply);
                form.AddField("idAnsweredReply", idAnsweredReply);
                if (isDeleted)
                    form.AddField("isDeleted", 1);
                else
                    form.AddField("isDeleted", 0);
            }
            else
            {
                scriptURI = "discussions/updateStatus/reply";
                form.AddField("idComment", idComment);
                form.AddField("idReply", idReply);
                replyStatus = 1;
                form.AddField("status", replyStatus);

                if (isDeleted)
                    form.AddField("isDeleted", 1);
                else
                    form.AddField("isDeleted", 0);
            }

            //Need to be logged in
            form.AddField("login", user.login);
            form.AddField("password", WebService.GetHashString(user.password));
        }

        protected override void OnEndLoading(long status, string result)
        {
            if (status == (long)Status.OK)
            {
                string[] splittedString = result.Split('%');
                if (!splittedString[0].Equals("reply_updated") && !splittedString[0].Equals("nothing_to_update") && !splittedString[0].Equals("reply_has_been_deleted"))
                {
                    JSONObject obj = JSONObject.Parse(result);
                    reply = Reply.ParseReply(obj["reply"].Obj, false);
                    nbReplies = int.Parse(obj["nbReplies"].Str);
                }
                else
                {
                    //Debug.Log("nbReplies : " + splittedString[1]);
                    nbReplies = int.Parse(splittedString[1]);
                }
            }
            idAuthor = 0;
            idReply = 0;
            base.OnEndLoading(status, result);
        }

    }
}