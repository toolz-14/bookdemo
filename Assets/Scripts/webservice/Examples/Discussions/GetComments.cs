﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class GetComments : WebService {

        //Inputs
        [HideInInspector]
        public int idProject;

        //Output
        [HideInInspector]
        public List<Comment> Comments;
        
        protected override void FillData()
        {
            scriptURI = "discussions/projectComments/";

            User user = GetComponent<User>(); // Get user
            int idUser = user.idUser;
            string login = user.login;
            string password = user.password;
            //Detect if in connected mode or not.
            if (idUser != -1) { // CHECK IF USER IS LOGGED 
                scriptURI = "discussions/projectComments/" + idUser + "/" + idProject;
            }
            else {
                scriptURI = "discussions/projectComments/" + idProject;
            }
            //Debug.Log("scriptURL " + scriptURL);
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log("result " + result);
            Comments = new List<Comment>();
            Comments.Clear();
            if (status == (long)Status.OK) {
                JSONArray comments = JSONArray.Parse(result);
                foreach (JSONValue comment in comments) {
                    Comment c = Comment.ParseComment(comment.Obj, GetComponent<User>().idUser != -1);
                    Comments.Add(c);
                }
            }
            
            base.OnEndLoading(status, result);
        }
    }
}