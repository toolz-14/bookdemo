﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class PostUserComment : WebService {

        //Inputs
        [HideInInspector]
        public int idLabel;
        [HideInInspector]
        public string commentText;
        [HideInInspector]
        public bool isDeleted;
        [HideInInspector]
        public int idAuthor;
        [HideInInspector]
        public int commentStatus;

        [HideInInspector]
        public int request_status; // 0 = adding, 1 = delete, 2 = update, 3 = update status by admin
        [HideInInspector]
        public int idComment;

        //Output
        [HideInInspector]
        public Comment comment;

        // Use this for initialization
        void Start() {
            isDeleted = false;
            idAuthor = 0;
        }

        protected override void FillData()
        {
            scriptURI = "discussions/";

            User user = GetComponent<User>();
            if (request_status != 3)
            {
                form.AddField("idProject", idLabel);
                if (idAuthor == 0)
                    form.AddField("idUser", user.idUser);
                else
                    form.AddField("idUser", idAuthor);
                
                form.AddField("status", commentStatus);
                form.AddField("comment", commentText);

                if (isDeleted)
                {
                    form.AddField("isDeleted", 1);
                }
                else
                {
                    form.AddField("isDeleted", 0);
                }
            }
            else
            {
                scriptURI = "discussions/updateStatus";
                form.AddField("idComment", idComment);
                commentStatus = 1;
                form.AddField("status", commentStatus);

                if (isDeleted)
                    form.AddField("isDeleted", 1);
                else
                    form.AddField("isDeleted", 0);
            }

            //Need to be logged in
            form.AddField("login", user.login);
            form.AddField("password", WebService.GetHashString(user.password));
        }

        protected override void OnEndLoading(long status, string result)
        {
            if (status == (long)Status.OK)
            {
                if (!result.Equals("comment_updated") && !result.Equals("nothing_to_update") && !result.Equals("comment_has_been_deleted"))
                {
                    Debug.Log(result);
                    JSONObject com = JSONObject.Parse(result);
                    comment = Comment.ParseComment(com, false);
                }
            }
            idAuthor = 0;
            base.OnEndLoading(status, result);
        }
    }
}