﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Boomlagoon.JSON;

namespace Toolz.WebServices
{
    public class GetSavedLabels : WebService
    {
        [HideInInspector]
        public Toolz.WebServices.Responses.LabelJSONObject labelJSONObject;

        // Use this for initialization
        protected override void FillData()
        {
            scriptURI = "saves/AllLabels";
        }

        protected override void OnEndLoading(long status, string result)
        {
            //Debug.Log(result);
            if (status == (long)Status.OK)
            {
                JSONArray jsonArray = JSONArray.Parse(result);
                labelJSONObject = labelJSONObject.Parse(jsonArray);
            }
            base.OnEndLoading(status, result);
        }
    }
}