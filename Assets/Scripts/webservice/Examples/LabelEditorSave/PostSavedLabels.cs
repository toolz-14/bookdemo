﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolz.WebServices
{
    public class PostSavedLabels : WebService
    {
        //Inputs
        [HideInInspector]
        public int id;
        [HideInInspector]
        public int idAuthor;
        [HideInInspector]
        public int phase;
        [HideInInspector]
        public int category;
        [HideInInspector]
        public string saveMasterString;
        [HideInInspector]
        public bool isDeleted;
        [HideInInspector]
        public bool isAdmin;
        [HideInInspector]
        public int labelStatus;

        // Use this for initialization
        private void Start()
        {
            isAdmin = false;
        }

        protected override void FillData()
        {
            scriptURI = "saves/labels";

            if (!isAdmin)
            {
                form.AddField("id", id);
                form.AddField("phase", phase);
                form.AddField("category", category);
                form.AddField("status", labelStatus);
                form.AddField("saveMasterString", saveMasterString);
                form.AddField("idAuthor", idAuthor);
                if (isDeleted)
                {
                    form.AddField("isDeleted", 1);
                }
                else
                {
                    form.AddField("isDeleted", 0);
                }
            }
            else
            {
                scriptURI = "saves/labels/admin";
                form.AddField("id", id);
                form.AddField("status", labelStatus);
                if (isDeleted)
                {
                    form.AddField("isDeleted", 1);
                }
                else
                {
                    form.AddField("isDeleted", 0);
                }
            }

        }

        protected override void OnEndLoading(long status, string result)
        {
            // Debug.Log("PostSavedLabels, OnEndLoading, status = "+status+" result = "+result);
            isAdmin = false;
            base.OnEndLoading(status, result);
        }
    }
}