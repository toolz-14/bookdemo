﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices {
    public class UserGetFollowedProjects : WebService {

        //Output
        [HideInInspector]
        public List<string> FollowedProjects;

        // Use this for initialization
        void Start() {
            FollowedProjects = new List<string>();
        }

        protected override void FillData()
        {
            scriptURI = "project/followed/";

            User user = GetComponent<User>();
            scriptURI += user.idUser.ToString() + "?login="+user.login+"&password="+WebService.GetHashString(user.password);
        }

        protected override void OnEndLoading(long status, string result) {
            FollowedProjects.Clear();
            if (status == (long)Status.OK) {
                JSONArray array = JSONArray.Parse(result);
                foreach (JSONValue project in array) {
                    FollowedProjects.Add(project.Str);
                }
            }

            base.OnEndLoading(status, result);
        }

    }
}