﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;
using Toolz.Flats;

namespace Toolz.WebServices
{
    public class GetUserCaptures : WebService
    {

        //Output
        //public Dictionary<string, Votes> VotesPerProject;
        private User _user;
        public int projectID;

        [HideInInspector]
        public Capture[] captures;

        [HideInInspector]
        public int ID;

        // Use this for initialization
        void Start()
        {
            scriptURI += "captures/Users/";
            _user = GetComponent<User>(); 
        }

        protected override void FillData()
        {
            scriptURI = scriptURI + _user.idUser + "/Projects/" + projectID + "" + "?login=" + _user.login + "&password=" + WebService.GetHashString(_user.password);
        }

        protected override void OnEndLoading(long status, string result)
        {
            // Saves.Clear();

            if (status == (long)Status.OK)
            {
                result = "{ \"Items\": " + result + "}";
                captures = JsonHelper.FromJson<Toolz.Flats.Capture>(result);
            }
            base.OnEndLoading(status, result);
        }
    }
}