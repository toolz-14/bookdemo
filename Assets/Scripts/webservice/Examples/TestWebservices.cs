﻿using UnityEngine;
using System.Collections;
using Toolz.WebServices;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class TestWebservices : MonoBehaviour
{
    // WEBSERVICE
    //private GetBookProject _getBookProject;

    private GetIdProjectByCategories _getIdProjectByCategories;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getIdProjectByCategories = webService.GetComponent<GetIdProjectByCategories>();
        _getIdProjectByCategories.onComplete += _getIdProjectByCategories_onComplete;
        _getIdProjectByCategories.onError += _getIdProjectByCategories_onError;
    }

    protected void Start()
    {
        // GET a specific project
        //_getBookProject.idProject = 1;
        // CREATE an empty project
        //_postBookProject.UseWebService();

        // UPDATE a specific project
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SendWB();
        }
    }

    private void SendWB()
    {
        _getIdProjectByCategories.categoriesSelected = new List<int>();
        _getIdProjectByCategories.categoriesSelected.Add(37);
        _getIdProjectByCategories.categoriesSelected.Add(1);
        _getIdProjectByCategories.UseWebService();

    }

    // WEB SERVICES CALLBACKS
    private void _getIdProjectByCategories_onComplete(long status, string message)
    {
        //Debug.Log("Get Age Stats OnComplete " + status + " : " + message );
        //Debug.Log( _getBookProject.bookData.ToString());
    }

    private void _getIdProjectByCategories_onError(long status, string message)
    {
        //Debug.Log("Get Age Stats OnError " + status + " : " + message);
    }
    
}