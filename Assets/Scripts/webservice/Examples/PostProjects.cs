﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolz.WebServices {
    [ExecuteInEditMode]
    public class PostProjects : WebService {

        private static readonly string LOGIN = "admin";
        private static readonly string PASSWORD = "admin";
        //Input
        public List<string> IdProjects;

        //No outputs

        // Use this for initialization
        void Start() {
            IdProjects = new List<string>();
        }

        protected override void FillData()
        {
            scriptURI = "admin/add-projects/";

            form.AddField("login", LOGIN);
            form.AddField("password", PASSWORD);
            form.AddField("length", IdProjects.Count);
            int i = 0;
            foreach (string idProject in IdProjects) {
                form.AddField(i.ToString(), idProject);
                ++i;
            }
        }

        protected override void OnEndLoading(long status, string result) {
            base.OnEndLoading(status, result);
        }
    }
}