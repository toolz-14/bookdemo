﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Collections.Generic;

namespace Toolz.WebServices {
    public class PostUserData : WebService {
        
        //Inputs
        public Dictionary<string, ProjectData> ProjectsData;

        private User _user;

        void Start() {
            scriptURI = "user/save";
            _user = GetComponent<User>();

            ProjectsData = new Dictionary<string, ProjectData>();
        }

        protected override void FillData() {
            int idUser = _user.idUser;
            form.AddField("idUser", idUser);
            form.AddField("useRatioData", 1);

            JSONObject projectsIdStateAndCost = null;
            JSONObject variablesIdAndRatios = null;

            JSONArray projectAndVar = null;
            JSONArray globalArray = new JSONArray();

            //ProjectData p;
            foreach (KeyValuePair<string, ProjectData> project in ProjectsData) {
                // Save project data (ID and activation state) in a JSON Object
                projectsIdStateAndCost = new JSONObject();
                projectsIdStateAndCost.Add("idProject", project.Key);
                projectsIdStateAndCost.Add("projectState", project.Value.Applied);
                projectsIdStateAndCost.Add("projectTotalCost", project.Value.Investment);

                // Save project variables data (ID and values) in a JSON Object

				variablesIdAndRatios = new JSONObject();
				if (project.Value.Values != null) {
                	foreach(KeyValuePair<string, float> value in project.Value.Values) {
                    	variablesIdAndRatios.Add(value.Key, value.Value);
                	}
				}
                // Save project data and variables data in a JSON Array
                projectAndVar = new JSONArray();
                projectAndVar.Add(projectsIdStateAndCost);
                projectAndVar.Add(variablesIdAndRatios);

                // Save project complete data in a global JSON Array
                globalArray.Add(projectAndVar);
            }

            form.AddField("projectsAndVariables", globalArray.ToString());   

			User user = GetComponent<User> ();
			form.AddField ("login", user.login);
			form.AddField ("password", GetHashString (user.password));
        }

        protected override void OnEndLoading(long status, string result) {
            base.OnEndLoading(status, result);
        }
    }
}