﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class GetUserData : WebService {

        //No inputs

        //Outputs
        //public List<ProjectData> ProjectsData;
        public Dictionary<string, ProjectData> ProjectsData;

        private User _user;
        // Use this for initialization
        void Start() {
            //For reference only, override by FillData to add get parameters
            scriptURI = "user/data/";
            _user = GetComponent<User>();
            ProjectsData = new Dictionary<string, ProjectData>();
        }

        protected override void FillData() {
            
            scriptURI = "user/data/" + _user.idUser;
        }

        protected override void OnEndLoading(long status, string result) {
            ProjectsData.Clear();
            if (status == (long)Status.OK) {
                JSONObject resultObject = JSONObject.Parse(result);
                foreach (var project in resultObject) {
                    ProjectData pdata = new ProjectData();
                    JSONArray projectArray = project.Value.Array;
                    JSONObject projectState = projectArray[0].Obj;
                    pdata.IdUser = _user.idUser;
                    pdata.Applied = int.Parse(projectState["projectState"].Str) != 0;
                    pdata.Investment = int.Parse(projectState["totalCost"].Str);
                    pdata.IdProject = project.Key;
                    //Check if project has variables
                    if (projectArray.Length == 2) {
                        JSONArray variables = projectArray[1].Array;
                        if (variables.Length > 0)
                            pdata.Values = new Dictionary<string, float>();
                        foreach (JSONValue variable in variables) {
                            string varId = variable.Obj["idVar"].Str;
                            float ratio = float.Parse(variable.Obj["varRatio"].Str);
                            pdata.Values.Add(varId, ratio);
                        }
                    }
                    ProjectsData.Add(pdata.IdProject, pdata);
                }
            }

            base.OnEndLoading(status, result);
        }
    }
}