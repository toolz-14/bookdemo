﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System;
using System.Collections.Generic;

namespace Toolz.WebServices {
    public class GetCommentReplies : WebService {

        //Input
        [HideInInspector]
        public uint idComment;

        //Output
        [HideInInspector]
        public List<Reply> Replies;
		public string OriginalCommentAuthorName;

        void Start() {
            Replies = new List<Reply>();
        }
        
        protected override void FillData()
        {
            //This line is for indication only, rewrited in AddFieldsToForm
            scriptURI = "discussions/reply/";

            User user = GetComponent<User>(); // Get user
            if (user.idUser != -1)
            { // CHECK IF USER IS LOGGED 
                scriptURI = "discussions/reply/" + user.idUser + "/" + idComment; // now all the same
                //scriptURL = "discussions/allReplies/" + idComment;
            }
            else
            {
                scriptURI = "discussions/reply/" + idComment;
            }
        }

        protected override void OnEndLoading(long status, string result) {
            Replies.Clear();
			OriginalCommentAuthorName = "";
            if (status == (long)Status.OK) {
				int i = 0;
                JSONArray replies = JSONArray.Parse(result);
                foreach (JSONValue reply in replies) {
                    Reply rep = Reply.ParseReply(reply.Obj, GetComponent<User>().idUser != -1);
                    Replies.Add(rep);
					if (i == 0) {
						OriginalCommentAuthorName = rep.originalCommentAuthorFullName;
					}
					++i;
                }
            }
            base.OnEndLoading(status, result);
        }

    }
}