﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

namespace Toolz.WebServices {
    public class GetUserProjectVote : WebService {

        //Input
        [HideInInspector]
        public string idProject;

        //Output
        [HideInInspector]
        //-1 = thumbdown +1 = thumbup, 0 = novote
        public int uservote = 0;

        void Start() {
            scriptURI = "vote/project/";
        }

        protected override void FillData() {
            User user = GetComponent<User>();
            scriptURI = "vote/project/" + user.idUser + "/" + idProject;
           // Debug.Log(scriptURL);
        }

        protected override void OnEndLoading(long status, string result) {
            uservote = 0;
			//Debug.Log ("GetUserProjectVote() result = "+result);
            if (status == (long)Status.OK) {
                uservote = int.Parse(JSONObject.Parse(result)["userVote"].Str);
				//Debug.Log (uservote);
            }
            base.OnEndLoading(status, result);
        }
    }
}