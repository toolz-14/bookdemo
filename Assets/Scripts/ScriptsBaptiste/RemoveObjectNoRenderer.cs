﻿using UnityEngine;
using System.Collections;

public class RemoveObjectNoRenderer : MonoBehaviour {

	private void Start () {

	}
	
	
	public void RemoveObject () {
		foreach (Transform trans in transform) {
			if(trans.gameObject.GetComponent<MeshRenderer> () == null){
				DestroyImmediate(trans.gameObject);
			}
		}
	}
}
