﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ClickToSelectObject : MonoBehaviour {

	public float DistanceRayon = 2000f;
	public LayerMask layerMask;
	public Camera myCamera;
	public Text DisplayText;
	private Ray myRay;
	private RaycastHit myHit;
	private string tempString = "";
	private Metadata tempMetadata;
	private Transform ImageBg;

	private void Start () {
		ImageBg = DisplayText.transform.parent;
	}

	private void Update () {
	
		myRay = myCamera.ScreenPointToRay (Input.mousePosition);
#if UNITY_STANDALONE
		if (Input.GetButtonDown ("Fire1")) {
#elif UNITY_IOS || UNITY_ANDROID
		if (Input.touchCount == 1){
#else
		if(false){
#endif

		if (Physics.Raycast (myRay, out myHit, DistanceRayon, layerMask)) {
//				Debug.Log ("myHit = "+myHit.transform.name);
		tempMetadata = myHit.transform.GetComponent<Metadata>();
//		Debug.Log("tempMetadata = "+tempMetadata);
			if(tempMetadata != null){
//				tempString = tempMetadata.keys[0]+" "+tempMetadata.values[0]+ tempMetadata.keys[1]+" "+tempMetadata.values[1]+ tempMetadata.keys[2]+" "+tempMetadata.values[2]+ tempMetadata.keys[3]+" "+tempMetadata.values[3]+ tempMetadata.keys[4]+" "+tempMetadata.values[4];
//		    	DisplayText.text = tempString;
				DisplayText.text = OrganizeString(tempMetadata.keys, tempMetadata.values);
			}
		}
	}
}

	private string OrganizeString(string[] keys, string[] values){
	    float textHeight = 0f;
		tempString = "";
		for(int i=0; i<keys.Length; i++){
			tempString += keys[i]+" : "+values[i]+"NEWLINE";
		}
		tempString = tempString.Replace("NEWLINE", "\n");
		//add 30 height to UI image and text
		textHeight = (keys.Length - 1f) * 30f;

		ImageBg.GetComponent<RectTransform>().sizeDelta = new Vector2(ImageBg.GetComponent<RectTransform>().sizeDelta.x, textHeight);
		DisplayText.GetComponent<RectTransform>().sizeDelta = new Vector2(ImageBg.GetComponent<RectTransform>().sizeDelta.x, textHeight);
		return tempString;
	}
}
	