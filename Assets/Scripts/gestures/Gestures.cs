//#define WIN_TOUCH

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Runtime.InteropServices;

#if WIN_TOUCH 
#pragma warning disable 
namespace UnityEngine
{
	public struct Touch
	{
		public Vector2 position;
	}
}
#endif

public class Gestures : MonoBehaviour
{
    //------------------| Dll Import |

#if WIN_TOUCH
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class tTouchData
	{
		public int m_x;
		public int m_y;
		public int m_ID;
		public int m_Time;
	};
	
	private bool _winTouchLibInitialized = false;
	private tTouchData[] _winTouches = new tTouchData[2];
	
	[DllImport("TouchOverlay", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
	public static extern int Initialise(string Str);
	string Str;
	[DllImport("TouchOverlay")]
	public static extern int GetTouchPointCount();
	[DllImport("TouchOverlay")]
	public static extern void GetTouchPoint(int i, tTouchData n);
#endif

    //public Text debugtext;

    //------------------| Pseudo Singleton |

    static private Gestures _instance;
	static public bool debug = true;

    //public bool useRTCinput;

    [HideInInspector] public bool forceTouch = false;
	
	static public Gestures instance
	{
		get { return _instance; }
	}

    // static 
    static public bool SHIFT;
	static public bool ALT;
	static public bool LEFT_CLIC;
	static public bool RIGHT_CLIC;
	
	// instance
	private bool _useTouch;
	private bool _useMouse;
	
	static private bool _debugMouse = false;
	static private bool _debugTouch = false;
	
	private List<Gesture> _gestures;
	private bool _enabled;
	private int _numGestures = 0;
	
	private Vector2 _mousePosition;
	private Vector3 _mouseScroll;
	
	private Touch[] _touches;
	private int _numTouches;
	
	//// settings
	//private float _scaleFactor = 1;
	
	// debug
	//private Gesture _gestureDebug;
	
	//------------------| Awake |
	
	private void Awake()
	{
		if (_instance != null)
		{
			//Debug.LogError("Only one gesture object can be on scene");
			GameObject.Destroy(this);
			return;
		}
		// init
		_gestures = new List<Gesture>();

//#if UNITY_WEBGL
        _useMouse = true;
        _useTouch = false;
//#elif !UNITY_WEBGL
		//if (Input.touchSupported)
		//{
		//	_useMouse = false;
		//	_useTouch = true;
		//}
		//else
		//{
		//	_useMouse = true;
		//	_useTouch = false;
		//}
//#endif
        //Debug.Log ("Gestures, Awake, _useMouse = "+_useMouse+" _useTouch = "+_useTouch);
        if (_debugMouse || _debugTouch)
		{
			_useMouse = _debugMouse;
			_useTouch = _debugTouch;
		}
		
		if (forceTouch)
		{
			_useTouch = true;
		}
		
		//_scaleFactor = Screen.dpi > 220 ? 2 : 1;
		
		_instance = this;
		GameObject.DontDestroyOnLoad(this);
		
		enabled = false;
	}
	
	protected virtual void OnDestroy()
	{
		if (_instance != this) return;
		
		if(_gestures != null)
		{
			_gestures.Clear();
			_gestures = null;
		}
		_instance = null;
	}
	
	//------------------| GUI |
#if WIN_TOUCH
	
	private void OnGUI()
	{
		if(!_winTouchLibInitialized)
		{
			Str = "Bordeaux_Bassin3D";
			//Debug.Log("try init win touch lib = "+Initialise(Str));
			if (Initialise(Str) < 0)
			{
				//Debug.Log("Unable to initialize windows touch library");
				return;
			}
			else
			{
				_winTouchLibInitialized = true;
				_useTouch = true;
				_useMouse = false;
				
				_winTouches[0] = new tTouchData();
				_winTouches[1] = new tTouchData();
				_touches = new Touch[2];
				_touches[0].position = Vector2.zero;
				_touches[1].position = Vector2.zero;
				//Debug.Log("Win touch initialized");
			}
		}
	}
#endif
	
	//------------------| Add / Remove |
	
	public void Register(Gesture g)
	{
		if (_gestures.Contains(g)) return;
		_gestures.Add(g);
		_numGestures++;
		
		if (_numGestures == 1) enabled = true;
		
		// Debug.Log("add gesture " + g);
	}
	
	public void Unregister(Gesture g)
	{
		if (!_gestures.Contains(g)) return;
		_gestures.Remove(g);
		_numGestures--;
		
		if (_numGestures == 0) enabled = false;
		// Debug.Log("remove gesture " + g);
	}
	
	//------------------| Update |
	
	protected void LateUpdate()
	{
		float dt = Time.deltaTime;
		//Debug.Log("touch = " + _useTouch + ", mouse = " + _useMouse);
		
		if (_useTouch && _useMouse)
		{
			UpdateMouse();
			UpdateTouch();
			ProcessMouseAndTouch(dt);
			return;
		}
		if (_useTouch)
		{
			UpdateTouch();
			ProcessTouches(dt);
		}
		if (_useMouse)
		{
			UpdateMouse();
			ProcessMouse(dt);
		}
		
		//if (_gestureDebug == null) return;
	}
	
	private void ProcessTouches(float dt)
	{
		int i = _numGestures;
		Gesture g = null;
		
		while(--i>-1)
		{
			g = _gestures[i];
			g.Touch(_numTouches, _touches, dt);
		}
	}
	
	private void ProcessMouse(float dt)
	{
		int i = _numGestures;
		Gesture g = null;
		
		while (--i > -1)
		{
			g = _gestures[i];
            g.Mouse(_mousePosition, _mouseScroll, dt);
		}
	}
	
	private void ProcessMouseAndTouch(float dt)
	{
		int i = _numGestures;
		Gesture g = null;
		
		while (--i > -1)
		{
			g = _gestures[i];

			g.Mouse(_mousePosition, _mouseScroll, dt);
			g.Touch(_numTouches, _touches, dt);
		}
	}
	
	private void UpdateMouse()
	{
        if (CustomInputModule.useRTCinputStatic)
        {
            _mousePosition = RTCInput.mousePosition;
            _mouseScroll = RTCInput.mouseScrollDelta;
            //debugtext.text = "Gestures, _mouseScroll = " + _mouseScroll.ToString();

            LEFT_CLIC = RTCInput.GetMouseButton(0);
            RIGHT_CLIC = RTCInput.GetMouseButton(1);
            SHIFT = RTCInput.GetKey(KeyCode.LeftShift) || RTCInput.GetKey(KeyCode.RightShift) || RTCInput.GetKey(KeyCode.RightShift);
            ALT = RTCInput.GetKey(KeyCode.LeftAlt) || RTCInput.GetKey(KeyCode.RightAlt) || RTCInput.GetKey(KeyCode.RightAlt);
        }
        else
        {
            _mousePosition = Input.mousePosition;
            _mouseScroll = Input.mouseScrollDelta;

            LEFT_CLIC = Input.GetMouseButton(0);
            RIGHT_CLIC = Input.GetMouseButton(1);
            SHIFT = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            ALT = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
        }
    }

    private void UpdateTouch()
	{
#if WIN_TOUCH
		if (!_winTouchLibInitialized)
			return;
		
		_numTouches = GetTouchPointCount();
		if (_numTouches > 2) _numTouches = 2;
		for (int i = 0; i < _numTouches; ++i)
		{
			GetTouchPoint(i, _winTouches[i]);
			_touches[i].position.x = _winTouches[i].m_x / 100;
			_touches[i].position.y = Screen.height -_winTouches[i].m_y / 100;
			//Log("touch["+i+"].position = "+_touches[i].position.ToString()  );
		}
		//Log("num touches = " + _numTouches);
#else
		_touches = Input.touches;
		_numTouches = Input.touchCount;
#endif
	}
	
	static public bool debugMouse
	{
		set
		{
			_debugMouse = value;
			if (_instance == null) return;
			_instance._useTouch = _debugTouch;
			_instance._useMouse = _debugMouse;
		}
	}
	
	static public bool debugTouch
	{
		set
		{
			_debugTouch = value;
			if (_instance == null) return;
			_instance._useMouse = _debugMouse;
			_instance._useTouch = _debugTouch;
		}
	}
	
}
