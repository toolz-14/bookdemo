using UnityEngine;
using System.Collections;

public class RotateGesture : Gesture
{
    private IRotateGesture _rotateDelegate;

    // test
    private float distances = 0;

    //------------------| Construct / Dispose |

    public RotateGesture(IRotateGesture rotateDelegate, string viewportId = null) : base()
    {
        _minTouchCount = _maxTouchCount = 2;

        idealSpeed = 400;
        angleT = 10;
        distanceT = 50;

        SetDelegate(rotateDelegate);
    }

    //------------------| Mouse |

    protected override void MouseBegan()
    {
        if (!Gestures.RIGHT_CLIC) return;
        _state = State.Updating;
        mp = m;
    }

    protected override void MouseUpdate(float dt)
    {
        if (!Gestures.RIGHT_CLIC)
        {
            _state = State.Ended;
            return;
        }
        dx = m.x - mp.x;
        mp = m;

        _rotateDelegate.Rotate(this, dx/8);
    }

    protected override void MouseEnd()
    {
        if (Gestures.RIGHT_CLIC) return;
        _state = State.Ended;
    }

    //------------------| Touch |

    protected override void TouchBegan()
    {
        pp1 = t1.position;
        pp2 = t2.position;

        _state = State.Updating;

        dx = pp2.x - pp1.x;
        dy = pp2.y - pp1.y;

        a1 = Mathf.Atan2(dy, dx);
        d1 = Vector2.Distance(pp2, pp1);

        ResetTest();
        distances = 0;
    }

    protected override void TouchUpdate(float dt)
    {
        p1 = t1.position;
        p2 = t2.position;

        dx = p2.x - p1.x;
        dy = p2.y - p1.y;
        a2 = Mathf.Atan2(dy, dx);
        //Debug.Log("p1 = " + p1 + ", p2 = " + p2);
        //Debug.Log("START ANGLE = " + _startAngle*Mathf.Rad2Deg + ", angle = " + _angle*Mathf.Rad2Deg);
        // angle diff in degrees
        float angleDiff = Mathf.DeltaAngle(a2 * Mathf.Rad2Deg, a1 * Mathf.Rad2Deg);
        a1 = a2;

        _rotateDelegate.Rotate(this, -angleDiff);
    }

    protected override void TouchEnd()
    {
        _state = State.Ended;
    }

    //------------------| Getters / Setters |

    public override void SetDelegate(IGestureDelegate deleg)
    {
        _rotateDelegate = deleg as IRotateGesture;
        base.SetDelegate(_rotateDelegate);
    }
}
