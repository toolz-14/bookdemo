using UnityEngine;
using System.Collections;

public class Gesture : IDisposable
{
    public delegate void GestureEvent(Gesture g);
    public event GestureEvent onViewportChange;

    protected enum State
    {
        Testing,
        Updating,
        Ended,
        Cancelled
    }
    protected State _state;
    
    // delegate
    protected IGestureDelegate _delegate;
    // todo concrete delegate in concrete gestures

    // touch
    protected Touch t1; // touch 1
    protected Touch t2; // touch 2
    protected int _touchCount = 0;
    protected int _minTouchCount = 1; // don't forget to set this on concrete classes
    protected int _maxTouchCount = 1;

    // testing
    protected float idealSpeed = 550; // px per second
    protected float angleT = 10; // seuil de detection de 15 degres par seconde
    protected float distanceT = 50; // seuil de cancel du gesture
    protected float angleT2; // seuil de l'angle adapté a la vitesse actuelle
    protected float distanceT2; // seuil de la distance adapté a la vitesse actuelle
    protected bool fail;
    protected float touchDistance = 0;
    protected float currentSpeed = 0;
    protected int numTests = 0;

    // mouse
    protected Vector2 m; // mouse position
    protected Vector2 s; // scroll magnitude
    protected Vector2 mp; // previous mouse pos

    // touch
    protected Vector2 p1; // position de la touche 1
    protected Vector2 p2; // position de la touche 2
    protected Vector2 pp1; // position precedente de la touche 1
    protected Vector2 pp2; // position precedente de la touche 2
    protected Vector2 pm1; // deplacement de la touche 1
    protected Vector2 pm2; // deplacement de la touche 2
    protected float d1; // distance de depart
    protected float d2; // distance actuelle
    protected float dd; // difference des distances
    protected float dx; // distance x
    protected float dy; // distance y;
    protected float a1; // angle de depart
    protected float a2; // angle actuel
    protected float ad; // difference des angles

    //------------------| Construct / Dispose |

    public Gesture()
    {
        _state = State.Ended;
		if (Gestures.instance != null) {
			Gestures.instance.Register (this);
		}
    }

    public virtual void Dispose()
    {
        if(Gestures.instance != null)
        {
            Gestures.instance.Unregister(this);
        }

        _delegate = null;

        onViewportChange = null;
    }


    //------------------| Mouse |

    // handle mouse
    public virtual void Mouse(Vector2 pos, Vector3 scroll, float dt)
    {

        m = pos;
        s = scroll;

        if (_state == State.Ended)
        {
            if (Gestures.LEFT_CLIC || Gestures.RIGHT_CLIC)
            {
                MouseBegan();
            }
        }
        else if (_state == State.Testing)
        {
            MouseTest(dt);
        }
        else if (_state == State.Updating)
        {
            if (!Gestures.LEFT_CLIC && !Gestures.RIGHT_CLIC)
            {
                MouseEnd();
                return;
            }
            MouseUpdate(dt);

        }
        else if (_state == State.Cancelled)
        {
            if (!Gestures.LEFT_CLIC && !Gestures.RIGHT_CLIC)
            {
                MouseEnd();
            }
        }
    }

    protected virtual void MouseBegan()
    {
        // to override
    }

    protected virtual void MouseTest(float dt)
    {
        // to overide
    }

    protected virtual void MouseUpdate(float dt)
    {
        // to override
    }

    protected virtual void MouseEnd()
    {
        // to override
    }

    //------------------| Touches |

    // handle touches
    public virtual void Touch(int touchCount, Touch[] touches, float dt)
    {

        _touchCount = touchCount;
        if (_touchCount > 1)
        {
            t1 = touches[0];
            t2 = touches[1];
        }
        else if (_touchCount > 0)
        {
            t1 = touches[0];
        }


        if (_state == State.Ended)
        {
            if (touchCount >= _minTouchCount)
            {
                TouchBegan();
            }
        }
        else if (_state == State.Testing)
        {
            if (_touchCount < _minTouchCount || _touchCount > _maxTouchCount)
            {
                TouchEnd();
                return;
            }
            TouchTest(dt);
        }
        else if (_state == State.Updating)
        {
            if (_touchCount < _minTouchCount || _touchCount > _maxTouchCount)
            {
                TouchEnd();
                return;
            }
            TouchUpdate(dt);

        }
        else if (_state == State.Cancelled)
        {
            if (_touchCount < _minTouchCount || _touchCount > _maxTouchCount)
            {
                TouchEnd();
            }
        }
    }

    protected virtual void TouchBegan()
    {
        // to override
    }

    protected virtual void TouchTest(float dt)
    {
        // to override
    }

    protected virtual void TouchUpdate(float dt)
    {
        // to override
    }

    protected virtual void TouchEnd()
    {
        // to override
    }

    //------------------| Testing |

    protected void ResetTest()
    {
        numTests = 0;
        currentSpeed = 0;
        touchDistance = 0;
    }

    protected void ApplySpeedThreshold()
    {
        currentSpeed = touchDistance;
        angleT2 = currentSpeed * angleT / idealSpeed;
        distanceT2 = currentSpeed * distanceT / idealSpeed;
    }

    protected void LogTouchTest(string id = null)
    {
        string s = (id == null ? "" : "[ "+id+" ] ");

        s += "RESULTS----------------------------";
        s += "\ndpi = " + Screen.dpi;
        s += "\ncurrentSpeed = " + currentSpeed + " idealSpeed = "+idealSpeed+" touchDistance = " + touchDistance;
        s += "\nangleT = " + angleT + ", angleT2 = " + angleT2;
        s += "\ndistanceT = " + distanceT + ", distanceT2 = " + distanceT2;
        s += "\nad = " + ad + " / threshold = " + angleT2 + ", distance diff = " + dd + " / threshold = " + distanceT2;
        s += "\n-------------------------------- FAIL = " + fail;

    }

    //------------------| Getters / Setters |

    public virtual void SetDelegate(IGestureDelegate deleg)
    {
        _delegate = deleg;
    }
}
