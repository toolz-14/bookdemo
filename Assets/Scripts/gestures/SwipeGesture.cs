using UnityEngine;
using System.Collections;

public class SwipeGesture : Gesture
{
    private ISwipeGesture _swipeDelegate;
    private float _scope = 100;

    private Vector2 _origin = Vector2.zero;
    private Vector2 _direction = Vector2.zero;
    private Vector2 _axis = Vector2.zero;
    private float _tweenDuration = 0;

    private float _time = 0;

    //------------------| Construct / Dispose |

    public SwipeGesture(ISwipeGesture swipeDelegate, string viewportId = null) : base()
    {
        SetDelegate(swipeDelegate);
    }

    //------------------| Mouse |

    protected override void MouseBegan()
    {
        if (!Gestures.LEFT_CLIC) return;
        if (Gestures.ALT || Gestures.SHIFT) return;

        _state = State.Updating;

        _origin = m;
        mp = m;
        
        _time = Time.realtimeSinceStartup;
        //Debug.Log("time = " + _time);
    }

    protected override void MouseUpdate(float dt)
    {
        if (Gestures.ALT || Gestures.SHIFT)
        {
            _state = State.Cancelled;
            return;
        }
        p1.x = m.x - mp.x;
        p1.y = m.y - mp.y;
        mp = m;

        _swipeDelegate.SwipeDrag(this, p1, _scope);
    }

    protected override void MouseEnd()
    {
        //Debug.Log("MOUSE END ..... " + _state.ToString());
        if (_state == State.Updating)
        {
            ValidateSwipe(m);
        }
        _state = State.Ended;
    }


    //------------------| Touch |

    protected override void TouchBegan()
    {
        if (_touchCount > 1)
        {
            _state = State.Cancelled;
            return;
        }
        pp1 = t1.position;
        _origin = pp1;

        _state = State.Updating;
    }

    protected override void TouchUpdate(float dt)
    {
        p1 = t1.position;
        pm1 = p1 - pp1;
        pp1 = p1;

        //touchDistance += _drag.x;
        //Gestures.Log("touchDistance = "+touchDistance);
        _swipeDelegate.SwipeDrag(this, pm1, _scope);
    }

    protected override void TouchEnd()
    {
        if(_state == State.Updating)
        {
            p1 = t1.position;
            ValidateSwipe(p1);
        }
        _state = State.Ended;
    }



    //------------------| Common |

    private void ValidateSwipe(Vector2 currentPos)
    {
        float dx = currentPos.x - _origin.x;
        float dy = currentPos.y - _origin.y;
        float threshold = .5f;

        float time = Time.realtimeSinceStartup;
        float dt = time - _time;

        // Debug.Log("---------------------------- DELTA TIME = " + dt);

        // horizontal
        if (Mathf.Abs(dx / _scope) >= threshold)
        {
            // horizontal swipe
            _direction.y = 0;

            // calculate speed needed to finish swipe
            float dragSpeed = dx / dt;
            //Debug.Log("DRAG SPEED = " + dragSpeed);
            float neededSpeed = Mathf.Abs((_scope - dx) * dragSpeed / dx);
            _tweenDuration = Mathf.Abs(_scope - dx) / neededSpeed;
            //Debug.Log("swipe validate tween duration = " + _tweenDuration);
            if (_tweenDuration < .2f) _tweenDuration = .2f;

            if (dx > 0)
            {
                _direction.x = 1;
            }
            else
            {
                _direction.x = -1;
            }
            _swipeDelegate.SwipeValidate(this, _direction, _tweenDuration);
        }
        else
        {
            _axis.x = 1;
            _axis.y = 0;
            _swipeDelegate.SwipeReset(this, _axis);
        }
        // vertical
        if (Mathf.Abs(dy / _scope) >= threshold)
        {
            // vertical swipe
            _direction.x = 0;

            // calculate speed needed to finish swipe
            float dragSpeed = dy / dt;
            //Debug.Log("DRAG SPEED = " + dragSpeed);
            float neededSpeed = Mathf.Abs((_scope - dy) * dragSpeed / dy);
            _tweenDuration = Mathf.Abs(_scope - dy) / neededSpeed;
            //Debug.Log("swipe validate tween duration = " + _tweenDuration);
            if (_tweenDuration < .2f) _tweenDuration = .2f;

            if (dy > 0)
            {
                _direction.y = 1;
            }
            else
            {
                _direction.y = -1;
            }
            _swipeDelegate.SwipeValidate(this, _direction, _tweenDuration);
        }
        else
        {
            _axis.x = 0;
            _axis.y = 1;
            _swipeDelegate.SwipeReset(this, _axis);
        }
    }

    //------------------| Getters / Setters |

    public override void SetDelegate(IGestureDelegate deleg)
    {
        _swipeDelegate = deleg as ISwipeGesture;
        if (_swipeDelegate == null) return;
        base.SetDelegate(deleg);
    }
}
