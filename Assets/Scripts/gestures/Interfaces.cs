using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public interface IUpdatable
{
    void Update(float dt);
}

public interface IDisposable
{
    void Dispose();
}
/*
public interface IWorldInteraction
{
    void move(float x, float y);
    void zoom(float z);
    void rotate(float r);
}*/

public interface IGraphicChild
{
    void Draw(List<UIVertex> vbo);
    void UpdateVertex(int index, float x, float y);
}

public interface ISerialisable
{
    void Unserialize(XmlNode xml);
    XmlNode Serialize();
}


public interface IView
{
    void Show(bool animated);
    void Hide(bool animated);
}


// gesture delegates

public interface IGestureDelegate
{
//    void GestureRecognized(Gesture g);
//    void GestureBegan(Gesture g);
//    void GestureEnded(Gesture g);
}

public interface IDragGesture : IGestureDelegate
{
	void Drag(DragGesture g, Vector2 magnitude);
}

public interface IZoomGesture : IGestureDelegate
{
    void Zoom(ZoomGesture g, float delta);
}

public interface IRotateGesture : IGestureDelegate
{
    void Rotate(RotateGesture g, float angle);
}

public interface ITapGesture : IGestureDelegate
{
    void Tap(TapGesture g, Vector2 screenPosition);
}

public interface IPanGesture : IGestureDelegate
{
    void Pan(PanGesture g, Vector2 panning);
}

public interface ISwipeGesture : IGestureDelegate
{
    void SwipeDrag(SwipeGesture g, Vector2 drag, float scope);
    void SwipeReset(SwipeGesture g, Vector2 axis); // x=0, y=1 si l axe est y, x=1,y=0 si l axe est x
    void SwipeValidate(SwipeGesture g, Vector2 direction, float tweenDuration);
}


// COMPONENTS

//public interface ListDelegate
//{
//    //ListItem GetListItem(int id);
//    void ConfigureListItem(List list, ListItem item); // l'id est en propri�t� de l'item
//    void SelectListItem(List list, ListItem item); // l'id est en propri�t� de l'item
//    void DeselectListItem(List list, ListItem item);
//}
//
//public interface FormDelegate
//{
//    void ConfigureFormItem(Form form, FormItem item);
//    string GetErrorLocalisation(Form.Error error);
//    void OnFormSuccess(Form form);
//    void OnFormError(Form form, Form.Error error);
//}
//
//public interface TabBarDelegate
//{
//    void ConfigureTabBt(TabBar tab, TabBarBt bt);
//    void SelectTabBt(TabBar tab, TabBarBt bt);
//    void DeselectTabBt(TabBar tab, TabBarBt bt);
//}

// Project

//public interface ProjectDelegate
//{
//    void UpdateViewData();
//	void OnVarChange(VarData variable);
//    void OnSwitchToggle(UISwitch uiSwitch, bool selected);
//    void OnCheckToggle(UICheckBox box, bool selected);
//}
//
//public interface ProjectSubStateDelegate
//{
//    void OnStateReady(ProjectSubState state);
//    void ChangeState(ProjectSubState oldState);
//    void OnStateExited(ProjectSubState state);
//}


// Localisation

public interface ILocalisable
{
    void UpdateLocalisation();
}


// Softkeyboard and keyboard

public interface SoftKeyboardDelegate
{
    void OnInputPublish(string text);
    void OnInputBgCancel();
    void OnInputCancel();
}

public interface KeyboardListener
{
    void OnKeyDown();
}

// Web Services and user actions
//
//public interface IDiscussionDelegate
//{
//    void OnCommentVote(CommentaryData comment, int up, int down);
//    void OnCommentRespond(CommentaryData comment);
//}
//
//public interface IResponseDelegate
//{
//    void OnRespondToResponse(ResponseData response);
//}
//
//// Carto and terrain
//
//public interface ISFTerrainDelegate
//{
//    void OnTerrainRendered();
//    void OnTerrainCleared();
//    Texture2D GetTerrainTexture(string id);
//}
//
//// stats
//public interface IStatView
//{
//    void UpdateStats();
//}