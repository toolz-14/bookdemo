using UnityEngine;
using System.Collections;

public class PanGesture : Gesture
{
    protected IPanGesture _panDelegate;
    protected Vector2 _drag = Vector2.zero;
    protected Vector2 _axis = Vector2.one;

    //------------------| Construct / Dispose |

    public PanGesture(IPanGesture panDelegate, string viewportId = null) : base()
    {
        SetDelegate(panDelegate);

        // settings
        _minTouchCount =
        _maxTouchCount = 2;

        idealSpeed = 150;

        distanceT = 30;
        angleT = 2;
    }

    //------------------| Mouse |

    protected override void MouseBegan()
    {
        if (!Gestures.LEFT_CLIC && !Gestures.ALT) return;
        _state = State.Updating;
        mp = m;
    }

    protected override void MouseUpdate(float dt)
    {
        if (!Gestures.ALT)
        {
            _state = State.Cancelled;
            return;
        }
        _drag.x = m.x - mp.x;
        _drag.y = m.y - mp.y;
        mp = m;
        _panDelegate.Pan(this, _drag);
    }

    protected override void MouseEnd()
    {
        _state = State.Ended;
    }

    //------------------| Touch |


    protected override void TouchBegan()
    {
        pp1 = t1.position;
        pp2 = t2.position;

        _state = State.Testing;

        dx = pp2.x - pp1.x;
        dy = pp2.y - pp1.y;

        a1 = Mathf.Atan2(dy, dx);
        d1 = Vector2.Distance(pp2, pp1);

        ResetTest();
        _axis.x = _axis.y = 0;
    }

    protected override void TouchUpdate(float dt)
    {
        //Debug.Log("update...");
        p1 = t1.position;
        p2 = t2.position;

        pm1 = p1 - pp1;
        pm2 = p2 - pp2;
        pp1 = p1;
        pp2 = p2;

        _drag = (pm1 + pm2) / 2;
        _drag.x *= _axis.x;
        _drag.y *= _axis.y;

        //Debug.Log("axis = " + _axis);
        _panDelegate.Pan(this, _drag);
    }

    protected override void TouchEnd()
    {
        //Debug.Log("end...");
        _state = State.Ended;
    }


    //------------------| Getters / Setters |

    public override void SetDelegate(IGestureDelegate deleg)
    {
        _panDelegate = deleg as IPanGesture;
        if (_panDelegate == null) return;
        base.SetDelegate(_panDelegate);
    }

}
