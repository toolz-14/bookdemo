using UnityEngine;
using System.Collections;

public class TapGesture : Gesture
{
    private ITapGesture _tap;
    private float _time;

    // mouse
    public float _mouseMaxDistance = 5;
    public float _mouseMaxTime = .2f;

    // touch
    public float _touchMaxDistance = 8;
    public float _touchMaxTime = .2f;


    //------------------| Construct / Dispose |

    public TapGesture(ITapGesture tapDelegate) : base() 
    {
        //Debug.Log("NEW TAP GESTURE " + this + ", "+id);
        SetDelegate(tapDelegate);
    }

    public override void Dispose()
    {
        //Debug.Log("DISPOSE TAP GESTURE " + this + ", " + id);
        _tap = null;
        base.Dispose();
    }

    //------------------| Mouse |

    protected override void MouseBegan()
    {
        // only on left clic
        if (!Gestures.LEFT_CLIC) return;
        _state = State.Updating;
        _time = 0;
        mp = m;
    }

    protected override void MouseUpdate(float dt)
    {
        //Debug.Log("Mouse update");

        _time += dt;
        if (_time > _mouseMaxTime) _state = State.Cancelled;
        float dist = Vector2.Distance(m, mp);
        if (dist > _mouseMaxDistance)
        {
            _state = State.Cancelled;
        }
    }

    protected override void MouseEnd()
    {
        //Debug.Log("Mouse end");

        if (Gestures.LEFT_CLIC) return;

        if (_state == State.Updating)
        {
            _tap.Tap(this, m);
        }

        _state = State.Ended;
    }


    //------------------| Touches |

    protected override void TouchBegan()
    {
        if (_touchCount > 1)
        {
            _state = State.Cancelled;
            return;
        }
        p1 = t1.position;

        _state = State.Updating;

        _time = 0;
    }

    protected override void TouchUpdate(float dt)
    {
        p2 = t1.position;

        _time += dt;
        if (_time > _touchMaxTime) _state = State.Cancelled;
        dx = p2.x - p1.x;
        dy = p2.y - p1.y;
        p1 = p2;

        d1 = Mathf.Sqrt(dx * dx + dy * dy);
        //Debug.Log("dist = " + dist);
        if (d1 > _touchMaxDistance)
        {
            _state = State.Cancelled;
        }
    }

    protected override void TouchEnd()
    {
        if (_state == State.Updating)
        {
            _tap.Tap(this, p2);
        }

        _state = State.Ended;
    }



    //------------------| Delegate |

    public override void SetDelegate(IGestureDelegate deleg)
    {
        _tap = deleg as ITapGesture;
        if (_tap == null) return;
        base.SetDelegate(deleg);
        //Debug.Log("tap delegate = " + _tap);
    }
}
