using UnityEngine;
using System.Collections;

public class ZoomGesture : Gesture
{
    private IZoomGesture _zoomDelegate;
    private float _zoomScale = 30;

    //------------------| Construct / Dispose |

    public ZoomGesture(IZoomGesture zoomDelegate, string viewportId = null) : base()
    {
        _minTouchCount = _maxTouchCount = 2;

        idealSpeed = 200;
        angleT = 4;
        distanceT = 20;

        SetDelegate(zoomDelegate);
    }

    //------------------| Mouse |

    // bypass normal behaviour
    public override void Mouse(Vector2 pos, Vector3 scroll, float dt)
    {
        if(scroll.magnitude != 0)
        {
            _zoomDelegate.Zoom(this, scroll.y);
        }
    }

    //------------------| Touch |

    protected override void TouchBegan()
    {
        pp1 = t1.position;
        pp2 = t2.position;

        _state = State.Updating;

        dx = pp2.x - pp1.x;
        dy = pp2.y - pp1.y;

        d1 = Vector2.Distance(pp2, pp1);
        a1 = Mathf.Atan2(dy, dx);
        ResetTest();
    }

    protected override void TouchUpdate(float dt)
    {
        //Debug.Log("-------------------update.....");
        p1 = t1.position;
        p2 = t2.position;

        d2 = Vector2.Distance(p2, p1);
        dd = d2 - d1;

        d1 = d2;

        _zoomDelegate.Zoom(this, dd/_zoomScale);
    }

    protected override void TouchEnd()
    {
        _state = State.Ended;
    }


    //------------------| Getters / Setters |

    public override void SetDelegate(IGestureDelegate deleg)
    {
        _zoomDelegate = deleg as IZoomGesture;
        base.SetDelegate(_zoomDelegate);
    }

    /**
     * la distance de deplacement en touch equivalente
     * a un scale de 1, par exemple 30px correspond a 100%
     */ 
    public float zoomScale
    {
        get { return _zoomScale; }
        set
        {
            _zoomScale = value;
        }
    }
}
