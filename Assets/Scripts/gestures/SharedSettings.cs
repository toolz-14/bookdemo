using UnityEngine;
using System.Collections;

public class SharedSettings
{
	// singleton
	static private SharedSettings _instance;
	static private bool _allowInstanciation;

	static public SharedSettings instance{
		get{
			if(_instance == null){
				_allowInstanciation = true;
				_instance = new SharedSettings();
				_allowInstanciation = false;
			}
			return _instance;
		}
	}

	// instance

	public AssetManager assets;
	public TextureAtlas uiAtlas;
	public float scaleFactor;

    public float fogDensity;
    public float fogStartDistance;
    public float fogEndDistance;
    public Color fogColor;

//	public PushNotificationManager pushManager;

	public SharedSettings()
	{
		if (!_allowInstanciation) {
			Debug.LogError ("SharedSettings is a singleton");
		}
	}

    public void SaveFogSettings()
    {
        fogDensity = RenderSettings.fogDensity;
        fogStartDistance = RenderSettings.fogStartDistance;
        fogEndDistance = RenderSettings.fogEndDistance;
        fogColor = RenderSettings.fogColor;
    }
}

