using UnityEngine;
using System.Collections;

public class DragGesture : Gesture
{

    private IDragGesture _dragDelegate;
    private Vector2 _drag = Vector2.zero;


    //------------------| Construct / Dispose |
    
    public DragGesture(IDragGesture dragDelegate, string viewportId = null) : base() 
    {
        SetDelegate(dragDelegate);
    }

    //------------------| Mouse |

    protected override void MouseBegan()
    {
       // Debug.Log("drag began ");
        if (!Gestures.LEFT_CLIC) return;
        if (Gestures.ALT || Gestures.SHIFT) return;
        _state = State.Updating;
        mp = m;
    }

    protected override void MouseUpdate(float dt)
    {
        if (Gestures.ALT || Gestures.SHIFT)
        {
            _state = State.Cancelled;
            return;
        }
      //  Debug.Log("drag update");
        _drag.x = m.x - mp.x;
        _drag.y = m.y - mp.y;
        mp = m;

        _dragDelegate.Drag(this, _drag);
    }

    protected override void MouseEnd()
    {
       // Debug.Log("drag end");
        if (Gestures.LEFT_CLIC) return;
        _state = State.Ended;
    }

    //------------------| Touch |

    protected override void TouchBegan()
    {
        if (_touchCount > 1)
        {
            _state = State.Cancelled;
            return;
        }

        _state = State.Updating;
    }

    protected override void TouchUpdate(float dt)
    {
        pm1 = p1 - pp1;
        pp1 = p1;

        _dragDelegate.Drag(this, pm1);
    }

    protected override void TouchEnd()
    {
        //Debug.Log("drag end");
        _state = State.Ended;
    }

    //------------------| Getters / Setters |

    public override void SetDelegate(IGestureDelegate deleg)
    {
        _dragDelegate = deleg as IDragGesture;
        if (_dragDelegate == null) return;
        base.SetDelegate(deleg);
    }
}
