using UnityEngine;
using System.Collections;

public class AssetManager : IDisposable
{
	// events
	public enum AssetManagerEventType
	{
		Progress,
		Loaded
	};
	
	public delegate void AssetManagerEvent(AssetManager assets);
	public event AssetManagerEvent onProgress;
	public event AssetManagerEvent onLoaded;

	// assets
	private TextureAtlas[] _atlases;
	private int _numAtlases = 0;
    private TextureCache _textures;

	// loading
	private int _queueIndex;
	private int _queueLength;
	private float _progress = 0;
	//private bool _loaded;

	//---------------------------- Construct / Dispose

	public AssetManager()
	{
		_atlases = new TextureAtlas[8];
        _textures = new TextureCache();
	}

	public void Dispose()
	{
		// TODO : Dispose AssetManager
	}

	//---------------------------- Atlases

	public void AddAtlas(string name, string path)
	{
		if (_numAtlases == _atlases.Length) {
			Debug.LogError ("max atlases limit reached");
			return;
		}
		TextureAtlas atlas = new TextureAtlas (name, path);
		_atlases [_numAtlases] = atlas;
		_numAtlases ++;
	}

	public TextureAtlas atlas(string name)
	{
		int i = _numAtlases;
		while (--i>-1) {
			if (_atlases [i].name.Equals (name))return _atlases [i];
		}
		return null;
	}

	public Sprite sprite(string name)
	{
		int i = _numAtlases;
		Sprite result = null;
		while (--i>-1) 
		{
			result = sprite (name, _atlases[i]);
			if(result != null) return result;
		}
		return null;
	}

	public Sprite sprite(string name, string atlasName)
	{
		return sprite (name, atlas (atlasName));
	}

	public Sprite sprite(string name, TextureAtlas atlas)
	{
		if (atlas == null) return null;
		return atlas.sprite (name);
	}

    //------------------| Textures |

    public void AddTexture(string id, string url)
    {
        _textures.Enqueue(id, url);
    }
    /*
    public void RemoveTexture(string id, bool dispose = true)
    {
        _textures.RemoveTexture(id, dispose);
    }*/

    public Texture2D texture(string id)
    {
        return _textures.GetTexture(id);
    }

    public TextureCache textures
    {
        get { return _textures; }
    }

	//--------------------------- Load

	public void Load()
	{
		//if (_loaded) return;
        //Debug.Log("ASSET MANAGER _textures.queueLength = " + _textures.queueLength);
		_queueLength = _numAtlases + _textures.queueLength;// + other assets
		_queueIndex = 0;

		LoadNext ();
	}

	protected void LoadNext()
	{
		UpdateLoadProgress();

		if (_queueIndex == _queueLength) 
		{
			if(onLoaded != null) onLoaded(this);
			return;
		}
		
		_queueIndex++;
		int index = _queueIndex - 1;

		// load atlases first
		if (index < _numAtlases) 
		{
			TextureAtlas atlas = _atlases[index];
			//Debug.Log ("Load Texture Atlas "+atlas.name);
			atlas.onLoaded += OnAtlasLoaded;
			atlas.Load ();
		}
        else
        {
            _queueIndex-=2; // bricolage
            _textures.onLoadProgress += OnTextureLoaded;
            _textures.LoadTextures();
        }
	}

    protected void UpdateLoadProgress()
    {
        _progress = (float)_queueIndex / (float)_queueLength;
        if (onProgress != null) onProgress(this);
    }

	protected void OnAtlasLoaded(TextureAtlas atlas)
	{
        atlas.onLoaded -= OnAtlasLoaded;
		//Debug.Log ("atlas loaded " + atlas.name);
		LoadNext ();
	}

    private void OnTextureLoaded(TextureCache c)
    {
        _queueIndex++;
        //Debug.Log("OnTexture Loaded....." + c.loadProgress + " queueIndex=" + _queueIndex + ", length = " + _queueLength);
        UpdateLoadProgress();

        if (_queueIndex == _queueLength)
        {
            _textures.onLoadProgress -= OnTextureLoaded;
            if (onLoaded != null) onLoaded(this);
            return;
        }
    }

	//-------------------------- Getters / Setters

	public float progress{ get{ return _progress; } }

}

