using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AtlasImage : MonoBehaviour 
{
	public string spriteId;
	public bool nativeSize;
    public bool sliced;

	protected Image img;
    protected Sprite sprite;


    //------------------| Awake / Start |

    protected void Awake()
    {
        img = GetComponent<Image>();
    }

	protected void Start ()
	{
        SetSpriteID(spriteId);
	}


    public void SetSpriteID(string id)
    {
        spriteId = id;
        if (id == null) return;

        if (!string.IsNullOrEmpty(spriteId))
        {
            if (SharedSettings.instance.uiAtlas != null)
            {
                sprite = img.sprite = SharedSettings.instance.uiAtlas.sprite(spriteId);
            }
        }

        if(sprite != null)
        {
            if (nativeSize)
            {
                //Utils.ResizeImage(img);
            }
            if (img.hasBorder || sliced)
            {
                img.type = Image.Type.Sliced;
            }
        }
    }

    public Image image
    {
        get { return img; }
    }
}
