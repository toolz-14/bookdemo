using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureCache : IDisposable
{
    public delegate void TextureEvent(TextureCache c);
    public event TextureEvent onLoadProgress;
    public event TextureEvent onLoaded;


    private List<string> _ids;
    private List<Texture2D> _textures;

    private int _numTextures = 0;
    private int _queueLength = 0;
    private float _loadProgress = 0;

    private List<string> _loadQueue;
    private LoadResource _load;


    //------------------| Construct / Dispose |

    public TextureCache()
    {
        _loadQueue = new List<string>();
        _ids = new List<string>();
        _textures = new List<Texture2D>();
    }

    public void Dispose()
    {
        if(_load!= null)
        {
            _load.Dispose();
            _load = null;
        }
        int i = _numTextures;
        while (--i > -1)
        {
            UnloadTexture(i);
        }
        _ids.Clear();
        _textures.Clear();
        _loadQueue.Clear();
        _ids = null;
        _textures = null;
        _loadQueue = null;
    }


    //------------------| Get / Add / Remove |

    public Texture2D GetTexture(string id)
    {
        int index = _ids.IndexOf(id);
        if (index == -1) return null;
        return _textures[index];
    }

    public void AddTexture(string id, Texture2D texture)
    {
        int index = _ids.IndexOf(id);
        if(index != -1)
        {
            Debug.LogError("TextureCache ERROR : Texture " + id + " already in cache");
            return;
        }
        //Debug.Log("TextureCache add texture " + id + ", " + texture);
        _ids.Add(id);
        _textures.Add(texture);
        _numTextures++;
    }

    public void RemoveTexture(string id, bool dispose = true)
    {
        int index = _ids.IndexOf(id);
        //Debug.Log("REMOVE TEXTURE .... " + id+", index = "+index);
        if (index == -1) return;
        UnloadTexture(index, dispose);
        _ids.RemoveAt(index);
        _numTextures--;

    }


    //------------------| Loading |

    public void Enqueue(string id, string url)
    {
        if (_loadQueue.Contains(id)) return;
        _loadQueue.Insert(0, url);
        _loadQueue.Insert(0, id);
    }

    public void LoadTextures()
    {
        if (_load == null)
        {
            _queueLength = _loadQueue.Count / 2;
            LoadNext();
        }
    }

    public void LoadTextures(params string[] idsAndUrls)
    {
        for(int i = 0 ; i<idsAndUrls.Length ; i+=2)
        {
            Enqueue(idsAndUrls[i], idsAndUrls[i + 1]);
            //_loadQueue.Insert(0, idsAndUrls[i+1]); // url
            //_loadQueue.Insert(0, idsAndUrls[i]); // id
        }

        LoadTextures();
    }

    private void LoadNext()
    {
        if (_load != null)
        {
            _load.Dispose();
            _load = null;
        }

        int index = _loadQueue.Count;
        if (index == 0)
        {
            // loaded
            _queueLength = 0;
            _loadProgress = 1;
            if (onLoadProgress != null) onLoadProgress(this);
            if (onLoaded != null) onLoaded(this);
            return;
        }

        _loadProgress = 1 - (_loadQueue.Count / 2 / _queueLength);
        if (onLoadProgress != null) onLoadProgress(this);


        _load = new LoadResource(_loadQueue[index - 1], false);
       // _load.onComplete += OnResourceLoaded;
       // _load.Execute();
    }

	private void OnResourceLoaded() //Command cmd
    {
        Texture2D texture = _load.asset as Texture2D;
        int n = _loadQueue.Count;

        AddTexture(_loadQueue[n - 2], texture);

        _loadQueue.RemoveAt(n - 1);
        _loadQueue.RemoveAt(n - 2);

        LoadNext();
    }


    //------------------| Unloading |

    private void UnloadTexture(int index, bool dispose = true)
    {
        Texture2D t = _textures[index];
        _textures.RemoveAt(index);
        if(dispose) Resources.UnloadAsset(t);
        t = null;
    }

    //------------------| Getters / Setters |

    public int numTextures
    {
        get { return _numTextures; }
    }

    public int queueLength
    {
        get { return _loadQueue.Count/2; }
    }

    public float loadProgress
    {
        get { return _loadProgress; }
    }








}
