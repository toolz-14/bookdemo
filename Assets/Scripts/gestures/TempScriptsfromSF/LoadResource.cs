using UnityEngine;
using System.Collections;

public class LoadResource //: Command
{
    protected bool _isTextAsset;
	protected string _path;
	protected ResourceRequest _request;

	protected bool _check;
	protected Object _asset;

    static private uint CMD_ID = 0;
//    private uint _cmdID = 0;

	//----------------------- Construct / Dispose

	public LoadResource(string path, bool isTextAsset) : base()
	{
      //  _cmdID = CMD_ID;
        CMD_ID++;

		_path = path;
        _isTextAsset = isTextAsset;
	}

	public void Dispose () //override
	{
		//base.Dispose ();
		_request = null;
        _path = null;
        _asset = null;
	}

	//---------------------- Start / Progress / Complete


	protected void Start () //override
	{
		//base.Start ();
		
		if(Application.HasProLicense())
		{
			// unity pro
			if (_isTextAsset)
			{
				_request = Resources.LoadAsync<TextAsset>(_path);
			}
			else
			{
				_request = Resources.LoadAsync(_path);
			}
			//Coroutines.instance.AddCoroutine("CheckProgress_"+_cmdID, CheckProgress());
            _check = true;
		}
		else
		{
			// unity free
			//Debug.Log ("load unity free");
			if (_isTextAsset)
			{
				_asset = Resources.Load<TextAsset>(_path);
			}
			else
			{
				_asset = Resources.Load(_path);
			}
			//Complete ();
		}
	}

	protected IEnumerator CheckProgress()
	{
        while(!_check)
        {
            yield return null;
        }
		while (_check)
		{
			// progress
			//Progress (_request.progress);

			if(_request.isDone){
                //Coroutines.instance.RemoveCoroutine("CheckProgress_" + _cmdID);
				_check = false;
				_asset = _request.asset;
				//Complete ();
                yield return null;
			}
			yield return new WaitForSeconds(.1f);
		}
		
	}

	//---------------------- Getters / Setters

	public Object asset{
		get{
			return _asset;
		}
	}
}

