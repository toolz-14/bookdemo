using UnityEngine;
using System.Collections;

public class ViewportManager 
{
//    private App app;
//    private UI ui;
//    private AppState stateId;
//
//    public Rect cameraViewport;
//    public Rect interactionViewport;
//
//    //------------------| Construct / Destroy |
//    /**
//     * Gère les changements de viewport pour la camera principale
//     * ainsi que les zones d'interaction avec le world
//     */
//    public ViewportManager(App app)
//    {
//        this.app = app;
//        ui = app.ui;
//        cameraViewport = new Rect();
//        interactionViewport = new Rect();
//
//        stateId = (app.state != null ? app.state.id : AppState.FavelaState);
//        ui.onResize += OnUiResize;
//    }
//
//
//    //------------------| Events |
//
//    private void OnUiResize(UI ui)
//    {
//        Update(true, true);
//    }
//
//    public void UpdateWithState(AppState stateId, bool applyCam, bool applyInteraction)
//    {
//        // met seulement à jour l'id du state
//        this.stateId = stateId;
//        Update(applyCam, applyInteraction);
//    }
//
//    public void Update(bool applyCam, bool applyInteraction)
//    {
//        UpdateCameraViewport(applyCam);
//        UpdateWorldInteractionViewport(applyInteraction);
//    }
//
//
//    //------------------| Utils |
//
//    /**
//     * Met à jour le viewport de la camera principale
//     * @param apply : applique le viewport
//     */
//    private void UpdateCameraViewport(bool apply)
//    {
//        switch(stateId)
//        {
//            case AppState.ProjectState :
//                cameraViewport.x = 0;
//                cameraViewport.y = 0;
//                cameraViewport.width = ui.w - ui.projectPanel.w + 2;
//                cameraViewport.height = ui.h;
//                break;
//            case AppState.DiscussionState:
//            case AppState.DiscussionStateFromProfile :
//                cameraViewport.x = 0;
//                cameraViewport.y = 0;
//                cameraViewport.width = ui.w/2;
//                cameraViewport.height = ui.h;
//                break;
//            case AppState.ProfileState:
//                cameraViewport.x = ui.profilePanel.w;
//                cameraViewport.y = 0;
//                cameraViewport.width = ui.w - cameraViewport.x;
//                cameraViewport.height = ui.h;
//                break;
//            case AppState.Map3DStateWithPanel:
//                cameraViewport.x = ui.mapPanel.w;
//                cameraViewport.y = 0;
//                cameraViewport.width = ui.w - cameraViewport.x;
//                cameraViewport.height = ui.h;
//                break;
//            case AppState.DefaultState :
//            case AppState.FavelaState :
//            default :
//                cameraViewport.x = 0;
//                cameraViewport.y = 0;
//                cameraViewport.width = ui.w;
//                cameraViewport.height = ui.h;
//                break;
//        }
//        // to ratios
//        cameraViewport.x /= ui.w;
//        cameraViewport.y /= ui.h;
//        cameraViewport.width /= ui.w;
//        cameraViewport.height /= ui.h;
//
//        if (apply && app.world.mainCamera != null)
//        {
//            app.world.mainCamera.rect = cameraViewport;
//        }
//    }
//
//    private void UpdateWorldInteractionViewport(bool apply)
//    {
//        switch (stateId)
//        {
//            case AppState.ProjectState:
//                interactionViewport.x = 0;
//                interactionViewport.y = 0;
//                interactionViewport.width = (ui.w - ui.projectPanel.w);
//                interactionViewport.height = ui.h;
//                break;
//            case AppState.DiscussionState:
//            case AppState.DiscussionStateFromProfile:
//                interactionViewport.x = 0;
//                interactionViewport.y = 0;
//                interactionViewport.width = (ui.w/2);
//                interactionViewport.height = ui.h;
//                break;
//            case AppState.ProfileState:
//                interactionViewport.x = ui.profilePanel.w;
//                interactionViewport.y = 0;
//                interactionViewport.width = (ui.w - cameraViewport.x);
//                interactionViewport.height = ui.h;
//                break;
//            case AppState.Map3DStateWithPanel:
//                interactionViewport.x = ui.mapPanel.w;
//                interactionViewport.y = 0;
//                interactionViewport.width = (ui.w - cameraViewport.x);
//                interactionViewport.height = ui.h;
//                break;
//            case AppState.DefaultState:
//            case AppState.FavelaState:
//            default :
//                interactionViewport.x = 55; // themes width
//                interactionViewport.y = 0;
//                interactionViewport.width = ui.w-interactionViewport.x;
//                interactionViewport.height = ui.h;
//                break;
//        }
//        //Debug.Log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx INTERACTION VIEWPORT = " + interactionViewport+", apply = "+apply+", world.interactor = "+app.world.interactor);
//        if (apply)
//        {
//            Gestures.instance.UpdateViewport("world", interactionViewport);
//        }
//    }


}
