using UnityEngine;
using System.Collections;

public class TextureAtlas : IDisposable
{
	// events
	public enum TextureAtlasEventType
	{
		Progress,
		Loaded
	};
	
	public delegate void TextureAtlasEvent(TextureAtlas atlas);
	public event TextureAtlasEvent onProgress;
	public event TextureAtlasEvent onLoaded;


	private string _name;
	private string _textureName;
	private string _texturePath;

	private Sprite[] _sprites;
	private string[] _names;

	private bool _loaded;
	private float _progress = 0;
	private int index;
	private int n;

	//--------------------------------- Construct / Dispose

	public TextureAtlas(string name)
	{
		_name = name;
	}

	public TextureAtlas(string name, string texturePath)
	{
		_name = name;
		_texturePath = texturePath;
	}

	public void Dispose()
	{
		if (!_loaded) return;
	}

	//--------------------------------- Description

	public override string ToString()
	{
		return "TextureAtlas (name="+_name+" numSprites="+n+")";
	}

	//--------------------------------- Load

	public void Load()
	{
		Load (null);
	}
	/**
	 * Charge les sprites de la sheet texturePath
	 * @param texturePath : le chemin vers la texture
	 */ 
	public void Load(string texturePath)
	{
		if (_loaded) return;

		if(_texturePath == null)_texturePath = texturePath;
		if (_texturePath == null) {
			Debug.Log ("TextureAtlas error, no base texture specified");
			return;
		}

		_progress = 0;
		if (onProgress != null) onProgress (this);

		_sprites = Resources.LoadAll<Sprite> (_texturePath);
		n = _sprites.Length;

		_names = new string[n];
		for (int i = 0; i<n; ++i) 
		{
			_names [i] = _sprites [i].name;
			//Debug.Log ("add sprite "+_names[i]+" texture name = "+_sprites[i].texture.name);
		}

		_progress = 1;
		if (onProgress != null) onProgress (this);
		if (onLoaded != null) onLoaded (this);

		_loaded = true;
	}

	//-------------------------------- Public

	
	public Sprite sprite(string name)
	{
		index = n;
		while (--index > -1) {
			if(_names[index].Equals (name)){
				//Debug.Log ("sprite found "+name);
				return _sprites[index];
			}
		}
		Debug.LogError ("Error getting sprite " + name + " in atlas" + _name);
		return null;
	}


	//-------------------------------- Getters / Setters

	public float progress{ get{ return _progress;} }
	public string name{ get{ return _name;} }
	public int numSprites { get{ return n;} }
	public Sprite[] sprites { get{ return _sprites;} }

}

