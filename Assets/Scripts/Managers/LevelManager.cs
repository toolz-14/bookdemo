﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Toolz.Managers {
	[AddComponentMenu("Toolz/Managers/LevelManager")]
	public class LevelManager : MonoBehaviour {

		public string SceneName = "";
		public GameObject ExitButton;
		private AsyncOperation _async;

        public static Camera ActiveCamera { get; set; }

        public static Transform ActiveCameraTransform { get; set; }

        private void Awake()
        {
            ActiveCamera = null;
            ActiveCameraTransform = null;
        }

        private void OnEnable() {
			if (SceneName != "") {
				StartCoroutine ("load");
			}
			if(ExitButton != null){
				ExitButton.SetActive(false);
			}
		}
		
		IEnumerator load() {
			yield return new WaitForEndOfFrame();

			_async = SceneManager.LoadSceneAsync(SceneName);
			_async.allowSceneActivation = false;

			yield return _async;
		}
		
		public void ActivateScene() {
			if(_async.progress >= 0.9f){
				_async.allowSceneActivation = true;
			}
		}

		private void Update(){
			if(_async != null && _async.progress >= 0.9f){
				if(ExitButton != null){
					ExitButton.SetActive(true);
				}
			}
		}

		private void Start () {
//			if(GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetBack360() > 0){
//				StartCoroutine("Back360");
//			}
		}

		IEnumerator Back360(){
//			if(!GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().BackFrom360()){
				yield return new WaitForEndOfFrame();
//				StartCoroutine("Back360");
//			}
		}

//		public void SetActiveCamera(Camera NewCamera){
//			foreach(Component component in ActiveCamera.GetComponents<Component>())
//			{
//				if(NewCamera.GetComponent<typeof(component)>() == null){
//					CopyComponent( component, NewCamera.gameObject);
//				}
//			}
//
//		}
//
//		private Component CopyComponent(Component original, GameObject destination)
//		{
//			System.Type type = original.GetType();
//			Component copy = destination.AddComponent(type);
//			// Copied fields can be restricted with BindingFlags
//			System.Reflection.FieldInfo[] fields = type.GetFields(); 
//			foreach (System.Reflection.FieldInfo field in fields)
//			{
//				field.SetValue(copy, field.GetValue(original));
//			}
//			return copy;
//		}

//		void test(){
//			Debug.Log("ActiveCamera.gameObject.name = "+ActiveCamera.gameObject.name);
//			foreach(Component component in ActiveCamera.GetComponents<Component>()){
//				Debug.Log(component.name);
//			}
//		}
	}
}
