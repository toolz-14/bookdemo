﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SaveLoadManager : MonoBehaviour {
	
	
	//---------------------- Save -----------------------------------------------------------------
	
	public static void SaveLastEmailEntered(string email){
		PlayerPrefs.SetString("lastEmailEntered", email);
	}

	public static void SaveLastUserNameEntered(string userName){
		PlayerPrefs.SetString("lastUserNameEntered", userName);
	}

	public static void SaveLastUserPWdEntered(string pwd){
		PlayerPrefs.SetString("lastUserPwdEntered", pwd);
	}

	//label editor objects
	public static void SaveLabelEditorObjects(string labelEditorObjectsValues){
		PlayerPrefs.SetString("AllLabelEditorObjects", labelEditorObjectsValues);
	}

	//Saved gps values
	public static void SaveGPSValues(string currentGPSValues){
		PlayerPrefs.SetString("GPSValues", currentGPSValues);
	}
    
    public static void SaveLogin(string data)
    {
        PlayerPrefs.SetString("UserLogin", data);
    }
    public static void SaveFirstname(string data)
    {
        PlayerPrefs.SetString("UserFirstname", data);
    }
    public static void SaveLastname(string data)
    {
        PlayerPrefs.SetString("UserLastname", data);
    }
    public static void SaveMail(string data)
    {
        PlayerPrefs.SetString("UserMail", data);
    }
    public static void SavePassword(string data)
    {
        PlayerPrefs.SetString("UserPassword", data);
    }
    public static void SaveAccountStatus(int data)
    {
        PlayerPrefs.SetInt("UserAccountStatus", data);
    }
    public static void SaveUserID(int data)
    {
        PlayerPrefs.SetInt("UserID", data);
    }


    //---------------------- Load -----------------------------------------------------------------

    public static string LoadLastEmailEntered(){
		
		if(!PlayerPrefs.HasKey("lastEmailEntered")){
			return null;
		}
		return PlayerPrefs.GetString("lastEmailEntered");
	}

	public static string LoadLastUserNameEntered(){
		
		if(!PlayerPrefs.HasKey("lastUserNameEntered")){
			return null;
		}
		return PlayerPrefs.GetString("lastUserNameEntered");
	}

	public static string LoadLastPwdNameEntered(){

		if(!PlayerPrefs.HasKey("lastUserPwdEntered")){
			return null;
		}
		return PlayerPrefs.GetString("lastUserPwdEntered");
	}

	//label editor objects
	public static string LoadLabelEditorObjects(){

		if(!PlayerPrefs.HasKey("AllLabelEditorObjects")){
			return null;
		}
		return PlayerPrefs.GetString("AllLabelEditorObjects");
	}

	//gps values
	public static string LoadGPSValues(){

		if(!PlayerPrefs.HasKey("GPSValues")){
			return null;
		}
		return PlayerPrefs.GetString("GPSValues");
    }
    
    public static string LoadLogin()
    {
        if (!PlayerPrefs.HasKey("UserLogin"))
        {
            return null;
        }
        return PlayerPrefs.GetString("UserLogin");
    }

    public static string LoadFirstname()
    {
        if (!PlayerPrefs.HasKey("UserFirstname"))
        {
            return null;
        }
        return PlayerPrefs.GetString("UserFirstname");
    }

    public static string LoadLastname()
    {
        if (!PlayerPrefs.HasKey("UserLastname"))
        {
            return null;
        }
        return PlayerPrefs.GetString("UserLastname");
    }

    public static string LoadMail()
    {

        if (!PlayerPrefs.HasKey("UserMail"))
        {
            return null;
        }
        return PlayerPrefs.GetString("UserMail");
    }
    public static string LoadPassword()
    {

        if (!PlayerPrefs.HasKey("UserPassword"))
        {
            return null;
        }
        return PlayerPrefs.GetString("UserPassword");
    }
    public static int LoadAccountStatus()
    {
        if (!PlayerPrefs.HasKey("UserAccountStatus"))
        {
            return -1;
        }
        return PlayerPrefs.GetInt("UserAccountStatus");
    }
    public static int LoadUserID()
    {
        if (!PlayerPrefs.HasKey("UserID"))
        {
            return -1;
        }
        return PlayerPrefs.GetInt("UserID");
    }
}
