﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
	public static GameManager instance = null;
	[HideInInspector]
	public int ProjectIndex = 10;

	public static int selectedBati;

	public static int AppartIndex;

	public static int EtageIndex;
	[HideInInspector]
	public string FloorButtonText = "";
	[HideInInspector]
	public string FlatTypeButtonText = "";
	[HideInInspector]
	public string SelectFlatButtonText = "";

	public static int CurrentStyleIndex;
	public static int CurrentGammeIndex;

	[HideInInspector]
	public Vector3 AppartPosition;
	[HideInInspector]
	public Vector3 CamHolderRotation;
	[HideInInspector]
	public Quaternion OrbitRotation;

	private string _wanted360View = "";

	private string _moduleFrom = "";
	private GameObject _moduleInitial;
	private Transform ModuleInitialView = null;

	[SerializeField]
	private int _backFrom360;
	[SerializeField]
	private int _backFromAppart;
	private bool _backFromAppartResult;

	private Transform tempBati;
//	private GoToBati tempGoToBati;
	private Transform floor;
	private Transform ActiveWhenModuleIsActive;

	private List<List<Transform>> FurnituresParentsContent = new List<List<Transform>>();
	[HideInInspector]
	public Dictionary<string, string> MatIDTypeSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDNameSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDColorSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDSizeSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDColorisSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDModelSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDRefSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDMatSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> MatIDClassSol = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDBathroomTypeName = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_1 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_2 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_3 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_4 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_5 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_6 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_7 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_8 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_9 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_10 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_11 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_12 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_13 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_14 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_15 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_16 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_17 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_18 = new Dictionary<string, string>();
	[HideInInspector]
	public Dictionary<string, string> BathroomTypeIDLine_19 = new Dictionary<string, string>();


	private void Awake(){
		//Check if instance already exists
		if (instance == null){
			
			//if not, set instance to this
			instance = this;
		}
		
		//If instance already exists and it's not this:
		else if (instance != this){
			
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);
		}
		
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);

		_backFromAppartResult = false;

	}

	private void Start(){
		FloorButtonText = "Test";
		FlatTypeButtonText = "Test";
		SelectFlatButtonText = "Test";
	}

	public void SetView360(string wantedView){
		_wanted360View = wantedView;
	}

	public string GetView360(){
		return _wanted360View;
	}

	public int GetBack360(){
		return _backFrom360;
	}

	public void SetBackFrom360(){
		_backFrom360 = 1;
	}

	public void SetViewAppart(string ModuleFrom, Vector3 pos, Vector3 rot){
		_moduleFrom  = ModuleFrom;
		AppartPosition = pos;
		CamHolderRotation = rot;
		_backFromAppart = 1;
	}

	public int GetBackAppart(){
		return _backFromAppart;
	}

	public void SetBackFromAppart(){
		_backFromAppart = 1;
	}

	public bool BackFromAppart(List<Transform> Etages){
		Debug.Log ("Gamemanager BackFromAppart(), _moduleFrom = "+_moduleFrom);

		if(_moduleFrom != ""){
			_backFromAppart = 0;

			if (GameObject.Find ("Module_Initial") != null && GameObject.Find ("Module_Initial").transform.Find ("ActiveWhenModuleIsActive") != null) {
//				//sphere camera local scale
//				Transform SphereCamera = GameObject.Find ("Module_Initial").transform.Find ("ActiveWhenModuleIsActive").Find("Sphere_Camera");
//				SphereCamera.localScale = new Vector3(126f, 126f, 126f);
				GameObject.Find ("Module_Initial").transform.Find ("ActiveWhenModuleIsActive").gameObject.SetActive (false);
				if (GameObject.Find (_moduleFrom) != null && GameObject.Find (_moduleFrom).transform.Find ("ActiveWhenModuleIsActive") != null) {
					GameObject.Find (_moduleFrom).transform.Find ("ActiveWhenModuleIsActive").gameObject.SetActive (true);
//					OnEnterAppartEditor (Etages);
					return true;
				}
			}
			return false;
		}
		return false;
	}

	private void OnEnterAppartEditor (List<Transform> Etages) {
		
	}

//	public void FillFloorsMatsInfos(CSVReaderBouyguesFloors CSVReaderBouyguesFloors){
//
//		MatIDTypeSol.Clear ();
//		MatIDNameSol.Clear ();
//		MatIDColorSol.Clear ();
//		MatIDSizeSol.Clear ();
//		MatIDColorisSol.Clear();
//		MatIDModelSol.Clear();
//		MatIDRefSol.Clear();
//		MatIDMatSol.Clear();
//		MatIDClassSol.Clear();
//
//		CSVReaderBouyguesFloors.ParseFile ();
//
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDTypeSol) {
//			MatIDTypeSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDNameSol) {
//			MatIDNameSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDColorSol) {
//			MatIDColorSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDSizeSol) {
//			MatIDSizeSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDColorisSol) {
//			MatIDColorisSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDModelSol) {
//			MatIDModelSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDRefSol) {
//			MatIDRefSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDMatSol) {
//			MatIDMatSol.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesFloors.MatIDClassSol) {
//			MatIDClassSol.Add(floor.Key, floor.Value);
//		}
//	}
//
//	public void FillSDBInfos(CSVReaderBouyguesBathroom CSVReaderBouyguesBathroom){
//
//		BathroomTypeIDBathroomTypeName.Clear ();
//		BathroomTypeIDLine_1.Clear ();
//		BathroomTypeIDLine_2.Clear ();
//		BathroomTypeIDLine_3.Clear ();
//		BathroomTypeIDLine_4.Clear ();
//		BathroomTypeIDLine_5.Clear ();
//		BathroomTypeIDLine_6.Clear ();
//		BathroomTypeIDLine_7.Clear ();
//		BathroomTypeIDLine_8.Clear ();
//		BathroomTypeIDLine_9.Clear ();
//		BathroomTypeIDLine_10.Clear ();
//		BathroomTypeIDLine_11.Clear ();
//		BathroomTypeIDLine_12.Clear ();
//		BathroomTypeIDLine_13.Clear ();
//		BathroomTypeIDLine_14.Clear ();
//		BathroomTypeIDLine_15.Clear ();
//		BathroomTypeIDLine_16.Clear ();
//		BathroomTypeIDLine_17.Clear ();
//		BathroomTypeIDLine_18.Clear ();
//		BathroomTypeIDLine_19.Clear ();
//
//		CSVReaderBouyguesBathroom.ParseFile ();
//
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDBathroomTypeName) {
//			BathroomTypeIDBathroomTypeName.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_1) {
//			BathroomTypeIDLine_1.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_2) {
//			BathroomTypeIDLine_2.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_3) {
//			BathroomTypeIDLine_3.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_4) {
//			BathroomTypeIDLine_4.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_5) {
//			BathroomTypeIDLine_5.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_6) {
//			BathroomTypeIDLine_6.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_7) {
//			BathroomTypeIDLine_7.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_8) {
//			BathroomTypeIDLine_8.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_9) {
//			BathroomTypeIDLine_9.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_10) {
//			BathroomTypeIDLine_10.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_11) {
//			BathroomTypeIDLine_11.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_12) {
//			BathroomTypeIDLine_12.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_13) {
//			BathroomTypeIDLine_13.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_14) {
//			BathroomTypeIDLine_14.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_15) {
//			BathroomTypeIDLine_15.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_16) {
//			BathroomTypeIDLine_16.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_17) {
//			BathroomTypeIDLine_17.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_18) {
//			BathroomTypeIDLine_18.Add(floor.Key, floor.Value);
//		}
//		foreach (KeyValuePair<string, string> floor in CSVReaderBouyguesBathroom.BathroomTypeIDLine_19) {
//			BathroomTypeIDLine_19.Add(floor.Key, floor.Value);
//		}
//	}
}