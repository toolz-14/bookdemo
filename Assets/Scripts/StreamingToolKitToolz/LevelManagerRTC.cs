﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Toolz.Managers {
	[AddComponentMenu("Toolz/Managers/LevelManagerRTC")]
	public class LevelManagerRTC : MonoBehaviour {

		public static Camera ActiveCamera { get; set; }

        public static Transform ActiveCameraTransform { get; set; }

        private void Awake () {
            ActiveCamera = null;
            ActiveCameraTransform = null;
        }
	}
}
