﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SimulateDropdown : MonoBehaviour {

    /// <summary>
    /// The peer list to operate on
    /// </summary>
    public PeerListState PeerList;


    /// <summary>
    /// A cached copy of the peer list used to diff and raise events on changes
    /// </summary>
    private List<PeerListState.Peer> previousPeerList = new List<PeerListState.Peer>();

    /// <summary>
    /// Unity engine object Update() hook
    /// </summary>
    private void Update()
    {
        if (this.PeerList.SelectedPeer != null)
        {
            return;
        }

        foreach (var peer in this.PeerList.Peers)
        {
            if (!this.previousPeerList.Contains(peer))
            {
                this.previousPeerList.Add(peer);
                this.PeerList.SelectedPeer = this.PeerList.Peers.First(p => p.Equals(peer));
            }
        }
    }
}
