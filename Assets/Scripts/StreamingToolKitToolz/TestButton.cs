﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestButton : MonoBehaviour {

    private bool swap;
    public Material cubeMat;

    void Start () {
		
	}
	
    public void OnbuttonClick()
    {
        swap = !swap;
        if (swap)
        {
            cubeMat.color = Color.blue;
        }
        else
        {
            cubeMat.color = Color.red;
        }
    }
}
