﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Horrible way of doing this but couldn't find another ... 
 * Have to handle all special character case i.e. backspace, enter, ...
 * Have to handle caret placement as well
 */

[RequireComponent(typeof(InputField))]
public class RTCInputField : MonoBehaviour {

    //public Text tex;
    private InputField infi;

    private void Start () {
        infi = GetComponent<InputField>();
    }
	
    //debug
    //public void DisplayInputFieldString()
    //{
    //    tex.text = infi.text;
    //}

    void Update()
    {
        if (CustomInputModule.useRTCinputStatic)
        {
            if (infi != null && infi.isFocused)
            {
                if (!RTCInput.inputString.Equals(""))
                {
                    if (RTCInput.inputString.Equals("enter"))
                    {
                        infi.onEndEdit.Invoke(infi.text);
                    }
                    else if (RTCInput.inputString.Equals("backspace"))
                    {
                        infi.text = infi.text.Substring(0, (infi.text.Length - 1));
                        infi.caretPosition = infi.text.Length;
                    }
                    else if (RTCInput.inputString.Equals("space"))
                    {
                        infi.text += " ";
                        infi.caretPosition = infi.text.Length;
                    }
                    else if (RTCInput.inputString.Equals("up"))
                    {
                        infi.caretPosition = infi.text.Length;
                    }
                    else if (RTCInput.inputString.Equals("down"))
                    {
                        infi.caretPosition = 0;
                    }
                    else if (RTCInput.inputString.Equals("left"))
                    {
                        infi.caretPosition -= 1;
                    }
                    else if (RTCInput.inputString.Equals("right"))
                    {
                        infi.caretPosition += 1;
                    }
                    else if (RTCInput.inputString.Equals("AltGraph") || RTCInput.inputString.Equals("Shift") || RTCInput.inputString.Equals("Control") || RTCInput.inputString.Equals("Tab") || RTCInput.inputString.Equals("CapsLock")
                        || RTCInput.inputString.Equals("escape") || RTCInput.inputString.Equals("Insert") || RTCInput.inputString.Equals("Delete") || RTCInput.inputString.Equals("Home") || RTCInput.inputString.Equals("End")
                        || RTCInput.inputString.Equals("PageUp") || RTCInput.inputString.Equals("PageDown") || RTCInput.inputString.Equals("NumLock") || RTCInput.inputString.Equals("Pause") || RTCInput.inputString.Equals("ScrollLock"))
                    {
                        //ignored for now
                    }
                    else if (RTCInput.inputString.Equals("F1") || RTCInput.inputString.Equals("F2") || RTCInput.inputString.Equals("F3") || RTCInput.inputString.Equals("F4") || RTCInput.inputString.Equals("F5")
                        || RTCInput.inputString.Equals("F6") || RTCInput.inputString.Equals("F7") || RTCInput.inputString.Equals("F8") || RTCInput.inputString.Equals("F9") || RTCInput.inputString.Equals("F10")
                        || RTCInput.inputString.Equals("F11") || RTCInput.inputString.Equals("F12"))
                    {
                        //ignored
                    }
                    else
                    {
                        infi.text += RTCInput.inputString;
                        infi.caretPosition = infi.text.Length;
                    }
                }
            }
        }
    }
}
