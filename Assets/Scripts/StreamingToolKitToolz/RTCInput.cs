﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class RTCInput
{
    private const float AXIS_NEUTRAL = 0.0f;
    private const float AXIS_POSITIVE = 1.0f;
    private const float AXIS_NEGATIVE = -1.0f;

    private const float m_gravity = 3.0f;
    private const float m_sensitivity = 3.0f;
    private const bool m_snap = true;
    private const bool m_invert = false;

    public static Text debugStaticTxt;

    public static float m_valueH
    {
        get;
        set;
    }

    public static float m_valueV
    {
        get;
        set;
    }

    public static bool simulateMouseWithTouches
    {
        get;
        set;
    }
    //Is any key or mouse button currently held down?
    public static bool anyKey
    {
        get;
        set;
    }
    //Returns true the first frame the user hits any key or mouse button.
    public static bool anyKeyDown
    {
        get;
        set;
    }
    //Returns the keyboard input entered this frame.
    public static string inputString
    {
        get;
        set;
    }
    //Last measured linear acceleration of a device in three-dimensional space.
    public static Vector3 acceleration
    {
        get;
        set;
    }
    //Returns list of acceleration measurements which occurred during the last frame. (Allocates temporary variables).
    public static AccelerationEvent[] accelerationEvents
    {
        get;
        set;
    }
    //Number of acceleration measurements which occurred during last frame.
    public static int accelerationEventCount
    {
        get;
        set;
    }
    //Returns list of objects representing status of all touches during last frame. (Allocates temporary variables).
    public static Touch[] touches
    {
        get;
        set;
    }
    //Number of touches. Guaranteed not to change throughout the frame.
    public static int touchCount
    {
        get;
        set;
    }
    //Indicates if a mouse device is detected.
    public static bool mousePresent
    {
        get;
        set;
    }
    //Property indicating whether keypresses are eaten by a textinput if it has focus (default true).
    //[Obsolete("eatKeyPressOnTextFieldFocus property is deprecated, and only provided to support legacy behavior.")]
    public static bool eatKeyPressOnTextFieldFocus
    {
        get;
        set;
    }
    //Returns true when Stylus Touch is supported by a device or platform.
    public static bool stylusTouchSupported
    {
        get;
        set;
    }
    //Returns whether the device on which application is currently running supports touch input.
    public static bool touchSupported
    {
        get;
        set;
    }
    //Property indicating whether the system handles multiple touches.
    public static bool multiTouchEnabled
    {
        get;
        set;
    }
    //Property for accessing device location (handheld devices only).
    public static LocationService location
    {
        get;
        set;
    }
    //Property for accessing compass (handheld devices only).
    public static Compass compass
    {
        get;
        set;
    }
    //Device physical orientation as reported by OS.
    public static DeviceOrientation deviceOrientation
    {
        get;
        set;
    }
    //Controls enabling and disabling of IME input composition.
    public static IMECompositionMode imeCompositionMode
    {
        get;
        set;
    }
    //The current IME composition string being typed by the user.
    public static string compositionString
    {
        get;
        set;
    }
    //Does the user have an IME keyboard input source selected?
    public static bool imeIsSelected
    {
        get;
        set;
    }
    //Bool value which let's users check if touch pressure is supported.
    public static bool touchPressureSupported
    {
        get;
        set;
    }
    // The current mouse scroll delta.
    public static Vector2 mouseScrollDelta
    {
        get;
        set;
    }
    // The current mouse position in pixel coordinates.
    public static Vector3 mousePosition
    {
        get;
        set;
    }
    //Returns default gyroscope.
    public static Gyroscope gyro
    {
        get;
        set;
    }
    //The current text input position used by IMEs to open windows.
    public static Vector2 compositionCursorPos
    {
        get;
        set;
    }
    //Should Back button quit the application? Only usable on Android, Windows Phone or Windows Tablets.
    public static bool backButtonLeavesApp
    {
        get;
        set;
    }
    //[Obsolete("isGyroAvailable property is deprecated. Please use SystemInfo.supportsGyroscope instead.")]
    public static bool isGyroAvailable
    {
        get;
        set;
    }
    //This property controls if input sensors should be compensated for screen orientation.
    public static bool compensateSensors
    {
        get;
        set;
    }

    //toolz to get value more easily for method below
    public static bool mouseLeftButtonPressed
    {
        get;
        set;
    }

    //toolz to get value more easily for method below
    public static bool mouseRightButtonPressed
    {
        get;
        set;
    }

    //toolz to get value more easily for method below
    public static bool mouseLeftButtonUp
    {
        get;
        set;
    }

    //toolz to get value more easily for method below
    public static bool mouseRightButtonUp
    {
        get;
        set;
    }

    //toolz to get value more easily for method below
    public static bool mouseLeftButtonHeld
    {
        get;
        set;
    }

    //toolz to get value more easily for method below
    public static bool mouseRightButtonHeld
    {
        get;
        set;
    }

    public static void SetInputs()
    {
        simulateMouseWithTouches = Input.simulateMouseWithTouches;
        
        acceleration = Input.acceleration;
        accelerationEvents = Input.accelerationEvents;
        accelerationEventCount = Input.accelerationEventCount;
        touches = Input.touches;
        touchCount = Input.touchCount;
        mousePresent = Input.mousePresent;

        

        stylusTouchSupported = Input.stylusTouchSupported;
        touchSupported = Input.touchSupported;
        multiTouchEnabled = Input.multiTouchEnabled;
        location = Input.location;
        compass = Input.compass;
        deviceOrientation = Input.deviceOrientation;

        

        touchPressureSupported = Input.touchPressureSupported;
        gyro = Input.gyro;
        compositionCursorPos = Input.compositionCursorPos;
        backButtonLeavesApp = Input.backButtonLeavesApp;
        //isGyroAvailable = Input.isGyroAvailable;
        isGyroAvailable = SystemInfo.supportsGyroscope;
        compensateSensors = Input.compensateSensors;

    }

    public static void SetInputsKeyboard()
    {
        anyKey = Input.anyKey;
        anyKeyDown = Input.anyKeyDown;
        //inputString = Input.inputString;

        eatKeyPressOnTextFieldFocus = Input.eatKeyPressOnTextFieldFocus;
        //eatKeyPressOnTextFieldFocus = true;

        imeCompositionMode = Input.imeCompositionMode;
        //imeCompositionMode = IMECompositionMode.On;

        //compositionString = Input.compositionString;

        imeIsSelected = Input.imeIsSelected;
        //imeIsSelected = true;
    }

    //     Returns specific acceleration measurement which occurred during last frame. (Does not allocate temporary variables).
    public static AccelerationEvent GetAccelerationEvent(int index)
    {
        return Input.GetAccelerationEvent(index);
    }
    //     Returns the value of the virtual axis identified by axisName.
    //[GeneratedByOldBindingsGenerator]
    public static float GetAxis(string axisName)
    {
        if (axisName.Equals("Horizontal"))
        {
            if (inputString.Equals("right"))
            {
                if (m_valueH < AXIS_NEUTRAL && m_snap)
                {
                    m_valueH = AXIS_NEUTRAL;
                }

                m_valueH += m_sensitivity * Time.deltaTime;
                if (m_valueH > AXIS_POSITIVE)
                {
                    m_valueH = AXIS_POSITIVE;
                }
            }
            else if (inputString.Equals("left"))
            {
                if (m_valueH > AXIS_NEUTRAL && m_snap)
                {
                    m_valueH = AXIS_NEUTRAL;
                }

                m_valueH -= m_sensitivity * Time.deltaTime;
                if (m_valueH < AXIS_NEGATIVE)
                {
                    m_valueH = AXIS_NEGATIVE;
                }
            }
            else
            {
                if (m_valueH < AXIS_NEUTRAL)
                {
                    m_valueH += m_gravity * Time.deltaTime;
                    if (m_valueH > AXIS_NEUTRAL)
                    {
                        m_valueH = AXIS_NEUTRAL;
                    }
                }
                else if (m_valueH > AXIS_NEUTRAL)
                {
                    m_valueH -= m_gravity * Time.deltaTime;
                    if (m_valueH < AXIS_NEUTRAL)
                    {
                        m_valueH = AXIS_NEUTRAL;
                    }
                }
            }
            return m_valueH;
        }
        if (axisName.Equals("Vertical"))
        {
            if (inputString.Equals("up"))
            {
                if (m_valueV < AXIS_NEUTRAL && m_snap)
                {
                    m_valueV = AXIS_NEUTRAL;
                }

                m_valueV += m_sensitivity * Time.deltaTime;
                if (m_valueV > AXIS_POSITIVE)
                {
                    m_valueV = AXIS_POSITIVE;
                }
            }
            else if (inputString.Equals("down"))
            {
                if (m_valueV > AXIS_NEUTRAL && m_snap)
                {
                    m_valueV = AXIS_NEUTRAL;
                }

                m_valueV -= m_sensitivity * Time.deltaTime;
                if (m_valueV < AXIS_NEGATIVE)
                {
                    m_valueV = AXIS_NEGATIVE;
                }
            }
            else
            {
                if (m_valueV < AXIS_NEUTRAL)
                {
                    m_valueV += m_gravity * Time.deltaTime;
                    if (m_valueV > AXIS_NEUTRAL)
                    {
                        m_valueV = AXIS_NEUTRAL;
                    }
                }
                else if (m_valueV > AXIS_NEUTRAL)
                {
                    m_valueV -= m_gravity * Time.deltaTime;
                    if (m_valueV < AXIS_NEUTRAL)
                    {
                        m_valueV = AXIS_NEUTRAL;
                    }
                }
            }
            return m_valueV;
        }

        return AXIS_NEUTRAL;

        //return Input.GetAxis(axisName);
    }
    //     Returns the value of the virtual axis identified by axisName with no smoothing filtering applied.
    //[GeneratedByOldBindingsGenerator]
    public static float GetAxisRaw(string axisName)
    {
        float value = 0.0f;
        if (inputString != null)
        {
            if (axisName.Equals("Horizontal"))
            {
                if (inputString.Equals("right"))
                {
                    value = 1.0f;
                }
                if (inputString.Equals("left"))
                {
                    value = -1.0f;
                }

                return value;

            }
            else if (axisName.Equals("Vertical"))
            {
                if (inputString.Equals("up"))
                {
                    value = 1.0f;
                }
                if (inputString.Equals("down"))
                {
                    value = -1.0f;
                }

                return value;
            }
            else if (axisName.Equals("Mouse X"))
            {
                return value;
            }
            else if (axisName.Equals("Mouse Y"))
            {
                return value;
            }
            else if (axisName.Equals("HorizontalAzerty"))
            {
                return value;
            }
            else if (axisName.Equals("VerticalAzerty"))
            {
                return value;
            }
            else if (axisName.Equals("VerticalLook"))
            {
                return value;
            }
            else if (axisName.Equals("HorizontalLook"))
            {
                return value;
            }
        }
        return value;

        //return Input.GetAxisRaw(axisName);
    }
    //Returns true while the virtual button identified by buttonName is held down.
    //[GeneratedByOldBindingsGenerator]
    public static bool GetButton(string buttonName)
    {
        return Input.GetButton(buttonName);
    }

    //Returns true during the frame the user pressed down the virtual button identified by buttonName.
    //[GeneratedByOldBindingsGenerator]
    public static bool GetButtonDown(string buttonName)
    {
        return Input.GetButtonDown(buttonName);
    }
    //Returns true the first frame the user releases the virtual button identified by buttonName.
    //[GeneratedByOldBindingsGenerator]
    public static bool GetButtonUp(string buttonName)
    {
        return Input.GetButtonUp(buttonName);
    }
    // Returns an array of strings describing the connected joysticks.
    //[GeneratedByOldBindingsGenerator]
    public static string[] GetJoystickNames()
    {
        return Input.GetJoystickNames();
    }
    //Returns true while the user holds down the key identified by name. Think auto fire.
    public static bool GetKey(string name)
    {
        return Input.GetKey(name);
    }

    // Returns true while the user holds down the key identified by the key KeyCode enum parameter.
    public static bool GetKey(KeyCode key)
    {
        return Input.GetKey(key);
    }
    //Returns true during the frame the user starts pressing down the key identified by the key KeyCode enum parameter.
    public static bool GetKeyDown(KeyCode key)
    {
        GetKey(key);
        return Input.GetKeyDown(key);
    }
    //Returns true during the frame the user starts pressing down the key identified by name.
    public static bool GetKeyDown(string name)
    {
        GetKey(name);
        return Input.GetKeyDown(name);
    }
    // Returns true during the frame the user releases the key identified by the key KeyCode enum parameter.
    public static bool GetKeyUp(KeyCode key)
    {
        return Input.GetKeyUp(key);
    }
    //Returns true during the frame the user releases the key identified by name.
    public static bool GetKeyUp(string name)
    {
        return Input.GetKeyUp(name);
    }
    //Returns whether the given mouse button is held down.
    //[GeneratedByOldBindingsGenerator]
    public static bool GetMouseButton(int button)
    {
        if (button == 0)
        {
            return mouseLeftButtonHeld;
        }
        else if (button == 1)
        {
            return mouseRightButtonHeld;
        }
        else //should only be button 2 left (middle)
        {
            return Input.GetMouseButton(2);
        }
    }
    //Returns true during the frame the user pressed the given mouse button.
    //[GeneratedByOldBindingsGenerator]
    public static bool GetMouseButtonDown(int button)
    {
        if (button == 0)
        {
            return mouseLeftButtonPressed;
        }
        else if (button == 1)
        {
            return mouseRightButtonPressed;
        }
        else //should only be button 2 left (middle)
        {
            return Input.GetMouseButton(2);
        }
    }
    //Returns true during the frame the user releases the given mouse button.
    //[GeneratedByOldBindingsGenerator]
    public static bool GetMouseButtonUp(int button)
    {
        if (button == 0)
        {
            return mouseLeftButtonUp;

        }
        else if (button == 1)
        {
            return mouseRightButtonUp;
        }
        else //should only be button 2 left (middle)
        {
            return Input.GetMouseButton(2);
        }
    }
    //Returns object representing status of a specific touch. (Does not allocate temporary variables).
    public static Touch GetTouch(int index)
    {
        return Input.GetTouch(index);
    }
    //Determine whether a particular joystick model has been preconfigured by Unity. (Linux-only).
    //[GeneratedByOldBindingsGenerator]
    public static bool IsJoystickPreconfigured(string joystickName)
    {
        //This fails on build with "error CS0117: `UnityEngine.Input' does not contain a definition for `IsJoystickPreconfigured'"
        //return Input.IsJoystickPreconfigured(joystickName);
        return false;
    }
    //Resets all input. After ResetInputAxes all axes return to 0 and all buttons return to 0 for one frame.
    //[GeneratedByOldBindingsGenerator]
    public static void ResetInputAxes()
    {
        Input.ResetInputAxes();
    }
}
