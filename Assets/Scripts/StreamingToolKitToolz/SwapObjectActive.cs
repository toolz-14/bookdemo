﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapObjectActive : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> setActiveObjects = new List<GameObject>();
    [SerializeField]
    private List<GameObject> setInactiveObjects = new List<GameObject>();


    private void Start()
    {

    }

    public void SwapToObject()
    {
        foreach (GameObject go in setInactiveObjects)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in setActiveObjects)
        {
            go.SetActive(true);
        }
    }

    public void SwapBack()
    {
        foreach (GameObject go in setInactiveObjects)
        {
            go.SetActive(true);
        }
        foreach (GameObject go in setActiveObjects)
        {
            go.SetActive(false);
        }
    }
}
