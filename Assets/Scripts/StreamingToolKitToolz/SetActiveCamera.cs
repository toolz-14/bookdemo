﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveCamera : MonoBehaviour
{
    [SerializeField]
    private Transform theCameraTransform;

    private void Start()
    {

    }

    private void OnEnable()
    {
        Invoke("SetActiveTransform", 0.1f);
    }

    public void SetActiveTransform()
    {
        if (theCameraTransform != null)
        {
            
            Toolz.Managers.LevelManager.ActiveCameraTransform = theCameraTransform;
           // Debug.Log("Toolz.Managers.LevelManager.ActiveCameraTransform.parent.parent.name = " + Toolz.Managers.LevelManager.ActiveCameraTransform.parent.parent.name);
            if (theCameraTransform.GetComponentInChildren<Camera>(true) != null)
            {
                if (!theCameraTransform.GetComponentInChildren<Camera>(true).enabled)
                {
                    theCameraTransform.GetComponentInChildren<Camera>(true).enabled = true;
                }

                if (Microsoft.Toolkit.ThreeD.WebRTCServerToolzFreecam.instance != null)
                {
                    Microsoft.Toolkit.ThreeD.WebRTCServerToolzFreecam.instance.LeftEye.CopyFrom(theCameraTransform.GetComponentInChildren<Camera>(true));
                    Microsoft.Toolkit.ThreeD.WebRTCServerToolzFreecam.instance.LeftEye.stereoTargetEye = StereoTargetEyeMask.Left;

                    theCameraTransform.GetComponentInChildren<Camera>(true).enabled = false;
                }
            }
        }
    }
}
