﻿using System;
using System.Collections.Generic;

namespace Toolz.Flats
{
    /// <summary>
    /// Représente une sauvegarde sur le serveur de la personnalisation d'un appartement
    /// par l'utilisateur.
    /// </summary>
    /// 
    [Serializable]
    public class Save
    {

        public int Id;
		public string Name;

        public string Created;
        /// <summary>
        /// L'utilisateur qui effectue la sauvegarde sur le serveur.
        /// </summary>
        public User User;
        /// <summary>
        /// La pièce sur laquelle l'utilisateur travaille.
        /// </summary>
        public Room Room;
        public Style Style;
        public Collection Collection;
        /// <summary>
        /// La liste des meubles positionnés dans l'appartement.
        /// </summary>
        public List<Furniture> Furnitures;

    }
}
