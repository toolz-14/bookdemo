﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Toolz.Flats
{
    /// <summary>
    /// Représente un projet de promotion immobilière.
    /// Par exemple un nouveau quartier à Millau.
    /// </summary>
    /// 
    [Serializable]
    public class Project
    {
        public int Id;
        public string Name;
        /// <summary>
        /// Les bâtiments contenus dans le projet.
        /// </summary>
        /// 
        [SerializeField]
        public List<Building> Buildings;
    }
}
