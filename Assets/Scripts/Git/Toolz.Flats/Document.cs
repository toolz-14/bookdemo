﻿using System;

namespace Toolz.Flats
{

    /// <summary>
    /// En cours d'analyse.
    /// Représente un document PDF a priori contenant les diverses infos
    /// d'une sauvegarde.
    /// </summary>
    /// 
    [Serializable]
    public class Document
    {
    }
}
