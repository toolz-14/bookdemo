﻿using System;

namespace Toolz.Flats
{
    /// <summary>
    /// Represente un appartement à un étage donné d'un immeuble donné
    /// d'un projet donné.
    /// </summary>
    /// 
    [Serializable]
    public class Room
    {
        public int Id;
        public string Name;
        

        /// <summary>
        /// L'appartement.
        /// </summary>
        public Flat Flat;

        
    }
}
