﻿using System;

namespace Toolz.Flats
{

    /// <summary>
    /// Represente un "style" dans la terminologie de Bouygues.
    /// </summary>
    /// 

    [Serializable]
    public class Style
    {
		public int Id;
		public string Designation;
    }
}
