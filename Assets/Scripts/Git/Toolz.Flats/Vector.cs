﻿using System;

namespace Toolz.Flats
{

    /// <summary>
    /// Représente la position d'un meuble dans un appartement.
    /// </summary>
    /// 
    [Serializable]
    public class Vector
    {
        public float x;
        public float y;
        public float z;
    }
}
