﻿using System;

namespace Toolz.Flats
{
    /// <summary>
    /// Représente un étage dans un bâtiment dans un projet.
    /// </summary>
    /// 
    [Serializable]
    public class Floor
    {
        public int Id;
        /// <summary>
        /// Le numéro de l'étage...
        /// </summary>
        public int Number;
        /// <summary>
        /// Le bâtiment.
        /// </summary>
        public Building Building;
    }
}
