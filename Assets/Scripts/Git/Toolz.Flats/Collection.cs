﻿
using System;

namespace Toolz.Flats
{
    /// <summary>
    /// Une collection, un "genre" selon Bouygues
    /// </summary>

    [Serializable]
    public class Collection
    {
        public int Id;
        public string Designation;
    }
}
