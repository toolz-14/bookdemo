﻿using System;

namespace Toolz.Flats
{

    /// <summary>
    /// Représente un meuble dans une sauvegarde.
    /// La banque de données des meubles est gérée chez eux.
    /// L'identifiant du meuble dans cette banque de donnée
    /// est simplement le nom. Car c'est le nom du fichier.
    /// </summary>
    /// 
    [Serializable]
    public class Furniture
    {
        public int Id;
        public string Name;
        /// <summary>
        /// La sauvegarde où l'on trouve ce meuble.
        /// </summary>
        /// 
        [NonSerialized]
        public Save Save;
        /// <summary>
        /// Sa position vectorisée dans l'appartement.
        /// </summary>
        public Vector VectorPosition;
        /// <summary>
        /// Axe de rotation de l'objet dans l'appartement.
        /// </summary>
        public Vector VectorRotation;

        public int StyleID;
        public int CollectionID;
        public int TypeID;
        public int ObjectID;

		//NEED DAMIEN TO ADD THESE
		public string FurnitureClassID;
		public string FloorID;
		public string FloorClassID;

		public string WallID;
		public string WallClassID;
		public Vector WallColor;

		public string SDB1_ID;
		public string SDB2_ID;
    }
}
