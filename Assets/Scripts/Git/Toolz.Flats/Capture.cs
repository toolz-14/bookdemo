﻿using System;

namespace Toolz.Flats
{

    /// <summary>
    /// Représente une capture d'écran (jpeg à sauvegarder sur le serveur)
    /// de l'appartement.
    /// A pousser sur les réseaux sociaux.
    /// Marc doit checker les formats "360" de facebook...
    /// </summary>
    /// 
    [Serializable]
    public class Capture
    {
        public int Id;
        public DateTime Created;
        public string Comment;
        // public User User;
        public String Path;
    }
}
