﻿using System;
using UnityEngine;

namespace Toolz.Flats
{
    /// <summary>
    /// Représente un bâtiment dans un projet.
    /// </summary>
    /// 
    [Serializable]
    public class Building
    {

        public int Id;

        [NonSerialized]
        public Project Project;
    }
}
