﻿using System;

namespace Toolz.Flats
{
    /// <summary>
    /// Represente un appartement à un étage donné d'un immeuble donné
    /// d'un projet donné.
    /// </summary>
    /// 
    [Serializable]
    public class Flat
    {
        public int Id;
        public string Name;
        /// <summary>
        /// L'étage de l'appartement.
        /// </summary>
        public Floor Floor;
    }
}
