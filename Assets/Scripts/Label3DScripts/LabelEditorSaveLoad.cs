﻿using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

//
//Needed to add this class as the loading needs to happen outside of the label editor
//
public class LabelEditorSaveLoad : MonoBehaviour
{

    public GameObject labelBase;
    public Transform labelParent;
    //[SerializeField]
    //private Slider slider;
    public PanelAtelier panelAtelier;
    //[SerializeField]
    //private Text sliderPanelText;
    [SerializeField]
    private bool loadAfterUpload; // boolean that indicated to refresh data
    [SerializeField]
    private GameObject globalErrorManager; // boolean that indicated to refresh data
    public static bool isAdmin = false;
    [SerializeField]
    private GameObject validationPopUp;

    //[HideInInspector]
    public List<LabelEditorObject> labelEditorObjectList = new List<LabelEditorObject>();

    [SerializeField]
    private SelectAndSaveLoadLabelsOnline selectAndSaveLoadLabelsOnline;

    // UI
    private bool blockButton;
    [SerializeField] private GameObject panelDisplayErrorConnexion;
    [SerializeField] private Text textError;
    [SerializeField] private GameObject panelOverlayMessage;

    //Web services to save and load labels
    private Toolz.WebServices.PostSavedLabels _postSavedLabels;
    private Toolz.WebServices.GetSavedLabels _getSavedLabels;
    private int uploadCounter;
    private string fulllLoadMasterString = "";

    private void Awake()
    {
        // WebService initialization
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _postSavedLabels = webService.GetComponent<Toolz.WebServices.PostSavedLabels>();
        _postSavedLabels.onComplete += _postSavedLabels_onComplete;
        _postSavedLabels.onError += _postSavedLabels_onError;
        _getSavedLabels = webService.GetComponent<Toolz.WebServices.GetSavedLabels>();
        _getSavedLabels.onComplete += _getSavedLabels_onComplete;
        _getSavedLabels.onError += _getSavedLabels_onError;

        loadAfterUpload = false;
        uploadCounter = 0;
    }

    private void Start()
    {
        ProfileUIManager.labelEditorSaveLoadDelegate += SetAllLabelsInterractableDelegate; 
        //LoadLabelEditorObjects();
    }

    // Tuto : check if object is selected 
    public void SetAllLabelsInterractableDelegate()
    {
        SetAllLabelsInterractable(isAdmin);
        // Remove action
        //ProfileUIManager.labelEditorSaveLoadDelegate -= SetAllLabelsInterractableDelegate;
    }

    void OnApplicationQuit()
    {
        // SaveLabelEditorObjects ();
    }

    void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus)
        {
            // SaveLabelEditorObjects ();
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            // SaveLabelEditorObjects ();
        }
    }

    public void ButtonSave()
    {

        panelOverlayMessage.SetActive(true);
        SaveLabelEditorObjects();
    }

    public void ButtonLoad()
    {
        // Get number of labels to load
        //_getNumberOfSavedLabels.UseWebService();

        _getSavedLabels.UseWebService();
    }

    private int FindInteractablelabelEditorObject()
    {
        int result = -1;
        if (labelEditorObjectList.Exists(x => x.interactable == true))
        {
            result = labelEditorObjectList.FindIndex(uploadCounter, x => x.interactable == true && x.isUpdated);
        }

        return result;
    }

    private void SaveLabelEditorObjects()
    {
        if (!blockButton)
        {
            if (labelEditorObjectList.Count > 0) // Save first label
            {
                int result = FindInteractablelabelEditorObject();

                if (result != -1)
                {
                    uploadCounter = result;
                    string SaveMasterString = "";
                  

                    //save label name and use '/' as separator for split on load
                    SaveMasterString += labelEditorObjectList[uploadCounter].LabelText.text + "/";
                    //save position
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.position.x.ToString() + "/";
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.position.y.ToString() + "/";
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.position.z.ToString() + "/";
                    //save scale
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.localScale.x.ToString() + "/";
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.localScale.y.ToString() + "/";
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.localScale.z.ToString() + "/";

                    LabelEditorObject labelEditorObject = labelEditorObjectList[uploadCounter].GetComponent<LabelEditorObject>();
                    //save center
                    SaveMasterString += labelEditorObject.centerActive + "/";
                    //save text color
                    SaveMasterString += labelEditorObject.textTitleColor.r + "/";
                    SaveMasterString += labelEditorObject.textTitleColor.g + "/";
                    SaveMasterString += labelEditorObject.textTitleColor.b + "/"; // cannot change alpha
                                                                                  //save bg color
                    SaveMasterString += labelEditorObject.ImageHeaderColor.r + "/";
                    SaveMasterString += labelEditorObject.ImageHeaderColor.g + "/";
                    SaveMasterString += labelEditorObject.ImageHeaderColor.b + "/"; // cannot change alpha

                    //save rotation + new symbol to split by object
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.eulerAngles.x.ToString() + "/";
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.eulerAngles.y.ToString() + "/";
                    SaveMasterString += labelEditorObjectList[uploadCounter].transform.eulerAngles.z.ToString() +"/";

                    SaveMasterString += labelEditorObjectList[uploadCounter].description;

                    SaveMasterString = SaveMasterString.Replace('_', ' ');

                    //remove last '_' to make load split easier
                    //SaveMasterString.Substring(0, SaveMasterString.Length - 1);

                    //save to playeprefs
                    //SaveLoadManagerLabel3D.SaveLabelEditorObjects (SaveMasterString);

                    //Saving in database in "bagneux2019_labels_save" table
                    _postSavedLabels.id = labelEditorObjectList[uploadCounter].id;
                    _postSavedLabels.phase = labelEditorObjectList[uploadCounter].phase;
                    _postSavedLabels.category = labelEditorObjectList[uploadCounter].category;
                    _postSavedLabels.labelStatus = labelEditorObjectList[uploadCounter].status;
                    _postSavedLabels.saveMasterString = SaveMasterString;
                    _postSavedLabels.isDeleted = labelEditorObjectList[uploadCounter].isDeleted;
                    _postSavedLabels.idAuthor = labelEditorObjectList[uploadCounter].idAuthor;


                    //loader.SetActive(true);
                    _postSavedLabels.UseWebService();
                }
                else
                {
                    DisplayErrorMessage("No labels to save");
                    panelOverlayMessage.SetActive(false);
                    uploadCounter = 0;
                    loadAfterUpload = true;
                }

            }
            else
            {
                DisplayErrorMessage("No labels to save");
            }
        }

        Invoke("DelayButton", 0.01f);
    }

    private void DelayButton()
    {
        blockButton = false;
    }

    private void LoadLabelEditorObjects(string LoadMasterString)
    {
        //string LoadMasterString = "";
        //LoadMasterString = SaveLoadManagerLabel3D.LoadLabelEditorObjects();

        if (LoadMasterString == null)
        {
            //nothing saved
            Debug.Log("Can't Load labels, nothing was saved");
        }
        else if (LoadMasterString == "")
        {
            //problem with save or load
            Debug.Log("Can't Load labels, LoadMasterString empty");
        }
        else
        {
            //clear object list
            DestroyAllLabels();

            string[] theSplitString = LoadMasterString.Split('_');

            			//for debug
            			//for (int i = 0; i < theSplitString.Length; i++) {
            			//	Debug.Log ("theSplitstring[i] = " + theSplitString [i]);
            			//}

            for (int i = 0; i < theSplitString.Length; i++)
            {
                if (theSplitString[i] != "")
                {
                    //a single label object is 7 string values
                    string[] theSubSplitString = theSplitString[i].Split('/');

                    //instantiate proper object type
                    GameObject TempObject = Instantiate(labelBase) as GameObject;
                    // TempObject.GetComponent<BoxCollider>().enabled = false;
                    TempObject.SetActive(true);
                    TempObject.transform.SetParent(labelParent);

                    //set name
                    TempObject.GetComponent<LabelEditorObject>().LabelText.text = theSubSplitString[0];

                    //place object
                    Vector3 TempV3 = Vector3.zero;
                    TempV3.x = float.Parse(theSubSplitString[1]);
                    TempV3.y = float.Parse(theSubSplitString[2]);
                    TempV3.z = float.Parse(theSubSplitString[3]);
                    TempObject.transform.position = TempV3;

                    //scale object
                    TempV3.x = float.Parse(theSubSplitString[4]);
                    TempV3.y = float.Parse(theSubSplitString[5]);
                    TempV3.z = float.Parse(theSubSplitString[6]);
                    TempObject.transform.localScale = TempV3;

                    // Center object
                    LabelEditorObject labelEditorObject = TempObject.GetComponent<LabelEditorObject>();
                    labelEditorObject.centerActive = bool.Parse(theSubSplitString[7]);
                    TempObject.transform.Find("Empty_Center").Find("Image_Background").gameObject.SetActive(labelEditorObject.centerActive);
                    TempObject.transform.Find("Empty_Center").Find("Image_Stroke").gameObject.SetActive(labelEditorObject.centerActive);

                    // Text Color
                    Color tempColor = new Color
                    {
                        r = float.Parse(theSubSplitString[8]),
                        g = float.Parse(theSubSplitString[9]),
                        b = float.Parse(theSubSplitString[10]),
                        a = 1
                    };
                    labelEditorObject.ChangeTextColor(tempColor);

                    // Bg Color
                    tempColor = new Color
                    {
                        r = float.Parse(theSubSplitString[11]),
                        g = float.Parse(theSubSplitString[12]),
                        b = float.Parse(theSubSplitString[13]),
                        a = 1
                    };
                    labelEditorObject.ChangeBgColor(tempColor);

                    //rotate object
                    TempV3.x = float.Parse(theSubSplitString[14]);
                    TempV3.y = float.Parse(theSubSplitString[15]);
                    TempV3.z = float.Parse(theSubSplitString[16]);
                    TempObject.transform.eulerAngles = TempV3;

                    TempObject.GetComponent<LabelEditorObject>().description = theSubSplitString[17].Replace("BREAKLINE","\n");

                    TempObject.GetComponent<LabelEditorObject>().id = _getSavedLabels.labelJSONObject.labelsList[i].id;
                    TempObject.GetComponent<LabelEditorObject>().phase = _getSavedLabels.labelJSONObject.labelsList[i].phase;
                    TempObject.GetComponent<LabelEditorObject>().category = _getSavedLabels.labelJSONObject.labelsList[i].category;
                    TempObject.GetComponent<LabelEditorObject>().idAuthor = _getSavedLabels.labelJSONObject.labelsList[i].idAuthor;
                    TempObject.GetComponent<LabelEditorObject>().publishedTime = _getSavedLabels.labelJSONObject.labelsList[i].publishedTime;
                    TempObject.GetComponent<LabelEditorObject>().status = _getSavedLabels.labelJSONObject.labelsList[i].status;
                    TempObject.GetComponent<LabelEditorObject>().authorName = _getSavedLabels.labelJSONObject.labelsList[i].authorName;
                    TempObject.GetComponent<LabelEditorObject>().nbNonvalidatedElem = _getSavedLabels.labelJSONObject.labelsList[i].nbComments;
                    TempObject.GetComponent<LabelEditorObject>().isDeleted = false;

                    //add to object list
                    labelEditorObjectList.Add(TempObject.GetComponent<LabelEditorObject>());
                }
            }
            if (!loadAfterUpload)
            {
                DisplayLabelByPhase();
                selectAndSaveLoadLabelsOnline.DisplayLabelButton(); // and all labels are not interractable
            }
            else
            {
                loadAfterUpload = false;
                DisplayLabelByPhase();
            }
            SetAllLabelsInterractable(isAdmin);
        }
    }

    public void DestroyAllLabels()
    {
        if (labelEditorObjectList != null && labelEditorObjectList.Count > 0)
        {
            foreach (LabelEditorObject label in labelEditorObjectList)
            {
                Destroy(label.gameObject);
            }
            //clear object list
            labelEditorObjectList.Clear();
        }

    }

    public void MaskAllLabels()
    {
        if (labelEditorObjectList != null && labelEditorObjectList.Count > 0)
        {
            foreach (LabelEditorObject label in labelEditorObjectList)
            {
                label.gameObject.SetActive(false);
            }
        }
        //sliderPanelText.text = "Phase : " + slider.value;
    }

    public void DisplayLabelByPhase()
    {
        MaskAllLabels();
        if (ShowHideChatLabelsZAC.isShowing & labelEditorObjectList != null && labelEditorObjectList.Count > 0)
        {
            
            // Find all labels
           // List<LabelEditorObject> results = labelEditorObjectList.FindAll(x => x.phase == slider.value);
            List<LabelEditorObject> results = labelEditorObjectList.FindAll(x => x.phase == panelAtelier.currentSelectedButton);
            if (results.Count != 0)
            {
                foreach (LabelEditorObject label in results)
                {
                    if (label.isDeleted == false && label.status == 1)
                        label.gameObject.SetActive(true);
                }
            }
            else
            {
                //Debug.Log("no result");
            }
            /*
            foreach (LabelEditorObject label in labelEditorObjectList)
            {
                label.gameObject.SetActive(false);
            }
            labelEditorObjectList[0].gameObject.SetActive(true);*/
        }
    }

    public void ActiveLabelCollider(bool value)
    {
        foreach (LabelEditorObject label in labelEditorObjectList)
        {
            label.GetComponent<BoxCollider>().enabled = value;
        }
    }


    public void SetAllLabelsInterractable(bool value)
    {
        if (labelEditorObjectList != null)
        {
            foreach (LabelEditorObject label in labelEditorObjectList)
            {
                label.interactable = value;
            }
        }
    }

    public void DisplayErrorMessage(string msg)
    {
        panelDisplayErrorConnexion.SetActive(true);
        textError.text = msg;
    }

    // WEB SERVICES CALLBACKS

    private void _postSavedLabels_onError(long status, string message)
    {
        panelOverlayMessage.SetActive(false);
        Debug.Log("_postSavedLabels_onError, status :" + status + ", message :" + message);
        uploadCounter = 0;
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            // Display error message
            panelDisplayErrorConnexion.SetActive(true);
            textError.text = "Vous n'avez pas accès à internet, veuillez réessayez.";
        }
        /*
        if (loader != null)
        {
            loader.SetActive(false);
        }*/
    }

    private void _postSavedLabels_onComplete(long status, string message)
    {
        Debug.Log("_postSavedLabels_onComplete");
        labelEditorObjectList[uploadCounter].interactable = false; // previous label has been saved
        uploadCounter++;

        // Didn't find a better solution, when _postSavedLabelsService has finished to call UseWebService(), it will broadcast _postUserSaveService_onComplete to all instances
        string SaveMasterString = "";
        if (labelEditorObjectList.Count > 0 && uploadCounter<labelEditorObjectList.Count)
        {
            int result = FindInteractablelabelEditorObject();

            if (result != -1)
            {
                uploadCounter = result;

                //save label name and use '/' as separator for split on load
                SaveMasterString += labelEditorObjectList[uploadCounter].LabelText.text + "/";

                //save position
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.position.x.ToString() + "/";
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.position.y.ToString() + "/";
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.position.z.ToString() + "/";
                //save scale
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.localScale.x.ToString() + "/";
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.localScale.y.ToString() + "/";
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.localScale.z.ToString() + "/";

                LabelEditorObject labelEditorObject = labelEditorObjectList[uploadCounter].GetComponent<LabelEditorObject>();
                //save center
                SaveMasterString += labelEditorObject.centerActive + "/";
                //save text color
                SaveMasterString += labelEditorObject.textTitleColor.r + "/";
                SaveMasterString += labelEditorObject.textTitleColor.g + "/";
                SaveMasterString += labelEditorObject.textTitleColor.b + "/"; // cannot change alpha
                                                                              //save bg color
                SaveMasterString += labelEditorObject.ImageHeaderColor.r + "/";
                SaveMasterString += labelEditorObject.ImageHeaderColor.g + "/";
                SaveMasterString += labelEditorObject.ImageHeaderColor.b + "/"; // cannot change alpha

                //save rotation + new symbol to split by object
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.eulerAngles.x.ToString() + "/";
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.eulerAngles.y.ToString() + "/";
                SaveMasterString += labelEditorObjectList[uploadCounter].transform.eulerAngles.z.ToString() + "/";

                //save description
                SaveMasterString += labelEditorObjectList[uploadCounter].description;
                SaveMasterString = SaveMasterString.Replace('_', ' ');

                //Saving in database in "bagneux2019_labels_save" table
                _postSavedLabels.id = labelEditorObjectList[uploadCounter].id;
                _postSavedLabels.phase = labelEditorObjectList[uploadCounter].phase;
                _postSavedLabels.labelStatus = labelEditorObjectList[uploadCounter].status;
                _postSavedLabels.category = labelEditorObjectList[uploadCounter].category;
                _postSavedLabels.saveMasterString = SaveMasterString;
                _postSavedLabels.isDeleted = labelEditorObjectList[uploadCounter].isDeleted;
                _postSavedLabels.idAuthor = labelEditorObjectList[uploadCounter].idAuthor;

                
                Invoke("PostUseWebService", 1f);
            }
            else
            {// Usually this function is never called
                panelOverlayMessage.SetActive(false);
                uploadCounter = 0;
                //DisplayErrorMessage("Upload finished");
                loadAfterUpload = true;
                DestroyAllLabels();
                _getSavedLabels.UseWebService();
            }
        }
        else
        {
            panelOverlayMessage.SetActive(false);
            uploadCounter = 0;
            //DisplayErrorMessage("Upload finished");
            Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>();
            if ((Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus != Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN)
            {
                validationPopUp.SetActive(true);
                validationPopUp.GetComponent<CommentValidationPopUpPanel>().StartOpenPanel();
            }

            loadAfterUpload = true;
            DestroyAllLabels();
            _getSavedLabels.UseWebService();
        }
    }

    private void PostUseWebService()
    {
        _postSavedLabels.UseWebService();
    }


    private void _getSavedLabels_onError(long status, string message)
    {
        Debug.Log("_getSavedLabels_onError, status :" + status + ", message :" + message);
        fulllLoadMasterString = "";
        globalErrorManager.SetActive(true);
        GlobalErrorManager globalErrorManagerScript = globalErrorManager.GetComponent<GlobalErrorManager>();
        globalErrorManagerScript.newMsg = "Une erreur est survenue lors du chargement, veuillez rééssayer";
        globalErrorManagerScript.OpenGlobalErrorPanel();
    }

    private void _getSavedLabels_onComplete(long status, string message)
    {
        //Debug.Log("_getSavedLabels_onComplete, status :" + status + ", message : Load success");
        LoadLabelEditorObjects(_getSavedLabels.labelJSONObject.fulllLoadMasterString);
    }

}
