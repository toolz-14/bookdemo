﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Gere les mouvement du label
public class LabelEditorObject : MonoBehaviour {

	public Outline Outline;
    public Text LabelText;
    public string labelTextModeration;
    public Image imageHeader;

    [SerializeField] private Camera theCamera;

    public static bool startUpdating;
    public bool interactable;
    public bool centerActive;
    [HideInInspector]
	public float SavedObjectHeight;

    public int id;
    public int idAuthor;
    public string authorName;
    public string description;
    public int phase;
    public int category;
    public int status;
    public int nbNonvalidatedElem;
    public DateTime publishedTime;
    public bool isDeleted;

    private bool isDragged;
    private bool isClicked;
    [HideInInspector]
    public bool isUpdated =false;
    private bool hasBeenClicked;
	private float DelayForDrag = 0.2f;
	private float timeMouseDown = 0f;
	private RaycastHit hit;
	private Ray ray;
	private float distance;
	private LabelEditor theLabelEditor;

    public Color textTitleColor;
    public Color ImageHeaderColor;

    // Tuto value
    private bool isChecking;
    private bool dragedAndDrop;
    public static bool dragAndDropSucced;

    public enum Category { ENERGIE = 0, TRANSPORT = 1, ARCHITECTURE = 2 };

    private void Start () {
		theLabelEditor = transform.parent.GetComponent<LabelEditorWorld>().LabelEditor;
		startUpdating = false;

        TutoLabelEditor.labelEditorDelegate += TutoForceOnmouseUp;
        InvokeRepeating("MyUpdate", 0f, 0.02f);
    }

    private void MyUpdate()
    {
        if (Camera.main != null)
        {
            transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward,
                Camera.main.transform.rotation * Vector3.up);
        }
        else
        {
            Debug.Log("The active camera should be tagged with MainCamera ");
        }
    }

    //only update label values if editor open
    private void Update(){
		if (LabelEditor.isOpen && startUpdating) {
			if (LabelEditor.selectedLabel == null) {
				Outline.enabled = false;
			}

			if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Began) {
				ray = theCamera.ScreenPointToRay (Input.GetTouch (0).position);
			} else {
				ray = theCamera.ScreenPointToRay (Input.mousePosition);
			}

			if (Physics.Raycast (ray, out hit, 100000f))
            {
                if (hit.transform.tag == "EditableLabel")
                {
                    if (isClicked)
                    {
                        //deselect previous if one selected
                        if (LabelEditor.selectedLabel != null) {
							LabelEditor.selectedLabel.GetComponent<LabelEditorObject> ().Outline.enabled = false;
							LabelEditor.selectedLabel = null;
						}
                        isUpdated = true;

                        Outline.enabled = true;
                        LabelEditor.selectedLabel = gameObject; // Give Editor new selected label
                        theLabelEditor.ResetLabelEditorValues();
                        LabelText.color = textTitleColor;
                        imageHeader.color = ImageHeaderColor;

                        timeMouseDown += Time.deltaTime;
					}
					if (timeMouseDown >= DelayForDrag) {
						isDragged = true;
					}
				}
			}
            if (isDragged&& interactable)
            {
                ray = theCamera.ScreenPointToRay(Input.mousePosition);
                Plane plane = new Plane(Vector3.up, transform.position);
                float distance;

                if (plane.Raycast(ray, out distance))
                {
                    FreeTerrainCameraLabel3D.BlockCameraMovement = true;
                    Vector3 temp = ray.GetPoint(distance);
                    transform.position = temp;
                }
            }
        }
	}

	private void OnMouseDown(){
        PressedMouseDown();
    }
    
    public void PressedMouseDown()
    {
        if (interactable)
        {
            isClicked = true;
            startUpdating = true;
            SavedObjectHeight = transform.localPosition.y;
            //need to reset slider value
        }
    }

	private void OnMouseUp()
    {
        PressedMouseUp();
    }

    public void PressedMouseUp()
    {
        if (interactable)
        {
            isClicked = false;
            isDragged = false;
            startUpdating = false;
            timeMouseDown = 0f;
            FreeTerrainCameraLabel3D.BlockCameraMovement = false;
        }

    }

    // Tuto : check if object is selected 
    public void TutoForceOnmouseUp()
    {
        isClicked = false;
        isDragged = false;
        startUpdating = false;
        timeMouseDown = 0f;
        FreeTerrainCameraLabel3D.BlockCameraMovement = false;

        // Remove action
        TutoLabelEditor.labelEditorDelegate -= TutoForceOnmouseUp;
        //Replace by next action
        TutoLabelEditor.labelEditorDelegate = CheckDrag;
    }

    // Tuto : check if object is dragged for at least 5s
    public void CheckDrag()
    {
        if(!dragedAndDrop && !dragAndDropSucced && !isChecking)
        {
            isChecking = true;
            Invoke("CheckDragForSeconds", 0.5f);
        }
        if (dragedAndDrop&& !isClicked) // dragged is done and user has release left click
        {
            dragAndDropSucced = true;
            TutoLabelEditor.labelEditorDelegate -= CheckDrag;
        }
    }

    private void CheckDragForSeconds()
    {
        isChecking = false;
        if (isDragged)
        {
            dragedAndDrop = true;
        }
    }

    public void ChangeTextColor(Color c)
    {
        textTitleColor = c;
        LabelText.color = c;
    }

    public void ChangeBgColor(Color c)
    {
        ImageHeaderColor = c;
        imageHeader.color = c;
    }

}
