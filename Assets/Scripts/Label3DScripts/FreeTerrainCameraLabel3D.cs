using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Xml;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class FreeTerrainCameraLabel3D : BaseCamera
{
	public float SpeedZoom= 1f;
	public float SpeedMove= 1f;
	public bool isOrtographicCam = false;
	private float Speed = 1f;
	private float ZoomingSpeed = 1f;
	public bool fixedElevation = false;
	public float minElevation = 0;
	public float maxElevation = 500;
	public float elevationOffset = 10;
	public float minRX = 20;
	public float maxRX = 90;
	public MeshFilter terrain;
	public string heightMapID;
	public Vector2 heightMapOffset = new Vector2(0, 0);

	private Texture2D _heightMap;

	public bool hasPositionConstrains = false;
	public Vector3 minPosConstrain = Vector3.zero;
	public Vector3 maxPosConstrain = Vector3.zero;

	protected Vector3 _rxOffset = Vector3.zero;

	protected Vector3 _min;
	protected Vector3 _max;
	protected Vector3 _drag;

	protected Vector2 _textureSize;
	protected Vector3 _posRatio;
	protected float _elevation = 100;
	protected float _altitude = 0;
	protected float _minAltitude;
	protected float _altitudeRatio = 0;

	private float _speedMult = 1f;
	private bool _autoElevationRotation = true;

	private Vector3 _tempVect3;
	private Vector3 _newZoomPosition;

	private bool _updatePosition = true;
	[HideInInspector]
	public bool IsMovingByGesture = true;

	#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
	#endif
	public static bool ForwardCollision;
	public static bool BlockCameraMovement;
	private bool JustExitedBlock;

	private Vector3 MySavedPos;
	private Vector3 MySavedEuler;
	private bool PreventAllInput;
	private float myaltitude = 0;


	//------------------| Awake |

	protected override void Awake()
	{
		base.Awake();
		JustExitedBlock = false;
		BlockCameraMovement = false;
		_posRatio = Vector3.zero;
		_drag = Vector3.zero;
		_min = Vector3.zero;
		_max = Vector3.zero;
		PreventAllInput = false;
	}

	protected override void Start () 
	{
		_position = _cameraHolder.position;
		_rotation = _cameraHolder.rotation.eulerAngles;
		_cameraHolder.position = _position;
		_cameraHolder.rotation = Quaternion.Euler(_rotation);
		base.Start();

	}

	protected override void OnEnable()
	{
		base.OnEnable();
		if (_cameraHolder == null) {
			return;
		}
		_updatePosition = true;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		_updatePosition = false;
	}

	protected override void Update()
	{
		base.Update();
		dt *= _speedMult;
		if (BlockCameraMovement) {
			JustExitedBlock = true;
			return;
		} else {
			if(JustExitedBlock){
				JustExitedBlock = false;
				_position = _cameraHolder.position;
				_rotation = _cameraHolder.rotation.eulerAngles;

				_cameraHolder.position = _position;
				_cameraHolder.rotation = Quaternion.Euler(_rotation);
			}
		}
		if (EventSystem.current != null) {
			if (IsPointerOverUIObject () == 5 ) {
				JustExitedBlock = true;
				return;
			}else {
				if(JustExitedBlock){
					JustExitedBlock = false;
					_position = _cameraHolder.position;
					_rotation = _cameraHolder.rotation.eulerAngles;

					_cameraHolder.position = _position;
					_cameraHolder.rotation = Quaternion.Euler(_rotation);
				}
			}
		}

		_tempVect3 = Vector3.Lerp (_cameraHolder.position, _position, moveSpeed * dt);
		if (!AuthorizeMove (_tempVect3, false)) {
			_updatePosition = false;
			_position = _cameraHolder.position;
		} else {
			if (Physics.Linecast (_cameraHolder.position, _tempVect3) ) {
				_updatePosition = false;
				_position = _cameraHolder.position;
			}else{
				AuthorizeMove (_tempVect3, true);
				_updatePosition = true;
			}
		}

		_altitude = _cameraHolder.position.y;
		// terrain min altitude
		_altitude = _min.y + (_max.y - _min.y) * _altitude; 
		if (_elevation < _altitude)
			_elevation = _altitude;

		_altitudeRatio = ((_elevation - _altitude) / (maxElevation - _altitude));

		moveSens = (.2f + .8f * _altitudeRatio) * SpeedMove;
		zoomSens = (.5f + .5f * _altitudeRatio) * SpeedZoom;
		rotateSens = .4f + .6f * _altitudeRatio;

		_cameraHolder.rotation = Quaternion.Lerp (_cameraHolder.rotation, Quaternion.Euler (_rotation), rotateSpeed * dt);

	}

    public void ForceCameraPosition(Transform pos)
    {
        if (_cameraHolder != null)
        {
            PreventAllInput = true;
            BlockCameraMovement = true;

            _cameraHolder.position = pos.position;
            _cameraHolder.rotation = pos.rotation;

            _position = _cameraHolder.position;
            _rotation = _cameraHolder.rotation.eulerAngles;

            PreventAllInput = false;
            BlockCameraMovement = false;
        }
    }


    public override void MoveTo(Transform t){
		MoveTo(t.position, t.eulerAngles);
	}

	public override void MoveTo(Vector3 position, Vector3 eulerAngles){
		_position.x = position.x;
		_position.z = position.z;
		_position.y = position.y;

		_rotation = eulerAngles;

		_rxOffset = Vector3.zero;
	}

	public void PreventMouseTouchInput(bool inputBlocked){
		PreventAllInput = inputBlocked;
	}

	public void SaveCameraValuesBeforeMoveTo(){
		MySavedPos = _position;
		MySavedEuler = _rotation;
	}

	public void ResetCameraAfterMoveTo(){
		_position.y = MySavedPos.y;
		_rotation = MySavedEuler;
	}

	//------------------| Gestures |

	public override void Drag(DragGesture g, Vector2 magnitude)
	{
		if (!PreventAllInput) {
			if (_updatePosition) {
				if (EventSystem.current != null) {
					if (IsPointerOverUIObject () == 5 ) {
						return;
					}
				}
				_speedMult = 1f;

				_drag.x = -magnitude.x * moveSens;
				_drag.z = -magnitude.y * moveSens;

				float angle = Mathf.Atan2 (_drag.z, _drag.x) - _cameraHolder.eulerAngles.y * Mathf.Deg2Rad;
				float move = _drag.magnitude;

				//Debug.Log("FreeTerrainCamera DRAG......" + magnitude);
				_position.x += Mathf.Cos (angle) * move;
				_position.z += Mathf.Sin (angle) * move;
			}
		}
	}

	public void InverseMoveSens(){
		moveSens = moveSens * -1f;
	}

	public override void Zoom(ZoomGesture g, float delta)
	{
		if (!PreventAllInput) {
			if (enabled) {
				if (EventSystem.current != null) {
					if (IsPointerOverUIObject () == 5 ) {
						return;
					}
				}
				_speedMult = 1f;

				if (!isOrtographicCam) {
					_position.y -= delta * zoomSpeed * zoomSens;
				} else {

					_cameraHolder.Find ("Camera").GetComponent<Camera> ().orthographicSize -= delta * zoomSpeed * zoomSens;
					if (_cameraHolder.Find ("Camera").GetComponent<Camera> ().orthographicSize <= 100f) {
						_cameraHolder.Find ("Camera").GetComponent<Camera> ().orthographicSize = 100f;
					}
				}
			}
		}
	}

	public override void Rotate(RotateGesture g, float angle)
	{
		if (!PreventAllInput) {
			if (_updatePosition) {
				if (EventSystem.current != null) {
					if (IsPointerOverUIObject () == 5 ) {
						return;
					}
				}
				_speedMult = 1f;
				_rotation.y += angle * 4 * rotateSens;
			}
		}
	}

	public override void Pan(PanGesture g, Vector2 panning)
	{
		if (!PreventAllInput) {
			if (_updatePosition) {
				if (EventSystem.current != null) {
					if (IsPointerOverUIObject () == 5 ) {
						return;
					}
				}
				_speedMult = 1f;
				_rotation.y -= panning.x * .8f * rotateSens;


				if (Mathf.Abs (panning.y) > 0) {
					_autoElevationRotation = true;
				}

				ConstrainRotationX ();
				_rotation.x += panning.y * .8f * rotateSens;
			}
		}
	} 

	private void ConstrainRotationX()
	{
		if (_rxOffset.x < -20){ _rxOffset.x = -20;}
		else if (_rxOffset.x > 20){ _rxOffset.x = 20;}
	}
}
