﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SaveLoadManagerLabel3D : MonoBehaviour {
	
	
	//---------------------- Save -----------------------------------------------------------------
	
	public static void SaveLastEmailEntered(string email){
		PlayerPrefs.SetString("lastEmailEntered", email);
	}

	public static void SaveLastUserNameEntered(string userName){
		PlayerPrefs.SetString("lastUserNameEntered", userName);
	}

	//grid objects
	public static void SaveLabelEditorObjects(string labelEditorObjectsValues){
		PlayerPrefs.SetString("AllLabelEditorObjects", labelEditorObjectsValues);
	}

	//---------------------- Load -----------------------------------------------------------------
	
	public static string LoadLastEmailEntered(){
		
		if(!PlayerPrefs.HasKey("lastEmailEntered")){
			return null;
		}
		return PlayerPrefs.GetString("lastEmailEntered");
	}

	public static string LoadLastUserNameEntered(){
		
		if(!PlayerPrefs.HasKey("lastUserNameEntered")){
			return null;
		}
		return PlayerPrefs.GetString("lastUserNameEntered");
	}

	//grid objects
	public static string LoadLabelEditorObjects(){

		if(!PlayerPrefs.HasKey("AllLabelEditorObjects")){
			return null;
		}
		return PlayerPrefs.GetString("AllLabelEditorObjects");
	}
}
