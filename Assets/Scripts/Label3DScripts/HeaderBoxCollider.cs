﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeaderBoxCollider : MonoBehaviour
{

    // Use this for initialization
    void OnEnable()
    {
        StartCoroutine(WaitFor2EndOfFrameCoroutineAndGetSize());
    }

    private IEnumerator WaitFor2EndOfFrameCoroutineAndGetSize()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        GetComponent<BoxCollider>().size = gameObject.GetComponent<RectTransform>().sizeDelta;
        StopCoroutine(WaitFor2EndOfFrameCoroutineAndGetSize());
    }

    private void OnMouseDown()
    {
        GetComponentInParent<LabelEditorObject>().PressedMouseDown();
    }

    private void OnMouseUp()
    {
        GetComponentInParent<LabelEditorObject>().PressedMouseUp();
    }

}
