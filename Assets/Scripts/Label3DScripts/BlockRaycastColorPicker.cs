﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BlockRaycastColorPicker : MonoBehaviour
{
    [SerializeField] private GameObject colorPickerOverlay;
    private bool blockRaycast;
    private bool presspointIn;


    // Use this for initialization
    void Awake () {
        blockRaycast = false;
    }
	
	// Update is called once per frame
	private void Update ()
    {
        if (!blockRaycast && Input.GetMouseButtonDown(0))
        {
            // Check if the mouse was clicked over a UI element
            if (presspointIn)
            {
                blockRaycast = true;
                colorPickerOverlay.SetActive(true);
            }
        }

        if (blockRaycast && Input.GetMouseButtonUp(0))
        {
            presspointIn = false;
            blockRaycast = false;
            colorPickerOverlay.SetActive(false);
        }
    }

    public void PointerEnter()
    {
        presspointIn = true;
    }
}
