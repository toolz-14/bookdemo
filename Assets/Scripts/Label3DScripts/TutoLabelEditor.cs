﻿using System;
using System.Collections;
using System.Collections.Generic;
//using uCPf;
using UnityEngine;
using UnityEngine.UI;

public class TutoLabelEditor : MonoBehaviour {

    public delegate void labelEditorTutoAction();
    public static labelEditorTutoAction labelEditorDelegate;

    [SerializeField] private LabelEditor labelEditor;
    [SerializeField] private LabelEditorSaveLoad labelEditorSaveLoad;

    [SerializeField] private Button ButtonTuto;
    [SerializeField] private Button buttonExitTuto;
    [SerializeField] private Button buttonFinishedTuto;

    [SerializeField] private Button button_Ajouter;
    [SerializeField] private Button button_Supprimer;
    [SerializeField] private Button button_Back;
    [SerializeField] private Button button_SizeUp;
    [SerializeField] private Button button_SizeDown;
    [SerializeField] private Button button_Masquer;
    [SerializeField] private Button button_Color_Text;
    [SerializeField] private Button button_Color_Bg;
    [SerializeField] private InputField inputFieldName;
    [SerializeField] private GameObject sliderParent;

    [SerializeField] private GameObject colorPicker_Panel;
 //   private ColorPicker colorPicker;
    Color defaultColor;

    private Text tutoText;
    private bool animationLooping;
    private Button buttonAnimated;
    private Image imgAnimated;
    private Color imgAnimatedDefaultColor;

    private int tutoStep;
    private bool tutoStepValidation;
    
    public delegate void StopAnimation();
    public StopAnimation stopAnimDelegate; // save animation to stop when exit


    private GameObject selectedLabel;

    // Use this for initialization
    private void Start ()
    {
        tutoStep = 1;
 //       colorPicker = colorPicker_Panel.GetComponent<ColorPicker>();
    }
	
	private void Update () {
        switch (tutoStep)
        {
            case 1:
                if (tutoStepValidation)
                {

                    // Step 1 : Added a new label 
                    selectedLabel = LabelEditor.selectedLabel;
                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    stopAnimDelegate();
                    // text to display
                    tutoText.text = "Pour renommer un label cliquez dans l'encadré blanc en dessous de NOM DU LABEL, tapez le texte et appuyez sur la touche Entrée";
                    // Init and Launch Anim
                    inputFieldName.interactable = true;

                    StartAnimInputfield();
                    stopAnimDelegate = ResetInputfieldAnim;
                }
                break;
            case 2:
                if (tutoStepValidation)
                {
                    // Step 2 : Renamed a new label 
                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    stopAnimDelegate();
                    // text to display
                    tutoText.text = "Pour changer la hauteur d'un label, sélectionnez le slider à droite et déplacez le vers le haut ou le bas";
                    // Launch Anim
                    sliderParent.GetComponentInChildren<Slider>().interactable = true;

                    StartSliderAnim();
                    stopAnimDelegate = ResetSliderAnim;

                }
                break;
            case 3:
                if (tutoStepValidation)
                {
                    // Step 3 : Changed height label 
                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    stopAnimDelegate();
                    // text to display
                    tutoText.text = "Pour diminuer la taille d'un label, cliquez sur le bouton \"-\" en haut à droite ";
                    // Launch Anim
                    button_SizeDown.interactable = true;
                    buttonAnimated = button_SizeDown;

                    StartButtonAnim();
                    stopAnimDelegate = ResetButtonAnim;
                    
                }
                break;
            case 4:
                if (tutoStepValidation)
                {
                    // Step 4 :  Sized down label
                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    stopAnimDelegate();
                    // text to display
                    tutoText.text = "Pour augementer la taille d'un label, cliquez sur le bouton \"+\" en haut à droite ";
                    // Launch Anim
                    button_SizeUp.interactable = true;
                    buttonAnimated = button_SizeUp;

                    StartButtonAnim();
                    stopAnimDelegate = ResetButtonAnim;
                }
                break;
            case 5:
                if (tutoStepValidation)
                {
                    // Step 5 : Sized up label 
                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    stopAnimDelegate();
                    // text to display
                    tutoText.text = "L'encadré en rouge autour du label indique que le label est actuellement sélectionné, cliquez n'importe où sur la scène ";
                    // Launch Anim
                    LabelEditor.canDeSelectedLabel = true;
                }
                break;
            case 6:
                if (tutoStepValidation)
                {
                    // Step 6 : label Deselected
                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    // text to display
                    tutoText.text = "Pour sélectionner un label cliquez dessus. Cliquez sur le label que vous venez de créer";
                    // Launch Anim
                }
                else
                {
                    CheckLabelDeselected();
                }
                break;
            case 7:
                if (tutoStepValidation)
                {
                    // Step 7 : label Selected

                    //FreeTerrainCamera.BlockCameraMovement = true;

                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    // text to display
                    tutoText.text = "Déplacez le label que vous venez de créer. Pour déplacer un label, maintenez le clic gauche sur le label et bougez la souris. Lachez le clic une fois la position désirée atteinte.";
                    // Launch Anim
                }
                else
                {
                    CheckLabelSelected();
                }
                break;
            case 8:
                if (tutoStepValidation)
                {
                    // Step 8 : Label has been moved
                    //FreeTerrainCamera.BlockCameraMovement = true;
                    labelEditorSaveLoad.SetAllLabelsInterractable(false);
                    LabelEditor.canDeSelectedLabel = false;

                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    // text to display
                    tutoText.text = "Pour masquer le point d'ancrage d'un label cliquez sur le bouton Masquer";
                    // Launch Anim
                    button_Masquer.interactable = true;
                    buttonAnimated = button_Masquer;

                    StartButtonAnim();
                    stopAnimDelegate = ResetButtonAnim;
                }
                else
                {
                    CheckLabelDragged();
                }
                break;
            case 9: // Step 9 : Center Masked
                if (tutoStepValidation)
                {
                    tutoStepValidation = false;
                    tutoStep++;
                    // Stop previous animation
                    stopAnimDelegate();

                    // text to display
                    tutoText.text = "Pour changer la couleur du texte, cliquez sur le bouton text Color Editor";
                    // Launch Anim
                    button_Color_Text.interactable = true;
                    buttonAnimated = button_Color_Text;

                    StartButtonAnim();
                    stopAnimDelegate = ResetButtonAnim;
                }
                break;
            case 10: // Step 10 : Color button clicked
                if (tutoStepValidation)
                {
                    tutoStepValidation = false;
                    tutoStep++;
                    // Stop previous animation
                    stopAnimDelegate();

                    // text to display
                    tutoText.text = "Changez la couleur en cliquant sur la palette";
                    // Launch Anim

 //                   defaultColor = colorPicker.hsv;
                }
                break;
            case 11: // Step 11 : Color text changed 
                if (tutoStepValidation)
                {
                    tutoStepValidation = false;
                    tutoStep++;
                    // Stop previous animation
                    stopAnimDelegate();

                    // text to display
                    tutoText.text = "Pour changer la couleur du fond, cliquez sur le bouton Image Color Editor";
                    // Launch Anim
                    button_Color_Bg.interactable = true;
                    buttonAnimated = button_Color_Bg;

                    StartButtonAnim();
                    stopAnimDelegate = ResetButtonAnim;
                }
                else
                {
                    CheckColorchanged();
                }
                break;
            case 12: // Step 12 : Color bg button clicked 
                if (tutoStepValidation)
                {
                    tutoStepValidation = false;
                    tutoStep++;
                    // Stop previous animation
                    stopAnimDelegate();

                    // text to display
                    tutoText.text = "Changez la couleur en cliquant sur la palette";
                    // Launch Anim
 //                   defaultColor = colorPicker.hsv;
                }
                break;
            case 13: // Step 13 : Color bg changed 
                if (tutoStepValidation)
                {
                    tutoStepValidation = false;
                    tutoStep++;
                    // Stop previous animation

                    // text to display
                    tutoText.text = "Pour fermer la palette, cliquez sur le bouton d'édition actif en bleu.";
                    // Launch Anim
                    button_Color_Bg.interactable = true;
                    buttonAnimated = button_Color_Bg;

                    StartButtonAnim();
                    stopAnimDelegate = ResetButtonAnim;
                }
                else
                {
                    CheckColorchanged();
                }
                break;
            case 14: // Step 14 :
                if (tutoStepValidation)
                {
                    tutoStepValidation = false;
                    tutoStep++;
                    // Stop previous animation
                    stopAnimDelegate();

                    // text to display
                    tutoText.text = "Pour supprimer un label, cliquez sur le bouton supprimer";
                    // Launch Anim
                    button_Supprimer.interactable = true;
                    buttonAnimated = button_Supprimer;

                    StartButtonAnim();
                    stopAnimDelegate = ResetButtonAnim;
                }
                break;
            case 15: // Step 15 : Label has been deleted
                if (tutoStepValidation)
                {
                    tutoStepValidation = false;
                    tutoStep++;

                    // Stop previous animation
                    stopAnimDelegate();
                    // text to display
                    tutoText.text = "Vous avez finis le tutoriel ! Pour sortir de l'éditeur de Label, cliquez sur le bouton retour en bas à gauche. Attention, une fois l'éditeur de label fermé il n'est pas possible d'interargir avec les labels.\n";
                    buttonFinishedTuto.gameObject.SetActive(true);
                    // Launch Anim
                    //button_Back.interactable = true;
                    //buttonAnimated = button_Back;
                    //StartImageAnim(button_Back, button_Back.transform.Find("Image").GetComponent<Image>());
                    //stopAnimDelegate = ResetImageAnim;
                }
                break;
        }
	}

    public void StartTuto()
    {
        // Open tuto
        buttonExitTuto.gameObject.SetActive(true);
        this.gameObject.SetActive(true);
        colorPicker_Panel.SetActive(false);
         //FreeTerrainCamera.BlockCameraMovement = true;

         // Initialize values
         selectedLabel = null;
        tutoStep = 1;
        tutoText = this.GetComponentInChildren<Text>();
        tutoStepValidation = false;

        // make all previous labels not interactables
        DeactivateAllInteractables();
        labelEditorSaveLoad.SetAllLabelsInterractable(false);
        LabelEditor.canDeSelectedLabel = false;

        // Set 1st step to do 
        tutoText.text = "Pour ajouter un label cliquez sur le bouton Ajouter";
        // Init and Launch Anim
        button_Ajouter.interactable = true;
        buttonAnimated = button_Ajouter;
        StartButtonAnim();
        stopAnimDelegate = ResetButtonAnim;
    }

    public void ExitTuto()
    {
        // Remove Listeners
        if(buttonAnimated)
            buttonAnimated.onClick.RemoveAllListeners();
        inputFieldName.onEndEdit.RemoveListener(OnEndEditInputfield);
        sliderParent.GetComponentInChildren<Slider>().onValueChanged.RemoveListener(OnValueChangedSlider);
        labelEditorDelegate = null;

        buttonFinishedTuto.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
        buttonExitTuto.gameObject.SetActive(false);
        ButtonTuto.gameObject.SetActive(true);
        selectedLabel = null;

        // Stop current anim
        stopAnimDelegate();
        StopAllCoroutines();
        /*
            ResetButtonAnim();
        ResetInputfieldAnim();
        ResetSliderAnim();
        ResetImageAnim();*/


        // Unblock actions
        ActivateAllInteractables();
        //FreeTerrainCamera.BlockCameraMovement = false;
        labelEditorSaveLoad.SetAllLabelsInterractable(false);
        LabelEditor.canDeSelectedLabel = false;
        LabelEditorObject.dragAndDropSucced = false;
    }

    #region ButtonAnim

    private void ButtonOnClick()
    {
        tutoStepValidation = true;
        buttonAnimated.onClick.RemoveAllListeners();
    }

    private void StartButtonAnim()
    {
        //AddListeners
        if(buttonAnimated)
            buttonAnimated.onClick.AddListener(ButtonOnClick);
        else
        {
            Debug.Log("buttonAnimated is null");
        }

        animationLooping = true;
        StartCoroutine("Blink");
    }

    private void StartButtonAnimNoListener(Button buttonToAnimate)
    {
        buttonAnimated = buttonToAnimate;
        //AddListeners

        animationLooping = true;
        StartCoroutine("Blink");
    }

    private IEnumerator Blink()
    {
        while (animationLooping)
        {
            for (float f = 1f; f >= 0; f -= 0.1f)
            {
                Color c = buttonAnimated.GetComponent<Outline>().effectColor;
                c.a = f;
                buttonAnimated.GetComponent<Outline>().effectColor = c;
                yield return new WaitForSeconds(.1f);
            }
        }
    }

    private void ResetButtonAnim()
    {
        animationLooping = false;
        StopCoroutine("Blink");

        // Set default color
        if (buttonAnimated)
        {
            Color c = buttonAnimated.GetComponent<Outline>().effectColor;
            c.a = 0f;
            buttonAnimated.GetComponent<Outline>().effectColor = c;

            buttonAnimated.onClick.RemoveListener(ButtonOnClick);
            buttonAnimated = null;
        }

        DeactivateAllInteractables();

    }

    private void StartImageAnim(Button buttonToAnimate, Image img)
    {
        buttonAnimated = buttonToAnimate;
        imgAnimated = img;
        imgAnimatedDefaultColor = img.color;
        //AddListeners
        buttonAnimated.onClick.AddListener(ButtonOnClick);

        animationLooping = true;
        StartCoroutine("BlinkImage");
    }

    private IEnumerator BlinkImage()
    {
        while (animationLooping)
        {
            for (float f = 1f; f >= 0; f -= 0.1f)
            {
                Color c = new Color(255,255, 0,f);
                imgAnimated.color = c;
                yield return new WaitForSeconds(.1f);
            }
        }
    }

    private void ResetImageAnim()
    {
        animationLooping = false;
        StopCoroutine("BlinkImage");

        // Set default color
        if (imgAnimated)
            imgAnimated.color = imgAnimatedDefaultColor;
        if (buttonAnimated)
        {
            buttonAnimated.onClick.RemoveListener(ButtonOnClick);
            buttonAnimated = null;
        }

        DeactivateAllInteractables();

    }

    #endregion

    #region InputFieldAnim
    private void OnEndEditInputfield(string arg0)
    {
        tutoStepValidation = true;
        inputFieldName.onEndEdit.RemoveListener(OnEndEditInputfield);
    }

    private void StartAnimInputfield()
    {
        //AddListeners
        inputFieldName.onEndEdit.AddListener(OnEndEditInputfield);

        animationLooping = true;
        StartCoroutine("BlinkInputfield");
    }


    private IEnumerator BlinkInputfield()
    {
        while (animationLooping)
        {
            for (float f = 1f; f >= 0; f -= 0.1f)
            {
                Color c = inputFieldName.GetComponent<Outline>().effectColor;
                c.a = f;
                inputFieldName.GetComponent<Outline>().effectColor = c;
                yield return new WaitForSeconds(.1f);
            }
        }
    }

    private void ResetInputfieldAnim()
    {
        animationLooping = false;
        StopCoroutine("BlinkInputfield");

        // Set default color
        Color c = inputFieldName.GetComponent<Outline>().effectColor;
        c.a = 0f;

        inputFieldName.GetComponent<Outline>().effectColor = c;

        inputFieldName.onEndEdit.RemoveListener(OnEndEditInputfield);

        DeactivateAllInteractables();
    }
    #endregion

    #region SliderAnim
    private void OnValueChangedSlider(float arg0)
    {
        if (!tutoStepValidation && Mathf.Abs(arg0)>0.2f)
        {
            tutoStepValidation = true;
            sliderParent.GetComponentInChildren<Slider>().onValueChanged.RemoveListener(OnValueChangedSlider);
        }
    }

    private void StartSliderAnim()
    {
        //AddListeners
        sliderParent.GetComponentInChildren<Slider>().onValueChanged.AddListener(OnValueChangedSlider);

        animationLooping = true;
        StartCoroutine("BlinkSliderAnim");
    }


    private IEnumerator BlinkSliderAnim()
    {
        while (animationLooping)
        {
            for (float f = 1f; f >= 0; f -= 0.1f)
            {
                Color c = sliderParent.GetComponentInChildren<Outline>().effectColor;
                c.a = f;
                sliderParent.GetComponentInChildren<Outline>().effectColor = c;
                yield return new WaitForSeconds(.1f);
            }
        }
    }

    private void ResetSliderAnim()
    {
        animationLooping = false;
        StopCoroutine("BlinkSliderAnim");

        // Set default color
        Color c = sliderParent.GetComponentInChildren<Outline>().effectColor;
        c.a = 0f;

        sliderParent.GetComponentInChildren<Outline>().effectColor = c;

        sliderParent.GetComponentInChildren<Slider>().onValueChanged.RemoveListener(OnValueChangedSlider);

        DeactivateAllInteractables();
    }
    #endregion

    private void CheckLabelDeselected()
    {
        if (!tutoStepValidation && LabelEditor.selectedLabel == null)
        {
            tutoStepValidation = true;
        }
    }

    private void CheckLabelSelected()
    {
        if (!tutoStepValidation && LabelEditor.selectedLabel != null && selectedLabel == LabelEditor.selectedLabel)
        {
            tutoStepValidation = true;
            labelEditorDelegate();
        }
    }


    private void CheckColorchanged()
    {
        //if (!tutoStepValidation && defaultColor!= (Color)colorPicker.hsv)
        //{
        //    tutoStepValidation = true;
        //}
    }

    private void CheckLabelDragged()
    {
        if (!tutoStepValidation && LabelEditor.selectedLabel != null && selectedLabel == LabelEditor.selectedLabel)
        {
            if (LabelEditorObject.dragAndDropSucced)
            {
                tutoStepValidation = true;
            }
            else
            {
                labelEditorDelegate();
            }
        }
    }

    private void DeactivateAllInteractables()
    {
        button_Ajouter.interactable = false;
        button_Supprimer.interactable = false;
        button_Back.interactable = false;
        button_SizeUp.interactable = false;
        button_SizeDown.interactable = false;
        button_Masquer.interactable = false;
        button_Color_Text.interactable = false;
        button_Color_Bg.interactable = false;
        sliderParent.GetComponentInChildren<Slider>().interactable = false;

        inputFieldName.interactable = false;
    }

    private void ActivateAllInteractables()
    {
        button_Ajouter.interactable = true;
        button_Supprimer.interactable = true;
        button_Back.interactable = true;
        button_SizeUp.interactable = true;
        button_SizeDown.interactable = true;
        button_Masquer.interactable = true;
        button_Color_Text.interactable = true;
        button_Color_Bg.interactable = true;
        sliderParent.GetComponentInChildren<Slider>().interactable = true;

        inputFieldName.interactable = true;
    }
}
