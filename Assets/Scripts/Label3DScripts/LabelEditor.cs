﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using uCPf;
using Toolz.WebServices;

// Panel de control
public class LabelEditor : MonoBehaviour
{

    [SerializeField] private InputField nameInputField;
    [SerializeField] private GameObject labelBase;
    [SerializeField] private InputField descriptionInputField;
    [SerializeField] private Transform labelParent;
    [SerializeField] private Camera theLabel3DCamera;
    [SerializeField] private Transform mainCameraHolder;
    [SerializeField] private GameObject addALabelButton;
    [Header("Scale Settings")]
    [SerializeField] private float sizeStep;
    [SerializeField] private float sizeMax;
    [SerializeField] private float sizeMin;
    [Header("")]
    [SerializeField] private int maxTextLength;
    [SerializeField] private int maxTextLengthDesc;
    [Header("Height Settings")]
    [SerializeField] private float heightOffset;
    private BoxCollider constrainBox;

    public static GameObject selectedLabel;
    public static bool canDeSelectedLabel;
    public static bool isOpen = false;
    //public static bool colorPickerIsOpenFirstTime = false; // colorPicker for the first time always call On value Change

    public Slider theSlider;
    //public Text maskButtonTxt;

    private RaycastHit hit;
    private Ray ray;
    private bool doOnce;
    [SerializeField] private LabelEditorSaveLoad labelEditorSaveLoad;
    [Header("")]
    [SerializeField] private float DistanceToCamera;
    [SerializeField] private float SliderAmplitude;

    //[Header("Color Editor Settings")]
    //[SerializeField] private GameObject colorPickerPanel;
    //private ColorPicker colorPickerScript;
    //private bool textColorEditing = false;
    //private bool bgColorEditing = false;

    //[SerializeField] private Color defaultButtonColor;
    //[SerializeField] private Color selectedButtonColor;
    //[SerializeField] private Image textColorImg;
    //[SerializeField] private Image bgColorImg;

    //[SerializeField]
    //private Dropdown dropDownCategorie; // Catégorie
    public int phase;

    private bool blockSliderUpdate;
    private bool hasStarted;

    Toolz.WebServices.User user;

    private void Awake()
    {
        doOnce = false;
        nameInputField.onEndEdit.AddListener(EditLabelName);
        descriptionInputField.onEndEdit.AddListener(EditDescription);
        constrainBox = theLabel3DCamera.GetComponentInParent<BoxCollider>();
        //colorPickerScript = colorPickerPanel.GetComponent<ColorPicker>();
        //textColorImg.color = defaultButtonColor;
        //bgColorImg.color = defaultButtonColor;
        phase = 0;
        hasStarted = true;
    }


    private void OnEnable()
    {
        theLabel3DCamera.transform.parent.parent.parent.GetComponent<FreeTerrainCameraLabel3D>().ForceCameraPosition(mainCameraHolder);
        theLabel3DCamera.transform.parent.position = mainCameraHolder.transform.position;
    }

    
    private void Update()
    {
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        }
        else
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        }
        if (Input.GetButtonDown("Fire1"))
        {
            if (IsPointerOverUIObject() == 5)
            {
                return;
            }
            if (Physics.Raycast(ray, out hit, 10000f))
            {
                if (hit.transform.tag != "EditableLabel" || hit.transform.tag == null)
                {
                    if (canDeSelectedLabel && selectedLabel != null)
                    { // Deselect a label
                        selectedLabel.GetComponentInParent<LabelEditorObject>().Outline.enabled = false;
                        selectedLabel = null;
                        nameInputField.text = "";
                        descriptionInputField.text = "";
                        //Debug.Log("deselect");
                    }
                }
                else
                {
                    //	Debug.Log ("1");
                }
            }
            else
            {
                //	Debug.Log ("2");
            }
        }
    }

    protected int IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (results.Count > 0)
        {
            return results[0].gameObject.layer;
        }
        else
        {
            return -1;
        }
    }

    // All
    public void OpenLabelEditor()
    {
        //need to reset slider value
        theSlider.value = 0;
        isOpen = true;
        canDeSelectedLabel = true;
        labelEditorSaveLoad.ActiveLabelCollider(true);

        user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>(); // Get user
        if ((Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
        {
            addALabelButton.SetActive(true);
        }
        else
        {
            addALabelButton.SetActive(false);
        }
    }

    public void CloseEditor()
    {
        nameInputField.text = "";
        descriptionInputField.text = "";
        if (selectedLabel != null)
        {
            selectedLabel.GetComponentInParent<LabelEditorObject>().Outline.enabled = false;
            selectedLabel = null;
        }
        isOpen = false;
        // labelEditorSaveLoad.ActiveLabelCollider(false);
        //colorPickerPanel.SetActive(false);
        // Reset camera
        mainCameraHolder.transform.parent.parent.GetComponent<FreeTerrainCamera>().ForceCameraPosition(theLabel3DCamera.transform.parent);
        mainCameraHolder.transform.position = theLabel3DCamera.transform.parent.position;

        User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>();
        if ((Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN)
        {
            LabelEditorSaveLoad.isAdmin = true;
        }
        else
        {
            LabelEditorSaveLoad.isAdmin = false;
        }
    }

    private void EditLabelName(string arg0)
    {
        if (selectedLabel != null)
        {
            string labelText = (arg0.Length <= maxTextLength) ? arg0 : arg0.Substring(0, maxTextLength); // crop to max length
            selectedLabel.GetComponentInParent<LabelEditorObject>().LabelText.text = labelText;
        }
        else
        {
            nameInputField.text = "";
            //Debug.Log("not selected");
        }
    }
    private void EditDescription(string arg0)
    {
        if (selectedLabel != null)
        {
            string labelText = (arg0.Length <= maxTextLengthDesc) ? arg0 : arg0.Substring(0, maxTextLengthDesc);
            selectedLabel.GetComponentInParent<LabelEditorObject>().description = labelText.Replace("\n", "BREAKLINE").Replace("\r", "BREAKLINE");
        }
        else
        {
            descriptionInputField.text = "";
            //Debug.Log("not selected");
        }
    }
    
    public void ResetLabelEditorValues()
    {
        blockSliderUpdate = true;
        theSlider.value = 0;
        LabelEditorObject labelEditorObject = selectedLabel.GetComponentInParent<LabelEditorObject>();
        //colorPickerScript.hsv = labelEditorObject.ImageHeaderColor;
        nameInputField.text = selectedLabel.GetComponentInParent<LabelEditorObject>().LabelText.text;
        descriptionInputField.text = selectedLabel.GetComponentInParent<LabelEditorObject>().description;
        //if (labelEditorObject.centerActive)
        //{
        //    maskButtonTxt.text = "Masquer";
        //}
        //else
        //{
        //    maskButtonTxt.text = "Afficher";
        //}
        StartCoroutine("WaitFrame");
    }

    IEnumerator WaitFrame()
    {

        yield return new WaitForEndOfFrame();
        blockSliderUpdate = false;
        StopCoroutine("WaitFrame");
    }

    public void AddALabel()
    {
        if (!doOnce)
        {
            doOnce = true;
            //deselect previous if one selected
            if (selectedLabel != null)
            {
                selectedLabel.GetComponentInParent<LabelEditorObject>().Outline.enabled = false;
                selectedLabel = null;
            }

            //need to reset slider value
            theSlider.value = 0;

            //position at center of screen
            Vector3 SpawnPosition = theLabel3DCamera.transform.forward * DistanceToCamera + theLabel3DCamera.transform.position;
            if (SpawnPosition.y < 1.2f)
            {
                SpawnPosition.y = 1.2f;
            }
            GameObject temp = Instantiate(labelBase, Vector3.zero, Quaternion.identity) as GameObject;
            temp.SetActive(true);
            temp.transform.SetParent(labelParent);
            temp.transform.position = SpawnPosition;
            Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>();
            temp.GetComponent<LabelEditorObject>().id = -1;
            if ((Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) //  User is admin)
            {
                temp.GetComponent<LabelEditorObject>().status = 1;
            }
            else
            {
                temp.GetComponent<LabelEditorObject>().status = 0;
            }
            temp.GetComponent<LabelEditorObject>().idAuthor = user.idUser;
            
            phase = labelEditorSaveLoad.panelAtelier.currentSelectedButton;
            temp.GetComponent<LabelEditorObject>().phase = phase;
            temp.GetComponent<LabelEditorObject>().category = 0;
            temp.GetComponent<LabelEditorObject>().Outline.enabled = true;
            temp.GetComponent<LabelEditorObject>().LabelText.text = "New label";
            temp.GetComponent<LabelEditorObject>().centerActive = true;
            temp.GetComponent<LabelEditorObject>().SavedObjectHeight = temp.transform.localPosition.y;
            temp.GetComponent<LabelEditorObject>().interactable = true;
            nameInputField.text = "";
            descriptionInputField.text = "";
            selectedLabel = temp;
            labelEditorSaveLoad.labelEditorObjectList.Add(temp.GetComponent<LabelEditorObject>());

            Invoke("Delayed", 0.05f);
        }
    }

    public void DestroyALabel()
    {
        if (!doOnce && selectedLabel != null)
        {
            doOnce = true;
            GameObject temp = selectedLabel;

            if (temp.GetComponent<LabelEditorObject>().id == -1)
            {
                labelEditorSaveLoad.labelEditorObjectList.Remove(temp.GetComponent<LabelEditorObject>());
                Destroy(temp);
            }
            else //We do not remove from list but say its deleted instead
            {
                temp.GetComponent<LabelEditorObject>().isDeleted = true;
                temp.SetActive(false);
            }

            selectedLabel = null;
            Invoke("Delayed", 0.05f);
        }
    }

    public void MaskCenterLabel()
    {
        if (!doOnce && selectedLabel != null)
        {
            doOnce = true;
            LabelEditorObject labelEditorObject = selectedLabel.GetComponentInParent<LabelEditorObject>();
            //if (labelEditorObject.centerActive)
            //{
            //    labelEditorObject.centerActive = false;
            //    maskButtonTxt.text = "Afficher";
            //}
            //else
            //{
            //    labelEditorObject.centerActive = true;
            //    maskButtonTxt.text = "Masquer";
            //}
            //selectedLabel.transform.Find("Empty_Center").Find("Image_Background").gameObject.SetActive(labelEditorObject.centerActive);
            //selectedLabel.transform.Find("Empty_Center").Find("Image_Stroke").gameObject.SetActive(labelEditorObject.centerActive);
            Invoke("Delayed", 0.05f);
        }
    }

    private void Delayed()
    {
        doOnce = false;
    }

    public void OnSliderMoved()
    {
        if (selectedLabel != null && !blockSliderUpdate)
        {
            Vector3 tempV = selectedLabel.transform.localPosition;

            float yMin = constrainBox.bounds.min.y;
            float yMax = constrainBox.bounds.max.y;

            tempV.y = (theSlider.value * SliderAmplitude) + selectedLabel.GetComponentInParent<LabelEditorObject>().SavedObjectHeight;

            if (tempV.y < yMin - heightOffset)
            {
                tempV.y = yMin - heightOffset;
            }
            else if (tempV.y > yMax - heightOffset)
            {
                tempV.y = yMax - heightOffset;
            }
            selectedLabel.transform.localPosition = tempV;
        }
    }

    public void SizeUp()
    {
        if (!doOnce && selectedLabel != null)
        {
            doOnce = true;
            if (selectedLabel.transform.localScale.x <= sizeMax) // Check only x because we are scaling uniformly
                selectedLabel.transform.localScale = selectedLabel.transform.localScale + Vector3.one * sizeStep;
            Invoke("Delayed", 0.05f);
        }
    }

    public void SizeDown()
    {
        if (!doOnce && selectedLabel != null)
        {
            doOnce = true;
            if (selectedLabel.transform.localScale.x >= sizeMin)
                selectedLabel.transform.localScale = selectedLabel.transform.localScale - Vector3.one * sizeStep;
            Invoke("Delayed", 0.05f);
        }
    }

    public void TextColorEditorbutton()
    {
        if (!doOnce)
        {
            doOnce = true;
            //if (bgColorEditing)
            //{
            //    bgColorEditing = false;
            //    bgColorImg.color = defaultButtonColor;
            //    colorPickerScript.OnChange.RemoveAllListeners();
            //}

            //if (textColorEditing)
            //{
            //    textColorEditing = false;
            //    textColorImg.color = defaultButtonColor;
            //}
            //else
            //{
            //    textColorEditing = true;
            //    textColorImg.color = selectedButtonColor;
            //}
            //colorPickerPanel.SetActive(textColorEditing);

            //if (textColorEditing)
            //{
            //    colorPickerScript.OnChange.RemoveAllListeners();
            //    colorPickerScript.OnChange.AddListener((Color c) => ChangeTextColor(c));
            //}
            Invoke("Delayed", 0.05f);
        }
    }

    public void Categorybutton(string hexaColorAndCategory)
    {
        string[] values = hexaColorAndCategory.Split('+');

        if (!doOnce)
        {
            doOnce = true;

            if (selectedLabel != null)
            {
                LabelEditorObject labelEditorObject = selectedLabel.GetComponentInParent<LabelEditorObject>();
                labelEditorObject.category = int.Parse(values[1]);
                Color newCol;
                if (ColorUtility.TryParseHtmlString(values[0], out newCol))
                    labelEditorObject.ChangeBgColor(newCol);
            }

            Invoke("Delayed", 0.05f);
        }
    }

    public void ChangeTextColor(Color c)
    {
        if (selectedLabel != null)
        {
            LabelEditorObject labelEditorObject = selectedLabel.GetComponentInParent<LabelEditorObject>();
            //if (!colorPickerIsOpenFirstTime) // Editor has never been open
            //{
            //    colorPickerIsOpenFirstTime = true;
            //    colorPickerScript.hsv = labelEditorObject.textTitleColor;
            //    c = colorPickerScript.hsv;
            //}
            labelEditorObject.ChangeTextColor(c);
        }
    }

    public void BgColorEditorbutton()
    {
        if (!doOnce)
        {
            doOnce = true;
            //if (textColorEditing)
            //{
            //    textColorEditing = false;
            //    textColorImg.color = defaultButtonColor;
            //    colorPickerScript.OnChange.RemoveAllListeners();
            //}

            //if (bgColorEditing)
            //{
            //    bgColorEditing = false;
            //    bgColorImg.color = defaultButtonColor;
            //}
            //else
            //{
            //    bgColorEditing = true;
            //    bgColorImg.color = selectedButtonColor;
            //}
            //colorPickerPanel.SetActive(bgColorEditing);

            //if (bgColorEditing)
            //{
            //    colorPickerScript.OnChange.RemoveAllListeners();
            //    colorPickerScript.OnChange.AddListener((Color c) => ChangeBgColor(c));
            //}
            Invoke("Delayed", 0.05f);
        }
    }

    public void ChangeBgColor(Color c)
    {
        if (selectedLabel != null)
        {
            //LabelEditorObject labelEditorObject = selectedLabel.GetComponent<LabelEditorObject>();
            //if (!colorPickerIsOpenFirstTime) // Editor has never been open
            //{
            //    colorPickerIsOpenFirstTime = true;
            //    colorPickerScript.hsv = labelEditorObject.ImageHeaderColor;
            //    c = colorPickerScript.hsv;
            //}
            //labelEditorObject.ChangeBgColor(c);
        }
    }

    //public void AssignPhase(Slider slider)
    //{
    //    this.phase = (int)slider.value;
    //}

    public void AssignPhaseButton(int index)
    {
        
        this.phase = index;
    }
}
