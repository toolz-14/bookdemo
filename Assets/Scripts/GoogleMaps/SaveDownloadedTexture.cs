﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace Toolz.GoogleMaps {
	[AddComponentMenu("Toolz/GoogleMaps/LevelManager")]

	public class SaveDownloadedTexture : MonoBehaviour {

		public string imageName = "SavedScreen";

		Texture2D sourceTex;

		public void Save() {
			sourceTex = (Texture2D)GetComponent<Renderer>().material.mainTexture;

			// Encode texture into PNG
			byte[] bytes = sourceTex.EncodeToPNG();
			//Object.Destroy(destTex);
			Debug.Log("bytes.Length = "+bytes.Length);

			// For testing purposes, also write to a file in the project folder
			File.WriteAllBytes(Application.dataPath + "/GoogleImage/"+imageName+".png", bytes);

			Debug.Log(Application.dataPath + "/GoogleImage/"+imageName+".png");
		}

		private void Start () {
		
		}
	}
}
