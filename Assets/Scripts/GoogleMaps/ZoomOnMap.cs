﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Toolz.GoogleMaps {
	[AddComponentMenu("Toolz/GoogleMaps/ZoomOnMap")]

	public class ZoomOnMap : MonoBehaviour, IZoomGesture {

		public GoogleMap googleMap;
		public Texture[] maptextures = new Texture[9];
		public bool isTexture = false;
		public float sensitivity = 10f;
		private int _oldZoom = 0;
		private int _zoom = 14;

		public Slider _slider;
		private int _inc = 0;
		private int _speed = 8;

		protected Gestures _gestures;
		protected ZoomGesture _zoomGesture;

		private void Awake(){

		}

		private void Start(){
			_gestures = Gestures.instance;
		}

		private void OnEnable() {
			if (isTexture) {
				GetComponent<MeshRenderer>().material.mainTexture = maptextures[maptextures.Length-1];
			}
			_zoomGesture = new ZoomGesture(this, "world");
		}
		
		void OnDisable(){
			if (_zoomGesture == null) return;
			_zoomGesture.Dispose();
			_zoomGesture = null;
		}

		public void Zoom(ZoomGesture g, float delta)
		{
			if (Input.GetAxis ("Mouse ScrollWheel") != 0f) {
				_zoom += (int)(Input.GetAxis ("Mouse ScrollWheel") * sensitivity);
				Zoom ();
				
			} else {
				if (Input.touchCount == 2 && (_inc == 0 || _inc >= _speed)) {
					if (delta > 1) {
						_zoom += 1;
						Zoom ();
					} else if (delta < 1) {
						_zoom -= 1;
						Zoom ();
					}
					_inc = 0;
				}
				_inc++;
			}
		}

		public void SliderZoom(){
			_zoom = 14-(int)_slider.value;
			Zoom();
		}

		private void Zoom(){
			if(_zoom < 5){
				_zoom = 5;
			}else if(_zoom > 14){
				_zoom = 14;
			}
			if(_oldZoom != _zoom){
				if(!isTexture){
					googleMap.zoom = _zoom;
					_oldZoom = _zoom;
					googleMap.Refresh();
				}else{
					SwapTexture(_zoom-5);
				}
			}
		}

		private void SwapTexture(int index){
			GetComponent<MeshRenderer>().material.mainTexture = maptextures[index];
		}

		private bool SameSign(float num1, float num2){
			return num1 >= 0 && num2 >= 0 || num1 < 0 && num2 < 0;
		}
	}
}
