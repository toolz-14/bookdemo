using UnityEngine;
using System.Collections;

namespace Toolz.GoogleMaps {
	[AddComponentMenu("Toolz/GoogleMaps/LevelManager")]

	public class GoogleMap : MonoBehaviour
	{
		public enum MapType
		{
			RoadMap,
			Satellite,
			Terrain,
			Hybrid
		}
		public bool loadOnStart = true;
		//public bool autoLocateCenter = true;
		public GoogleMapLocation centerLocation;
		public int zoom = 5;
		public MapType mapType;
		public int size = 512;
		public bool doubleResolution = false;
		//public GoogleMapMarker[] markers;
		//public GoogleMapPath[] paths;

		private WWW _req;
		
		void Start() {
			if(loadOnStart) Refresh();	
		}
		
		public void Refresh() {
	//		if(autoLocateCenter && (markers.Length == 0 && paths.Length == 0)) {
	//			Debug.LogError("Auto Center will only work if paths or markers are used.");	
	//		}
			StartCoroutine(_Refresh());
		}
		
		IEnumerator _Refresh ()
		{
			var googleAPIKey = "AIzaSyDh8cauU_IXVx3Pn8lVYbUvWk7sRikwdjQ";
			var url = "http://maps.googleapis.com/maps/api/staticmap";
			var qs = "";

	//		if(zoom < 6){
	//			centerLocation.latitude = 47.05443f;
	//			centerLocation.longitude = 2.382637f;
	//		}else if(zoom == 6 || zoom == 7){
	//			centerLocation.latitude = 48.2491724f;
	//			centerLocation.longitude = 2.604212f;
	//		}else if(zoom == 8 || zoom ==9){
	//			centerLocation.latitude = 48.4672625f;
	//			centerLocation.longitude = 2.462763f;
	//		}else if(zoom == 10 || zoom ==11){
	//			centerLocation.latitude = 48.4672625f;
	//			centerLocation.longitude = 2.462763f;
	//		}else if(zoom > 11){
	//			centerLocation.latitude = 48.631849f;
	//			centerLocation.longitude = 2.392495f;
	//		}
			centerLocation.latitude = 48.631849f;
			centerLocation.longitude = 2.392495f;
				
			if (centerLocation.address != ""){
				qs += "center=" + WWW.UnEscapeURL (centerLocation.address);
			}
			else {
				qs += "center=" + WWW.UnEscapeURL (string.Format ("{0},{1}", centerLocation.latitude, centerLocation.longitude));
			}
		
			qs += "&zoom=" + zoom.ToString ();

			qs += "&size=" + WWW.UnEscapeURL (string.Format ("{0}x{0}", size));
			qs += "&scale=" + (doubleResolution ? "2" : "1");
			qs += "&maptype=" + mapType.ToString ().ToLower ();
			var usingSensor = false;
	#if UNITY_IPHONE
			usingSensor = Input.location.isEnabledByUser && Input.location.status == LocationServiceStatus.Running;
	#endif
			qs += "&sensor=" + (usingSensor ? "true" : "false");

			var req = new WWW (url + "?" + qs + "&key="+googleAPIKey);
			yield return req;
			GetComponent<Renderer>().material.mainTexture = req.texture;
		}
	}


	public enum GoogleMapColor
	{
		black,
		brown,
		green,
		purple,
		yellow,
		blue,
		gray,
		orange,
		red,
		white
	}

	[System.Serializable]
	public class GoogleMapLocation
	{
		public string address;
		public float latitude;
		public float longitude;
	}

	[System.Serializable]
	public class GoogleMapMarker
	{
		public enum GoogleMapMarkerSize
		{
			Tiny,
			Small,
			Mid
		}
		public GoogleMapMarkerSize size;
		public GoogleMapColor color;
		public string label;
		public GoogleMapLocation[] locations;
		
	}

	[System.Serializable]
	public class GoogleMapPath
	{
		public int weight = 5;
		public GoogleMapColor color;
		public bool fill = false;
		public GoogleMapColor fillColor;
		public GoogleMapLocation[] locations;	
	}
}