﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropsDownPlaceOfResidence : MonoBehaviour {

    [SerializeField]
    private Dropdown dropDownArea; // Secteur de Toulouse (Toulouse Centre, Toulouse Rive Gauche...)
    [SerializeField]
    private Dropdown dropDownDistrict; // Différents quartiers des secteurs
    [SerializeField]
    private Text dropdownLabel2;

    public Dictionary<string, string[]> residenceDictionnary = new Dictionary<string, string[]>()
    {
        { "Toulouse Centre", new string[] { "Capitole", "Arnaud Bernard", "Carmes", "Amidonniers", "Compans Caffarelli", "Les Chalets", "Bayard","Belfort", "Saint Aubin", "Dupuy"} },
        { "Toulouse Rive Gauche", new string[] { "Saint Cyprien", "Croix de Pierre", "Route d’Espagne", "Fontaine Lestant", "Arènes", "Bagatelle", "Papus", "Tabar", "Bordelongue", "Mermoz", "La Farouette", "Casselardit", "Fontaine-Bayonne", "Cartoucherie"} },
        { "Toulouse Nord", new string[] { "Minimes", "Barrière de Paris", "Ponts-Jumeaux", "Sept-Deniers", "Ginestous", "Lalande", "Trois cocus", "Borderouge", "Croix Daurade", "Paleficat", "Grand Selve"} },
        { "Toulouse Est", new string[] { "Lapujade", "Bonnefoy", "Periole", "Marengo", "La colonne", "Jolimont", "Soupetard", "Roseraie", "Gloire", "Gramont", "Amouroux", "Bonhoure", "Guilheméry", "Chateau de l’Hers", "Limayrac", "Cote Pavée" } },
        { "Toulouse Sud-Est", new string[] { "Pont des Demoiselles", "Ormeau", "Montaudran", "La Terrasse", "Malepère", "Rangueil", "Sauzelong", "Pech David", "Pouvourville", "Saint Michel", "Le Busca", "Empalot", "Saint Agne", } },
        { "Toulouse Ouest", new string[] { "Arènes Romaines", "Saint Martin du Touche", "Purpan", "Lardenne", "Pradettes", "Basso-Cambo", "Mirail Université", "Reynerie", "Bellefontaine", "Saint Simon", "Lafourgette", "Oncopole"} },
        { "Je n'habite pas Toulouse", new string[] { "Je n'habite pas Toulouse" } }
    };
    // Use this for initialization and enabling script in Editor
    void Start () {}
	
    private void OnEnable()
    {
        SetDropDownValues();
    }
   
    private void SetDropDownValues()
    {
        // Set DropDown 1 values
        Dropdown.OptionData itemdropDownArea;
        dropDownArea.options.Clear();

        foreach (KeyValuePair<string, string[]> entry in residenceDictionnary)
        {
            itemdropDownArea = new Dropdown.OptionData(entry.Key);
            dropDownArea.options.Add(itemdropDownArea);
        }

        // Set DropDown 2 values
        Dropdown.OptionData itemdropDropDownDistrict;
        dropDownDistrict.options.Clear();

        foreach (string var in residenceDictionnary["Toulouse Centre"])
        {
            itemdropDropDownDistrict = new Dropdown.OptionData(var);
            dropDownDistrict.options.Add(itemdropDropDownDistrict);
        }
    }

    // OnValueChange of Dropdown1
    public void OnValueChange_dropDownArea() {
        UpdatedropDownDistrict(dropDownArea.options[dropDownArea.value].text);
    }

    private void UpdatedropDownDistrict(string key)
    {
        Dropdown.OptionData itemdropDropDownDistrict;
        dropDownDistrict.options.Clear();

        foreach (string var in residenceDictionnary[key])
        {
            itemdropDropDownDistrict = new Dropdown.OptionData(var);
            dropDownDistrict.options.Add(itemdropDropDownDistrict);
        }
        
        //Update texte of the selected dropdown item 
        dropdownLabel2.text = residenceDictionnary[key][dropDownDistrict.value];
    }
}
