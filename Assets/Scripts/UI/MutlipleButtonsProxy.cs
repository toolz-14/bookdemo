﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MutlipleButtonsProxy : MonoBehaviour {

	[Tooltip("This event will be sent to every child button of this gameobject.")]
	public UnityEngine.Events.UnityEvent HotspotEvent;

	private Button[] _buttons;
	private bool DoOnce;

	private void Start () {
		RegisterToButtonsEvents();
	}

	public void StartRegisterToEvent () {
		RegisterToButtonsEvents();
	}

	private void TriggerHotspotEvent() {
		if (!DoOnce) {
			DoOnce = true;
			HotspotEvent.Invoke ();
			Invoke ("Delayed", 0.2f);
		}
	}

	private void Delayed(){
		DoOnce = false;
	}

	private void RegisterToButtonsEvents(){
		DoOnce = false;
		_buttons = GetComponentsInChildren<Button>();
		foreach(Button b in _buttons){
			b.onClick.AddListener(TriggerHotspotEvent);
		}
	}
}