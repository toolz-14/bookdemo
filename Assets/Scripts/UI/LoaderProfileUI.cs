﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Toolz.WebServices;

public class LoaderProfileUI : MonoBehaviour
{

    public AppLoader appLoader;
    public Text loginMessageToUser;

    public GameObject ConnectionView;
    public GameObject RegisterView;
    public Text Title;
    public Text registerMsgtoUser;

    public InputField userName;
    public InputField userFirstname;
    public InputField userLogin;
    public InputField userMail;
    public InputField userPassword;

    public GameObject LoginFormError;
    public InputField LoginUserName;
    public InputField LoginPassword;

    private GameObject _tempObject;
    private Vector2 _tempVect2;

    private RegisterUser _registerUserService;
    private UserLogin _userLoginService;
    private ApplicationLogin _checkUserService;
    public enum View : int { LOGIN = 0, REGISTER = 1 };

    public delegate void LoginCompleted(int userStatus); // call to PaneProject delegate to get all projects Data
    public static LoginCompleted loginCompleted; // instance

    public delegate void GetUserStatus(int status); // call to PanelCreerProjet delegate to get user status
    public static GetUserStatus getUserAccess; // instance
    private bool doOnce; // prevent spam on button

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _registerUserService = webService.GetComponent<RegisterUser>();
        _userLoginService = webService.GetComponent<UserLogin>();
        _checkUserService = webService.GetComponent<ApplicationLogin>();

        _registerUserService.onComplete += _registerUserService_onComplete;
        _registerUserService.onError += _registerUserService_onError;
        _userLoginService.onComplete += _userLoginService_onComplete;
        _userLoginService.onError += _userLoginService_onError;
        doOnce = false;
    }

    private void SelectViewToShow()
    {
        SwapView((int)View.LOGIN);
    }

    public void SwapView(int view)
    {
        //disable all views
        ConnectionView.SetActive(false);
        RegisterView.SetActive(false);

        //show relevant view
        switch ((View)view)
        {
            case View.LOGIN:
                ConnectionView.SetActive(true);
                Title.text = "Connexion";
                if (SaveLoadManager.LoadLastUserNameEntered() != null)
                {
                    LoginUserName.text = SaveLoadManager.LoadLastUserNameEntered();
                }

                if (SaveLoadManager.LoadLastPwdNameEntered() != null)
                {
                    LoginPassword.text = SaveLoadManager.LoadLastPwdNameEntered();
                }
                registerMsgtoUser.text = "";
                break;
            case View.REGISTER:
                loginMessageToUser.text = "";
                RegisterView.SetActive(true);
                Title.text = "Creation de Profil";
                break;
            default:
                ;
                break;
        }

        Title.transform.GetComponent<Lean.LeanLocalizedText>().PhraseName = Title.text;
        Title.transform.GetComponent<Lean.LeanLocalizedText>().UpdateLocalization();
    }

    public void RegisterButtonOK()
    {
        if (RegisterView.GetComponent<ProfileRegisterBook>().CheckAllFieldsComplete())
        {
            _registerUserService.lastname = userName.text;
            _registerUserService.firstname = userFirstname.text;
            _registerUserService.login = userLogin.text;
            _registerUserService.mail = userMail.text;
            _registerUserService.password = userPassword.text;

            _registerUserService.UseWebService();
            //Debug.Log("Finished RegisterUser() " + _registerUserService.lastname);
        }
        else
        {
            registerMsgtoUser.text = "Un des champs n'est pas valide, veuillez le compléter";
        }
    }

    public void LoginButtonOK()
    {
        //Debug.Log("LoginButtonOK");
        if (!doOnce && LoginUserName.text != "" && LoginPassword.text != "")
        {
            doOnce = true;
            _checkUserService.userName = LoginUserName.text;
            _checkUserService.onComplete += checkUserService_onComplete;
            _checkUserService.onError += checkUserService_onError;
            _checkUserService.UseWebService();
            Invoke("Delayed", 1.5f);
        }
        else if(LoginUserName.text == "" && LoginPassword.text == "")
        {
            loginMessageToUser.text = "Veuillez remplir tous les champs";
        }
    }

    private void Delayed()
    {
        doOnce = false;
    }

    // WEB SERVICES CALLBACKS
    private void _registerUserService_onError(long status, string message)
    {
        //        Debug.Log("Register user error");
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            registerMsgtoUser.text = "Veuillez vous connecter à internet pour vous enregistrer.";
        }
        else if (status.Equals((long)WebService.Status.PRECONDITION_FAILED))
        {
            if (_registerUserService.errorCode == 3)
            {
                registerMsgtoUser.text = "Votre entreprise n'est pas répertoriée";
            }
            if (_registerUserService.errorCode==4)
            {
                registerMsgtoUser.text = "Ce nom d'utilisateur est déjà utilisé veuillez en choisir un autre";
            }
            if (_registerUserService.errorCode == 5)
            {
                registerMsgtoUser.text = "Cet email est déjà utilisé veuillez en choisir un autre";
            }
        }
        else
        {
            Debug.Log("Error : " + message);
            registerMsgtoUser.text = "Un problème est survenu, veuillez réessayer plus tard.";
        }
    }

    private void _registerUserService_onComplete(long satus, string message)
    {
        SaveLoadManager.SaveLastUserNameEntered(userLogin.text);
        SaveLoadManager.SaveLastUserPWdEntered(userPassword.text);
        //Debug.Log("userLogin.text "+ userLogin.text);
        SwapView((int)View.LOGIN);
    }

    private void checkUserService_onError(long status, string message)
    {
        _checkUserService.onComplete -= checkUserService_onComplete;
        _checkUserService.onError -= checkUserService_onError;
        Debug.Log("status " + status + " result" + message);
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            loginMessageToUser.text = "Connexion internet requise";
        }
        else
        {
            loginMessageToUser.text = "Un problème est survenu, veuillez réessayer plus tard.";
        }
    }

    private void checkUserService_onComplete(long satus, string message)
    {
        _checkUserService.onComplete -= checkUserService_onComplete;
        _checkUserService.onError -= checkUserService_onError;
        //Debug.Log("checkUserService_onComplete " + message);
        if (satus == (long)WebService.Status.OK)
        {
            //Debug.Log("Service completed satus = OK");
            if (_checkUserService.userStatus == ApplicationLogin.ApplicationLoginStatus.NONE)
            {
                loginMessageToUser.text = "Veuillez vous enregistrer en cliquant sur le bouton \"Register\" ";
            }
            else if (_checkUserService.userStatus == ApplicationLogin.ApplicationLoginStatus.USER_BANNED)
            {
                //Debug.Log("userStatus == USER_BANNED");
                //message banned plus exit app(button text == exit)
                loginMessageToUser.text = "vous n'avez pas le droit d'acceder a l'application.\n Veuillez contacter Toolz";
            }
            else if (_checkUserService.userStatus == ApplicationLogin.ApplicationLoginStatus.USER_NEED_VALIDATION)
            {
                //Debug.Log("userStatus == USER_NEED_VALIDATION");
                //message need validation plus exit app(button text == exit)
                loginMessageToUser.text = "Votre demande d'accès a bien été recue et est en attente de validation.\n Veuillez verifier vos emails";
            }
            else if (_checkUserService.userStatus == ApplicationLogin.ApplicationLoginStatus.USER_OK || _checkUserService.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN)
            {
                //Debug.Log("userStatus == USER_OK");
                //no message and go to splash

                _userLoginService.login = LoginUserName.text;
                _userLoginService.password = LoginPassword.text;

                _userLoginService.UseWebService();

            }
            else
            {
                //Debug.Log("userStatus not recognised or NULL");
            }
        }
        else
        {
            //Debug.Log("Service completed satus = NOT OK");
        }
    }

    private void _userLoginService_onError(long status, string message)
    {
        //Debug.Log("status " + status + " result" + message);
        LoginFormError.SetActive(true);
    }

    private void _userLoginService_onComplete(long status, string message)
    {
        //Debug.Log("status " + status + " result" + message);
        SaveLoadManager.SaveLastUserNameEntered(LoginUserName.text);
        SaveLoadManager.SaveLastUserPWdEntered(LoginPassword.text);

        Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>();
        user.idUser = _userLoginService.idUser;
        user.login = _userLoginService.login;
        user.userName = _userLoginService.lastname;
        user.firstname = _userLoginService.firstname;
        user.mail = _userLoginService.mail;
        user.password = _userLoginService.password;
        user.userStatus = _userLoginService.userStatus;

        // Get projects
        loginCompleted(user.userStatus);
        getUserAccess(user.userStatus);

        //alpha out
        appLoader.LauchSplash();
        StartCoroutine("AlphaOut");
    }

    IEnumerator AlphaOut()
    {
        yield return new WaitForSeconds(0.1f);
        if (GetComponent<CanvasGroup>() != null)
        {
            if (GetComponent<CanvasGroup>().alpha > 0)
            {
                GetComponent<CanvasGroup>().alpha -= 0.1f;
                StartCoroutine("AlphaOut");
            }
            else
            {
                StopCoroutine("AlphaOut");
                gameObject.SetActive(false);
            }
        }
    }

    public void ResetAlpha()
    {
        GetComponent<CanvasGroup>().alpha = 1;
    }
}
