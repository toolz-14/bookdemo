﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TabNavigator : MonoBehaviour {

	public int startIndex = 0;
	private int targetIndex;

	public List<GameObject> objectTabs = new List<GameObject>();

	private void Start() {

		//targetIndex should be the inputfield below the on currently selected and should reset anytime an input field is selected by click/touch
		//if not clicked the start should be 0? first input field

		targetIndex = startIndex - 1;
//		SetCurrentTabObject();
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.Tab))
			SetCurrentTabObject();
	}

	private void SetCurrentTabObject() {
		targetIndex ++;
		if (targetIndex >= objectTabs.Count) {
			targetIndex = 0;
		}

		EventSystem.current.SetSelectedGameObject(objectTabs[targetIndex]);  
	}
}
