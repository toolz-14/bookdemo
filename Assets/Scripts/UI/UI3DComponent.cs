﻿using UnityEngine;
using System.Collections;

namespace Toolz.UI {
    [AddComponentMenu("Toolz/UI/UI3DComponent")]

    public class UI3DComponent : MonoBehaviour {

        public UnityEngine.Events.UnityEvent ClickEvent;
        public bool ActiveOnlyWhenModuleIsActive = true;

        private bool _activateInteraction;

		private void Start() {
        }

		private  void OnEnable() {
            Module parentModule = GetComponentInParent<Module>();
            if (parentModule) {
                parentModule.OnModuleActivated += CheckActivation;
                parentModule.OnModuleDeactivated += CheckActivation;
            }
            CheckActivation();
        }

		private  void OnDisable() {
            Module parentModule = GetComponentInParent<Module>();
            if (parentModule) {
                parentModule.OnModuleActivated -= CheckActivation;
                parentModule.OnModuleDeactivated -= CheckActivation;
            }
        }

        // PUBLIC
        public void OnMouseDown() {
            if (!_activateInteraction) {
                return;
            }
            ClickEvent.Invoke();
        }

        // PRIVATE
        private void CheckActivation() {
            if (ActiveOnlyWhenModuleIsActive) {
                GameObject parentModule = GetComponentInParent<Module>() ? GetComponentInParent<Module>().gameObject : null;
                if (parentModule != null) {
                    Transform t = parentModule.transform.Find(Module.ACTIVE_WHEN_MODULE_IS_ACTIVE);
					//Debug.Log(" t = "+t.name);
                    _activateInteraction = t != null && t.gameObject.activeInHierarchy;
                }
                else {
                    //If ever we have a UI3DComponent outside of any module...
                    _activateInteraction = true;
                }
            }
            else {
                _activateInteraction = true;
            }
        }
    }
}