﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HotspotsReorder : MonoBehaviour {

	public Transform theCamera;
	public List<Transform> HotspotsList = new List<Transform>();

	private void Start () {
		InvokeRepeating ("Reorder", 0f, 0.3f);
	}

//	private void Update(){
//		Reorder ();
//	}

	private void Reorder(){

		HotspotsList.Sort(delegate(Transform c1, Transform c2){
			return Vector3.Distance(theCamera.position, c1.position).CompareTo((Vector3.Distance(theCamera.position, c2.position)));   
		});

		for (int i = 0; i < HotspotsList.Count; i++) {
			HotspotsList [i].SetSiblingIndex ((HotspotsList.Count-1)-i);
		}
	}
}
