﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace Toolz.UI
{
	[AddComponentMenu("Toolz/UI/FadeInOutOnEnable")]

	public class FadeInOutOnEnable : MonoBehaviour {

		private float _alpha;
		public float speed = 1f;
		public CanvasGroup canvasGroup;
		public bool DoOnEnable = true;
		public bool FadeOutOnStart = true;
		public bool CallTransitionAfterFadeout = false;
		public Toolz.Triggers.TransitionTrigger transition;

		private void Awake () {

		}

		private void Start () {
			if(FadeOutOnStart){
				StartFadeIn(false);
			}
		}

		private void OnEnable(){
			if(DoOnEnable){
				StartFadeIn(true);
			}

		}


		public void StartFadeIn(bool isFadeIn){
			if(isFadeIn){
				StartCoroutine("FadeIn");
			}else{
				StartCoroutine("FadeOut");
			}
		}

		IEnumerator FadeIn(){
			if(canvasGroup.alpha < 1f){
				yield return new WaitForEndOfFrame();
				canvasGroup.alpha += Time.deltaTime * speed;
				StartCoroutine("FadeIn");
			}else{
				StopCoroutine("FadeIn");
			}
		}

		IEnumerator FadeOut(){
			if(canvasGroup.alpha > 0f){
				yield return new WaitForEndOfFrame();
				canvasGroup.alpha -= Time.deltaTime * speed;
				StartCoroutine("FadeOut");
			}else{
				if(CallTransitionAfterFadeout && transition != null){
					transition.TriggerTransition();
				}
				StopCoroutine("FadeOut");
			}
		}
	}
}
