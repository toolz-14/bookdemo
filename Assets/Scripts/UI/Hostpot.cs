﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace Toolz.UI {
	[AddComponentMenu("Toolz/UI/Hostpot")]

	public class Hostpot : MonoBehaviour {

		public bool IgnorePublicValues = false;
		public float DistanceForFlag = 9f;
		public float DistanceForCenter = 13f;
		public float _minDistanceForBothSL = 20f;
		public float _minDistanceForBothAL = 3f;

		public float FlagWidth = 180f;
		public float FlagHeight = 40f;
		public float Flag_x = -100f;
		public float Flag_y = 60f;
		public float Flag_z = -2.4f;
		public float Text_x = 10f;
		public float Text_y = -21f;

		public Color FlagColor = new Color(0.05f, 0.113f, 0.141f, 1f);



		public enum HSType{ StraightLineWithDot, AngledLineWithoutDot, test1 }
		public HSType HotSpotType = HSType.StraightLineWithDot;

		private Animator _emptyLineAndFlag;
		private Animator _center;

		private float _distanceToCamera;
		private const float _minScale = 0.1f;
		private const float _maxScale = 0.5f;
		private const float _distOffset = 0.01f;


		public bool _lineFlagVisible{
			get { return _emptyLineAndFlag.GetBool("isVisible"); }
			set { _emptyLineAndFlag.SetBool("isVisible", value); }
		}

		public bool _centerVisible{
			get { return _center.GetBool("isVisible"); }
			set { _center.SetBool("isVisible", value); }
		}

		private void Awake(){
			_emptyLineAndFlag = transform.Find("EmptyLineAndFlag").GetComponent<Animator>();
			_center = transform.Find("Empty_Center").GetComponent<Animator>();
			if (!IgnorePublicValues) {
				if (HotSpotType == HSType.AngledLineWithoutDot) {
					SetupAngledLineWithoutDotType ();
				}
				if (HotSpotType == HSType.StraightLineWithDot) {
					StraightLineWithDot ();
				}
			} 
		}

		private void Start (){
			SetElementsVisible(true, true);
		}

		private void OnEnable (){
			SetElementsVisible(true, true);
		}

		private void Update (){
			//Debug.Log ("DistanceTOCamera() = "+DistanceTOCamera());
			_distanceToCamera = DistanceTOCamera ();
			if(_distanceToCamera <= _minDistanceForBothSL && HotSpotType == HSType.StraightLineWithDot){
				SetElementsVisible(false, false);
			}else if(_distanceToCamera <= _minDistanceForBothAL && HotSpotType == HSType.AngledLineWithoutDot){
				SetElementsVisible(false, false);
			}else{
				if(_distanceToCamera < DistanceForFlag){
					SetElementsVisible(true, true);
				}else if(_distanceToCamera > DistanceForFlag && DistanceTOCamera() < DistanceForCenter){
					SetElementsVisible(false, true);
				}else{
					SetElementsVisible(false, false);
				}
				_distanceToCamera = Mathf.Clamp (_distanceToCamera * _distOffset, _minScale, _maxScale);
				transform.localScale = new Vector3 (_distanceToCamera, _distanceToCamera, _distanceToCamera);
			}
		}

		private float DistanceTOCamera(){
			return Vector3.Distance(Toolz.Managers.LevelManager.ActiveCamera.transform.position, transform.position);
		}

		private void SetElementsVisible( bool lineFlag, bool center){
			_lineFlagVisible = lineFlag;
			_centerVisible = center;

		}

		private void SetupAngledLineWithoutDotType(){
			//disable background and stroke
			transform.Find("Empty_Center").Find("Image_Background").gameObject.SetActive(false);
			transform.Find("Empty_Center").Find("Image_Stroke").gameObject.SetActive(false);

			//disable flag icon
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").Find("Image_Header").Find("Image_Icon").gameObject.SetActive(false);

			//rotate line and extend it a bit
			transform.Find("EmptyLineAndFlag").Find("Image_Line").GetComponent<RectTransform>().sizeDelta = new Vector2(3f, 40f);
			transform.Find("EmptyLineAndFlag").Find("Image_Line").localEulerAngles = new Vector3(357f, 1f, 310f);

			//reduce flag width as we removed the flag icon
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").Find("Image_Header").GetComponent<RectTransform>().sizeDelta = new Vector2(FlagWidth, FlagHeight); 

			//place flag
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").GetComponent<RectTransform>().localPosition = new Vector3(Flag_x, Flag_y, Flag_z);

			//place text area
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").Find("Image_Header").Find("Text_Title").GetComponent<RectTransform>().anchorMax = new Vector2(1f, 1f);
		}

		private void StraightLineWithDot(){
			//set flag width and height
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").Find("Image_Header").GetComponent<RectTransform>().sizeDelta = new Vector2(FlagWidth, FlagHeight); 
			
			//place flag
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").GetComponent<RectTransform>().localPosition = new Vector3(Flag_x, Flag_y, Flag_z);
			
			//place text area
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").Find("Image_Header").Find("Text_Title").GetComponent<RectTransform>().anchoredPosition = new Vector2(Text_x, Text_y);
			transform.Find("EmptyLineAndFlag").Find("Image_Flag").Find("Image_Header").Find("Text_Title").GetComponent<RectTransform>().anchorMax = new Vector2(1f, 1f);
		}
	}
}
