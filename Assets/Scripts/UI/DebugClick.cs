﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugClick : MonoBehaviour {

	private RaycastHit hit;
	private Ray ray;
	public Camera MyCamera;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		ray = MyCamera.ScreenPointToRay (Input.mousePosition);
		//get the selection
		if (Input.GetButtonDown ("Fire1")) {
			if (Physics.Raycast (ray, out hit)) {
				Debug.Log("hit.transform.gameObject.name = "+hit.transform.gameObject.name);
			}
		}
	}
}
