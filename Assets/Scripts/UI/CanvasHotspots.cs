﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public struct HospotData {
	public List<Sprite> HotspotIconList;
	public List<Color> HotspotIconColorList;
	public string ProjectName;
	//only for bagneux where 1proj == 1 theme only
	public Color OutlineColor;
}

public class CanvasHotspots : MonoBehaviour {

	public List<GameObject> HsHolderList = new List<GameObject> ();
	public Sprite[] SmallThemeTextures;
	public ThemeUIManager ThemePanel;

	private void Start () {
	
	}

	public HospotData GetHotspotData(string projectID){
		List<string> tempList = FindProjectActiveThemes(projectID);

		HospotData hospotData = new HospotData();
		hospotData.HotspotIconList = new List<Sprite>();
		hospotData.HotspotIconColorList = new List<Color>();
		hospotData.ProjectName = FindProjectName(projectID);

		if(tempList != null && tempList.Count > 0){
			foreach(string theme in tempList){
				hospotData.HotspotIconList.Add( GetThemeSprite(theme));
				hospotData.HotspotIconColorList.Add( GetThemeColor(theme));
				//only for bagneux where 1proj == 1 theme only
				hospotData.OutlineColor = GetThemeOutlineColor (theme);
			}
		}


		return hospotData;
	}

	private List<string> FindProjectActiveThemes(string projectID){
		Project[] allprojects = GameObject.FindObjectsOfType<Project> ();
		
		foreach (Project pr in allprojects) {
			if(pr.projectId == projectID){
				return pr.activeThemes;
			}
		}
		return null;
	}

	private string FindProjectName(string projectID){
		Project[] allprojects = GameObject.FindObjectsOfType<Project> ();
		
		foreach (Project pr in allprojects) {
			if(pr.projectId == projectID){
				return pr.projectName;
			}
		}
		return null;
	}
	
	private Color GetThemeColor(string themeName){
		foreach(Theme th in ThemePanel.availableThemes){
//			Debug.Log ("themeName = "+themeName);
//			Debug.Log ("th.themeName = "+th.themeName);
			if(th.themeName.Contains(themeName.ToLower())){
				return th.themeColor;
			}
		}
		return Color.white;
	}

	private Color GetThemeOutlineColor(string themeName){
		foreach(Theme th in ThemePanel.availableThemes){
			if(th.themeName.Contains(themeName.ToLower())){
				return th.themeOutlineColor;
			}
		}
		return Color.white;
	}

	private Sprite GetThemeSprite(string themeName){
		if (SmallThemeTextures.Length > 0) {
			foreach (Sprite sp in SmallThemeTextures) {
				if (sp.name.ToLower ().Contains (themeName)) {
					return sp;
				}
			}
		}
		return null;
	}

	//order sibling index for hotspots depending on distance to camera
	//to avoid seeing hospots through other hotspots

	//label vs hotspot ?


}
