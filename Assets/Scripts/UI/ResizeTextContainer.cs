﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResizeTextContainer : MonoBehaviour {

	private RectTransform _diapoButton;
	private RectTransform _theText;
	private RectTransform _theContainer;
	private float _childrenCombinedHeight;

	void Start () {
		_theContainer = GetComponent<RectTransform> ();
		_diapoButton = transform.Find ("Button").GetComponent<RectTransform> ();
		_theText = transform.Find ("Text").GetComponent<RectTransform> ();
	}
	

	public void ResizeContainer () {
		 //need to wait for the content size fitter update
		StartCoroutine(WaitForAFrame());
	}

	IEnumerator WaitForAFrame(){

		//wait 1 frame
		yield return 0;

		//calculate new height
		_childrenCombinedHeight = _diapoButton.sizeDelta.y + _theText.rect.height + 18f;

		//apply
		if (_childrenCombinedHeight > _theContainer.sizeDelta.y) {
			_theContainer.sizeDelta = new Vector2 (_theContainer.sizeDelta.x, _childrenCombinedHeight);
		}
	}
}
