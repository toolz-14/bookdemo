﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HotspotsOnOff : MonoBehaviour {

	public List<GameObject> HotspotsList = new List<GameObject>();
	public Text _buttonText;
	private string _textonoff = "";
	private bool _activate;

	private void Awake () {
		_activate = true;
	}

	private void Start () {
	
	}

	public void ActivateHotspot(){
		_activate = !_activate;
		for (int i=0; i<HotspotsList.Count; i++) {
			HotspotsList[i].SetActive(_activate);
		}
		if (_activate) {
			_textonoff = "On";
		} else {
			_textonoff = "Off";
		}
		_buttonText.text = _textonoff;
	}
}
