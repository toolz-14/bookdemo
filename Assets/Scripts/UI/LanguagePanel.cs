﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LanguagePanel : MonoBehaviour {

	public List<GameObject> LanguageButtonList = new List<GameObject>();
    private Color Grey82 = new Color(0.8235f, 0.8235f, 0.8235f, 1f);

    private void Start () {
		SetDefaultLanguage ();
	}

	private void SetDefaultLanguage(){
		SetColorOnClick (0);
	}

	//0 = French, 1 = English, 2 = German, 3 = Spanish, 4 = Portuguese
	public void SetColorOnClick(int ButtonID){
		SetAllInactif ();
		if(ButtonID >= 0 && ButtonID <= 4){
			LanguageButtonList [ButtonID].GetComponent<Image>().color = Grey82;
        }
	}

	private void SetAllInactif(){
		for (int i=0; i<LanguageButtonList.Count; i++) {
			LanguageButtonList[i].GetComponent<Image>().color = Color.white;
        }
	}
}
