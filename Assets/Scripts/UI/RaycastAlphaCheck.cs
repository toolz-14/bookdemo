﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RaycastAlphaCheck : MonoBehaviour {
	
	public Camera theCamera;
	public Texture2D buttonTexture;
	private Ray ray;
	private RaycastHit hit;
	private Color pixel;


	private void Start () {

	}

	protected virtual void Update () {
		if (Input.GetButton("Fire1")) {
			ray = theCamera.ScreenPointToRay(Input.mousePosition);
			if (GetComponent<Collider>().Raycast(ray, out hit, 100.0f))	{
				Color pixel = buttonTexture.GetPixel((int)(hit.textureCoord.x * buttonTexture.width + 0.5f), (int)(hit.textureCoord.y * buttonTexture.height + 0.5f));
				if (pixel.a > 0.9f){
					CallStuff();
				}
			}
		}
	}

	protected virtual void CallStuff(){

	}
}
