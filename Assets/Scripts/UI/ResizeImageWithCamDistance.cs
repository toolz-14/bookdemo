﻿using UnityEngine;
using System.Collections;

public class ResizeImageWithCamDistance : MonoBehaviour {

	private float _distanceToCamera;
	public float _minScale = 0.04f;
	public float _maxScale = 0.4f;
	public float _distOffset = 0.009f; 
	public Transform ModuleCamHolder;

	private void Start () {
	
	}

	private void Update (){
		//Debug.Log ("DistanceTOCamera() = "+DistanceTOCamera());
		_distanceToCamera = DistanceTOCamera ();

		_distanceToCamera = Mathf.Clamp (_distanceToCamera * _distOffset, _minScale, _maxScale);
		transform.localScale = new Vector3 (_distanceToCamera, _distanceToCamera, _distanceToCamera);

	}
	
	private float DistanceTOCamera(){
		return Vector3.Distance(ModuleCamHolder.position, transform.position);
	}

}
