﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace Toolz.UI {
	[AddComponentMenu("Toolz/UI/HotspotTransport")]

	public class HotspotTransport : MonoBehaviour {

		public bool resize = true;
		public float DistanceForIcon = 50f;
		private float _distanceToCamera;
		public float _minScale = 0.1f;
		public float _maxScale = 0.8f;
		public float _distOffset = 0.001f;


		private void Start (){
			
		}

		private void Update (){
			//Debug.Log ("DistanceTOCamera() = "+DistanceTOCamera());
			_distanceToCamera = DistanceTOCamera ();
			if (_distanceToCamera > DistanceForIcon) {
				//hide icon
				GetComponent<Image>().enabled = false;
			} else {
				GetComponent<Image>().enabled = true;
				if (resize) {
					_distanceToCamera = Mathf.Clamp (_distanceToCamera * _distOffset, _minScale, _maxScale);
					transform.localScale = new Vector3 (_distanceToCamera, _distanceToCamera, _distanceToCamera);
				}
			}
		}
		
		private float DistanceTOCamera(){
			return Vector3.Distance(Toolz.Managers.LevelManager.ActiveCamera.transform.position, transform.position);
		}
	}
}
