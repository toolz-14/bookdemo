﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResizeContainerPanelProject : MonoBehaviour
{

    private float containerSize;


    //private void Start()
    //{
    //    GetChildrensAndResize();

    //}
    public void StartResize()
    {
        StartCoroutine("Delay");
    }

    IEnumerator Delay()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        GetChildrensAndResize();
        StopCoroutine("Delay");
    }

    public void GetChildrensAndResize()
    {

        containerSize = 0f;
        
        foreach (Transform trans in gameObject.transform)
        {
            //add 30 to height per openned toggle
            if (trans.gameObject.activeInHierarchy)
            {
                containerSize += trans.GetComponent<RectTransform>().sizeDelta.y + GetComponent<VerticalLayoutGroup>().spacing;
            }
        }
        containerSize += GetComponent<VerticalLayoutGroup>().padding.top;
        containerSize += GetComponent<VerticalLayoutGroup>().padding.bottom;
        //Debug.Log(containerSize);
        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, containerSize);
		transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, containerSize);
        transform.parent.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, containerSize);
    }
}
