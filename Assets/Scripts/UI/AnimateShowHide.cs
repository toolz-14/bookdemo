﻿using UnityEngine;
using System.Collections;

namespace Toolz.UI {
    [AddComponentMenu("Toolz/UI/AnimateShowHide")]
    [RequireComponent(typeof(CanvasGroup))]

    public class AnimateShowHide : MonoBehaviour {

        public enum AnimateShowHideType { FADE_IN_OUT, TRANSLATE };
        [Tooltip("Type of animation you want.")]
        public AnimateShowHideType Type;

        public bool VisibleOnStart = false;
        public float Duration = 2f;
        public float Delay = 0f;

        //Private fields
        private bool _isAnimating;
        private float _startTime;
        private CanvasGroup _canvasGroup;
        private bool _showed;
        private float _actualDuration;
        private bool _onEnableCalledOnce = false;
        private bool _onDisableCalledOnce = false;

        void Awake() {
            //We know it's here thanks to "RequireComponent"
            _canvasGroup = GetComponent<CanvasGroup>();
            _isAnimating = false;
            _showed = false;
        }
        // Use this for initialization
        void Start() {
            _canvasGroup.alpha = 0f;
        }

        void OnEnable() {
            if (!_onEnableCalledOnce) {
                _onEnableCalledOnce = true;
                return;
            }
            if (VisibleOnStart) {
                ShowHide();
            }
        }

        void OnDisable() {
            if (!_onDisableCalledOnce) {
                _onDisableCalledOnce = true;
                return;
            }
            ShowHide();
        }


        // PUBLIC
        public void ShowHide() {
            switch (Type) {
                case AnimateShowHideType.FADE_IN_OUT :
                    Fade();
                    break;
                case AnimateShowHideType.TRANSLATE:
                    Debug.LogWarning("AnimateShowHide.TRANSLATE not implemented yet.");
                    break;
            }
        }

        // PRIVATE
        private void Fade() {
            string action = null;
            //If we are already fading out...
            if (_isAnimating && _showed) {
                CancelInvoke();
                action = "DoFadeIn";
                //We want to revert the last animation, so duration=last animation duration.
                _actualDuration = Time.realtimeSinceStartup - _startTime;
            }
            //If we are already fading in...
            else if (_isAnimating && !_showed) {
                CancelInvoke();
                action = "DoFadeOut";
                _actualDuration = Time.realtimeSinceStartup - _startTime;
            }
            //If we were not animating
            else {
                action = _showed ? "DoFadeOut" : "DoFadeIn";
                _actualDuration = Duration;
            }
            _isAnimating = true;
            _startTime = Time.realtimeSinceStartup;
            InvokeRepeating(action, Delay, 1/30f);
        }

        //Invoked by Fade()
        private void DoFadeIn() {
            float elapsedTime = Time.realtimeSinceStartup - _startTime;
            if (elapsedTime > _actualDuration) {
                _canvasGroup.alpha = 1f;
                _isAnimating = false;
                _showed = true;
                CancelInvoke();
                return;
            }

            _canvasGroup.alpha = elapsedTime / Duration;
        }

        //Invoked by Fade()
        private void DoFadeOut() {
            float elapsedTime = Time.realtimeSinceStartup - _startTime;
            if (elapsedTime > _actualDuration) {
                _canvasGroup.alpha = 0f;
                _isAnimating = false;
                _showed = false;
                CancelInvoke();
                return;
            }

            _canvasGroup.alpha = 1f - (elapsedTime / Duration);
        }
    }
}