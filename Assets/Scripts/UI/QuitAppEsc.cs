﻿using UnityEngine;
using System.Collections;

public class QuitAppEsc : MonoBehaviour {

	[SerializeField]
	private GameObject Fps;
	
	private void Start () {
	
	}
	
	private void Update() {
		if(Input.GetKeyDown(KeyCode.Escape) && !Fps.activeInHierarchy){
			Application.Quit();
		}
	}
}
