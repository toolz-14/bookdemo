﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class HSLabel : MonoBehaviour {

	public bool resize = true;
	public bool ActivateHeader = true;
	public bool ActivateLine = true;
	public bool ActivateDot = true;
	public float DistanceForFlag = 50f;
	public float DistanceForCenter = 80f;
	public float _minDistanceForBoth = 20f;
	public GameObject Image_Header;
	public GameObject Image_Line;
	public GameObject Empty_Center;
	public  float _minScale = 1f;
	public  float _maxScale = 2.5f;
	public  float _distOffset = 0.02f;

	private float _distanceToCamera;
	private bool DoOnce;


	private void Awake(){
		DoOnce = false;
	}

	private void Start (){
		InvokeRepeating ("MyUpdate", 0f, 0.03f);
	}

	private void OnEnable (){
		SetElementsVisible(true, true);
	}

	private void OnRenderObject(){
		if (!DoOnce) {
			DoOnce = true;
			StartUpdateSize ();
		}
	}

	public void StartUpdateSize(){
		Image_Header.GetComponent<ContentSizeFitter> ().enabled = true;
		Invoke("UpdateSize", 0.1f);
	}

	private void UpdateSize (){
		Vector2 SizeDelta = Image_Header.GetComponent<RectTransform> ().sizeDelta;
		Image_Header.GetComponent<ContentSizeFitter> ().enabled = false;
		Image_Header.GetComponent<RectTransform> ().sizeDelta = SizeDelta;
	}

	private void MyUpdate (){
//			Debug.Log ("DistanceTOCamera() = "+DistanceTOCamera());
		_distanceToCamera = DistanceTOCamera ();
		if(_distanceToCamera <= _minDistanceForBoth){
			SetElementsVisible(false, false);
		}else{
			if(_distanceToCamera < DistanceForFlag){
				SetElementsVisible(true, true);
			}else if(_distanceToCamera > DistanceForFlag && _distanceToCamera < DistanceForCenter){
				SetElementsVisible(false, true);
			}else{
				SetElementsVisible(false, false);
			}
			if(resize){
				_distanceToCamera = Mathf.Clamp (_distanceToCamera * _distOffset, _minScale, _maxScale);
				transform.localScale = new Vector3 (_distanceToCamera, _distanceToCamera, _distanceToCamera);
			}
		}
	}

	private float DistanceTOCamera(){
		return Vector3.Distance(Toolz.Managers.LevelManager.ActiveCamera.transform.position, transform.position);
	}

	private void SetElementsVisible( bool flagAndline, bool center){
		if (ActivateHeader) {
			Image_Header.SetActive (flagAndline);
		}
		if (ActivateLine) {
			Image_Line.SetActive (flagAndline);
		}
		if (ActivateDot) {
			Empty_Center.SetActive (center);
		}
	}
}
