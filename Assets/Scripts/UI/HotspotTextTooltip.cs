﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HotspotTextTooltip : MonoBehaviour {

	public string TheText = "";
	public TextTooltip TextTooltip;
	private string TheTitle = "";

	private void Start(){

	}

	public void OpenTooltip(){

		if (gameObject.name == "Text_Title") {
			TheTitle = GetComponent<Text> ().text;
		} else if (gameObject.name == "Image_Flag") {
			TheTitle = transform.Find ("Image_Header").Find ("Text_Title").GetComponent<Text> ().text;
		} else if (gameObject.name == "Image_Header") {
			TheTitle = transform.Find ("Text_Title").GetComponent<Text> ().text;
		} else {
			TheTitle = transform.Find ("EmptyLineAndFlag").Find ("Image_Flag").Find ("Image_Header").Find ("Text_Title").GetComponent<Text> ().text;
		}

		TextTooltip.SetTitle (TheTitle);
		TextTooltip.SetText (TheText);
		TextTooltip.OpenTextTooltipPanel ();
	}

}
