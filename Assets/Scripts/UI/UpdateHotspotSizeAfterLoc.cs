﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateHotspotSizeAfterLoc : MonoBehaviour {

	private List<Toolz.UI.Hostpot_v2> Hostpot_v2List = new List<Toolz.UI.Hostpot_v2>();
	private List<Hostpot_v2_Fiches> Hostpot_v2_FichesList = new List<Hostpot_v2_Fiches>();

	private void Start () {
		Invoke("GenerateLists", 0.3f);
	}

	private void GenerateLists(){
		Hostpot_v2List.Clear ();
		Toolz.UI.Hostpot_v2[] temparray = GameObject.FindObjectsOfType<Toolz.UI.Hostpot_v2> ();
		for(int i=0; i<temparray.Length; i++){
			Hostpot_v2List.Add (temparray[i]);
		}

		Hostpot_v2_FichesList.Clear ();
		Hostpot_v2_Fiches[] temparray2 = GameObject.FindObjectsOfType<Hostpot_v2_Fiches> ();
		for(int i=0; i<temparray2.Length; i++){
			Hostpot_v2_FichesList.Add (temparray2[i]);
		}
	}

	public void UpdateHotspotsSize(){
		for(int i=0; i<Hostpot_v2List.Count; i++){
			Hostpot_v2List [i].StartUpdateSize ();
		}

		for(int i=0; i<Hostpot_v2_FichesList.Count; i++){
			Hostpot_v2_FichesList [i].StartUpdateSize ();
		}
	}
}
