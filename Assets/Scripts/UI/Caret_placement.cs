﻿using UnityEngine;
using System.Collections;

public class Caret_placement : MonoBehaviour {


	void Start () {
		Invoke("PlaceCaret", 0.1f);
	}
	

	void PlaceCaret () {
		if (transform.Find ("InputField Input Caret") != null) {
			Vector3 temp = transform.Find ("InputField Input Caret").position;
			temp.y += 10f;
			transform.Find ("InputField Input Caret").position = temp;
		} else {
			Invoke("PlaceCaret", 0.2f);
		}
	}
}
