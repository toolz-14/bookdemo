﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace Toolz.UI {
	[AddComponentMenu("Toolz/UI/HotspotCycle")]

	public class HotspotCycle : MonoBehaviour {

		public bool resize = true;
		
		private float _distanceToCamera;
		public float _minScale = 0.1f;
		public float _maxScale = 0.8f;
		public float _distOffset = 0.001f;

		private void Start (){

		}

		private void Update (){
			//Debug.Log ("DistanceTOCamera() = "+DistanceTOCamera());
			_distanceToCamera = DistanceTOCamera ();
			if(resize){
				_distanceToCamera = Mathf.Clamp (_distanceToCamera * _distOffset, _minScale, _maxScale);
				transform.localScale = new Vector3 (_distanceToCamera, _distanceToCamera, _distanceToCamera);
			}
		}
		
		private float DistanceTOCamera(){
			return Vector3.Distance(Toolz.Managers.LevelManager.ActiveCamera.transform.position, transform.position);
		}
	}
}
