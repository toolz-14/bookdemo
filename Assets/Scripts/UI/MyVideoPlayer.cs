﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using System.IO;

public class MyVideoPlayer : MonoBehaviour {

    public GameObject loader;
    public VideoPlayer TheVideoComponent;
	public string TheVideoName = "";
	public VideoClip TheClip;
	public GameObject TheVideoCanvas;
	private bool IsPaused = false;
	public Sprite PauseOn;
	public Sprite PauseOff;
	public Image Pause;
	public Material CameraBG;
	private Material SavedMat = null;
    private string subFolder = "";

    private void Start()
    {
        if (GetComponentInParent<Project>() != null)
        {
            if (GetComponentInParent<Project>().is2018)
            {
                subFolder = "2018/";
            }
            //else if (GetComponentInParent<Project>().is2019)
            //{
            //    subFolder = "2019/";
            //}
        }
        TheVideoComponent.loopPointReached += EndReached;
    }

    public void StartTheVideo()
    {

        if (GetComponent<IsImage>().theCamera.GetComponent<PanoramicViewBouyguesWeb>() != null)
        {
            GetComponent<IsImage>().theCamera.GetComponent<PanoramicViewBouyguesWeb>().enabled = false;
        }
        GetComponent<IsImage>().theCamera.localRotation = Quaternion.identity;
        GetComponent<IsImage>().theCamera.localPosition = Vector3.zero;

        if (GetComponent<IsImage>() != null && GetComponent<IsImage>().StartwithImage)
        {
            GetComponent<IsImage>().theImage.SetActive(false);
        }
        if (TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material != null &&
            !TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material.name.Contains("Cam_black_bg"))
        {
            SavedMat = TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material;
        }
        TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material = CameraBG;

        TheVideoComponent.playOnAwake = false;
        TheVideoComponent.isLooping = false;

        if (TheVideoComponent.clip == null && TheVideoName != "")
        {
            TheVideoComponent.source = VideoSource.Url;
            //Debug.Log("video url = "+ "http://preprod.toolz.fr/internal/BagneuxVideos2019/" + subFolder + TheVideoName);
            //TheVideoComponent.url = "http://preprod.toolz.fr/internal/BagneuxVideos/" + TheVideoName + ".mp4";
            //TheVideoComponent.url = "http://budgetparticipatif.bagneux92.fr/carte3d//BagneuxVideos2019/" + subFolder + TheVideoName + ".mp4";
            //TheVideoComponent.url = "http://toolz.mairie-bagneux.fr:8080/BagneuxVideos2019/" + subFolder + TheVideoName + ".mp4";

            TheVideoComponent.url = "http://preprod.toolz.fr/internal/BookVideosProjet/" + TheVideoName + ".mp4";

        }

        //if (TheVideoComponent.length != 0)
        //{
        TheVideoComponent.playOnAwake = false;
        TheVideoComponent.isLooping = true;

        TheVideoComponent.audioOutputMode = VideoAudioOutputMode.AudioSource;
        TheVideoComponent.controlledAudioTrackCount = 1;
        TheVideoComponent.EnableAudioTrack(0, true);
        TheVideoComponent.SetTargetAudioSource(0, TheVideoComponent.transform.parent.parent.parent.GetComponent<AudioSource>());
        TheVideoComponent.transform.parent.parent.parent.GetComponent<AudioSource>().volume = 1f;

        StartCoroutine(PrepareAndPlayVideo());
    }

    IEnumerator PrepareAndPlayVideo()
    {
        TheVideoCanvas.SetActive(true);

        if (loader != null)
        {
            loader.SetActive(true);
        }

        TheVideoComponent.Prepare();
        while (!TheVideoComponent.isPrepared)
        {
            //Debug.Log("Preparing Video");
            yield return null;
        }

        TheVideoComponent.Play();
        TheVideoComponent.transform.parent.parent.parent.GetComponent<AudioSource>().Play();

        if (loader != null)
        {
            loader.SetActive(false);
        }
        if (GetComponent<IsImage>() != null && GetComponent<IsImage>().Loader != null)
        {
            GetComponent<IsImage>().Loader.SetActive(false);
        }


    }

    private void EndReached(VideoPlayer vp)
    {
        StopTheVideoPanelProject();
    }

    public void StopTheVideo()
    {
        if (TheVideoCanvas.activeInHierarchy)
        {
            if (IsPaused)
            {
                Pause.sprite = PauseOff;
                IsPaused = false;
            }
            StopCoroutine(PrepareAndPlayVideo());
            TheVideoComponent.Stop();
            TheVideoCanvas.SetActive(false);
            if (GetComponent<IsImage>() != null && GetComponent<IsImage>().StartwithImage)
            {
                GetComponent<IsImage>().theImage.SetActive(true);
            }
            TheVideoComponent.transform.parent.parent.parent.GetComponent<MeshRenderer>().material = SavedMat;
            if (GetComponent<IsImage>().theCamera.GetComponent<PanoramicViewBouyguesWeb>() != null)
            {
                GetComponent<IsImage>().theCamera.GetComponent<PanoramicViewBouyguesWeb>().enabled = true;
            }
            GetComponent<IsImage>().Reset360CamPosition();
        }
        if (loader != null)
        {
            loader.SetActive(false);
        }
        if (GetComponent<IsImage>() != null && GetComponent<IsImage>().Loader != null)
        {
            GetComponent<IsImage>().Loader.SetActive(false);
        }
    }

    public void StopTheVideoPanelProject()
    {
        StopTheVideo();
        ProjectUIManager.Instance.ActiveProjectPanel();
    }

    public void SwapVideoPause()
    {
        //if (TheVideoComponent.length != 0)
        //{
            IsPaused = !IsPaused;

            if (IsPaused)
            {
                PauseTheVideo();
                Pause.sprite = PauseOn;
            }
            else
            {
                TheVideoComponent.Play();
                Pause.sprite = PauseOff;
            }
        //}
    }

    public void PauseTheVideo()
    {
        TheVideoComponent.Pause();
    }
}
