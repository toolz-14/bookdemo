﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaycastOverride : Image {
    
    public override bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
    {
        Vector2 circleCenter = GetComponent<RectTransform>().position;
        
        // get scaled radius according to canvas scale
        float canvasScale = canvas.transform.localScale.x;
        float scaledRadius = GetComponent<RectTransform>().sizeDelta.x / 2 * canvasScale;

        float rotation = GetComponent<RectTransform>().rotation.eulerAngles.z;
		Vector2 ord = MyRotate(Vector2.up, rotation).normalized;
        Vector2 arcPointStart = circleCenter + ord * scaledRadius; // Get starting Point of given arc

        Vector2 rotationPointStart = circleCenter + new Vector2(0,1) * scaledRadius; 
        //transform.Find("Image1").GetComponent<RectTransform>().position = arcPoint2;

        bool is_in = false;
        is_in = PointInArc(screenPoint, circleCenter, rotationPointStart, arcPointStart, fillAmount);

        return is_in;
    }

    private static Vector2 MyRotate(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);

        return v;
    }

    private bool PointInArc(Vector2 screenPoint, Vector2 centerOfCircle, Vector2 pStart, Vector2 p1, float fillAmount)
    {
        bool result = false;

        float radius = GetComponent<RectTransform>().sizeDelta.x / 2;
        // get scaled radius according to canvas scale
        float canvasScale = canvas.transform.localScale.x;
        float scaledRadius = GetComponent<RectTransform>().sizeDelta.x / 2 * canvasScale;


        // Check 1st if the point is in the circle according to circle equation
        if (Mathf.Sqrt(Mathf.Pow(Mathf.Abs(screenPoint.x - centerOfCircle.x),2f) + Mathf.Pow(Mathf.Abs(screenPoint.y - centerOfCircle.y),2f)) < scaledRadius){
            // Other shorcut :
            // Vector2.Distance(circleCenter, screenPoint) < scaledRadius;

            #region Calculate fillAmountMouse
            float angleScreenPoint = Mathf.Atan2(screenPoint.x - centerOfCircle.x, screenPoint.y - centerOfCircle.y) - Mathf.Atan2(pStart.x - centerOfCircle.x, pStart.y - centerOfCircle.y); // get the angle between -Pi and Pi
            // convert angle to fill amount
            // slice 0 to Pi corresponds to 0 - 0.5 in fill
            float fillAmountMouse = (angleScreenPoint / Mathf.PI) * 0.5f; 

            // slice -Pi to 0 corresponds to 0.5 - 1 in fill
            if (fillAmountMouse < 0) { 
                fillAmountMouse = 1f + fillAmountMouse;
            }
            #endregion

            #region Calculate fillAmountP1
            float anglePoint1 = Mathf.Atan2(p1.x - centerOfCircle.x, p1.y - centerOfCircle.y) - Mathf.Atan2(pStart.x - centerOfCircle.x, pStart.y - centerOfCircle.y); // get the angle between -Pi and Pi
            // convert angle to fill amount
            // slice 0 to Pi corresponds to 0 - 0.5 in fill
            float fillAmount1 = (anglePoint1 / Mathf.PI) * 0.5f;
            // slice -Pi to 0 corresponds to 0.5 - 1 in fill
            if (fillAmount1 < 0)
            {
                fillAmount1 = 1f + fillAmount1;
            }
            #endregion

            // Calculate fillAmountP2
            float fillAmount2 = fillAmount1 + fillAmount;

            //Then check if screen point fill amount is between fill amount start and fill amount end
            if (fillAmount1< fillAmountMouse && fillAmountMouse < fillAmount2)
            {
                result = true;
            }
        }

        return result;
    }
}
