﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropDownForAge : MonoBehaviour {

	[SerializeField] int AgeMin = 10;
	[SerializeField] int AgeMax = 130;

	private Dropdown dropDown;


	private void Start () {
		
	}

	private void OnEnable(){
		SetDropDownValues ();
	}

	private void SetDropDownValues () {
		Dropdown.OptionData item;
		dropDown = GetComponent<Dropdown> ();

		dropDown.options.Clear();

		for (int i = AgeMin; i <= AgeMax; i++) {
			item = new Dropdown.OptionData(i.ToString());
			dropDown.options.Add (item);
		}
	}
}
