﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextTooltip : MonoBehaviour {

	private Transform _theTooltipText;
	private Transform _theTooltipTitle;
	[HideInInspector]
	public bool isOpen = false;

	private void Awake(){

	}

	private void Start(){

	}

	public void OpenTextTooltipPanel(){
	
		if(!isOpen){
			isOpen = true;
			GetComponent<Animation> ().GetComponent<Animation>() ["TextTooltip"].speed = 1f;
			
			GetComponent<Animation> ().Play ("TextTooltip");
			if(transform.Find ("Image_TextContainer").Find ("Text").GetComponent<RectTransform>().anchoredPosition.y > 0f){
				transform.Find ("Image_TextContainer").Find ("Text").GetComponent<RectTransform>().anchoredPosition = new Vector2(1f, -167f);
			}
		}
	}

	public void CloseTextTooltipPanel(){
		if(isOpen){
			isOpen = false;
			GetComponent<Animation> ().GetComponent<Animation>() ["TextTooltip"].time = GetComponent<Animation> ().GetComponent<Animation>() ["TextTooltip"].length;
			GetComponent<Animation> ().GetComponent<Animation>() ["TextTooltip"].speed = -1f;
			
			GetComponent<Animation> ().Play ("TextTooltip");
		}
	}

	public void SetText(string newText){
		if(newText != ""){
			_theTooltipText = transform.Find ("Image_TextContainer").Find ("Text");
			_theTooltipText.GetComponent<Text>().text = newText;
			if(newText.Contains("center Mosquee")){
				_theTooltipText.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTextBadiaMosquee";
			}else if(newText.Contains("Market complex in Badia")){
				_theTooltipText.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTextMarketComplex";
			}else if(newText.Contains("Employment complex text")){
				_theTooltipText.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTextEmployementComplex";
			}else{
				_theTooltipText.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTextElse";
			}

			_theTooltipText.GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
		}
	}

	public void SetTitle(string newTitle){
		if(newTitle != ""){
			_theTooltipTitle = transform.Find ("Panel_Header").Find ("Text_Title");
			_theTooltipTitle.GetComponent<Text>().text = newTitle;
			if(newTitle.Contains("Badia Mosquee") || newTitle.Contains("Mosquée de Badia")){
				_theTooltipTitle.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTitleBadiaMosquee";
			}else if(newTitle.Contains("Market complex") || newTitle.Contains("Centre commercial")){
				_theTooltipTitle.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTitleMarketComplex";
			}else if(newTitle.Contains("Employment complex") || newTitle.Contains("Pole emploi")){
				_theTooltipTitle.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTitleEmployementComplex";
			}else{
				_theTooltipTitle.GetComponent<Lean.LeanLocalizedText>().PhraseName = "TooltipTitleElse";
			}

			_theTooltipTitle.GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
		}
	}
}
