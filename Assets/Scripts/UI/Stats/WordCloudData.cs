﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Word
{
    public string word;
    public float value;
}

[Serializable]
public class Data
{
    public List<Word> data;
}

public class WordCloudData : MonoBehaviour
{
    private string url = "http://preprod.toolz.fr/internal/ite-backend/stats/mots";
    private float maxRatio = 2.5f;

	private List<Word> data;
    public Font font;
    private List<Transform> lines = new List<Transform>();

    // Use this for initialization
    IEnumerator Start()
    {
        //Initialize Random
        UnityEngine.Random.InitState(DateTime.Now.Millisecond);

        //Get JSon from data base
        using (WWW www = new WWW(url))
        {
            yield return www;
            if (www.text.Equals("")|| www.text == null)
            {
                yield break;
            }
            data = JsonUtility.FromJson<Data>("{\"data\" :" + www.text + "}").data;
        }
 
        FormatData();
        MakeCloud();

        //Refresh all UI elements
        foreach (RectTransform line in lines)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(line);
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());

        //Disable component ContentSizeFitter for optimization as it is not needed anymore
        foreach (var item in GetComponentsInChildren<ContentSizeFitter>())
            item.enabled = false;

        //Link lines to object
        foreach (Transform line in lines)
            line.SetParent(transform);
    }

    private void MakeCloud()
    {
        List<Transform> words = CreateWordList();
        words = FormatWordList(words);
        FillLines(words);
    }

    #region lines
    private void FillLines(List<Transform> words)
    {
        foreach (Transform word in words)
            word.SetParent(GetMatchingLine(word));
    }

    private GameObject CreateLine()
    {
        GameObject line = new GameObject("CloudLine");

        //Component in order to align words
        HorizontalLayoutGroup HLGroup = line.AddComponent<HorizontalLayoutGroup>();
        HLGroup.childControlWidth = false;

        line.AddComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.PreferredSize;

        line.transform.localScale = Vector3.one;

        lines.Add(line.transform);
        return line;
    }

    private Transform GetMatchingLine(Transform word)
    {
        float wordWidth = word.GetComponent<RectTransform>().rect.width;
        float width = 0;
        foreach (Transform line in lines)
        {
            foreach (RectTransform Rtrans in line.GetComponentsInChildren<RectTransform>())
                width += Rtrans.rect.width;

            if (width + wordWidth < this.GetComponent<RectTransform>().rect.width)
                return line;

            width = 0;
        }

        //Randomly sort the line to have different outputs
        lines.Sort((x, y) => UnityEngine.Random.Range(-1, 2));

        return CreateLine().transform;
    }
    #endregion

    #region words
    //Scale the biggest word to width / maxRatio and other words on this ratio
    private List<Transform> FormatWordList(List<Transform> words)
    {
        float ratio = ComputeRatio(words);

        if (ratio <= 1.1 && ratio >= 0.9)
            return words;

        foreach (Transform word in words)
        {
            Text text = word.GetComponent<Text>();
            text.fontSize = (int)(text.fontSize * ratio);
            LayoutRebuilder.ForceRebuildLayoutImmediate(word.GetComponent<RectTransform>());
        }

        return FormatWordList(words);
    }

    private float ComputeRatio(List<Transform> words)
    {
        float larger = words.Max(w => w.GetComponent<RectTransform>().rect.width);
        float maxWidth = this.GetComponent<RectTransform>().rect.width;

        return (maxWidth / maxRatio) / larger;

    }

    private List<Transform> CreateWordList()
    {
        List<Transform> words = new List<Transform>();
        foreach (Word word in data)
            words.Add(CreateWord(word));

        return words;
    }

    private Transform CreateWord(Word word)
    {
        GameObject go = new GameObject(word.word);

        ContentSizeFitter CSFilter = go.AddComponent<ContentSizeFitter>();
        CSFilter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        CSFilter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

        Text text = go.AddComponent<Text>();
		text.color = Color.black;
        text.text = word.word;

        text.font = font;
        text.fontSize = (int)word.value;

        go.transform.localScale = Vector3.one;

        LayoutRebuilder.ForceRebuildLayoutImmediate(go.GetComponent<RectTransform>());

        return go.transform;
    }
    #endregion

    #region data

    private void FormatData()
    {
        float sum = ComputeSum();

        foreach (Word word in data)
            word.value = (word.value / sum) * 100f;

        data.Sort((x, y) => UnityEngine.Random.Range(-1, 2));
    }

    private float ComputeSum()
    {
        float sum = 0f;

        foreach (Word word in data)
            sum += word.value;

        return sum;
    }
    #endregion
}
