﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieChartVisitingFrequency : PieChartUI
{

    public string[] elements = { "Tous les jours", "Plusieurs fois par semaine", "Une à deux fois par semaine", "Une à deux fois par mois", "Une à deux fois par an", "Autre" };

    // WEBSERVICE
    private Toolz.WebServices.GetPlaceVisitingFrequencyStats _getVisitingFrequencyStats;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getVisitingFrequencyStats = webService.GetComponent<Toolz.WebServices.GetPlaceVisitingFrequencyStats>();

        _getVisitingFrequencyStats.onComplete += _getVisitingFrequencyStats_onComplete;
        _getVisitingFrequencyStats.onError += _getVisitingFrequencyStats_onError;
    }

    protected override void Start()
    {
        base.Start();
        _getVisitingFrequencyStats.UseWebService();
    }


    // WEB SERVICES CALLBACKS
    private void _getVisitingFrequencyStats_onComplete(long status, string message)
    {
        //Debug.Log("GetPlaceVisitingFrequencyStats OnComplete " + status + " : " + message);
        values = new float[elements.Length];
        Dictionary<string, float> graphValues = _getVisitingFrequencyStats.elementsValues;
        for (int i = 0; i < elements.Length; i++)
        {
            if (graphValues.ContainsKey(elements[i]))
            {
                float value = graphValues[elements[i]];
                values[i] = value;
            }
            else
            {
                values[i] = 0f;
            }
        }
        base.CreateGraph();
    }

    private void _getVisitingFrequencyStats_onError(long status, string message)
    {
        Debug.Log("GetPlaceVisitingFrequencyStats OnError " + status + " : " + message);
    }
}
