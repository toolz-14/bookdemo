﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;

public class PieChartSatisfaction : MonoBehaviour
{

    // WEBSERVICE
    private Toolz.WebServices.GetSatisfactionsStats _getSatisfactionsStats;
    private Toolz.WebServices.GetUserSatisfactionComments _postSatisfactionsCommentsStats;

    private float[] values;
    public Color[] wedgeColor;
    [SerializeField]
    private RaycastOverride WedgeButtonPrefab;
    [SerializeField]
    private GameObject PanelPopUp;
    [SerializeField]
    private GameObject CommentElement;
    [SerializeField]
    private List<Image> legendeElements = new List<Image>();

    private float halfRadius;
    private float radiusWithOffset;
    private float offset = 8f;
    private List<GameObject> savedElements = new List<GameObject>();
    private Transform parent;
    private bool doOnce;

    private string[] elements = { "Très satisfaisant", "Plutôt satisfaisant", "Peu satisfaisant", "Pas satisfaisant du tout" };

    [SerializeField]
    protected GameObject errorMsgUI;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getSatisfactionsStats = webService.GetComponent<Toolz.WebServices.GetSatisfactionsStats>();
        _postSatisfactionsCommentsStats = webService.GetComponent<Toolz.WebServices.GetUserSatisfactionComments>();

        _getSatisfactionsStats.onComplete += _getSatisfactionsStats_onComplete;
        _getSatisfactionsStats.onError += _getSatisfactionsStats_onError;
        _postSatisfactionsCommentsStats.onComplete += _postSatisfactionsCommentsStats_onComplete;
        _postSatisfactionsCommentsStats.onError += _postSatisfactionsCommentsStats_onError;
    }

    private void Start()
    {
        doOnce = false;
        halfRadius = WedgeButtonPrefab.GetComponent<RectTransform>().sizeDelta.x / 4f;
        radiusWithOffset = (WedgeButtonPrefab.GetComponent<RectTransform>().sizeDelta.x / 2f) + offset;
        _getSatisfactionsStats.UseWebService();

    }

    private void CreateGraph()
    {
        float total = 0f;
        float zRotation = 0f;

        legendeElements[0].color = wedgeColor[0];
        legendeElements[1].color = wedgeColor[1];
        legendeElements[2].color = wedgeColor[2];
        legendeElements[3].color = wedgeColor[3];

        for (int i = 0; i < values.Length; i++)
        {
            total += values[i];
        }
        Vector3 temp;
        Quaternion childRotation;
        float bestNumber;
        for (int i = 0; i < values.Length; i++)
        {
            RaycastOverride newWedge = Instantiate(WedgeButtonPrefab) as RaycastOverride;
            newWedge.GetComponent<Button>().onClick.RemoveAllListeners();
            int index = i;
            newWedge.GetComponent<Button>().onClick.AddListener(() => OnWedgeClick(index));
            newWedge.transform.SetParent(transform, false);
            newWedge.color = wedgeColor[i];
            newWedge.fillAmount = values[i] / total;
            childRotation = newWedge.transform.Find("TextHolder").rotation;
            newWedge.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, zRotation));
            newWedge.transform.Find("TextHolder").rotation = childRotation;

            if (Mathf.Round(values[i] * 100f) != 0)
            {
                newWedge.transform.Find("TextHolder").Find("Text").GetComponent<Text>().text = (Mathf.Round(values[i] * 100f)).ToString();
                if (newWedge.fillAmount <= 0.088f) // if the slice is too small, reduce "%" size
                {
                    newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().fontSize = 8;
                    newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
                }
            }
            else
            {
                newWedge.transform.Find("TextHolder").Find("Text").GetComponent<Text>().text = "";
                newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().text = "";
            }

            temp = new Vector3(1, 1, 0);

            //move text
            if (newWedge.fillAmount <= 0.091f)
            {
                //need to move to position + out of wedge
                bestNumber = radiusWithOffset;

                //chnage color
                newWedge.transform.Find("TextHolder").Find("Text").GetComponent<Text>().color = newWedge.color;
                newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().color = newWedge.color;
            }
            else
            {
                bestNumber = halfRadius;
            }
            temp.x = Mathf.Sin(values[i] * Mathf.PI) * bestNumber;
            temp.y = Mathf.Cos(values[i] * Mathf.PI) * bestNumber;

            newWedge.transform.Find("TextHolder").localPosition = temp;

            zRotation -= newWedge.fillAmount * 360f;
        }
    }

    public void OnWedgeClick(int index)
    {
        if (!doOnce)
        {
            doOnce = true;
            
            _postSatisfactionsCommentsStats.statisfactionCategory = elements[index];
            _postSatisfactionsCommentsStats.UseWebService();
        }
    }

    private void Delayed()
    {
        float containerSizeX = 0f;
        foreach (Transform trans in parent)
        {
            if (containerSizeX < trans.GetComponent<RectTransform>().sizeDelta.x)
            {
                containerSizeX = trans.GetComponent<RectTransform>().sizeDelta.x;
            }
        }

        parent.GetComponent<RectTransform>().sizeDelta = new Vector2(containerSizeX + 40f, GetComponent<RectTransform>().sizeDelta.y);
        parent.GetComponent<ResizeContainerWithChildAmount>().GetChildrensAndResize();
        doOnce = false;
    }

    private void CleanPreviousElements()
    {
        foreach (GameObject go in savedElements)
        {
            Destroy(go);
        }
        savedElements.Clear();
    }

    // WEB SERVICES CALLBACKS
    private void _getSatisfactionsStats_onComplete(long status, string message)
    {
        //Debug.Log("getSatisfactionsStat OnComplete " + status + " : " + message);
        values = new float[elements.Length];
        Dictionary<string, float> graphValues = _getSatisfactionsStats.elementsValues;
        for (int i = 0; i < elements.Length; i++)
        {
            if (graphValues.ContainsKey(elements[i]))
            {
                float value = graphValues[elements[i]];
                values[i] = value;
            }
            else
            {
                values[i] = 0f;
            }
        }
        CreateGraph();
    }

    private void _getSatisfactionsStats_onError(long status, string message)
    {
        //Debug.Log("getSatisfactionsStat OnError " + status + " : " + message);

        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }

        PanelPopUp.SetActive(true);
        parent = PanelPopUp.transform.Find("Scroll View").GetComponent<ScrollRect>().content;
        CleanPreviousElements();

        // Instanciate only one element for message
        GameObject element = Instantiate(CommentElement) as GameObject;

        element.GetComponent<Text>().text = "Une erreur est survenue, veuillez rééssayer plus tard";
        element.transform.SetParent(parent, false);
        element.GetComponent<RectTransform>().localScale = Vector3.one;
        savedElements.Add(element);

        Invoke("Delayed", 0.2f);
    }

    private void _postSatisfactionsCommentsStats_onComplete(long status, string message)
    {
        PanelPopUp.SetActive(true);
        parent = PanelPopUp.transform.Find("Scroll View").GetComponent<ScrollRect>().content;

        CleanPreviousElements();

        // Instantiate Comments object
        if (_postSatisfactionsCommentsStats.elementsValues.Length > 0)
        {
            for (int i = 0; i < _postSatisfactionsCommentsStats.elementsValues.Length; i++)
            {
                GameObject element = Instantiate(CommentElement) as GameObject;

                element.GetComponent<Text>().text = _postSatisfactionsCommentsStats.elementsValues[i];

                element.transform.SetParent(parent, false);
                element.GetComponent<RectTransform>().localScale = Vector3.one;
                savedElements.Add(element);
            }
        }
        else {
            // Instanciate only one element for message
            GameObject element = Instantiate(CommentElement) as GameObject;

            element.GetComponent<Text>().text = "Pas de commentaires pour l'élement sélectionné";
            element.transform.SetParent(parent, false);
            element.GetComponent<RectTransform>().localScale = Vector3.one;
            savedElements.Add(element);
        }

        Invoke("Delayed", 0.2f);
    }

    private void _postSatisfactionsCommentsStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("_postSatisfactionsCommentsStats_onError OnError " + status + " : " + message);
    }
}