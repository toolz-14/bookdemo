﻿using UnityEngine;
using System.Collections.Generic;
using Toolz.WebServices;

public class PieChartElementOfSatisfaction : PieChartUI {

    public string[] elements = { "La végétation", "L’espace libre", "Le parking", "La liaison avec le jardin Compans-Cafarelli", "Rien", "Autre" };
    // WEBSERVICE
    private Toolz.WebServices.GetElementsOfSatisfactionStats _getGetElementsOfSatisfactionStats;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getGetElementsOfSatisfactionStats = webService.GetComponent<GetElementsOfSatisfactionStats>();

        _getGetElementsOfSatisfactionStats.onComplete += _getGetElementsOfSatisfactionStats_onComplete;
        _getGetElementsOfSatisfactionStats.onError += _getGetElementsOfSatisfactionStats_onError;
    }

    protected override void Start() {
        base.Start();
        _getGetElementsOfSatisfactionStats.UseWebService();
    }


    // WEB SERVICES CALLBACKS
    private void _getGetElementsOfSatisfactionStats_onComplete(long status, string message)
    {
        //Debug.Log("Get Survey Gender OnComplete " + status + " : " + message);
        values = new float[elements.Length];
        Dictionary<string, float> graphValues = _getGetElementsOfSatisfactionStats.elementsValues;
        for (int i=0; i < elements.Length; i++) {
            if (graphValues.ContainsKey(elements[i]))
            {
                float value = graphValues[elements[i]];
                values[i] = value;
            }
            else {
                values[i] = 0f;
            }
        }
        base.CreateGraph();
    }

    private void _getGetElementsOfSatisfactionStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("Get Survey Gender OnError " + status + " : " + message);
    }
}
