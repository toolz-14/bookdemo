﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PieChartUI : MonoBehaviour {

    public float[] values;
    public Color[] wedgeColor;
    [SerializeField]
    protected Image WedgePrefab;

    private float halfRadius;
    private float radiusWithOffset;
    private float offset = 8f;

    [SerializeField]
    protected GameObject errorMsgUI;

    protected virtual void Start()
    {
        halfRadius = WedgePrefab.GetComponent<RectTransform>().sizeDelta.x / 4f;
        radiusWithOffset = (WedgePrefab.GetComponent<RectTransform>().sizeDelta.x /2f) + offset;
	}

	protected void CreateGraph(){
		float total = 0f;
		float zRotation = 0f;

		for (int i = 0; i < values.Length; i++) {
			total += values [i];
		}
        Vector3 temp;
        Quaternion childRotation;
        Quaternion childRotation2;
        float bestNumber;
        for (int i=0; i<values.Length; i++){
			Image newWedge = Instantiate (WedgePrefab) as Image;
			newWedge.transform.SetParent (transform, false);
			newWedge.color = wedgeColor [i];
            newWedge.fillAmount = values [i] / total;
            childRotation = newWedge.transform.Find ("TextHolder").rotation;
            newWedge.transform.rotation = Quaternion.Euler (new Vector3(0f, 0f, zRotation));
			newWedge.transform.Find ("TextHolder").rotation = childRotation;

            if (Mathf.Round(values[i] * 100f) != 0)
            {
                newWedge.transform.Find("TextHolder").Find("Text").GetComponent<Text>().text = (Mathf.Round(values[i] * 100f)).ToString();
                if (newWedge.fillAmount <= 0.088f) // if the slice is too small, reduce "%" size
                {
                    newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().fontSize = 8;
                    newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
                }
            }
            else {
                newWedge.transform.Find("TextHolder").Find("Text").GetComponent<Text>().text = "";
                newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().text = "";
            }

            temp = new Vector3(1, 1, 0);

            //move text
            if (newWedge.fillAmount <= 0.091f) // if the slice is too small
            {
                //need to move to position + out of wedge
                bestNumber = radiusWithOffset;

                //chnage color
                newWedge.transform.Find("TextHolder").Find("Text").GetComponent<Text>().color = newWedge.color;
                newWedge.transform.Find("TextHolder").Find("TextPercent").GetComponent<Text>().color = newWedge.color;
            }
            else {
                bestNumber = halfRadius;
            }
            temp.x = Mathf.Sin(values[i] * Mathf.PI) * bestNumber;
            temp.y = Mathf.Cos(values[i] * Mathf.PI) * bestNumber;

            newWedge.transform.Find("TextHolder").localPosition = temp;

            zRotation -= newWedge.fillAmount * 360f;
		}
	}
}