﻿using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;

public class PieChartVisitingReason : PieChartUI
{

    public string[] elements = { "J’y passe pour aller travailler / étudier", "J’y passe quand je me promène dans le quartier", "J’y passe quand je fais du sport (footing, vélo, roller, etc.)", "Je m’y arrête pour faire du roller / skate", "Je m’y arrête pour me reposer, me détendre", "Autre" };

    // WEBSERVICE
    private GetVisitingReasonStats _getVisitingReasonStats;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getVisitingReasonStats = webService.GetComponent<GetVisitingReasonStats>();

        _getVisitingReasonStats.onComplete += _getVisitingReasonStats_onComplete;
        _getVisitingReasonStats.onError += _getVisitingReasonStats_onError;
    }

    protected override void Start()
    {
        base.Start();
        _getVisitingReasonStats.UseWebService();
    }


    // WEB SERVICES CALLBACKS
    private void _getVisitingReasonStats_onComplete(long status, string message)
    {
        //Debug.Log("GetVisitingReasonStats OnComplete " + status + " : " + message);
        values = new float[elements.Length];
        Dictionary<string, float> graphValues = _getVisitingReasonStats.elementsValues;
        for (int i = 0; i < elements.Length; i++)
        {
            if (graphValues.ContainsKey(elements[i]))
            {
                float value = graphValues[elements[i]];
                values[i] = value;
            }
            else
            {
                values[i] = 0f;
            }
        }
        base.CreateGraph();
    }

    private void _getVisitingReasonStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("GetVisitingReasonStats OnError " + status + " : " + message);
    }
}
