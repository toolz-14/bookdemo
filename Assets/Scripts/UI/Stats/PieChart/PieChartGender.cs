﻿using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class PieChartGender: PieChartUI {

    // WEBSERVICE
    private Toolz.WebServices.GetGenderStats _getGenderStats;
    [SerializeField]
    private Text textTotal;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getGenderStats = webService.GetComponent<Toolz.WebServices.GetGenderStats>();

        _getGenderStats.onComplete += _getGenderStats_onComplete;
        _getGenderStats.onError += _getGenderStats_onError;
    }

    protected override void Start() {
        base.Start();
        _getGenderStats.UseWebService();
    }


    // WEB SERVICES CALLBACKS
    private void _getGenderStats_onComplete(long status, string message)
    {
        //Debug.Log("Get Survey Gender OnComplete " + status + " : " + message);
        values = new float[2];
        values[0] = _getGenderStats.surveyGender.proportionOfWomen;
        values[1] = _getGenderStats.surveyGender.proportionOfMen;
        textTotal.text += (int)_getGenderStats.surveyGender.total + " personnes interrogées";
        base.CreateGraph();
    }

    private void _getGenderStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("Get Survey Gender OnError " + status + " : " + message);
    }
}
