﻿using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class PopulationChartPerSecteur : MonoBehaviour
{
    [SerializeField]
    private Text nonToulousainsEffectifTxt;

    [SerializeField]
	private List<Image> regions = new List<Image>();
	//modify image height to scale
	[SerializeField]
	private List<RectTransform> populationCharacters = new List<RectTransform>();
	[SerializeField]
	private Color regionColor = new Color();
	private int[] values;
	private float valueMax;
    // private string[] secteurArray = new string[] { "Toulouse Centre", "Toulouse Rive Gauche", "Toulouse Nord", "Toulouse Est", "Toulouse Ouest", "Toulouse Sud-Est" };

    // WEBSERVICE
    private Toolz.WebServices.GetResidenceAreaStats _getResidenceAreaStats;


    [SerializeField]
    protected GameObject errorMsgUI;
    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getResidenceAreaStats = webService.GetComponent<Toolz.WebServices.GetResidenceAreaStats>();

        _getResidenceAreaStats.onComplete += _getResidenceAreaStats_onComplete;
        _getResidenceAreaStats.onError += _getResidenceAreaStats_onError;
    }

    protected void Start()
    {
        _getResidenceAreaStats.UseWebService();
    }

    private void SetGraph() {
        if (values == null) {
            //Debug.Log("An error has occured, the values array is empty");
            return;
        }
        valueMax = 0f;
        for (int i = 0; i < values.Length; i++) {
            if (values[i] > valueMax) {
                valueMax = (float)values[i];
            }
        }

        for (int i = 0; i < regions.Count; i++) {
            //set region sprite color
            regions[i].color = ChangeColorBrightness(regionColor, 1f - (float)((float)values[i] / valueMax) - 0.1f);

            //set char size
            Vector2 temp = populationCharacters[i].sizeDelta;
            temp.y = CaluculateCharacterSize(values[i]);
            populationCharacters[i].sizeDelta = temp;
            regions[i].transform.Find("Text").GetComponent<Text>().text = values[i].ToString();
        }

        nonToulousainsEffectifTxt.text += values[regions.Count].ToString();

    }

	private float CaluculateCharacterSize(float value){
		//normalize value to 0,1 range
		float currentValue = value/valueMax;

		//remap value to 20,100 range
		currentValue = Remap(currentValue, 0f, 1f, 20f, 100f);

		return currentValue;
	}

	//from1,to1 is actual range, from2,to2 is wanted range
	private float Remap (float value, float from1, float to1, float from2, float to2) {
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
	}

	//correctionFactor MUST be between -1 and 1
	private Color ChangeColorBrightness(Color color, float correctionFactor) {

		float red = color.r;
		float green = color.g;
		float blue = color.b;

		if (correctionFactor < 0) {
			correctionFactor = 1 + correctionFactor;
			red *= correctionFactor;
			green *= correctionFactor;
			blue *= correctionFactor;
		}else {
			red = (1f - red) * correctionFactor + red;
			green = (1f - green) * correctionFactor + green;
			blue = (1f - blue) * correctionFactor + blue;
		}

		return  new Color(red, green, blue, color.a);
	}

    // WEB SERVICES CALLBACKS
    private void _getResidenceAreaStats_onComplete(long status, string message)
    {
        //Debug.Log("Get Survey Area Stats OnComplete " + status + " : " + message);
        this.values = _getResidenceAreaStats.resultsArray;
        SetGraph();
    }

    private void _getResidenceAreaStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("Get Survey Area Stats OnError " + status + " : " + message);
    }
}
