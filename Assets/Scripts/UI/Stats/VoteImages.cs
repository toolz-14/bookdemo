﻿using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class VoteImages : MonoBehaviour
{

    [SerializeField]
    private List<Image> images = new List<Image>();
    [SerializeField]
    private Color voteTextBackground;
    [SerializeField]
    private Color voteText;
    [SerializeField]
    private Dictionary<string, int> imagesVoteValues = new Dictionary<string, int>();

    private Object[] sprites;

    [SerializeField]
    protected GameObject errorMsgUI;

    // WEBSERVICE
    private Toolz.WebServices.GetImagesStats _getImagesStats;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getImagesStats = webService.GetComponent<Toolz.WebServices.GetImagesStats>();

        _getImagesStats.onComplete += _getResidenceAreaStats_onComplete;
        _getImagesStats.onError += _getResidenceAreaStats_onError;
    }

    private void Start()
    {
        _getImagesStats.UseWebService();
        //Invoke("Delayed", 1f);
    }


    private void Delayed()
    {
        _getImagesStats.UseWebService();
    }

    //the masks must already be set in image child in decreasing order
    private void SetGraph()
    {
        //amount of images must match vote amount
        if (imagesVoteValues.Count == images.Count)
        {
            int i = 0;
            sprites = Resources.LoadAll("SurveyImages", typeof(Sprite));
            foreach (KeyValuePair<string, int> kv in imagesVoteValues)
            {
                foreach (Sprite sp in sprites)
                {
                    if (sp.name.Equals(kv.Key))
                    {
                        images[i].sprite = sp;
                        images[i].transform.Find("Image_mask").Find("Image").GetComponent<Image>().color = voteTextBackground;
                        images[i].transform.Find("Image_mask").Find("Image").Find("Text").GetComponent<Text>().text = kv.Value.ToString() + " votes";
                        images[i].transform.Find("Image_mask").Find("Image").Find("Text").GetComponent<Text>().color = voteText;
                    }
                }
                i++;
            }
        }
    }

    private void ClearDictionnary()
    {
        imagesVoteValues.Clear();
        /*
        imagesVoteValues.Add("image1", 2000);
        imagesVoteValues.Add("image3", 1400);
        imagesVoteValues.Add("image4", 800);
        imagesVoteValues.Add("image2", 100);*/
    }

    // WEB SERVICES CALLBACKS
    private void _getResidenceAreaStats_onComplete(long status, string message)
    {
        //Debug.Log("Get Survey Images Stats OnComplete " + status + " : " + message);
        ClearDictionnary();
        imagesVoteValues = _getImagesStats.imagesVoteValues;
        SetGraph();
    }

    private void _getResidenceAreaStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("Get Survey Images Stats OnError " + status + " : " + message);
    }
}