﻿using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class JobChart : MonoBehaviour {

	[SerializeField]
	private List<Transform> images = new List<Transform>();
	private float[] values;

	private float valueMax = 0;
	float currentValue;


    [SerializeField]
    protected GameObject errorMsgUI;

    // WEBSERVICE
    private Toolz.WebServices.GetSocioCatStats _getSocioCatStats;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getSocioCatStats = webService.GetComponent<Toolz.WebServices.GetSocioCatStats>();

        _getSocioCatStats.onComplete += _getSocioCatStats_onComplete;
        _getSocioCatStats.onError += _getSocioCatStats_onError;
    }

    private void Start ()
    {
        _getSocioCatStats.UseWebService();
	}

	private void SetChart(){

		valueMax = 0f;
		for (int i = 0; i < values.Length; i++) {
			valueMax += (float)values [i];
		}

		for(int i=0; i<images.Count; i++){
			//normalize value to 0,1 range
			currentValue = values[i]/valueMax;
			double tempDouble = System.Math.Round ((values [i] * 100f), 2);
			images[i].Find("TextPercent").GetComponent<Text>().text = (tempDouble) + "%";

			//set char size
			Vector2 temp = images[i].Find("Image").GetComponent<RectTransform>().sizeDelta;
			//remap value to 50,130 range
			if (currentValue == 0) {
				temp.y = 0;
			} else {
				temp.y = Remap (currentValue, 0f, 1f, 50f, 130f);
			}
			images[i].Find("Image").GetComponent<RectTransform>().sizeDelta = temp;
		}
	}


	//from1,to1 is actual range, from2,to2 is wanted range
	private float Remap (float value, float from1, float to1, float from2, float to2) {
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
    
    // WEB SERVICES CALLBACKS
    private void _getSocioCatStats_onComplete(long status, string message)
    {
        //Debug.Log("Get Socio Stats OnComplete " + status + " : " + message);
        values = _getSocioCatStats.socioCatValues;
        SetChart();
    }

    private void _getSocioCatStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("Get Socio Stats OnError " + status + " : " + message);
    }
}
