﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FillGenderLegends : MonoBehaviour {

    [SerializeField]
    protected GameObject[] legendGroup;

    [SerializeField]
    private GameObject pieChartsGender;
    private PieChartUI _pieChartsScript;

    // Use this for initialization
    private void Start () {

        this._pieChartsScript = pieChartsGender.GetComponent<PieChartUI>();
        for(int i = 0; i < legendGroup.Length; i++)
        {
            Transform child = legendGroup[i].transform.Find("Image");
            child.GetComponent<Image>().color = this._pieChartsScript.wedgeColor[i];
            /*
            foreach (Transform child in legendGroup[i].transform)
            {
                if (child.name == "Image")
                {
                    child.GetComponent<Image>().color = this._pieChartsScript.wedgeColor[i];
                }
            }*/
        }
    }
    
}
