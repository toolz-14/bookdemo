﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FillVisitingReasonLegends : MonoBehaviour
{
    [SerializeField]
    protected GameObject[] legendGroup;

    [SerializeField]
    private GameObject pieChartElementOfSatisfaction;
    private PieChartVisitingReason _pieChartsScript;

    // Use this for initialization
    private void Start()
    {
        this._pieChartsScript = pieChartElementOfSatisfaction.GetComponent<PieChartVisitingReason>();
        legendGroup = new GameObject[this.transform.childCount];
        for (int i = 0; i < this.transform.childCount; i++)
        {
            legendGroup[i] = this.transform.GetChild(i).gameObject;
        }

        for (int i = 0; i < legendGroup.Length; i++)
        {
            Transform childImage = legendGroup[i].transform.Find("Image");
            childImage.GetComponent<Image>().color = this._pieChartsScript.wedgeColor[i];
        }
    }
}
