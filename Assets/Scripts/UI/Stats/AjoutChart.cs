﻿using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class AjoutChart : MonoBehaviour {

	[SerializeField]
	private RectTransform[] HorizontalBars;
	private int[] values;

	private float valueMax;
	private float currentValue;
    
    // WEBSERVICE
    private Toolz.WebServices.GetSurveyElementsStats _getSurveyElementsStats;

    [SerializeField]
    protected GameObject errorMsgUI;
    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getSurveyElementsStats = webService.GetComponent<Toolz.WebServices.GetSurveyElementsStats>();

        _getSurveyElementsStats.onComplete += _getSurveyElementsStats_onComplete;
        _getSurveyElementsStats.onError += _getSurveyElementsStats_onError;
    }

    protected void Start()
    {
        _getSurveyElementsStats.UseWebService();
    }

    private void SetGraph(){

		valueMax = 0f;

		foreach(int value in values){
			if (value > valueMax) {
				valueMax = (float)value;
			}
		}

		for(int i=0 ; i < values.Length; i++) {
			currentValue = (float)values[i] / valueMax;

			HorizontalBars[i].transform.Find("Text").GetComponent<Text>().text = (values[i]).ToString ();

			Vector2 size = HorizontalBars[i].sizeDelta;
			size.y = Remap (currentValue, 0f, 1f, 12f, 310f);
			HorizontalBars[i].sizeDelta = size;
		}
	}

	//from1,to1 is actual range, from2,to2 is wanted range
	private float Remap (float value, float from1, float to1, float from2, float to2) {
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    // WEB SERVICES CALLBACKS
    private void _getSurveyElementsStats_onComplete(long status, string message)
    {
        //Debug.Log("Get Age Stats OnComplete " + status + " : " + message);
        values = this._getSurveyElementsStats.elementsValues;
        SetGraph();
    }

    private void _getSurveyElementsStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            this.errorMsgUI.SetActive(true);
        }
        //Debug.Log("Get Age Stats OnError " + status + " : " + message);
    }
}
