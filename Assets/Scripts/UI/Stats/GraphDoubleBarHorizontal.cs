﻿using Toolz.WebServices;
using UnityEngine;
using UnityEngine.UI;

public class GraphDoubleBarHorizontal : MonoBehaviour {

	//each index represent an age range starting with lowest one (0-15), value is amount
	[SerializeField]
	private int[] valuesHomme;
	//each index represent an age range starting with lowest one (0-15), value is amount
	[SerializeField]
	private int[] valuesFemme;
	[SerializeField]
	private GameObject barHomme;
	[SerializeField]
	private GameObject barFemme;

	private int valueMaxFemme;
	private int valueMaxHomme;

    // WEBSERVICE
    private Toolz.WebServices.GetAgesStats _getAgeStats;

    [SerializeField]
    private GameObject errorMsgUI;

    private void Awake()
    {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _getAgeStats = webService.GetComponent<Toolz.WebServices.GetAgesStats>();

        _getAgeStats.onComplete += _getAgeStats_onComplete;
        _getAgeStats.onError += _getAgeStats_onError;
    }

    protected void Start()
    {
        _getAgeStats.UseWebService();
    }

    private void CreateGraph(){
		valueMaxFemme = 0;
		for (int i = 0; i < valuesFemme.Length; i++) {
			if(valuesFemme[i] > valueMaxFemme){
				valueMaxFemme = valuesFemme [i];
			}
		}

		for(int i=0; i<valuesFemme.Length; i++){

			GameObject tempBar = Instantiate (barFemme) as GameObject;
			tempBar.transform.SetParent (transform);
			tempBar.transform.localScale = Vector3.one;

			Vector2 size = tempBar.GetComponent<RectTransform> ().sizeDelta;
			tempBar.transform.Find ("Text").GetComponent<Text> ().text = valuesFemme [i].ToString ();
			float tempValue = ((float)valuesFemme [i] / (float)valueMaxFemme);
			size.x = Remap(tempValue, 0, 1, 20, 120);
			tempBar.GetComponent<RectTransform> ().sizeDelta = size;

			Vector3 temp = Vector3.zero;
			temp.x = 5f;
			temp.y = -101f + (i * 30f);
			tempBar.GetComponent<RectTransform>().localPosition = temp;
		}

		valueMaxHomme = 0;
		for (int i = 0; i < valuesHomme.Length; i++) {
			if(valuesHomme[i] > valueMaxHomme){
				valueMaxHomme = valuesHomme [i];
			}
		}

		for(int i=0; i<valuesHomme.Length; i++){

			GameObject tempBar = Instantiate (barHomme) as GameObject;
			tempBar.transform.SetParent (transform);
			tempBar.transform.localScale = Vector3.one;

			Vector2 size = tempBar.GetComponent<RectTransform> ().sizeDelta;
			tempBar.transform.Find ("Text").GetComponent<Text> ().text = valuesHomme [i].ToString ();
			float tempValue = ((float)valuesHomme [i] / (float)valueMaxHomme);
			size.x = Remap(tempValue, 0, 1, 20, 120);
			tempBar.GetComponent<RectTransform> ().sizeDelta = size;

			Vector3 temp = Vector3.zero;
			temp.x = -5f;
			temp.y = -101f + (i * 30f);
			tempBar.GetComponent<RectTransform>().localPosition = temp;
		}

	}

	//from1,to1 is actual range, from2,to2 is wanted range
	private float Remap (float value, float from1, float to1, float from2, float to2) {
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    // WEB SERVICES CALLBACKS
    private void _getAgeStats_onComplete(long status, string message)
    {
        //Debug.Log("Get Age Stats OnComplete " + status + " : " + message);
        valuesFemme = _getAgeStats.agesDictionnary[0];
        valuesHomme = _getAgeStats.agesDictionnary[1];
        CreateGraph();
    }

    private void _getAgeStats_onError(long status, string message)
    {
        if (status.Equals((long)WebService.Status.NO_CONNECTION)) // Si l'utilisateur n'a pas accès à internet
        {
            errorMsgUI.SetActive(true);
        }
        //Debug.Log("Get Age Stats OnError " + status + " : " + message);
    }
}
