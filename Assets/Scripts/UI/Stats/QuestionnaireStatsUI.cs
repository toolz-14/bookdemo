﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionnaireStatsUI : MonoBehaviour {

	[SerializeField] private GameObject ButtonPrevious;
	[SerializeField] private GameObject ButtonNext;
	public List<GameObject> SubPanelList = new List<GameObject> ();

	private int CurrentPanelIndex;

	private void Start () {
		
	}

	private void OnEnable () {
		Init ();
	}

	private void Init () {
		if (SubPanelList.Count > 0) {
			CurrentPanelIndex = 0;
			ButtonPrevious.SetActive (false);
			if (SubPanelList.Count == 1) {
				ButtonNext.SetActive (false);
			} else {
				ButtonNext.SetActive (true);
			}
			foreach(GameObject go in SubPanelList){
				go.SetActive (false);
			}
			//Debug.Log ("Init (), CurrentPanelIndex = "+CurrentPanelIndex);
			SubPanelList [CurrentPanelIndex].SetActive (true);
		} else {
			// error message array empty
		}
	}

	public void ButtonPreviousClick(){

		SubPanelList [CurrentPanelIndex].SetActive (false);
		CurrentPanelIndex--;
		SubPanelList [CurrentPanelIndex].SetActive (true);

		//Next and Previous buttons display
		if(CurrentPanelIndex == 0){
			ButtonPrevious.SetActive (false);
		}
		if(CurrentPanelIndex == SubPanelList.Count-2){
			ButtonNext.SetActive (true);
		}
	}

	public void ButtonNextClick(){

		SubPanelList [CurrentPanelIndex].SetActive (false);
		CurrentPanelIndex++;
		SubPanelList [CurrentPanelIndex].SetActive (true);

		//Next and Previous buttons display
		if(CurrentPanelIndex == SubPanelList.Count-1){
			ButtonNext.SetActive (false);
		}
		if(CurrentPanelIndex == 1){
			ButtonPrevious.SetActive (true);
		}
	}
}
