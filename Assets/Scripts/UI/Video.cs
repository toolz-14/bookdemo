﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Video : MonoBehaviour {

#if UNITY_STANDALONE
    private MovieTexture movie;
	private AudioSource sound;

	void Awake(){
		movie = GetComponent<RawImage>().texture as MovieTexture;
		sound = GetComponent<AudioSource>();
	}

	public void PlayVideo(){
		movie.Play();
		sound.Play();
		StartCoroutine("WaitForMovieEnd");
	}

	public void StopVideo(){
		StopCoroutine("WaitForMovieEnd");
		movie.Stop();
		sound.Stop();
		GoToPreloader();
	}

	IEnumerator WaitForMovieEnd(){
		yield return new WaitForSeconds(movie.duration);
		GoToPreloader();
	}

	private void GoToPreloader(){
		Application.LoadLevel ("Preloader");
	}
#elif UNITY_ANDROID || UNITY_IOS

	public string ChosenVideo = "SF_VIDEO_INTRO_H264.mp4";

	public void PlayVideo(){
		StartCoroutine("PlayVideoCoroutine");
	}

	IEnumerator PlayVideoCoroutine(){
		Handheld.PlayFullScreenMovie (ChosenVideo, Color.black, FullScreenMovieControlMode.CancelOnInput);    
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		//Debug.Log("Video playback completed.");
		SceneManager.LoadScene ("Preloader");
	}
#endif
}
