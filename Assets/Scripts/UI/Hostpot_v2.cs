﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace Toolz.UI {
	[AddComponentMenu("Toolz/UI/Hostpot_v2")]

	public class Hostpot_v2 : MonoBehaviour {

		public bool IsProjectHotspot = false;
		public bool onlyStroke = false;
		public bool resize = true;

		public float DistanceForFlag = 50f;
		public float DistanceForCenter = 80f;
		public float _minDistanceForBoth = 20f;

		public Color FlagColor = new Color(0.05f, 0.113f, 0.141f, 1f);
		public Color TextColor = new Color(0.77f, 0.86f, 0.854f, 1f);
		public Color LineColor = new Color(1f, 1f, 1f, 1f);
		public Color PointColor = new Color(1f, 1f, 1f, 1f);
		public Color StrokeColor = new Color(0.05f, 0.113f, 0.141f, 1f);

		public int Padding_Right = 6;
		public int Padding_Left = 6;
		public int Padding_Top = 6;
		public int Padding_Bottom = 6;
		public float Spacing = 8f;

		public float Icon_Width = 20f;
		public float Icon_Height = 20f;


		private Animator _emptyLineAndFlag;
		private Animator _center;
		private HorizontalLayoutGroup _horizontalLG;

		public float Icon_Xpos = 35;
		public bool UseProjectName;
		public string HotSpotName;
		public string ProjectID;
		public GameObject Image_Header;
		public GameObject Empty_Center;
		public GameObject Image_Icon;
		public GameObject Image_Stroke;
		
		private CanvasHotspots _canvasHotspots;
		private Vector2 _tempVect2;
		private Vector3 _tempVect3;
		private float _offset = 3f;
		private const float _themeIconYPos = 1f;

		private float _distanceToCamera;
		public  float _minScale = 1f;
		public  float _maxScale = 2.5f;
		public  float _distOffset = 0.02f;
		private bool DoOnce;

		public bool _lineFlagVisible{
			get { return _emptyLineAndFlag.GetBool("isVisible"); }
			set { _emptyLineAndFlag.SetBool("isVisible", value); }
		}

		public bool _centerVisible{
			get { return _center.GetBool("isVisible"); }
			set { _center.SetBool("isVisible", value); }
		}

		private void Awake(){
			if (transform.Find ("EmptyLineAndFlag") != null && transform.Find ("EmptyLineAndFlag").GetComponent<Animator> () != null) {
				_emptyLineAndFlag = transform.Find ("EmptyLineAndFlag").GetComponent<Animator> ();
			}
			_center = transform.Find("Empty_Center").GetComponent<Animator>();
			_canvasHotspots = transform.parent.parent.GetComponent<CanvasHotspots> ();
		}

		private void Start (){
			if (IsProjectHotspot) {
				SetIcons_Name_Colors ();
			}
			SetElementsVisible(true, true);
			if (GetComponent<MutlipleButtonsProxy> () != null) {
				GetComponent<MutlipleButtonsProxy> ().StartRegisterToEvent();
			}
			InvokeRepeating ("MyUpdate", 0f, 0.03f);
		}

		private void OnRenderObject(){
			if (!DoOnce) {
				DoOnce = true;
				StartUpdateSize ();
			}
		}

		private void OnEnable (){
			SetElementsVisible(true, true);
			if (GetComponent<MutlipleButtonsProxy> () != null) {
				GetComponent<MutlipleButtonsProxy> ().StartRegisterToEvent();
			}
		}

		public void StartUpdateSize(){
			Image_Header.GetComponent<ContentSizeFitter> ().enabled = true;
			Invoke("UpdateSize", 0.1f);
		}

		private void UpdateSize (){
			Vector2 SizeDelta = Image_Header.GetComponent<RectTransform> ().sizeDelta;
			Image_Header.GetComponent<ContentSizeFitter> ().enabled = false;
			Image_Header.GetComponent<RectTransform> ().sizeDelta = SizeDelta;
		}

		public void UpdateHotspot(){
			SetElements();
		}

		private void MyUpdate (){
//			Debug.Log ("DistanceTOCamera() = "+DistanceTOCamera());
			_distanceToCamera = DistanceTOCamera ();
			if(_distanceToCamera <= _minDistanceForBoth){
				SetElementsVisible(false, false);
			}else{
				if(_distanceToCamera < DistanceForFlag){
					SetElementsVisible(true, true);
				}else if(_distanceToCamera > DistanceForFlag && _distanceToCamera < DistanceForCenter){
					SetElementsVisible(false, true);
				}else{
					SetElementsVisible(false, false);
				}
				if(resize){
					_distanceToCamera = Mathf.Clamp (_distanceToCamera * _distOffset, _minScale, _maxScale);
					transform.localScale = new Vector3 (_distanceToCamera, _distanceToCamera, _distanceToCamera);
				}
			}
		}

		private float DistanceTOCamera(){
			return Vector3.Distance(Toolz.Managers.LevelManager.ActiveCamera.transform.position, transform.position);
		}

		private void SetElementsVisible( bool lineFlag, bool center){
			if (!onlyStroke) {
				_lineFlagVisible = lineFlag;
			}
			_centerVisible = center;
		}

		private void SetElements(){
			//set padding and spacing
			if (transform.Find ("EmptyLineAndFlag") != null) {
				_horizontalLG = transform.Find ("EmptyLineAndFlag").Find ("Image_Header").GetComponent<HorizontalLayoutGroup> ();
				_horizontalLG.spacing = Spacing;
				_horizontalLG.padding.right = Padding_Right;
				_horizontalLG.padding.left = Padding_Left;
				_horizontalLG.padding.top = Padding_Top;
				_horizontalLG.padding.bottom = Padding_Bottom;

				//set flag color
				transform.Find ("EmptyLineAndFlag").Find ("Image_Header").GetComponent<Image> ().color = FlagColor;

				//set text color
				transform.Find ("EmptyLineAndFlag").Find ("Image_Header").Find ("Text_Title").GetComponent<Text> ().color = TextColor;

				//set line color
				transform.Find ("EmptyLineAndFlag").Find ("Image_Line").GetComponent<Image> ().color = LineColor;
			}

			if (transform.Find ("Empty_Center") != null) {
				//set point color
				transform.Find ("Empty_Center").Find ("Image_Dot").GetComponent<Image> ().color = PointColor;

				//set stroke color
				transform.Find ("Empty_Center").Find ("Image_Stroke").GetComponent<Image> ().color = StrokeColor;
			}

			//set project name
			if(HotSpotName != ""){
				Debug.Log("HotSpotName has string");
				HotSpotName = HotSpotName.Replace("NEWLINE", "\n");
				Image_Header.transform.Find ("Text_Title").GetComponent<Text> ().text = HotSpotName;
				if(Image_Header != null && Image_Header.transform.Find ("Text_Title") != null &&
				   Image_Header.transform.Find ("Text_Title").GetComponent<Lean.LeanLocalizedText>() != null){
					Image_Header.transform.Find ("Text_Title").GetComponent<Lean.LeanLocalizedText>().PhraseName = HotSpotName;
					Image_Header.transform.Find ("Text_Title").GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
				}
			}
		}

		private void SetIcons_Name_Colors(){
//			float previousX = 0;
//			HospotData tempData = _canvasHotspots.GetHotspotData(ProjectID);
//			GameObject _tempObject;
//			GameObject _tempStroke;
//			Vector2 _tempVectSize;
//			for(int i=0; i < tempData.HotspotIconList.Count; i++){
//				
//				//////set icon and color
//				_tempObject = (GameObject)Instantiate(Image_Icon);
//				_tempObject.SetActive(true);
//				
//				//set icon
//				_tempObject.GetComponent<Image>().sprite = tempData.HotspotIconList[i];
//
//				//set color
//				_tempObject.GetComponent<Image>().color = tempData.HotspotIconColorList[i];
//				
//				//set parent
//				_tempObject.transform.SetParent(Image_Header.transform);
//				_tempObject.transform.SetAsLastSibling();
//				
//				//set position
//				_tempObject.transform.localScale = new Vector3(1f,1f,1f);
//				_tempObject.transform.localRotation = Quaternion.identity;
//				_tempVect2.y = _themeIconYPos;
//				_tempVect2.x = previousX;
//				_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
//				_tempVect3 = _tempObject.GetComponent<RectTransform>().anchoredPosition3D;
//				_tempVect3.x -= Icon_Xpos;
//				_tempVect3.z = 0;
//				_tempObject.GetComponent<RectTransform>().anchoredPosition3D = _tempVect3;
//				
//				previousX += _tempObject.GetComponent<RectTransform>().rect.width + _offset;
//				
//				//////set stroke and color
//				_tempStroke = (GameObject)Instantiate(Image_Stroke);
//				_tempStroke.SetActive(true);
//				
//				//set color
//				_tempStroke.GetComponent<Image>().color = tempData.HotspotIconColorList[i];
//				
//				//set parent
//				_tempStroke.transform.SetParent(Empty_Center.transform);
//				_tempStroke.transform.SetAsLastSibling();
//				
//				//set position
//				_tempStroke.transform.localScale = new Vector3(1f,1f,1f);
//				_tempStroke.transform.localRotation = Quaternion.identity;
//				// +5 width, +5 height
//				_tempVectSize.y = _tempStroke.GetComponent<RectTransform>().sizeDelta.y + (i * 5f);
//				_tempVectSize.x = _tempStroke.GetComponent<RectTransform>().sizeDelta.x + (i * 5f);
//				//Debug.Log("_tempVectSize = "+_tempVectSize);
//				_tempStroke.GetComponent<RectTransform>().sizeDelta = _tempVectSize;
//				
//				_tempStroke.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
//
//				if(UseProjectName){
//					HotSpotName = HotSpotName.Replace("NEWLINE", "\n");
//					Image_Header.transform.Find ("Text_Title").GetComponent<Text> ().text = tempData.ProjectName;
//					if(Image_Header != null && Image_Header.transform.Find ("Text_Title") != null &&
//					   Image_Header.transform.Find ("Text_Title").GetComponent<Lean.LeanLocalizedText>() != null){
//						Image_Header.transform.Find ("Text_Title").GetComponent<Lean.LeanLocalizedText>().PhraseName = tempData.ProjectName;
//						Image_Header.transform.Find ("Text_Title").GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
//					}
//				}
//			}
		}
	}
}
