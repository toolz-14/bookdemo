﻿using UnityEngine;
using System.Collections;


namespace Toolz.Transitions
{
	[AddComponentMenu("Toolz/Transitions/LoadLevelAsync")]
	public class LoadLevelAsync : MonoBehaviour {
		
		public string SceneName;
		private AsyncOperation _async;
	
		
		private void OnEnable() {
			StartCoroutine("load");
		}
		
		IEnumerator load() {
			yield return new WaitForEndOfFrame();
			_async = Application.LoadLevelAsync(SceneName);
			_async.allowSceneActivation = false;
			yield return _async;
		}
		
		public void ActivateScene() {

			if(_async.progress >= 0.9f){
				_async.allowSceneActivation = true;
			}
		}
	}
}