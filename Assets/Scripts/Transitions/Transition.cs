﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolz.Transitions {
    [AddComponentMenu("Toolz/Transitions/Transition")]

    public class Transition : MonoBehaviour {
        //Path to transitions's prefab in 'Resources' folder
        public static readonly string TRANSITIONS_PATH = "SetTransitions";

		[HideInInspector]
        public float ModuleToActivationTime;
      
		[HideInInspector]
		public float ModuleFromDeactivationTime;

        //The module we are transitioning from
        public Module From {get; private set;}
        //The module we are transitioning to
        public Module To {get; private set;}

        private List<TransitionBrick> _bricks;
        private float _totalDuration;
		private TransitionBrick _brick;

		private void Awake() {
            _bricks = new List<TransitionBrick>();
            _totalDuration = 0f;
            foreach (Transform child in transform) {
				_brick = child.GetComponent<TransitionBrick>();
				if (_brick == null) {
                    Debug.LogError("A Transition cannot have a child which is not a TransitionBrick.");
                    continue;
                }
				_bricks.Add(_brick);   
				_brick.gameObject.SetActive(false);
            }
        }

		private void Start() {

        }

        // PUBLIC METHODS

		public void DoTransition(Module from, Module to, float startingTime, float transitionDuration) {
            From = from;
            To = to;
            foreach (TransitionBrick brick in _bricks) {
                brick.gameObject.SetActive(true);
				brick.StartingTime = startingTime;
				brick.TransitionDuration = transitionDuration;
				float totalDuration = startingTime + transitionDuration;
				if (totalDuration > _totalDuration) {
					_totalDuration = totalDuration;
				}
            }
            
            BroadcastMessage("ActivateTransition");
            Invoke("TransitionFinished", _totalDuration);

			if(ModuleToActivationTime < 0f) {
                Debug.LogError("ModuleToActivationTime is negative.");
                ModuleToActivationTime = 0f;
            }

            Invoke("ActivateModuleTo", ModuleToActivationTime);
            Invoke("DeactivateModuleFrom", ModuleFromDeactivationTime);
        }

        // PRIVATE METHODS

        private void TransitionFinished() {

            foreach (TransitionBrick brick in _bricks) {
                brick.gameObject.SetActive(false);
            }

            Invoke("DestroyMyself", 0.5f);
        }

        private void ActivateModuleTo() {
            To.Activate();
        }

        private void DeactivateModuleFrom() {
            From.Deactivate();
        }

        private void DestroyMyself() {
            Destroy(gameObject);
        }
    }
}