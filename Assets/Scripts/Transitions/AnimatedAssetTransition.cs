﻿using UnityEngine;
using System.Collections;


namespace Toolz.Transitions {
	[AddComponentMenu("Toolz/Transitions/AnimatedAssetTransition")]

	public class AnimatedAssetTransition : TransitionBrick {

		/*
		 * to be visible during the transition, the asset HAS to be attached to the transition camera ????
		 * - if camera moves => can be added as child of a camera transition 
		 * 
		 * - if static camera eg UIcam => other issue: the Ps won't render in front of ui unless the canvas render mode is on camera
		 *   which is screwed since we got 2 cameras (from mod, to mod)
		 * => also needs to be positionned in front of the active camera, also screwed same reason
		 *  idem particule system
		 */


		//HAVE to get this as a public variable since the object is disabled and won't find it otherwise
		[Tooltip("The animated asset used for the transition")]
		public Animator animatedObject;

		public static readonly string PLAY_ANIMATION = "isPlaying";

		private bool _play {
			get { return animatedObject.GetBool(PLAY_ANIMATION); }
			set { animatedObject.SetBool(PLAY_ANIMATION, value); }
		}

		
		private void Awake() {
			
		}
		
		private void Start() {
			
		}
		
		// PUBLIC METHODS
		
		public override void ActivateTransition() {
			_play = true;
			Invoke("StartTransition", StartingTime);
		}
		
		// PRIVATE METHODS
		
		private void StartTransition(){

			
			Invoke("StopAnimating", TransitionDuration-0.1f);
		}
		
		private void StopAnimating(){
			_play = false;
		}
	}
}
