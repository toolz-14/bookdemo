﻿using UnityEngine;
using System.Collections;

namespace Toolz.Transitions {
	[AddComponentMenu("Toolz/Transitions/TransitionBrick")]

	public abstract class TransitionBrick : MonoBehaviour {

	    [HideInInspector]
	    public float StartingTime;
	    [HideInInspector]
	    public float TransitionDuration;

	    public abstract void ActivateTransition();
	}
}