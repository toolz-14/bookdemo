﻿using UnityEngine;
using System.Collections;

namespace Toolz.Transitions {
	[AddComponentMenu("Toolz/Transitions/WireCameraTransitionTranslate")]

	public class WireCameraTransitionTranslate : TransitionBrick {

        private Transform _transitionCamera;

		private bool _move;
		private Transform _from;
		private float _fromFOV;
		private float _fromFarClipping;
		private float _fromNearClipping;
		private Transform _to;
		private float _toFOV;
		private float _toFarClipping;
		private float _toNearClipping;
		private float _timeSinceTransitionStarted;
		private float _t;

		private Vector3 _fromPos;


		private void Awake() {
			_move = false;
			if(transform.parent != null && transform.parent.GetComponent<Transition>() != null &&
				transform.parent.GetComponent<Transition>().From != null &&
				transform.parent.GetComponent<Transition>().From.ModuleCameraHolder != null){
          	  _from = transform.parent.GetComponent<Transition>().From.ModuleCameraHolder.transform;
			}
			if (transform.parent != null && transform.parent.GetComponent<Transition> () != null &&
			   transform.parent.GetComponent<Transition> ().To != null &&
				transform.parent.GetComponent<Transition> ().To.ModuleCameraHolder != null) {
				_to = transform.parent.GetComponent<Transition> ().To.ModuleCameraHolder.transform;
			}
			_timeSinceTransitionStarted = 0f;
		}

		private void Start() {

		}

		private void Update() {
			if (_move){
				LerpCamera();
			}
		}


		// PUBLIC METHODS

		public override void ActivateTransition() {
			if (_from != null && _to != null) {
				_timeSinceTransitionStarted = 0f;
				if (_transitionCamera == null) {
					_transitionCamera = GetComponentInChildren<Camera> ().transform;
				}
				_fromPos = _from.position;
				//set camera start position
				_transitionCamera.position = _fromPos;
				_transitionCamera.rotation = _from.rotation;

				_fromFOV = _from.Find ("Camera").GetComponent<Camera> ().fieldOfView;
				_toFOV = _to.Find ("Camera").GetComponent<Camera> ().fieldOfView;

				_fromFarClipping = _from.Find ("Camera").GetComponent<Camera> ().farClipPlane;
				_toFarClipping = _to.Find ("Camera").GetComponent<Camera> ().farClipPlane;
				_transitionCamera.GetComponent<Camera> ().farClipPlane = _fromFarClipping;

				_fromNearClipping = _from.Find ("Camera").GetComponent<Camera> ().nearClipPlane;
				_toNearClipping = _to.Find ("Camera").GetComponent<Camera> ().nearClipPlane;
				_transitionCamera.GetComponent<Camera> ().nearClipPlane = _fromNearClipping;

				Invoke ("StartTransition", StartingTime);
			}
		}
		
		// PRIVATE METHODS

		private void LerpCamera(){
            /*
             * 0 => TransitionDuration / 0 => 1
             * _ratio = currentTime since start/ TransitionDuration
             * */
			if (_from != null && _to != null) {
				_timeSinceTransitionStarted += Time.deltaTime;
				_t = _timeSinceTransitionStarted / TransitionDuration;
				_transitionCamera.position = Vector3.Lerp (_fromPos, _to.position, _t);
				_transitionCamera.rotation = Quaternion.Lerp (_from.rotation, _to.rotation, _t);
				_transitionCamera.GetComponent<Camera> ().fieldOfView = Mathf.Lerp (_fromFOV, _toFOV, _t);
				_transitionCamera.GetComponent<Camera> ().farClipPlane = Mathf.Lerp (_fromFarClipping, _toFarClipping, _t);
				_transitionCamera.GetComponent<Camera> ().nearClipPlane = Mathf.Lerp (_fromNearClipping, _toNearClipping, _t);
				if (Vector3.Distance (_transitionCamera.position, _to.position) < 0.01f) {
					_move = false;
				}
			}
		}

		private void StartTransition(){
			_move = true;
		}

	}
}
