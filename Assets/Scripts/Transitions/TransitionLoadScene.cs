﻿using UnityEngine;
using System.Collections;


namespace Toolz.Transitions
{
	[AddComponentMenu("Toolz/Transitions/TransitionLoadScene")]
	public class TransitionLoadScene : MonoBehaviour {

		public string SceneName;
		public float Delay = 0f;

		public void LoadScene() {
			Invoke("LoadSceneAfterDelay", Delay);
		}

		private void LoadSceneAfterDelay(){
			if(SceneName != ""){
				Application.LoadLevel(SceneName);
			}else{
				Debug.LogWarning("need a scene name");
			}
		}

		private void Start(){

		}
	}
}
