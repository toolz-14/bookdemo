﻿using UnityEngine;
using System.Collections;


namespace Toolz.Transitions {
	[AddComponentMenu("Toolz/Transitions/AssetTransitionPS")]
	public class AssetTransitionPS : TransitionBrick {

		//The particle system used for the transition
		private ParticleSystem _assetParticles = null;

		private void Awake() {

		}
		
		private void Start() {
			
		}

		// PUBLIC METHODS
		
		public override void ActivateTransition() {
            if (_assetParticles == null) {
                _assetParticles = transform.GetComponentInChildren<ParticleSystem>();
            }
			
			Invoke("StartTransition", StartingTime);
		}
		
		// PRIVATE METHODS

		private void StartTransition(){
			_assetParticles.Play();

			Invoke("StopEmitting", TransitionDuration);
		}

		private void StopEmitting(){
			_assetParticles.Stop();
		}
	}

}
