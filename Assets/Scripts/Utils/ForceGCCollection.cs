﻿using UnityEngine;
using System.Collections;

public class ForceGCCollection : MonoBehaviour {
	
	void Start () {
		InvokeRepeating ("Collect", 0, 0.5f);
	}
	

	void Collect () {
		System.GC.Collect ();
	}
}
