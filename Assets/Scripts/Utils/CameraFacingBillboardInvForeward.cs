﻿using UnityEngine;
using System.Collections;


namespace Toolz.Utils
{
	[AddComponentMenu("Toolz/Utils/CameraFacingBillboardInvForeward")]

	public class CameraFacingBillboardInvForeward : MonoBehaviour {
		
		void Update () {
			transform.LookAt(transform.position + Toolz.Managers.LevelManager.ActiveCamera.transform.rotation * (Vector3.forward * -1f),
			                 Toolz.Managers.LevelManager.ActiveCamera.transform.rotation * Vector3.up);
		}
	}
}
