﻿using UnityEngine;
using System.Collections;

namespace Toolz.Utils {
    [AddComponentMenu("Toolz/Utils/MoveCameraForward")]
    public class MoveCameraForward : MonoBehaviour {

        public Vector3 EndPoint;
        public float Speed;
        public bool ActivateOnEnable;

        public SpriteRenderer BigMap;
        public SpriteRenderer HighDefMap;

        private bool _isActivated = false;
		private float _remainingDistance;

		private void Start() {
            
        }

		private void OnEnable() {
            if (ActivateOnEnable) {
                Activate();
            }
        }

		private void OnDisable() {
            _isActivated = false;
        }

		private void Update() {
            if (_isActivated) {
				_remainingDistance = Vector3.Distance(transform.position, EndPoint);
				if (_remainingDistance < 0.01f) {
                    _isActivated = false;
                    StartCoroutine("SwapSprites");
                }
                else {
                    transform.position += transform.forward * Speed * Time.deltaTime;
                }
            }
        }

        // PUBLIC

        public void Activate() {
            _isActivated = true;
        }

        private IEnumerator SwapSprites() {
            yield return new WaitForSeconds(0.01f);
            Color tmp = BigMap.color;
            tmp.a -= 0.1f;
            BigMap.color = tmp;
            tmp = HighDefMap.color;
            tmp.a += 0.1f;
            HighDefMap.color = tmp;
            if (HighDefMap.color.a <= 1.0f) {
                StartCoroutine("SwapSprites");
            }
        }
    }
}