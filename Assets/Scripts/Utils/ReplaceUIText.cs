﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Toolz.Utils
{
	[AddComponentMenu("Toolz/Utils/ReplaceUIText")]

	public class ReplaceUIText : MonoBehaviour {

		//private string _previousText = "";

		public void ReplaceText(string TextToUse){
			GetComponent<Text>().text = TextToUse;
		}

		public void AddToText(string TextToAdd){
//			if(_previousText == ""){
//				_previousText = TextToAdd;
//			}else{
//				if(_previousText != TextToAdd){
					GetComponent<Text>().text += TextToAdd;
					GetComponent<Text>().text += "\n";
//					_previousText = TextToAdd;
//				}
//			}

		}
	}
}
