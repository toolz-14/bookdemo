﻿using UnityEngine;
using System.Collections;

namespace Toolz.Util {
    [AddComponentMenu("Toolz/Utils/ActivateModuleInitial")]
    public class ActivateModuleInitial : MonoBehaviour {

        public Module ModuleInitial;
		
        private void Start() {
            ModuleInitial.gameObject.SetActive(false);
        }

        public void ActivateModuleInital() {
            if (ModuleInitial != null) {
                ModuleInitial.gameObject.SetActive(true);
            }
        }
    }
}