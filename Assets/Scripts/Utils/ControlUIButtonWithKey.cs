﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
 * add this to a ui button and fill the keyName to control the ui button with that key.
 */

namespace Toolz.Utils
{
	[AddComponentMenu("Toolz/Utils/ControlUIButtonWithKey")]

	public class ControlUIButtonWithKey : MonoBehaviour {

        //public bool useRTCinput;
        [SerializeField]
        private string keyName;
		private Button _button;

		private void Awake () {
			_button = GetComponent<Button>();
		}
		
		private void Update() {
            if (CustomInputModule.useRTCinputStatic)
            {
                if (RTCInput.GetKeyDown(keyName))
                {
                    _button.onClick.Invoke();
                    QuitWithEsc.delay = true;
                    Invoke("Delay", 1.4f);
                }
            }
            else
            {
                if (Input.GetKeyDown(keyName) && gameObject.activeInHierarchy)
                {
                    _button.onClick.Invoke();
                    QuitWithEsc.delay = true;
                    Invoke("Delay", 1.4f);
                }
            }
		}

        private void Delay()
        {
            QuitWithEsc.delay = false;
        }
	}
}
