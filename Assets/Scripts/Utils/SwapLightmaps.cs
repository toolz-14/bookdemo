﻿using UnityEngine;
using System.Collections;

/**
 * WIP Class hardcoded
 **/
namespace Toolz.Utils {
	[AddComponentMenu("Toolz/Utils/SwapLightmaps")]

	public class SwapLightmaps : MonoBehaviour {

		public enum LMType{full, construction}
		public LMType ChosenLightmap = LMType.full;
		public bool Delay = false;
		public float DelayLength = 1f;
		private int _newArraySize = 36;
        //Dirty trick to avoid multiple loading of lightmap data
        private static LightmapData[] _lightmapFull;
        private static LightmapData[] _lightmapConstruction;
        private static bool _lightMapLoaded;

		private static readonly string _path1 = "LightMaps/StadeFFR/LightmapFar-";
		//private string _path11 = "LightMaps/StadeFFR/LightmapScale-";
		private static readonly string _path2 = "LightMaps/StadeFFR_construction/LightmapFar-";
		//private string _path22 = "LightMaps/StadeFFR_construction/LightmapScale-";

		private void Awake() {
            if (!_lightMapLoaded) {
                //Create Lightmap containers
                _lightmapFull = new LightmapData[_newArraySize];
                _lightmapConstruction = new LightmapData[_newArraySize];

                for (int i = 0; i < _newArraySize; ++i) {
                    _lightmapFull[i] = new LightmapData();
                    _lightmapConstruction[i] = new LightmapData();
                }

                //Load Lightmap data
                for (int i = 0; i < _newArraySize; ++i) {
                    _lightmapFull[i].lightmapColor = Resources.Load(_path1 + i.ToString(), typeof(Texture2D)) as Texture2D;
                    _lightmapConstruction[i].lightmapColor = Resources.Load(_path2 + i.ToString(), typeof(Texture2D)) as Texture2D;
                    //lightmapData[i].lightmapNear = Resources.Load( nearPath + i.ToString(), typeof(Texture2D)) as Texture2D;
                }
                LightmapSettings.lightmaps = _lightmapFull;
                _lightMapLoaded = true;
            }
        }
		
		private void Start () {
		}

		public void SwapTheLightmaps(){
			if(Delay){
				Invoke("Swap",DelayLength);
			}else{
				Swap();
			}
		}

        private void Swap() {
            switch (ChosenLightmap) {
                case LMType.full: 
                    LightmapSettings.lightmaps = _lightmapFull;
                    break;
                case LMType.construction:
                    LightmapSettings.lightmaps = _lightmapConstruction;
                    break;
            }
        }
	}
}
