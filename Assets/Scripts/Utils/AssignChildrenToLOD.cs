﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignChildrenToLOD : MonoBehaviour {

	[SerializeField]
	private float LOD_0 = 0.1f;
	[SerializeField]
	private float LOD_1 = 0.1f;
	[SerializeField]
	private float LOD_2 = 0.1f;

	private List<Transform> FirstParent = new List<Transform>();

	private void Start () {
		
	}

	public void GetChildrenAndAssign(){
		int i;
		FirstParent.Clear ();
		foreach(Transform trans in transform){
			LODGroup group;
			if (trans.gameObject.GetComponent<LODGroup> () == null) {
				group = trans.gameObject.AddComponent<LODGroup> ();
			} else {
				group = trans.GetComponent<LODGroup> ();
			}
			FirstParent.Add (trans);
			i = 0;
			LOD[] lods = new LOD[3];
			Renderer[] temp = new Renderer[1];
			foreach(Transform tr in trans){
				temp [0] = tr.GetComponent<MeshRenderer> ();
				if (i == 0) {
					lods [i] = new LOD (0.9f , temp);
				}else if(i == 1){
					lods [i] = new LOD (0.5f, temp);
				}else if(i == 2){
					lods [i] = new LOD (0.2f, temp);
				}
			}
			Debug.Log ("SettingLod, LOD COUNT = "+ group.lodCount);

			group.SetLODs(lods);
			group.RecalculateBounds();
		}
	}

	public void AssignPercent(){
		foreach(Transform trans in FirstParent){
			LOD[] lods = GetComponent<LODGroup> ().GetLODs ();
			lods[0].screenRelativeTransitionHeight = LOD_0;
			lods[1].screenRelativeTransitionHeight = LOD_1;
			lods[2].screenRelativeTransitionHeight = LOD_2;
		}
	}
}
