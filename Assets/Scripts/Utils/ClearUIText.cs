﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace Toolz.Utils {
	[AddComponentMenu("Toolz/Utils/ClearUIText")]

	public class ClearUIText : MonoBehaviour {

		public Text mytext;

		public void ClearText(){
			mytext.text = "";
		}
	}
}