﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Toolz.Utils {
	[AddComponentMenu("Toolz/Utils/RotateObject")]

	public class RotateObjectAndStop : MonoBehaviour {

		public float RotationSpeed = 1f;
		public bool IsUIElement = false;
		public enum ChosenAxis{x, y, z};
		public ChosenAxis MyAxis = ChosenAxis.z;
        [SerializeField]
        private Image img;

        private Vector3 _objectEulerAngle = Vector3.zero;

		private void Update () {    
            if(img.sprite == null)
            {
                if (MyAxis == ChosenAxis.y)
                {
                    _objectEulerAngle.y = Time.deltaTime * RotationSpeed;
                }
                else if (MyAxis == ChosenAxis.x)
                {
                    _objectEulerAngle.x = Time.deltaTime * RotationSpeed;
                }
                else if (MyAxis == ChosenAxis.z)
                {
                    _objectEulerAngle.z = Time.deltaTime * RotationSpeed;
                }
                if (!IsUIElement)
                {
                    transform.Rotate(_objectEulerAngle);
                }
                else
                {
                    GetComponent<RectTransform>().Rotate(_objectEulerAngle);
                }
            }
		}
	}
}