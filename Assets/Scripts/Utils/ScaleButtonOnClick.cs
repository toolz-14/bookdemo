﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class ScaleButtonOnClick : MonoBehaviour, IPointerDownHandler {

	public float newScale = 1.2f;
	private RectTransform _button;

	private void Start () {
		_button = GetComponent<RectTransform> ();
	}

	private void Update(){

	}

	public void OnPointerDown (PointerEventData eventData) {
		_button.localScale = new Vector3 (newScale, newScale, newScale);
	}
}
