﻿using UnityEngine;
using System.Collections;


namespace Toolz.Utils
{
	[AddComponentMenu("Toolz/Utils/CameraFacingBillboard")]

	public class CameraFacingBillboard : MonoBehaviour {

		public Transform myCamera = null;

        private void Update () {
			if (myCamera == null) {
				transform.LookAt (transform.position + Toolz.Managers.LevelManager.ActiveCamera.transform.rotation * Vector3.forward,
				                  Toolz.Managers.LevelManager.ActiveCamera.transform.rotation * Vector3.up);
			}
			else {
				transform.LookAt (transform.position + myCamera.rotation * Vector3.forward,	myCamera.rotation * Vector3.up);
			}
		}
	}
}
