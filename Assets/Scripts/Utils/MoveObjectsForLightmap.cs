﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolz.Utils  {
	
	[AddComponentMenu("Toolz/Utils/MoveObjectsForLightmap")]

	[ExecuteInEditMode]
	public class MoveObjectsForLightmap : MonoBehaviour {

		public float DistanceFirstObject = 1000f;
		public float DistanceBetweenOtherObjects = 500f;

		public List<Transform> ObjectList;

		private List<Vector3> _resetValues;

		private void Awake () {
			_resetValues = new List<Vector3>();
		}

		public void MoveAllObjects(){

			SaveOriginalPositions();
			ObjectList[0].position = new Vector3(DistanceFirstObject, ObjectList[0].position.y, ObjectList[0].position.z);
			for(int i=1; i< ObjectList.Count; i++){
				ObjectList[i].position = new Vector3(DistanceFirstObject + (DistanceBetweenOtherObjects * i), ObjectList[0].position.y, ObjectList[0].position.z);
			}
		}

		public void ResetAllObjects(){
			for(int i=0; i < _resetValues.Count; i++){
				ObjectList[i].position = _resetValues[i];
			}
		}

		private void SaveOriginalPositions(){
			_resetValues.Clear();
			foreach(Transform t in ObjectList){
				_resetValues.Add(t.position);
			}
		}

	}
}
