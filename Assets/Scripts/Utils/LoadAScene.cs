﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class LoadAScene : MonoBehaviour {

    public string sceneName = "";
    public bool loadAsynced;

    private AsyncOperation _async;
    	
	public void LoadTheScene(){
        if (!loadAsynced)
        {
            if (sceneName != "")
            {
                SceneManager.LoadScene(sceneName);
            }
            else
            {
                Debug.Log("The Scene Name is Empty!");
            }
        }
        else
        {
            LauchCoroutineLoad();
        }
	}

    public void LauchCoroutineLoad()
    {
        if (sceneName != "")
        {
            StartCoroutine("Load");
        }
        else
        {
            Debug.Log("The Scene Name is Empty!");
        }
    }

    IEnumerator Load()
    {
        yield return new WaitForEndOfFrame();

        _async = SceneManager.LoadSceneAsync(sceneName);
        _async.allowSceneActivation = false;

        yield return _async;
    }

    private void Update()
    {
        if (_async != null && _async.progress >= 0.9f)
        { //&& bundleIsLoaded
            _async.allowSceneActivation = true;
        }
    }
}
