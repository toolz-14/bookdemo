﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class MoveSliderByKeys : MonoBehaviour {

    [SerializeField]
    private KeyCode keyNamePlus;
    [SerializeField]
    private KeyCode keyNameMinus;
    [SerializeField]
    private float moveValue = 0.01f;

    private Slider slider;
    private bool startUpdate;

    private void Start () {
        slider = GetComponent<Slider>();
    }

    private void OnEnable()
    {
        startUpdate = true;
    }

    private void OnDisable()
    {
        startUpdate = false;
    }

    private void Update()
    {
        if (startUpdate)
        {
            if (Input.GetKey(keyNamePlus))
            {
                if (slider.value < slider.maxValue)
                {
                    slider.value += moveValue;
                }
            }

            if (Input.GetKey(keyNameMinus))
            {
                if (slider.value > slider.minValue)
                {
                    slider.value -= moveValue;
                }
            }
        }
    }
}
