﻿using UnityEngine;
using System.Collections;

public class QuitWithEsc : MonoBehaviour {

    [SerializeField]
    private GameObject Fps;
    public static bool delay;

    private void Start()
    {
        InvokeRepeating("MyUpdate", 0f, 0.05f);
    }

    private void MyUpdate()
    {
        if (!Fps.activeInHierarchy && !delay && Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
}
