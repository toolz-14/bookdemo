﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace Toolz.Bricks {
    [AddComponentMenu("Toolz/Bricks/AnimationController")]
    public class AnimationController : MonoBehaviour {

        public static readonly string PLAY_ANIMATION = "PlayAnimation";

        public bool PlayOnActivation;
        private Animator _animator;

        private bool _play {
            get { return _animator.GetBool(PLAY_ANIMATION); }
            set { _animator.SetBool(PLAY_ANIMATION, value); }
        }

        private void Awake() {
            _animator = GetComponent<Animator>();
        }

        public void OnEnable() {
            if (PlayOnActivation) {
                PlayAnimation();
            }
        }

        public void PlayAnimation() {
            Debug.Log("AnimationController::PlayAnimation()");
            _play = !_play;
        }

        public void StopAnimation() {
            _play = false;
        }

    }
}