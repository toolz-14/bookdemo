﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace Toolz.Bricks.View
{
	[AddComponentMenu("Toolz/Bricks/View/ZoomOnMouseandTouch")]

	public class ZoomOnMouseandTouch : BaseCamera {
		public float minFov = 15f;
		public float maxFov = 90f;
		public float sensitivity = 10f;
		public float touchSensitivity = 10f;
		private float _fov;
		private Touch touch;
		
		protected override void Update () {
			base.Update();
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			if(Input.GetAxis("Mouse ScrollWheel") != 0f){
				_fov = _cameraHolder.Find("Camera").GetComponent<Camera>().fieldOfView;
				_fov -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
				_fov = Mathf.Clamp(_fov, minFov, maxFov);
				_cameraHolder.Find("Camera").GetComponent<Camera>().fieldOfView = _fov;
			}
		}

		public override void Zoom(ZoomGesture g, float delta)
		{
			if (enabled) {
				//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
				if (EventSystem.current != null) {
					if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
						return;
					}
				}
				_fov = _cameraHolder.Find("Camera").GetComponent<Camera>().fieldOfView;
				_fov -= delta * touchSensitivity;
				_fov = Mathf.Clamp(_fov, minFov, maxFov);
				_cameraHolder.Find("Camera").GetComponent<Camera>().fieldOfView = _fov;
			}
		}
	}
}
