using UnityEngine;
using System.Collections;

public class PlaneCamera : BaseCamera
{
    public GameObject plane;
    public float xRatio = 0;
    public float zRatio = 0;

    private float minRX = 40;
    private float maxRX = 90;
    private float upZoom = 250;

    private float xMoveRatio = 0;
    private float zMoveRatio = 0;
    private Vector3 moveMagnitude;
    private Vector3 zoomMagnitude;
    //private Vector3 rotateMagnitude;

    //private Vector3 _position;
    private Vector3 _angles;
    //private Quaternion _rotation;

    private Vector3 _center;
    private Vector3 _minPos;
    private Vector3 _maxPos;

    //------------------| Awake / Start |

    protected override void Start()
    {
        base.Start();
        //Debug.Log("new plane camera " + plane);
        _position = Vector3.zero;
        _angles = new Vector3(80, 0, 0);
        transform.eulerAngles = _angles;
        //_rotation = new Quaternion();

        _center = Vector3.zero;
        _minPos = Vector3.zero;
        _maxPos = Vector3.zero;

        moveMagnitude = Vector3.zero;
        zoomMagnitude = Vector3.zero;
        //rotateMagnitude = Vector3.zero;

        UpdateConstrains();
        if (plane != null) plane.SetActive(false);
    }

    //------------------| Constrains |

    protected void UpdateConstrains()
    {
        Transform t = plane.transform;
        _center = _minPos = _maxPos = t.position;

        moveMagnitude.x = t.localScale.x / 2;
        moveMagnitude.z = t.localScale.y / 2;

        _minPos.x = _center.x - t.localScale.x / 2;
        _minPos.z = _center.z - t.localScale.y / 2;
        _maxPos.x = _center.x + t.localScale.x / 2;
        _maxPos.z = _center.z + t.localScale.y / 2;

        //Debug.Log("min pos = " + _minPos + ", max pos = " + _maxPos);

        xMoveRatio = moveSpeed / moveMagnitude.x * 2;
        zMoveRatio = moveSpeed / moveMagnitude.z * 2;

        UpdatePosition();
    }

    //------------------| Activate / Unactivate |

    protected override void OnEnable()
    {
        base.OnEnable();
       // if (cam == null) return;
        // update x and y ratio
		xRatio = ((_cameraHolder.position.x - _minPos.x) / (_maxPos.x - _minPos.x)) * 2 - 1;
		zRatio = ((_cameraHolder.position.z - _minPos.z) / (_maxPos.z - _minPos.z)) * 2 - 1;
        //_angles.x = transform.eulerAngles.x;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
//        UpdateControllerPosition();
    }


    //------------------| Update |

    protected void UpdatePosition()
    {
        _position = _center;
        _position.x += moveMagnitude.x * xRatio;
        _position.z += moveMagnitude.z * zRatio;
    }

    protected override void Update()
    {
        base.Update();
        UpdatePosition();
        _position.y += zoomMagnitude.y;

		_cameraHolder.position = Vector3.Lerp(_cameraHolder.position, _position, moveSpeed * dt);
		_cameraHolder.rotation = Quaternion.Lerp(_cameraHolder.rotation, Quaternion.Euler(_angles), rotateSpeed * dt);
    }


    //------------------| Gestures |

    public override void Drag(DragGesture g, Vector2 magnitude)
    {
		if (enabled) {
			magnitude *= .1f;
			xRatio -= magnitude.x * xMoveRatio * moveSens;
			zRatio -= magnitude.y * zMoveRatio * moveSens;
			if (xRatio < -1)
				xRatio = -1;
			else if (xRatio > 1)
				xRatio = 1;
			if (zRatio < -1)
				zRatio = -1;
			else if (zRatio > 1)
				zRatio = 1;
		}
    }

    public override void Pan(PanGesture g, Vector2 panning)
    {
		if (enabled) {
			_angles.x += panning.y * .1f;
			if (_angles.x < minRX)
				_angles.x = minRX;
			else if (_angles.x > maxRX)
				_angles.x = maxRX;
		}
    }

    public override void Zoom(ZoomGesture g, float delta)
    {
		if (enabled) {
			zoomMagnitude.y -= delta * zoomSpeed;
			if (zoomMagnitude.y > upZoom)
				zoomMagnitude.y = upZoom;
			else if (zoomMagnitude.y < -upZoom)
				zoomMagnitude.y = -upZoom;

			if (zoomMagnitude.y < 0) {
				moveSens = .2f + (1 + zoomMagnitude.y / upZoom) * .8f;
			} else {
				moveSens = 1;
			}
		}
    }


    //------------------| Interaction |
    /*
    public override void move(float x, float y)
    {
        xRatio -= x * xMoveRatio*moveSens;
        zRatio -= y * zMoveRatio*moveSens;
        if (xRatio < -1) xRatio = -1;
        else if (xRatio > 1) xRatio = 1;
        if (zRatio < -1) zRatio = -1;
        else if (zRatio > 1) zRatio = 1;
    }*/
    /*
    public override void rotate(float r)
    {
        _angles.y += r * rotateSpeed;
    }*/
    /*
    public override void zoom(float z)
    {
        zoomMagnitude.y -= z*zoomSpeed;
        if (zoomMagnitude.y > upZoom) zoomMagnitude.y = upZoom;
        else if (zoomMagnitude.y < -upZoom) zoomMagnitude.y = -upZoom;

        if (zoomMagnitude.y < 0)
        {
            moveSens = .2f + (1 + zoomMagnitude.y / upZoom)*.8f;
        }
        else
        {
            moveSens = 1;
        }
    }*/
}
