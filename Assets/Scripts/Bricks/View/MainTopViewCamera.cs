using UnityEngine;
using System.Collections;
using System.Xml;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class MainTopViewCamera : BaseCamera
{
	private float Speed = 0.5f;//0.08f;
//	private float _maxSpeed = 2f;
	private float SpeedZoom = 0.4f;
//	private float zoomRatio = -0.05f;

	protected Vector3 _drag;
    private float _speedMult = 1f;

	private Vector3 _tempVect3;

	private bool _updatePosition = true;
	[HideInInspector]
	public bool IsMovingByGesture = true;

	#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
	#endif
	public static bool ForwardCollision;
	public static bool BlockCameraMovement;
	private bool JustExitedBlock;
//	private float _startSpeed;


    //------------------| Awake |
 
    protected override void Awake()
    {
		base.Awake();
		JustExitedBlock = false;
		BlockCameraMovement = false;
        _drag = Vector3.zero;

		_position = _cameraHolder.position;
		_rotation = _cameraHolder.rotation.eulerAngles;

		_cameraHolder.position = _position;
		_cameraHolder.rotation = Quaternion.Euler(_rotation);
//		_startSpeed = Speed;
    }

	protected override void Start () 
	{
		base.Start();
		InvokeRepeating ("MyUpdate", 0f, 0.01f);
	}

    //------------------| Activate / Deactivate |

	public new void OnEnable()
    {
		
		if (_cameraHolder == null) {
			return;
		}
		if (_cameraHolder != null) {
			AddGestures ();
		}
		_updatePosition = true;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
		_updatePosition = false;
    }

    //------------------| Update |

	 void MyUpdate() //
    {
//        base.Update();
		dt = Time.deltaTime;
        dt *= _speedMult;
		if (BlockCameraMovement) {
			JustExitedBlock = true;
			return;
		} else {
			if(JustExitedBlock){
				JustExitedBlock = false;
				_position = _cameraHolder.position;
				_rotation = _cameraHolder.rotation.eulerAngles;

				_cameraHolder.position = _position;
				_cameraHolder.rotation = Quaternion.Euler(_rotation);
			}
		}
		if (EventSystem.current != null) {
			if (IsPointerOverUIObject () == 5) {
				JustExitedBlock = true;
				return;
			}else {
				if(JustExitedBlock){
					JustExitedBlock = false;
					_position = _cameraHolder.position;
					_rotation = _cameraHolder.rotation.eulerAngles;

					_cameraHolder.position = _position;
					_cameraHolder.rotation = Quaternion.Euler(_rotation);
				}
			}
		}
		if (IsMovingByGesture) {
			//Debug.Log ("before lerping");

			_tempVect3 = Vector3.Lerp (_cameraHolder.position, _position, moveSpeed * dt);

			if (!AuthorizeMove (_tempVect3, false)) {
				_updatePosition = false;
				_position = _cameraHolder.position;
			} else {
				if (Physics.Linecast (_cameraHolder.position, _tempVect3) ) {
					_updatePosition = false;
					_position = _cameraHolder.position;
				}else{
					AuthorizeMove (_tempVect3, true);
					_updatePosition = true;
				}
			}

			moveSens = .2f * Speed;
			zoomSens = .5f * SpeedZoom;
			rotateSens = .4f;

			_cameraHolder.rotation = Quaternion.Lerp (_cameraHolder.rotation, Quaternion.Euler (_rotation), rotateSpeed * dt);

		}
    }

    //------------------| Special |

    public override void MoveTo(Transform t) {
		MoveTo(t.position, t.eulerAngles);

        if (!_enabled) {
			_cameraHolder.position = _position;
			_cameraHolder.eulerAngles = _rotation;
        }

    }

    public override void MoveTo(Vector3 position, Vector3 eulerAngles)  {
        _position.x = position.x;
        _position.z = position.z;
		_position.y = position.y;

        _rotation = eulerAngles;
    }

    //------------------| Gestures |

    public override void Drag(DragGesture g, Vector2 magnitude) {
		if (_updatePosition) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			_speedMult = 1f;

        
			_drag.x = -magnitude.x * moveSens;
			_drag.z = -magnitude.y * moveSens;

			float angle = Mathf.Atan2 (_drag.z, _drag.x) - _cameraHolder.eulerAngles.y * Mathf.Deg2Rad;
			float move = _drag.magnitude;

			//Debug.Log("FreeTerrainCamera DRAG......" + magnitude);
			_position.x += Mathf.Cos (angle) * move;
			_position.z += Mathf.Sin (angle) * move;
		}
    }

    public override void Zoom(ZoomGesture g, float delta) {
		if (enabled) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}

			_speedMult = 1f;
			_position.y -= delta * zoomSpeed * zoomSens;

//			//TODO Change this to use distance ground/cam!
//			Speed -= delta * zoomSpeed * zoomSens;
//			if(Speed < _startSpeed){
//				Speed = _startSpeed;
//			}
//			if(Speed >= _maxSpeed){
//				Speed = _maxSpeed;
//			}
		}
    }

    public override void Rotate(RotateGesture g, float angle)
    {
		if (_updatePosition) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			//Debug.Log("FreeTerrainCamera Rotate......" + angle);
			_speedMult = 1f;
			_rotation.y += angle * 4 * rotateSens;
		}
    }
}
