using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class OrbitCam : Toolz.Bricks.View.LookAtCamera
{
    private Quaternion _orbitRotation;
    private Quaternion _nullRotation;
//    private Vector3 _finalRotation;
	
    [Header ("Rotation x")]
    public float rxVarMin = 20;
    public float rxVarMax = 5;
//    private float _rxMax;
//    private float _rxMin;
    private float _rxStart;
	public float speed = 0.1f;
	#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
	#endif

    //------------------| Start |

    protected override void Start()
    {
        base.Start();
		_orbitRotation = new Quaternion(0, 0, 0, 1);
        _nullRotation = new Quaternion(0, 0, 0, 1);
//        _finalRotation = new Vector3(0, 0, 0);

    }

    protected override void OnEnable()
    {
        base.OnEnable();
//        _rxStart = transform.localEulerAngles.x;
//        //Debug.Log("rxStart = " + _rxStart);
//        _rxMin = _rxStart - rxVarMin;
//        _rxMax = _rxStart + rxVarMax;
		_cameraHolder.LookAt(LookAtTarget);
    }

	protected override void OnDisable()
	{
		base.OnDisable ();
	}

    //------------------| Update |

	protected override void Update()
    {
        base.Update();

        if (EventSystem.current != null)
        {
            if (IsPointerOverUIObject() == 5)
            {
                return;
            }
        }

        _cameraHolder.RotateAround(LookAtTarget.position, Vector3.up, _orbitRotation.y);

//		_finalRotation = _cameraHolder.localEulerAngles;
//        _finalRotation.x += _rotation.x;
//
//        // TODO : Smooth rotation x
//        if (_finalRotation.x < _rxMin)
//        {
//            _finalRotation.x = _rxMin;
//        }
//        else if(_finalRotation.x > _rxMax)
//        {
//            _finalRotation.x = _rxMax;
//        }
//		_cameraHolder.localEulerAngles = _finalRotation;
		_cameraHolder.LookAt(LookAtTarget);

		_orbitRotation = Quaternion.Lerp(_orbitRotation, _nullRotation, Time.deltaTime * rotateSpeed);

    }


    //------------------| Interaction |

    public override void Drag(DragGesture g, Vector2 magnitude)
    {
        //if (enabled) {
        if (EventSystem.current != null)
        {
            if (IsPointerOverUIObject() == 5)
            {
                return;
            }
        }
        _orbitRotation.y += magnitude.x * .35f;
		//}
    }

	public override void SetCamera(){
		TheLookAtCamera = transform.Find("ConstraintBox").Find("CameraHolder");
	}
}
