﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanoramicView : BaseCamera
{

    //public Text debugtext;
    //public Text debugtext2;
    //public Text debugtext3;
    //public Text debugtext4;

    private float speed = 8f;
    private Quaternion _orbitRotation;
    private Quaternion _nullRotation;

    protected override void Awake()
    {
        Resources.UnloadUnusedAssets();
        base.Awake();
    }

    protected override void Start()
    {
        _position = _cameraHolder.position;
        _rotation = _cameraHolder.rotation.eulerAngles;
        _cameraHolder.position = _position;
        _cameraHolder.rotation = Quaternion.Euler(_rotation);

        _orbitRotation = new Quaternion(0, 0, 0, 1);
        _nullRotation = new Quaternion(0, 0, 0, 1);

        base.Start();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (_cameraHolder == null)
        {
            return;
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    protected override void Update()
    {
        base.Update();

        _cameraHolder.RotateAround(_cameraHolder.position, _cameraHolder.right, _orbitRotation.x);
        _cameraHolder.RotateAround(_cameraHolder.position, Vector3.up, _orbitRotation.y);

        _orbitRotation = Quaternion.Lerp(_orbitRotation, _nullRotation, speed * dt);

        Vector3 temp = _cameraHolder.localEulerAngles;

        if (temp.x > 50f && temp.x < 260f)
        {
            temp.x = 50f;
        }
        if (temp.x < 300f && temp.x > 260f)
        {
            temp.x = 300f;
        }
        temp.z = 0;
        _cameraHolder.localEulerAngles = temp;
    }

    public override void Drag(DragGesture g, Vector2 magnitude)
    {
        if (EventSystem.current != null)
        {
            if (IsPointerOverUIObject() == 5)
            {
                return;
            }
        }

        _orbitRotation.y += -magnitude.x * .015f;
        _orbitRotation.x += magnitude.y * .015f;

    }
}
