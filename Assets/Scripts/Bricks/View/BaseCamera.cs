using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class BaseCamera : MonoBehaviour, IDragGesture, IPanGesture, IRotateGesture, IZoomGesture 
{
    public bool _disableOnStart = false;

    protected Gestures _gestures;
    protected bool _enabled;

    // gestures
    protected DragGesture _dragGesture;
    protected PanGesture _panGesture;
    protected ZoomGesture _zoomGesture;
    protected RotateGesture _rotateGesture;

    // transform to manipulate
    protected Transform _cameraHolder;

	private BoxCollider _boundaries;

    // sensitivity
	protected float moveSens = 1;
    protected float rotateSens = 1;
    protected float zoomSens = 1;

    // easing (made with Lerp)
	protected float moveSpeed = 10f;
	protected float rotateSpeed = 10f;
	protected float zoomSpeed = 10f;

    // helpers
    protected float dt;

    // vecteurs wich are being fed to the camera holder transform
    protected Vector3 _position;
    protected Vector3 _rotation;

    [SerializeField]
    //protected bool useRTCinput;


    //------------------| Awake / Start |

    protected virtual void Awake()
	{
		_boundaries = transform.Find("ConstraintBox").GetComponent<BoxCollider>();
		_cameraHolder = _boundaries.transform.Find("CameraHolder");
	}

	protected virtual void Start () 
    {
        _gestures = Gestures.instance;

        _enabled = true;
        if (_disableOnStart)
        {
            enabled = false;
        }
    }

    //------------------| Active / Unactive |

    protected virtual void OnEnable()
    {
        if (_cameraHolder != null)
        {
            AddGestures();
        }
    }

    protected virtual void OnDisable()
    {
        if (_cameraHolder != null)
        {
            RemoveGestures();
        }
    }

	protected int IsPointerOverUIObject()
	{
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        if (CustomInputModule.useRTCinputStatic)
        {
            eventDataCurrentPosition.position = new Vector2(RTCInput.mousePosition.x, RTCInput.mousePosition.y);
        }
        else
        {
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
            

		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		if (results.Count > 0) {
			return results [0].gameObject.layer;
		} else {
			return -1;
		}
	}

    protected int IsPointerOverUIObjectTouch(Canvas canvas, Vector2 screenPosition)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);

        eventDataCurrentPosition.position = screenPosition;
        GraphicRaycaster uiRaycaster = canvas.gameObject.GetComponent<GraphicRaycaster>();

        List<RaycastResult> results = new List<RaycastResult>();
        uiRaycaster.Raycast(eventDataCurrentPosition, results);

        if (results.Count > 0)
        {
            return results[0].gameObject.layer;
        }
        else
        {
            return -1;
        }
    }

    //------------------| Gestures |

    protected virtual void AddGestures()
    {
        _dragGesture = new DragGesture(this, "world");
        _panGesture = new PanGesture(this, "world");
        _rotateGesture = new RotateGesture(this, "world");
        _zoomGesture = new ZoomGesture(this, "world");
    }

    protected virtual void RemoveGestures()
    {
        if (_dragGesture == null) return;
        _dragGesture.Dispose();
        _dragGesture = null;
        _panGesture.Dispose();
        _panGesture = null;
        _rotateGesture.Dispose();
        _rotateGesture = null;
        _zoomGesture.Dispose();
        _zoomGesture = null;
    }

    public virtual void Drag(DragGesture g, Vector2 magnitude)
    {
        // to override
    }

    public virtual void Pan(PanGesture g, Vector2 panning)
    {
        // to override
    }

    public virtual void Rotate(RotateGesture g, float angle)
    {
        // to override
    }

    public virtual void Zoom(ZoomGesture g, float delta)
    {
        // to override
    }

    //------------------| Update |

    protected virtual void Update()
    {

        dt = Time.deltaTime;
    }

    //------------------| Special |

	protected bool IsOutOfBounds(Vector3 nextPosition)
	{
		return !_boundaries.bounds.Contains(nextPosition);
	}

	/**
     * @brief Called by children classes in order to actually move.
     * Check if the destination is still in the constraint box.
     * If not, do not move.
     * @param position new position wanted by the camera
     **/
	protected bool AuthorizeMove(Vector3 position, bool doMove)
	{
		if (!IsOutOfBounds(position) ){
			if(doMove){
				_cameraHolder.position = position;
			}
			return true;
		}
		else{
			return false;
		}
	}

    public virtual void MoveTo(Transform t)
    {
        // to override
    }

    public virtual void MoveTo(Vector3 position, Vector3 eulerAngles)
    {
        // to override
    }

    public void SetPositionRotation(Vector3 pos, Vector3 rot)
    {
        _position = pos;
        _rotation = rot;
    }
}
