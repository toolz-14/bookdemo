using UnityEngine;
using System.Collections;

public class OrbitCamWithElevation : BaseCamera
{
    public Transform lookAtTarget;

    public float maxElevation = 5;
    public float minElevation = 1;
    public float elevationSpeed = .01f;
    public bool autoRX = false;
    public bool invertStartAngle = false;
    public bool enableRXPanning = false;
    public float minRX = -20;
    public float maxRX = 20;
    public bool enableZooming = true;
    public float zoomIn = .5f;
    public float zoomOut = .1f;

    private float _zoom = 1;

    private float _orbit = 0; // angle
    private float _radius = 0;
    private float _camRadius = 0;
    private float _elevation = 0;
    private float _rx = 0;

    private Vector3 _angles = Vector3.zero;

    private Vector3 _camAngles = Vector3.zero;

    private Vector3 _targetPosition = Vector3.zero;
    private Vector3 _camOffset = Vector3.zero;
    private Vector3 _camPos = Vector3.zero;

    private Vector3 _angleOffset = Vector3.zero;
    private bool _resetRXOffset;

    //------------------| Start  |

	protected override void Awake()
    {
        _targetPosition = lookAtTarget.position;
        _camOffset = transform.position - _targetPosition;

        minRX *= Mathf.Deg2Rad;
        maxRX *= Mathf.Deg2Rad;

        //Debug.Log("TARGET POSITION = " + _targetPosition + ", CAM OFFSET = " + _camOffset);

        _radius = _camRadius = Mathf.Sqrt(_camOffset.x * _camOffset.x + _camOffset.z * _camOffset.z);//Vector3.Distance(transform.position, _targetPosition);
        float orbitSin = _camOffset.x / _radius;

        _orbit = Mathf.Asin(orbitSin);
        if (invertStartAngle)
        {
            _orbit += Mathf.PI;
        }

        _angles = transform.eulerAngles * Mathf.Deg2Rad;
        _angles.y = _orbit;
        _elevation = _camOffset.y;

        if (_elevation < minElevation) _elevation = minElevation;
        else if (_elevation > maxElevation) _elevation = maxElevation;
        _camOffset.y = _elevation;

        if (autoRX)
        {
            _rx = Mathf.Atan(_camOffset.y / _radius);
            _angles.x = _rx;
        }


        _camOffset.x = Mathf.Sin(_angles.y) * _radius;
        _camOffset.z = Mathf.Cos(_angles.y) * _radius;
        _camPos = _targetPosition + _camOffset;
        transform.position = _camPos;

        _camAngles = _angles;
        _camAngles.y += Mathf.PI;
        _camAngles *= Mathf.Rad2Deg;
        transform.eulerAngles = _camAngles;
    }

    //------------------| Enable / Disable |

    protected override void OnDisable()
    {
		if (_cameraHolder != null)
        {
//            UpdateControllerPosition();
        }
        base.OnDisable();
    }


    protected override void Update()
    {
        base.Update();

        _radius += (_camRadius*_zoom - _radius)*(zoomSpeed * dt);
        _angles.y += (_orbit - _angles.y) * (rotateSpeed * dt);

        _camOffset.x = Mathf.Sin(_angles.y) * _radius;
        _camOffset.z = Mathf.Cos(_angles.y) * _radius;
        _camOffset.y += (_elevation - _camOffset.y) * (moveSpeed * dt);

        if (autoRX)
        {
            _rx = Mathf.Atan(_camOffset.y / _radius);
            _angles.x += (_rx - _angles.x) * (rotateSpeed * dt);
        }

        if (_resetRXOffset)
        {
            _angleOffset = Vector3.Lerp(_angleOffset, Vector3.zero, dt * rotateSpeed);
        }

        _camPos = _targetPosition + _camOffset;
		_cameraHolder.position = _camPos;


        _angles = Vector3.Lerp(_angles, _angles + _angleOffset, rotateSpeed * dt);
        _camAngles = _angles;
        _camAngles.y += Mathf.PI;
		_cameraHolder.eulerAngles = _camAngles*Mathf.Rad2Deg;
    }


    //------------------| Special |

    public override void MoveTo(Transform t)
    {
        _targetPosition = t.position;
    }

    //------------------| Interaction |

    public override void Drag(DragGesture g, Vector2 magnitude)
    {
        //Debug.Log("dragging = " + magnitude);
        _elevation -= magnitude.y * elevationSpeed;
        if (Mathf.Abs(magnitude.y) > 4)
        {
            _resetRXOffset = true;
        }
        else
        {
            _resetRXOffset = false;
        }
        //Debug.Log("elevation = " + _elevation);
        if (_elevation > maxElevation) _elevation = maxElevation;
        else if (_elevation < minElevation) _elevation = minElevation;
    }

    public override void Pan(PanGesture g, Vector2 panning)
    {
        _resetRXOffset = false;
        
        _orbit += panning.x *.5f * Mathf.Deg2Rad;

        if (!enableRXPanning) return;
        _angleOffset.x += panning.y *.5f * Mathf.Deg2Rad;
        if (_angleOffset.x < minRX) _angleOffset.x = minRX;
        else if (_angleOffset.x > maxRX) _angleOffset.x = maxRX;
    }

    public override void Rotate(RotateGesture g, float angle)
    {
        _orbit += angle * 3 * Mathf.Deg2Rad;
    }

    public override void Zoom(ZoomGesture g, float delta)
    {
        if(!enableZooming) return;
        _zoom -= delta * .05f;
        if (_zoom < 1 - zoomIn) _zoom = 1 - zoomIn;
        else if (_zoom > 1 + zoomOut) _zoom = 1 + zoomOut;
    }


//    public override Vector3 originalPosition
//    {
//        get
//        {
//            //Debug.Log("get original position...");
//            return _targetPosition + _camOffset;
//        }
//    }
}
