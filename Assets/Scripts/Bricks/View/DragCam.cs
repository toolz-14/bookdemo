﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class DragCam : BaseCamera {
	
	public enum MoveAxis{ZOnly, XOnly, Both}
	public MoveAxis chosenAxis;
	
	protected Vector3 _drag;

	private float _speedMult = 1f;
	private bool _updatePosition = true;
	private Vector3 _tempVect3;
	[HideInInspector]
	public bool IsMovingByGesture = true;
	#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
	#endif
	
	protected override void Awake(){
		base.Awake();
	}

	protected override void Start () {
		base.Start();
	}

	protected override void OnEnable() {
		base.OnEnable();
		if (_cameraHolder == null) {
			return;
		}
	}
	
	protected override void OnDisable() {
		base.OnDisable();
	}

	protected override void Update()
	{
		base.Update();
		dt *= _speedMult;
		//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
		if (EventSystem.current != null) {
			if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
				return;
			}
		}
		if (IsMovingByGesture) {
			_tempVect3 = Vector3.Lerp (_cameraHolder.position, _position, moveSpeed * dt);
		
			if (!AuthorizeMove (_tempVect3, false)) {
				_updatePosition = false;
				_position = _cameraHolder.position;
			} else {
				AuthorizeMove (_tempVect3, true);
				_updatePosition = true;
			}
		}
	}

	public override void Drag(DragGesture g, Vector2 magnitude)
	{
		if (_updatePosition) {
			//if the mouse is over a UI element that doesn't belong to layer 8, then don't move [layer 8 for hospots]
			if (EventSystem.current != null) {
				if (IsPointerOverUIObject () != 8 && IsPointerOverUIObject () != -1) {
					return;
				}
			}
			_speedMult = 1f;
		
		
			_drag.x = -magnitude.x * moveSens;
			_drag.z = -magnitude.y * moveSens;
		
			float angle = Mathf.Atan2 (_drag.z, _drag.x) - _cameraHolder.eulerAngles.y * Mathf.Deg2Rad;
			float move = _drag.magnitude;
		
			//Debug.Log("DragCam DRAG......" + magnitude);

			if(chosenAxis == MoveAxis.ZOnly || chosenAxis == MoveAxis.Both){
				_position.z += Mathf.Sin (angle) * move;
			}
			if(chosenAxis == MoveAxis.XOnly || chosenAxis == MoveAxis.Both){
				_position.x += Mathf.Cos (angle) * move;
			}

		
//			if (terrain != null) {
//				//Debug.Log("position = " + _position);
//				if (hasPositionConstrains) {
//					if (_position.x < minPosConstrain.x)
//						_position.x = minPosConstrain.x;
//					else if (_position.x > maxPosConstrain.x)
//						_position.x = maxPosConstrain.x;
//					if (_position.z < minPosConstrain.z)
//						_position.z = minPosConstrain.z;
//					else if (_position.z > maxPosConstrain.z)
//						_position.z = maxPosConstrain.z;
//				}
//				
//				if (_position.x < _min.x)
//					_position.x = _min.x;
//				else if (_position.x > _max.x)
//					_position.x = _max.x;
//				if (_position.z < _min.z)
//					_position.z = _min.z;
//				else if (_position.z > _max.z)
//					_position.z = _max.z;
//			}
		}
	}
}
