﻿using UnityEngine;
using System.Collections;


namespace Toolz.Bricks.View
{
	[AddComponentMenu("Toolz/Bricks/View/ZoomOnMouseScroll")]

	public class MoveOnMouseScroll : BaseCamera {

		public float MoveSpeed = 2f;
		public enum MoveDirection {forward, updown, leftright}
		public MoveDirection Direction = MoveDirection.updown;

		private Vector3 _newMovePosition = Vector3.zero;
		private Vector3 _movedirection = Vector3.zero;


	

		protected override void Update(){
			SetMoveDirection(Direction);
			if(Input.GetAxis("Mouse ScrollWheel") != 0f){
				_newMovePosition = _cameraHolder.position + (_movedirection * -Input.GetAxis("Mouse ScrollWheel") * MoveSpeed);
				AuthorizeMove(_newMovePosition, true);
			}
		}

		private void SetMoveDirection(MoveDirection direction){
			switch(direction){
			case MoveDirection.forward:_movedirection = -_cameraHolder.forward;
				break;
			case MoveDirection.leftright:_movedirection = -_cameraHolder.right;
				break;
			case MoveDirection.updown:_movedirection = -_cameraHolder.up;
				break;
			default: Debug.LogWarning("the zoom direction is not recognized");
				break;
			}
		}
	}
}
