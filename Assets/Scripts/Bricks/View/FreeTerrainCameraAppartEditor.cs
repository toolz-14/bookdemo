using UnityEngine;
using System.Collections;
using System.Xml;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class FreeTerrainCameraAppartEditor : BaseCamera
{
	public float SpeedZoom= 1f;
	public float SpeedMove= 1f;
	public bool isOrtographicCam = false;
	private float Speed = 1f;
	private float ZoomingSpeed = 1f;
	public bool fixedElevation = false;
	public float minElevation = 0;
	public float maxElevation = 500;
	public float elevationOffset = 10;
	public float minRX = 20;
	public float maxRX = 90;
	public MeshFilter terrain;
	public string heightMapID;
	public Vector2 heightMapOffset = new Vector2(0, 0);

	private Texture2D _heightMap;

	public bool hasPositionConstrains = false;
	public Vector3 minPosConstrain = Vector3.zero;
	public Vector3 maxPosConstrain = Vector3.zero;

	protected Vector3 _rxOffset = Vector3.zero;

	protected Vector3 _min;
	protected Vector3 _max;
	protected Vector3 _drag;

	protected Vector2 _textureSize;
	protected Vector3 _posRatio;
	protected float _elevation = 100;
	protected float _altitude = 0;
	protected float _minAltitude;
	protected float _altitudeRatio = 0;

	private float _speedMult = 1f;
	private bool _autoElevationRotation = true;

	private Vector3 _tempVect3;
	private Vector3 _newZoomPosition;

	private bool _updatePosition = true;
	[HideInInspector]
	public bool IsMovingByGesture = true;

	#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
	#endif
	public static bool ForwardCollision;

	private Vector3 MySavedPos;
	private Vector3 MySavedEuler;
	private bool PreventAllInput;
	private float myaltitude = 0;
	public static bool IsCameraDragged;
	private float Angle;


	//------------------| Awake |

	protected override void Awake()
	{
		base.Awake();
		_posRatio = Vector3.zero;
		_drag = Vector3.zero;
		_min = Vector3.zero;
		_max = Vector3.zero;
		PreventAllInput = false;
		IsCameraDragged = false;
	}

	protected override void Start () 
	{
		_position = _cameraHolder.position;
		_rotation = _cameraHolder.rotation.eulerAngles;
		_cameraHolder.position = _position;
		_cameraHolder.rotation = Quaternion.Euler(_rotation);
		base.Start();

	}

	protected override void OnEnable()
	{
		base.OnEnable();
		if (_cameraHolder == null) {
			return;
		}
		_updatePosition = true;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		_updatePosition = false;
	}

	protected override void Update()
	{
		base.Update();
		dt *= _speedMult;

		if (EventSystem.current != null) {
			if (IsPointerOverUIObject () == 5 ) {
				return;
			}
		}
		_cameraHolder.RotateAround(transform.Find("ConstraintBox").position, Vector3.up, Angle);

	}

	public override void MoveTo(Transform t){
		MoveTo(t.position, t.eulerAngles);
	}

	public override void MoveTo(Vector3 position, Vector3 eulerAngles){
		_position.x = position.x;
		_position.z = position.z;
		_position.y = position.y;

		_rotation = eulerAngles;

		_rxOffset = Vector3.zero;
	}

	public void PreventMouseTouchInput(bool inputBlocked){
		PreventAllInput = inputBlocked;
	}

	public void SaveCameraValuesBeforeMoveTo(){
		MySavedPos = _position;
		MySavedEuler = _rotation;
	}

	public void ResetCameraAfterMoveTo(){
		_position.y = MySavedPos.y;
		_rotation = MySavedEuler;
	}

	//------------------| Gestures |

	public override void Drag(DragGesture g, Vector2 magnitude)
	{

	}

	public void InverseMoveSens(){
		moveSens = moveSens * -1f;
	}

	public override void Zoom(ZoomGesture g, float delta)
	{
		if (!PreventAllInput) {
			if (enabled) {
				if (EventSystem.current != null) {
					if (IsPointerOverUIObject () == 5 ) {
						return;
					}
				}
				_speedMult = 1f;

				_position.y -= delta * zoomSpeed * zoomSens;
				_tempVect3 = Vector3.Lerp (_cameraHolder.position, _position, moveSpeed * dt);
				if (!AuthorizeMove (_tempVect3, false)) {
					_updatePosition = false;
					_position = _cameraHolder.position;
				} else {
					if (Physics.Linecast (_cameraHolder.position, _tempVect3) ) {
						_updatePosition = false;
						_position = _cameraHolder.position;
					}else{
						AuthorizeMove (_tempVect3, true);
						_updatePosition = true;
					}
				}
				_altitude = _cameraHolder.position.y;
				// terrain min altitude
				_altitude = _min.y + (_max.y - _min.y) * _altitude; 
				if (_elevation < _altitude)
					_elevation = _altitude;

				_altitudeRatio = ((_elevation - _altitude) / (maxElevation - _altitude));

				zoomSens = (.5f + .5f * _altitudeRatio) * SpeedZoom;
			}
		}
	}

	public override void Rotate(RotateGesture g, float angle)
	{
		if (!PreventAllInput) {
			if (_updatePosition) {
				if (EventSystem.current != null) {
					if (IsPointerOverUIObject () == 5 ) {
						return;
					}
				}
				_speedMult = 1f;
//				_rotation.y += angle * 4 * rotateSens;
				Angle = angle;
			}
		}
	}

	public override void Pan(PanGesture g, Vector2 panning)
	{

	} 

	private void ConstrainRotationX()
	{
		if (_rxOffset.x < -20){ _rxOffset.x = -20;}
		else if (_rxOffset.x > 20){ _rxOffset.x = 20;}
	}
}
