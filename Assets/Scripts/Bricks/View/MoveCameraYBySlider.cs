﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Toolz.Bricks.View
{
	[AddComponentMenu("Toolz/Bricks/View/MoveCameraYBySlider")]

	public class MoveCameraYBySlider : BaseCamera {

		public Slider TheSlider;
		public float Amplitude = 16f;
		public bool isLookatCam;
		public Transform LookAtTarget;
		public float Speed = 10f;

		private Vector3 _newPosition;
		private float _currentValue;
		private float _min = 0;
		private float _max = 0;

		protected override void Start () {
			_currentValue = TheSlider.value;
			_min = _cameraHolder.position.y - Amplitude;
			_max = _cameraHolder.position.y;
		}

		public void move(){
			_newPosition = _cameraHolder.position;
			_currentValue = TheSlider.value;

			_newPosition.y = Mathf.Lerp(_min, _max, (_currentValue)/(Speed));
			AuthorizeMove (_newPosition, true);
		}

		protected override void Update(){
			if(isLookatCam){
				_cameraHolder.LookAt(LookAtTarget);
			}
		}
	}
}
