﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ZoomForSmoothScript : MonoBehaviour, IZoomGesture  {

	public float minFov = 15f;
	public float maxFov = 90f;
	public float sensitivityFOv = 10f;
	public float touchSensitivityFOv = 10f;
	public float sensitivity = 20f;
	private float _fov;
	private Touch touch;
	protected Gestures _gestures;
	protected ZoomGesture _zoomGesture;
	protected float zoomSpeed = 10f;


	private  void Start(){
		_gestures = Gestures.instance;
	}

	private void OnEnable()	{
		AddGestures ();
	}

	private void OnDisable(){
		RemoveGestures();
	}

	private void Update () {

		if(Input.GetAxis("Mouse ScrollWheel") != 0f){
				_fov = GetComponent<Camera> ().fieldOfView;
				_fov -= Input.GetAxis ("Mouse ScrollWheel") * sensitivityFOv;
				_fov = Mathf.Clamp (_fov, minFov, maxFov);
				GetComponent<Camera> ().fieldOfView = _fov;
		}
	}

	public virtual void Zoom(ZoomGesture g, float delta) {

			_fov = GetComponent<Camera> ().fieldOfView;
			_fov -= delta * touchSensitivityFOv;
			_fov = Mathf.Clamp (_fov, minFov, maxFov);
			GetComponent<Camera> ().fieldOfView = _fov;
	}

	protected virtual void AddGestures(){
		_zoomGesture = new ZoomGesture(this, "world");
	}

	protected virtual void RemoveGestures() {
		_zoomGesture.Dispose();
		_zoomGesture = null;
	}
}
