﻿using UnityEngine;
using System.Collections;

namespace Toolz.Bricks.View  {

	[AddComponentMenu("Toolz/Bricks/View/LookAtCamera")]
	public abstract class LookAtCamera : BaseCamera {

		[Tooltip("The target this camera is looking at")]
		public Transform LookAtTarget;

		[HideInInspector]
		public Transform TheLookAtCamera;

		public abstract void SetCamera();

		public void LookAtTheTarget(){
			SetCamera();
			if(LookAtTarget != null){
				TheLookAtCamera.LookAt(LookAtTarget);
			}else{
				Debug.LogWarning("LookAtTarget is null");
			}
		}
	}
}