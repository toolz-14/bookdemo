using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class FreeTerrainCamera : BaseCamera
{
    //public Text debugtext;

    public float SpeedZoom= 1f;
	public float SpeedMove= 1f;
	public bool isOrtographicCam = false;

    protected Vector3 _min;
    protected Vector3 _max;
    protected Vector3 _drag;

    private float _speedMult = 1f;

	private Vector3 _tempVect3;
	private Vector3 _newZoomPosition;

	private bool _updatePosition = true;
	[HideInInspector]
	public bool IsMovingByGesture = true;

	#if UNITY_IOS || UNITY_ANDROID
	private Touch touch;
	#endif
	public static bool ForwardCollision;

	private Vector3 MySavedPos;
	private Vector3 MySavedEuler;
	public static bool PreventAllInput;
	public static bool BlockCameraMovement;
	public static bool ignoreUI;
	private bool JustExitedBlock;

	private float savedMoveSpeed = 10f;
	private float savedRotateSpeed = 10f;
	private float savedZoomSpeed = 10f;


	//------------------| Awake |

	protected override void Awake()
	{
		base.Awake();
		JustExitedBlock = false;
		BlockCameraMovement = false;
		ignoreUI = false;

        _drag = Vector3.zero;
        _min = Vector3.zero;
        _max = Vector3.zero;
        PreventAllInput = false;
	}

	protected override void Start () 
	{
		_position = _cameraHolder.position;
		_rotation = _cameraHolder.rotation.eulerAngles;
		_cameraHolder.position = _position;
		_cameraHolder.rotation = Quaternion.Euler(_rotation);
		base.Start();
    }

	protected override void OnEnable()
	{
		base.OnEnable();
        if (_cameraHolder == null) {
			return;
		}
		_updatePosition = true;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
        _updatePosition = false;
	}

    protected override void Update()
    {
        base.Update();
        dt *= _speedMult;
        if (BlockCameraMovement)
        {
            JustExitedBlock = true;
            return;
        }
        else
        {
            if (JustExitedBlock)
            {
                JustExitedBlock = false;
                _position = _cameraHolder.position;
                _rotation = _cameraHolder.rotation.eulerAngles;

                _cameraHolder.position = _position;
                _cameraHolder.rotation = Quaternion.Euler(_rotation);
            }
        }
        if (EventSystem.current != null && !ignoreUI)
        {
            if (IsPointerOverUIObject() == 5)
            {
                JustExitedBlock = true;
                return;
            }
            else
            {
                if (JustExitedBlock)
                {
                    JustExitedBlock = false;
                    _position = _cameraHolder.position;
                    _rotation = _cameraHolder.rotation.eulerAngles;

                    _cameraHolder.position = _position;
                    _cameraHolder.rotation = Quaternion.Euler(_rotation);
                }
            }
        }

        _tempVect3 = Vector3.Lerp(_cameraHolder.position, _position, moveSpeed * dt);
        if (!AuthorizeMove(_tempVect3, false))
        {
            _updatePosition = false;
            _position = _cameraHolder.position;
        }
        else
        {
            if (Physics.Linecast(_cameraHolder.position, _tempVect3))
            {
                _updatePosition = false;
                _position = _cameraHolder.position;
            }
            else
            {
                AuthorizeMove(_tempVect3, true);
                _updatePosition = true;
            }
        }

        moveSens = (.2f + .8f) * SpeedMove;
        zoomSens = (.5f + .5f) * SpeedZoom;
        rotateSens = .4f + .6f;

        _cameraHolder.rotation = Quaternion.Lerp(_cameraHolder.rotation, Quaternion.Euler(_rotation), rotateSpeed * dt);

    }

    public Transform GetCameraHolder()
    {
        return _cameraHolder;
    }

    public void ForceCameraPosition(Transform pos){
		if(_cameraHolder != null){
			PreventAllInput = true;
			BlockCameraMovement = true;

			_cameraHolder.position = pos.position;
			_cameraHolder.rotation = pos.rotation;

			_position = _cameraHolder.position;
			_rotation = _cameraHolder.rotation.eulerAngles;

			PreventAllInput = false;
			BlockCameraMovement = false;
		}
	}

    public void ForceCameraPosition(Vector3 position, Vector3 eulerAngles)
    {
        if (_cameraHolder != null)
        {
            PreventAllInput = true;
            BlockCameraMovement = true;

            _cameraHolder.position = position;
            _cameraHolder.eulerAngles = eulerAngles;

            _position = _cameraHolder.position;
            _rotation = _cameraHolder.rotation.eulerAngles;

            PreventAllInput = false;
            BlockCameraMovement = false;
        }
    }

    public override void MoveTo(Transform t){
		MoveTo(t.position, t.eulerAngles);

		Invoke ("SpeedValuesBack", 1f);
	}

	public void MoveToSpeed(Transform t, float speed){
		MoveTo(t.position, t.eulerAngles);

		savedMoveSpeed = moveSpeed;
		moveSpeed = speed;
		savedRotateSpeed = rotateSpeed;
		rotateSpeed = speed;
		savedZoomSpeed = zoomSpeed;
		zoomSpeed = speed;

		Invoke ("SpeedValuesBack", 1f);
	}

	private void SpeedValuesBack(){
		moveSpeed = savedMoveSpeed;
		rotateSpeed = savedRotateSpeed;
		zoomSpeed = savedZoomSpeed;
	}

	public override void MoveTo(Vector3 position, Vector3 eulerAngles){
		_position.x = position.x;
		_position.z = position.z;
		_position.y = position.y;

		_rotation = eulerAngles;
	}

	public void PreventMouseTouchInput(bool inputBlocked){
		PreventAllInput = inputBlocked;
	}

	public void SaveCameraValuesBeforeMoveTo(){
		MySavedPos = _position;
		MySavedEuler = _rotation;
	}

	public void ResetCameraAfterMoveTo(){
		_position.y = MySavedPos.y;
		_rotation = MySavedEuler;
	}

	public void InverseMoveSens(){
		moveSens = moveSens * -1f;
	}

	//------------------| Gestures |

	public override void Drag(DragGesture g, Vector2 magnitude)
	{
        if (!PreventAllInput)
        {
            if (_updatePosition)
            {
                if (EventSystem.current != null)
                {
                    if (IsPointerOverUIObject() == 5)
                    {
                        return;
                    }
                }
                _speedMult = 1f;

                _drag.z = -magnitude.y * moveSens;
                _drag.x = -magnitude.x * moveSens;


                float angle = Mathf.Atan2 (_drag.z, _drag.x) - _cameraHolder.eulerAngles.y * Mathf.Deg2Rad;
				float move = _drag.magnitude;

				_position.x += Mathf.Cos (angle) * move;
				_position.z += Mathf.Sin (angle) * move;

            }
        }
    }

	public override void Zoom(ZoomGesture g, float delta)
	{
		if (!PreventAllInput) {
			if (enabled) {
				if (EventSystem.current != null) {
                    if (IsPointerOverUIObject() == 5)
                    {
                        return;
                    }
                }
				_speedMult = 1f;

				if (!isOrtographicCam) {
					_position.y -= delta * zoomSpeed * zoomSens;
				} else {

					_cameraHolder.Find ("Camera").GetComponent<Camera> ().orthographicSize -= delta * zoomSpeed * zoomSens;
					if (_cameraHolder.Find ("Camera").GetComponent<Camera> ().orthographicSize <= 100f) {
						_cameraHolder.Find ("Camera").GetComponent<Camera> ().orthographicSize = 100f;
					}
				}
			}
		}
	}

	public override void Rotate(RotateGesture g, float angle)
	{
        if (!PreventAllInput)
        {
            if (_updatePosition)
            {
                if (EventSystem.current != null)
                {
                    if (IsPointerOverUIObject() == 5)
                    {
                        return;
                    }
                }
                _speedMult = 1f;
				_rotation.y += angle * 4 * rotateSens;
            }
        }
    }

	public override void Pan(PanGesture g, Vector2 panning)
	{
		if (!PreventAllInput) {
			if (_updatePosition) {
				if (EventSystem.current != null) {
                    if (IsPointerOverUIObject() == 5)
                    {
                        return;
                    }
                }
				if (_rotation.x < 92f && _rotation.x > -92f) {
					_speedMult = 1f;

					_rotation.x += panning.y * .8f * rotateSens;
				}
				else {
					if (_rotation.x > 92f) {
						Vector3 temp = _rotation;
						temp.x = 91f;
						_rotation = temp;
					}
					if (_rotation.x < -92f) {
						Vector3 temp = _rotation;
						temp.x = -91f;
						_rotation = temp;
					}
				}
			}
		}
	} 
}
