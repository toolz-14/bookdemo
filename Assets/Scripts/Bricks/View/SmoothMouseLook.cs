﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SmoothMouseLook : MonoBehaviour {

	public Canvas theMainCanvas;
	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;
	public float MouseSpeed = 2f;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;
//	#if UNITY_STANDALONE || UNITY_WEBGL
	float rotationX = 0F;
	float rotationY = 0F;
	public float frameCounter = 20f;
	private List<float> rotArrayX = new List<float>();
	float rotAverageX = 0F;	

	private List<float> rotArrayY = new List<float>();
	float rotAverageY = 0F;
	private Quaternion originalRotation;
//	#endif

	private Vector3 firstpoint;
	private Vector3 secondpoint;

	private void Awake(){

	}

	private void Start () {
		#if UNITY_STANDALONE || UNITY_WEBGL
		originalRotation = transform.localRotation;
		#endif
	}
		
	private void Update () {
		
	
		//if the mouse is over a UI element
		if (EventSystem.current != null) {
			if(IsPointerOverUIObject () == 5){
				return;
			}
		}
		if (Input.GetButton ("Fire1")) {
			if (axes == RotationAxes.MouseXAndY) {			
				rotAverageY = 0f;
				rotAverageX = 0f;

				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;
				rotationX += Input.GetAxis ("Mouse X") * sensitivityX;

				rotArrayY.Add (rotationY);
				rotArrayX.Add (rotationX);

				if (rotArrayY.Count >= frameCounter) {
					rotArrayY.RemoveAt (0);
				}
				if (rotArrayX.Count >= frameCounter) {
					rotArrayX.RemoveAt (0);
				}

				for (int j = 0; j < rotArrayY.Count; j++) {
					rotAverageY += rotArrayY [j];
				}
				for (int i = 0; i < rotArrayX.Count; i++) {
					rotAverageX += rotArrayX [i];
				}

				rotAverageY /= rotArrayY.Count;
				rotAverageX /= rotArrayX.Count;

				rotAverageY = ClampAngle (rotAverageY, minimumY, maximumY);
				rotAverageX = ClampAngle (rotAverageX, minimumX, maximumX);

				Quaternion yQuaternion = Quaternion.AngleAxis (rotAverageY, Vector3.left);
				Quaternion xQuaternion = Quaternion.AngleAxis (rotAverageX, Vector3.up);
				transform.localRotation = originalRotation * xQuaternion * yQuaternion;

			} else if (axes == RotationAxes.MouseX) {			
				rotAverageX = 0f;

				rotationX += Input.GetAxis ("Mouse X") * sensitivityX;

				rotArrayX.Add (rotationX);

				if (rotArrayX.Count >= frameCounter) {
					rotArrayX.RemoveAt (0);
				}
				for (int i = 0; i < rotArrayX.Count; i++) {
					rotAverageX += rotArrayX [i];
				}
				rotAverageX /= rotArrayX.Count;

				rotAverageX = ClampAngle (rotAverageX, minimumX, maximumX);

				Quaternion xQuaternion = Quaternion.AngleAxis (rotAverageX, Vector3.up);
				transform.localRotation = originalRotation * xQuaternion;
				Vector3 temp = transform.localEulerAngles;
				temp.z = 0;
				temp.x = 0;
				transform.localEulerAngles = temp;
			} else {			
				rotAverageY = 0f;

				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;

				rotArrayY.Add (rotationY);

				if (rotArrayY.Count >= frameCounter) {
					rotArrayY.RemoveAt (0);
				}
				for (int j = 0; j < rotArrayY.Count; j++) {
					rotAverageY += rotArrayY [j];
				}
				rotAverageY /= rotArrayY.Count;

				rotAverageY = ClampAngle (rotAverageY, minimumY, maximumY);

				Quaternion yQuaternion = Quaternion.AngleAxis (rotAverageY, Vector3.left);
				transform.localRotation = originalRotation * yQuaternion;
			}
		}

	}

	public static float ClampAngle (float angle, float min, float max)
	{
		angle = angle % 360;
		if ((angle >= -360F) && (angle <= 360F)) {
			if (angle < -360F) {
				angle += 360F;
			}
			if (angle > 360F) {
				angle -= 360F;
			}			
		}
		return Mathf.Clamp (angle, min, max);
	}

	protected int IsPointerOverUIObject(){
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);

		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		if (results.Count > 0) {
			return results [0].gameObject.layer;
		} else {
			return -1;
		}
	}

	private int IsPointerOverUIObjectTouch(Canvas canvas, Vector2 screenPosition){
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);

		eventDataCurrentPosition.position = screenPosition;
		GraphicRaycaster uiRaycaster = canvas.gameObject.GetComponent<GraphicRaycaster> ();

		List<RaycastResult> results = new List<RaycastResult>();
		uiRaycaster.Raycast (eventDataCurrentPosition, results);

		if (results.Count > 0) {
			return results [0].gameObject.layer;
		} else {
			return -1;
		}
	}
}
