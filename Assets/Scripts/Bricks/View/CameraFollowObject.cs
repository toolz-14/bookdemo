﻿using UnityEngine;
using System.Collections;

namespace Toolz.Bricks.View  {
	
	[AddComponentMenu("Toolz/Bricks/View/CameraFollowObject")]

	public class CameraFollowObject : BaseCamera {

		public Transform ObjectToFollow;
		private Vector3 _distance;
		private Vector3 _newPosition;

		protected override void Awake(){

			_distance = ObjectToFollow.position - _cameraHolder.position;
			_newPosition = _cameraHolder.position;
		}

		protected override void Update(){
			_newPosition = ObjectToFollow.position - _distance;
			AuthorizeMove(_newPosition, true);
		}
	}
}
