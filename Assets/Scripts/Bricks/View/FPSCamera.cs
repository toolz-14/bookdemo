﻿using UnityEngine;
using System.Collections;

namespace Toolz.Bricks.View  {
	
	[AddComponentMenu("Toolz/Bricks/View/FPSCamera")]

	public class FPSCamera : BaseCamera {

		protected override void Awake(){
		}
		 //just call base awake as camera doesn't move, fixed to player for now
	}
}
