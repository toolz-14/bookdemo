﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace Toolz.Bricks.UI {
    [AddComponentMenu("Toolz/Bricks/UI/DisplayText")]
    [RequireComponent(typeof(Text))]
    public class DisplayText : MonoBehaviour {
        public Transform TransformToDisplay;

        private Text _text;
        // Use this for initialization
		private void Start() {
            _text = GetComponent<Text>();
        }

        // Update is called once per frame
        private void LateUpdate() {
            _text.text = "Altitude: " + TransformToDisplay.position.y; 
        }
    }
}