﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Toolz.Bricks.UI
{
    [AddComponentMenu("Toolz/Bricks/UI/Slide")]
    public class Slide : MonoBehaviour
    {

        public SlideShow slideShow;

        [HideInInspector]
        public bool move = false;
        [HideInInspector]
        public Vector2 endPos = Vector2.zero;
        [HideInInspector]
        public Image image;

        private RectTransform rectTranform;

        public float MOVE_LENGTH = 1300f;

        public float LerpTime = 4f;
        float currentLerpTime;
        private bool isTeleportingToRight;
        private bool isTeleportingToLeft;


        private void Awake(){
            rectTranform = GetComponent<RectTransform>();
            image = GetComponent<Image>();
            isTeleportingToRight = false;
            isTeleportingToLeft = false;
            endPos = rectTranform.anchoredPosition;
        }

		private void Start(){

        }

		private void Update(){
            if (move)
            {
                MoveXTo();
            }
        }

        public void ForceToEndPos(){
            rectTranform.anchoredPosition = endPos;
        }

        public void SlideLeft(){
            //If image in center
            if (Mathf.Abs(rectTranform.anchoredPosition.x - 0) < 1.0f){
                endPos.x = -MOVE_LENGTH;
            }
            //Else if image right
            else if (Mathf.Abs(rectTranform.anchoredPosition.x - MOVE_LENGTH) < 1.0f){
                endPos.x = 0;
            }
            //Else if image left
            else if (Mathf.Abs(rectTranform.anchoredPosition.x - (-MOVE_LENGTH)) < 1.0f){
                isTeleportingToRight = true;
                endPos.x = MOVE_LENGTH;
            }
            move = true;
        }

        public void SlideRight(){
            //If image in center
            if (Mathf.Abs(rectTranform.anchoredPosition.x - 0) < 1.0f){
                endPos.x = MOVE_LENGTH;
            }
            //Else if image right
            else if (Mathf.Abs(rectTranform.anchoredPosition.x - MOVE_LENGTH) < 1.0f){
                isTeleportingToLeft = true;
                endPos.x = -MOVE_LENGTH;
            }
            //Else if image left
            else if (Mathf.Abs(rectTranform.anchoredPosition.x - (-MOVE_LENGTH)) < 1.0f){
                endPos.x = 0;
            }
            move = true;
        }

        private void MoveXTo() {
            if (isTeleportingToRight){
				move = false;
				isTeleportingToRight = false;
                rectTranform.anchoredPosition = endPos;
                currentLerpTime = 0f;
                //The number indicates what image index we want (see Slideshow.GetNextImage)
                image.sprite = slideShow.GetNextImage(1);
            }
            else if (isTeleportingToLeft){
				move = false;
                isTeleportingToLeft = false;
                rectTranform.anchoredPosition = endPos;
                currentLerpTime = 0f;
                //The number indicates what image index we want (see Slideshow.GetNextImage)
                image.sprite = slideShow.GetNextImage(-1);
            }
            else if (Mathf.Abs(rectTranform.anchoredPosition.x - endPos.x) >= 1.0f){

                //increment timer once per frame
                currentLerpTime += Time.deltaTime;
                if (currentLerpTime > LerpTime){
                    currentLerpTime = LerpTime;
                }

                float perc = currentLerpTime / LerpTime;
                rectTranform.anchoredPosition = Vector2.Lerp(rectTranform.anchoredPosition, endPos, perc);

            }
            else{
                move = false;
                currentLerpTime = 0f;
            }
        }

    }
}