﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine.EventSystems;

namespace Toolz.Bricks.UI
{
    [AddComponentMenu("Toolz/Bricks/UI/SlideShow")]
	public class SlideShow : MonoBehaviour, ISwipeGesture
	{
		protected Gestures _gestures;
		protected SwipeGesture _swipeGesture;
		
		private Sprite[] _slides;
		public string SpotName = "CoteVestiaires";
		private bool _isFirstEnable = true;
		
		public Slide Image1;
		public Slide Image2;
		public Slide Image3;

		private int _shownImageIndex = 0;

		private bool _nextIsDone;
		

		private void Start(){
			_gestures = Gestures.instance;
		}


		public void SwipeDrag(SwipeGesture g, Vector2 drag, float scope){
		}

		public void SwipeReset(SwipeGesture g, Vector2 axis){
		}

		public void SwipeValidate(SwipeGesture g, Vector2 direction, float tweenDuration){
			if (direction.x == -1) {
				Next();
			}
			if (direction.x == 1) {
				Previous();
			}
		}
		
		private void OnEnable() {
			_swipeGesture = new SwipeGesture(this, "world");

			if (_isFirstEnable) {
				_isFirstEnable = false;
			} else {
				LoadSprites();
				InitSlide();
			}
			
		}
		
		private void OnDisable() { 
			if (_swipeGesture == null) return;
			_swipeGesture.Dispose();
			_swipeGesture = null;
		}
		
		private void LoadSprites(){
			Resources.UnloadUnusedAssets ();
			_slides = Resources.LoadAll("Slideshow/"+SpotName, typeof(Sprite)).Cast<Sprite>().ToArray();
		}

        public void InitSlide(){
            Image1.gameObject.GetComponent<Image>().sprite = GetNextImage(0);
            Image2.gameObject.GetComponent<Image>().sprite = GetNextImage(+1);
            Image3.gameObject.GetComponent<Image>().sprite = GetNextImage(-1);
        }

        /**
         * @param direction: >0 == we want the next image to the right. <0 == we want the next image to the left
         */
        public Sprite GetNextImage(int direction = 0){
            int indexImage = _shownImageIndex;
            indexImage += direction;

			if (indexImage >= _slides.Length) {
                indexImage = 0;
            }
            else if (indexImage < 0) {
				indexImage = _slides.Length - 1;
            }

			Sprite nextImage = _slides[indexImage];
            return nextImage;
        }

        public void Next(){
            ++_shownImageIndex;
			if (_shownImageIndex >= _slides.Length)
                _shownImageIndex = 0;

            Image1.ForceToEndPos();
            Image2.ForceToEndPos();
            Image3.ForceToEndPos();

            Image1.SlideLeft();
            Image2.SlideLeft();
            Image3.SlideLeft();

        }

        public void Previous(){
            --_shownImageIndex;
            if (_shownImageIndex < 0)
				_shownImageIndex = _slides.Length - 1;

            Image1.ForceToEndPos();
            Image2.ForceToEndPos();
            Image3.ForceToEndPos();

            Image1.SlideRight();
            Image2.SlideRight();
            Image3.SlideRight();

        }
    }
}