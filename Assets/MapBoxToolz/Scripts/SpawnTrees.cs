﻿using UnityEngine;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration.Factories;
using Mapbox.Unity.Utilities;
using System.Collections.Generic;
using System.Xml;
using System;
using System.Collections;

// SpawnTrees must be called after AbstractMap in execution order
public class SpawnTrees : MonoBehaviour
{
    [SerializeField]
    private AbstractMap _map;

    [SerializeField]
    private GameObject _markerPrefab;
    [SerializeField]
    float _spawnScale = 100f;

    [SerializeField]
    private string serverURL = "http://www.overpass-api.de/api/";
    [SerializeField]
    private string serverURL2 = "http://www.overpass-api.de/api/";

    private Dictionary<ulong, OsmTreeNode> nodes;
    private List<Vector2d> _locations; // lat, lon
    private List<GameObject> _spawnedObjects;

    void Start()
    {
        _locations = new List<Vector2d>();
        _spawnedObjects = new List<GameObject>();
        nodes = new Dictionary<ulong, OsmTreeNode>();

        StartCoroutine(LoadWebData());
    }

    IEnumerator LoadWebData()
    {
        #region Set Bounds
        // Calculate a bounds half - The base zoom level * the current zoom level
        //float bounds = baseZoom * (int)mapZoom;

        float boundsOffset = 0.007f;// 0.008f;

        //Debug.Log(_map.CenterLatitudeLongitude.y + " : " + _map.CenterLatitudeLongitude.x);

        double longitudeMin = _map.CenterLatitudeLongitude.y - boundsOffset;
        double latitudeMin = _map.CenterLatitudeLongitude.x - boundsOffset;
        double longitudeMax = _map.CenterLatitudeLongitude.y + boundsOffset;
        double latitudeMax = _map.CenterLatitudeLongitude.x + boundsOffset;

        string urlParamters = "map?bbox="
                            + longitudeMin.ToString() + ","
                            + latitudeMin.ToString() + ","
                            + longitudeMax.ToString() + ","
                            + latitudeMax.ToString();
        #endregion

        //Load XML data from a URL
        string url = serverURL + urlParamters;

        //Debug.Log(url);
        WWW www = new WWW(url);

        //Load the data and yield (wait) till it's ready before we continue executing the rest of this method.
        yield return www;

        bool loadSuccessful = Handledata(www);

        if (!loadSuccessful)
        {
            Debug.LogError("Loading data failed, trying fallback url: " + serverURL2);

            //Load XML data from the fallback URL
            url = serverURL2 + urlParamters;

            www = new WWW(url);

            //Load the data and yield (wait) till it's ready before we continue executing the rest of this method.
            yield return www;

            loadSuccessful = Handledata(www);
        }

    }

    /// <summary>
    /// Handles loaded data from the web
    /// </summary>
    /// <param name="www">www object with web data</param>
    /// <returns></returns>
    private bool Handledata(WWW www)
    {
        bool loadSuccessful = true;

        // If we have don't an error
        if (www.error == null)
        {
            //Sucessfully loaded the XML
            //Debug.Log("Text found" + www.text);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(www.text);

            GetNodes(doc.GetElementsByTagName("node"));

            StartCoroutine(InstanciateTrees());
        }
        else // Otherwise something went wrong
        {
            // Mark this as unsuccessful
            loadSuccessful = false;

            //Failed to download data
            Debug.LogError("Failed to download data " + www.error);
        }

        return loadSuccessful;
    }

    // Get nodes that got tag natural in it
    private void GetNodes(XmlNodeList xmlNodeList)
    {
        foreach (XmlNode n in xmlNodeList)
        {
            if (CheckIsTree(n))
            {
                OsmTreeNode node = new OsmTreeNode(n);
                nodes[node.Id] = node;

                _locations.Add(new Vector2d(node.Latitude, node.Longitude));
            }
        }
    }

    // return true to nodes that got tags with key = natural et value = true;
    private bool CheckIsTree(XmlNode node)
    {
        bool result = false;
        // check if node "tag" exist
        //Debug.Log(node.OuterXml.ToString());
        if (node.SelectNodes("tag") != null)
        {
            foreach (XmlNode child in node.SelectNodes("tag"))
            {
                //Debug.Log(child.OuterXml.ToString());
                if (child.Attributes != null && child.Attributes["k"] != null && child.Attributes["v"] != null)
                {
                    string key = child.Attributes["k"].Value;
                    string value = child.Attributes["v"].Value;
                    //Debug.Log(key + " : " + value);
                    if (key.Equals("natural") && value.Equals("tree"))
                        result = true;
                }
            }
        }
        return result;
    }

    private IEnumerator InstanciateTrees()
    {
        for (int i = 0; i < _locations.Count; i++)
        {
            if (i % 20 == 0)
                yield return new WaitUntil(() => CreateObject(i));
            else
                CreateObject(i);
        }
    }

    private bool CreateObject(int i)
    {
        bool result = false;
        GameObject instance = Instantiate(_markerPrefab); // beter to have a pool ?
        instance.transform.localPosition = _map.GeoToWorldPosition(_locations[i], true);
        instance.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
        instance.transform.parent = this.transform;
        _spawnedObjects.Add(instance);
        result = true;

        return result;
    }

    // TO REDO
    private void Update()
    {
        int count = _spawnedObjects.Count;
        for (int i = 0; i < count; i++)
        {
            if (_map.AbsoluteZoom < 16)
            {
                _spawnedObjects[i].SetActive(false);
            }
            else
            {
                GameObject spawnedObject = _spawnedObjects[i];
                spawnedObject.SetActive(true);
                Vector2d location = _locations[i];
                spawnedObject.transform.localPosition = _map.GeoToWorldPosition(location, true);
                spawnedObject.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
            }
        }
    }
}
