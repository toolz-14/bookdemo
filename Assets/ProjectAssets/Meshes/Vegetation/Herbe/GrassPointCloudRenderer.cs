﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GrassPointCloudRenderer : MonoBehaviour {

	
	public MeshFilter MeshF;
	private Mesh grassMesh;
	//public Material grassMat;

	public Vector2 size;

	[Range(1, 60000)]
	public int grassnumber;
	public float startHeight = -1000f;
	private Vector3 lastposition;
	public int seed;

	public float grassOffset = 0f ;



	// Update is called once per frame
	void Update()
	{
		if (lastposition != this.transform.position)
		{

			Random.InitState(seed);
			List<Vector3> positions = new List<Vector3>(grassnumber);
			int[] indicies = new int[grassnumber];
			List<Color> colors = new List<Color>(grassnumber);

			for (int i = 0; i < grassnumber; i++)
			{
				Vector3 origin = transform.position;
				origin.y = startHeight;
				origin.x += size.x * Random.Range(-0.5f, 0.5f);
				origin.z += size.y * Random.Range(-0.5f, 0.5f);
				Ray ray = new Ray(origin, Vector3.down);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					origin = hit.point;
					origin.y += grassOffset;
					positions.Add(origin);
					indicies[i]=i;
					colors.Add(new Color(Random.Range(0, 1), Random.Range(0, 1), Random.Range(0, 1)));
				}
			}

			grassMesh = new Mesh();
			grassMesh.SetVertices(positions);
			grassMesh.SetIndices(indicies, MeshTopology.Points,0);

			grassMesh.SetColors(colors);
			MeshF.sharedMesh = grassMesh;

			lastposition = this.transform.position;
		}
		//Graphics.DrawMeshInstanced(grassMesh, 0, grassMat,	);
	}
}
