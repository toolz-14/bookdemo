//Based off of the tutorial
//https://catlikecoding.com/unity/tutorials/flow/texture-distortion/

Shader "Custom/DistortionFlow" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		[NoScaleOffset] _FlowMap ("Flow (RG,A noise)", 2D) = "black" {}
		[NoScaleOffset] _NormalMap ("Normals", 2D) = "bump" {}
		_Speed ("Speed", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex,_FlowMap,_NormalMap;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Speed;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			
	

			
			float2 flowVector =abs( tex2D(_FlowMap, IN.uv_MainTex).rg* 2 - 1);
			
			float2 uv = IN.uv_MainTex ;
			float noise = tex2D(_FlowMap, IN.uv_MainTex).a;
			float time =_Speed * _Time.y + noise;

			float phaseOffsetA = false ? 0.5 : 0;
			float phaseOffsetB = true ? 0.5 : 0;

			float progressA = frac(time + phaseOffsetA);
			float progressB = frac(time + phaseOffsetB);

			float3 uvwA;
			uvwA.xy = uv - flowVector * progressA;
			uvwA.z = 1 - abs(1 - 2 * progressA); //In z store the wave of on/off.
			fixed4 texA = tex2D(_MainTex, uvwA.xy) * uvwA.z;

			float3 uvwB;
			uvwB.xy = uv - flowVector * progressB;
			uvwB.z = 1 - abs(1 - 2 * progressB); //In z store the wave of on/off.
			fixed4 texB = tex2D(_MainTex, uvwB.xy) * uvwB.z;

			float3 normalA = UnpackNormal(tex2D(_NormalMap, uvwA.xy)) * uvwA.z;
			float3 normalB = UnpackNormal(tex2D(_NormalMap, uvwB.xy)) * uvwB.z;
			o.Normal = normalize(normalA + normalB)/100;

			fixed4 c =texA +texB* _Color;
			o.Albedo = saturate(c);

			o.Metallic = _Metallic;
			o.Smoothness = (1 -abs(c.rgb) - 0.25)/1.3;
			o.Alpha = c.a;
		}
		ENDCG
	}

	FallBack "Diffuse"
}