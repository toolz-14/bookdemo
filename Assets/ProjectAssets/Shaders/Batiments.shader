﻿Shader "Custom/Batiments" 
{
	Properties 
	{
		_ColorTintY ("Color Tint Y", Color) = (1,1,1,1)
		_ColorTintXZ ("Color Tint XZ", Color) = (1,1,1,1)
		_TriplanarBlendSharpness ("Blend Sharpness",float) = 1
		_Glossiness ("Smoothness", Range(0,1)) = 0.55
		_Metallic ("Metallic", Range(0,1)) = 0.1
		_YOFFSET  ("Y offset",float) = 0
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0



		float _TriplanarBlendSharpness;
		float _YOFFSET;
		half _Glossiness;
		half _Metallic;
		fixed4 _ColorTintY;
		fixed4 _ColorTintXZ;

		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			float4 color: Color; // Vertex color
		}; 

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			// Find our UVs for each axis based on world position of the fragment.
			half2 yUV = IN.worldPos.xz ;
			half2 xUV = IN.worldPos.zy ;
			half2 zUV = IN.worldPos.xy;
			// Now do texture samples from our diffuse map with each of the 3 UV set's we've just made.
			half3 yDiff = _ColorTintY;
			half3 xDiff = _ColorTintXZ;
			half3 zDiff = _ColorTintXZ;
			// Get the absolute value of the world normal.
			// Put the blend weights to the power of BlendSharpness, the higher the value, 
            // the sharper the transition between the planar maps will be.
			half3 blendWeights = pow (abs(IN.worldNormal), _TriplanarBlendSharpness);
			// Divide our blend mask by the sum of it's components, this will make x+y+z=1
			blendWeights = blendWeights / (blendWeights.x + blendWeights.y + blendWeights.z);
			// Finally, blend together all three samples based on the blend mask and multiply by vertex color.
			fixed3 c = xDiff * blendWeights.x + yDiff * blendWeights.y + zDiff * blendWeights.z;
			o.Albedo = IN.color.rgb * c;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = 1.0;
		} 
		ENDCG
	}
		FallBack "Diffuse"
}