﻿// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/SphereMap" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_LightIntensity("Light Intensity", Range(0,10)) = 1
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		Cull Front
		LOD 150
		//ZWrite Off
		//Ztest Always

		CGPROGRAM
#pragma surface surf Lambert noforwardadd

		sampler2D _MainTex;
		float _LightIntensity;

		struct Input {
			float2 uv_MainTex;
			float4 color : COLOR;
		};

		void vert(inout appdata_full v)
		{
			v.normal.xyz = v.normal * -1;
		}

		void surf(Input IN, inout SurfaceOutput o) {
			fixed3 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * _LightIntensity;
			o.Alpha = 1;
		}
		ENDCG
	}
}
