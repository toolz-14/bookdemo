﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Toolz.EditorScripts {
	
	[CustomEditor(typeof(SetDatabaseProjectList_Fiches), true)]
	public class ButtonSendIDsToDatabase_Fiches : Editor {

		private SetDatabaseProjectList_Fiches _evCtrl = null;
		
		void OnEnable(){
			_evCtrl = (SetDatabaseProjectList_Fiches)target;
		}
		
		public override void OnInspectorGUI() {
			
			DrawDefaultInspector ();
			
			if(GUILayout.Button("SendAllIDsToDatabase")){
				_evCtrl.SendProjectsIDToDatabase();
			}
			
		}
	}
}
