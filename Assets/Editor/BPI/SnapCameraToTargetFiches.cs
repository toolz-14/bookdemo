﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Toolz.EditorScripts {

	[CustomEditor(typeof(LookAtCamera_Fiches), true)]
	public class SnapCameraToTargetFiches : Editor {

		private LookAtCamera_Fiches _evCtrl = null;
		
		void OnEnable(){
			_evCtrl = (LookAtCamera_Fiches)target;
		}

		public override void OnInspectorGUI() {

			DrawDefaultInspector ();

			if(GUILayout.Button("SnapCameraToTarget")){
				_evCtrl.LookAtTheTarget();
			}

		}
	}
}
