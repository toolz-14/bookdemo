﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Toolz.EditorScripts {
	
	[CustomEditor(typeof(DropDownSelected_Fiches), true)]
	public class DropDownDefault_Fiches : Editor {

		private DropDownSelected_Fiches _evCtrl = null;
		
		void OnEnable(){
			_evCtrl = (DropDownSelected_Fiches)target;
		}
		
		public override void OnInspectorGUI() {
			
			DrawDefaultInspector ();
			
			if(GUILayout.Button("Update Default Values")){
				_evCtrl.SetDefaultLanguage();
				EditorUtility.SetDirty( target );
			}
		}
		
		public override bool RequiresConstantRepaint()
		{
			return true;
		}
	}
}
