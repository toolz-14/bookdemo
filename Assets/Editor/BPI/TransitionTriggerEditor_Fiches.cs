﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;

namespace Toolz.EditorScripts {
	[CustomEditor(typeof(TransitionTrigger_Fiches))]
	public class TransitionTriggerEditor_Fiches : Editor {

        private static bool IsValidName(string name) {
			string folderPath = Application.dataPath + "/Resources/" + Transition_Fiches.TRANSITIONS_PATH;
            return System.IO.Directory.GetFiles(folderPath).Count<string>(s => s.Contains(name)) > 1;
        }

		private TransitionTrigger_Fiches _trigger;
        private string _prefabToDrop = "Drop here";

        void OnEnable() {
			_trigger = target as TransitionTrigger_Fiches;
            if (_trigger != null) {
                _prefabToDrop = _trigger.TransitionName;
            }
        }

        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            EditorGUILayout.Space();
            
            Event evt = Event.current;

            Rect dropArea = GUILayoutUtility.GetRect(0.0f, 20.0f);
            EditorGUI.LabelField(dropArea, "Add Transition: ", _prefabToDrop, EditorStyles.textField);
            
            switch (evt.type) {
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (!dropArea.Contains(evt.mousePosition))
                        return;

                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (evt.type == EventType.DragPerform) {
                        DragAndDrop.AcceptDrag();

                        foreach (Object draggedObject in DragAndDrop.objectReferences) {
                            if (IsValidName(draggedObject.name)) {
                                _trigger.TransitionName = draggedObject.name;
                                _prefabToDrop = draggedObject.name;
                            }
                            else {
                                Debug.LogError("The object you are adding is not a valid transition. Check if it exists in the Resources/SetTransitions's folder.");
                            }
                        }
                    }
                    break;
            }
        }
    }
}
