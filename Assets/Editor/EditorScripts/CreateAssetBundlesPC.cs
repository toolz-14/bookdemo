﻿using UnityEditor;
using System.IO;

public class CreateAssetBundlesPC
{
	[MenuItem("Assets/Build AssetBundlesPC")]
	static void BuildAllAssetBundles()
	{
		string assetBundleDirectory = "Assets/BagneuxBundlesPC";
		if(!Directory.Exists(assetBundleDirectory))
		{
			Directory.CreateDirectory(assetBundleDirectory);
		}
		BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows64);
	}
}
