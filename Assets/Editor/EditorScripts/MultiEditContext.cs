﻿using System.Collections;
using UnityEngine;
using UnityEditor;
using Toolz.Triggers;

[CustomEditor(typeof(TransitionTrigger), true)]
[CanEditMultipleObjects]
public class MultiEditContext : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector ();
	}
}
