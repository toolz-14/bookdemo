﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Hotspot_v3), true)]
[CanEditMultipleObjects]
public class MultiEditHotspotV3 : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector ();
	}
}
