﻿using UnityEditor;
using System.IO;

public class CreateAssetBundlesWebgl
{
	[MenuItem("Assets/Build AssetBundlesWebgl")]
	static void BuildAllAssetBundles()
	{
		string assetBundleDirectory = "Assets/BagneuxBundlesWebgl";
		if(!Directory.Exists(assetBundleDirectory))
		{
			Directory.CreateDirectory(assetBundleDirectory);
		}
		BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.WebGL);
	}
}
