﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolz.EditorScripts {
    [AddComponentMenu("Toolz/EditorScripts")]
    [ExecuteInEditMode]
    public class EditorGetAddedChildren : MonoBehaviour {

        List<GameObject> childrenList;
        List<GameObject> newChildrenList;

        [HideInInspector]
        public GameObject addedchild;
        public GameObject removedchild;

        void Awake() {
            childrenList = new List<GameObject>();
            foreach (Transform child in transform) {
                childrenList.Add(child.gameObject);
            }
        }

        void Start() {

        }

        void OnTransformChildrenChanged() {

            Debug.Log(GetNewChildReference());

        }

        string GetNewChildReference() {
            GameObject result = null;
            newChildrenList = new List<GameObject>();

            //get all children since number has changed
            foreach (Transform child in transform) {
                newChildrenList.Add(child.gameObject);
            }

            //child added
            if (newChildrenList.Count > childrenList.Count) {
                foreach (GameObject child in newChildrenList) {
                    if (!childrenList.Contains(child.gameObject)) {
                        result = child;
                    }
                }

                //add child to persistant list
                childrenList.Add(result);

                return "added child: " + result.name;

            }
            //child removed
            else if (newChildrenList.Count < childrenList.Count) {
                foreach (GameObject child in childrenList) {
                    if (!newChildrenList.Contains(child.gameObject)) {
                        result = child;
                    }
                }

                //remove child from persistant list
                childrenList.Remove(result);

                return "removed child: " + result.name;
            }
            else {
                //newChildrenList.Count == childrenList.Count so error
                Debug.LogError("lists are the same size, error in the code!");
                return "";
            }
        }
    }
}