﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Toolz.EditorScripts {
	
	[CustomEditor(typeof(Toolz.UI.Hostpot_v2), true)]
	[CanEditMultipleObjects]
	public class ButtonUpdateHotspot : Editor {
		
		private Toolz.UI.Hostpot_v2 _evCtrl = null;
		
		void OnEnable(){
			_evCtrl = (Toolz.UI.Hostpot_v2)target;
		}
		
		public override void OnInspectorGUI() {
			
			DrawDefaultInspector ();
			
			if(GUILayout.Button("Update Hotspot")){
				_evCtrl.UpdateHotspot();
				EditorUtility.SetDirty( target );
			}
		}

		public override bool RequiresConstantRepaint()
		{
			return true;
		}
	}
}

