﻿using UnityEditor;
using System.IO;

public class CreateAssetBundlesAndroid
{
	[MenuItem("Assets/Build AssetBundlesAndroid")]
	static void BuildAllAssetBundles()
	{
		string assetBundleDirectory = "Assets/BagneuxBundlesAndroid";
		if(!Directory.Exists(assetBundleDirectory))
		{
			Directory.CreateDirectory(assetBundleDirectory);
		}
		BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.Android);
	}
}
