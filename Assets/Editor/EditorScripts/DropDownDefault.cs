﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Toolz.EditorScripts {
	
	[CustomEditor(typeof(DropDownSelected), true)]
	public class DropDownDefault : Editor {

		private DropDownSelected _evCtrl = null;
		
		void OnEnable(){
			_evCtrl = (DropDownSelected)target;
		}
		
		public override void OnInspectorGUI() {
			
			DrawDefaultInspector ();
			
			if(GUILayout.Button("Update Default Values")){
				_evCtrl.SetDefaultLanguage();
				EditorUtility.SetDirty( target );
			}
		}
		
		public override bool RequiresConstantRepaint()
		{
			return true;
		}
	}
}
