﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Toolz.EditorScripts {

	[CustomEditor(typeof(Toolz.Bricks.View.LookAtCamera), true)]
	public class SnapCameraToTarget : Editor {

		private Toolz.Bricks.View.LookAtCamera _evCtrl = null;
		
		void OnEnable(){
			_evCtrl = (Toolz.Bricks.View.LookAtCamera)target;
		}

		public override void OnInspectorGUI() {

			DrawDefaultInspector ();

			if(GUILayout.Button("SnapCameraToTarget")){
				_evCtrl.LookAtTheTarget();
			}

		}
	}
}
