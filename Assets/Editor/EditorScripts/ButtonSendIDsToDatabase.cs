﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Toolz.EditorScripts {
	
	[CustomEditor(typeof(SetDatabaseProjectList), true)]
	public class ButtonSendIDsToDatabase : Editor {

		private SetDatabaseProjectList _evCtrl = null;
		
		void OnEnable(){
			_evCtrl = (SetDatabaseProjectList)target;
		}
		
		public override void OnInspectorGUI() {
			
			DrawDefaultInspector ();
			
			if(GUILayout.Button("SendAllIDsToDatabase")){
				_evCtrl.SendProjectsIDToDatabase();
			}
			
		}
	}
}
