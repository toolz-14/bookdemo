﻿using UnityEditor;
using System.IO;

public class CreateAssetBundlesiOS
{
	[MenuItem("Assets/Build AssetBundlesiOS")]
	static void BuildAllAssetBundles()
	{
		string assetBundleDirectory = "Assets/BagneuxBundlesiOS";
		if(!Directory.Exists(assetBundleDirectory))
		{
			Directory.CreateDirectory(assetBundleDirectory);
		}
		BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.iOS);
	}
}
