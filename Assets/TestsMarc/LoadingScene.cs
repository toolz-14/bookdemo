﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour {

	public string SceneName = "";
	AsyncOperation async = null;
	int pointsAmount = 0;
	Text _text;

	void OnEnable () {
		_text = GetComponent<Text> ();
		if (SceneName != "") {
			StartCoroutine ("Load");
			StartCoroutine("Display");
		}

	}

	void Ondisable(){
		StopCoroutine ("Load");
		StopCoroutine ("Display");
	}

	void Update () {
		if(async != null){
			if(async.progress >= 0.9f){
				async.allowSceneActivation = true;
			}
		}
	}

	IEnumerator Load() {
		yield return new WaitForEndOfFrame();
		async = SceneManager.LoadSceneAsync(SceneName);
		async.allowSceneActivation = false;
		yield return async;
	}

	IEnumerator Display(){
		if(pointsAmount > 2){
			_text.text = "Loading ";
			pointsAmount = 0;
		}
		if(pointsAmount == 0 || pointsAmount == 1 || pointsAmount == 2){
			//add one and wait
			_text.text += ".";
			pointsAmount++;
		}
		yield return new WaitForSeconds (0.2f);
		StartCoroutine("Display");
	}
}
