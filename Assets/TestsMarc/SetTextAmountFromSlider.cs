﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetTextAmountFromSlider : MonoBehaviour {

	public Slider Slider;
	public Text Mirrors;
	public Text Produced;

	private void Start(){

	}

	public void SetTheTexts(){
		int i = (int)Slider.value;

		Mirrors.text = "Nombre de mirroirs: "+ (i*5).ToString();
		Produced.text = "Produced: "+i.ToString()+" MWe";
	}
}
