﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class ReadAudioFromDesktop : MonoBehaviour {

	#if UNITY_STANDALONE
	private string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
	private string _audioPath;
	
	private void Start () {
		_audioPath = GetAudioPath ();
		Debug.Log ("_audioPath = "+_audioPath);
		if (_audioPath != "") {
			StartCoroutine ("LaunchAudio");
		}
	}
	
	IEnumerator LaunchAudio(){

		WWW loader = new WWW (_audioPath);
		yield return loader;
		AudioClip music = loader.GetAudioClip();

		if (GetComponent<AudioSource> () == null) {
			Debug.LogWarning ("This script needs to be on an objetc with an audio source!");
		} else {
			GetComponent<AudioSource> ().clip = music;
			GetComponent<AudioSource> ().loop = true;
			GetComponent<AudioSource> ().Play ();
		}
	}
	
	private string GetAudioPath(){
		filePath =filePath +@"\BackendStade\UsedMusic\";
		
		if (!Directory.Exists (filePath)) {
			Debug.Log ("BackendStade UsedMusic n'existe pas");
			return "";
		} else {
			Debug.Log ("BackendStade UsedMusic existe !!");
			
			string[] filesname = Directory.GetFiles(filePath);
			if(filesname != null && filesname.Length > 0){
				Debug.Log("filesname = "+filesname[0]);
				return "file:///" +filesname[0];
			}else{
				Debug.Log ("No file in UsedMusic folder");
				return "";
			}
		}
	}
	
	#endif
}
