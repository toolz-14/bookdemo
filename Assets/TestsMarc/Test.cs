﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class Test : MonoBehaviour {

	ProjectContainer aProject;

	void Start () {

		ReadSomeData ();

		Invoke ("WriteSomeData", 4f);
	}

	public void ReadSomeData(){
		aProject = ProjectContainer.Load(Path.Combine(Application.dataPath, "Resources/XML/TerrainSwap.xml"));
		for(int i=1; i<aProject.ProjectParts.Length; i++){

		}
	}

	public void WriteSomeData(){

		aProject.Save(Path.Combine(Application.dataPath, "Resources/XML/TerrainSwap.xml"));
		Invoke ("ReadSomeData", 2f);
	}
}

// when to read = on scene load

//when to save = ? on scene load after read ? check if value null then fill ?

//if nothing to read [no xml] ? create xml ?