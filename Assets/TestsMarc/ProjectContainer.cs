﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("ProjectData")]
public class ProjectContainer {

	[XmlArray("ProjectParts"),XmlArrayItem("ProjectPart")]
	public ProjectPart[] ProjectParts;

	[XmlArray("SettingsProject"),XmlArrayItem("PSettings")]
	public PSettings[] SettingsProject;
	
	public void Save(string path)
	{
		var serializer = new XmlSerializer(typeof(ProjectContainer));
		using(var stream = new FileStream(path, FileMode.Create))
		{
			serializer.Serialize(stream, this);
		}
	}
	
	public static ProjectContainer Load(string path)
	{

		var serializer = new XmlSerializer(typeof(ProjectContainer));
		using(var stream = new FileStream(path, FileMode.Open))
		{
			return serializer.Deserialize(stream) as ProjectContainer;
		}
	}
	
	//Loads the xml directly from the given string. Useful in combination with www.text.
	public static ProjectContainer LoadFromText(string text) 
	{
		var serializer = new XmlSerializer(typeof(ProjectContainer));
		return serializer.Deserialize(new StringReader(text)) as ProjectContainer;
	}
	
//	public void CreateXML() 
//   { 
//      StreamWriter writer; 
//      FileInfo t = new FileInfo(_FileLocation+"\\"+ _FileName); 
//      if(!t.Exists) 
//      { 
//         writer = t.CreateText(); 
//      } 
//      else 
//      { 
//         t.Delete(); 
//         writer = t.CreateText(); 
//      } 
//      writer.Write(_data); 
//      writer.Close(); 
//      Debug.Log("File written."); 
//   }
}
