﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;


public class ProjectPart
{ 
	[XmlAttribute("id")]
	public string id;
	
	public string Title;
	public string Text;
	public string Video;
	
}

