﻿Shader "Reflective/Transparent/Diffuse" {
	Properties {
		_Color ("Main Color", Color) = (1, 1, 1, 1)
		_ReflectColor ("Reflection Color", Color) = (1, 1, 1, 1)
		
		_MainAlpha("MainAlpha", Range(0, 1)) = 1
        _CubeAlpha("ReflectionAlpha", Range(0, 1)) = 1
        _TintColor ("Tint Color (RGB)", Color) = (1,1,1)

		_MainTex ("Base Texture (RGB)", 2D) = "white" {}
		_Cube ("Reflection Cubemap", Cube) = "_Skybox" {TexGen CubeReflect}
	}
	
	SubShader {
		Tags {"IgnoreProjector" = "True" "RenderType" = "Transparent"}
		LOD 200
		Cull Off
        ZWrite Off
        Colormask RGBA   
        Blend SrcAlpha OneMinusSrcAlpha
        Color [_TintColor]
       
        
		CGPROGRAM
		#pragma surface surf Lambert alpha
		
		sampler2D _MainTex;
		samplerCUBE _Cube;
		fixed4 _Color;
		fixed4 _ReflectColor;
		
		struct Input {
			float2 uv_MainTex;
			float3 worldRefl;
		};
		
		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 c = tex * _Color;
			o.Albedo = c.rgb;
			
			fixed4 reflcol = texCUBE(_Cube, IN.worldRefl);
			reflcol *= tex.a;
			o.Emission = reflcol.rgb * _ReflectColor.rgb;
			o.Alpha = c.a;
		}
		
		ENDCG
		
		 Pass {
            SetTexture[_Cube] { constantColor(0,0,0, [_CubeAlpha]) matrix [_ProjMatrix] combine texture * previous, constant}
        }
        Pass{
            SetTexture[_MainTex] { constantColor(0,0,0, [_MainAlpha]) combine texture * primary, texture * constant}
        }
	}
	
	Fallback "Transparent/VertexLit"
} 
