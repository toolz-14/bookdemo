using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;


public class SettingsProject
{ 
	//general
	public string projectId;
	public string projectName;
	public int projectCost;
	
	//menu
	public bool Context;
	public bool Fonctionnement;
	public bool CoutBenefice;
	public bool Realisation;
	
	//sub menu
	public bool Discussion;
	public bool Comparer;
	
	//ui parts
	public bool ShowVotes;
	public bool ShowBudget;
	
	//themes
	public bool energy;
	public bool waste;
	public bool water;
	public bool transport;
	public bool architecture;
	public bool economy;
	public bool education;
	public bool health;
	public bool culture;

}