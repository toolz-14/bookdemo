﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GlobalErrorManager : MonoBehaviour {

	private Animation theAnim;
    private string msg = "Please log in to use this feature.";
    public string newMsg = "";

    private void Awake(){
		theAnim = GetComponent<Animation> ();
	}

	public void OpenGlobalErrorPanel(){

		CancelInvoke ("CloseGlobalErrorPanel");
		theAnim["GlobalErrorSlideIn"].speed = 1f;
        if(!newMsg.Equals(""))
            transform.Find("ErrorMessage").GetComponent<Text>().text = newMsg;
        theAnim.Play("GlobalErrorSlideIn");

		Invoke ("CloseGlobalErrorPanel", 3f);
	}
	
	public void CloseGlobalErrorPanel(){
		CancelInvoke ("CloseGlobalErrorPanel");
		theAnim["GlobalErrorSlideIn"].time = theAnim["GlobalErrorSlideIn"].length;
		theAnim["GlobalErrorSlideIn"].speed = -1f;
		
		theAnim.Play("GlobalErrorSlideIn");
		Invoke ("SwitchOff", (theAnim["GlobalErrorSlideIn"].length + 0.5f));
	}

	private void SwitchOff(){
		gameObject.SetActive (false);
        newMsg = "";
        transform.Find("ErrorMessage").GetComponent<Text>().text = msg;


    }
}
