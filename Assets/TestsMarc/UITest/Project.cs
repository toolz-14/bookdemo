﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Project : MonoBehaviour {


	//general
	public string projectName;
	public string projectDescription;
	public string projectSuivi;
	public int stepProgressionProject = 1;
	public int voteNumber = 0;
	public bool isProjectChoosen = false;
	public bool is2018 = false;
	//can contain ONLY letters numbers underscores minus.
	public string projectId;

	//cout du projet
	public float projectCost = 10000f;

	//projectUIPanel management
	public enum ProjectUIElements{ hidden, shown}
	public ProjectUIElements contexte;
	public ProjectUIElements fonctionnement;
	public ProjectUIElements coutbenefice;
	public ProjectUIElements realisation;
	public ProjectUIElements discussion;
	public ProjectUIElements comparer;
	
	public bool ShowVotes;
	public bool ShowBudget;

	//get ref to the hotspot button to get access to its transition
	//bad way of doing this, when there is time to work on the framework, 
	//the project should hold the data to be able to create the transition where we need it
	public GameObject Hotspot;
 	//same as above but for project exit button
	public Button ExitButton;

	//project Theme management
	public enum ProjectTheme{ no, yes}
	public ProjectTheme vie;
	public ProjectTheme social;
	public ProjectTheme culture;
	public ProjectTheme education;
	public ProjectTheme ville;
	public ProjectTheme sport;
	public ProjectTheme transport;
	public ProjectTheme enfance;

	public Dictionary<string, bool> ProjectsThemes;
	[HideInInspector]
	public List<string> activeThemes;
	[HideInInspector]
	public float CurrentUserInvestment;
	[HideInInspector]
	public float AllUserInvestmentAverage;

//editor button: update xml with this script values
//editor button: update this script with xml values
	
	private void Start(){

	}

	private void Awake () {
		CreateThemeDictio ();
	}


	private void CreateThemeDictio(){

		ProjectsThemes = new Dictionary<string, bool>();
		activeThemes = new List<string> ();

		ProjectsThemes ["vie"] = System.Convert.ToBoolean ((int)vie);
		if(System.Convert.ToBoolean ((int)vie)){
			activeThemes.Add("vie");
		}

		ProjectsThemes ["social"] = System.Convert.ToBoolean ((int)social);
		if(System.Convert.ToBoolean ((int)social)){
			activeThemes.Add("social");
		}

		ProjectsThemes ["culture"] = System.Convert.ToBoolean ((int)culture);
		if(System.Convert.ToBoolean ((int)culture)){
			activeThemes.Add("culture");
		}

		ProjectsThemes ["education"] = System.Convert.ToBoolean ((int)education);
		if(System.Convert.ToBoolean ((int)education)){
			activeThemes.Add("education");
		}

		ProjectsThemes ["ville"] = System.Convert.ToBoolean ((int)ville);
		if(System.Convert.ToBoolean ((int)ville)){
			activeThemes.Add("ville");
		}

		ProjectsThemes ["sport"] = System.Convert.ToBoolean ((int)sport);
		if(System.Convert.ToBoolean ((int)sport)){
			activeThemes.Add("sport");
		}

		ProjectsThemes ["transport"] = System.Convert.ToBoolean ((int)transport);
		if(System.Convert.ToBoolean ((int)transport)){
			activeThemes.Add("transport");
		}

		ProjectsThemes ["enfance"] = System.Convert.ToBoolean ((int)enfance);
		if(System.Convert.ToBoolean ((int)enfance)){
			activeThemes.Add("enfance");
		}
	}
}
