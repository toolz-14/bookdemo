﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResizeTheViewport : MonoBehaviour {
	
	public ProjectUIManager ProjectUIManager;
	public Button Exit_Button;
	public Project Module;
	public Camera ModuleCam;
	
	private bool _firstTimeIn;
	private bool _firstTimeOut;
	
	private float _from1;
	private float _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	float Duration1 = 0.5f;
	
	private void Start () {
		
	}
	
	private void Update () {
		if (_launch) {
			ResizeViewport ();
		}
	}
	
	public void StartResize(bool isOpening){
		_timeSinceTransitionStarted = 0f;
		if (isOpening) {
			_from1 = ModuleCam.rect.width;
			_to1 = 0.8f;
			Duration1 = 0.5f;
		} else {
			_from1 = ModuleCam.rect.width;
			_to1 = 1f;
			Duration1 = 0.3f;
		}
		_launch = true;
	}
	
	private void Resize(){
		//StartResize(true);
	}
	
	private void ResizeViewport(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		ModuleCam.rect = new Rect (0, 0, Mathf.Lerp (_from1, _to1, _t), 1f);
		if( Mathf.Abs(ModuleCam.rect.width - _to1) < 0.001f){
			_launch = false;
		}
	}
	
		private void OnEnable () {
			if (_firstTimeIn) {
				Invoke("Resize", 0.1f);
	
			} else {
				_firstTimeIn = true;
			}
		}
}
