﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Toolz.WebServices;
using System.Collections.Generic;

public class RealisationUITest : MonoBehaviour {

	private Slider _slider;
	public Text SliderValueDisplay;

	private PostUserData _postUserDataService;
	private GetUserData _getUserDataService;

	private void OnDisable(){
		SaveUserValue ();
	}

	public void SetSliderMax(){
		_slider.maxValue = ProjectUIManager.CurrentProjectData.projectCost;
		Debug.Log ("_slider.maxValue = " + _slider.maxValue);
	}
	
	private void Awake(){
		_slider = GetComponent<Slider>();
	}

	private void Update(){
		if(enabled){
			SliderValueDisplay.text = ((int)_slider.value).ToString();
		}
	}

	private void OnEnable () {
		if (GameObject.FindGameObjectWithTag ("WebServices") != null &&
		    GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser != -1) {
			Debug.Log("user is logged in");
			_postUserDataService = GameObject.FindGameObjectWithTag ("WebServices").GetComponent<PostUserData> ();
			_postUserDataService.onComplete += (status, message) => 
			{
				Debug.Log("Post User Data OK");
			};

			_postUserDataService.onError += (status, message) => 
			{
				Debug.Log("Post User Data ERROR");
			};

			_getUserDataService = GameObject.FindGameObjectWithTag ("WebServices").GetComponent<GetUserData> ();
			_getUserDataService.onComplete += _getUserDataService_onComplete;
			_getUserDataService.onError += _getUserDataService_onError;

			_getUserDataService.UseWebService ();
		}
	}

	private void _getUserDataService_onError(long status, string message) {
		Debug.LogError("Get user data ERROR");
		_getUserDataService.onError -= _getUserDataService_onError;
	}
	
	private void _getUserDataService_onComplete(long status, string message) {
		Debug.Log("Get Users data OK");
		if(status == (long)WebService.Status.OK) {
			if(_getUserDataService.ProjectsData.ContainsKey(ProjectUIManager.CurrentProjectData.idLabel)) {
				_slider.value = (float)_getUserDataService.ProjectsData[ProjectUIManager.CurrentProjectData.idLabel].Investment;
			}
		}
		_getUserDataService.onComplete -= _getUserDataService_onComplete;
	}

	private void SaveUserValue(){
		if (GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser != -1) {
			Dictionary<string, Toolz.WebServices.ProjectData> data = _postUserDataService.ProjectsData;
			data.Clear ();

			string id = ProjectUIManager.CurrentProjectData.idLabel;
			Toolz.WebServices.ProjectData pdata = new Toolz.WebServices.ProjectData ();
			pdata.IdProject = id;
			pdata.IdUser = GameObject.FindGameObjectWithTag ("WebServices").GetComponent<User> ().idUser;
			pdata.Applied = false;
			//Debug.Log ("_slider.value = " + _slider.value);
			pdata.Investment = (int)_slider.value;
			data.Add (id, pdata);
			_postUserDataService.UseWebService ();
		}
	}
}
