﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropDown1Entry : MonoBehaviour {

	public GameObject Holder;
	//french
	public GameObject LanguageButton1;
	//english
	public GameObject LanguageButton2;
	public float Duration = 0.01f;
	public DropDownSelected DropDownSelected;
	private Vector2 _temp;
	[HideInInspector]
	public bool isOpen;
	[HideInInspector]
	public bool buttonIsBlocked;

	private void Start () {

	}

	public void AnimateButtonList(){
		if (!buttonIsBlocked) {
			buttonIsBlocked = true;
			if (isOpen) {
				AnimateButtonClose ();
			} else {
				AnimateButtonOpen ();
			}
			isOpen = !isOpen;
		}
	}

	public void AnimateButtonOpen(){
		
		Holder.SetActive (true);
		
		//all buttons at 1st pos
		if(DropDownSelected.GetCurrentLanguage() == "English"){
			LanguageButton2.SetActive (false);
			_temp = LanguageButton1.GetComponent<RectTransform>().anchoredPosition;
			_temp.y = 36.5f;
			LanguageButton1.GetComponent<RectTransform>().anchoredPosition = _temp;
		}else if(DropDownSelected.GetCurrentLanguage() == "French"){
			LanguageButton1.SetActive (false);
			_temp = LanguageButton2.GetComponent<RectTransform>().anchoredPosition;
			_temp.y = 36.5f;
			LanguageButton2.GetComponent<RectTransform>().anchoredPosition = _temp;
		}
	}

	public void AnimateButtonClose(){
		Holder.SetActive (false);
		LanguageButton1.SetActive (true);
		LanguageButton2.SetActive (true);
	}
}
