﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControlsPanelButtonMenu : MonoBehaviour {

	public GameObject ControlePanel;

	private void Start () {

	}

	public void ControlPanelState(){
		if (!ControlePanel.activeInHierarchy) {
			ControlePanel.SetActive (true);
		}
		ControlePanel.GetComponent<ControlsPanel> ().SetControlPanelActif ();
	}
}
