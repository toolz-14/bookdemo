﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OptionsDisplayManager : MonoBehaviour {

	public List<GameObject> OptionsList = new List<GameObject>();

	public bool ButtonInterfaceActive = true;
	public bool ButtonArbresActive = true;

	public Image OptionsIcon;
	public Sprite OptionsActive;
	public Sprite OptionsInactive;

//	public ControlsPanel ControlsPanel;
//	public LanguagePanel LanguagePanel;

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;
	
	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private bool _isOpen;
	private bool _buttonLocked;

	private void Awake () {
		_isOpen = false;
		_buttonLocked = false;
		OptionsIcon.sprite = OptionsInactive;
		SetMenus ();
	}
	
	
	private void Start () {

	}

	private void Update () {
		if(_launch){
			OpenOptionsPanel();
		}
		if(_launch2){
			CloseOptionsPanel();
		}
	}
	
	private void SetMenus(){
		OptionsList [0].SetActive (ButtonInterfaceActive);
		OptionsList [1].SetActive (ButtonArbresActive);
	}

	public bool GetIsOpen(){
		return _isOpen;
	}

	public void OnOptionsClicked(){
		if (!_buttonLocked) {
			_buttonLocked = true;
			_isOpen = !_isOpen;
			if (_isOpen) {
				OptionsIcon.sprite = OptionsActive;
				StartOpenOptionsPanel ();
			} else {
				OptionsIcon.sprite = OptionsInactive;
				StartCloseOptionsPanel ();
//				//if controls still open, close it
//				if(ControlsPanel != null && ControlsPanel.GetIsOpen()){
//					ControlsPanel.SetControlPanelClosed();
//				}
//				//if language still open, close it
//				if(LanguagePanel != null && LanguagePanel.GetIsOpen()){
//					LanguagePanel.SetControlPanelClosed();
//				}
			}
		}
	}

	public void ForceCloseOptionsPanel(){
		if(_isOpen){
			Debug.Log("ForceCloseOptionsPanel");
			_isOpen = false;
			OptionsIcon.sprite = OptionsInactive;
			StartCloseOptionsPanel ();
		}
	}

	public void ForceOpenOptionsPanel(){
		if(!_isOpen){
			Debug.Log("ForceOpenOptionsPanel");
			_isOpen = true;
			OptionsIcon.sprite = OptionsActive;
			StartOpenOptionsPanel ();
		}
	}

	private void StartOpenOptionsPanel(){
		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().anchoredPosition;
		_to1 = new Vector2 (40f, GetComponent<RectTransform> ().anchoredPosition.y);
		_launch = true;
	}
	
	private void OpenOptionsPanel(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f){
			_launch = false;
			_buttonLocked = false;
		}
	}
	
	public void StartCloseOptionsPanel(){
		_timeSinceTransitionStarted2 = 0f;
		_from2 = GetComponent<RectTransform> ().anchoredPosition;
		_to2 = new Vector2 (-150f, GetComponent<RectTransform> ().anchoredPosition.y);
		_launch2 = true;
	}
	
	private void CloseOptionsPanel(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
			_buttonLocked = false;
			gameObject.SetActive (false);
		}
	}
}
