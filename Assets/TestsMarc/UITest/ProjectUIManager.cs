﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;

public struct ProjectData {
	//general
	public string projectName;
	public string projectDescription;
	public string projectSuivi;
	public int stepProgressionProject;
	public bool isProjectChoosen;
	public bool is2018;
	public int voteNumber;
	public string idLabel;
	public float projectCost;
    public int nbUpvote;
    public int nbDownVote;
	
	//projectUIPanel management
	public bool contexte;
	public bool fonctionnement;
	public bool coutbenefice;
	public bool realisation;
	public bool discussion;
	public bool comparer;
	
	public bool ShowVotes;
	public bool ShowBudget;
	
	//project Theme management
	public List<string> activeThemes;
	public bool vie;
	public bool social;
	public bool culture;
	public bool education;
	public bool ville;
	public bool sport;
	public bool transport;
	public bool enfance;

	public Project_ContextData contextData;
	public Project_FonctionnementData fonctionnementData;
	public Project_CoutbenefData coutbenefData;
	public Project_RealisationData realisationData;
}

public class ProjectUIManager : MonoBehaviour {
	[SerializeField] private List<string> listProjectAvancementType;
	[SerializeField] private List<Image> listImageProjectAvacement;
	private Image imageButton;

	[SerializeField] private ResizeContainerPanelProject resizeContainerScript;
	[SerializeField] private Sprite UIFolderOFF;
	[SerializeField] private Sprite UIFolderON;

    public GameObject PopUp;
	public Text Budget;
	public Text descriptionText;
	public Text suiviText;
	public Text voteText;
	public GameObject pictureVoteYear;
	public GameObject description_GAO;
	public GameObject video_GAO;
	public GameObject SuivisProject_GAO;
	public GameObject localisation_GAO;

	public GameObject description_Info_GAO;
	public GameObject video_Info_GAO;
	public GameObject SuivisProject_Info_GAO;
	public GameObject localisation_Info_GAO;

	public GameObject TogglesPanel;
	public GameObject panelLoader;
    public GameObject panelLoaderBP;
    public GlobalErrorManager GlobalErrorManager;
	public ProfileUIManager ProfileUIManager;
//	public ConnectionUIManager ConnectionPanel;
	public ThemeUIManager ThemePanel;
	public DiscussionUIManagerLabel DiscussionUIManager;
//	public LanguageSelectionUI LanguageSelectionUI;

	//exit button needs to exit the project too
	public GameObject ExitButton;
	public GameObject BackButton;
	public Text ProjectName;
	public Text SubSectionName;
	public GameObject ProjectVote;
	public GameObject Footer;
	public List<GameObject> Menu = new List<GameObject>();
	public List<GameObject> OtherMenu = new List<GameObject>();
	public GameObject ContainerVerneuil;
	public List<GameObject> VerneuilButtonHolder = new List<GameObject>();

	public float BudgetTotal = 100000f;

	private Button _projectExitButton;
	private Vector2 _tempVect2;
    private PostUserProjectVote _postUserProjectVoteService;
    private GetUserProjectVote _getUserProjectVoteService;
	
	public static ProjectData CurrentProjectData;

	public static List<ProjectData> CurrentProjectDataList;
	public static bool InitOnlyOnce;

    private bool isLocalisationOpen;

    // Instance
    private static ProjectUIManager _instance;
	public static ProjectUIManager Instance { get { return _instance; } }

	private void Awake () {
		if (_instance)
		{
			Destroy(gameObject);
			return;
		}

		_instance = this;

		InitOnlyOnce = false;
		SetProjectDataStructEmpty ();
	}

	private void Start() {
        BackButton.SetActive(false);
    }

	public void OpenProjecPanel(Button projectExitButton){
		if(ThemePanel.Overlay.activeInHierarchy){
			ThemePanel.Overlay.SetActive (false);
			ThemePanel.DeactivateAllProjectList();
			ThemePanel.SetAllButtonColorToTheme ();
		}

        isLocalisationOpen = false;
            
        GetComponent<ProjectPanelLerp> ().StartLerpToQuaterCanvas ();

		//get ref to project exit button
		_projectExitButton = projectExitButton;

		//if profile ui still open, close it
		if(ProfileUIManager.IsOpen){
			ProfileUIManager.CloseProfilePanel();
		}

		//close ui
//		if (ConnectionPanel != null) {
//			ConnectionPanel.CloseConnectionPanel ();
//		}
//		if (ThemePanel != null) {
//			ThemePanel.CloseThemePanel ();
//		}
//		if (LanguageSelectionUI != null) {
//			LanguageSelectionUI.LangPanelSlideOut ();
//		}

//		if (TogglesPanel != null && TogglesPanel.activeInHierarchy) {
//			TogglesPanel.SetActive (false);
//		}

		if (PopUp != null && PopUp.activeInHierarchy) {
			PopUp.SetActive (false);
		}

		resizeContainerScript.GetChildrensAndResize();

	}

	public void CloseProjectPanel(){
		if(ContainerVerneuil != null && ContainerVerneuil.activeInHierarchy){
			ContainerVerneuil.SetActive (false);
		}
		if(DiscussionUIManager != null && DiscussionUIManager.IsOpen){
			DiscussionUIManager.CloseDiscussionPanel ();
		}
		GetComponent<ProjectPanelLerp> ().StartLerpBack ();
		//if(_projectExitButton != null){
		//	if(_projectExitButton.transform.parent.parent.GetComponent<ResizeTheViewport>() != null)
		//	_projectExitButton.transform.parent.parent.GetComponent<ResizeTheViewport>().StartResize(false);
		//}

		ForceClose(description_GAO, description_Info_GAO);
		ForceClose(video_GAO, video_Info_GAO);
		ForceClose(SuivisProject_GAO, SuivisProject_Info_GAO);
		ForceClose(localisation_GAO, localisation_Info_GAO);

		resizeContainerScript.GetChildrensAndResize();

	}

	public void AfterPanelClose(){

		if(_projectExitButton != null){
			//close the project as well
			_projectExitButton.onClick.Invoke();
		}
		
		//open ui
//		if (ConnectionPanel != null) {
//			ConnectionPanel.OpenConnectionPanel ();
//		}
//		if (ThemePanel != null) {
//			ThemePanel.OpenThemePanel ();
//		}
//		if (LanguageSelectionUI != null) {
//			LanguageSelectionUI.LangPanelSlideIn ();
//		}

//		if (TogglesPanel != null && !TogglesPanel.activeInHierarchy) {
//			TogglesPanel.SetActive (true);
//		}

		SetProjectDataStructEmpty ();
		
		//reset menus
		//foreach(GameObject go in Menu){
		//	go.SetActive(false);
		//}

		gameObject.SetActive (false);

		if (panelLoader != null && panelLoader.activeInHierarchy) {
			panelLoader.SetActive (false);
		}
        if (panelLoaderBP != null && panelLoaderBP.activeInHierarchy)
        {
            panelLoaderBP.SetActive(false);
        }
        

    }
	public void SetImageButtonClick(Image imageButtonClick)
	{
		imageButton = imageButtonClick;
	}

	public void OnClickCategoriesProject(GameObject children = null)
	{
		if (imageButton.sprite == UIFolderOFF)
		{
			CloseCategories(children);
		}
		else
		{
			OpenCategories(children);
		}

		resizeContainerScript.GetChildrensAndResize();
	}

	private void OpenCategories(GameObject children)
	{
		imageButton.sprite = UIFolderOFF;
        if (children != null)
        {
            children.SetActive(true);
            if (children.name.Equals("TODELETE"))
            {
                OpenLocalisation();
            }
        }
    }

	private void CloseCategories(GameObject children)
	{
		imageButton.sprite = UIFolderON;
        if (children != null)
        {
            children.SetActive(false);
            if (children.name.Equals("TODELETE"))
            {
                CloseLocalisation();
            }
        }
    }

	private void ForceClose( GameObject buttonGAO, GameObject children)
	{
		SetImageButtonClick( buttonGAO.transform.GetChild(1).GetComponent<Image>() );
		CloseCategories(children);
	}

	public void OpenSuiviDuProjet(Text txtPhase)
	{
		for (int i = 0; i < CurrentProjectData.stepProgressionProject; i++)
		{
			listImageProjectAvacement[i].color = Color.green;
		}

		for (int i = CurrentProjectData.stepProgressionProject; i < listImageProjectAvacement.Count; i++)
		{
			listImageProjectAvacement[i].color = Color.white;
		}

		txtPhase.text = listProjectAvancementType[CurrentProjectData.stepProgressionProject - 1];
	}

	public void OpenVideo()
	{
		if (CurrentProjectData.coutbenefData.MyVideoPlayer != null)
		{
			CurrentProjectData.coutbenefData.MyVideoPlayer.StartTheVideo();
			GetComponent<CanvasGroup>().alpha = 0;
			GetComponent<CanvasGroup>().interactable = false;
		}
	}

    public void OpenLocalisation()
    {
        if (CurrentProjectData.contextData.fixedCamera != null)
        {
            CurrentProjectData.contextData.CameraSphere.SetActive(false);
            CurrentProjectData.contextData.fixedCamera.SetActive(true);
            Toolz.Managers.LevelManager.ActiveCamera = CurrentProjectData.contextData.fixedCamera.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
        }
    }

    public void CloseLocalisation()
    {
        if (CurrentProjectData.contextData.fixedCamera != null && CurrentProjectData.contextData.fixedCamera.activeInHierarchy)
        {
            CurrentProjectData.contextData.CameraSphere.SetActive(true);
            CurrentProjectData.contextData.fixedCamera.SetActive(false);
        }
    }

    public void ActiveProjectPanel()
	{
		GetComponent<CanvasGroup>().alpha = 1;
		GetComponent<CanvasGroup>().interactable = true;

	}

	public void SetSubSectionTitle(string title){
		SubSectionName.text = title;
	}
	
	public void SwapTitles(bool isSubsection){
		ProjectName.gameObject.SetActive (!isSubsection);
		SubSectionName.gameObject.SetActive (isSubsection);
		
	}

	public void SetProjectData(Project theProject){
        if (!InitOnlyOnce){
			InitOnlyOnce = true;
            /*
			GameObject webservice = GameObject.FindGameObjectWithTag("WebServices");
			_postUserProjectVoteService = webservice.GetComponent<PostUserProjectVote>();
			_postUserProjectVoteService.onComplete += _postUserProjectVoteService_onComplete;
			_postUserProjectVoteService.onError += _postUserProjectVoteService_onError;

			_getUserProjectVoteService = webservice.GetComponent<GetUserProjectVote>();
			_getUserProjectVoteService.onComplete += _getUserProjectVoteService_onComplete;
			_getUserProjectVoteService.onError += _getUserProjectVoteService_onError;*/
		}
		SetProjectDataStruct (theProject);
		_tempVect2 = Vector2.zero;
		int i;
//		Debug.Log ("CurrentProjectData.projectName = "+CurrentProjectData.projectName);
		ProjectName.text = CurrentProjectData.projectName;
		ProjectVote.SetActive(CurrentProjectData.ShowVotes);

		Budget.text = CurrentProjectData.projectCost.ToString () + "€";
		descriptionText.text = CurrentProjectData.projectDescription.ToString();
		suiviText.text = CurrentProjectData.projectSuivi.ToString ();
		voteText.text = CurrentProjectData.voteNumber.ToString () + " voix reçue";

		if (!CurrentProjectData.is2018 && CurrentProjectData.isProjectChoosen)
        {
            SuivisProject_GAO.SetActive(true);
            pictureVoteYear.SetActive(false);
        }
        else if(CurrentProjectData.is2018 && CurrentProjectData.isProjectChoosen)
        {
            SuivisProject_GAO.SetActive(true);
            pictureVoteYear.SetActive(true);
        }
        else if( (CurrentProjectData.is2018 && !CurrentProjectData.isProjectChoosen) ||
            (!CurrentProjectData.is2018 && !CurrentProjectData.isProjectChoosen) )
        {
            SuivisProject_GAO.SetActive(false);
            pictureVoteYear.SetActive(false);
        }


		Footer.SetActive(CurrentProjectData.ShowBudget);
		//if footer is show, set it
		if(CurrentProjectData.ShowBudget){
			SetFooter();
		}

		//Menu [0].SetActive (CurrentProjectData.contexte);
		//Menu [1].SetActive (CurrentProjectData.fonctionnement);
		//Menu [2].SetActive (CurrentProjectData.coutbenefice);
		//Menu [3].SetActive (CurrentProjectData.realisation);
		//i = 0;
		//foreach(GameObject go in Menu){
		//	if(go.activeInHierarchy){
		//		_tempVect2.x = go.GetComponent<RectTransform>().anchoredPosition.x;
		//		_tempVect2.y = i*(-50f);
		//		go.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
		//		i++;
		//	}
		//}

		//OtherMenu [0].SetActive (CurrentProjectData.discussion);
		//OtherMenu [1].SetActive (CurrentProjectData.comparer);
		//i = 0;
		//foreach(GameObject go in OtherMenu){
		//	if(go.activeInHierarchy){
		//		_tempVect2.x = go.GetComponent<RectTransform>().anchoredPosition.x;
		//		_tempVect2.y = i*(-50f);
		//		go.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
		//		i++;
		//	}
		//}

        //Get user's vote
        //_getUserProjectVoteService.idProject = CurrentProjectData.idProject;
        //_getUserProjectVoteService.UseWebService();

		if (CurrentProjectData.idLabel == "VerneuilBatiments") {
			ContainerVerneuil.SetActive (true);
			//activate buttons
			VerneuilButtonHolder [0].SetActive (true);
			VerneuilButtonHolder [1].SetActive (false);
			VerneuilButtonHolder [2].SetActive (false);
			VerneuilButtonHolder [3].SetActive (false);
			//set title
			ContainerVerneuil.transform.Find ("BackGround").Find ("Title").GetComponent<Text> ().text = "Hauteur des batiments";

		} else if (CurrentProjectData.idLabel == "VerneuilQuais") {
			ContainerVerneuil.SetActive (true);
			//activate buttons
			VerneuilButtonHolder [0].SetActive (false);
			VerneuilButtonHolder [1].SetActive (true);
			VerneuilButtonHolder [2].SetActive (false);
			VerneuilButtonHolder [3].SetActive (false);
			//set title
			ContainerVerneuil.transform.Find ("BackGround").Find ("Title").GetComponent<Text> ().text = "Hauteur des quais";
		} else if (CurrentProjectData.idLabel == "VerneuilTalus") {
			ContainerVerneuil.SetActive (true);
			//activate buttons
			VerneuilButtonHolder [0].SetActive (false);
			VerneuilButtonHolder [1].SetActive (false);
			VerneuilButtonHolder [2].SetActive (true);
			VerneuilButtonHolder [3].SetActive (false);
			//set title
			ContainerVerneuil.transform.Find ("BackGround").Find ("Title").GetComponent<Text> ().text = "Hauteur des talus";
		} else if (CurrentProjectData.idLabel == "VerneuilEau") {
			ContainerVerneuil.SetActive (true);
			//activate buttons
			VerneuilButtonHolder [0].SetActive (false);
			VerneuilButtonHolder [1].SetActive (false);
			VerneuilButtonHolder [2].SetActive (false);
			VerneuilButtonHolder [3].SetActive (true);
			//set title
			ContainerVerneuil.transform.Find ("BackGround").Find ("Title").GetComponent<Text> ().text = "Hauteur de l'eau";
		}
	}

	public void FindProjectSetDataStruct(string projectId){
		Project[] allprojects = GameObject.FindObjectsOfType<Project> ();
		
		foreach (Project pr in allprojects) {
			if(pr.projectId == projectId){
				SetProjectDataStruct(pr);
			}
		}
	}

	private void SetProjectDataStructEmpty(){
		CurrentProjectData.projectName = "";
		CurrentProjectData.idLabel = "";

		CurrentProjectData.projectCost = 0;

		CurrentProjectData.contexte = false;
//		CurrentProjectData.contextData = new Project_ContextData();
		CurrentProjectData.contextData = null;

		CurrentProjectData.fonctionnement = false;
//		CurrentProjectData.fonctionnementData = new Project_FonctionnementData();
		CurrentProjectData.fonctionnementData = null;

		CurrentProjectData.coutbenefice = false;
//		CurrentProjectData.coutbenefData = new Project_CoutbenefData();
		CurrentProjectData.coutbenefData = null;

		CurrentProjectData.realisation = false;
//		CurrentProjectData.realisationData = new Project_RealisationData();
		CurrentProjectData.realisationData = null;

		CurrentProjectData.discussion = false;
		CurrentProjectData.comparer = false;

		CurrentProjectData.ShowVotes = false;
		CurrentProjectData.ShowBudget = false;

		CurrentProjectData.activeThemes = new List<string> ();
		CurrentProjectData.vie = false;

		CurrentProjectData.social = false;

		CurrentProjectData.ville = false;

		CurrentProjectData.transport = false;

		CurrentProjectData.sport = false;

		CurrentProjectData.enfance = false;

		CurrentProjectData.education = false;

		CurrentProjectData.culture = false;

	}



	// Si Besoin De setter des Value en provenance du BackEnd, c'est possible !!
	// Il suivis de get les infos avant le call de cette methode et de'affecter a la propertie de CurrentProjectData la value souhaiter.
	// Example : CurrentProjectData.projectDescription = backendValue;
	//
	// Alexandre
	
	private void SetProjectDataStruct(Project theProject){

		CurrentProjectData.projectName = theProject.projectName;
		CurrentProjectData.projectDescription = theProject.projectDescription;
		CurrentProjectData.projectSuivi = theProject.projectSuivi;
		CurrentProjectData.stepProgressionProject = theProject.stepProgressionProject;
		CurrentProjectData.voteNumber = theProject.voteNumber;
        CurrentProjectData.idLabel = theProject.projectId;
		CurrentProjectData.projectCost = theProject.projectCost;
		CurrentProjectData.isProjectChoosen = theProject.isProjectChoosen;
		CurrentProjectData.is2018 = theProject.is2018;

		CurrentProjectData.contexte = System.Convert.ToBoolean ((int)theProject.contexte);
		if(CurrentProjectData.contexte && theProject.gameObject.GetComponent<Project_ContextData>() != null){
			CurrentProjectData.contextData = theProject.gameObject.GetComponent<Project_ContextData>();
		}
		CurrentProjectData.fonctionnement = System.Convert.ToBoolean ((int)theProject.fonctionnement);
		if(CurrentProjectData.fonctionnement && theProject.gameObject.GetComponent<Project_FonctionnementData>() != null){
			CurrentProjectData.fonctionnementData = theProject.gameObject.GetComponent<Project_FonctionnementData>();
		}
		CurrentProjectData.coutbenefice = System.Convert.ToBoolean ((int)theProject.coutbenefice);
		if(CurrentProjectData.coutbenefice && theProject.gameObject.GetComponent<Project_CoutbenefData>() != null){
			CurrentProjectData.coutbenefData = theProject.gameObject.GetComponent<Project_CoutbenefData>();
		}
		CurrentProjectData.realisation = System.Convert.ToBoolean ((int)theProject.realisation);
		if(CurrentProjectData.realisation && theProject.gameObject.GetComponent<Project_RealisationData>() != null){
			CurrentProjectData.realisationData = theProject.gameObject.GetComponent<Project_RealisationData>();
		}
		CurrentProjectData.discussion = System.Convert.ToBoolean ((int)theProject.discussion);
		CurrentProjectData.comparer = System.Convert.ToBoolean ((int)theProject.comparer);

		CurrentProjectData.ShowVotes = theProject.ShowVotes;
		CurrentProjectData.ShowBudget = theProject.ShowBudget;

		CurrentProjectData.activeThemes = new List<string> ();
		CurrentProjectData.vie = System.Convert.ToBoolean ((int)theProject.vie);
		if(CurrentProjectData.vie){
			CurrentProjectData.activeThemes.Add("Vie");
		}
		CurrentProjectData.social = System.Convert.ToBoolean ((int)theProject.social);
		if(CurrentProjectData.social){
			CurrentProjectData.activeThemes.Add("Social");
		}
		CurrentProjectData.ville = System.Convert.ToBoolean ((int)theProject.ville);
		if(CurrentProjectData.ville){
			CurrentProjectData.activeThemes.Add("Ville");
		}
		CurrentProjectData.transport = System.Convert.ToBoolean ((int)theProject.transport);
		if(CurrentProjectData.transport){
			CurrentProjectData.activeThemes.Add("Transport");
		}
		CurrentProjectData.sport = System.Convert.ToBoolean ((int)theProject.sport);
		if(CurrentProjectData.sport){
			CurrentProjectData.activeThemes.Add("Sport");
		}
		CurrentProjectData.enfance = System.Convert.ToBoolean ((int)theProject.enfance);
		if(CurrentProjectData.enfance){
			CurrentProjectData.activeThemes.Add("Enfance");
		}
		CurrentProjectData.education = System.Convert.ToBoolean ((int)theProject.education);
		if(CurrentProjectData.education){
			CurrentProjectData.activeThemes.Add("Education");
		}
		CurrentProjectData.culture = System.Convert.ToBoolean ((int)theProject.culture);
		if(CurrentProjectData.culture){
			CurrentProjectData.activeThemes.Add("Culture");
		}

		SetColorLines ();
	}

	private void SetColorLines(){
		if (CurrentProjectData.activeThemes.Count > 0) {
			DeactivateallColorLines();
			Image line;
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 10f;
			for (int i=0; i < CurrentProjectData.activeThemes.Count; i++) {
				line = transform.Find ("VLayout").Find ("ColorLines").Find ("Image_"+(i+1).ToString()).GetComponent<Image>();
				line.color = GetThemeColor(CurrentProjectData.activeThemes[i]);
				line.gameObject.SetActive(true);
			}
		} else {
			transform.Find ("VLayout").Find ("ColorLines").GetComponent<LayoutElement> ().preferredHeight = 0f;
		}
	}
	
	private void DeactivateallColorLines(){
		Transform line;
		for(int i=0; i<9; i++){
			line = transform.Find ("VLayout").Find ("ColorLines").Find ("Image_"+(i+1).ToString());
			line.gameObject.SetActive(false);
		}
	}

	private Color GetThemeColor(string themeName){
		foreach(Theme th in ThemePanel.availableThemes){
			if(th.themeName.Contains(themeName.ToLower())){
				return th.themeColor;
			}
		}
		return Color.white;
	}

	public void ProjectRealisation(){
		//should show project mesh always?
		CloseProjectPanel ();
	}

	public void ProjectCancel(){
		//should hide project mesh always?
		CloseProjectPanel ();
	}

	public void ProjectValidate(){
		//should show project mesh always?
		//something with the budget ?
		CloseProjectPanel ();
	}

	private void SetFooter(){

		Vector3 tempScale;

		//set CostValue value and unit
		//cout du projet
		Footer.transform.Find("Budget").Find("CostValue").GetComponent<Text>().text = CurrentProjectData.projectCost.ToString()+" €";

		//set RemainingValue and unit
		//budgettotal - (tous les cout de projet realises) [-le cout de ce projet ?]
		Footer.transform.Find("Budget").Find("RemainingValue").GetComponent<Text>().text = "€";

		//set BudgetGauge Cost (scale x)
		tempScale = Footer.transform.Find("Budget").Find("BudgetGauge").Find("Cost").localScale;
		tempScale.x = 0.7f;
		Footer.transform.Find ("Budget").Find ("BudgetGauge").Find ("Cost").localScale = tempScale;

		//set BudgetGauge Remaining  (scale x)
		tempScale = Footer.transform.Find("Budget").Find("BudgetGauge").Find("Remaining").localScale;
		tempScale.x = 0.5f;
		Footer.transform.Find ("Budget").Find ("BudgetGauge").Find ("Remaining").localScale = tempScale;

		//set BudgetGauge Emboss  (scale x)
		Footer.transform.Find ("Budget").Find ("BudgetGauge").Find ("Emboss").localScale = tempScale;


		//if project is "realized"
		//show both buttons
		Footer.transform.Find ("Buttons").gameObject.SetActive (true);
		Footer.transform.Find ("RealizeButton").gameObject.SetActive (false);
		
		//else
		//show realisation button
		Footer.transform.Find("Buttons").gameObject.SetActive (false);
		Footer.transform.Find ("RealizeButton").gameObject.SetActive (true);


	}

	private void DoVote(bool isFor){
        _postUserProjectVoteService.idProject = CurrentProjectData.idLabel;
        if (isFor)
            _postUserProjectVoteService.userVote = 1;
        else
            _postUserProjectVoteService.userVote = -1;

        _postUserProjectVoteService.UseWebService();
	}

	public void ShowglobalError(){
		GlobalErrorManager.gameObject.SetActive (true);
		GlobalErrorManager.OpenGlobalErrorPanel();
	}

	private void ResetMenus(){
		foreach (GameObject go in Menu) {
			go.SetActive(false);
		}
		foreach (GameObject go in OtherMenu) {
			go.SetActive(false);
		}
	}

    private void UpdateVoteUI(int uservote) {
        Transform ButtonFor = ProjectVote.transform.Find("Bts").Find("ForBt");
        Transform ButtonAgainst = ProjectVote.transform.Find("Bts").Find("AgainstBt");

        if (GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>().idUser != -1) {

            //if user already voted
            if (uservote != 0) {
                //if voted yes
                if (uservote == 1) {
                    //Grey out 'vote yes' button
                    ButtonFor.GetComponent<CanvasGroup>().alpha = 0.3f;
                    //Lighten up 'vote no' button
                    ButtonAgainst.GetComponent<CanvasGroup>().alpha = 1.0f;
                    //disable 'vote yes' button
                    ButtonFor.GetComponent<Button>().interactable = false;
                    //Activate 'vote no' button
                    ButtonAgainst.GetComponent<Button>().interactable = true;
                    ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => DoVote(false));
                }

                //if voted no
                if (uservote == -1) {
                    //Grey out "vote no" button
                    ButtonAgainst.GetComponent<CanvasGroup>().alpha = 0.3f;
                    //Lighten up "vote yes" button
                    ButtonFor.GetComponent<CanvasGroup>().alpha = 1.0f;
                    //disable "vote no" button
                    ButtonAgainst.GetComponent<Button>().interactable = false;
                    //Activate "vote yes" button
                    ButtonFor.GetComponent<Button>().interactable = true;
                    ButtonFor.GetComponent<Button>().onClick.AddListener(() => DoVote(true));
                }
            }
            else {
                ButtonFor.GetComponent<Button>().onClick.AddListener(() => DoVote(true));
                ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => DoVote(false));

				//reset all
				ButtonAgainst.GetComponent<CanvasGroup>().alpha = 1.0f;
				ButtonFor.GetComponent<CanvasGroup>().alpha = 1.0f;
				ButtonAgainst.GetComponent<Button>().interactable = true;
				ButtonFor.GetComponent<Button>().interactable = true;
            }

        }
        else {
            ButtonFor.GetComponent<Button>().onClick.AddListener(() => ShowglobalError());
            ButtonAgainst.GetComponent<Button>().onClick.AddListener(() => ShowglobalError());
        }
    }

    // WEB SERVICES CALLBACKS
    private void _postUserProjectVoteService_onError(long status, string message) {
//        Debug.Log("User vote project error");
    }

    private void _postUserProjectVoteService_onComplete(long status, string message) {
//        Debug.Log("User vote project OK");
        UpdateVoteUI(_postUserProjectVoteService.userVote);
    }


    private void _getUserProjectVoteService_onError(long status, string message) {
//        Debug.Log("GetUserProjectVote error");
    }

    private void _getUserProjectVoteService_onComplete(long status, string message) {
        UpdateVoteUI(_getUserProjectVoteService.uservote);
    }

	private void OnDestroy()
	{
		if (this == _instance)
			_instance = null;
	}
}
