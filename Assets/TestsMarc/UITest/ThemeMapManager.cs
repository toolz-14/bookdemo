﻿using UnityEngine;
using System.Collections;

public class ThemeMapManager : MonoBehaviour {

	public ThemeUIManager ThemePanel;
	public ProfileUIManager ProfileUIManager;

	private void Start () {
	
	}

	public void LauchMap(){
		//close ui
		if(ProfileUIManager.IsOpen){
			ProfileUIManager.CloseProfilePanel();
		}
		if (ThemePanel != null) {
			ThemePanel.CloseThemePanel ();
		}
//		GetComponent<Toolz.Triggers.TransitionTriggerIcade> ().TriggerTransition ();
	}
}
