﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControlsPanel : MonoBehaviour {

	public ControlsPanelButton ControlsPanelButton;
	public Sprite ControlsTouch;
	public Sprite ControlsNotTouch;
	public Sprite SecondImage;
	public GameObject Overlay;
//	public LanguagePanel LanguagePanel;

	private Vector3 _from1;
	private Vector3 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.1f;
	
	private Vector3 _from2;
	private Vector3 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.1f;

	private bool isOpen = false;
	private Sprite FirstImage;
	private bool isfirstImage = false;
	
	private void Start () {

	}
	
	private void Update () {
		if(_launch){
			OpenControlsPanel();
		}
		if(_launch2){
			CloseControlsPanel();
		}
	}

	private void SetControlSprite(){
#if UNITY_ANDROID || UNITY_IOS || WIN_TOUCH
		FirstImage = ControlsTouch;
#else
		FirstImage = ControlsNotTouch;
#endif
		transform.Find("ImageBg").Find("Image").GetComponent<Image>().sprite = FirstImage;
		isfirstImage = true;
	}

	public void SetControlPanelActif(){
		SetControlSprite ();
		isOpen = !isOpen;
		if (isOpen) {
			Overlay.SetActive(true);
			StartOpenControlsPanel();
//			//if language still open, close it
//			if(LanguagePanel.GetIsOpen()){
//				LanguagePanel.SetControlPanelClosed();
//			}
		} else {
			Overlay.SetActive(false);
			StartCloseControlsPanel();
		}
	}

	public void SwapImages(){
		isfirstImage = !isfirstImage;
		if (isfirstImage) {
			if (FirstImage != null) {
				transform.Find ("ImageBg").Find ("Image").GetComponent<Image> ().sprite = FirstImage;
			}
		} else {
			if (SecondImage != null) {
				transform.Find ("ImageBg").Find ("Image").GetComponent<Image> ().sprite = SecondImage;
			}
		}
	}

	public bool GetIsOpen(){
		return isOpen;
	}

	public void SetControlPanelClosed(){
		isOpen = false;
		ControlsPanelButton.isOpen = false;
		ControlsPanelButton.ControlsPanelIsClosed();
		Overlay.SetActive (false);
		StartCloseControlsPanel ();
	}

	public void DeactivateControlPanel(){
		Invoke ("ControlPanelSetInactive", 0.45f);
	}
	
	private void ControlPanelSetInactive(){
		gameObject.SetActive (false);
	}

	private void StartOpenControlsPanel(){
		_timeSinceTransitionStarted = 0f;
		_from1 = Vector3.zero;
		_to1 = new Vector3(1f, 1f, 1f);
		_launch = true;
	}
	
	private void OpenControlsPanel(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<RectTransform>().localScale = Vector3.Lerp(_from1, _to1, _t);
		if(Vector3.Distance(GetComponent<RectTransform>().localScale, _to1) < 0.01f){
			_launch = false;
		}
	}
	
	private void StartCloseControlsPanel(){
		_timeSinceTransitionStarted2 = 0f;
		_from2 = new Vector3(1f, 1f, 1f);
		_to2 = Vector3.zero;
		_launch2 = true;
	}
	
	private void CloseControlsPanel(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		GetComponent<RectTransform>().localScale = Vector3.Lerp(_from2, _to2, _t2);
		if(Vector3.Distance(GetComponent<RectTransform>().localScale, _to2) < 0.01f){
			_launch2 = false;
		}
	}
}
