﻿using UnityEngine;
using System.Collections;

public class UIBarDiscussionStats : MonoBehaviour {

	[HideInInspector]
	public float AmountOfComments = 0;
	[HideInInspector]
	public float nbForVotes = 0;
	[HideInInspector]
	public float nbAgainstVotes = 0;

	private void Start () {
		
	}
}
