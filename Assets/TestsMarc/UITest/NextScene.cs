using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Toolz.WebServices;

public class NextScene : MonoBehaviour {

	public InputField inputField;
	public Text messageToUser;
	public GameObject button_Go;
	public GameObject button_Exit;
	public GameObject button_TryAgain;

    private Toolz.WebServices.ApplicationLogin checkUserService;
//	private GameObject splashPanel;
	private AppLoader LoadPanel;
	private Toggle toggle;

	// Regular expression, which is used to validate an E-Mail address.
	public const string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
			+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
			+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
			+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

//	public bool IsOpen{
//		get { return splashPanel.GetComponent<Animator>().GetBool("isOpen"); }
//		set { splashPanel.GetComponent<Animator>().SetBool("isOpen", value); }
//	}


	private void Awake() {
        checkUserService = GameObject.FindWithTag("WebServices").GetComponent<Toolz.WebServices.ApplicationLogin>();
        checkUserService.onComplete += checkUserService_onComplete;
		 
		//load the last email entered if any
		if(SaveLoadManager.LoadLastEmailEntered() != null){
			inputField.text = SaveLoadManager.LoadLastEmailEntered();
		}
//		splashPanel = transform.parent.FindChild("Panel_splash").gameObject;
		toggle = transform.Find("Toggle_NDA").GetComponent<Toggle>();

		LoadPanel = GameObject.Find("Canvas_Loader").transform.Find("Panel_Load").GetComponent<AppLoader>();
    }

	private void Start () {
		
	}


    public void CheckUser(){
		//Debug.Log("boutton clicked");
		//clean message text
		messageToUser.text = "";

		//check if NDA has been accepted
		if(!toggle.isOn){
			messageToUser.text = "Vous devez accepter le NDA !";
		}else{
			if(IsEmail(inputField.text)){
				SaveLoadManager.SaveLastEmailEntered(inputField.text);
				//checkUserService.userMail = inputField.text;
				checkUserService.UseWebService();
			}else{
				//Debug.Log("Not an email adress");
				//message please input an email adress
				messageToUser.text = "Il semble qu'il y ait une erreur \n de saisie dans votre email";
			}
		}
    }

    private void checkUserService_onComplete(long satus, string message) {
        //Debug.Log("Service completed");
        if (satus == (long)WebService.Status.OK){
			//Debug.Log("Service completed satus = OK");
            if (checkUserService.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_BANNED) {
				//Debug.Log("userStatus == USER_BANNED");
                //message banned plus exit app(button text == exit)
				messageToUser.text = "vous n'avez pas le droit d'acceder a l'application.\n Veuillez contacter Toolz";
				SwapButtons();
            }
            else if (checkUserService.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_NEED_VALIDATION) {
				//Debug.Log("userStatus == USER_NEED_VALIDATION");
				//message need validation plus exit app(button text == exit)
				messageToUser.text = "Votre demande d'accès a bien été recue et est en attente de validation.\n Veuillez verifier vos emails";
				SwapButtons();
            }
            else if (checkUserService.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_OK || checkUserService.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) {
				//Debug.Log("userStatus == USER_OK");
				//no message and go to splash
				//IsOpen = true;

				//alpha out
				StartCoroutine("AlphaOut");
            }
			else{
				//Debug.Log("userStatus not recognised or NULL");
			}
        }
		else{
			//Debug.Log("Service completed satus = NOT OK");
		}
    }


	// Checks whether the given Email-Parameter is a valid E-Mail address.
	// <param name="email">Parameter-string that contains an E-Mail address
	// <returns>True, when Parameter-string is not null and contains a valid E-Mail address, otherwise false.
	public static bool IsEmail(string email){
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}

	public void ExitApp(){
		Application.Quit();
	}

	private void SwapButtons(){
		button_Go.SetActive(false);
		button_Exit.SetActive(true);
		button_TryAgain.SetActive(true);
	}

	private void StartAlphaOut(){

	}

	IEnumerator AlphaOut(){
		yield return new WaitForSeconds (0.1f);
		if(GetComponent<CanvasGroup>() != null){
			if(GetComponent<CanvasGroup>().alpha > 0){
				GetComponent<CanvasGroup>().alpha -= 0.1f;
				StartCoroutine("AlphaOut");
			}else{
				StopCoroutine("AlphaOut");
				//LoadPanel.LauchCoroutineLoad();
			}
		}
	}
}
