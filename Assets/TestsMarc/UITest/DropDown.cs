﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropDown : MonoBehaviour {

	public GameObject Holder;
	public List<GameObject> LanguageButtonList = new List<GameObject>();
	public float Duration = 0.01f;

	float _timeSinceTransitionStarted = 0;
	float _t = 0;
	bool _ShouldLerpOpen;
	bool _ShouldLerpClose;
	float currentYPos;
	float nextYPos;
	Vector2 temp;
	int startIncValue = 1;
	[HideInInspector]
	public bool isOpen;
	[HideInInspector]
	public bool buttonIsBlocked;

	private void Awake () {

	}

	private void Start () {
	
	}

	private void Update () {
		if(_ShouldLerpOpen){
			LerpButtonsOpen();
		}
		if(_ShouldLerpClose){
			LerpButtonsClose();
		}
	}

	public void AnimateButtonList(){
		if (!buttonIsBlocked) {
			buttonIsBlocked = true;
			if (isOpen) {
				AnimateButtonClose ();
			} else {
				AnimateButtonOpen ();
			}
			isOpen = !isOpen;
		}
	}

	private void AnimateButtonOpen(){

		Holder.SetActive (true);

		//all buttons at 1st pos
		for(int i=0; i<LanguageButtonList.Count; i++){
			temp = LanguageButtonList[i].GetComponent<RectTransform>().anchoredPosition;
			temp.y = 36.5f;
			LanguageButtonList[i].GetComponent<RectTransform>().anchoredPosition = temp;
		}
		startIncValue = 1;
		currentYPos = 36.5f;
		nextYPos = currentYPos - 35.6f;
		_ShouldLerpOpen = true;
	}

	private void LerpButtonsOpen(){

		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration;

		for (int i=startIncValue; i<LanguageButtonList.Count; i++) {
			temp = LanguageButtonList[i].GetComponent<RectTransform>().anchoredPosition;
			temp.y = Mathf.Lerp (currentYPos, nextYPos, _t);
			LanguageButtonList[i].GetComponent<RectTransform>().anchoredPosition = temp;
		}
		if( Mathf.Abs(temp.y - nextYPos) < 0.001f){
			_ShouldLerpOpen = false;
			if(startIncValue<LanguageButtonList.Count){
				PrepareNextLerpOpen();
			}
		}else{
			if(startIncValue >= LanguageButtonList.Count){
				buttonIsBlocked = false;
				_ShouldLerpOpen = false;
			}
		}
	}

	private void PrepareNextLerpOpen(){
		_timeSinceTransitionStarted = 0;
		startIncValue++;
		currentYPos = nextYPos;
		nextYPos = currentYPos - 35.6f;
		_ShouldLerpOpen = true;
	}

	public void AnimateButtonClose(){
		_timeSinceTransitionStarted = 0;
		currentYPos = LanguageButtonList[LanguageButtonList.Count-1].GetComponent<RectTransform>().anchoredPosition.y;
		nextYPos = currentYPos + 35.6f;
		startIncValue = LanguageButtonList.Count-1;
		_ShouldLerpClose = true;
	}

	private void LerpButtonsClose(){
		
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration;

		for (int i=startIncValue; i<LanguageButtonList.Count; i++) {
			temp = LanguageButtonList[i].GetComponent<RectTransform>().anchoredPosition;
			temp.y = Mathf.Lerp (currentYPos, nextYPos, _t);
			LanguageButtonList[i].GetComponent<RectTransform>().anchoredPosition = temp;
		}
		if( Mathf.Abs(temp.y - nextYPos) < 0.001f){
			_ShouldLerpClose = false;

			if(startIncValue>0){
				PrepareNextLerpClose();
			}
		}else{
			if(startIncValue <= 0){
				buttonIsBlocked = false;
				_ShouldLerpClose = false;
				Holder.SetActive (false);
			}
		}
	}
	
	private void PrepareNextLerpClose(){
		_timeSinceTransitionStarted = 0;
		startIncValue--;
		currentYPos = nextYPos;
		nextYPos = currentYPos + 35.6f;
		_ShouldLerpClose = true;
	}
}
