﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputfieldFocused : MonoBehaviour {

	private InputField _inputField;


	private void Awake(){
		_inputField = GetComponent<InputField>();
	}
	
	private void Start () {
		_inputField.shouldHideMobileInput = true;
	}

	private void OnEnable(){
		_inputField.ActivateInputField ();
	}
}
