﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Toolz.WebServices;

public class ResponseInputUIManager : MonoBehaviour {

	public Animation _responseView;
	public GameObject Bg;
	public Text inputedText;
	public GameObject Panel_KeyboardInput;
    public DiscussionUIManagerLabel Panel_DiscussionUIManager;
    public ResponseUIManager Panel_ResponseUIManager;
    [SerializeField]
    private GameObject panelDisplayMessage;
	[SerializeField]
	private GameObject ValidationPopUp;

    //Is this ResponseInputUIManager used to send a comment or a reply?
    private enum InputMode { COMMENT, REPLY };
    private InputMode _inputMode;

    private PostUserComment _postUserCommentService;
    private int idLabel;
    private PostUserReply _postUserReplyService;
    private int idComment;
    private int idAnsweredReply;

    private bool webservicesStarted = false;

    private void Awake(){
		
	}

	private void Start () {
        SetWebservices();
    }

    private void SetWebservices()
    {
        if (!webservicesStarted)
        {
            webservicesStarted = true;
            GameObject webservices = GameObject.FindGameObjectWithTag("WebServices");

            _postUserCommentService = webservices.GetComponent<PostUserComment>();
            _postUserCommentService.onComplete += _postUserCommentService_onComplete;
            _postUserCommentService.onError += _postUserCommentService_onError;

            _postUserReplyService = webservices.GetComponent<PostUserReply>();
            _postUserReplyService.onComplete += _postUserReply_onComplete;
            _postUserReplyService.onError += _postUserReply_onError;
        }
    }

	public void OpenResponseInputPanel(){
		inputedText.text = Panel_KeyboardInput.transform.Find ("Empty_KeyboardInput").Find ("InputField").Find ("Text").GetComponent<Text> ().text;

		_responseView.gameObject.SetActive (true);
		_responseView.GetComponent<Animation>()["ResponseInputUISlideIn"].speed = 1f;
		
		_responseView.Play("ResponseInputUISlideIn");

		Bg.SetActive (true);
		Panel_KeyboardInput.SetActive (false);
	}
	
	public void CloseResponseInputPanel(){
		_responseView.GetComponent<Animation>()["ResponseInputUISlideIn"].time = _responseView.GetComponent<Animation>()["ResponseInputUISlideIn"].length;
		_responseView.GetComponent<Animation>()["ResponseInputUISlideIn"].speed = -1f;
		
		_responseView.Play("ResponseInputUISlideIn");

		InvokeRepeating ("DetectAnimationEnd", 0f, 0.1f);
	}

	public void SetLabelText(string newText){
		transform.Find ("ResponseInputView").Find ("Header").Find ("Label").GetComponent<Text> ().text = newText;
	}

	private void DetectAnimationEnd(){
		if(_responseView.GetComponent<Animation>()["ResponseInputUISlideIn"].time <= 0f){
			Bg.SetActive (false);
			CancelInvoke("DetectAnimationEnd");
			_responseView.gameObject.SetActive (false);
		}
    }
    public void SetidLabel(int idLabel)
    {
        this.idLabel = idLabel;
    }

    public void PrepareToComment(int idLabel) {
        _inputMode = InputMode.COMMENT;
        this.idLabel = idLabel;
    }

    public void PrepareToReply(int idComment, int idAnsweredReply) {
        Debug.Log("Prepare to reply");
        _inputMode = InputMode.REPLY;
        this.idComment = idComment;
        this.idAnsweredReply = idAnsweredReply;
    }

	//FOR JN: NEED TO UPLOAD THE COMMENT/RESPONSE BEFORE CLOSING THE PANEL
	public void PublishResponse(){

        if (_inputMode == InputMode.COMMENT) {
            if(idLabel!= -1)
            {
                _postUserCommentService.idLabel = idLabel;
                _postUserCommentService.commentText = inputedText.text;
                _postUserCommentService.request_status = 0;
                _postUserCommentService.UseWebService();
            }
            else
            {
                panelDisplayMessage.SetActive(true);
                panelDisplayMessage.transform.Find("TextError").GetComponent<Text>().text = "Le label choisi n'est pas enregistré en ligne, veuillez l'enregistrer en ligne";
            }
        }
        else if (_inputMode == InputMode.REPLY)
        {
            if (idLabel != -1)
            {
                _postUserReplyService.idComment = idComment;
                _postUserReplyService.idAnsweredReply = idAnsweredReply;
                _postUserReplyService.text = inputedText.text;
                _postUserReplyService.request_status = 0;

                _postUserReplyService.UseWebService();
            }
            else
            {
                panelDisplayMessage.SetActive(true);
                panelDisplayMessage.transform.Find("TextError").GetComponent<Text>().text = "Le label choisi n'est pas enregistré en ligne, veuillez l'enregistrer en ligne";
            }
        }
		CloseResponseInputPanel ();
	}

    // ADMIN VALIDATION
    public void UpdateComment(int idAuthor, string msg, bool isDeleted)
    {
        SetWebservices();
        _postUserCommentService.idLabel = this.idLabel;
        _postUserCommentService.idAuthor = idAuthor; 
        _postUserCommentService.commentText = msg;
        _postUserCommentService.isDeleted = isDeleted;

        if (isDeleted)
            _postUserReplyService.request_status = 1;
        else
            _postUserReplyService.request_status = 2;

        _postUserCommentService.UseWebService();
    }


    public void UpdateReply(int idAuthor, int idComment,int idReply, int idAnsweredReply, string msg, bool isDeleted)
    {
        SetWebservices();
        _postUserReplyService.idComment = idComment;
        _postUserReplyService.idReply = idReply;
        _postUserReplyService.idAuthor = idAuthor;
        _postUserReplyService.idAnsweredReply = idAnsweredReply;
        _postUserReplyService.isDeleted = isDeleted;
        _postUserReplyService.text = msg;

        if (isDeleted)
            _postUserReplyService.request_status = 1;
        else
            _postUserReplyService.request_status = 2;

        _postUserReplyService.UseWebService();
    }

    private void _postUserReply_onError(long status, string message) {
        Debug.Log("Post User Reply ERROR : " + message);
    }

    private void _postUserReply_onComplete(long status, string message) {
        Debug.Log("Post User Reply OK");

        if (_postUserReplyService.request_status == 0)
        {
            Panel_ResponseUIManager.gameObject.SetActive(true);
            Panel_ResponseUIManager.OpenResponsePanel((uint)_postUserReplyService.idComment);
            Panel_DiscussionUIManager.UpdateComment(_postUserReplyService.idComment, _postUserReplyService.nbReplies);

            ValidationPopUp.SetActive(true);
            ValidationPopUp.GetComponent<CommentValidationPopUpPanel>().StartOpenPanel();
        }
        else
        {
            if (_postUserReplyService.request_status == 1) //deleted
            {
                Panel_DiscussionUIManager.UpdateComment(_postUserReplyService.idComment, _postUserReplyService.nbReplies);
            }
        }

    }

    private void _postUserCommentService_onError(long status, string message) {
        Debug.Log("Post user comment ERROR : " + message);
    }

    private void _postUserCommentService_onComplete(long status, string message) {
        Debug.Log("Post user comment OK : " + message);
        if(_postUserCommentService.request_status == 0)
        {
            Panel_DiscussionUIManager.AddCommentToList(_postUserCommentService.comment);
            ValidationPopUp.SetActive(true);
            ValidationPopUp.GetComponent<CommentValidationPopUpPanel>().StartOpenPanel();
        }
		
    }
}
