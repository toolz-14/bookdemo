﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Toolz.WebServices;

public class ProfileUIManager : MonoBehaviour {
    private static bool userInit = false;

    public GameObject NotLoggedInView;
    public GameObject ConnectionView;
    public GameObject RegisterView;
    public GameObject ProfileView;
	public GameObject ExitButton;
	public GameObject BackButton;
	public GameObject Footer;
	public GameObject ButtonDeconnect;
	public GameObject ButtonModeration;
	public GameObject FollowedDiscussionItem;
	public Transform FollowedDiscussionList;
	public Text Title;
	public GameObject UserInfo;
	public static bool IsOpen;

//	public ConnectionUIManager ConnectionPanel;
	//public AnimateHolder animateHolder;
	public CanvasCalqueBagneux canvasCalqueBagneux;
//	public LanguagePanel LanguagePanel;
	public OptionsDisplayManager OptionsDisplayManager;
	public DiscussionUIManagerLabel DiscussionManager;
	public ChatUIManagerLabel ChatUIManagerLabel;
    public SelectAndSaveLoadLabelsOnline selectAndSaveLoadLabelsOnline;

    // InputField du Register
    public InputField userLastname;
    public InputField userFirstname;
    public InputField userLogin;
    public InputField userMail;
    public InputField userPassword;

    // InputField du Login
    public GameObject LoginFormError;
    public InputField LoginUserName;
	public InputField LoginPassword;

    private Text loginFormErrorTxt;
    private GameObject _tempObject;
	private Vector2 _tempVect2;

    private Toolz.WebServices.RegisterUser _registerUserService;
    private Toolz.WebServices.UserLogin _userLoginService;
    // private Toolz.WebServices.UserGetFollowedProjects _userGetFollowedProjects;
    public enum View: int{ NOTLOGGEDIN = 0, LOGIN = 1, REGISTER = 2, PROFILE = 3};

    private Toolz.WebServices.User user;
    public delegate void LabelEditorSaveLoadDelegate(); // call to LabelEditorSaveLoad delegate
    public static LabelEditorSaveLoadDelegate labelEditorSaveLoadDelegate; // instance

    [SerializeField]
    private ShowHideChatLabelsZAC showHideChatLabelsZAC;

    private void Awake() {
        GameObject webService = GameObject.FindGameObjectWithTag("WebServices");
        _registerUserService = webService.GetComponent<Toolz.WebServices.RegisterUser>();
        _userLoginService = webService.GetComponent<Toolz.WebServices.UserLogin>();

        _registerUserService.onComplete += _registerUserService_onComplete;
        _registerUserService.onError += _registerUserService_onError;

        _userLoginService.onComplete += _userLoginService_onComplete;
        _userLoginService.onError += _userLoginService_onError;

        user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>();

        IsOpen = false;
    }

	private void Start () {

        ButtonDeconnect.GetComponent<Button>().onClick.AddListener(() => GameObject.FindGameObjectWithTag("WebServices").GetComponent<User>().LogOff());
        ButtonDeconnect.GetComponent<Button>().onClick.AddListener(() => RemovePP());
        ButtonDeconnect.GetComponent<Button>().onClick.AddListener(() => selectAndSaveLoadLabelsOnline.UnDisplayLabelButton());
//		if (ConnectionPanel != null) {
//			ButtonDeconnect.GetComponent<Button> ().onClick.AddListener (() => ConnectionPanel.SetUserName ("Non connecté"));
//		}
        ButtonDeconnect.GetComponent<Button>().onClick.AddListener(() => ProfileView.SetActive(false));
		ButtonDeconnect.GetComponent<Button>().onClick.AddListener(() => NotLoggedInView.SetActive(true));
		ButtonDeconnect.GetComponent<Button>().onClick.AddListener(() => ButtonModeration.SetActive (false));
	}

	private void SelectViewToShow ()
    {
        //if logged in show user profile
        if (user.idUser != -1) // L'utilisateur est déjà loggué (soit hors connexion ou en ligne)
        {
            FillUserInfo(user.userName, user.firstname, user.login, user.mail);
            SwapView ((int)View.PROFILE);
			//if show user profile, get followed discussions to show
			//_userGetFollowedProjects.UseWebService();
			//FillFollowedDiscussionList();
		} else {
			SwapView ((int)View.NOTLOGGEDIN);
		}
	}

	private void OnDisable(){
		RegisterView.transform.Find("Container").GetComponent<ProfileRegister> ().OnExitProfile ();
	}

	public void SwapView(int view){
		//disable all views
        NotLoggedInView.SetActive(false);
        ConnectionView.SetActive(false);
        RegisterView.SetActive(false);
        ProfileView.SetActive(false);
        
		//show relevant view
		switch((View)view){
		case View.NOTLOGGEDIN: NotLoggedInView.SetActive(true);
			ExitButton.SetActive(true);
			BackButton.SetActive(false);
                Footer.SetActive(false);
                Title.text = "Identification";
			break;
		case View.LOGIN: ConnectionView.SetActive(true);
            //load the username entered if any
            if (SaveLoadManager.LoadLastUserNameEntered() != null)
            {
                LoginUserName.text = SaveLoadManager.LoadLastUserNameEntered();
            }
            if (SaveLoadManager.LoadLastPwdNameEntered() != null)
            {
                LoginPassword.text = SaveLoadManager.LoadLastPwdNameEntered();
            }
            ExitButton.SetActive(false);
			BackButton.SetActive(true);
                Footer.SetActive(false);
                Title.text = "Connexion";
            LoginFormError.SetActive(true);
            loginFormErrorTxt = LoginFormError.transform.Find("Flag").Find("Caption").GetComponent<Text>();
            LoginFormError.SetActive(false);
            break;
		case View.REGISTER: RegisterView.SetActive(true);
			ExitButton.SetActive(false);
			BackButton.SetActive(true);
                Footer.SetActive(false);
                Title.text = "Creation de Profil";
			break;
		case View.PROFILE: ProfileView.SetActive(true);
			ExitButton.SetActive(true);
			BackButton.SetActive(false);
			Footer.SetActive(true);
			Title.text = "Mon Profil";
			break;
		default:;
			break;
		}

		Title.transform.GetComponent<Lean.LeanLocalizedText>().PhraseName = Title.text;
		Title.transform.GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
	}

	public void OpenProfilePanel(){
		IsOpen = true;

		SelectViewToShow ();
		GetComponent<Animation>().GetComponent<Animation>()["ProfileUISlideIn"].speed = 1f;
		
		GetComponent<Animation>().Play("ProfileUISlideIn");

		//close ui
//		if (ConnectionPanel != null) {
//			ConnectionPanel.CloseConnectionPanel ();
//		}
		if (OptionsDisplayManager != null) {
			OptionsDisplayManager.ForceCloseOptionsPanel ();
		}
//		if (LanguagePanel != null && LanguagePanel.GetIsOpen()) {
//			LanguagePanel.SetControlPanelClosed ();
//		}
		if (ChatUIManagerLabel != null && ChatUIManagerLabel.IsOpen) {
			ChatUIManagerLabel.CloseChatPanel ();
		}

		if (canvasCalqueBagneux != null) {
			canvasCalqueBagneux.ClosePanelFromEcole ();
		}

		//animateHolder.CloseAllCommentPanels ();
	}
	
	public void CloseProfilePanel(){
		if(IsOpen){
			IsOpen = false;

			GetComponent<Animation>().GetComponent<Animation>()["ProfileUISlideIn"].time = GetComponent<Animation>().GetComponent<Animation>()["ProfileUISlideIn"].length;
			GetComponent<Animation>().GetComponent<Animation>()["ProfileUISlideIn"].speed = -1f;
			
			GetComponent<Animation>().Play("ProfileUISlideIn");

			if (canvasCalqueBagneux != null) {
				canvasCalqueBagneux.OpenPanelFromEcole ();
			}
		}
	}

	public void DeactivateProfilePanel(){
		Invoke ("ProfilePanelSetInactive", 0.45f);
	}

	private void ProfilePanelSetInactive(){
		gameObject.SetActive (false);
	}

    // Bouton Ok register
    public void RegisterUser() {        
        _registerUserService.firstname = userFirstname.text;
        _registerUserService.login = userLogin.text;
        _registerUserService.mail = userMail.text;
        _registerUserService.password = userPassword.text;
        _registerUserService.lastname = userLastname.text;
        
        _registerUserService.UseWebService();
    }

    // Bouton Ok Login
    public void LogUser() {
        _userLoginService.login = LoginUserName.text;
        _userLoginService.password = LoginPassword.text;
        _userLoginService.UseWebService();
    }

    // Remove some of the Player Prefs
    public void RemovePP()
    {
        SaveLoadManager.SaveUserID(-1);
        SaveLoadManager.SaveLogin("");
        SaveLoadManager.SaveLastname("");
        SaveLoadManager.SaveFirstname("");
        SaveLoadManager.SaveMail("");
        SaveLoadManager.SavePassword("");
        SaveLoadManager.SaveAccountStatus(-1);
    }

    // WEB SERVICES CALLBACKS
    private void _registerUserService_onError(long status, string message) {
        Debug.Log("Register user error status = "+ status+ "  message = " + message);
    }

    private void _registerUserService_onComplete(long status, string message) {
        Debug.Log("Register user success");

        // If the user has internet and got authentication sucessfull, we save login and password into player prefs
        SaveLoadManager.SaveLastUserNameEntered(userLogin.text);
        SaveLoadManager.SaveLastUserPWdEntered(userPassword.text);
        SwapView((int)View.LOGIN);

    }

    private void _userLoginService_onError(long status, string message) {

        if (status.Equals((long)WebService.Status.NO_CONNECTION))
        {// Pas d'internet
            LoginFormError.SetActive(true);
            loginFormErrorTxt.text = "Connexion internet requise";
        }
        else
        {
            LoginFormError.SetActive(true);
            loginFormErrorTxt.text = "Username or password incorrect.";
        }
    }

    private void _userLoginService_onComplete(long status, string message)
    {
        // Sauve les username et mot de passe
        SaveLoadManager.SaveLastUserNameEntered(LoginUserName.text);
        SaveLoadManager.SaveLastUserPWdEntered(LoginPassword.text);

        Toolz.WebServices.User user = GameObject.FindGameObjectWithTag("WebServices").GetComponent<Toolz.WebServices.User>();
        user.idUser = _userLoginService.idUser;
        user.login = _userLoginService.login;
        user.userName = _userLoginService.name;
        user.firstname = _userLoginService.firstname;
        user.mail = _userLoginService.mail;
        user.password = _userLoginService.password;
        user.userStatus = _userLoginService.userStatus;
        

        if ((Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus)user.userStatus == Toolz.WebServices.ApplicationLogin.ApplicationLoginStatus.USER_ADMIN) {
			LabelEditorSaveLoad.isAdmin = true;
			ButtonModeration.SetActive (true);
		} else {
			LabelEditorSaveLoad.isAdmin = false;
			ButtonModeration.SetActive (false);
		}
        labelEditorSaveLoadDelegate();

        FillUserInfo(user.userName, user.firstname, user.login, user.mail);
        SwapView((int)View.PROFILE); ;
        if (ShowHideChatLabelsZAC.isShowing)
            selectAndSaveLoadLabelsOnline.DisplayLabelButton();

    }

    private void _userGetFollowedProjects_onComplete(long status, string message) {
        //FillFollowedDiscussionList();
    }

    private void _userGetFollowedProjects_onError(long status, string message) {
        Debug.Log("Unable to retrieve followed project: " + status + "=>" + message);
    }

    private void FillUserInfo(string name, string firstname, string login, string mail) {
		if(UserInfo != null){
			UserInfo.transform.Find ("GridRightHolder").Find ("UserInfoName").GetComponent<Text> ().text = firstname + " " + name;
			UserInfo.transform.Find ("GridRightHolder").Find ("UserInfoMail").GetComponent<Text> ().text = mail;
			UserInfo.transform.Find ("GridRightHolder").Find ("UserInfoLogin").GetComponent<Text> ().text = login; 
		}
    }

    /*
    private void FillFollowedDiscussionList() {
        int i = 0;

		//reset container pos x and width
		FollowedDiscussionList.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
		FollowedDiscussionList.GetComponent<RectTransform> ().sizeDelta = new Vector2(FollowedDiscussionList.GetComponent<RectTransform> ().sizeDelta.x, 0f);
        
        foreach (string project in _userGetFollowedProjects.FollowedProjects) {
            _tempObject = (GameObject)Instantiate(FollowedDiscussionItem);
            _tempObject.SetActive(true);

			//A REALLY BAD, BAD, UN-OPTIMIZED, WAY OF DOING THIS, WILL HAVE TO CHANGE IT WHENEVER POSSIBLE
			//set text
			Project[] allprojects = GameObject.FindObjectsOfType<Project> ();
			
			foreach (Project pr in allprojects) {
				if(pr.projectId == project){
					_tempObject.transform.Find("Caption").GetComponent<Text>().text = pr.projectName;
				}
			}

            //set button onclick
			//set project data
			_tempObject.GetComponent<Button>().onClick.AddListener(delegate{GameObject.FindGameObjectWithTag("ProjectUIManager").GetComponent<ProjectUIManager>().FindProjectSetDataStruct(project);});
            //open discussion
			_tempObject.GetComponent<Button>().onClick.AddListener(() => DiscussionManager.OpenDiscussionPanel(false));
			//close profile panel
			_tempObject.GetComponent<Button>().onClick.AddListener(() => CloseProfilePanel());

            //set parent
            _tempObject.transform.SetParent(FollowedDiscussionList);

            //set position
            _tempVect2.x = 0;
            _tempVect2.y = i * (-50f);
            _tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
			_tempObject.GetComponent<RectTransform>().offsetMax = new Vector2(0, _tempObject.GetComponent<RectTransform>().offsetMax.y);
            ++i;    
        }
		//augment width of container each time
		FollowedDiscussionList.GetComponent<RectTransform> ().sizeDelta = new Vector2(FollowedDiscussionList.GetComponent<RectTransform> ().sizeDelta.x, ((i+1) * 50f));
    }*/
}
