﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Map3dPanel : MonoBehaviour {

	public RectTransform Container_Carto;

	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;

	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private RectTransform rectTransform;
	private Vector3 _opennedPosition = Vector3.zero;
	private Vector3 _closedPosition = new Vector3(-240f, 0f, 0f);

	private void Start(){

	}

	private void Update () {
		if(_launch){
			LerpPanelOpen();
		}
		if(_launch2){
			LerpPanelBack();
		}
	}

	public void StartLerpPanelOpen(){
		rectTransform = GetComponent<RectTransform> ();
		_timeSinceTransitionStarted = 0f;
		_from1 = rectTransform.anchoredPosition;
		_to1 = _opennedPosition;
		_launch = true;
	}

	private void LerpPanelOpen(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;

		rectTransform.anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(rectTransform.anchoredPosition, _to1) < 0.01f){
			_launch = false;
		}
	}

	public void StartLerpPanelBack(){
		rectTransform = GetComponent<RectTransform> ();
		_timeSinceTransitionStarted2 = 0f;
		_from2 = rectTransform.anchoredPosition;
		_to2 = _closedPosition;
		_launch2 = true;
	}

	private void LerpPanelBack(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;

		rectTransform.anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(rectTransform.anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
		}
	}

	public void SetPanelData(){
		if (Container_Carto != null) {
			Container_Carto.gameObject.SetActive (true);
		}
	}
}
