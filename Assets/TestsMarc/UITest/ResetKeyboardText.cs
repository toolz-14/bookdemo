﻿using UnityEngine;
using UnityEngine.UI;

public class ResetKeyboardText : MonoBehaviour {

	[SerializeField]
	private InputField inputField;

	private void OnEnable () {
		inputField.text = "";
	}
}