﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConnectionUIManager : MonoBehaviour {

	private Text _displayedText;

	private void Awake(){
		_displayedText = transform.Find ("Text_UserName").GetComponent<Text> ();
	}

	private void Start () {
        SetUserName("Non connecté");
        OpenConnectionPanel ();
	}
	
	public void OpenConnectionPanel(){
		GetComponent<Animation>().GetComponent<Animation>()["ConnectionUISlideIn"].speed = 1f;
		
		GetComponent<Animation>().Play("ConnectionUISlideIn");
	}
	
	public void CloseConnectionPanel(){
		GetComponent<Animation>().GetComponent<Animation>()["ConnectionUISlideIn"].time = GetComponent<Animation>().GetComponent<Animation>()["ConnectionUISlideIn"].length;
		GetComponent<Animation>().GetComponent<Animation>()["ConnectionUISlideIn"].speed = -1f;
		
		GetComponent<Animation>().Play("ConnectionUISlideIn");
	}

	public void SetUserName(string username){
		_displayedText.text = username;
	}
}
