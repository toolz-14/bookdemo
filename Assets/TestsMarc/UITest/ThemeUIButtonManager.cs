﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThemeUIButtonManager : MonoBehaviour {

	public GameObject ProjectItem;
	public Text ThemeTitle;
	public Image Checkmark;
	public ProjectUIManager ProjectUIManager;
	public ProjectUIManager_Fiches ProjectUIManager_Fiches;
	private GameObject _tempObject;
	private Vector2 _tempVect2;
	[HideInInspector]
	public int IndexInList;
	[HideInInspector]
	public Image ThemeIcon;
	[HideInInspector]
	public Color ThemeColor;
	public CanvasHotspots CanvasHotspots;
	public ThemeUIManager ThemeUIManager;
	private Toggle theToggle;
	private string theThemeName;
	[HideInInspector]
	public List<GameObject> ProjectListDisplay = new List<GameObject>();

	private void Awake () {
		ThemeIcon = transform.Find ("iconBt").Find ("icon").GetComponent<Image> ();
	}
	
	public void SetOnClick(){

		bool active = transform.Find ("ProjectList").gameObject.activeInHierarchy;
		if(!active){
			transform.parent.parent.GetComponent<ThemeUIManager>().DeactivateAllProjectList();
		}
		#if UNITY_IOS || UNITY_ANDROID
		transform.Find ("ProjectList").gameObject.SetActive (true);
		#elif !UNITY_IOS && !UNITY_ANDROID
		transform.Find ("ProjectList").gameObject.SetActive (!active);
		#endif

		SetColortoTheme ();
		//set all other icons to grey
//		foreach(GameObject go in transform.parent.parent.GetComponent<ThemeUIManager>().buttonList){
//			if(go.GetComponent<ThemeUIButtonManager>().IndexInList != IndexInList){
//				go.GetComponent<ThemeUIButtonManager>().ThemeIcon.color = Color.grey;
//			}
//		}
	}

	public void SetOnExitClick(){

	}

	public void SetOnProj(){

	}

	public void SetOnExitProj(){
		Reset ();
	}

	private void Reset(){
		transform.parent.parent.GetComponent<ThemeUIManager>().DeactivateAllProjectList();
		transform.parent.parent.GetComponent<ThemeUIManager> ().SetAllButtonColorToTheme ();
	}

	public void SetColortoTheme(){
		if(ThemeIcon != null){
			ThemeIcon.color = ThemeColor;
		}
	}

	public void OnToggleClick () {
		theToggle = transform.Find("ProjectList").Find("Header").Find("Background").GetComponent<Toggle> ();
		if (theToggle.isOn) {
			//hide other theme hotspots
			foreach(GameObject go in CanvasHotspots.HsHolderList){
				if (!go.name.Contains (theThemeName)) {
					go.SetActive (false);
				} else {
					go.SetActive (true);
				}
			}
			//unclick all other toggles
			foreach(Toggle tg in ThemeUIManager.toggleList){
				if(tg != theToggle){
					tg.isOn = false;
				}
			}

		} else {
			//show all HS
			if (transform.Find ("ProjectList").gameObject.activeInHierarchy) {
				foreach (GameObject go in CanvasHotspots.HsHolderList) {
					go.SetActive (true);
				}
			}
		}
	}

	private string ThemeFullNameBagneux(string name){

		switch (name) {
		case "vie":
			return "CADRE DE VIE";
		case "social":
			return "SOLIDARITÉ & SOCIAL";
		case "culture":
			return "CULTURE & PATRIMOINE";
		case "education":
			return "EDUCATION ET JEUNESSE";
		case "ville":
			return "VILLE CONNECTÉE";
		case "sport":
			return "SPORT";
		case "transport":
			return "TRANSPORT & MOBILITÉ";
		case "enfance":
			return "PETITE ENFANCE";
		default:
			return "";
		}
	}

	public void SetProjectListData(Color themeColor, string themeName){
		ProjectListDisplay.Clear ();
		theThemeName = themeName;
		//theme name  
		ThemeTitle.text = ThemeFullNameBagneux(themeName);

		//theme color Header ThemeTitle
		ThemeTitle.color = themeColor;

		//theme color Header Background Checkmark
		Checkmark.color = themeColor;

		//list of projects
		int i = 0;
		Project[] allprojects = GameObject.FindObjectsOfType<Project> ();


		//foreach(Project pr in allprojects){
		for(int j=0; j<allprojects.Length; j++){

			if(allprojects[j].ProjectsThemes[themeName]){
				Project tempProject = allprojects[j];

				//add this project to list
				_tempObject = (GameObject)Instantiate(ProjectItem);
				_tempObject.SetActive(true);

				//set project name
				_tempObject.transform.Find("ProjectTitle").GetComponent<Text>().text = tempProject.projectName;
				_tempObject.GetComponent<ThemeProjectDisplayID> ().projectDisplayID = tempProject.projectId;
				//set button to project:

				//get transition
				_tempObject.GetComponent<Button>().onClick.AddListener(() => tempProject.Hotspot.GetComponent<Toolz.Triggers.TransitionTrigger>().TriggerTransition());
				//overlay
				_tempObject.GetComponent<Button>().onClick.AddListener(delegate{ThemeUIManager.Overlay.gameObject.SetActive(false);});
				//set current project data
				_tempObject.GetComponent<Button>().onClick.AddListener(delegate{ProjectUIManager.gameObject.SetActive(true); ProjectUIManager.SetProjectData(tempProject);});
				//get open ui + project exit button 
				_tempObject.GetComponent<Button>().onClick.AddListener(delegate{ProjectUIManager.OpenProjecPanel(tempProject.ExitButton);});
				//close theme project display list
				_tempObject.GetComponent<Button>().onClick.AddListener(() => ThemeUIManager.DeactivateAllProjectList());

				//set parent
				_tempObject.transform.SetParent(transform.Find ("ProjectList"));
				_tempVect2 = _tempObject.GetComponent<RectTransform>().anchoredPosition;
				_tempVect2.y += i*(-44f);
				_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
				_tempObject.GetComponent<RectTransform> ().localScale = Vector3.one;
				ProjectListDisplay.Add (_tempObject);
				i++;
			}
		}

		Project_Fiche[] allprojectFiche = GameObject.FindObjectsOfType<Project_Fiche> ();
		if (allprojectFiche.Length > 0) {
			for(int j=0; j<allprojectFiche.Length; j++){

				if(allprojectFiche[j].ProjectsThemes[themeName]){
					Project_Fiche tempProject = allprojectFiche[j];

					//add this project to list
					_tempObject = (GameObject)Instantiate(ProjectItem);
					_tempObject.SetActive(true);

					//set project name
					_tempObject.transform.Find("ProjectTitle").GetComponent<Text>().text = tempProject.projectName;

					//get transition
					_tempObject.GetComponent<Button>().onClick.AddListener(() => tempProject.Hotspot.GetComponent<TransitionTrigger_Fiches>().TriggerTransition());
					//set current project data
					_tempObject.GetComponent<Button>().onClick.AddListener(delegate{ProjectUIManager_Fiches.gameObject.SetActive(true); ProjectUIManager_Fiches.SetProjectData(tempProject);});
					//get open ui + project exit button 
					_tempObject.GetComponent<Button>().onClick.AddListener(delegate{ProjectUIManager_Fiches.OpenProjecPanel(tempProject.ExitButton);});

					//set parent
					_tempObject.transform.SetParent(transform.Find ("ProjectList"));
					_tempVect2 = _tempObject.GetComponent<RectTransform>().anchoredPosition;
					_tempVect2.y += i*(-44f);
					_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
					_tempObject.GetComponent<RectTransform> ().localScale = Vector3.one;
					i++;
				}
			}
		}
	}
}
