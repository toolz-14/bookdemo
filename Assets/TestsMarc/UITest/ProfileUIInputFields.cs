﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class ProfileUIInputFields : MonoBehaviour {
/*
	public ScrollRect Scroller;
	public GameObject Container;
	private InputField _inputTextFirstName;
	private InputField _inputTextLastName;
	private InputField _inputTextEmail;
	private InputField _inputTextUserName;
	private InputField _inputTextPwd;
	private InputField _inputTextPwdConfirmation;  
	private Vector3 _tempVect3;
	private InputField _currentFocusedInputField;
	private EventSystem _system;

	private void Awake () {
		_inputTextFirstName = transform.FindChild ("InputTextFirstName").FindChild ("Input").GetComponent<InputField>();
		_inputTextLastName = transform.FindChild ("InputTextLastName").FindChild ("Input").GetComponent<InputField>();
		_inputTextEmail = transform.FindChild ("InputTextEmail").FindChild ("Input").GetComponent<InputField>();
		_inputTextUserName = transform.FindChild ("InputTextUserName").FindChild ("Input").GetComponent<InputField>();
		_inputTextPwd = transform.FindChild ("InputTextPwd").FindChild ("Input").GetComponent<InputField>();
		_inputTextPwdConfirmation = transform.FindChild ("InputTextPwdConfirmation").FindChild ("Input").GetComponent<InputField>();
		_currentFocusedInputField = null;
	}

	private void Start () {
		_system = EventSystem.current;
	}

	private void Update () {
		DetectSelected ();
		if(Input.GetKeyDown(KeyCode.Tab)){
			TabToFocusOther();
		}
	}

	private void DetectSelected () {

		_tempVect3 = Vector3.zero;

		if(_system.currentSelectedGameObject == _inputTextFirstName.gameObject){
			_tempVect3.y = 60f;
			Scroller.content.anchoredPosition3D = _tempVect3;
			Scroller.movementType = ScrollRect.MovementType.Clamped;
			_currentFocusedInputField = _inputTextFirstName;
		}else if(_system.currentSelectedGameObject == _inputTextLastName.gameObject){
			_tempVect3.y = 120f;
			Scroller.content.anchoredPosition3D = _tempVect3;
			Scroller.movementType = ScrollRect.MovementType.Clamped;
			_currentFocusedInputField = _inputTextLastName;
		}else if(_system.currentSelectedGameObject == _inputTextEmail.gameObject){
			_tempVect3.y = 180f;
			Scroller.content.anchoredPosition3D = _tempVect3;
			Scroller.movementType = ScrollRect.MovementType.Clamped;
			_currentFocusedInputField = _inputTextEmail;
		}else if(_system.currentSelectedGameObject == _inputTextUserName.gameObject){
			_tempVect3.y = 240f;
			Scroller.content.anchoredPosition3D = _tempVect3;
			Scroller.movementType = ScrollRect.MovementType.Clamped;
			_currentFocusedInputField = _inputTextUserName;
		}else if(_system.currentSelectedGameObject == _inputTextPwd.gameObject){
			_tempVect3.y = 300f;
			Scroller.content.anchoredPosition3D = _tempVect3;
			Scroller.movementType = ScrollRect.MovementType.Clamped;
			_currentFocusedInputField = _inputTextPwd;
		}else if(_system.currentSelectedGameObject == _inputTextPwdConfirmation.gameObject){
			_tempVect3.y = 360f;
			Scroller.content.anchoredPosition3D = _tempVect3;
			Scroller.movementType = ScrollRect.MovementType.Clamped;
			_currentFocusedInputField = _inputTextPwdConfirmation;
		}else{
			//Scroller.movementType = ScrollRect.MovementType.Elastic;
			_currentFocusedInputField = null;
			Container.GetComponent<ProfileRegister>().HideAllFormError();
		}
	}

	private void TabToFocusOther(){
		if(_currentFocusedInputField == null){
			_currentFocusedInputField = _inputTextFirstName;
		}
		_tempVect3 = Vector3.zero;
		Scroller.movementType = ScrollRect.MovementType.Clamped;

		if(_currentFocusedInputField == _inputTextFirstName){

			_system.SetSelectedGameObject(_inputTextLastName.gameObject, new BaseEventData(_system));

			_inputTextLastName.OnPointerClick(new PointerEventData(_system));
			
			_currentFocusedInputField = _inputTextLastName;
			_tempVect3.y = 120f;
			Scroller.content.anchoredPosition3D = _tempVect3;

		}else if(_currentFocusedInputField == _inputTextLastName){

			_system.SetSelectedGameObject(_inputTextEmail.gameObject, new BaseEventData(_system));
			_inputTextEmail.OnPointerClick(new PointerEventData(_system));

			_currentFocusedInputField = _inputTextEmail;
			_tempVect3.y = 180f;
			Scroller.content.anchoredPosition3D = _tempVect3;

		}else if(_currentFocusedInputField == _inputTextEmail){

			_system.SetSelectedGameObject(_inputTextUserName.gameObject, new BaseEventData(_system));
			_inputTextUserName.OnPointerClick(new PointerEventData(_system));

			_currentFocusedInputField = _inputTextUserName;
			_tempVect3.y = 240f;
			Scroller.content.anchoredPosition3D = _tempVect3;

		}else if(_currentFocusedInputField == _inputTextUserName){

			_system.SetSelectedGameObject(_inputTextPwd.gameObject, new BaseEventData(_system));
			_inputTextPwd.OnPointerClick(new PointerEventData(_system));

			_currentFocusedInputField = _inputTextPwd;
			_tempVect3.y = 300f;
			Scroller.content.anchoredPosition3D = _tempVect3;

		}else if(_currentFocusedInputField == _inputTextPwd){


			_system.SetSelectedGameObject(_inputTextPwdConfirmation.gameObject, new BaseEventData(_system));
			_inputTextPwdConfirmation.OnPointerClick(new PointerEventData(_system));

			_currentFocusedInputField = _inputTextPwdConfirmation;
			_tempVect3.y = 360f;
			Scroller.content.anchoredPosition3D = _tempVect3;

		}else if(_currentFocusedInputField == _inputTextPwdConfirmation){

			_system.SetSelectedGameObject(_inputTextFirstName.gameObject, new BaseEventData(_system));
			_inputTextFirstName.OnPointerClick(new PointerEventData(_system));

			_currentFocusedInputField = _inputTextFirstName;
			_tempVect3.y = 60f;
			Scroller.content.anchoredPosition3D = _tempVect3;

		}
	}
	*/
}
