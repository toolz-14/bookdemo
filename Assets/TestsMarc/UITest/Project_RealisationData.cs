﻿using UnityEngine;
using System.Collections;

public class Project_RealisationData : MonoBehaviour {

	public string TitleRealisation;
	
	public string TextRealisation;
	
	//for slider
	[HideInInspector]
	public string SliderCaption;
	[HideInInspector]
	public string SliderUnitDisplay;
	[HideInInspector]
	public float SliderMinValue;
	[HideInInspector]
	public float SliderMaxValue;

	
	private void Start () {
		
	}
}
