﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;

public class StatsUIManager : MonoBehaviour {
	
//	public PieChartTest ThePie;
	public ThemeUIManager ThemePanel;
	public Transform ProjectVoteTogglecontainer;
	public Transform ProjectVoteBarcontainer;
	public Transform DiscussionVoteTogglecontainer;
	public Transform DiscussionVoteBarcontainer;
	public Transform PieCharText;
	public Text PieSectionTitle;
	public Text VoteSectionTitle;
	public GameObject PieCaption;
	public Transform PieChartView;
	public GameObject InvestmentDetailView;
	public GameObject PanelLoader;
	public Transform StatsBudgetView;
	public GameObject BudgetStatListItem;
	public Transform StatsGeneralInfoView;
	public GameObject SimpleListItem;
	
	public GameObject BarRatioItem;
	public GameObject BarDoubleItem;
	public GameObject BarItem;
	public GameObject DoubleBarItem;
	
	private CanvasGroup _canvasGroup;
	private bool isDestroyed;
	private bool isDrawn;
	private Toggle _ratio;
	private Toggle _for;
	private Toggle _against;
	private List<GameObject> ProjectVote_RatioList = new List<GameObject>();
	private List<GameObject> ProjectVote_ForList = new List<GameObject>();
	private List<GameObject> ProjectVote_AgainstList = new List<GameObject>();
	private List<GameObject> PieCaptionList = new List<GameObject>();
	private Toggle _tendance;
	private Toggle _votes;
	private List<GameObject> SetDiscussionVote_TendanceList = new List<GameObject>();
	private List<GameObject> SetDiscussionVote_VotesList = new List<GameObject>();
	private List<GameObject> BudgetStatList = new List<GameObject>();
//	private List<GameObject> InvestmentDetail = new List<GameObject>();
	private Project[] _allprojects;
	private List<float> _userVoteData = new List<float>();
	
	
	private const float _magicPercentMultiplier = 1.5f;
	private const float _magicBarHeightAt0Percent = -180;
	private int _lastIncrementRight = 0;
	private int _k;
	
	private GetAllProjectsVotes _getAllProjectsVotesService;
	private GetNbCommentsPerProject _getNbCommentsPerProjectService;
	private GetNbCommentVotesPerProject _getNbCommentVotesPerProjectService;
	private GetUserProjectVote _getUserProjectVoteService;
	private bool _userProjectVoteServiceActif;
	private bool _startUserProjectVoteService;
	
	[HideInInspector]
	public bool _isAllUsers;
	
	private void Awake () {
		_isAllUsers = true;
		_ratio = ProjectVoteTogglecontainer.Find("Toggle_Ratio").GetComponent<Toggle>();
		_for = ProjectVoteTogglecontainer.Find("Toggle_For").GetComponent<Toggle>();
		_against = ProjectVoteTogglecontainer.Find("Toggle_Against").GetComponent<Toggle>();
		_tendance = DiscussionVoteTogglecontainer.Find("Toggle_Tendance").GetComponent<Toggle>();
		_votes = DiscussionVoteTogglecontainer.Find("Toggle_Votes").GetComponent<Toggle>();
		_userProjectVoteServiceActif = false;
		_startUserProjectVoteService = false;

		GameObject webservice = GameObject.FindGameObjectWithTag("WebServices");
		_getAllProjectsVotesService = webservice.GetComponent<GetAllProjectsVotes>();
		_getAllProjectsVotesService.onComplete += _getAllProjectsVotesService_onComplete;
		_getAllProjectsVotesService.onError += _getAllProjectsVotesService_onError;

		_getNbCommentsPerProjectService = webservice.GetComponent<GetNbCommentsPerProject>();
		_getNbCommentsPerProjectService.onComplete += _getNbCommentsPerProjectService_onComplete;
		_getNbCommentsPerProjectService.onError += _getNbCommentsPerProjectService_onError;

		_getNbCommentVotesPerProjectService = webservice.GetComponent<GetNbCommentVotesPerProject>();
		_getNbCommentVotesPerProjectService.onComplete += _getNbCommentVotesPerProjectService_onComplete;
		_getNbCommentVotesPerProjectService.onError += _getNbCommentVotesPerProjectService_onError;

		_getUserProjectVoteService = webservice.GetComponent<GetUserProjectVote>();
		_getUserProjectVoteService.onComplete += _getUserProjectVoteService_onComplete;
		_getUserProjectVoteService.onError += _getUserProjectVoteService_onError;

		_canvasGroup = transform.Find("HolderToAnimSubPanels").GetComponent<CanvasGroup>();
		SetAllProjects ();

//		StatsPanelSetInactive ();
	}
	
	private void Start () {
		
	}
	
	private void Update () {
		if(_canvasGroup.alpha <= 0 && !isDestroyed){
			isDestroyed = true;
			isDrawn = false;
			DestroyPie();
		}else if(_canvasGroup.alpha >= 1 && !isDrawn){
			isDrawn = true;
			isDestroyed = false;
			CreatePie();
		}
		
		if(_startUserProjectVoteService && !_userProjectVoteServiceActif && _k < _allprojects.Length-1){
			_userProjectVoteServiceActif = true;
			_k++;
			_getUserProjectVoteService.idProject = _allprojects[_k].projectId;
			_getUserProjectVoteService.UseWebService();
		}
	}
	
	public void OpenStatsPanel(){
		if (!gameObject.activeInHierarchy) {
			gameObject.SetActive (true);
		}

		GetComponent<Animation>().GetComponent<Animation>()["StatsUISlideIn"].speed = 1f;
		
		GetComponent<Animation>().Play("StatsUISlideIn");
		
		_getAllProjectsVotesService.UseWebService();
		SetProjectVoteView ();
		
		SetDiscussionVoteView ();
		_getNbCommentsPerProjectService.UseWebService();
		
		_getNbCommentVotesPerProjectService.UseWebService();
		
		if(GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser != -1){
			_k = 0;
			_getUserProjectVoteService.idProject = _allprojects[_k].projectId;
			_userProjectVoteServiceActif = true;
			_startUserProjectVoteService = true;
			_getUserProjectVoteService.UseWebService();
		}
		ResetPieCaptions ();
		SetRightSideTitles ();
	}
	
	public void CloseStatsPanel(){
		GetComponent<Animation>().GetComponent<Animation>()["StatsUISlideIn"].time = GetComponent<Animation>().GetComponent<Animation>()["StatsUISlideIn"].length;
		GetComponent<Animation>().GetComponent<Animation>()["StatsUISlideIn"].speed = -1f;
		
		GetComponent<Animation>().Play("StatsUISlideIn");
		_startUserProjectVoteService = false;
		
		ResetPieCaptions();
		DestroyPie();

		if (gameObject.activeInHierarchy) {
			gameObject.SetActive (false);
		}
	}

	public void DeactivateStatsPanel(){
		Invoke ("StatsPanelSetInactive", 0.45f);
	}
	
	private void StatsPanelSetInactive(){
		gameObject.SetActive (false);
	}
	
	public void SetInvestmentOverviewView(bool isAllUsers){
		GameObject tempObject;
		Vector3 tempVect3;
		string tempCaption;
		float tempFloat;
		
		float BudgetTotal;
		float Investment;
		
		if(BudgetStatList != null && BudgetStatList.Count >0){
			foreach(GameObject go in BudgetStatList){
				Destroy(go);
			}
		}
		
		if (isAllUsers) {
			BudgetTotal = 7000000f; //add each project budget ?
			Investment = 600000f; //add all-user average investment for each project ?
		} else {
			BudgetTotal = 1000000f; //add each project budget ?
			Investment = 500000f; //add user investment for each project ?
		}
		
		
		//set BudgetTitle
		//StatsBudgetView.FindChild ("BudgetTitle").GetComponent<Text>().text = "BUDGET";
		
		//fill Container
		for(int i = 0; i<3; i++) {
			tempObject = (GameObject)Instantiate (BudgetStatListItem);
			tempObject.SetActive (true);
			
			if(i==0){
				tempCaption = "StatsOverviewBudgetStatListItemStart";
				tempFloat = BudgetTotal;
			}else if(i==1){
				tempCaption = "StatsOverviewBudgetStatListItemInvest";
				tempObject.transform.Find("Caption").GetComponent<Text>().color = Color.red;
				tempObject.transform.Find("ValueField").GetComponent<Text>().color = Color.red;
				tempObject.transform.Find("UnitField").GetComponent<Text>().color = Color.red;
				tempFloat = Investment;
			}else{
				tempCaption = "StatsOverviewBudgetStatListItemLeft";
				tempFloat = BudgetTotal - Investment;
			}
			tempObject.transform.Find("Caption").GetComponent<Lean.LeanLocalizedText>().PhraseName = tempCaption;
			tempObject.transform.Find("Caption").GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();

			//set Caption
			//tempObject.transform.FindChild("Caption").GetComponent<Text>().text = tempCaption;
			
			//set ValueField
			tempObject.transform.Find("ValueField").GetComponent<Text>().text = tempFloat.ToString();
			
			//set UnitField
			tempObject.transform.Find("UnitField").GetComponent<Text>().text = "€";
			
			//set parent
			tempObject.transform.SetParent (StatsBudgetView.Find ("List").Find ("Container"));
			
			//set pos
			tempObject.transform.localScale = new Vector3 (1f, 1f, 1f);
			tempVect3 = tempObject.GetComponent<RectTransform> ().anchoredPosition3D;
			tempVect3.x = 2f;
			tempVect3.y = 30 + i * (-25f);
			tempVect3.z = 0;
			tempObject.GetComponent<RectTransform> ().anchoredPosition3D = tempVect3;
			
			if(tempObject != null && BudgetStatList != null){
				BudgetStatList.Add(tempObject);
			}
		}
		
		//set InfoListTitle
		StatsGeneralInfoView.Find ("InfoListTitle").GetComponent<Text>().text = "INFOS GENERALES";
		
		//fill GeneralInfoList
		tempObject = (GameObject)Instantiate(SimpleListItem);
		tempObject.SetActive(true);
		
		//set parent
		tempObject.transform.SetParent(StatsGeneralInfoView.Find ("GeneralInfoList").Find("Container"));
		
		//set pos
		tempObject.transform.localScale = new Vector3 (1f, 1f, 1f);
		tempObject.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
	}
	
	public void DestroyPie(){
		//ThePie.ErasethePie ();
	}
	
	public void CreatePie(){
		//ThePie.DrawThePie ();
	}
	
	public void SetPieCenterCaption(string themeName){
		if(themeName == ""){
			PieCharText.GetComponent<Lean.LeanLocalizedText>().PhraseName = "StatsChartsPieCenterNoThemeSelect";
		}else if(themeName == "noInvestments"){
			PieCharText.GetComponent<Lean.LeanLocalizedText>().PhraseName = "StatsChartsPieCenterThemeNoInvest";
		}else{
			PieCharText.GetComponent<Lean.LeanLocalizedText>().PhraseName = themeName;
		}
		PieCharText.GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
	}
	
//	public void SetInvestmentDetailView(bool selected, string themeName, Dictionary<string, List<ProjectAndInvestment> > _projectsInvestment){
//		InvestmentDetailView.SetActive (selected);
//		
//		if (selected) {
//			GameObject tempObject;
//			Vector2 tempVect2;
//			int i = 0;
//			
//			if(InvestmentDetail != null && InvestmentDetail.Count >0){
//				foreach(GameObject go in InvestmentDetail){
//					Destroy(go);
//				}
//			}
//			
//			foreach (ProjectAndInvestment stru in _projectsInvestment[themeName]) {
//				tempObject = (GameObject)Instantiate(InvestmentDetailView.transform.FindChild("List").FindChild("InvestmentDetailItem").gameObject);
//				tempObject.SetActive(true);
//				
//				//set project name
//				tempObject.transform.FindChild("Project").GetComponent<Text>().text = stru.projectName;
//				
//				//set porject name color
//				tempObject.transform.FindChild("Project").GetComponent<Text>().color = GetThemeColor(themeName);
//				
//				//set Investment amount
//				tempObject.transform.FindChild("Investment").GetComponent<Text>().text = stru.investmentAmount.ToString();
//				
//				//set Unit
//				tempObject.transform.FindChild("Unit").GetComponent<Text>().text = "€";
//				
//				if(_isAllUsers){
//					
//					//set NumUsers
//					//tempObject.transform.FindChild("NumUsers").GetComponent<Text>().text = "???";
//					
//					//set Users
//					tempObject.transform.FindChild("Users").GetComponent<Text>().text = "utilisateurs";
//					
//				}else{
//					//set NumUsers
//					tempObject.transform.FindChild("NumUsers").GetComponent<Text>().text = "1";
//					
//					//set Users
//					tempObject.transform.FindChild("Users").GetComponent<Text>().text = "utilisateur";
//				}
//				
//				//set parent
//				tempObject.transform.SetParent(InvestmentDetailView.transform.FindChild("List").FindChild("Container"));
//				
//				//set pos
//				tempObject.transform.localScale = new Vector3(1f,1f,1f);
//				tempVect2 = Vector3.zero;
//				tempVect2.y += i * (-25f);
//				tempObject.GetComponent<RectTransform>().anchoredPosition = tempVect2;
//				tempObject.GetComponent<RectTransform>().offsetMax = new Vector2(0f, tempObject.GetComponent<RectTransform>().offsetMax.y);
//				
//				i++;
//				
//				InvestmentDetail.Add(tempObject);
//			}
//		}
//	}
	
	public void SetAPieViewCaption(string themeName, float arcMinDegree, float arcMaxDegree, Color themeColor, float totalInvestment, int inc){
		GameObject tempObject;
		Vector3 tempVect3;
		
		tempObject = (GameObject)Instantiate(PieCaption);
		tempObject.SetActive(true);
		
		//calculate the position of caption for the arc
		if( arcMinDegree + ((arcMaxDegree - arcMinDegree)/2) < 180){
			//caption on the right
			//set parent
			tempObject.transform.SetParent(PieChartView.Find("RightCaptions"));
			_lastIncrementRight = inc;
		}else{
			//>= 180, so caption on the left
			//set parent
			tempObject.transform.SetParent(PieChartView.Find("LeftCaptions"));
			inc -= (_lastIncrementRight+1);
		}
		
		//set name
		tempObject.transform.Find ("Top").GetComponent<Text> ().text = themeName;
		tempObject.transform.Find ("Bot").GetComponent<Text> ().text = totalInvestment.ToString()+" €";
		
		//set color
		tempObject.transform.Find ("Top").GetComponent<Text> ().color = themeColor;
		tempObject.transform.Find ("Bot").GetComponent<Text> ().color = Color.white;
		
		//set pos
		tempObject.transform.localScale = new Vector3(1f,1f,1f);
		tempVect3 = Vector3.zero;
		tempVect3.y += inc * (-40f);
		tempObject.GetComponent<RectTransform>().anchoredPosition3D = tempVect3;
		tempObject.GetComponent<RectTransform>().offsetMax = new Vector2(0f, tempObject.GetComponent<RectTransform>().offsetMax.y);
		
		//calculate line
		
		//need the middle point(Vector3) of an arc as the start for the line
		//need caption start point as the end for the line
		
		//angled line
		
		//rotate line and extend it a bit
		//transform.FindChild("EmptyLineAndFlag").FindChild("Image_Line").GetComponent<RectTransform>().sizeDelta = new Vector2(3f, 40f);
		//transform.FindChild("EmptyLineAndFlag").FindChild("Image_Line").localEulerAngles = new Vector3(357f, 1f, 310f);
		
		PieCaptionList.Add (tempObject);
		
	}
	
	public void ResetPieCaptions(){
		foreach (GameObject go in PieCaptionList) {
			Destroy(go);
		}
		PieCaptionList.Clear ();
	}
	
	public void SetRightSideTitles(){
		if (_isAllUsers) {
			PieSectionTitle.transform.GetComponent<Lean.LeanLocalizedText>().PhraseName = "StatsChartsPieTitleUsers";
			VoteSectionTitle.transform.GetComponent<Lean.LeanLocalizedText>().PhraseName = "StatsChartsVoteTitleUsers";
		} else {
			PieSectionTitle.transform.GetComponent<Lean.LeanLocalizedText>().PhraseName = "StatsChartsPieTitleUser";
			VoteSectionTitle.transform.GetComponent<Lean.LeanLocalizedText>().PhraseName = "StatsChartsVoteTitleUser";
		}

		PieSectionTitle.transform.GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
		VoteSectionTitle.transform.GetComponent<Lean.LeanLocalizedText> ().UpdateLocalization();
	}
	
	public Project[] GetAllProjects(){
		return _allprojects;
	}
	
	private void SetAllProjects(){
		_allprojects = GameObject.FindObjectsOfType<Project> ();
	}
	
	public Color GetThemeColor(string themeName){
		foreach(Theme th in ThemePanel.availableThemes){
			if(th.themeName.Contains(themeName.ToLower())){
				return th.themeColor;
			}
		}
		return Color.white;
	}
	
	private void SetProjectVoteView(){
		_ratio.onValueChanged.AddListener (ProjectVote_Ratio);
		_for.onValueChanged.AddListener (ProjectVote_For);
		_against.onValueChanged.AddListener (ProjectVote_Against);
		
		_ratio.isOn = true;
	}
	
	public void ResetToggle(){
		_ratio.isOn = true;
		_for.isOn = false;
		_against.isOn = false;
	}
	
	public void ProjectVote_Ratio(bool value){
		if(value){
			int i = 0;
			GameObject tempObject;
			Vector2 tempVect2;
			Dictionary<string, GetAllProjectsVotes.Votes> data = new Dictionary<string, GetAllProjectsVotes.Votes>();
			float ForVotes = 0f;
			float AgainstVotes = 0f;
			
			if(_isAllUsers){
				data = _getAllProjectsVotesService.VotesPerProject;
			}
			
			if(ProjectVote_RatioList != null && ProjectVote_RatioList.Count > 0){
				foreach(GameObject go in ProjectVote_RatioList){
					Destroy(go);
				}
				ProjectVote_RatioList.Clear();
			}
			
			//reset container pos x and width
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			for(int l=0; l < _allprojects.Length; l++){
				//If project has no votes, skip
				if(_isAllUsers){
					if (!data.ContainsKey(_allprojects[l].projectId)){
						continue;
					}
				}else{
					if(_userVoteData[l] == 0f){
						++l;
						continue;
					}
				}
				
				tempObject = (GameObject)Instantiate(BarRatioItem);
				tempObject.SetActive(true);
				
				if(_isAllUsers){
					//get amount of votes for this project
					ForVotes = (float) data[_allprojects[l].projectId].nbUp;
					
					//get amount of votes against this project 
					AgainstVotes = (float) data[_allprojects[l].projectId].nbDown;
				}else{
					if(_userVoteData[i] == -1f){
						ForVotes = 0f;
						AgainstVotes = 1f;
					}else{
						ForVotes = 1f;
						AgainstVotes = 0f;
					}
				}
				
				//set top bar label (for)
				tempObject.transform.Find("BarContainer").Find("TopBar").Find("LabelTop").GetComponent<Text>().text = Mathf.Round((float)((ForVotes * 100)/(ForVotes + AgainstVotes))).ToString()+"%";
				
				//set top bar height (for)
				tempObject.transform.Find("BarContainer").Find("TopBar").GetComponent<RectTransform>().sizeDelta  = new Vector2 (tempObject.transform.Find("BarContainer").Find("TopBar").GetComponent<RectTransform>().sizeDelta.x, _magicBarHeightAt0Percent + Mathf.Round(((float)((ForVotes * 100)/(ForVotes + AgainstVotes)))) * _magicPercentMultiplier);
				
				//if 0% then greyed
				if(ForVotes == 0){
					tempObject.transform.Find("BarContainer").Find("TopBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				//set down bar label (against)
				tempObject.transform.Find("BarContainer").Find("DownBar").Find("LabelDown").GetComponent<Text>().text = Mathf.Round((float)((AgainstVotes * 100)/(ForVotes + AgainstVotes))).ToString()+"%";
				
				//set down bar height (against)
				tempObject.transform.Find("BarContainer").Find("DownBar").GetComponent<RectTransform>().sizeDelta  = new Vector2 (tempObject.transform.Find("BarContainer").Find("DownBar").GetComponent<RectTransform>().sizeDelta.x, _magicBarHeightAt0Percent + Mathf.Round(((float)((AgainstVotes * 100)/(ForVotes + AgainstVotes)))) * _magicPercentMultiplier);
				
				//if 0% then greyed
				if(AgainstVotes == 0){
					tempObject.transform.Find("BarContainer").Find("DownBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				
				//set % of for votes for ordering the list
				tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteFor = Mathf.Round((float)((ForVotes * 100)/(ForVotes + AgainstVotes)));
				
				//set project name
				tempObject.transform.Find("BarRatioInfo").Find("Title").GetComponent<Text>().text = _allprojects[l].projectName;
				
				//set project name color
				tempObject.transform.Find("BarRatioInfo").Find("Title").GetComponent<Text>().color = GetThemeColor(_allprojects[l].activeThemes[0]);
				
				//set amount
				tempObject.transform.Find("BarRatioInfo").Find("ValueContainer").Find("Total").GetComponent<Text>().text = (ForVotes + AgainstVotes).ToString();
				
				//set units
				if((ForVotes + AgainstVotes) > 1){
					tempObject.transform.Find("BarRatioInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "votes";
				}else{
					tempObject.transform.Find("BarRatioInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "vote";
				}
				
				//set parent
				tempObject.transform.SetParent(ProjectVoteBarcontainer);
				
				//set position
				tempObject.transform.localScale = new Vector3(1f,1f,1f);
				tempVect2.x = i * (90f);
				tempVect2.y = 0;
				tempObject.GetComponent<RectTransform>().anchoredPosition = tempVect2;
				tempObject.GetComponent<RectTransform>().offsetMin = new Vector2(tempObject.GetComponent<RectTransform>().offsetMin.x , 0f);
				++i; 
				
				//add to list
				ProjectVote_RatioList.Add(tempObject);
				
				//ORDERED BY %FOR Descending
				if(ProjectVote_RatioList.Count -2 >= 0){
					for(int j=ProjectVote_RatioList.Count -2; j>=0; j--){
						if( tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteFor > ProjectVote_RatioList[j].GetComponent<UIBarAmountBothVote>().AmountOfVoteFor){
							//swap position in ui
							Vector2 tempPos = tempObject.GetComponent<RectTransform>().anchoredPosition;
							tempObject.GetComponent<RectTransform>().anchoredPosition = ProjectVote_RatioList[j].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_RatioList[j].GetComponent<RectTransform>().anchoredPosition = tempPos;
							//swap position in list
							GameObject tmp = ProjectVote_RatioList[j];
							int tempIndex = ProjectVote_RatioList.IndexOf(tempObject);
							ProjectVote_RatioList[j] = tempObject;
							ProjectVote_RatioList[tempIndex] = tmp;
						}
					}
				}
			}
			//augment width of container each time
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(((i+1) * 90f), ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			// for loop end
			if(i==0){
				//if i==0 then no votes exist for any projects
				ProjectVoteBarcontainer.parent.parent.Find("NoVotesText").gameObject.SetActive(true);
			}else{
				ProjectVoteBarcontainer.parent.parent.Find("NoVotesText").gameObject.SetActive(false);
			}
		}else{
			if(ProjectVote_RatioList != null && ProjectVote_RatioList.Count > 0){
				foreach(GameObject go in ProjectVote_RatioList){
					Destroy(go);
				}
				ProjectVote_RatioList.Clear();
			}
		}
	}
	
	public void ProjectVote_For(bool value){
		if(value){
			int i = 0;
			GameObject tempObject;
			Vector2 tempVect2;
			
			float maxInBothVotes = 0f;
			Dictionary<string, GetAllProjectsVotes.Votes> data = new Dictionary<string, GetAllProjectsVotes.Votes>();
			
			float ForVotes = 0f;
			float AgainstVotes = 0f;
			
			if(_isAllUsers){
				data = _getAllProjectsVotesService.VotesPerProject;
			}
			
			if(ProjectVote_ForList != null && ProjectVote_ForList.Count > 0){
				foreach(GameObject go in ProjectVote_ForList){
					Destroy(go);
				}
				ProjectVote_ForList.Clear();
			}
			
			if(_isAllUsers){
				foreach(Project project in _allprojects){
					if (!data.ContainsKey(project.projectId))
						continue;
					GetAllProjectsVotes.Votes votes = data[project.projectId];
					float max = Math.Max((float) votes.nbUp, (float) votes.nbUp);
					maxInBothVotes = Math.Max(maxInBothVotes, max);
				}
			}else{
				maxInBothVotes = 1f;
			}
			
			//reset container pos x and width
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			for(int l=0; l < _allprojects.Length; l++){
				if(_isAllUsers){
					if (!data.ContainsKey(_allprojects[l].projectId)){
						continue;
					}
				}else{
					if(_userVoteData[l] == 0f){
						++l;
						continue;
					}
				}
				
				tempObject = (GameObject)Instantiate(BarDoubleItem);
				tempObject.SetActive(true);
				
				if(_isAllUsers){
					//get amount of votes for this project
					ForVotes = (float) data[_allprojects[l].projectId].nbUp;
					
					//get amount of votes against this project 
					AgainstVotes = (float) data[_allprojects[l].projectId].nbDown;
				}else{
					if(_userVoteData[i] == -1f){
						ForVotes = 0f;
						AgainstVotes = 1f;
					}else{
						ForVotes = 1f;
						AgainstVotes = 0f;
					}
				}
				
				//need to swap bar color so green is first
				Color temp = tempObject.transform.Find("RightBar").GetComponent<Image>().color;
				tempObject.transform.Find("RightBar").GetComponent<Image>().color = tempObject.transform.Find("LeftBar").GetComponent<Image>().color;
				tempObject.transform.Find("LeftBar").GetComponent<Image>().color = temp;
				
				//set top bar label (for)
				tempObject.transform.Find("LeftBar").Find("LeftBarLabel").GetComponent<Text>().text = ForVotes.ToString();
				
				//set top bar height (for)
				tempObject.transform.Find("LeftBar").GetComponent<RectTransform>().offsetMax = new Vector2 (tempObject.transform.Find("LeftBar").GetComponent<RectTransform>().offsetMax.x, (185 - (ForVotes * (185/maxInBothVotes)))*-1);
				
				//if 0% then greyed
				if(ForVotes == 0){
					tempObject.transform.Find("LeftBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				
				//set down bar label (against)
				tempObject.transform.Find("RightBar").Find("RightBarLabel").GetComponent<Text>().text = AgainstVotes.ToString();
				
				//set down bar height (against)
				tempObject.transform.Find("RightBar").GetComponent<RectTransform>().offsetMax  = new Vector2 (tempObject.transform.Find("RightBar").GetComponent<RectTransform>().offsetMax.x, (185 - (AgainstVotes * (185/maxInBothVotes)))*-1);
				
				//if 0% then greyed
				if(AgainstVotes == 0){
					tempObject.transform.Find("RightBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				
				//set % of for votes for ordering the list
				tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteFor = ForVotes;
				tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst = AgainstVotes;
				
				//set project name
				tempObject.transform.Find("BarDoubleInfo").Find("Title").GetComponent<Text>().text = _allprojects[l].projectName;
				
				//set project name color
				tempObject.transform.Find("BarDoubleInfo").Find("Title").GetComponent<Text>().color = GetThemeColor(_allprojects[l].activeThemes[0]);
				
				//set amount
				tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Total").GetComponent<Text>().text = (ForVotes + AgainstVotes).ToString();
				
				//set units
				if((ForVotes + AgainstVotes) > 1){
					tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "votes";
				}else{
					tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "vote";
				}
				
				//set parent
				tempObject.transform.SetParent(ProjectVoteBarcontainer);
				
				//set position
				tempObject.transform.localScale = new Vector3(1f,1f,1f);
				tempVect2.x = i * (100f);
				tempVect2.y = 0;
				tempObject.GetComponent<RectTransform>().anchoredPosition = tempVect2;
				tempObject.GetComponent<RectTransform>().offsetMin = new Vector2(tempObject.GetComponent<RectTransform>().offsetMin.x , 0f);
				++i;
				
				//add to list
				ProjectVote_ForList.Add(tempObject);
				
				//ORDERED BY nb votes FOR Descending
				if(ProjectVote_ForList.Count -2 >= 0){
					for(int j=ProjectVote_ForList.Count -2; j>=0; j--){
						if( tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteFor > ProjectVote_ForList[j].GetComponent<UIBarAmountBothVote>().AmountOfVoteFor){
							Vector2 tempPos = tempObject.GetComponent<RectTransform>().anchoredPosition;
							tempObject.GetComponent<RectTransform>().anchoredPosition = ProjectVote_ForList[j].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_ForList[j].GetComponent<RectTransform>().anchoredPosition = tempPos;
							//swap position in list
							GameObject tmp = ProjectVote_ForList[j];
							int tempIndex = ProjectVote_ForList.IndexOf(tempObject);
							ProjectVote_ForList[j] = tempObject;
							ProjectVote_ForList[tempIndex] = tmp;
						}
					}
				}
			}
			
			//augment width of container each time
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(((i+1) * 100f), ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			// foreach end
			if(i==0){
				//if i==0 then no votes exist for any projects
				ProjectVoteBarcontainer.parent.parent.Find("NoVotesText").gameObject.SetActive(true);
			}else{
				ProjectVoteBarcontainer.parent.parent.Find("NoVotesText").gameObject.SetActive(false);
				
				//order first bar same value by second bar value
				for (int m = 0; m < ProjectVote_ForList.Count - 1; m++)	{
					for (var n = 0; n < ProjectVote_ForList.Count - 1; n++) {
						if (ProjectVote_ForList[n].GetComponent<UIBarAmountBothVote>().AmountOfVoteFor == ProjectVote_ForList[n + 1].GetComponent<UIBarAmountBothVote>().AmountOfVoteFor && 
						    ProjectVote_ForList[n].GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst > ProjectVote_ForList[n + 1].GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst) {
							//swap position in list
							var temp = ProjectVote_ForList[n];
							ProjectVote_ForList[n] = ProjectVote_ForList[n + 1];
							ProjectVote_ForList[n + 1] = temp;
							
							//swap position in ui
							Vector2 tempPos = ProjectVote_AgainstList[n].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_AgainstList[n].GetComponent<RectTransform>().anchoredPosition = ProjectVote_AgainstList[n+1].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_AgainstList[n+1].GetComponent<RectTransform>().anchoredPosition = tempPos;
						}
					}
				}
			}
		}else{
			if(ProjectVote_ForList != null && ProjectVote_ForList.Count > 0){
				foreach(GameObject go in ProjectVote_ForList){
					Destroy(go);
				}
				ProjectVote_ForList.Clear();
			}
		}
	}
	
	public void ProjectVote_Against(bool value){
		if(value){
			int i = 0;
			GameObject tempObject;
			Vector2 tempVect2;
			
			float maxInBothVotes = 0f;
			Dictionary<string, GetAllProjectsVotes.Votes> data = new Dictionary<string, GetAllProjectsVotes.Votes>();
			
			float ForVotes = 0f;
			float AgainstVotes = 0f;
			
			if(_isAllUsers){
				data = _getAllProjectsVotesService.VotesPerProject;
			}
			
			if(ProjectVote_AgainstList != null && ProjectVote_AgainstList.Count > 0){
				foreach(GameObject go in ProjectVote_AgainstList){
					Destroy(go);
				}
				ProjectVote_AgainstList.Clear();
			}
			
			if(_isAllUsers){
				foreach (Project project in _allprojects) {
					if (!data.ContainsKey(project.projectId))
						continue;
					GetAllProjectsVotes.Votes votes = data[project.projectId];
					float max = Math.Max((float)votes.nbUp, (float)votes.nbUp);
					maxInBothVotes = Math.Max(maxInBothVotes, max);
				}
			}else{
				maxInBothVotes = 1f;
			}
			
			//reset container pos x and width
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			for(int l=0; l < _allprojects.Length; l++){
				if(_isAllUsers){
					if (!data.ContainsKey(_allprojects[l].projectId)){
						continue;
					}
				}else{
					if(_userVoteData[l] == 0f){
						++l;
						continue;
					}
				}
				
				tempObject = (GameObject)Instantiate(BarDoubleItem);
				tempObject.SetActive(true);
				
				if(_isAllUsers){
					//get amount of votes for this project
					ForVotes = (float) data[_allprojects[l].projectId].nbUp;
					
					//get amount of votes against this project 
					AgainstVotes = (float) data[_allprojects[l].projectId].nbDown;
				}else{
					if(_userVoteData[i] == -1f){
						ForVotes = 0f;
						AgainstVotes = 1f;
					}else{
						ForVotes = 1f;
						AgainstVotes = 0f;
					}
				}
				
				//set top bar label (for)
				tempObject.transform.Find("LeftBar").Find("LeftBarLabel").GetComponent<Text>().text = AgainstVotes.ToString();
				
				//set top bar height (for)
				tempObject.transform.Find("LeftBar").GetComponent<RectTransform>().offsetMax  = new Vector2 (tempObject.transform.Find("LeftBar").GetComponent<RectTransform>().offsetMax.x, (185 - (AgainstVotes * (185/maxInBothVotes)))*-1);
				
				//if 0% then greyed
				if(AgainstVotes == 0){
					tempObject.transform.Find("LeftBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				//********************************************
				//set down bar label (against)
				tempObject.transform.Find("RightBar").Find("RightBarLabel").GetComponent<Text>().text = ForVotes.ToString();
				
				//set down bar height (against)
				tempObject.transform.Find("RightBar").GetComponent<RectTransform>().offsetMax  = new Vector2 (tempObject.transform.Find("RightBar").GetComponent<RectTransform>().offsetMax.x, (185 - (ForVotes * (185/maxInBothVotes)))*-1);
				
				//if 0% then greyed
				if(ForVotes == 0){
					tempObject.transform.Find("RightBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				
				//set % of against votes for ordering the list
				tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst = AgainstVotes;
				//set % of for votes for ordering the list
				tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteFor = ForVotes;
				
				//set project name
				tempObject.transform.Find("BarDoubleInfo").Find("Title").GetComponent<Text>().text = _allprojects[l].projectName;
				
				//set project name color
				tempObject.transform.Find("BarDoubleInfo").Find("Title").GetComponent<Text>().color = GetThemeColor(_allprojects[l].activeThemes[0]);
				
				//set amount
				tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Total").GetComponent<Text>().text = (ForVotes + AgainstVotes).ToString();
				
				//set units
				if((ForVotes + AgainstVotes) > 1){
					tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "votes";
				}else{
					tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "vote";
				}
				
				//set parent
				tempObject.transform.SetParent(ProjectVoteBarcontainer);
				
				//set position
				tempObject.transform.localScale = new Vector3(1f,1f,1f);
				tempVect2.x = i * (100f);
				tempVect2.y = 0;
				tempObject.GetComponent<RectTransform>().anchoredPosition = tempVect2;
				tempObject.GetComponent<RectTransform>().offsetMin = new Vector2(tempObject.GetComponent<RectTransform>().offsetMin.x , 0f);
				++i;
				
				//add to list
				ProjectVote_AgainstList.Add(tempObject);
				
				//ORDERED BY %AGAINST Descending
				if(ProjectVote_AgainstList.Count -2 >= 0){
					for(int j=ProjectVote_AgainstList.Count -2; j>=0; j--){
						if( tempObject.GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst > ProjectVote_AgainstList[j].GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst){
							Vector2 tempPos = tempObject.GetComponent<RectTransform>().anchoredPosition;
							tempObject.GetComponent<RectTransform>().anchoredPosition = ProjectVote_AgainstList[j].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_AgainstList[j].GetComponent<RectTransform>().anchoredPosition = tempPos;
							//swap position in list
							GameObject tmp = ProjectVote_AgainstList[j];
							int tempIndex = ProjectVote_AgainstList.IndexOf(tempObject);
							ProjectVote_AgainstList[j] = tempObject;
							ProjectVote_AgainstList[tempIndex] = tmp;
						}
					}
				}
			}
			//augment width of container each time
			ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(((i+1) * 100f), ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			// foreach end
			if(i==0){
				//if i==0 then no votes exist for any projects
				ProjectVoteBarcontainer.parent.parent.Find("NoVotesText").gameObject.SetActive(true);
			}else{
				ProjectVoteBarcontainer.parent.parent.Find("NoVotesText").gameObject.SetActive(false);
				
				//order first bar same value by second bar value
				Debug.Log("before against sort by second value");
				for (int m = 0; m < ProjectVote_AgainstList.Count - 1; m++)	{
					for (var n = 0; n < ProjectVote_AgainstList.Count - 1; n++) {
						if (ProjectVote_AgainstList[n].GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst == ProjectVote_AgainstList[n + 1].GetComponent<UIBarAmountBothVote>().AmountOfVoteAgainst && 
						    ProjectVote_AgainstList[n].GetComponent<UIBarAmountBothVote>().AmountOfVoteFor > ProjectVote_AgainstList[n + 1].GetComponent<UIBarAmountBothVote>().AmountOfVoteFor) {
							//swap position in list
							var temp = ProjectVote_AgainstList[n];
							ProjectVote_AgainstList[n] = ProjectVote_AgainstList[n + 1];
							ProjectVote_AgainstList[n + 1] = temp;
							//swap position in ui
							Vector2 tempPos = ProjectVote_AgainstList[n].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_AgainstList[n].GetComponent<RectTransform>().anchoredPosition = ProjectVote_AgainstList[n+1].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_AgainstList[n+1].GetComponent<RectTransform>().anchoredPosition = tempPos;
						}
					}
				}
			}
		}else{
			if(ProjectVote_AgainstList != null && ProjectVote_AgainstList.Count > 0){
				foreach(GameObject go in ProjectVote_AgainstList){
					Destroy(go);
				}
				ProjectVote_AgainstList.Clear();
			}
		}
	}
	
	private void SetDiscussionVoteView(){
		_tendance.onValueChanged.AddListener (SetDiscussion_Tendance);
		_votes.onValueChanged.AddListener(SetDiscussion_Votes);
		
		_tendance.isOn = true;
	}
	
	public void SetDiscussion_Tendance(bool value){
		if(value){
			int i = 0;
			GameObject tempObject;
			Vector2 tempVect2;
			SetDiscussionVote_TendanceList = new List<GameObject> ();
			
			float maxComment = 0f;
			Dictionary<string, int> data = _getNbCommentsPerProjectService.ProjectStats;
			
			foreach (Project project in _allprojects) {
				if (!data.ContainsKey(project.projectId))
					continue;
				int nbComments = data[project.projectId];
				maxComment = Math.Max(maxComment, nbComments);
			}
			
			//reset container pos x and width
			DiscussionVoteBarcontainer.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			DiscussionVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, DiscussionVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			foreach(Project project in _allprojects){
				if (!data.ContainsKey(project.projectId))
					continue;
				
				tempObject = (GameObject)Instantiate(BarItem);
				tempObject.SetActive(true);
				
				//get amount of comments for this project
				float nbComment = (float) data[project.projectId];
				
				//set bar height
				tempObject.transform.Find("Bar").GetComponent<RectTransform>().offsetMax  = new Vector2 (tempObject.transform.Find("Bar").GetComponent<RectTransform>().offsetMax.x, (185 - (nbComment * (185/maxComment))*1));
				
				//set bar color
				tempObject.transform.Find("Bar").GetComponent<Image>().color = GetThemeColor(project.activeThemes[0]);
				
				//set amount of comments for ordering the list
				tempObject.GetComponent<UIBarDiscussionStats>().AmountOfComments = nbComment;
				
				//set project name
				tempObject.transform.Find("Footer").Find("Title").GetComponent<Text>().text = project.projectName;
				
				//set project name color
				tempObject.transform.Find("Footer").Find("Title").GetComponent<Text>().color = GetThemeColor(project.activeThemes[0]);
				
				//set amount
				tempObject.transform.Find("Footer").Find("ValueContainer").Find("Total").GetComponent<Text>().text = (nbComment).ToString();
				
				//set units
				if((nbComment) > 1){
					tempObject.transform.Find("Footer").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "commentaires";
				}else{
					tempObject.transform.Find("Footer").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "commentaire";
				}
				
				//set parent
				tempObject.transform.SetParent(DiscussionVoteBarcontainer);
				
				//set position
				tempObject.transform.localScale = new Vector3(1f,1f,1f);
				tempVect2.x = i * (110f);
				tempVect2.y = 0;
				tempObject.GetComponent<RectTransform>().anchoredPosition = tempVect2;
				tempObject.GetComponent<RectTransform>().offsetMin = new Vector2(tempObject.GetComponent<RectTransform>().offsetMin.x , 0f);
				++i;
				
				//add to list
				SetDiscussionVote_TendanceList.Add(tempObject);
				
				//ORDERED BY amount of comments Descending
				if(SetDiscussionVote_TendanceList.Count -2 >= 0){
					for(int j=SetDiscussionVote_TendanceList.Count -2; j>=0; j--){
						if( tempObject.GetComponent<UIBarDiscussionStats>().AmountOfComments > SetDiscussionVote_TendanceList[j].GetComponent<UIBarDiscussionStats>().AmountOfComments){
							Vector2 tempPos = tempObject.GetComponent<RectTransform>().anchoredPosition;
							tempObject.GetComponent<RectTransform>().anchoredPosition = SetDiscussionVote_TendanceList[j].GetComponent<RectTransform>().anchoredPosition;
							SetDiscussionVote_TendanceList[j].GetComponent<RectTransform>().anchoredPosition = tempPos;
							//swap position in list
							GameObject tmp = SetDiscussionVote_TendanceList[j];
							int tempIndex = SetDiscussionVote_TendanceList.IndexOf(tempObject);
							SetDiscussionVote_TendanceList[j] = tempObject;
							SetDiscussionVote_TendanceList[tempIndex] = tmp;
						}
					}
				}
			}
			//augment width of container each time
			DiscussionVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(((i+1) * 110f), ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			// foreach end
			if(i==0){
				//if i==0 then no votes exist for any projects
				DiscussionVoteBarcontainer.parent.parent.Find("NoCommentText").gameObject.SetActive(true);
				DiscussionVoteBarcontainer.parent.parent.Find("NoCommentText").GetComponent<Text>().text = "Il n'y a pas encore de commentaires";
			}else{
				DiscussionVoteBarcontainer.parent.parent.Find("NoCommentText").gameObject.SetActive(false);
			}
		}else{
			if(SetDiscussionVote_TendanceList != null && SetDiscussionVote_TendanceList.Count > 0){
				foreach(GameObject go in SetDiscussionVote_TendanceList){
					Destroy(go);
				}
			}
		}
	}
	
	public void SetDiscussion_Votes(bool value){
		if(value){
			int i = 0;
			GameObject tempObject;
			Vector2 tempVect2;
			SetDiscussionVote_VotesList = new List<GameObject> ();
			
			float maxInBothVotes = 0f;
			Dictionary<string, GetNbCommentVotesPerProject.ProjectCommentsVotes> data = _getNbCommentVotesPerProjectService.ProjectsCommentsVotes;
			
			foreach (Project project in _allprojects) {
				if (!data.ContainsKey(project.projectId))
					continue;
				GetNbCommentVotesPerProject.ProjectCommentsVotes commentVote = data[project.projectId];
				float max = Math.Max(commentVote.nbDown, commentVote.nbUp);
				maxInBothVotes = Math.Max(max, maxInBothVotes);
			}
			
			//reset container pos x and width
			DiscussionVoteBarcontainer.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			DiscussionVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, DiscussionVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			foreach(Project project in _allprojects){
				if (!data.ContainsKey(project.projectId))
					continue;
				
				tempObject = (GameObject)Instantiate(DoubleBarItem);
				tempObject.SetActive(true);
				
				//get amount of votes for this project'discussion
				float ForVotes = data[project.projectId].nbUp;
				
				//get amount of votes against this project'discussion
				float AgainstVotes = data[project.projectId].nbDown;
				
				//need to swap bar color so green is first
				Color temp = tempObject.transform.Find("RightBar").GetComponent<Image>().color;
				tempObject.transform.Find("RightBar").GetComponent<Image>().color = tempObject.transform.Find("LeftBar").GetComponent<Image>().color;
				tempObject.transform.Find("LeftBar").GetComponent<Image>().color = temp;
				
				//set top bar label (for)
				tempObject.transform.Find("RightBar").Find("RightBarLabel").GetComponent<Text>().text = AgainstVotes.ToString();
				
				//set top bar height (for)
				tempObject.transform.Find("RightBar").GetComponent<RectTransform>().offsetMax  = new Vector2 (tempObject.transform.Find("RightBar").GetComponent<RectTransform>().offsetMax.x, (185 - (AgainstVotes * (185/maxInBothVotes)))*-1);
				
				//if 0% then greyed
				if(AgainstVotes == 0){
					tempObject.transform.Find("RightBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				//set down bar label (against)
				tempObject.transform.Find("LeftBar").Find("LeftBarLabel").GetComponent<Text>().text = ForVotes.ToString();
				
				//set down bar height (against)
				tempObject.transform.Find("LeftBar").GetComponent<RectTransform>().offsetMax  = new Vector2 (tempObject.transform.Find("LeftBar").GetComponent<RectTransform>().offsetMax.x, (185 - (ForVotes * (185/maxInBothVotes)))*-1);
				
				//if 0% then greyed
				if(ForVotes == 0){
					tempObject.transform.Find("LeftBar").GetComponent<CanvasGroup>().alpha = 0.3f;
				}
				
				//set % of for votes for ordering the list
				tempObject.GetComponent<UIBarDiscussionStats>().nbForVotes = ForVotes;
				//set % of against votes for ordering the list
				tempObject.GetComponent<UIBarDiscussionStats>().nbAgainstVotes = AgainstVotes;
				
				//set project name
				tempObject.transform.Find("BarDoubleInfo").Find("Title").GetComponent<Text>().text = project.projectName;
				
				//set project name color
				tempObject.transform.Find("BarDoubleInfo").Find("Title").GetComponent<Text>().color = GetThemeColor(project.activeThemes[0]);
				
				//set amount
				tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Total").GetComponent<Text>().text = (ForVotes + AgainstVotes).ToString();
				
				//set units
				if((ForVotes + AgainstVotes) > 1){
					tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "votes";
				}else{
					tempObject.transform.Find("BarDoubleInfo").Find("ValueContainer").Find("Unit").GetComponent<Text>().text = "vote";
				}
				
				//set parent
				tempObject.transform.SetParent(DiscussionVoteBarcontainer);
				
				//set position
				tempObject.transform.localScale = new Vector3(1f,1f,1f);
				tempVect2.x = i * (90f);
				tempVect2.y = 0;
				tempObject.GetComponent<RectTransform>().anchoredPosition = tempVect2;
				tempObject.GetComponent<RectTransform>().offsetMin = new Vector2(tempObject.GetComponent<RectTransform>().offsetMin.x , 0f);
				++i;
				
				//add to list
				SetDiscussionVote_VotesList.Add(tempObject);
				
				//ORDERED BY %AGAINST Descending
				if(SetDiscussionVote_VotesList.Count -2 >= 0){
					for(int j=SetDiscussionVote_VotesList.Count -2; j>=0; j--){
						if( tempObject.GetComponent<UIBarDiscussionStats>().nbForVotes > SetDiscussionVote_VotesList[j].GetComponent<UIBarDiscussionStats>().nbForVotes){
							Vector2 tempPos = tempObject.GetComponent<RectTransform>().anchoredPosition;
							tempObject.GetComponent<RectTransform>().anchoredPosition = SetDiscussionVote_VotesList[j].GetComponent<RectTransform>().anchoredPosition;
							SetDiscussionVote_VotesList[j].GetComponent<RectTransform>().anchoredPosition = tempPos;
							//swap position in list
							GameObject tmp = SetDiscussionVote_VotesList[j];
							int tempIndex = SetDiscussionVote_VotesList.IndexOf(tempObject);
							SetDiscussionVote_VotesList[j] = tempObject;
							SetDiscussionVote_VotesList[tempIndex] = tmp;
						}
					}
				}
			}
			//augment width of container each time
			DiscussionVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(((i+1) * 90f), ProjectVoteBarcontainer.GetComponent<RectTransform> ().sizeDelta.y);
			
			// foreach end
			if(i==0){
				//if i==0 then no votes exist for any projects
				DiscussionVoteBarcontainer.parent.parent.Find("NoCommentText").gameObject.SetActive(true);
				DiscussionVoteBarcontainer.parent.parent.Find("NoCommentText").GetComponent<Text>().text = "Il n'y a pas encore de votes";
			}else{
				DiscussionVoteBarcontainer.parent.parent.Find("NoCommentText").gameObject.SetActive(false);
				
				//order first bar same value by second bar value
				for (int m = 0; m < SetDiscussionVote_VotesList.Count - 1; m++)	{
					for (var n = 0; n < SetDiscussionVote_VotesList.Count - 1; n++) {
						if (SetDiscussionVote_VotesList[n].GetComponent<UIBarDiscussionStats>().nbForVotes == SetDiscussionVote_VotesList[n + 1].GetComponent<UIBarDiscussionStats>().nbForVotes && 
						    SetDiscussionVote_VotesList[n].GetComponent<UIBarDiscussionStats>().nbAgainstVotes > SetDiscussionVote_VotesList[n + 1].GetComponent<UIBarDiscussionStats>().nbAgainstVotes) {
							//swap position in list
							var temp = SetDiscussionVote_VotesList[n];
							SetDiscussionVote_VotesList[n] = SetDiscussionVote_VotesList[n + 1];
							SetDiscussionVote_VotesList[n + 1] = temp;
							
							//swap position in ui
							Vector2 tempPos = ProjectVote_AgainstList[n].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_AgainstList[n].GetComponent<RectTransform>().anchoredPosition = ProjectVote_AgainstList[n+1].GetComponent<RectTransform>().anchoredPosition;
							ProjectVote_AgainstList[n+1].GetComponent<RectTransform>().anchoredPosition = tempPos;
						}
					}
				}
				
			}
		}else{
			if(SetDiscussionVote_VotesList != null && SetDiscussionVote_VotesList.Count > 0){
				foreach(GameObject go in SetDiscussionVote_VotesList){
					Destroy(go);
				}
			}
		}
	}
	
	// WEB SERVICES CALLBACKS
	private void _getAllProjectsVotesService_onError(long status, string message) {
		Debug.Log("Get All Projects Votes ERROR");
	}
	
	private void _getAllProjectsVotesService_onComplete(long status, string message) {
		Debug.Log("Get All Project Votes OK");
		ProjectVote_Ratio(true);
	}
	
	private void _getNbCommentsPerProjectService_onError(long status, string message) {
		Debug.Log("Get NB Comments per Project ERROR");
	}
	
	private void _getNbCommentsPerProjectService_onComplete(long status, string message) {
		Debug.Log("Get NB Comments per Project OK");
		SetDiscussion_Tendance(true);
	}
	
	private void _getNbCommentVotesPerProjectService_onError(long status, string message) {
		Debug.Log("Get nb comment votes per project ERROR");
	}
	
	private void _getNbCommentVotesPerProjectService_onComplete(long status, string message) {
		Debug.Log("Get nb comment votes per project OK");
		SetDiscussion_Votes(false); 
	}
	
	private void _getUserProjectVoteService_onError(long status, string message) {
		Debug.Log("GetUserProjectVote ERROR");
	}
	
	private void _getUserProjectVoteService_onComplete(long status, string message) {
		
		_userVoteData.Add((float)_getUserProjectVoteService.uservote);
		_userProjectVoteServiceActif = false;
	}
}
