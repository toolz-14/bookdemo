﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public class ProfileRegister : MonoBehaviour {

    [SerializeField]
    private RectTransform Text1;
    [SerializeField]
    private RectTransform Text2;

    public GameObject FormErrorFirstName;
	public GameObject FormErrorLastName;
	public GameObject FormErrorEmail;
	public GameObject FormErrorUserName;
	public GameObject FormErrorPWD;
	public GameObject FormErrorPWDConf;

	public GameObject InputFieldFirstName;
	public GameObject InputFieldLastName;
	public GameObject InputFieldEmail;
	public GameObject InputFieldUserName;
	public GameObject InputFieldPWD;
	public GameObject InputFieldPWDConf;

	// Regular expression, which is used to validate an E-Mail address.
	public const string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
		+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
			+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
			+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

	private string _password;
	private InputField input_FN;
	private InputField input_LN;
	private InputField input_EM;
	private InputField input_UN;
	private InputField input_PWD;
	private InputField input_PWDC;
	private bool firstNameValid;
	private bool lastNameValid;
	private bool emailValid;
	private bool useNameValid;
	private bool pwdValid;
	private bool pwdConfValid;

	
	private void Start () {

		_password = "";

		input_FN = InputFieldFirstName.GetComponent<InputField>();
		input_FN.onEndEdit.AddListener(CheckFirstName);

		input_LN = InputFieldLastName.GetComponent<InputField>();
		input_LN.onEndEdit.AddListener(CheckLastName);

		input_EM = InputFieldEmail.GetComponent<InputField>();
		input_EM.onEndEdit.AddListener(CheckEmail);

		input_UN = InputFieldUserName.GetComponent<InputField>();
		input_UN.onEndEdit.AddListener(CheckUserName);

		input_PWD = InputFieldPWD.GetComponent<InputField>();
		input_PWD.onEndEdit.AddListener(CheckPWD);

		input_PWDC = InputFieldPWDConf.GetComponent<InputField>();
		input_PWDC.onEndEdit.AddListener(CheckPWDConf);

		//debug
		//ForceShowAllErrors ();
	}

	private void OnDisable(){
		OnExitProfile ();
	}

	public void OnExitProfile(){
		//empty all fields
		if (input_FN != null && input_FN.text != null) {
			input_FN.text = "";
		}
		if (input_LN != null && input_LN.text != null) {
			input_LN.text = "";
		}
		if (input_EM != null && input_EM.text != null) {
			input_EM.text = "";
		}
		if (input_UN != null && input_UN.text != null) {
			input_UN.text = "";
		}
		if (input_PWD != null && input_PWD.text != null) {
			input_PWD.text = "";
		}
		if (input_PWDC != null && input_PWDC.text != null) {
			input_PWDC.text = "";
		}
	}

	public void HideAllFormError(){
		FormErrorFirstName.SetActive (false);
		FormErrorLastName.SetActive (false);
		FormErrorEmail.SetActive (false);
		FormErrorUserName.SetActive (false);
		FormErrorPWD.SetActive (false);
		FormErrorPWDConf.SetActive (false);
	}

	public bool CheckAllFieldsComplete(){
		return firstNameValid && lastNameValid && emailValid && useNameValid && pwdValid && pwdConfValid;
	}
	
	private void ShowFormErrorFirstName(bool show){
		if(show){
			HideAllFormError();
		}
		FormErrorFirstName.SetActive (show);
        GetComponent<ResizeContainer>().GetChildrensAndResize();
        //pass size to parent for scroll rect
        transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.GetComponent<RectTransform>().sizeDelta.x, GetComponent<RectTransform>().sizeDelta.y+ Text1.sizeDelta.y + Text2.sizeDelta.y +50f);
	}

	private void ShowFormErrorLastName(bool show){
		if(show){
			HideAllFormError();
		}
		FormErrorLastName.SetActive (show);
        GetComponent<ResizeContainer>().GetChildrensAndResize();
        //pass size to parent for scroll rect
        transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.GetComponent<RectTransform>().sizeDelta.x, GetComponent<RectTransform>().sizeDelta.y + Text1.sizeDelta.y + Text2.sizeDelta.y + 50f);
    }

	private void ShowFormErrorEmail(bool show){
		if(show){
			HideAllFormError();
		}
		FormErrorEmail.SetActive (show);
        GetComponent<ResizeContainer>().GetChildrensAndResize();
        //pass size to parent for scroll rect
        transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.GetComponent<RectTransform>().sizeDelta.x, GetComponent<RectTransform>().sizeDelta.y + Text1.sizeDelta.y + Text2.sizeDelta.y + 50f);
    }
	
	private void ShowFormErrorUserName(bool show){
		if(show){
			HideAllFormError();
		}
		FormErrorUserName.SetActive (show);
        GetComponent<ResizeContainer>().GetChildrensAndResize();
        //pass size to parent for scroll rect
        transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.GetComponent<RectTransform>().sizeDelta.x, GetComponent<RectTransform>().sizeDelta.y + Text1.sizeDelta.y + Text2.sizeDelta.y + 50f);
    }
	
	private void ShowFormErrorPWD(bool show){
		if(show){
			HideAllFormError();
		}
		FormErrorPWD.SetActive (show);
        GetComponent<ResizeContainer>().GetChildrensAndResize();
        //pass size to parent for scroll rect
        transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.GetComponent<RectTransform>().sizeDelta.x, GetComponent<RectTransform>().sizeDelta.y + Text1.sizeDelta.y + Text2.sizeDelta.y + 50f);
    }
	
	private void ShowFormErrorPWDConf(bool show){
		if(show){
			HideAllFormError();
		}
		FormErrorPWDConf.SetActive (show);
        GetComponent<ResizeContainer>().GetChildrensAndResize();
        //pass size to parent for scroll rect
        transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.parent.GetComponent<RectTransform>().sizeDelta.x, GetComponent<RectTransform>().sizeDelta.y + Text1.sizeDelta.y + Text2.sizeDelta.y + 50f);
    }

	public void CheckFirstName(string value){

		if(Regex.IsMatch(value, @"^[\p{L}\p{M}' \.\-]+$") && !DoesStringContainWhiteSpace(value)){
			ShowFormErrorFirstName(false);
			firstNameValid = true;
		}else{
			ShowFormErrorFirstName(true);
			firstNameValid = false;
		}
	}

	public void CheckLastName(string value){
		
		if(Regex.IsMatch(value, @"^[\p{L}\p{M}' \.\-]+$") && !DoesStringContainWhiteSpace(value)){
			ShowFormErrorLastName(false);
			lastNameValid = true;
		}else{
			ShowFormErrorLastName(true);
			lastNameValid = false;
		}
	}

	public void CheckEmail(string value){
		if(IsEmail(value)){
			ShowFormErrorEmail(false);
			emailValid = true;
		}else{
			ShowFormErrorEmail(true);
			emailValid = false;
		}
	}
	
	// Checks whether the given Email-Parameter is a valid E-Mail address.
	public static bool IsEmail(string email){
		if (email != null) {
			return Regex.IsMatch (email, MatchEmailPattern);

            //email already exist in database


		} else {
			return false;
		}
	}

	public void CheckUserName(string value){
		//pas d'espace
		if(!DoesStringContainWhiteSpace(value)){
			ShowFormErrorUserName(false);
			useNameValid = true;
		}else{
			ShowFormErrorUserName(true);
			useNameValid = false;
		}
	}

	public void CheckPWD(string value){
		//au moins 8 caracter
		if(value.Length >= 8){
			ShowFormErrorPWD(false);
			_password = value;
			pwdValid = true;
		}else{
			ShowFormErrorPWD(true);
			pwdValid = false;
		}
	}

	public void CheckPWDConf(string value){
		//entry must match Pwd
		if(String.Equals(_password, value)){
			ShowFormErrorPWDConf(false);
			pwdConfValid = true;
		}else{
			ShowFormErrorPWDConf(true);
			pwdConfValid = false;
		}
	}

	private void ForceShowAllErrors(){
		ShowFormErrorFirstName (true);
		ShowFormErrorLastName (true);
		ShowFormErrorEmail (true);
		ShowFormErrorUserName (true);
		ShowFormErrorPWD (true);
		ShowFormErrorPWDConf (true);
        GetComponent<ResizeContainer>().GetChildrensAndResize();
    }

	private bool DoesStringContainWhiteSpace(string toBeChecked){
		foreach(char c in toBeChecked){
			if(c == ' '){
				return true;
			}
		}
		return false;
	}
}
