﻿using UnityEngine;
using System.Collections;

public class LanguageSelectionUI : MonoBehaviour {

	private void Start () {
		LangPanelSlideIn ();
	}
	
	public void LangPanelSlideIn(){
		GetComponent<Animation>().GetComponent<Animation>()["LangSelectionUISlideIn"].speed = 1f;
		
		GetComponent<Animation>().Play("LangSelectionUISlideIn");
	}
	
	public void LangPanelSlideOut(){
		GetComponent<Animation>().GetComponent<Animation>()["LangSelectionUISlideIn"].time = GetComponent<Animation>().GetComponent<Animation>()["LangSelectionUISlideIn"].length;
		GetComponent<Animation>().GetComponent<Animation>()["LangSelectionUISlideIn"].speed = -1f;
		
		GetComponent<Animation>().Play("LangSelectionUISlideIn");
	}
}
