﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatsTab : MonoBehaviour {

	private Animator _animator;

	private void Awake () {
		_animator = GetComponent<Animator> ();
	}

	private void Start () {
	
	}
	
	public void SelectedAnim(){
		_animator.Play ("Selected");
	}

	public void DeSelectedAnim(){
		_animator.Play ("DeSelected");
	}
}
