﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControlsPanelButton : MonoBehaviour {

	public Image ControlsIcon;
	public Image ControlsBg;
	public Text ControlsText;
	private Color Black303034 = new Color(0.1176f, 0.1176f, 0.1333f);
	[HideInInspector]
	public bool isOpen = false;

	private void Start () {
		ControlsIcon.color = Color.white;
		ControlsText.color = Color.white;
		ControlsBg.color = Black303034;
	}

	public void ControlPanelState(){
		isOpen = !isOpen;
		if (isOpen) {
			ControlsPanelIsOpen();
		} else {
			ControlsPanelIsClosed();
		}
	}

	private void ControlsPanelIsOpen(){
		ControlsIcon.color = Black303034;
		ControlsText.color = Black303034;
		ControlsBg.color = Color.white;
	}

	public void ControlsPanelIsClosed(){
		ControlsIcon.color = Color.white;
		ControlsText.color = Color.white;
		ControlsBg.color = Black303034;
	}
}
