﻿using UnityEngine;
using System.Collections;

public class InputfieldSlideScreen : MonoBehaviour {
		
	// Assign canvas here in editor
	public Canvas canvas;

	// Set by InputfieldFocused.cs
	[HideInInspector]
	public bool InputFieldActive = false;

	public RectTransform childRectTransform;

#if UNITY_IOS || UNITY_ANDROID
	private Vector2 _initPosition;
	private bool _isDone;

	void Awake(){
		_initPosition = GetComponent<RectTransform> ().anchoredPosition;
	}

	void OnEnable(){
		_isDone = false;
	}

	 float GetAndroidKeyboardAreaHeight(){
//		using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
//			AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject> ("currentActivity").Get<AndroidJavaObject> ("mUnityPlayer").Call<AndroidJavaObject> ("getView");
//		
//			using (AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect")) {
//				View.Call ("getWindowVisibleDisplayFrame", Rct);
//			
//				return (float)(Screen.height - Rct.Call<int> ("height"));
//			}
//		}
		return 0.1f;
	}

	
	void LateUpdate () {
		if (!_isDone && TouchScreenKeyboard.isSupported) {
			if (TouchScreenKeyboard.visible) {

				Vector3[] corners = {Vector3.zero,Vector3.zero,Vector3.zero,Vector3.zero};
				Rect rect = GetScreenRect (corners, childRectTransform);
#if UNITY_IOS
				float keyboardHeight = TouchScreenKeyboard.area.height;
#elif UNITY_ANDROID
				float keyboardHeight = GetAndroidKeyboardAreaHeight();
#endif
				float heightPercentOfKeyboard = keyboardHeight / (Screen.height * 100f);
				float heightPercentOfInput = (Screen.height - (rect.y + rect.height)) / (Screen.height * 100f);

				if (heightPercentOfKeyboard > heightPercentOfInput) {
					// keyboard covers input field so move it up
					float differenceHeightPercent = heightPercentOfKeyboard - heightPercentOfInput;
					float newYPos = (GetComponent<RectTransform> ().rect.height / 100f) * differenceHeightPercent;
				
					Vector2 newAnchorPosition = _initPosition;
					newAnchorPosition.y = newYPos;
					GetComponent<RectTransform> ().anchoredPosition = newAnchorPosition;

					heightPercentOfInput = (Screen.height - (rect.y + rect.height)) / (Screen.height * 100f);

					_isDone = true;
				} else {
					// Keyboard top is below the position of the input field
					GetComponent<RectTransform> ().anchoredPosition = _initPosition;
				}
			}
		}
	}
	
	
	public Rect GetScreenRect(Vector3[] corners,RectTransform rectTransform) {
		rectTransform.GetWorldCorners(corners);
		
		float xMin = float.PositiveInfinity, xMax = float.NegativeInfinity, yMin = float.PositiveInfinity, yMax = float.NegativeInfinity;
		for (int i = 0; i < 4; ++i) {
			// For Canvas mode Screen Space - Overlay there is no Camera; best solution I've found
			// is to use RectTransformUtility.WorldToScreenPoint) with a null camera.
			Vector3 screenCoord = RectTransformUtility.WorldToScreenPoint(null, corners[i]);
			if (screenCoord.x < xMin) xMin = screenCoord.x;
			if (screenCoord.x > xMax) xMax = screenCoord.x;
			if (screenCoord.y < yMin) yMin = screenCoord.y;
			if (screenCoord.y > yMax) yMax = screenCoord.y;
			corners[i] = screenCoord;
		}
		Rect result = new Rect(xMin, Screen.height-yMin - (yMax - yMin), xMax - xMin, yMax - yMin);
		return result;
	}
	#endif
}
