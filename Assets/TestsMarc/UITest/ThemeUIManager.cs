﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct Theme {
	
	public string themeName;
	public Color themeColor;
	public Color themeOutlineColor;
	public Sprite themeIcon;
}

public class ThemeUIManager : MonoBehaviour {

	[SerializeField]
	private OptionsDisplayManager optionsDisplayManager;
	public GameObject Overlay;
	public List<Theme> availableThemes;
	public Transform Panel_Themes;
	public GameObject ThemeButton;
	[HideInInspector]
	public List<GameObject> buttonList;

	private GameObject _tempObject;
	private Vector2 _tempVect2;
	[HideInInspector]
	public List<Toggle> toggleList = new List<Toggle>();
	private bool optionPanlWasOpenned;

	private void Start () {
		optionPanlWasOpenned = false;
		buttonList = new List<GameObject> ();
		if (Panel_Themes != null) {
			FillIconsList ();
		}
		OpenThemePanel ();
	}
	
	public void OpenThemePanel(){

			GetComponent<ThemePanelLerp> ().StartOpenThemePanel ();
			if (optionsDisplayManager != null && optionPanlWasOpenned) {
				optionPanlWasOpenned = false;
				optionsDisplayManager.gameObject.SetActive (true);
				optionsDisplayManager.ForceOpenOptionsPanel ();
			}

			SetAllButtonColorToTheme ();
	}
	
	public void CloseThemePanel(){

		GetComponent<ThemePanelLerp> ().StartCloseThemePanel ();
		if(optionsDisplayManager != null && optionsDisplayManager.gameObject.activeInHierarchy){
			optionPanlWasOpenned = true;
			optionsDisplayManager.ForceCloseOptionsPanel ();
		}
	}

	public void SetThemebarActive(bool active){
		Panel_Themes.gameObject.SetActive (active);
	}

	//select the projects displayed in the theme bar
	public void SelectDisplayedProject(List<string> theSelectedProjects, bool active){
		//list of theme button
		foreach(GameObject go in buttonList){
            if (go != null && go.GetComponent<ThemeUIButtonManager>() != null)
            {
                foreach (GameObject gob in go.GetComponent<ThemeUIButtonManager>().ProjectListDisplay)
                {
                    if (gob != null && gob.GetComponent<ThemeProjectDisplayID>() != null &&
                        theSelectedProjects.Contains(gob.GetComponent<ThemeProjectDisplayID>().projectDisplayID))
                    {
                        gob.SetActive(active);
                    }
                }
            }
		}
	}

	private void FillIconsList(){
		int i = 0;
		toggleList.Clear ();
		foreach(Theme theme in availableThemes){
			_tempObject = (GameObject)Instantiate(ThemeButton);
			_tempObject.SetActive(true);
			//set icon
			_tempObject.transform.Find("iconBt").Find("icon").GetComponent<Image>().sprite = theme.themeIcon;
			//set color
			_tempObject.transform.Find("iconBt").Find("icon").GetComponent<Image>().color = theme.themeColor;

			//set parent
			_tempObject.transform.SetParent(Panel_Themes);
			_tempObject.GetComponent<RectTransform>().localScale = new Vector3(1f,1f,1f);
			_tempVect2.x = 0f;
			_tempVect2.y = i*(-40f);
			_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
			_tempObject.GetComponent<ThemeUIButtonManager>().IndexInList = i;
			i++;

			//save color
			_tempObject.GetComponent<ThemeUIButtonManager>().ThemeColor = theme.themeColor;

			_tempObject.GetComponent<ThemeUIButtonManager>().SetProjectListData(theme.themeColor, theme.themeName);
			toggleList.Add (_tempObject.transform.Find ("ProjectList").Find ("Header").Find ("Background").GetComponent<Toggle> ());
			buttonList.Add(_tempObject);
		}
		foreach (GameObject go in buttonList) {
			foreach (GameObject gob in go.GetComponent<ThemeUIButtonManager>().ProjectListDisplay) {
				gob.SetActive (false);
			}
		}
	}

	public void DeactivateAllProjectList(){
		foreach(GameObject go in buttonList){
			go.transform.Find ("ProjectList").gameObject.SetActive (false);
		}
	}

	public void SetAllButtonColorToTheme(){
		if(buttonList.Count > 0){
			foreach(GameObject go in buttonList){
				if(go != null && go.GetComponent<ThemeUIButtonManager>() != null){
					go.GetComponent<ThemeUIButtonManager>().SetColortoTheme();
				}
			}
		}
	}
}
