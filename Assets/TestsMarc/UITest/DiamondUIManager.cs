﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DiamondUIManager : MonoBehaviour {

	//parent for blocks
	public RectTransform GraphBlockHolder;
	public RectTransform Header;
	public RectTransform Texts;
	public RectTransform VerticalLine;
	public RectTransform WaterfallGraph;
	public GameObject GraphLines;
	public GameObject SmallGraphCircles;

	[HideInInspector]
	public bool _isOpen;

	private List<GameObject> _graphLines = new List<GameObject>();
	private List<GameObject> _smallGraphCircle = new List<GameObject>();
	private List<Vector3> _diamondValuesPoints = new List<Vector3>();
	private float[] _diamondValues;
	private float _YValue;
	private float _mult;
	private float _YValue1;

	private void Start () {
	
	}

	public void OpenPanel(){
		if (!_isOpen) {
			_isOpen = true;
			SetPanelValues ();

			if(WaterfallGraph.localScale.x > 0f){
				WaterfallGraph.GetComponent<WaterfallUIManager>().ClosePanel();
			}
			
			GetComponent<Animation> ().GetComponent<Animation>() ["DiamondGraph"].speed = 1f;
			
			GetComponent<Animation> ().Play ("DiamondGraph");
		} else {
			ClosePanel();
		}
	}
	
	public void ClosePanel(){
		_isOpen = false;
		GetComponent<Animation>().GetComponent<Animation>()["DiamondGraph"].time = GetComponent<Animation>().GetComponent<Animation>()["DiamondGraph"].length;
		GetComponent<Animation>().GetComponent<Animation>()["DiamondGraph"].speed = -1f;
		
		GetComponent<Animation>().Play("DiamondGraph");
		
		DestroyGraphElements ();
	}

	private void DestroyGraphElements(){
		
		if(_graphLines != null && _graphLines.Count > 0){
			foreach(GameObject go in _graphLines){
				Destroy(go);
			}
			_graphLines.Clear();
		}
		if(_smallGraphCircle != null && _smallGraphCircle.Count > 0){
			foreach(GameObject go in _smallGraphCircle){
				Destroy(go);
			}
			_smallGraphCircle.Clear();
		}
		Invoke ("DeactivateDiamond", 0.15f);
	}

	private void DeactivateDiamond(){
		gameObject.SetActive (false);
	}

	private void SetPanelValues(){

		SetArrayValues ();
		ConvertValueToPoint ();
		SetGraph ();
	}

	private void SetArrayValues(){
		//set array size
		_diamondValues = new float[6];
		//fill array - just for testing
		for (int i=0; i< _diamondValues.Length; i++) {
			_diamondValues[i] = Random.Range(0f, 5f);
//			Debug.Log("SetArrayValues(), _diamondValues["+i+"] = "+_diamondValues[i]);
//			_diamondValues[i] = i;
		}
	}

	private void SetGraph(){

		//instantiate normal line
		for (int i = 0; i < _diamondValuesPoints.Count; i++) {
			
			GameObject tempLine = Instantiate (GraphLines) as GameObject;
			
			//set parent
			tempLine.transform.SetParent (GraphBlockHolder);
			
			//scale
			tempLine.transform.localScale = new Vector3 (1f, 1f, 1f);

			int A;

			//set start pos
			tempLine.GetComponent<RectTransform> ().anchoredPosition3D = _diamondValuesPoints[i];
			if(i == _diamondValuesPoints.Count-1){
				A = 0;
			}else{
				A = i+1;
			}

			//set rotation
			Vector3 differenceVector = _diamondValuesPoints[A] - _diamondValuesPoints[i];
			float angle = Mathf.Atan2 (differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
			tempLine.GetComponent<RectTransform> ().rotation = Quaternion.Euler (0, 0, 360f + angle);
			
			//set length
			float dist = Vector3.Distance (_diamondValuesPoints[i], _diamondValuesPoints[A]);
			tempLine.GetComponent<RectTransform> ().sizeDelta = new Vector2 (dist, 2f);
			
			//set color
			tempLine.GetComponent<Image> ().color = Color.yellow;
			
			//save in list
			_graphLines.Add (tempLine);
		}
		
		for (int i = 0; i < _diamondValuesPoints.Count; i++) {
			
			GameObject tempSmallC = Instantiate (SmallGraphCircles) as GameObject;
			
			//set position
			tempSmallC.GetComponent<RectTransform> ().anchoredPosition3D = _diamondValuesPoints[i];

			//set parent
			tempSmallC.transform.SetParent (GraphBlockHolder);
			
			//scale
			tempSmallC.transform.localScale = new Vector3 (1f, 1f, 1f);
			
			//save in list
			_smallGraphCircle.Add (tempSmallC);

		}
	}

	private void ConvertValueToPoint(){
		_diamondValuesPoints.Clear ();

//		float HalfLineHeight = VerticalLine.sizeDelta.y / 2;

		//for first value - vertical line
		_diamondValuesPoints.Add(new Vector3(-1f, (((34f) * _diamondValues[0]) -8f), 0f));

		//for second value - -60deg line
		_diamondValuesPoints.Add(new Vector3(-1f +(29f * _diamondValues[1]), -8f + ((17f) * _diamondValues[1]), 0f));
		
		//for third value - 60deg line
		_diamondValuesPoints.Add(new Vector3(-1f +(29 * _diamondValues[2]), -8f - ((17f) * _diamondValues[2]), 0f));
		
		//for fourth value - vertical line
		_diamondValuesPoints.Add(new Vector3(-1f, (-8f - ((34f) * _diamondValues[3])), 0f));
		
		//for fifth value - -60deg line
		_diamondValuesPoints.Add(new Vector3(-1f - (29f * _diamondValues[4]), -8f - ((17f) * _diamondValues[4]), 0f));
		
		//for sixth value - 60deg line
		_diamondValuesPoints.Add(new Vector3(-1f - (29f * _diamondValues[5]), -8f + ((17f) * _diamondValues[5]), 0f));
	}
}
