﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HotspotProject : MonoBehaviour {

	public float Icon_Xpos = 35;
	public bool DontSetNameFromCode;
	public string ProjectName;
	public string ProjectID;
	public GameObject Image_Header;
	public GameObject Empty_Center;
	public GameObject Image_Icon;
	public GameObject Image_Stroke;

	private CanvasHotspots _canvasHotspots;
	private Vector2 _tempVect2;
	private Vector3 _tempVect3;
	private float _offset = 3f;
	private const float _themeIconYPos = 1f;

	
	private void Awake(){
		_canvasHotspots = transform.parent.GetComponent<CanvasHotspots> ();
	}

	private void Start () {
		SetIcons_Name_Colors ();
	}

	private void SetIcons_Name_Colors(){
		float previousX = 0;
		HospotData tempData = _canvasHotspots.GetHotspotData(ProjectID);
		GameObject _tempObject;
		GameObject _tempStroke;
		Vector2 _tempVectSize;
		for(int i=0; i < tempData.HotspotIconList.Count; i++){

			//////set icon and color
			_tempObject = (GameObject)Instantiate(Image_Icon);
			_tempObject.SetActive(true);
			
			//set icon
			_tempObject.GetComponent<Image>().sprite = tempData.HotspotIconList[i];
			
			//set color
			_tempObject.GetComponent<Image>().color = tempData.HotspotIconColorList[i];
			
			//set parent
			_tempObject.transform.SetParent(Image_Header.transform);
			_tempObject.transform.SetAsLastSibling();
			
			//set position
			_tempObject.transform.localScale = new Vector3(1f,1f,1f);
			_tempObject.transform.localRotation = Quaternion.identity;
			_tempVect2.y = _themeIconYPos;
			_tempVect2.x = previousX;
			_tempObject.GetComponent<RectTransform>().anchoredPosition = _tempVect2;
			_tempVect3 = _tempObject.GetComponent<RectTransform>().anchoredPosition3D;
			_tempVect3.x -= Icon_Xpos;
			_tempVect3.z = 0;
			_tempObject.GetComponent<RectTransform>().anchoredPosition3D = _tempVect3;

			previousX += _tempObject.GetComponent<RectTransform>().rect.width + _offset;

			//////set stroke and color
			_tempStroke = (GameObject)Instantiate(Image_Stroke);
			_tempStroke.SetActive(true);

			//set color
			_tempStroke.GetComponent<Image>().color = tempData.HotspotIconColorList[i];

			//set parent
			_tempStroke.transform.SetParent(Empty_Center.transform);
			_tempStroke.transform.SetAsLastSibling();

			//set position
			_tempStroke.transform.localScale = new Vector3(1f,1f,1f);
			_tempStroke.transform.localRotation = Quaternion.identity;
			// +5 width, +5 height
			_tempVectSize.y = _tempStroke.GetComponent<RectTransform>().sizeDelta.y + (i * 5f);
			_tempVectSize.x = _tempStroke.GetComponent<RectTransform>().sizeDelta.x + (i * 5f);
			//Debug.Log("_tempVectSize = "+_tempVectSize);
			_tempStroke.GetComponent<RectTransform>().sizeDelta = _tempVectSize;

			_tempStroke.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
		}
		
		//set project name
		if(!DontSetNameFromCode){
			Image_Header.transform.Find ("Text_Title").GetComponent<Text> ().text = ProjectName;
		}
	}
	
}
