﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RealisationLogin : MonoBehaviour {

	public ProjectUIProjectPart projectUIProjectPart;
	public GlobalErrorManager globalErrorManager;
	public GameObject closeButton;
	public GameObject returnButton;
	public GameObject projectSummary;

	private void Start () {
	
	}

	private void CheckIfUserLoggedIn(){
//		Debug.Log ("GameObject.FindGameObjectWithTag (WebServices).GetComponent<Toolz.WebServices.User> ().idUser = "+GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser);
		if (GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser != -1) {

			GetComponent<Button>().onClick.RemoveAllListeners();
			GetComponent<Button>().onClick.AddListener(() => closeButton.SetActive (false));
			GetComponent<Button>().onClick.AddListener(() => projectSummary.SetActive (false));
			GetComponent<Button>().onClick.AddListener(() => returnButton.SetActive (true));
			GetComponent<Button>().onClick.AddListener(() => projectUIProjectPart.ActivateRealisation());


		} else {
			globalErrorManager.gameObject.SetActive(true);
			GetComponent<Button>().onClick.RemoveAllListeners();
			GetComponent<Button>().onClick.AddListener(() => globalErrorManager.OpenGlobalErrorPanel());
		}
	}

	private void OnEnable(){
		CheckIfUserLoggedIn ();
	}
}
