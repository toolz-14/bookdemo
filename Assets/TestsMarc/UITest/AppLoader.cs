﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AppLoader : MonoBehaviour {

    [SerializeField]
    private bool showProfilePanelOnStart;

    [SerializeField]
	private GameObject loginPanel;
    [SerializeField]
    private GameObject splashPanel;

    

    private void Start () {
        if (showProfilePanelOnStart)
        {
            loginPanel.SetActive(true);
            splashPanel.SetActive(false);
            loginPanel.GetComponent<LoaderProfileUI>().SwapView(0);
        }
	}

	public void LauchSplash(){
        splashPanel.SetActive(true);
        StartCoroutine ("Load");
	}

	IEnumerator Load() {
		yield return new WaitForSeconds(1.5f);
        splashPanel.SetActive(false); 
    }
}
