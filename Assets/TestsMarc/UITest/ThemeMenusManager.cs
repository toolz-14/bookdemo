﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThemeMenusManager : MonoBehaviour {

	public List<GameObject> MenusList = new List<GameObject>();
	public bool MenuProfileActive = true;
	public bool MenuStatsActive = true;
	public bool MenuMapsActive = true;
	public bool MenuOptionsActive = true;
	public bool MenuCompassActive = true;
	public bool MenuTopViewActive = true;
	public bool MenuTimelineActive = true;

	private void Start () {
		SetMenus ();
	}

	private void SetMenus(){
		if (MenusList.Count > 0) {
			MenusList [0].SetActive (MenuProfileActive);
			MenusList [1].SetActive (MenuStatsActive);
			MenusList [2].SetActive (MenuMapsActive);
			MenusList [3].SetActive (MenuOptionsActive);
			MenusList [4].SetActive (MenuCompassActive);
			MenusList [5].SetActive (MenuTopViewActive);
			MenusList [6].SetActive (MenuTimelineActive);
		}
    }
}
