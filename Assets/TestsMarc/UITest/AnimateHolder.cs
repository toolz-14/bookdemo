﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class AnimateHolder : MonoBehaviour {

	public GameObject ResponseManager;
	
	private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	public float Duration1 = 0.1f;

	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	public float Duration2 = 0.3f;
	
	private Vector2 _from3;
	private Vector2 _to3;
	private bool _launch3;
	private float _timeSinceTransitionStarted3;
	private float _t3;
	public float Duration3 = 0.3f;

	private Vector2 _from4;
	private Vector2 _to4;
	private bool _launch4;
	private float _timeSinceTransitionStarted4;
	private float _t4;
	public float Duration4 = 0.3f;

	private float _from5;
	private float _to5;
	private bool _launch5;
	private float _timeSinceTransitionStarted5;
	private float _t5;
	public float Duration5 = 0.26f;

	private Vector2 _startPos;

	public UnityEngine.Events.UnityAction OnDiscussionLerpedBack;

	private void Start () {
		_startPos = GetComponent<RectTransform> ().anchoredPosition;
	}

	private void Update () {
		if(_launch){
			LerpToHalfCanvas();
		}
		if(_launch2){
			LerpToFullCanvas();
		}
		if(_launch3){
			LerpBackToHAlf();
		}
		if(_launch4){
			LerpBackToFull();
		}
		if (_launch5) {
			ResizeViewport ();
		}
	}

	private void StartResize(bool isOpening){
		_timeSinceTransitionStarted5 = 0f;
		if (isOpening) {
			_from5 =  Toolz.Managers.LevelManager.ActiveCamera.rect.width;
			_to5 = 0.5f;
		} else {
			_from5 = Toolz.Managers.LevelManager.ActiveCamera.rect.width;
			_to5 = 0.75f;
		}
		_launch5 = true;
	}

	private void Resize(){
		StartResize (true);
	}
	
	private void ResizeViewport(){
		_timeSinceTransitionStarted5 += Time.deltaTime;
		_t = _timeSinceTransitionStarted5 / Duration5;
		
		Toolz.Managers.LevelManager.ActiveCamera.rect = new Rect (0, 0, Mathf.Lerp (_from5, _to5, _t), 1f);
		if( Mathf.Abs(Toolz.Managers.LevelManager.ActiveCamera.rect.width - _to5) < 0.001f){
			_launch5 = false;
		}
	}

	public void StartLerpToHalfCanvas(){
		_timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().anchoredPosition;
//		Debug.Log ("transform.parent.GetComponent<RectTransform> ().sizeDelta.x = "+transform.parent.GetComponent<RectTransform> ().sizeDelta.x);
		_to1 = new Vector2 (280f, GetComponent<RectTransform> ().anchoredPosition.y); //transform.parent.GetComponent<RectTransform> ().sizeDelta.x
		_launch = true;
	}

	private void LerpToHalfCanvas(){
		
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f){
			_launch = false;
		}
	}

	public void StartLerpToFullCanvas(){
		_timeSinceTransitionStarted2 = 0f;
		_from2 = GetComponent<RectTransform> ().anchoredPosition;
		_to2 = new Vector2 (0, GetComponent<RectTransform> ().anchoredPosition.y);
		_launch2 = true;
	}
	
	private void LerpToFullCanvas(){
		
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
		}
	}

	public void StartLerpBackToHAlf(){
		_timeSinceTransitionStarted3 = 0f;
		_from3 = GetComponent<RectTransform> ().anchoredPosition;
		_to3 = new Vector2 (transform.parent.GetComponent<RectTransform> ().sizeDelta.x/2f, GetComponent<RectTransform> ().anchoredPosition.y);
		_launch3 = true;
	}
	
	private void LerpBackToHAlf(){
		
		_timeSinceTransitionStarted3 += Time.deltaTime;
		_t3 = _timeSinceTransitionStarted3 / Duration3;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from3, _to3, _t3);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to3) < 0.01f){
			_launch3 = false;
			//disable response
			ResponseManager.SetActive(false);
		}
	}

	public void StartLerpBackToFull(){
		_timeSinceTransitionStarted4 = 0f;
		_from4 = GetComponent<RectTransform> ().anchoredPosition;
		_to4 = _startPos;
		_launch4 = true;

	}
	
	private void LerpBackToFull(){
		//StartResize (false);
		_timeSinceTransitionStarted4 += Time.deltaTime;
		_t4 = _timeSinceTransitionStarted4 / Duration4;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from4, _to4, _t4);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to4) < 0.01f){
			_launch4 = false;
			if (OnDiscussionLerpedBack != null) {
				OnDiscussionLerpedBack();
			}
		}
	}
}
