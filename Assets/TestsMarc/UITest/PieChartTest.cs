﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
//using Vectrosity;
using Toolz.WebServices;

public struct ProjectAndInvestment {
	
	public string projectName;
	public float investmentAmount;
}

public struct ArcValues {
//
//	public VectorLine pieArc;
//	public float startDegree;
//	public float endDegree;
//	public Color col;
//	public bool isSelected;
//	public string themeName;
}

public class PieChartTest : MonoBehaviour {
//
//	VectorLine pieArc;
//	VectorLine selectedPieArc;
//
//	VectorLine pieArc2;
//
//	VectorLine pieOutline;

//	float _arcRadius = 80f;
//	float _arcWidth = 20f;
//	float _spaceBetxeenArcs = 2f;

//	float _selectedArcRadius = 96f;
//	float _selectedArcWidth = 10f;
//
//	float _outlineRadius = 100f;
//	float _outlineWidth = 2f;

	private bool isSelected;

//	Vector2[] linePoints = new Vector2[2];

	public Transform PieCharText;
	public StatsUIManager statsUIManager;
	private int _segmentIndex;
//	ArcValues tempstruct = new ArcValues();

	private List<ArcValues> _valuesForAllArcs = new List<ArcValues>();
	private Dictionary<string, List<ProjectAndInvestment> > _projectsInvestment = new Dictionary<string, List<ProjectAndInvestment> >();

    private GetUserData _getUserDataService;
    private GetAverageInvestmentPerProject _getAverageInvestmentPerProjectService;

	private void Start () {

	}

	public void ErasethePie(){
//		VectorLine tempLine;
//		if (_valuesForAllArcs != null && _valuesForAllArcs.Count > 0) {
//			for(int j = 0; j < _valuesForAllArcs.Count; j++){
//				tempLine = _valuesForAllArcs[j].pieArc;
//				VectorLine.Destroy (ref tempLine);
//			}
//		}
//		if(pieOutline != null){
//			VectorLine.Destroy (ref pieOutline);
//		}
//		if(selectedPieArc != null){
//			VectorLine.Destroy (ref selectedPieArc);
//		}
	}

	private void GetInvestmentPerTheme(bool isAllUsers){
		_projectsInvestment.Clear ();
		ProjectAndInvestment temp = new ProjectAndInvestment();

		//for that theme get all the projects
		foreach (Project project in statsUIManager.GetAllProjects()) {
            if(isAllUsers){
                if(!_getAverageInvestmentPerProjectService.ProjectsAverageInvestment.ContainsKey(project.projectId)){
                    continue;
				}
                project.AllUserInvestmentAverage = _getAverageInvestmentPerProjectService.ProjectsAverageInvestment[project.projectId];
            }
            else {
                if (!_getUserDataService.ProjectsData.ContainsKey(project.projectId))
                    continue;
                Toolz.WebServices.ProjectData projectData = _getUserDataService.ProjectsData[project.projectId];
                project.CurrentUserInvestment = projectData.Investment;
            }

			foreach (string activeTheme in project.activeThemes) {

				if(!_projectsInvestment.ContainsKey(activeTheme)){
					_projectsInvestment[activeTheme] = new List<ProjectAndInvestment>();
				}
				temp.projectName = project.projectName;

				if(isAllUsers){
					temp.investmentAmount = (float)(project.AllUserInvestmentAverage / project.activeThemes.Count);
				}else{
					temp.investmentAmount = (float)(project.CurrentUserInvestment / project.activeThemes.Count);
				}

				_projectsInvestment[activeTheme].Add(temp);
			}
		}
	}

	private float CalculateTotalInvestment(){
		float totalInvestment = 0;
		foreach (string theme in _projectsInvestment.Keys) {
			foreach (ProjectAndInvestment stru in _projectsInvestment[theme]) {
				totalInvestment += stru.investmentAmount;
			}
		}
		return totalInvestment;
	}

	public void DrawThePie(){
		GameObject webservice = GameObject.FindGameObjectWithTag("WebServices");

		_valuesForAllArcs.Clear ();
		//depending on the overview tab selected
		if(statsUIManager._isAllUsers){
			_getAverageInvestmentPerProjectService = webservice.GetComponent<GetAverageInvestmentPerProject>();
			_getAverageInvestmentPerProjectService.onComplete += _getAverageInvestmentPerProjectService_onComplete;
			_getAverageInvestmentPerProjectService.onError += _getAverageInvestmentPerProjectService_onError;
            _getAverageInvestmentPerProjectService.UseWebService();
		}else{
			_getUserDataService = webservice.GetComponent<GetUserData>();
			_getUserDataService.onComplete += _getUserDataService_onComplete;
			_getUserDataService.onError += _getUserDataService_onError;
            _getUserDataService.UseWebService();
		}
	}

    //Called by webservice callback
    private void ActuallyDrawThePie(bool isForAllUsers) {
//        float totalInvestment = CalculateTotalInvestment();
//        float themeInvestment;
//        float themePercent;
//        float themeArcSizeDegree;
//        linePoints[0] = new Vector2(0, 0);
//        linePoints[1] = new Vector2(400f, 400f);
//        int i = 0;
//        float lastArcMaxDegree = 0;
//        float arcMinDegree = 0;
//        float arcMaxDegree = 0;
//        ArcValues tempstruct = new ArcValues();
//        Color tempColor;
//		bool noInvestments = true;
//
//        foreach (string theme in _projectsInvestment.Keys) {
//
//            themeInvestment = 0;
//            foreach (ProjectAndInvestment stru in _projectsInvestment[theme]) {
//                themeInvestment += stru.investmentAmount;
//            }
//            if (totalInvestment > 0) {
//                //Debug.Log("DrawThePie(), theme = "+theme);
//                //Debug.Log("DrawThePie(), themeInvestment = "+themeInvestment);
//                //Debug.Log("DrawThePie(), totalInvestment = "+totalInvestment);
//                themePercent = (100 * themeInvestment / (totalInvestment));
//
//                themeArcSizeDegree = themePercent * 3.6f;
//
//                arcMinDegree = lastArcMaxDegree;
//                arcMaxDegree = arcMinDegree + themeArcSizeDegree;
//
//                // get data for struct
//				tempstruct.themeName = theme;
//                tempstruct.startDegree = arcMinDegree;
//                tempstruct.endDegree = arcMaxDegree;
//                //Debug.Log("DrawThePie(), arcMinDegree = "+arcMinDegree);
//                //Debug.Log("DrawThePie(), arcMaxDegree = "+arcMaxDegree);
//
//
//                pieArc = new VectorLine("Arc_" + i.ToString(), linePoints, null, _arcWidth, LineType.Discrete, Joins.Weld);
//                pieArc.rectTransform.SetParent(PieCharText);
//                pieArc.rectTransform.localScale = new Vector3(1f, 1f, 1f);
//                pieArc.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
//                pieArc.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
//                pieArc.rectTransform.pivot = new Vector2(0.5f, 0.5f);
//                pieArc.rectTransform.anchoredPosition = Vector2.zero;
//
//                pieArc.Resize(pieArc.points2.Count + 1000);
//                pieArc.MakeArc(Vector2.zero, _arcRadius, _arcRadius, arcMinDegree, (arcMaxDegree - _spaceBetxeenArcs));
//
//                //set color from theme
//                tempColor = statsUIManager.GetThemeColor(theme);
//                //get color for struct
//                tempstruct.col = tempColor;
//
//                pieArc.SetColor(tempColor);
//				pieArc.collider = true;
//                pieArc.Draw();
//
//                // get arc for struct
//                tempstruct.pieArc = pieArc;
//
//                //set struct boolean
//                tempstruct.isSelected = false;
//
//                //update min position for next arc
//                lastArcMaxDegree = arcMaxDegree;
//
//				//create caption line and angled line
//				statsUIManager.SetAPieViewCaption(theme, arcMinDegree, arcMaxDegree, tempColor, themeInvestment, i);
//                //update i for the arc name
//                i++;
//
//                //add to list for use when selected
//                _valuesForAllArcs.Add(tempstruct);
//
//				noInvestments = false;
//            }
//        }
//		if(noInvestments){
//			statsUIManager.SetPieCenterCaption ("noInvestments");
//		}
//
//        //draw the outline circle
//        pieOutline = new VectorLine("Outline", linePoints, null, _outlineWidth, LineType.Discrete, Joins.Weld);
//        pieOutline.rectTransform.SetParent(PieCharText);
//        pieOutline.rectTransform.localScale = new Vector3(1f, 1f, 1f);
//        pieOutline.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
//        pieOutline.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
//        pieOutline.rectTransform.pivot = new Vector2(0.5f, 0.5f);
//        pieOutline.rectTransform.anchoredPosition = Vector2.zero;
//        pieOutline.Resize(pieOutline.points2.Count + 5000);
//        pieOutline.MakeCircle(Vector2.zero, _outlineRadius);
//        pieOutline.SetColor(new Color32(63, 113, 120, 255));
//        pieOutline.Draw();
//
//        Destroy(GameObject.Find("VectorCanvas"));
    }

	private void DrawSelected(float startDegree, float endDegree, Color col, string themeName){
//
//		selectedPieArc = new VectorLine("selectedLine", linePoints, null, _selectedArcWidth, LineType.Discrete, Joins.Weld);		
//		selectedPieArc.rectTransform.SetParent (PieCharText);
//		selectedPieArc.rectTransform.localScale = new Vector3 (1f,1f,1f);
//		selectedPieArc.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
//		selectedPieArc.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
//		selectedPieArc.rectTransform.pivot = new Vector2(0.5f, 0.5f);
//		selectedPieArc.rectTransform.anchoredPosition = Vector2.zero;
//		
//		selectedPieArc.Resize (selectedPieArc.points2.Count + 5000);
//		selectedPieArc.MakeArc (Vector2.zero, _selectedArcRadius, _selectedArcRadius, startDegree, (endDegree - _spaceBetxeenArcs));
//		//modify color alpha
//		col.a = (110f/255f);
//		selectedPieArc.SetColor (col);
//
//		selectedPieArc.Draw ();
//
//		statsUIManager.SetPieCenterCaption (themeName);
//		statsUIManager.SetInvestmentDetailView (true, themeName, _projectsInvestment);
	}

	public void EraseSelected(){
//		if (selectedPieArc != null) {
//			VectorLine.Destroy (ref selectedPieArc);
//			statsUIManager.SetPieCenterCaption("");
//			//since first param is false, don't need other params
//			statsUIManager.SetInvestmentDetailView (false, "", null);
//		}
	}

	private void Update () {
//		if (Input.GetMouseButtonDown (0) && statsUIManager.gameObject.GetComponent<CanvasGroup>().alpha > 0) {
//			if(_valuesForAllArcs != null && _valuesForAllArcs.Count > 0){
//				for(int j = 0; j < _valuesForAllArcs.Count; j++){
//					if(_valuesForAllArcs[j].pieArc != null && _valuesForAllArcs[j].pieArc.Selected (Input.mousePosition - _valuesForAllArcs[j].pieArc.rectTransform.position, 28, out _segmentIndex)) {
//						//Debug.Log("pieArc Selected");
//						_segmentIndex += 1;
//						EraseSelected();
//						if(!_valuesForAllArcs[j].isSelected){
//							tempstruct = _valuesForAllArcs[j];
//							tempstruct.isSelected = true;
//							_valuesForAllArcs[j] = tempstruct;
//							DrawSelected(_valuesForAllArcs[j].startDegree, _valuesForAllArcs[j].endDegree, _valuesForAllArcs[j].col, _valuesForAllArcs[j].themeName);
//						}else{
//							tempstruct = _valuesForAllArcs[j];
//							tempstruct.isSelected = false;
//							_valuesForAllArcs[j] = tempstruct;
//						}
//					}
//				}
//			}
//		}
	}


    private void _getAverageInvestmentPerProjectService_onError(long status, string message) {
        Debug.LogError("Get average user investment ERROR");
		_getAverageInvestmentPerProjectService.onError -= _getAverageInvestmentPerProjectService_onError;
    }

    private void _getAverageInvestmentPerProjectService_onComplete(long status, string message) {
		_getAverageInvestmentPerProjectService.onComplete -= _getAverageInvestmentPerProjectService_onComplete;
		Debug.Log("Get average user investment OK");
        GetInvestmentPerTheme(true);
        ActuallyDrawThePie(true);
    }

    private void _getUserDataService_onError(long status, string message) {
        Debug.LogError("Get user data ERROR");
		_getUserDataService.onError -= _getUserDataService_onError;
    }

    private void _getUserDataService_onComplete(long status, string message) {
        Debug.Log("Get Users data OK");
		_getUserDataService.onComplete -= _getUserDataService_onComplete;
		GetInvestmentPerTheme(false);
        ActuallyDrawThePie(false);
    }
}
