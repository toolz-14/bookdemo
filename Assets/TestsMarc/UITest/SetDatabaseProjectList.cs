﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Toolz.WebServices;
#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode]
#endif
public class SetDatabaseProjectList : MonoBehaviour {
	
    private PostProjects _postProjectsService;
	private void Start () {

        _postProjectsService = GameObject.FindGameObjectWithTag("WebServices").GetComponent<PostProjects>();
        _postProjectsService.onComplete += _postProjectsService_onComplete;
        _postProjectsService.onError += _postProjectsService_onError;
	}

    private void OnEnable() {
        if(_postProjectsService == null)
            _postProjectsService = GameObject.FindGameObjectWithTag("WebServices").GetComponent<PostProjects>();
    }

	private void GetAllProjectsID(){
		Project_Fiche[] allprojectsFiche = null;
		Project[] allprojects = GameObject.FindObjectsOfType<Project> ();
		if(GameObject.FindObjectOfType<Project_Fiche>() != null){
			allprojectsFiche = GameObject.FindObjectsOfType<Project_Fiche> ();
		}

        List<string> idProjects = _postProjectsService.IdProjects;
        idProjects.Clear();
		foreach (Project pr in allprojects) {
			//Debug.Log(pr.projectId);
            idProjects.Add(pr.projectId);
		}
		if (GameObject.FindObjectOfType<Project_Fiche> () != null) {
			foreach (Project_Fiche pr in allprojectsFiche) {
				//Debug.Log(pr.projectId);
				idProjects.Add(pr.projectId);
			}
		}
        _postProjectsService.UseWebService();
	}

	public void SendProjectsIDToDatabase(){
		GetAllProjectsID ();
	}

    //WEBSERVICE CALLBACKS
    private void _postProjectsService_onError(long status, string message) {
        Debug.LogError("Post projects service: ERROR");
		#if UNITY_EDITOR
        EditorUtility.DisplayDialog("Post projects", "Failed to push projects: " + message , "OK");
		#endif
    }

    private void _postProjectsService_onComplete(long status, string message) {
        Debug.Log("Post projects service: OK");
		#if UNITY_EDITOR
        EditorUtility.DisplayDialog("Post projects", "Successfully pushed projects.", "OK");
		#endif
    }
}
