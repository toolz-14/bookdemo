﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingSceneManager : MonoBehaviour {

	public bool SecureBuild_mail = false;
	public bool SecureBuild_code = false;
	public bool SecureBuild_mailandcode = false;

	public Transform SecurityCanavas;
	private AppLoader LoadPanel;

	private void Awake() {
		LoadPanel = GameObject.Find("Canvas_Loader").transform.Find("Panel_Load").GetComponent<AppLoader>();
	}

	private void Start () {
		if (SecureBuild_mail && SecureBuild_code) {
			Debug.LogWarning("both booleans cannot be true at the same time");
		}else if (SecureBuild_mail) {
			SecurityCanavas.Find("Panel_login_mail").gameObject.SetActive (true);
		}else if (SecureBuild_code) {
			Invoke("LoginDelayed", 1f);
		} else if (SecureBuild_mailandcode) {
			SecurityCanavas.Find("Panel_login_mailandCode").gameObject.SetActive (true);
		}else {
			//LoadPanel.LauchCoroutineLoad();
		}
	}

	private void LoginDelayed(){
		SecurityCanavas.Find("Panel_login_code").gameObject.SetActive (true);
	}
}
