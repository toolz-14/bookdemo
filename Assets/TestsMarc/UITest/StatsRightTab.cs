﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StatsRightTab : MonoBehaviour {

	public GlobalErrorManager GlobalErrorManager;
	public StatsUIManager statsUIManager;
	private List<Transform> _firstDepthChildren = new List<Transform>();
	private StatsTab _selectedChild;

	private void Start () {
		GetFirstChildren ();
		SetChildName ();
		_selectedChild = _firstDepthChildren [1].GetComponent<StatsTab>();
		_selectedChild.SelectedAnim ();
		statsUIManager.SetInvestmentOverviewView (true);
	}

	private void GetFirstChildren(){
		Transform[] transforms = GetComponentsInChildren<Transform>();
		
		foreach(Transform tr in transforms){
			if (tr.parent == transform){
				_firstDepthChildren.Add(tr);
			}
		}
	}

	public void SwapSelectedChild(StatsTab newChild){
		if(newChild != _selectedChild){
			
			if (newChild.gameObject.name == "TabBarBt_AllUsers") {
				_selectedChild.DeSelectedAnim ();
				statsUIManager.ResetPieCaptions();
				statsUIManager.DestroyPie ();
				statsUIManager._isAllUsers = true;
				statsUIManager.CreatePie ();

				statsUIManager.SetInvestmentOverviewView (true);
				statsUIManager.SetRightSideTitles ();
				statsUIManager.ResetToggle();
				statsUIManager.ProjectVote_For(false);
				statsUIManager.ProjectVote_Against(false);
				statsUIManager.ProjectVote_Ratio(true);
				newChild.SelectedAnim ();
				_selectedChild = newChild;
			} else {
				//is user logged in, if not global error
				if (GameObject.FindGameObjectWithTag ("WebServices").GetComponent<Toolz.WebServices.User> ().idUser != -1 && newChild.gameObject.name == "TabBarBt_User") { 
					_selectedChild.DeSelectedAnim ();
					statsUIManager.ResetPieCaptions();
					statsUIManager.DestroyPie ();
					statsUIManager._isAllUsers = false;
					statsUIManager.CreatePie ();

					statsUIManager.SetInvestmentOverviewView (false);
					statsUIManager.SetRightSideTitles ();
					statsUIManager.ResetToggle();
					statsUIManager.ProjectVote_For(false);
					statsUIManager.ProjectVote_Against(false);
					statsUIManager.ProjectVote_Ratio(true);
					newChild.SelectedAnim ();
					_selectedChild = newChild;
				} else {
					ShowglobalError ();
				}
			}
		}
	}

	private void ShowglobalError(){
		GlobalErrorManager.gameObject.SetActive (true);
		GlobalErrorManager.OpenGlobalErrorPanel();
	}

	private void SetChildName(){
		if(_firstDepthChildren [0] != null && _firstDepthChildren [0].Find("Text") != null &&
		   _firstDepthChildren [0].Find("Text").GetComponent<Text>() != null){
			_firstDepthChildren [0].Find("Text").GetComponent<Text>().text = "Utilisateur";
		}
		if(_firstDepthChildren [1] != null && _firstDepthChildren [1].Find("Text") != null &&
		   _firstDepthChildren [1].Find("Text").GetComponent<Text>() != null){
			_firstDepthChildren [1].Find("Text").GetComponent<Text>().text = "Tout les utilisateurs";
		}
	}
}
