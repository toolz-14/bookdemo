﻿using UnityEngine;
using System.Collections;

public class ProjectPanelLerp : MonoBehaviour {

    [SerializeField]
    private GameObject ButtonSceneInitPanel;
    [SerializeField]
    private GameObject ButtonHolder_2;
    [SerializeField]
    private GameObject ButtonSchematic;
    [SerializeField]
    private GameObject ButtonLightCycle;
    [SerializeField]
    private GameObject ButtonModeration;
    [SerializeField]
    private ShowHidePanelAtelier showHidePanelAtelier;
    [SerializeField]
    private GameObject panel_InfoZACLabels;

    private Vector2 _from1;
	private Vector2 _to1;
	private bool _launch;
	private float _timeSinceTransitionStarted;
	private float _t;
	private float Duration1 = 0.4f;

	private Vector2 _from2;
	private Vector2 _to2;
	private bool _launch2;
	private float _timeSinceTransitionStarted2;
	private float _t2;
	private float Duration2 = 0.4f;

	private Vector2 _startPos;
    private bool atelierWasOn;
    private bool moderationWasOn;
    private bool infoZACWasOn;

    private void Start () {
		_startPos = GetComponent<RectTransform> ().anchoredPosition;
        atelierWasOn = false;
        moderationWasOn = false;
        infoZACWasOn = false;
    }

	private void Update () {
		if(_launch){
			OpenProjectPanel();
		}
		if(_launch2){
			CloseProjectPanel();
		}
	}


	public void StartLerpToQuaterCanvas(){
        ButtonSchematic.SetActive(false);
        ButtonHolder_2.SetActive(false);
        ButtonLightCycle.SetActive(false);
        ButtonSceneInitPanel.SetActive(false);
        if (ButtonModeration.activeInHierarchy)
        {
            moderationWasOn = true;
            ButtonModeration.SetActive(false);
        }
        if (panel_InfoZACLabels.activeInHierarchy)
        {
            infoZACWasOn = true;
            panel_InfoZACLabels.SetActive(false);
        }
        if (ShowHidePanelAtelier.isAtelierOn)
        {
            atelierWasOn = true;
            showHidePanelAtelier.HidePanel();
        }
        _timeSinceTransitionStarted = 0f;
		_from1 = GetComponent<RectTransform> ().anchoredPosition;
		_to1 = new Vector2 (-GetComponent<RectTransform> ().sizeDelta.x, GetComponent<RectTransform> ().anchoredPosition.y);
		_launch = true;
	}
	
	private void OpenProjectPanel(){
		_timeSinceTransitionStarted += Time.deltaTime;
		_t = _timeSinceTransitionStarted / Duration1;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from1, _to1, _t);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to1) < 0.01f){
			_launch = false;
		}
	}

	public void StartLerpBack(){
		_timeSinceTransitionStarted2 = 0f;
		_from2 = GetComponent<RectTransform> ().anchoredPosition;
		_to2 = _startPos;
		_launch2 = true;
	}
	
	private void CloseProjectPanel(){
		_timeSinceTransitionStarted2 += Time.deltaTime;
		_t2 = _timeSinceTransitionStarted2 / Duration2;
		
		GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_from2, _to2, _t2);
		if(Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, _to2) < 0.01f){
			_launch2 = false;
			GetComponent<ProjectUIManager>().AfterPanelClose();
            ButtonHolder_2.SetActive(true);
            ButtonSchematic.SetActive(true);
            ButtonLightCycle.SetActive(true);
            ButtonSceneInitPanel.SetActive(true);
            if (moderationWasOn)
            {
                moderationWasOn = false;
                ButtonModeration.SetActive(true);
            }
            if (infoZACWasOn)
            {
                infoZACWasOn = false;
                panel_InfoZACLabels.SetActive(true);
            }
            if (atelierWasOn)
            {
                atelierWasOn = false;
                showHidePanelAtelier.ShowPanel();
            }
        }
	}
}
