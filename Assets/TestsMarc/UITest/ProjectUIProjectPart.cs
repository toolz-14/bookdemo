﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProjectUIProjectPart : MonoBehaviour {

	public AnmaAnimCamMove anmaAnimCamMove;
//	public VideoPlayerUI videoPlayerUI;
	public GameObject videoPlayer;
	//public GameObject Context;
	//public GameObject Fonctionnement;
	//public GameObject Realisation;
	//public GameObject CoutBenefice;

	[SerializeField]
	private GameObject loader;
	private Vector3 _savedCameraPos;
	private Quaternion _savedCameraRot;
	private bool _isAnimPlaying;
	private string _myText;
	private bool _show;
	private bool _showWire;
	public RectTransform ContextImage;
	public RectTransform FonctionnementImage;
	public RectTransform CoutBeneficeImage;
	public GameObject Sphere360;

	private void Start () {
		_isAnimPlaying = false;
		_show = false;
		_showWire = false;
	}

	public void SetAllInactive(){
//		if(Context.activeInHierarchy){
//			if(ProjectUIManager.CurrentProjectData.contextData.ShowImageContext && ContextImage.GetComponent<ImageContext_Fiches> ().isOpen){
//				ContextImage.GetComponent<ImageContext_Fiches> ().StartCloseImage();
//			}
//			if (ProjectUIManager.CurrentProjectData.contextData.fixedCamera != null && ProjectUIManager.CurrentProjectData.contextData.fixedCamera.activeInHierarchy) {
//				ProjectUIManager.CurrentProjectData.contextData.CameraSphere.SetActive (true);
//				ProjectUIManager.CurrentProjectData.contextData.fixedCamera.SetActive (false);
//			}
//			Context.SetActive(false);
//		}
//		if(Fonctionnement.activeInHierarchy){

//			if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "ChaufferieBiomasse" ||
//			    ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "PlatformeEffluents") {
//				StopAnim (ProjectUIManager.CurrentProjectData.fonctionnementData.animatedObject, ProjectUIManager.CurrentProjectData.fonctionnementData.animationclipname);
//			} else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "citeduvin" ||
//			         ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "amna") {
//				SwapCamera (false);
//			} else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "BartsCloseFPOL") {
//				Fonctionnement.transform.Find ("ButtonRenovations").gameObject.SetActive (false);
//				ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate .SetActive (false);
//			} else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "BishopGateFPOL") {
				
//			} else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "Wiredemo") {
//				Fonctionnement.transform.Find ("ButtonWireframe").gameObject.SetActive (false);
//				ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate .SetActive (false);
////				GameObject.FindGameObjectWithTag("Wireframe").GetComponent<WireframeButton> ().SetWireframeActive (false);
//			}

//			if(ProjectUIManager.CurrentProjectData.fonctionnementData.ShowImageFonctionnement && FonctionnementImage.GetComponent<ImageFonctionnement> ().isOpen){
//				FonctionnementImage.GetComponent<ImageFonctionnement> ().StartCloseImage();
//			}

//			Fonctionnement.SetActive(false);
//		}
//		if(Realisation.activeInHierarchy){
//			Realisation.SetActive(false);
//		}
//		if(CoutBenefice.activeInHierarchy){
//			if(ProjectUIManager.CurrentProjectData.coutbenefData.ShowImageCoutbenef && CoutBeneficeImage.GetComponent<ImageCoutbenef> ().isOpen){
//				CoutBeneficeImage.GetComponent<ImageCoutbenef> ().StartCloseImage();
//			}
//			if (ProjectUIManager.CurrentProjectData.coutbenefData.MyVideoPlayer != null) {
//				ProjectUIManager.CurrentProjectData.coutbenefData.MyVideoPlayer.StopTheVideo ();
//			}
//			CoutBenefice.SetActive(false);
//		}
	}

    public void ActivateContext()
    {

    }

    private void SetContextData()
    {

    }

    public void ActivateFonctionnement()
    {

    }

    private void SetFonctionnementData()
    {

    }

    public void ActivateCoutBenefice()
    {

    }

    private void SetCoutBeneficeData()
    {

    }

    public void ActivateRealisation()
    {

    }

    private void SetRealisationData()
    {

    }

    //	public void ActivateContext(){
    //		if (ProjectUIManager.CurrentProjectData.contextData != null) {
    //			SetAllInactive ();
    //			SetContextData ();
    //			Context.SetActive (true);
    //		}
    //	}

    //	private void SetContextData(){

    //		//set title
    //		_myText = ProjectUIManager.CurrentProjectData.contextData.TitleContext;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		Context.transform.Find ("Scroller").Find ("Container").Find ("BackGround").Find ("Title").GetComponent<Text> ().text = _myText;

    //		//set text
    //		_myText = ProjectUIManager.CurrentProjectData.contextData.TextContext;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		Context.transform.Find ("Scroller").Find ("Container").Find ("Text").GetComponent<Text> ().text = _myText;

    ////		Debug.Log ("ProjectUIManager.CurrentProjectData.contextData.VideoImage = "+ProjectUIManager.CurrentProjectData.contextData.VideoImage);
    //		//if null there is no video
    //		if (ProjectUIManager.CurrentProjectData.contextData.VideoImage != null) {
    //			//set video image
    //			Context.transform.Find ("Scroller").Find ("Container").Find ("Button_Video").GetComponent<Image> ().sprite = ProjectUIManager.CurrentProjectData.contextData.VideoImage;

    //			//set video button
    //			Context.transform.Find ("Scroller").Find ("Container").Find ("Button_Video").GetComponent<Button> ().onClick.RemoveAllListeners();
    ////			Context.transform.Find ("Scroller").Find ("Container").Find ("Button_Video").GetComponent<Button> ().onClick.AddListener (() => videoPlayerUI.PlayVideo (ProjectUIManager.CurrentProjectData.contextData.VideoName));
    //		} else {
    //			Context.transform.Find ("Scroller").Find ("Container").Find ("Button_Video").gameObject.SetActive(false);
    //		}

    //		if (ProjectUIManager.CurrentProjectData.contextData.ShowImageContext) {
    //			//set image
    //			ContextImage.gameObject.SetActive (true);
    //			ContextImage.GetComponent<ImageContext_Fiches> ().StartOpenImage();
    //		}

    //		if (ProjectUIManager.CurrentProjectData.contextData.fixedCamera != null) {
    //			ProjectUIManager.CurrentProjectData.contextData.CameraSphere.SetActive (false);
    //			ProjectUIManager.CurrentProjectData.contextData.fixedCamera.SetActive (true);
    //			Toolz.Managers.LevelManager.ActiveCamera = ProjectUIManager.CurrentProjectData.contextData.fixedCamera.transform.Find ("ConstraintBox").Find ("CameraHolder").Find ("Camera").GetComponent<Camera> ();
    //		}
    //	}

    //	public void ActivateFonctionnement(){
    //		if (ProjectUIManager.CurrentProjectData.fonctionnementData != null) {
    //			SetAllInactive ();
    //			SetFonctionnementData ();
    //			Fonctionnement.SetActive (true);
    //		}
    //	}

    //	private void SetFonctionnementData(){

    //		//set title
    //		_myText = ProjectUIManager.CurrentProjectData.fonctionnementData.TitleFonctionnement;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		Fonctionnement.transform.Find ("BackGround").Find ("Title").GetComponent<Text> ().text = _myText;

    //		//set text
    //		_myText = ProjectUIManager.CurrentProjectData.fonctionnementData.TextFonctionnement;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		Fonctionnement.transform.Find ("TextContainer").Find ("Text").GetComponent<Text> ().text = _myText;

    //		if (ProjectUIManager.CurrentProjectData.fonctionnementData.ShowImageFonctionnement) {
    //			//set image
    //			FonctionnementImage.gameObject.SetActive (true);
    //			FonctionnementImage.transform.Find ("Image_Fiche_2").gameObject.SetActive (false);
    //			FonctionnementImage.transform.Find ("Image_Fiche").GetComponent<Image> ().sprite = ProjectUIManager.CurrentProjectData.fonctionnementData.Image_Fonctionnement;
    //			FonctionnementImage.GetComponent<ImageFonctionnement> ().StartOpenImage();
    //		}

    //		//set anim button
    //		if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "ChaufferieBiomasse" ||
    //		    ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "PlatformeEffluents") {
    //			Fonctionnement.transform.Find ("ButtonAnim").GetComponent<Button> ().onClick.RemoveAllListeners ();
    //			Fonctionnement.transform.Find ("ButtonAnim").GetComponent<Button> ().onClick.AddListener (() => PlayAnim (ProjectUIManager.CurrentProjectData.fonctionnementData.animatedObject, ProjectUIManager.CurrentProjectData.fonctionnementData.animationclipname));
    //		} else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "citeduvin" ||
    //		         ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "amna") {
    //			Fonctionnement.transform.Find ("ButtonAnim").GetComponent<Button> ().onClick.RemoveAllListeners ();
    //			Fonctionnement.transform.Find ("ButtonAnim").GetComponent<Button> ().onClick.AddListener (() => SwapCamera (true));
    //		} else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "BartsCloseFPOL") {
    //			Fonctionnement.transform.Find ("ButtonRenovations").gameObject.SetActive (true);
    //			Fonctionnement.transform.Find ("ButtonRenovations").GetComponent<Button> ().onClick.RemoveAllListeners ();
    //			Fonctionnement.transform.Find ("ButtonRenovations").GetComponent<Button> ().onClick.AddListener (() => ShowRenovations ());
    //		} else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "BishopGateFPOL") {

    //		}else if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "Wiredemo") {
    //			Fonctionnement.transform.Find ("ButtonWireframe").gameObject.SetActive (true);
    //			Fonctionnement.transform.Find ("ButtonWireframe").GetComponent<Button> ().onClick.RemoveAllListeners ();
    //			Fonctionnement.transform.Find ("ButtonWireframe").GetComponent<Button> ().onClick.AddListener (() => ShowWireframe ());
    //		}

    //	}

    //	public void ShowWireframe(){
    //		_showWire = !_showWire;
    //		if (ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate != null) {
    ////			if (_showWire) {
    ////				ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate.SetActive (true);
    ////				GameObject.FindGameObjectWithTag("Wireframe").GetComponent<WireframeButton> ().SetWireframeActive (true);
    ////				GameObject.FindGameObjectWithTag("Wireframe").GetComponent<BuildingDisplayInfo> ().SetBuildingInfo ();
    ////			} else {
    ////				ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate .SetActive (false);
    ////				GameObject.FindGameObjectWithTag("Wireframe").GetComponent<WireframeButton> ().SetWireframeActive (false);
    ////			}
    //		}
    //	}

    //	public void ShowRenovations(){
    //		_show = !_show;
    //		if (ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate != null) {
    //			if (_show) {
    //				ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate.SetActive (true); 
    //			} else {
    //				ProjectUIManager.CurrentProjectData.fonctionnementData.objectToActivate .SetActive (false);
    //			}
    //		}
    //	}

    //	public void SwapCamera(bool isGoingIn){
    //		//Debug.Log("SwapCamera isGoingIn = "+isGoingIn);
    //		if (isGoingIn) {
    //			ProjectUIManager.CurrentProjectData.fonctionnementData.oldCamera.SetActive (false);
    //			ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.SetActive (true);
    //			Toolz.Managers.LevelManager.ActiveCamera = ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
    //			if(ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "amna"){
    //				anmaAnimCamMove.LauchCameraMvmt();
    //			}
    //		} else {
    //			ProjectUIManager.CurrentProjectData.fonctionnementData.oldCamera.SetActive (true);
    //			ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.SetActive (false);
    //			Toolz.Managers.LevelManager.ActiveCamera = ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();
    //		}
    //	}

    //	public void PlayAnim(Animation animatedObject, string clipName){

    //		if (!_isAnimPlaying) {
    //			_isAnimPlaying = true;
    //			//swap cameras
    //			ProjectUIManager.CurrentProjectData.fonctionnementData.oldCamera.SetActive (false);
    //			ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.SetActive (true);
    //			_savedCameraPos = ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.transform.Find ("ConstraintBox").Find ("CameraHolder").position;
    //			_savedCameraRot = ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.transform.Find ("ConstraintBox").Find ("CameraHolder").rotation;
    //			Toolz.Managers.LevelManager.ActiveCamera = ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.transform.Find ("ConstraintBox").Find ("CameraHolder").Find ("Camera").GetComponent<Camera> ();

    //			animatedObject.GetComponent<Animation>() [clipName].time = 0f;
    //			animatedObject.GetComponent<Animation>() [clipName].speed = 1f;
    //			animatedObject.Play (clipName);

    //			if (ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "ChaufferieBiomasse" ) {
    //				ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.GetComponent<BiomassAnimCamMove> ().LauchCameraMvmt ();
    //			} else if( ProjectUIManager.CurrentProjectData.fonctionnementData.projectID == "PlatformeEffluents"){
    //				ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.GetComponent<EffluentAnimCamMove> ().LauchCameraMvmt ();
    //			}
    //		}
    //	}

    //	public void StopAnim(Animation animatedObject, string clipName){
    //		//swap cameras
    //		ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.SetActive (false);
    //		ProjectUIManager.CurrentProjectData.fonctionnementData.oldCamera.SetActive (true);
    //		Toolz.Managers.LevelManager.ActiveCamera = ProjectUIManager.CurrentProjectData.fonctionnementData.oldCamera.transform.Find("ConstraintBox").Find("CameraHolder").Find("Camera").GetComponent<Camera>();

    //		animatedObject.Play(clipName);
    //		animatedObject.GetComponent<Animation>()[clipName].speed = -1;
    //		animatedObject.GetComponent<Animation>()[clipName].time = 0;

    //		//reset anim camera pos
    //		ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.transform.Find("ConstraintBox").Find("CameraHolder").position = _savedCameraPos;
    //		ProjectUIManager.CurrentProjectData.fonctionnementData.animCamera.transform.Find("ConstraintBox").Find("CameraHolder").rotation = _savedCameraRot;
    //		_isAnimPlaying = false;
    //	}

    //	public void ActivateRealisation(){
    //		SetAllInactive ();
    //		SetRealisationData ();
    //		Debug.Log ("setting realisation active");
    //		Realisation.SetActive (true);
    //		//set slider
    //		Realisation.transform.Find ("SliderVarTest").Find ("Slider").GetComponent<RealisationUITest> ().SetSliderMax ();
    //	}

    //	private void SetRealisationData(){

    //		//set title
    //		_myText = ProjectUIManager.CurrentProjectData.realisationData.TitleRealisation;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		Realisation.transform.Find ("Title").GetComponent<Text> ().text = _myText;

    //		//set text
    //		_myText = ProjectUIManager.CurrentProjectData.realisationData.TextRealisation;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		Realisation.transform.Find ("TextContainer").Find ("Text").GetComponent<Text> ().text = _myText;

    //	}

    //	public void ActivateCoutBenefice(){
    //		if (ProjectUIManager.CurrentProjectData.coutbenefData != null) {
    //			SetAllInactive ();
    //			SetCoutBeneficeData ();
    //			CoutBenefice.SetActive (true);
    //		}
    //	}

    //	private void SetCoutBeneficeData(){
    //		//set title
    //		_myText = ProjectUIManager.CurrentProjectData.coutbenefData.TitleCoutbenef;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		CoutBenefice.transform.Find ("BackGround").Find ("Title").GetComponent<Text> ().text = _myText;

    //		//set text
    //		_myText = ProjectUIManager.CurrentProjectData.coutbenefData.TextCoutbenef;
    //		_myText = _myText.Replace("NEWLINE", "\n");
    //		CoutBenefice.transform.Find ("TextContainer").Find ("Text").GetComponent<Text> ().text = _myText;

    //		if (ProjectUIManager.CurrentProjectData.coutbenefData.ShowImageCoutbenef) {
    //			//set image
    //			CoutBeneficeImage.gameObject.SetActive (true);
    //			CoutBeneficeImage.transform.Find ("Image_Fiche_2").gameObject.SetActive (false);
    //			CoutBeneficeImage.transform.Find ("Image_Fiche").GetComponent<Image> ().sprite = ProjectUIManager.CurrentProjectData.coutbenefData.Image_Coutbenef;
    //			CoutBeneficeImage.GetComponent<ImageCoutbenef> ().StartOpenImage();
    //		}

    //		if (ProjectUIManager.CurrentProjectData.coutbenefData.MyVideoPlayer != null) {
    //			loader.SetActive (true);
    //			ProjectUIManager.CurrentProjectData.coutbenefData.MyVideoPlayer.StartTheVideo ();
    //		}
    //	}
}
