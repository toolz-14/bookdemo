﻿using UnityEngine;
using System.Collections;

public class AnimateMaterial : MonoBehaviour {
    
    public Material[] MaterialsToAnimate;
    public float ScrollSpeed = 0.5f;
    public bool ActivateOnStart = true;

    private Vector2 _animateVector;
	// Use this for initialization
	void Start () {
        _animateVector = Vector2.zero;
        if (ActivateOnStart) {
            StartAnimate();
        }
	}


    public void StartAnimate() {
        InvokeRepeating("Animate", 0, 1 / 24f);
    }

    private void Animate() {
        foreach (Material mat in MaterialsToAnimate) {
            float offset = Time.time * ScrollSpeed;
            _animateVector.y = offset;
            mat.SetTextureOffset("_MainTex", _animateVector);
        }
    }
}
