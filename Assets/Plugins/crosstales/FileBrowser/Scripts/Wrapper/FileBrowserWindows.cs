﻿#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
using UnityEngine;
using System;

namespace Crosstales.FB.Wrapper
{
    /// <summary>File browser implementation for Windows.</summary>
    public class FileBrowserWindows : FileBrowserBase
    {
        #region Variables

        private static string _initialPath = string.Empty;

        private const int OFN_NOCHANGEDIR = 0x00000008;
        private const int OFN_ALLOWMULTISELECT = 0x00000200;
        private const int OFN_EXPLORER = 0x00080000;
        private const int OFN_FILEMUSTEXIST = 0x00001000;
        private const int OFN_PATHMUSTEXIST = 0x00000800;
        private const int OFN_OVERWRITEPROMPT = 0x00000002;
        //private const int OFN_NOVALIDATE = 0x00000100;
        //private const int MAX_OPEN_FILE_LENGTH = 65536;
        private const int MAX_OPEN_FILE_LENGTH = 4096;
        private const int MAX_SAVE_FILE_LENGTH = 1024;
        private const int MAX_PATH_LENGTH = 1024;
        //private const int MAX_FILETITLE_LENGTH = 65536;
        //private const int MAX_FILETITLE_LENGTH = 32768;

        // Constants for sending and receiving messages in BrowseCallBackProc
        private const int WM_USER = 0x400;
        private const int BFFM_INITIALIZED = 1;
        private const int BFFM_SELCHANGED = 2;
        //private const int BFFM_VALIDATEFAILEDA = 3;
        //private const int BFFM_VALIDATEFAILEDW = 4;
        //private const int BFFM_IUNKNOWN = 5; // provides IUnknown to client. lParam: IUnknown*
        //private const int BFFM_SETSTATUSTEXTA = WM_USER + 100;
        //private const int BFFM_ENABLEOK = WM_USER + 101;
        //private const int BFFM_SETSELECTIONA = WM_USER + 102;
        private const int BFFM_SETSELECTIONW = WM_USER + 103;
        private const int BFFM_SETSTATUSTEXTW = WM_USER + 104;
        //private const int BFFM_SETOKTEXT = WM_USER + 105; // Unicode only
        //private const int BFFM_SETEXPANDED = WM_USER + 106; // Unicode only

        // Browsing for directory.
        //private uint BIF_RETURNONLYFSDIRS = 0x0001;  // For finding a folder to start document searching
        //private uint BIF_DONTGOBELOWDOMAIN = 0x0002;  // For starting the Find Computer
        //private uint BIF_STATUSTEXT = 0x0004;  // Top of the dialog has 2 lines of text for BROWSEINFO.lpszTitle and one line if
        // this flag is set.  Passing the message BFFM_SETSTATUSTEXTA to the hwnd can set the
        // rest of the text.  This is not used with BIF_USENEWUI and BROWSEINFO.lpszTitle gets
        // all three lines of text.
        //private uint BIF_RETURNFSANCESTORS = 0x0008;
        //private uint BIF_EDITBOX = 0x0010;   // Add an editbox to the dialog
        //private uint BIF_VALIDATE = 0x0020;   // insist on valid result (or CANCEL)

        private const uint BIF_NEWDIALOGSTYLE = 0x0040;   // Use the new dialog layout with the ability to resize
        // Caller needs to call OleInitialize() before using this API
        //private uint BIF_USENEWUI = 0x0040 + 0x0010; //(BIF_NEWDIALOGSTYLE | BIF_EDITBOX);

        //private uint BIF_BROWSEINCLUDEURLS = 0x0080;   // Allow URLs to be displayed or entered. (Requires BIF_USENEWUI)
        //private uint BIF_UAHINT = 0x0100;   // Add a UA hint to the dialog, in place of the edit box. May not be combined with BIF_EDITBOX
        //private uint BIF_NONEWFOLDERBUTTON = 0x0200;   // Do not add the "New Folder" button to the dialog.  Only applicable with BIF_NEWDIALOGSTYLE.
        //private uint BIF_NOTRANSLATETARGETS = 0x0400;  // don't traverse target as shortcut

        //private uint BIF_BROWSEFORCOMPUTER = 0x1000;  // Browsing for Computers.
        //private uint BIF_BROWSEFORPRINTER = 0x2000;// Browsing for Printers
        //private uint BIF_BROWSEINCLUDEFILES = 0x4000; // Browsing for Everything
        private const uint BIF_SHAREABLE = 0x8000;  // sharable resources displayed (remote shares, requires BIF_USENEWUI)

        private static IntPtr currentWindow = NativeMethods.GetActiveWindow();
        //private static string openFile = Util.Helper.CreateString(" ", MAX_OPEN_FILE_LENGTH);
        private static char[] nullChar = { (char)0 };


        #endregion


        #region Implemented methods

        public override bool canOpenMultipleFiles
        {
            get
            {
                return true;
            }
        }

        public override bool canOpenMultipleFolders
        {
            get
            {
                return false;
            }
        }


        public override string[] OpenFiles(string title, string directory, ExtensionFilter[] extensions, bool multiselect)
        {
            NativeMethods.OpenFileName ofn = new NativeMethods.OpenFileName();
            string dir = getDirectory(directory);

            try
            {
                ofn.dlgOwner = currentWindow;
                ofn.filter = getFilterFromFileExtensionList(extensions);
                ofn.filterIndex = 1;

                if (!string.IsNullOrEmpty(dir))
                {
                    ofn.initialDir = dir;
                }

#if ENABLE_IL2CPP
                //ofn.file = System.Runtime.InteropServices.Marshal.StringToCoTaskMemUni(Util.Helper.CreateString(" ", MAX_OPEN_FILE_LENGTH));
                ofn.file = System.Runtime.InteropServices.Marshal.StringToBSTR(Util.Helper.CreateString(" ", MAX_OPEN_FILE_LENGTH));
#else
                ofn.file = Util.Helper.CreateString(" ", MAX_OPEN_FILE_LENGTH);
#endif
                ofn.maxFile = MAX_OPEN_FILE_LENGTH;

                //ofn.fileTitle = new string(new char[MAX_FILETITLE_LENGTH]);
                //ofn.maxFileTitle = MAX_FILETITLE_LENGTH;

                ofn.title = title;
                ofn.flags = OFN_NOCHANGEDIR | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | (multiselect ? OFN_ALLOWMULTISELECT | OFN_EXPLORER : 0x00000000);
                //ofn.flags = OFN_NOCHANGEDIR | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST;

                ofn.structSize = System.Runtime.InteropServices.Marshal.SizeOf(ofn);

                if (NativeMethods.GetOpenFileName(ofn))
                {
#if ENABLE_IL2CPP
                    //string file = System.Runtime.InteropServices.Marshal.PtrToStringUni(ofn.file);
                    string file = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ofn.file);
#else
                    string file = ofn.file;
#endif

                    //Debug.Log("FILE Len: " + file.Length);

                    if (multiselect)
                    {
                        string[] files = file.Split(nullChar, StringSplitOptions.RemoveEmptyEntries);

                        //Debug.Log("FILE: '" + Util.Helper.ValidateFile(file) + "'");
                        //Debug.Log("FILES: " + files.CTDump());
                        //Debug.Log("FILES count: " + files.Length);

                        if (files.Length > 2)
                        {
                            System.Collections.Generic.List<string> selectedFilesList = new System.Collections.Generic.List<string>();

                            string resultFile;
                            for (int ii = 1; ii < files.Length - 1; ii++)
                            {
                                resultFile = Util.Helper.ValidateFile(files[0] + '\\' + files[ii]);
                                selectedFilesList.Add(resultFile);

                                //Debug.Log("selectedFile: " + resultFile);
                            }

                            return selectedFilesList.ToArray();
                        }
                        /*
                        else
                        {
                            Debug.Log("single selectedFile: " + file);
                        }
                        */
                    }

                    return new string[] { Util.Helper.ValidateFile(file) };
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
            /*
#if ENABLE_IL2CPP
            finally
            {
                if (ofn.file != IntPtr.Zero)
                    //System.Runtime.InteropServices.Marshal.FreeHGlobal(bufferAddress);
                    System.Runtime.InteropServices.Marshal.FreeCoTaskMem(ofn.file);
            }
#endif
*/
            return new string[0];
        }

        public override string[] OpenFolders(string title, string directory, bool multiselect)
        {
            if (Util.Config.DEBUG && !string.IsNullOrEmpty(title))
                Debug.LogWarning("'title' is not supported under Windows.");

            if (multiselect)
                Debug.LogWarning("'multiselect' for folders is not supported under Windows.");

            NativeMethods.BROWSEINFO bi = new NativeMethods.BROWSEINFO();

            if (!string.IsNullOrEmpty(directory))
                _initialPath = directory;

            IntPtr pidl = IntPtr.Zero;
            IntPtr bufferAddress = IntPtr.Zero;
            //System.Text.StringBuilder sb = new System.Text.StringBuilder(MAX_PATH_LENGTH);

            string folder = string.Empty;
            try
            {
                bufferAddress = System.Runtime.InteropServices.Marshal.AllocHGlobal(MAX_PATH_LENGTH);

                bi.dlgOwner = currentWindow;
                bi.pidlRoot = IntPtr.Zero;
                //bi.lpszTitle = title;
                bi.ulFlags = BIF_NEWDIALOGSTYLE | BIF_SHAREABLE;
                bi.lpfn = new NativeMethods.BrowseCallbackProc(onBrowseEvent);
                bi.lParam = IntPtr.Zero;
                bi.iImage = 0;

                pidl = NativeMethods.SHBrowseForFolder(ref bi);

                if (NativeMethods.SHGetPathFromIDList(pidl, bufferAddress))
                {
                    folder = System.Runtime.InteropServices.Marshal.PtrToStringUni(bufferAddress);
                    _initialPath = folder;
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
            finally
            {
                if (bufferAddress != IntPtr.Zero)
                    System.Runtime.InteropServices.Marshal.FreeHGlobal(bufferAddress);

                if (pidl != IntPtr.Zero)
                    System.Runtime.InteropServices.Marshal.FreeCoTaskMem(pidl);

            }

            return new string[] { folder };
        }

        public override string SaveFile(string title, string directory, string defaultName, ExtensionFilter[] extensions)
        {
            NativeMethods.OpenFileName sfn = new NativeMethods.OpenFileName();

            string dir = getDirectory(directory);
            string defaultExtension = getDefaultExtension(extensions);

            //Debug.Log("extensions: " + extensions.CTDump());
            //Debug.Log("ext: '" + defaultExtension + "'");

            try
            {
                sfn.dlgOwner = currentWindow;
                sfn.filter = getFilterFromFileExtensionList(extensions);
                sfn.filterIndex = 1;

                string fileNames;
                if (string.IsNullOrEmpty(dir))
                {
                    fileNames = defaultExtension.Equals("*") ? defaultExtension : defaultName + "." + defaultExtension;
                }
                else
                {
                    fileNames = defaultExtension.Equals("*") ? dir + defaultExtension : dir + defaultName + "." + defaultExtension;
                    sfn.initialDir = dir;
                }
#if ENABLE_IL2CPP
                //sfn.file = System.Runtime.InteropServices.Marshal.StringToCoTaskMemUni(fileNames + Util.Helper.CreateString(" ", MAX_OPEN_FILE_LENGTH - fileNames.Length));
                sfn.file = System.Runtime.InteropServices.Marshal.StringToBSTR(fileNames + Util.Helper.CreateString(" ", MAX_OPEN_FILE_LENGTH - fileNames.Length));
#else
                sfn.file = fileNames + Util.Helper.CreateString(" ", MAX_OPEN_FILE_LENGTH - fileNames.Length);
#endif
                sfn.maxFile = MAX_SAVE_FILE_LENGTH;

                //sfn.fileTitle = new string(new char[MAX_FILETITLE_LENGTH]);
                //sfn.maxFileTitle = MAX_FILETITLE_LENGTH;

                sfn.title = title;
                sfn.defExt = defaultExtension;
                sfn.flags = OFN_NOCHANGEDIR | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT;

                sfn.structSize = System.Runtime.InteropServices.Marshal.SizeOf(sfn);

                if (NativeMethods.GetSaveFileName(sfn))
                {
#if ENABLE_IL2CPP
                    //string file = System.Runtime.InteropServices.Marshal.PtrToStringUni(sfn.file);
                    string file = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(sfn.file);
#else
                    string file = sfn.file;
#endif

                    return Util.Helper.ValidateFile(file);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
            /*
#if ENABLE_IL2CPP
            finally
            {
                if (sfn.file != IntPtr.Zero)
                    //System.Runtime.InteropServices.Marshal.FreeHGlobal(bufferAddress);
                    System.Runtime.InteropServices.Marshal.FreeCoTaskMem(sfn.file);
            }
#endif
*/
            return string.Empty;
        }

        public override void OpenFilesAsync(string title, string directory, ExtensionFilter[] extensions, bool multiselect, Action<string[]> cb)
        {
            cb.Invoke(OpenFiles(title, directory, extensions, multiselect));
        }

        public override void OpenFoldersAsync(string title, string directory, bool multiselect, Action<string[]> cb)
        {
            cb.Invoke(OpenFolders(title, directory, multiselect));
        }

        public override void SaveFileAsync(string title, string directory, string defaultName, ExtensionFilter[] extensions, Action<string> cb)
        {
            cb.Invoke(SaveFile(title, directory, defaultName, extensions));
        }

        #endregion


        #region Private methods

        [AOT.MonoPInvokeCallback(typeof(NativeMethods.BrowseCallbackProc))]
        private static int onBrowseEvent(IntPtr hWnd, int msg, IntPtr lp, IntPtr lpData)
        {
            if (msg == BFFM_INITIALIZED)
            {
                NativeMethods.SendMessage(new System.Runtime.InteropServices.HandleRef(null, hWnd), BFFM_SETSELECTIONW, 1, _initialPath);
            }
            else if (msg == BFFM_SELCHANGED)
            {
                IntPtr pathPtr = System.Runtime.InteropServices.Marshal.AllocHGlobal(260 * System.Runtime.InteropServices.Marshal.SystemDefaultCharSize);

                if (NativeMethods.SHGetPathFromIDList(lp, pathPtr))
                    NativeMethods.SendMessage(new System.Runtime.InteropServices.HandleRef(null, hWnd), BFFM_SETSTATUSTEXTW, 0, pathPtr);

                System.Runtime.InteropServices.Marshal.FreeHGlobal(pathPtr);
            }

            return 0;
        }

        private string getDirectory(string directory, bool addEndDelimiter = true)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(directory))
            {
                result = directory.Replace('/', '\\');

                if (addEndDelimiter)
                {
                    if (!result.EndsWith(Util.Constants.PATH_DELIMITER_WINDOWS))
                    {
                        result += Util.Constants.PATH_DELIMITER_WINDOWS;
                    }
                }
            }

            return result;
        }

        private static string getDefaultExtension(ExtensionFilter[] extensions)
        {
            if (extensions != null && extensions.Length > 0 && extensions[0].Extensions.Length > 0)
            {
                return extensions[0].Extensions[0];
            }

            return "*";
        }

        private static string getFilterFromFileExtensionList(ExtensionFilter[] extensions)
        {
            if (extensions != null && extensions.Length > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                ExtensionFilter filter;

                for (int xx = 0; xx < extensions.Length; xx++)
                {
                    filter = extensions[xx];

                    sb.Append(filter.Name);
                    sb.Append("\0");

                    for (int ii = 0; ii < filter.Extensions.Length; ii++)
                    {
                        sb.Append("*.");
                        sb.Append(filter.Extensions[ii]);

                        if (ii + 1 < filter.Extensions.Length)
                            sb.Append(";");
                    }

                    sb.Append("\0");
                }

                sb.Append("\0");

                if (Util.Config.DEBUG)
                    Debug.Log("getFilterFromFileExtensionList: " + sb.ToString());

                return sb.ToString();
            }

            return Util.Constants.TEXT_ALL_FILES + "\0*.*\0\0";
        }

        #endregion
    }

    internal static class NativeMethods
    {
        public delegate int BrowseCallbackProc(IntPtr hwnd, int uMsg, IntPtr lParam, IntPtr lpData);

        //[System.Runtime.InteropServices.DllImport("Comdlg32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        [System.Runtime.InteropServices.DllImport("Comdlg32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        public static extern bool GetOpenFileName([System.Runtime.InteropServices.In, System.Runtime.InteropServices.Out] OpenFileName ofn);

        //[System.Runtime.InteropServices.DllImport("Comdlg32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        [System.Runtime.InteropServices.DllImport("Comdlg32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        public static extern bool GetSaveFileName([System.Runtime.InteropServices.In, System.Runtime.InteropServices.Out] OpenFileName sfn);

        [System.Runtime.InteropServices.DllImport("shell32.dll")]
        internal static extern IntPtr SHBrowseForFolder(ref BROWSEINFO lpbi);

        // Note that the BROWSEINFO object's pszDisplayName only gives you the name of the folder.
        // To get the actual path, you need to parse the returned PIDL
        [System.Runtime.InteropServices.DllImport("shell32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        //[System.Runtime.InteropServices.DllImport("shell32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        internal static extern bool SHGetPathFromIDList(IntPtr pidl, IntPtr pszPath);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        internal static extern IntPtr GetActiveWindow();

        [System.Runtime.InteropServices.DllImport("user32.dll", PreserveSig = true)]
        public static extern IntPtr SendMessage(System.Runtime.InteropServices.HandleRef hWnd, uint Msg, int wParam, IntPtr lParam);

        //[System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        public static extern IntPtr SendMessage(System.Runtime.InteropServices.HandleRef hWnd, int msg, int wParam, string lParam);

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        //[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        internal struct OpenFileName
        {
            public int structSize;
            public IntPtr dlgOwner;
            public IntPtr instance;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
            public string filter;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPStr)]
            public string customFilter;
            public int maxCustFilter;
            public int filterIndex;
#if ENABLE_IL2CPP
            public IntPtr file;
#else
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
            public string file;
#endif
            public int maxFile;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPStr)]
            public string fileTitle;
            public int maxFileTitle;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
            public string initialDir;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
            public string title;
            public int flags;
            public ushort fileOffset;
            public ushort fileExtension;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
            public string defExt;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
            public string custData;
            public IntPtr hook;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
            public string templateName;
            public IntPtr reservedPtr;
            public int reservedInt;
            public int flagsEx;
        }

        //[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        internal struct BROWSEINFO
        {
            public IntPtr dlgOwner;
            public IntPtr pidlRoot;
            public IntPtr pszDisplayName;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPStr)]
            public string lpszTitle;
            public uint ulFlags;
            public BrowseCallbackProc lpfn;
            public IntPtr lParam;
            public int iImage;
        }
    }
}
#endif
// © 2017-2019 crosstales LLC (https://www.crosstales.com)