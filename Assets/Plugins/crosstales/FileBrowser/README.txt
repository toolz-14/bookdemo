﻿# FileBrowser PRO 2019.4.0

Thank you for buying our asset "File Browser PRO"! 
If you have any questions about this asset, send us an email at [fb@crosstales.com](mailto:fb@crosstales.com). 



## Description
A wrapper for native file dialogs on Windows, macOS, Linux and UWP.

* Works with Windows, macOS, Linux and UWP in editor and runtime
* Full IL2CPP-support
* Open file/folder, save file dialogs supported
* Multiple file selection
* Multiple folder selection on macOS and Linux
* File extension filters
* Other platforms are currently not supported



##Notes:

### macOS
* Sync calls can throw exceptions in development builds after the panel loses and gains focus. Use async calls to avoid this.
* The provided library works with x86_64. If you need x86 support, please replace it with the version inside "FileBrowser.bundle_x86.zip"

### Linux
* The provided library uses GTK3+. If you need an older version, please replace it with the version inside "GTK2.zip"



## Release notes

See "VERSIONS.txt" for details.



## Credits

Partially based on:
https://github.com/gkngkc/UnityStandaloneFileBrowser



## Contact

crosstales LLC
Schanzeneggstrasse 1
CH-8002 Zürich

* [Homepage](https://www.crosstales.com/)
* [Email](mailto:fb@crosstales.com)

### Social media
* [Discord](https://discord.gg/ZbZ2sh4)
* [Facebook](https://www.facebook.com/crosstales/)
* [Twitter](https://twitter.com/crosstales)
* [LinkedIN](https://www.linkedin.com/company/crosstales)



## More information
* [Homepage](https://www.crosstales.com/)
* [AssetStore](https://assetstore.unity.com/lists/crosstales-42213?aid=1011lNGT)
* [Forum](https://forum.unity.com/threads/file-browser-native-file-browser-for-windows-and-macos.510403/)
* [Documentation](https://www.crosstales.com/media/data/assets/FileBrowser/FileBrowser-doc.pdf)
* [API](https://www.crosstales.com/media/data/assets/FileBrowser/api/)

### Videos
[Youtube-channel](https://www.youtube.com/c/Crosstales)

### Demos
* [Windows-Demo](https://www.crosstales.com/media/data/assets/FileBrowser/downloads/FileBrowser_win.zip)
* [macOS-Demo](https://www.crosstales.com/media/data/assets/FileBrowser/downloads/FileBrowser_mac.zip)


`Version: 26.07.2019`