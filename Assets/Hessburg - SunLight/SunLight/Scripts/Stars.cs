﻿using System.Collections;
using Hessburg;
using UnityEngine;

public class Stars : MonoBehaviour {

    public SunLight SunLight;
    public GameObject theStars;

    private Material StarsMaterial;
    private Transform StarsTransform;
    private Transform StarsTransformParent;


    void Start () {
        StarsTransform = theStars.GetComponent<Transform>();
        StarsTransformParent = StarsTransform.parent;
        StarsMaterial = theStars.GetComponent<Renderer>().material;
    }
	
	void Update () {
        // simple stars – follow rotation of the sun and change the transparency at twilight
        StarsTransformParent.eulerAngles = new Vector3(SunLight.GetStarDomeDeclination(), 0.0f, 0.0f);
        StarsTransform.localEulerAngles = new Vector3(0.0f, 360.0f / 24.0f * SunLight.timeInHours, 0.0f);

        Color C;
        if (SunLight.GetSunAltitude() > 180.0)
        {
            C = new Color(1.0f, 1.0f, 1.0f, Mathf.Clamp((355.0f - SunLight.GetSunAltitude()) * 0.05f, 0.0f, 1.0f));
        }
        else
        {
            C = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        }
        StarsMaterial.SetColor("_TintColor", C);
    }
}
